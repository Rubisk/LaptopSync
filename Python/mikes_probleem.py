import time
FAMILIES_TO_PRINT = ["UNKNOWN", "POWER OF TWO", "BY POWER OF TWO"]


solutions = []


def get_family(s):
    binary_n = "{0:b}".format(s[-1])
    digit_sum = sum([int(i) for i in binary_n])
    if digit_sum == 1:
        family = "POWER OF TWO"
    elif digit_sum == 2:
        family = "TWO POWERS OF TWO"
    elif s[3] % 2 == 0:
        family = "MULTIPLE BY POWER OF TWO"
    else:
        family = "UNKNOWN"
    return family


def print_solution(s):
    family = get_family(s)
    if family in FAMILIES_TO_PRINT:
        print(family + " x:{0}, y:{1}, z:{2}, n:{3}".format(*s))


n = 1
old_time = time.time()
while True:
    if time.time() - old_time > 30:
        print("{0} squares evaluated in total".format(n))
        old_time = time.time()
    binary_n_squared = "{0:b}".format(n ** 2)
    binary_len = len(binary_n_squared)
    one_indices = list(filter(lambda x: x[1] == '1', enumerate(binary_n_squared)))
    l = len(one_indices)

    if l == 3:
        solution = [binary_len - x[0] - 1 for x in one_indices] + [n]
        print_solution(solution)
        solutions.append(solution)
    elif l == 2:
        one_indices = list(map(lambda x: binary_len - x[0] - 1, one_indices))
        solution_1 = [one_indices[0], one_indices[1] - 1, one_indices[1] - 1, n]
        solution_2 = [one_indices[0] - 1, one_indices[0] - 1, one_indices[1], n]
        print_solution(solution_1)
        print_solution(solution_2)
        solutions.append(solution_1)
        solutions.append(solution_2)
    elif l == 1:
        one_indices = list(map(lambda x: binary_len - x[0] - 1, one_indices))
        solution = [one_indices[0] - 1, one_indices[0] - 2, one_indices[0] - 2, n]
        print_solution(solution)
        solutions.append(solution)
    n += 1
