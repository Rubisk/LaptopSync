from fractions import gcd
import os
import copy
from collections import Counter
import string
import itertools as it

source = """Beehbkea eted  lebare  n  E
dgpdv lnia eontkvrlsd etZau
EeaeZlmoaeih. d tjentvlstod
dseioherlrertn  dena kannar
nnl d avdaiera o n  lteen r
g ie|nernrnd   ,avle eitfr 
p  tor semaanropaoeenv hor 
  dnuaennr Eadjeevnld dvaBo
no,  aoSke vknoaee ndtok  e
ntmjriwednvndai  aedbr B  """


source = source.replace("\n", "")
source = source.replace(" ", "*")
print(source.index("."))
for size in range(2, 40):
    lengths = [0 for _ in range(size)]
    d = 1
    x = 0
    for _ in range(269):
        lengths[x] += 1
        if x == 0:
            d = 1
        elif x == size - 1:
            d = -1
        x += d

    partial_sums = [sum(lengths[:i + 1]) for i in range(len(lengths))]

    new_source = copy.copy(source)
    rows = []
    for l in lengths:
        rows.append(new_source[:l])
        new_source = new_source[l:]
    d = 1

    x = 0

    result = ""
    for _ in range(269):
        result += rows[x][0]
        rows[x] = rows[x][1:]

        if x == 0:
            d = 1
        elif x == size - 1:
            d = -1
        x += d
    print(result)