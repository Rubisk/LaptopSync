from fractions import gcd
import os
import copy
from collections import Counter
import string
import itertools as it

source = """Beehbkea eted  lebare  n  E
dgpdv lnia eontkvrlsd etZau
EeaeZlmoaeih. d tjentvlstod
dseioherlrertn  dena kannar
nnl d avdaiera o n  lteen r
g ie|nernrnd   ,avle eitfr 
p  tor semaanropaoeenv hor 
  dnuaennr Eadjeevnld dvaBo
no,  aoSke vknoaee ndtok  e
ntmjriwednvndai  aedbr B  """

# for j in range(1,10):

# for i in range(2, 20):
for k in range(1,100):
    columns = ["" for _ in range(k)]

    source = source.replace(" ", "*")
    source = source.replace("\n", "")

    for (i, s) in enumerate(source):
        columns[i % len(columns)] += s

    if "." in columns[-1]:
        for c in columns:
            print(c)
        input()

def fit_together(col1, col2):
    assert len(col1) >= len(col2)
    for c, d in zip(col1, col2):
        if c == " " and d == " ":
            return False
        if c == "," and d != " ":
            return False
        # if d in "EZBS"and c != " ":
        #     return False
    return True

def find_valid_column_orders(long_columns, short_columns, head=[]):
    columns = long_columns if len(long_columns) > 0 else short_columns

    if len(columns) == 0:
        yield head
    for col in columns:
        if len(head) == 0 and not col[0] in "EZBS":
            continue
        if len(columns) > 1 and col[-1] == ".":
            continue
        if len(head) > 0 and not fit_together(head[-1], col):
            continue
        if len(long_columns) > 0:
            new_long_columns = copy.copy(long_columns)
            new_long_columns.remove(col)
            for order in find_valid_column_orders(new_long_columns, short_columns, head + [col]):
                yield order
        else:
            assert len(long_columns) == 0
            new_short_columns = copy.copy(short_columns)
            new_short_columns.remove(col)
            for order in find_valid_column_orders(long_columns, new_short_columns, head + [col]):
                yield order


# # FOR 12 COLUMNS!!!!
# for x in it.combinations(range(12), 5):
#     special_increments = [1 if i in x else 0 for i in range(12)]
#     if special_increments[0] + special_increments[1] + special_increments[2] != 1:
#         continue
#     if special_increments[2] != 1:
#         continue
#     sizes = [22 + x for x in special_increments]

#     assert sum(sizes) == len(source )

#     new_source = copy.copy(source)

#     long_columns = []
#     short_columns = []


#     for s in sizes:
#         if s == 23:
#             long_columns.append(new_source[:s])
#         else:
#             short_columns.append(new_source[:s])
#         new_source = new_source[s:]
    

#     for order in find_valid_column_orders(long_columns, short_columns):
#         for c in order:
#             print(c)
#         input()
#         result = ""
#         for i in range(22):
#             for col in order:
#                 result += col[i]
#             # for col in order:
#             #     result += col[i]

#         for col in order:
#             if len(col) == 23:
#                 result += col[22]
#         print(result)


# # FOR 16 COLUMNS!!!!
# count = 0
# for x in it.combinations(range(16), 13):
#     count += 1
#     print(count)
#     special_increments = [1 if i in x else 0 for i in range(16)]
#     if special_increments[0] + special_increments[1] + special_increments[2] + special_increments[3] != 3:
#         continue
#     if special_increments[3] != 1:
#         continue
#     sizes = [16 + x for x in special_increments]

#     assert sum(sizes) == len(source )

#     new_source = copy.copy(source)

#     long_columns = []
#     short_columns = []

#     for s in sizes:
#         if s == 17:
#             long_columns.append(new_source[:s])
#         else:
#             short_columns.append(new_source[:s])
#         new_source = new_source[s:]

#     # for col in long_columns + short_columns:
#     #     print(col)

#     for order in find_valid_column_orders(long_columns, short_columns):
#         for c in order:
#             print(c)
#         input()
#         result = ""
#         for i in range(16):
#             for col in order:
#                 result += col[i]

#         for col in order:
#             if len(col) == 17:
#                 result += col[16]
#         print(result)
