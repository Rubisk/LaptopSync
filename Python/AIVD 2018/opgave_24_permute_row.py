from fractions import gcd
import os
import copy
from collections import Counter
import random

source = """Beehbkea eted  lebare  n  E
dgpdv lnia eontkvrlsd etZau
EeaeZlmoaeih. d tjentvlstod
dseioherlrertn  dena kannar
nnl d avdaiera o n  lteen r
g ie-nernrnd   ,avle eitfr 
p  tor semaanropaoeenv hor 
  dnuaennr Eadjeevnld dvaBo
no,  aoSke vknoaee ndtok  e
ntmjriwednvndai  aedbr B  """.lower()


words = set()
word_starts = {}
word_ends = {}

for file in os.listdir("Dutch"):
    print("Reading Dictionaries: {0}.".format(file))
    for line in open("Dutch\\" + file).readlines():
        word = line[:-1].lower()
        if len(word) == 1 and word != "e":
            continue
        words.add(word)
        if len(word) == 1 and word != "e":
            continue
        for i in range(len(word)):
            start = word[:i + 1]
            word_starts[start] = float(word_starts.get(start, 0) + 1)
            end = word[i:]
            word_ends[end] = float(word_ends.get(end, 0) + 1)

def find_permutations(row, head=""):
    for c in set(row):
        # if len(head) > 18:
        #     print(head)
        new_head = head + c
        row_words = new_head.split(" ")
        if len(row_words[0]) > 0 and row_words[0] not in word_ends:
            continue

        should_continue = False
        for word in row_words[1:-1]:
            if len(word) <= 2 or word not in words:
                should_continue = True
                break
        if should_continue:
            continue

        if len(row_words[-1]) > 0 and row_words[-1] not in word_starts:
            continue

        i = row.index(c)
        new_row = row[:i] + row[i + 1:]
        if len(new_row) > 0:
            for perm in find_permutations(new_row, new_head):
                yield perm
        else:
            yield new_head



for (i, row) in enumerate(source.split("\n")):
    if i != 4:
        continue
    row.replace(".", "")
    row.replace(",", "")
    row.replace("-", "")
    print("Analyzing row {0}, which reads <{1}>.".format(i, row))
    for perm in find_permutations(row):
        print(perm)