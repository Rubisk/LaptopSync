from fractions import gcd
import os
import copy
from collections import Counter
import string
import itertools as it

source = """Beehbkea eted  lebare  n  E
dgpdv lnia eontkvrlsd etZau
EeaeZlmoaeih. d tjentvlstod
dseioherlrertn  dena kannar
nnl d avdaiera o n  lteen r
g ie|nernrnd   ,avle eitfr 
p  tor semaanropaoeenv hor 
  dnuaennr Eadjeevnld dvaBo
no,  aoSke vknoaee ndtok  e
ntmjriwednvndai  aedbr B  """

words = set()
for file in os.listdir("Dutch"):
    print("Reading Dictionaries: {0}.".format(file))
    for line in open("Dutch\\" + file).readlines():
        word = line[:-1].lower()
        if len(word) == 1 and word != "u":
            continue
        words.add(word)

# rows = source.split("\n")
# row_counters = list(map(Counter, rows))

# words = set()
# prefixes = [set() for i in range(40)]
# for file in os.listdir("Dutch"):
#     for line in open("Dutch\\" + file).readlines():
#         if len(line) < 13:
#             break
#         word_counter = Counter(line[:-1])

#         good_word = [True for i in row_counters]
#         for char in word_counter:
#             for (i, row_counter) in enumerate(row_counters):
#                 if not good_word[i]:
#                     continue
#                 if word_counter[char] > row_counter[char]:
#                     good_word[i] = False
#         for (i, row) in enumerate(good_word):
#             if good_word[i]:
#                 if i == 4:
#                     print(i, line[:-1])



# def is_admissible(string):
#     while len(string) > 0 and string[0] in " ,.?":
#         string = string[1:]
#     if len(string) == 0:
#         return True
#     for i in range(1, len(string)):
#         if string[:i] in words:
#             if is_admissible(string[i:]):
#                 return True
#     if string in prefixes[len(string)]:
#         return True
#     return False



# rows = source.split("\n")
# columns_ = [[rows[i][j] for i in range(len(rows))] for j in range(len(rows[0]))]

# def try_permutations(prefix=[], columns=None):
#     if columns == None or len(columns) == 0:
#         return
#     prefix_len = len(prefix)
#     for i in range(len(columns)):
#         prefix.append(columns[i])
#         admissible = True
#         for row in range(len(prefix[0])):
#             admissible &= is_admissible("".join([col[row] for col in prefix]))
#         if not admissible:
#             prefix = prefix[:-1]
#             continue
        
#         assert(len(columns[:i] + columns[i + 1:]) == len(columns) - 1)
#         try_permutations(copy.copy(prefix), columns[:i] + columns[i + 1:])
#         if len(prefix) > 3:
#             for row in range(len(prefix[0])):
#                 print("".join([col[row] for col in prefix]))

#         prefix = prefix[:-1]
#         assert len(prefix) == prefix_len

# try_permutations([], columns=columns_)


source = source.replace("\n", "")
# count = 0
# while True:
#     for _ in range(10):
#         result = ""
#         for j in range(27):
#             for i in range(10):
#                 if j != 26 or i != 9:
#                     if j % 2 == 0:
#                         result += source[i][j]
#                     else:
#                         result += source[-i - 1][j]
#         print(result, count)
#         if result[-1] == ".": print(result, count)
#         count += 1
#         # for i in range(10):
#         #     print(result[i*27:(i + 1)*27])
#         source = [result[i*27:(i + 1)*27] for i in range(10)]

# for x in it.combinations(range(8), 5):
#     special_increments = [1 if i in x else 0 for i in range(8)]
#     if  special_increments[0] != 0 or special_increments[1] != 1:
#         continue
#     sizes = [33 + x for x in special_increments]

#     assert sum(sizes) == len(source)

#     new_source = copy.copy(source)

#     long_columns = []
#     short_columns = []

#     for s in sizes:
#         if s == 34:
#             long_columns.append(new_source[:s])
#         else:
#             short_columns.append(new_source[:s])
#         new_source = new_source[s:]

#     for col in long_columns + short_columns:
#         print(col)
#     input()
    
    # for long_col_order in it.permutations(long_columns):
    #     for short_col_order in it.permutations(short_columns):
    #         result = ""
    #         for i in range(33):
    #             for col in long_col_order + short_col_order:
    #                     result += col[i]

    #         for col in long_col_order:
    #             result += col[33]
    #         # assert len(result) == 269
    #         # print(len(result))
    #         # if result[0] in "EZBS":
    #         print(result)

def fit_together(col1, col2):
    assert len(col1) >= len(col2)
    for c, d in zip(col1, col2):
        if c == " " and d == " ":
            return False
        if c == "," and d != " ":
            return False
        if d in "EZBS"and c != " ":
            return False
        # if d == " "and c in "EZBS":
        #     return False
    return True


def find_valid_column_orders(long_columns, short_columns, head=[]):
    columns = long_columns if len(long_columns) > 0 else short_columns

    if len(head) == 12:
        for i in range(len(short_columns[0])):
            row = "".join([col[i] for col in head])
            test_words = row.split(" ")[1:-1]
            for word in test_words:
                if "E" in word or "Z" in word or "B" in word or "S" in word or "|" in word:
                    continue
                word.replace(",", "")
                word.replace(".", "")
                if word.upper() not in words:
                    return


    if len(columns) == 0:
        yield head
    for col in columns:
        if len(head) == 0 and not col[0] in "EZBS":
            continue
        if len(columns) > 1 and col[-1] == ".":
            continue
        if len(head) > 0 and not fit_together(head[-1], col):
            continue
        if len(long_columns) > 0:
            new_long_columns = copy.copy(long_columns)
            new_long_columns.remove(col)
            for order in find_valid_column_orders(new_long_columns, short_columns, head + [col]):
                yield order
        else:
            assert len(long_columns) == 0
            new_short_columns = copy.copy(short_columns)
            new_short_columns.remove(col)
            for order in find_valid_column_orders(long_columns, new_short_columns, head + [col]):
                yield order


# FOR 12 COLUMNS!!!!
# for x in it.combinations(range(12), 5):
#     special_increments = [1 if i in x else 0 for i in range(12)]
#     if special_increments[0] + special_increments[1] + special_increments[2] != 1:
#         continue
#     if special_increments[2] != 1:
#         continue
#     sizes = [22 + x for x in special_increments]

#     assert sum(sizes) == len(source )

#     new_source = copy.copy(source)

#     long_columns = []
#     short_columns = []


#     for s in sizes:
#         if s == 23:
#             long_columns.append(new_source[:s])
#         else:
#             short_columns.append(new_source[:s])
#         new_source = new_source[s:]

#     # for col in long_columns + short_columns:
#     #     print(col)
    
#     for order in find_valid_column_orders(long_columns, short_columns):
#         # for c in order:
#         #     print(c)
#         # input()
#         result = ""
#         for i in range(22):
#             for col in order:
#                 result += col[i]
#             # for col in order:
#             #     result += col[i]

#         for col in order:
#             if len(col) == 23:
#                 result += col[22]
#         print(result)


# # FOR 16 COLUMNS!!!!
# count = 0
# for x in it.combinations(range(16), 13):
#     count += 1
#     print(count)
#     special_increments = [1 if i in x else 0 for i in range(16)]
#     if special_increments[0] + special_increments[1] + special_increments[2] + special_increments[3] != 3:
#         continue
#     if special_increments[3] != 1:
#         continue
#     sizes = [16 + x for x in special_increments]

#     assert sum(sizes) == len(source )

#     new_source = copy.copy(source)

#     long_columns = []
#     short_columns = []

#     for s in sizes:
#         if s == 17:
#             long_columns.append(new_source[:s])
#         else:
#             short_columns.append(new_source[:s])
#         new_source = new_source[s:]

#     # for col in long_columns + short_columns:
#     #     print(col)

#     for order in find_valid_column_orders(long_columns, short_columns):
#         for c in order:
#             print(c)
#         input()
#         result = ""
#         for i in range(16):
#             for col in order:
#                 result += col[i]

#         for col in order:
#             if len(col) == 17:
#                 result += col[16]
#         print(result)


count = 0
for x in it.combinations(range(24), 5):
    special_increments = [1 if i in x else 0 for i in range(24)]

    sizes = [11 + x for x in special_increments]
    # print(sizes)
    partial_sums = [sum(sizes[:i + 1]) for i in range(len(sizes))]
    if 67 not in partial_sums:
        continue

    assert sum(sizes) == len(source )

    new_source = copy.copy(source)

    long_columns = []
    short_columns = []

    should_continue = [False, False]
    for s in sizes:
        if s == 12:
            long_columns.append(new_source[:s])
            if long_columns[-1][-1] == ".":
                should_continue[0] = True
            if long_columns[-1][0] in "EZBS" and long_columns[-1][-1] != ".":
                should_continue[1] = True
        else:
            short_columns.append(new_source[:s])
        new_source = new_source[s:]


    if not all(should_continue):
        continue

    for order in find_valid_column_orders(long_columns, short_columns):
        result = ""

        for i in range(11):
            for col in order:
                result += col[i]

        for col in order:
            if len(col) == 12:
                result += col[11]
        print(result)
