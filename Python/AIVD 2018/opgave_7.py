alphabet = "abcdefghijklmnopqrstuvwxyz"
import itertools as it
primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199]


def fib(n):
    if n in (1, 2):
        return 1
    return fib(n - 1) + fib(n - 2)

def v(letter):
    return alphabet.index(letter)

def l(value):
    return alphabet[value]

def abc(woord):
    letters = sum(map(v, woord))
    klinkers = sum(map(lambda x: v(x) if x in "eoiau" else 0, woord))
    return(letters, (letters - klinkers) * klinkers, letters + klinkers * (klinkers - 1))

# while True:
#     print(abc(input()))
