from scipy import misc
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import itertools as it

image = misc.imread("opgave_6.png")

alphabet = "abcdefghijklmnopqrstuvwxyz"

test = np.zeros([21, 55])
new_test = np.zeros([21, 55])

result = np.zeros([21 * 31, 55 * 31, 4], dtype='uint8')
result_string = ""

W = [255, 255, 255, 255]
B = [0, 0, 0, 128]


test_square = np.array([[B, W, W, W], [W, B, W, W], [W, W, B, W], [W, W, W, B]])



for row in range(21):
    for column in range(55):
        row_start = int(row * len(image) / 21)
        column_start = int(column * len(image[0]) / 55)
        square = image[row_start:row_start + 31,column_start:column_start + 31]

        triangle_center = [0, 0]
        for x in range(len(square)):
            for y in range(len(square[0])):
                weight = square[x][y][2] / 255
                triangle_center[0] += (x - len(square) / 2) * weight
                triangle_center[1] += (y - len(square[0]) / 2) * weight

        x, y = triangle_center
        # if (column + row) % 2 == 0:
        #     new_square = np.rot90(square)
        #     new_square = np.rot90(new_square)
        # else:
        #     new_square = square
        # RECHTSONDER
        if x < 0 and y < 0:
            result_string += "-"
        elif x < 0 and y > 0:
            result_string += " "

        # RECHTSBOVEN
        elif x > 0 and y < 0:
            result_string += " "

        # LINKSBOVEN
        elif x > 0 and y > 0:
            result_string += "."

        # for (i, j) in it.product(range(31), repeat=2):
        #     if i < len(new_square) and j < len(new_square[0]):
        #         for t in range(4):
        #             result[31 * row + i][31 * column + j][t] = int(new_square[i][j][t])

# print(result_string)
# print(len(result_string))
# count = 0
# for y1, x1 in it.product(range(21), range(51)):
#     for y2, x2 in it.product(range(21), range(51)):
#         if x1 >= x2 or y1 >= y2:
#             continue
#         if test[y1][x1] != 3:
#             continue
#         if test[y2][x1] != 1:
#             continue
#         if test[y1][x2] != 2:
#             continue
#         if test[y2][x2] != 0:
#             continue
#         new_test[y1][x1] = 255
#         new_test[y1][x2] = 255
#         new_test[y2][x1] = 255
#         new_test[y2][x2] = 255
# print(new_test)


# set1 = [len([x for x in a if x == 0]) for a in test]
# set2 = [len([x for x in a if x == 1]) for a in test]
# set3 = [len([x for x in a if x == 2]) for a in test]
# set4 = [len([x for x in a if x == 3]) for a in test]

# print(sum(set1))
# print(sum(set2))
# print(sum(set3))
# print(sum(set4))

# # # print("".join([alphabet[len([x for x in a if x == 2])] for a in test]))
# # # print(test)
# # # print(len([x for x in a if x == 3]) for a in test]))
# # test = np.rot90(test)
# # tes1 = [len([x for x in a if x == 0]) for a in test]
# # tes2 = [len([x for x in a if x == 1]) for a in test]
# # tes3 = [len([x for x in a if x == 2]) for a in test]
# # tes4 = [len([x for x in a if x == 3]) for a in test]
# # img = Image.fromarray(result, 'RGBA')
# # img.save("opgave_6_plaatje", format="png")
# # img.show()
print(result_string)
# print(image)
# plt.imshow(result, interpolation='none')
# plt.show()
# print(tes1)
# print(tes2)
# print(tes3)
# print(tes4)
