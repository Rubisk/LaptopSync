from fractions import gcd
import os
import copy
from collections import Counter
import random
import numpy as np
import math

source = """Beehbkea eted  lebare  n  E
dgpdv lnia eontkvrlsd etZau
EeaeZlmoaeih.  dtjentvlstod
dseioherlrertn  dena kannar
nnl  davdaiera o n  lteen r
g ie?nernrnd   ,avle eitfr 
p  tor semaanropaoeenvh or 
  dnuaennr Eadjeevnld dvaBo
no,  aoSke vknoaee ndtok  e
ntmjriwednvndai  aedbr B   """.upper()


frequencies = {}
pair_frequencies = {}
pair_totals = 0
letter_totals = 0

test = Counter(source.split('\n')[1])

for file in os.listdir("Dutch"):
    for line in open("Dutch\\" + file).readlines():
        word = line[:-1]
        if len(word) < 2:
            continue
        for i in range(len(word)):
            letter_totals += 1
            frequencies[word[i]] = frequencies.get(word[i], 0.0) + 1
            if i < len(word):
                if i == 0:
                    pair = " " + word[0]
                elif i == len(word) - 1:
                    pair = word[-1] + " "
                else:
                    pair = word[i:i + 2]
                pair_frequencies[pair] = pair_frequencies.get(pair, 0.0) + 1
                pair_totals += 1

for (key, value) in frequencies.items():
    frequencies[key] = frequencies[key] / letter_totals * 0.95
frequencies[" "] = 0.05

for (key, value) in pair_frequencies.items():
    pair_frequencies[key] = pair_frequencies[key] / pair_totals

pair_frequencies["  "] = 10E-10
print(frequencies)
print(pair_frequencies)

source = source.replace(".", " ")
source = source.replace("?", " ")
source = source.replace(",", " ")

rows = source.lower().split("\n")

result = np.zeros([len(rows[0]), len(rows[0])])

for col1 in range(len(rows[0])):
    for col2 in range(len(rows[0])):
        likelihood = 0
        if col1 == col2:
            continue
        for row in rows:
            c = row[col1]
            d = row[col2]
            pair = c + d
            c_prob = frequencies.get(c, 10E-5)
            d_prob = frequencies.get(d, 10E-5)
            pair_prob = pair_frequencies.get(pair, 10E-5)

            likelihood += pair_prob / c_prob / d_prob
        result[col1][col2] = likelihood

for column in result:
    print([x[1] for x in sorted(enumerate(column), key=lambda s: s[1])][-3:])
for column in result:
    print([x[0] for x in sorted(enumerate(column), key=lambda s: s[1])])

