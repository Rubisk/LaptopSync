from fractions import gcd
import os
import copy
from collections import Counter
import random

source = """Beehbkea eted  lebare  n  E
dgpdv lnia eontkvrlsd etZau
EeaeZlmoaeih.  dtjentvlstod
dseioherlrertn  dena kannar
nnl  davdaiera o n  lteen r
g ie?nernrnd   ,avle eitfr 
p  tor semaanropaoeenvh or 
  dnuaennr Eadjeevnld dvaBo
no,  aoSke vknoaee ndtok  e
ntmjriwednvndai  aedbr B  !""".upper()


words = set()
word_starts = {}
word_ends = {}

test = Counter(source.split('\n')[1])

for file in os.listdir("Dutch"):
    for line in open("Dutch\\" + file).readlines():
        word = line[:-1]
        words.add(word)
        for i in range(len(word)):
            start = word[:i]
            word_starts[start] = float(word_starts.get(start, 0) + 1)
            end = word[i:]
            word_ends[end] = float(word_ends.get(end, 0) + 1)

            # REMOVE THIS
            if len(word[i:]) > 2:
                words.add(word[i:])
            if len(word[:i]) > 2:
                words.add(word[:i])
            if len(word) > 10:
                good_word = True
                for char in word:
                    if test[char] < Counter(word)[char]:
                        good_word = False
                if good_word and word[0] == 'Z':
                    print(word)

# for i in range(1, 40):
#     total = max([value for (key, value) in word_ends.items() if len(key) == i])
#     for (key, value) in word_ends.items():
#         if len(key) == i:
#             word_ends[key] = word_ends[key] / total

# def score_grid(grid):
#     score = 0
#     for row in grid:
#         for i in range(len(row)):
#             for j in range(i):
#                 row_slice = row[j:i]
#                 if row_slice in words and len(row_slice) > 2:
#                     score += len(row_slice) ** 2
#     return score


# iters = 0
# old_grid = source.split("\n")
# old_score = score_grid(old_grid)
# while True:
#     grid = copy.copy(old_grid)
#     for _ in range(5):
#         col1 = random.randint(0, 26)
#         col2 = col1
#         while col2 == col1:
#             col2 = random.randint(0, 26)
#         new_grid = []
#         if col1 > col2:
#             col1, col2 = col2, col1
#         for row in grid:
#             new_row = row[:col1] + row[col2] + row[col1 + 1:col2] + row[col1] + row[col2 + 1:]
#             if "!" in new_row:
#                 new_row = new_row.replace("!", "")
#                 new_row += "!"
#             new_grid.append(new_row)
#         grid = copy.copy(new_grid)
#     new_score = score_grid(grid)
#     if new_score > old_score:
#         for row in grid:
#             print(row)
#         print(new_score)
#         old_score = new_score
#         old_grid = grid
#     iters += 1
