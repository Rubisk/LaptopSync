import math
import itertools as it

rekenlessen = set()

count = 0
for (T, N, R) in it.product(range(0, 10), repeat=3):
    S = (- T + N + T * N) % 10
    t = str(T)
    n = str(N)
    r = str(R)
    s = str(S)
    for E in range(0, 10):
        e = str(E)
        st = int(s + t)
        en = int(e + n)
        et = int(e + t)
        rs = int(r + s)
        if (st - en - et * en + rs) % 100 == 0:
            count += 1
            for (M, I) in it.product(range(0, 10), repeat=2):
                m = str(M)
                i = str(I)
                erst = int(e + r + s + t)
                enen = int(e + n + e + n)
                met = int(m + e + t)
                tien = int(t + i + e + n)
                ters = int(t + e + r + s)
                if (erst - enen - met * tien + ters) % 10000 == 0:
                    for (K, L) in it.product(range(0, 10), repeat=2):
                        l = str(L)
                        k = str(K)
                        kerst = int(k + e + r + s + t)
                        rekenen = int(r + e + k + e + n + e + n)
                        letters = int(l + e + t + t + e + r + s)
                        # print(kerst - rekenen - met * tien + letters)
                        if kerst == rekenen + met*tien - letters:
                            if len(set([k, e, r, s, t, m, i, n, l])) == 9:
                                print("MINSTREEL:", m + i + n + s + t + r + e + e + l)
                                print("REKENLES:", r + e + k + e + n + l + e + s)
                                print("TROTSEREN:", t + r + k + t + s + e + r + e + n)

for (les, less) in it.product(rekenlessen, repeat=2):
    # print(set(str(les * less)).intersection(set(["1", "8", "9"])), les * less)
    needed_letters = len(set(str(les * less)).intersection(set(["1", "8", "9"])))
    print(needed_letters)
    # if "0" not in str(les * less):
    #     if needed_letters <= 1:
    #         print(str(les)[2], les * less)