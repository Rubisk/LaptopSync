import base64
import itertools as it

frequencies = [7.66, 1.36, 1.30, 5.41, 19.06, 0.73, 3.12, 3.12, 6.29, 1.82, 2.79, 3.8, 2.56, 9.91, 5.8, 1.49, 0.01, 5.62,
               3.84, 6.42, 2.1, 2.24, 1.72, 0.05, 0.06, 1.60, 0.03, 0.03,0.01, 0.01, 0.02]

characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

def value(character):
    return characters.index(character)

def difference(a, b):
    result1 = []
    result2 = []
    for (c, d) in zip(list(a), list(b)):
         result2.append(characters[value(c) - value(d) % 26])
         result1.append(characters[value(d) - value(c) % 26])
    return "".join(result1), "".join(result2)

while True:
    x = input()
    print(difference(x, "Rgx hppeq"))