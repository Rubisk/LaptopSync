from fractions import gcd
import os
import copy
from collections import Counter
import random

source = """T ,be  ou erkmeiodtoentn em
   ti t?Godnw srne ah erkes
emgute toosenv rrt  edg ihm
roeSgioaonaejnnrsm  sd  sdd
tedaaraerel otrd  om a.szpe
hc no nih e e jt emeo iMuon
nigiwe tn s  mw osdvoazncao
raightegkjei letndei.ne ink""".lower()


words = set()
word_starts = {}
word_ends = {}

for file in os.listdir("Dutch"):
    print("Reading Dictionaries: {0}.".format(file))
    for line in open("Dutch\\" + file).readlines():
        word = line[:-1]
        words.add(word)
        for i in range(len(word)):
            start = word[:i + 1]
            word_starts[start] = float(word_starts.get(start, 0) + 1)
            end = word[i:]
            word_ends[end] = float(word_ends.get(end, 0) + 1)



for i in range(1, 35):
    print("Sorting word ends of len {0}.".format(i))
    try:
        total = max([value for (key, value) in word_ends.items() if len(key) == i])
        for (key, value) in word_ends.items():
            if len(key) == i:
                word_ends[key] = word_ends[key] / total
    except:
        pass


for i in range(1, 35):
    print("Sorting word starts of len {0}.".format(i))
    try:
        total = max([value for (key, value) in word_starts.items() if len(key) == i])
        for (key, value) in word_starts.items():
            if len(key) == i:
                word_starts[key] = word_starts[key] / total
    except:
        pass

def end_score(word):
    score = 0
    i = 0
    while word[-i - 1:] in word_ends and i <= len(word):
        i += 1
        score = max(score, i * word_ends[word[-i:]])
    return score


def start_score(word):
    score = 0
    i = 0
    while word[:i + 1] in word_starts and i <= len(word):
        i += 1
        score = max(score, i * word_starts[word[:i]])
    return score


def score_grid(grid):
    score = 0
    for row in grid:
        word_sections = row.split(" ")
        score += end_score(word_sections[0])
        score += start_score(word_sections[-1])
        for word in word_sections[1:-1]:
            score += start_score(word) + end_score(word)
    return score


iters = 0
old_grid = source.split("\n")
old_score = score_grid(old_grid)
print(old_score)

counter = 0
to_permute = 2
while True:
    grid = copy.copy(old_grid)
    for _ in range(to_permute):
        col1 = random.randint(0, 26)
        col2 = col1
        while col2 == col1:
            col2 = random.randint(0, 26)
        new_grid = []
        if col1 > col2:
            col1, col2 = col2, col1
        for row in grid:
            new_row = row[:col1] + row[col2] + row[col1 + 1:col2] + row[col1] + row[col2 + 1:]
            if "!" in new_row:
                new_row = new_row.replace("!", "")
                new_row += "!"
            new_grid.append(new_row)
        grid = copy.copy(new_grid)
    new_score = score_grid(grid)
    if new_score > old_score:
        for row in grid:
            print(row)
        print(new_score)
        old_score = new_score
        old_grid = grid
        counter = 0
        to_permute = max(2, to_permute - 2)
    counter += 1
    if counter > 10 ** to_permute:
        counter = 0
        to_permute += 1
    iters += 1
