import math
import itertools as it


def get_all_substrings(string):
  length = len(string)
  alist = []
  for i in range(length):
    for j in range(i,length):
      alist.append(string[i:j + 1])
  return alist

n = 612242109049354757693703731756141884122575855425310699918367263271480642730811111952684256523677275662759320997
for substring in get_all_substrings(str(n)):
    if int(substring) == 0 or not 1 < int(substring[::-1]) < n:
        continue
    if n % int(substring) == 0 and len(substring) > 0:
        print(substring)
    if n % int(substring[::-1]) == 0 and len(substring) > 0:
        print("REVERSE", substring)