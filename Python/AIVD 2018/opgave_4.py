alphabet = "abcdefghijklmnopqrstuvwxyz"
import itertools as it
from collections import Counter

def v(letter):
    return alphabet.index(letter) + 1

def l(value):
    return alphabet[value - 1]

# rmelad
for a1 in range(26):
    a2 = (v("m") - a1) % 26
    a3 = (a2 + v("d")) % 26
    a4 = (a3 + v("m")) % 26
    print("DE." + l(a4) + "m" + l(a3) + l(a1) + l(a2) + "d")

# for a1 in range(1, 26):
#     a2 = v("w") - a1
#     if 0 <= a1 <= 26 and 0 <= a2 <= 26:
#         print("w" + l(a1) + l(a2) + "d")

for (a1, a2, a4) in it.product(range(1, 27), repeat=3):
    b1 = (a1 + a2) % 26
    b2 = (a2 + v("d")) % 26
    b3 = (v("d") + a4) % 26
    c1 = (b1 + b2) % 26 
    c2 = (b2 + b3) % 26 
    d1 = (c1 + c2) % 26
    if max([a1, a2, a4, b1, b2, b3, c1, c2, d1]) > 26:
        continue
    elif min([a1, a2, a4, b1, b2, b3, c1, c2, d1]) < 0:
        continue
    text = l(d1) + l(c1) + l(c2) + l(b1) + l(b2) + l(b3) + l(a1) + l(a2) + "d" + l(a4)
    syllables = sum([Counter(text)[i] for i in "aeiou"])
    stupids = sum([Counter(text)[i] for i in "qxyz"])
    if syllables > 2 and stupids < 1:
        if "win" in text:
            print(text)

# for a2 in range(1, 26):
#     b1 = (a2 + v("n")) % 26
#     b2 = (v("t") - b1) % 26
#     a3 = (b2 - a2) % 26
#     print("t" + l(b1) + l(b2) + "n" + l(a2) + l(a3))
