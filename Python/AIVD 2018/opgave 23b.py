import math
import itertools as it

for i in range(1, 10):
    for perm in it.permutations(range(1, i + 1)):
        number = int("".join(map(str, perm)))
        if math.sqrt(number).is_integer():
            print(math.sqrt(number) ** 3)

