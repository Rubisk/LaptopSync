import copy

rows = [
    "AEHINRTTWXXZ",
    "ADEINSTTVXXX",
    "DEJMNORTTXXX",
    "EEIKMRSSTWXX",
    "DDEOOPRRTXXX",
    "AAEGIKKQRRSZ",
    "CFHLMMQTWXYZ",
    "AACFHQSUVWWY",
    "FIIJLOORTVVX",
    "ACFGHIJLOQXY",
    "ABBFFFLLOPRX",
    "EEIIOQRTUUZZ",
    "GIKNPTUVVVXZ"]

columns = [
    "AFIIIJKLRSXXZ",
    "BDDEIJMNOQRTV",
    "CFHIKMRTXXXXZ",
    "AHHJNOSTUUVXX",
    "AEFFGLOOPTVWW",
    "AEFMMNOPQTXXZ",
    "CDEIILLRTVXXX",
    "ADEGGORSSTUYZ",
    "AEEHLQTWXXYZZ",
    "AAINOQRRRSVXX",
    "FKKOPQRTTVWWY",
    "BCEEFIIRTTUVX"
    ]

solved_rows = [
    "INXHETXZWART",
    "STXVANXDEXTI",
    "JDXXOMTRENTX",
    "KERSTMISXXWE",
    "RDXOPXDEXROT",
    "**KAG*****K*",
    "*MM******Q**",
    "****W*****W*",
    "************",
    "************",
    "*BF********B",
    "****O*****Q*",
    "***N********"
]

found = [
    "INXHETXZWART",
    "STXVANXDEXTI",
    "JDXXOMTRENTX",
    "KERSTMISXXWE",
    "RDXOPXDEXROT",
    "**KAG*****K*",
    "*MM*W****Q**",
    "***UW*****W*",
    "**********V*",
    "*******G****",
    "*BFX******PB",
    "***UO**U**Q*",
    "***NVP*G**KU",
]
col_l = len(columns)
row_l = len(rows)

options = [[set("ABCDEFGHIJKLMNOPQRSTUVWXYZ") for __ in range(len(columns))] for _ in range(len(rows))]


def updated_row_possibilities(i):
    def try_sort_row(i, head=[], remaining=[]):
        while len(head) < col_l and solved_rows[i][len(head)] != "*":
            letter = solved_rows[i][len(head)]
            if letter in remaining:
                head.append(letter)
                remaining.remove(letter)
            else:
                return
        head_length = len(head)
        if head_length == col_l:
            yield head
        for letter in remaining:
            if letter in updated_columns[head_length] and letter in options[i][head_length]:
                new_remaining = copy.copy(remaining)
                new_remaining.remove(letter)
                for row in try_sort_row(i, head + [letter], new_remaining):
                    yield row

    for row in try_sort_row(i, head=[], remaining=list(rows[i])):
        yield row


def updated_column_possibilities(i):
    def try_sort_column(i, head=[], remaining=[]):
        while len(head) < row_l and solved_rows[len(head)][i] != "*":
            letter = solved_rows[len(head)][i]
            if letter in remaining:
                head.append(letter)
                remaining.remove(letter)
            else:
                return
        head_length = len(head)
        if head_length == row_l:
            yield head
        for letter in remaining:
            if letter in updated_rows[head_length] and letter in options[head_length][i]:
                new_remaining = copy.copy(remaining)
                new_remaining.remove(letter)
                for col in try_sort_column(i, head + [letter], new_remaining):
                    yield col

    for row in try_sort_column(i, head=[], remaining=list(columns[i])):
        yield row

row_possibilities = [[] for _ in range(row_l)]
column_possibilities = [[] for _ in range(col_l)]

for dummy in range(20):
    row_possibilities = [[] for _ in range(row_l)]
    column_possibilities = [[] for _ in range(col_l)]

    updated_columns = list(copy.copy(map(list, columns)))
    for (i, column) in enumerate(updated_columns):
        for r in solved_rows:
            if r[i] != "*":
                column.remove(r[i])

    updated_rows = list(copy.copy(map(list, rows)))
    for (i, row) in enumerate(updated_rows):
        for j in range(col_l):
            if solved_rows[i][j] != "*":
                row.remove(solved_rows[i][j])

    # options = [[set() for ___ in range(col_l)] for ____ in range(row_l)]
    for i in range(5, 13):
            row_options = [set() for _____ in range(col_l)]
            count = 0
            for row in updated_row_possibilities(i):
                row_possibilities[i].append(row)
                for (j, r) in enumerate(row):
                    row_options[j].add(r)
            for j in range(col_l):
                options[i][j].intersection_update(row_options[j])

    for i in range(col_l):
        col_options = [set() for _ in range(row_l)]
        for col in updated_column_possibilities(i):
            column_possibilities[i].append(col)
            for (j, r) in enumerate(col):
                col_options[j].add(r)

        for (j, col_opt) in enumerate(col_options):
            options[j][i] = options[j][i].intersection(col_opt)

    # Guess column 2
    if dummy >= 10:
        if len(column_possibilities[1]) >= 2:
            for (i, l) in enumerate(column_possibilities[1][1]):
                options[i][1].intersection_update(set([l]))
        else:
            for (i, l) in enumerate(column_possibilities[0][0]):
                options[i][0].intersection_update(set([l]))

            for (i, l) in enumerate(column_possibilities[2][0]):
                options[i][2].intersection_update(set([l]))

    for (i, row_options) in enumerate(options):
        if i <= 4:
            continue
        row_options = "".join([list(x)[0] if len(x) == 1 else "*" for x in row_options])
        solved_rows[i] = row_options
    for r in solved_rows:
        if dummy >= 10:
            print(r)
    if dummy >= 11:
        for c in column_possibilities:
            print(len(c))


solution_test = [
"INXHETXZWART",
"STXVANXDEXTI",
"JDXXOMTRENTX",
"KERSTMISXXWE",
"RDXOPXDEXROT",
"AIKAGQRSZRKE",
"XMMTWZLYHQFC",
"FQHUWACAYSWV",
"IOIJFXVTLOVI",
"IJCHLOIGQAYF",
"LBFXFFLOARPB",
"ZRTUOEEUZIQR",
"XVZNVPXGTVKU"
]

for (index, row) in enumerate(solution_test):
    if not rows[index] == "".join(sorted(row)): print(index, row, sorted(row))

for j in range(col_l):
    column = [r[j] for r in solution_test]
    print("".join(sorted(column)) == columns[j])