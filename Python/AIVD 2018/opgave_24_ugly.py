from fractions import gcd
import os
import copy
from collections import Counter
import string
import itertools as it

# source = """ er e wutemsr  9es srjeknita niebdngrr DmTieie ollcTioretDlek wlrrnomookuewsednne1o lrVGSle0m jn iulndTwadn oahsediao  8mddoooa   eds.ccrst uorooplao uusncvieoar .snzi0uilaIi   e.edtcei sanïa e kdcveaena k2n  tCstenkdj se tne  eidoaj nneg   aj5mniaAfTaol  a oncvclinlleius e qtgdhunp z eaeieiakdarelg we te."""
source = open("opgave_24.txt").read().replace("\n", "")

source = source.replace("\n", "")
size = 4
leftovers = len(source) % size


words = set()

for file in os.listdir("Dutch"):
    print("Reading Dictionaries: {0}.".format(file))
    for line in open("Dutch\\" + file).readlines():
        word = line[:-1]
        words.add(word)


for file in os.listdir("English"):
    print("Reading Dictionaries: {0}.".format(file))
    for line in open("Dutch\\" + file).readlines():
        word = line[:-1]
        words.add(word)



def fit_together(col1, col2):
    if not len(col1) >= len(col2):
        return False
    for c, d in zip(col1, col2):
        if c == " " and d == " ":
            return False
        # if c == "," and d != " ":
        #     return False
        # if d in string.ascii_uppercase and c != " ":
        #     continue
    return True


def find_valid_column_orders(long_columns, short_columns, head=[]):
    all_columns = long_columns + short_columns
    size = len(all_columns)
    allowed_orders = [[fit_together(coli, colj) for colj in all_columns] for coli in all_columns]

    column_order = [0]

    def _find_column_orders(leftover, head=[], wildcard=False):
        if len(leftover) == 0:
            yield head
            return
        for (j, col_index) in enumerate(leftover):
            if allowed_orders[head[-1]][col_index] or wildcard:
                new_wildcard = allowed_orders[head[-1]][col_index] and wildcard
                for order in _find_column_orders(leftover[:j] + leftover[j + 1:], head + [col_index], wildcard=new_wildcard):
                    yield order

    for first_column in range(size):
        for order in _find_column_orders([x for x in range(size) if x != first_column], head=[first_column], wildcard=False):
            columns = [all_columns[i] for i in order]
            yield columns


def score_text(text):
    score = 0
    word_sections = "".join([x for x in text.upper() if x in string.ascii_uppercase + " "]).split(" ")[1:-1]
    for word in word_sections:
        if word in words:
            score += len(word)
    return score

count = 0
for x in it.combinations(range(size), leftovers):
    count += 1
    print(count)
    special_increments = [1 if i in x else 0 for i in range(size)]
    block_size = int(len(source) / size)

    sizes = [block_size + x for x in special_increments]
    partial_sums = [sum(sizes[:i + 1]) for i in range(len(sizes))]

    new_source = copy.copy(source)

    columns = []


    for s in sizes:
        columns.append(new_source[:s])

    prev_i = -1
    for (i, j, k) in it.product(range(67), repeat=3):
        if prev_i != i:
            prev_i = i
            print(i)
        columns[1] = columns[1][i:] + columns[1][:i]
        columns[2] = columns[2][j:] + columns[2][:j]
        columns[3] = columns[3][k:] + columns[3][:k]
    
        result = ""
        for i in range(block_size):
            for col in columns:
                result += col[i]

        for col in columns:
            if len(col) == block_size + 1:
                result += col[block_size]

        if score_text(result) > 50:
            print(result)