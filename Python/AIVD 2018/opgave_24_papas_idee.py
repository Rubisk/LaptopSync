from fractions import gcd
import os
import copy
from collections import Counter
import string
import itertools as it


source = """Beehbkea eted  lebare  n  E
dgpdv lnia eontkvrlsd etZau
EeaeZlmoaeih. d tjentvlstod
dseioherlrertn  dena kannar
nnl d avdaiera o n  lteen r
g ie|nernrnd   ,avle eitfr 
p  tor semaanropaoeenv hor 
  dnuaennr Eadjeevnld dvaBo
no,  aoSke vknoaee ndtok  e
ntmjriwednvndai  aedbr B  """

source = source.replace("\n", "")

for x in range(269):
    for z in range(269):
        y = 268 - (x * 66 + z * 66 ** 2) % 269

        result = ["" for _ in range(269)]
        good = True
        for a in range(269):
            if result[(z * a ** 2 + x * a + y) % 269] != "":
                good = False
                break
            result[(z * a ** 2 + x * a + y) % 269] = source[a]
        if not good:
            continue
        assert result[-1] == "."
        if result[0] in string.ascii_uppercase:
            print("".join(result))