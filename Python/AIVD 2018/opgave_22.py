import itertools as it
from sympy import isprime
import numpy as np

def int_to_roman(input):
    """ Convert an integer to a Roman numeral. """

    if not isinstance(input, type(1)):
        raise TypeError("expected integer, got %s" % type(input))
    if not 0 < input < 4000:
        raise ValueError("Argument must be between 1 and 3999")
    ints = (1000, 900,  500, 400, 100,  90, 50,  40, 10,  9,   5,  4,   1)
    nums = ('M',  'CM', 'D', 'CD','C', 'XC','L','XL','X','IX','V','IV','I')
    result = []
    for i in range(len(ints)):
        count = int(input / ints[i])
        result.append(nums[i] * count)
        input -= ints[i] * count
    return ''.join(result)


def roman_to_int(input):
    """ Convert a Roman numeral to an integer. """
    if not isinstance(input, type("")):
        raise TypeError("expected string, got %s" % type(input))
    input = input.upper(  )
    nums = {'M':1000, 'D':500, 'C':100, 'L':50, 'X':10, 'V':5, 'I':1}
    sum = 0
    for i in range(len(input)):
        try:
            value = nums[input[i]]
            # If the next place holds a larger number, this value is negative
            if i+1 < len(input) and nums[input[i+1]] > value:
                sum -= value
            else: sum += value
        except KeyError:
            raise ValueError('input is not a valid Roman numeral: %s' % input)
    # easiest test for validity...
    if int_to_roman(sum) == input:
        return sum
    else:
        raise ValueError('input is not a valid Roman numeral: %s' % input)
 
grid = [
    ["*", "*", "*", "*", "*"],
    ["*", "*", "*", "*", "*"],
    ["*", "*", "*", "*", "*"],
    ["*", "*", "*", "*", "*"],
    ["V", "I", "C", "I", "I"],
]

characters = "MDCLXVI"

count = 0
def print_grid():
    print("=====")
    for x in grid:
        print("".join(x))
    print("=====")

for (f0, f1, f2, f3) in it.product(list(characters), repeat=4):
    # Write F
    grid[0][0] = f0
    grid[1][0] = f1
    grid[2][0] = f2
    grid[3][0] = f3
    F = "".join([grid[i][0] for i in range(5)])
    try:
        f_val = roman_to_int(F)
        assert f_val % 2 == 1
        # make sure D fits:
        assert f0 == f1
        d_val = f_val
    except:
        continue


    # WRITE D
    for i in range(5):
        grid[1][i] = grid[i][0]

        for (j0, j2, j3) in it.product(list(characters), repeat=3):
            grid[0][4] = j0
            grid[2][4] = j2
            grid[3][4] = j3
            J = "".join([grid[i][4] for i in range(5)])
            try:
                j_val = roman_to_int(J)
            except:
                continue

            quotient = 0
            while quotient * j_val < 4000:
                quotient += 1
                try:
                    E = int_to_roman(quotient * j_val)
                    e_val = quotient * j_val
                    assert len(E) == 5
                    assert E[0] == grid[0][0]
                    assert E[4] == grid[0][4]
                    for i in range(5):
                        grid[0][i] = E[i]
                except:
                    continue
                # print_grid()
                for (c1, c2, c3) in it.product(list(characters), repeat=3):
                    grid[2][1] = c1
                    if c2 in ["I", "V", "X", "L"]:
                        continue
                    grid[2][2] = c2
                    grid[2][3] = c3
                    C = "".join([grid[2][i] for i in range(5)])
                    try:
                        c_val = roman_to_int(C)
                        assert isprime(c_val)
                    except:
                        continue
                    for x in list(characters):
                        grid[3][1] = x
                        G = "".join([grid[i][1] for i in range(5)])
                        try:
                            g_val = roman_to_int(G)
                            assert d_val > g_val
                        except:
                            continue
                        for y in list(characters):
                            grid[3][2] = y
                            H = "".join([grid[i][2] for i in range(5)])
                            try:
                                h_val = roman_to_int(H)
                                b_val = g_val + h_val - e_val
                                assert b_val > 0
                                B = int_to_roman(b_val)
                                assert len(B) == 5
                                assert B[0] == grid[3][0]
                                assert B[1] == grid[3][1]
                                assert B[2] == grid[3][2]
                                assert B[4] == grid[3][4]
                                
                                grid[3][3] = B[3]
                                i_val = roman_to_int("".join([grid[i][3] for i in range(5)]))
                            except:
                                continue
                            print_grid()
                            for row in grid:
                                try:
                                    print(roman_to_int("".join(row)))
                                except:
                                    pass
                            new_grid = np.rot90(grid)
                            for col in new_grid:
                                try:
                                    print(roman_to_int("".join(col)))
                                except:
                                    pass

                            count += 1
