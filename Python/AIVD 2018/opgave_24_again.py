from fractions import gcd
import os
import copy
from collections import Counter
import string
import itertools as it


source = """Beehbkea eted  lebare  n  E
dgpdv lnia eontkvrlsd etZau
EeaeZlmoaeih. d tjentvlstod
dseioherlrertn  dena kannar
nnl d avdaiera o n  lteen r
g ie|nernrnd   ,avle eitfr 
p  tor semaanropaoeenv hor 
  dnuaennr Eadjeevnld dvaBo
no,  aoSke vknoaee ndtok  e
ntmjriwednvndai  aedbr B  """


def transpose_four(source):
    for i in range(4):
        output = []
        new_src = copy.copy(source)
        assert len(new_src) == 269
        offset = int(269 / 4)
        for j in range(4):
            if j == i:
                offset += 1
            output.append(new_src[:offset])
            new_src = new_src[offset:]
            if j == i:
                offset -= 1
        for output_perm in it.permutations(output):
            output_str = ""
            for x in range(offset + 2):
                for row in output_perm:
                    if len(row) > x:
                        output_str += row[x]
            yield output_str


source = source.replace("\n", "")
for text in transpose_four(source):
    for text2 in transpose_four(text): 
        if "  " not in text2:
            print(text2)