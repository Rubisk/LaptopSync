from fractions import gcd
import os
import copy
from collections import Counter
import string
import itertools as it

# source = """ er e wutemsr  9es srjeknita niebdngrr DmTieie ollcTioretDlek wlrrnomookuewsednne1o lrVGSle0m jn iulndTwadn oahsediao  8mddoooa   eds.ccrst uorooplao uusncvieoar .snzi0uilaIi   e.edtcei sanïa e kdcveaena k2n  tCstenkdj se tne  eidoaj nneg   aj5mniaAfTaol  a oncvclinlleius e qtgdhunp z eaeieiakdarelg we te."""
source = open("opgave_24.txt").read().replace("\n", "")

result = ""
i = 1
for _ in range(268):
    result += source[(i - 1) % 269]
    i *= 2
print(result)