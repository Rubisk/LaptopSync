import copy

rows = [
    "AEHINRTTWXXZ",
    "ADEINSTTVXXX",
    "DEJMNORTTXXX",
    "EEIKMRSSTWXX",
    "DDEOOPRRTXXX",
    "AAEGIKKQRRSZ",
    "CFHLMMQTWXYZ",
    "AACFHQSUVWWY",
    "FIIJLOORTVVX",
    "ACFGHIJLOQXY",
    "ABBFFFLLOPRX",
    "EEIIOQRTUUZZ",
    "GIKNPTUVVVXZ"]

columns = [
    "AFIIIJKLRSXXZ",
    "BDDEIJMNOQRTV",
    "CFHIKMRTXXXXZ",
    "AHHJNOSTUUVXX",
    "AEFFGLOOPTVWW",
    "AEFMMNOPQTXXZ",
    "CDEIILLRTVXXX",
    "ADEGGORSSTUYZ",
    "AEEHLQTWXXYZZ",
    "AAINOQRRRSVXX",
    "BCEEFIIRTTUVX"
    "FKKOPQRTTVWWY",
    ]

solved_rows = [
    "INXHETXZWART",
    "STXVANXDEXTI",
    "JDXXOMTRENTX",
    "KERSTMISXXWE",
    "RDXOPXDEXROT",
    "**KAG*****K*",
    "*MM******Q**",
    "****W*****W*",
    "************",
    "************",
    "*BF********B",
    "****O*****Q*",
    "***N********"
]

col_l = len(columns)
row_l = len(rows)
#
# new_rows = []
# for row in rows:
#     new_rows.append(set(list(row)))
#
# new_columns = []
# for column in columns:
#     new_columns.append(set(list(column)))
#
# first_try = [[new_rows[r].intersection(new_columns[c]) for c in range(col_l)] for r in range(row_l)]
# first_try_lengths = [[len(first_try[r][c])  for c in range(col_l)] for r in range(row_l)]
# print(sum(map(sum, first_try_lengths)))

# index row.column
options = [[set("ABCDEFGHIJKLMNOPQRSTUVWXYZ") for __ in range(len(columns))] for _ in range(len(rows))]

# def row_possibilities(i):
#     def try_sort_row(i, head=[], remaining=[]):
#         head_length = len(head)
#         if head_length == col_l:
#             yield head
#         for letter in remaining:
#             if letter in columns[head_length]:
#                 new_remaining = copy.copy(remaining)
#                 new_remaining.remove(letter)
#                 for row in try_sort_row(i, head + [letter], new_remaining):
#                     yield row
#
#     for row in try_sort_row(i, head=[], remaining=list(rows[i])):
#         yield row
#
# def column_possibilities(i):
#     def try_sort_column(i, head=[], remaining=[]):
#         head_length = len(head)
#         if head_length == row_l:
#             yield head
#         for letter in remaining:
#             if letter in rows[head_length]:
#                 new_remaining = copy.copy(remaining)
#                 new_remaining.remove(letter)
#                 for column in try_sort_column(i, head + [letter], new_remaining):
#                     yield column
#
#     for column in try_sort_column(i, head=[], remaining=list(columns[i])):
#         yield column


def updated_row_possibilities(i):
    def try_sort_row(i, head=[], remaining=[]):
        while len(head) < col_l and solved_rows[i][len(head)] != "*":
            letter = solved_rows[i][len(head)]
            if letter in remaining:
                head.append(letter)
                remaining.remove(letter)
            else:
                return
        head_length = len(head)
        if head_length == col_l:
            yield head
        for letter in remaining:
            if letter in updated_columns[head_length] and letter in options[i][head_length]:
                new_remaining = copy.copy(remaining)
                new_remaining.remove(letter)
                for row in try_sort_row(i, head + [letter], new_remaining):
                    yield row

    for row in try_sort_row(i, head=[], remaining=list(rows[i])):
        yield row


def updated_column_possibilities(i):
    def try_sort_column(i, head=[], remaining=[]):
        while len(head) < row_l and solved_rows[len(head)][i] != "*":
            letter = solved_rows[len(head)][i]
            if letter in remaining:
                head.append(letter)
                remaining.remove(letter)
            else:
                return
        head_length = len(head)
        if head_length == row_l:
            yield head
        for letter in remaining:
            if letter in updated_rows[head_length] and letter in options[head_length][i]:
                new_remaining = copy.copy(remaining)
                new_remaining.remove(letter)
                for col in try_sort_column(i, head + [letter], new_remaining):
                    yield col

    for row in try_sort_column(i, head=[], remaining=list(columns[i])):
        yield row

while True:
    updated_columns = list(copy.copy(map(list, columns)))
    for (i, column) in enumerate(updated_columns):
        for r in solved_rows:
            if r[i] != "*":
                column.remove(r[i])

    updated_rows = list(copy.copy(map(list, rows)))
    for (i, row) in enumerate(updated_rows):
        for j in range(col_l):
            if solved_rows[i][j] != "*":
                row.remove(solved_rows[i][j])

    # options = [[set() for ___ in range(col_l)] for ____ in range(row_l)]
    for i in range(5, 13):
            row_options = [set() for _____ in range(col_l)]
            count = 0
            for row in updated_row_possibilities(i):
                count += 1
                for (j, r) in enumerate(row):
                    row_options[j].add(r)
            print(i, count)
            for j in range(col_l):
                options[i][j].intersection_update(row_options[j])
            # row_options = [list(x)[0] if len(x) == 1 else "*" for x in row_options]
            #

    for i in range(col_l):
        print(i)
        col_options = [set() for _ in range(row_l)]
        for col in updated_column_possibilities(i):
            for (j, r) in enumerate(col):
                col_options[j].add(r)

        for (j, col_opt) in enumerate(col_options):
            options[j][i] = options[j][i].intersection(col_opt)

    for (i, row_options) in enumerate(options):
        if i <= 4:
            continue
        row_options = "".join([list(x)[0] if len(x) == 1 else "*" for x in row_options])
        solved_rows[i] = row_options
    for solved in solved_rows:
        print(str(solved))
# options = [[set() for c in range(col_l)] for r in range(row_l)]
# for row in row_possibilities(0):
#     text_row = copy.copy(row)
#     while "X" in text_row:
#         text_row.remove("X")
#     text = "".join(text_row)
#     if "INHETZWART" in text:
#         for (r, x) in enumerate(row):
#             options[0][r].add(x)
# print("FIRST ROW")
# print(options[0])
#
# options = [[set() for c in range(col_l)] for r in range(row_l)]
# for row in row_possibilities(1):
#     text_row = copy.copy(row)
#     while "X" in text_row:
#         text_row.remove("X")
#     text = "".join(text_row)
#     if "STVANDE" in text:
#         for (r, x) in enumerate(row):
#             options[1][r].add(x)
# print("SECOND ROW")
# print(options[1])
#
# options = [[set() for c in range(col_l)] for r in range(row_l)]
# for row in row_possibilities(2):
#     text_row = copy.copy(row)
#     while "X" in text_row:
#         text_row.remove("X")
#     text = "".join(text_row)
#     if "OMTRENT" in text:
#         for (r, x) in enumerate(row):
#             options[2][r].add(x)
# print("THIRD ROW")
# print(options[2])

# options = [[set() for c in range(col_l)] for r in range(row_l)]
# for row in row_possibilities(3):
#     text_row = copy.copy(row)
#     while "X" in text_row:
#         text_row.remove("X")
#     text = "".join(text_row)
#     if "KERSTMIS" in text:
#         for (r, x) in enumerate(row):
#             options[3][r].add(x)
# print("FOURTH ROW")
# print(options[3])
#
#
# options = [[set() for c in range(col_l)] for r in range(row_l)]
# for row in row_possibilities(4):
#     text_row = copy.copy(row)
#     while "X" in text_row:
#         text_row.remove("X")
#     text = "".join(text_row)
#     if "RDOPDEROT" in text:
#         for (r, x) in enumerate(row):
#             options[4][r].add(x)
# print("FIFTH ROW")
# print(options[4])

#
# options = [[set() for c in range(col_l)] for r in range(row_l)]
# for row in row_possibilities(5):
#     text_row = copy.copy(row)
#     while "X" in text_row:
#         text_row.remove("X")
#     text = "".join(text_row)
#     print(text)
#     #if "TER" in text:
#     #     for (r, x) in enumerate(row):
#     #        options[5][r].add(x)
# print("FIFTH ROW")
# print(options[5])