import os
import string
from collections import Counter

frequencies = [7.328, 1.449, 1.251, 6.111, 18.992, 0.708, 3.295, 3.308, 6.285, 1.764, 2.386, 3.644, 2.305, 10.464, 
               5.991, 1.178, 0.008, 5.887, 3.554, 6.002, 1.706, 2.398, 1.814, 0.024, 0.307, 1.842, 0.0, 0.0, 0.0]

key_length = 8
length = 6

words = set()
prefixes = [set() for i in range(40)]
for file in os.listdir("Dutch"):
    print(file)
    for line in open("Dutch\\" + file).readlines():
        words.add(line.replace("\n", ""))
        for i in range(1, len(line)):
            prefixes[i].add(line[:i])


source = "emokrytrtjnidntgvqumvdasxxrjkfuhranjnhrcrgdnofrwnsdynffwlpwsykxxoorqzeuwyy"
head = source[:length]

def is_admissible(string):
    if len(string) == 0:
        return True
    for i in range(3, len(string)): 
        if string[:i] in words:
            if is_admissible(string[i:]):
                return True
    if string in prefixes[len(string)]:
        return True
    return False

starts = set()

def find_key(head="", max_depth=8):
    if max_depth == 0:
        return
    for c in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
        if max_depth > 7:
            print(c)
        new_head = head + c
        if is_admissible(new_head):
            decryption = "".join([chr((ord(x) - ord(y)) % 26 + ord('A')) for (x, y) in zip(source[:len(new_head)], new_head)])
            if is_admissible(decryption):
                    if(max_depth == 1):
                        starts.add(head[:8])
                    find_key(new_head, max_depth-1)

def likelihood(test):
    # lower = better
    result = 0
    for test_f, actual_f in zip(test, frequencies):
        if test_f == 0:
            continue
        if actual_f < 0.01:
            if test_f > 0.05:
                result += 10E6
        # elif actual_f == 0:
        #     mean += 20
        result += (test_f - actual_f) ** 2
    return result


def suggest_caesar_chars(msg):
    result = []
    counts = Counter(msg)
    frequencies = [counts[i] for i in string.ascii_lowercase]
    for (i, keychar) in enumerate(string.ascii_lowercase):
        frequencies = frequencies[26 - i:] + frequencies[:26 - i]
        result.append((likelihood(frequencies), keychar))
    print(list(map(lambda x: int(x[0]), sorted(result))))
    print("".join(map(lambda x: x[1], sorted(result))))

for i in range(8):
    suggest_caesar_chars(source[i::key_length])

find_key(head="UITGE", max_depth = 6)
print(starts)