from problem_3b import Game
import pytest
import sys


def simulate(john, sarah, simulation_size=10000):
    """
    Runs a simulation and determines the mean game length, variance and sarah's win percentage.
    :param john: Number of coins John starts with.
    :param sarah: Number of coins Sarah stars with.
    :param simulation_size: Number of times to run the simulation.
    :return type: (float, float, float, float, float)
    :return: (mean game length, game length variance, sarah win percentage, average games won by sarah/john)
    """
    game_test = Game(john, sarah)

    game_lengths = []
    sarah_wins = []
    john_win_lengths = []
    sarah_win_lengths = []

    for _ in range(simulation_size):
        game_test.play_game()
        game_lengths.append(game_test.tosses)
        sarah_wins.append(game_test.winner == Game.SARAH)  # Python is awesome and can sum booleans

        if game_test.winner == Game.SARAH:
            sarah_win_lengths.append(game_test.tosses)
        else:
            john_win_lengths.append(game_test.tosses)

        game_test.reset()

    mean = sum(game_lengths) / simulation_size
    variance = sum([(l - mean) ** 2 for l in game_lengths]) / simulation_size
    sarah_percentage = sum(sarah_wins) / simulation_size
    average_sarah_length = sum(sarah_win_lengths) / len(sarah_win_lengths)
    average_john_length = sum(john_win_lengths) / len(john_win_lengths)

    return mean, variance, sarah_percentage, average_sarah_length, average_john_length


def compare_simulation(john, sarah, simulation_size=10000):
    """
    Simulates a game and compares it to mathematical expectations.
    After that, it outputs it's findings immediately into stdout.
    :param john: Number of coins John starts with.
    :param sarah: Number of coins Sarah stars with.
    :param simulation_size: Number of times to run the simulation.
    """
    mean, variance, sarah_percentage, asl, ajl = simulate(john, sarah, simulation_size)
    expected_mean = john * sarah
    expected_variance = john * sarah * (john ** 2 + sarah ** 2 - 2) / 3
    expected_sarah_percentage = sarah / (john + sarah)
    print("Average simulated game length is {0}, expected {1}.".format(mean, expected_mean))
    print("Average simulated game length variance is {0}, expected {1}.".format(variance, expected_variance))
    print("Average simulated percentage of games won by Sarah is {0}, expected {1}.".format(sarah_percentage,
                                                                                            expected_sarah_percentage))
    print("Average length of games won by Sarah is {0}.".format(asl))
    print("Average length of games won by John is {0}.".format(ajl))


if __name__ == "__main__":  # package protection
    try:
        user_input = [int(sys.argv[i]) for i in (1, 2, 3)]
    except (ValueError, IndexError):
        print("Invalid argument. Invoke like:")
        print(">>  python <john> <sarah> <simulation size>")
        print("To simulate a game.")
        print("Now running with default arguments (5, 10, 10000).")
        user_input = (5, 10, 10000)
    compare_simulation(*user_input)
