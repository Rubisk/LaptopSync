import sys
import pytest
import numpy as np
from problem_2b import ComputationException, can_partition, find_biggest_without_solution


if __name__ == "__main__":  # allow this to be used as a library too
    # For some reason we have to hard code our input.
    # See problem_2b.py for variable input and test cases.
    answer = find_biggest_without_solution((6, 9, 20))
    print("Largest number of McNuggets that cannot be bought in exact quantity: %d" % answer)
