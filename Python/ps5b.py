import sys
import pytest
import itertools as it
from problem_5a import count_substring_match_recursive


def substring_match_exact_gen(target, key):
    """
    Search for all the positions of key in target.
    Generator implementation (for efficiency in possible implementations).
    :type target: str
    :param target: String to search through.
    :type key: str
    :param key: String to find.
    :return type: int
    :return: Number of times key appears in target.
    """
    for i in range(len(target) - len(key) + 1):
        if target[i:len(key) + i] == key:
            yield i


def substring_match_exact(target, key):
    return tuple(substring_match_exact_gen(target, key))


def test_substring_match_exact():
    targets = ("atgacatgcacaagtatgcat", "atgaatgcatggatgtaaatgcag")
    keys = ("a", "atg", "atgc", "atgca")
    for target, key in it.product(targets, keys):
        for index in substring_match_exact_gen(target, key):
            assert target[index:index + len(key)] == key
        assert len(substring_match_exact(target, key)) == count_substring_match_recursive(target, key)


if __name__ == "__main__":  # package protection
    if "-test" in sys.argv:
        pytest.main(__file__)
    else:
            try:
                positions = substring_match_exact(*sys.argv[1:])
                print("String \"{2}\" appears at indices {1} in \"{0}\".".format(sys.argv[1],
                                                                                 positions,
                                                                                 sys.argv[2]))
            except (IndexError, TypeError):
                print("Invalid argument. Invoke like:")
                print(">>  python <target> <key> {-test}")
