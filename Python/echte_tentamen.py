# opgave 1
print("Opgave 1:")


s = 0; k = 1; M = 3
while k <= M:
    s += 1 / k
    k += 1
print(s)

# opgave 2
print("Opgave 2:")


def a(x, t, dt=0.000001):
    return (x(t + dt) - 2 * x(t) + x(t - dt)) / (dt ** 2)


def x(t):
    return 10 - 9.8*t**2


print(a(x, 3, 0.01))

# opgave 3
print("Opgave 3:")


def Opgave3(a, b, c, d, e, f):
    # yay lineaire algebra. De vergelijking is:
    # (a, b)(x)   (-c)
    # (d, e)(y) = (-f)
    c = -c
    f = -f
    # de vraag is dus of deze inverteerbaar is. Dat is zo als det != 0
    det = a*e - b*d
    # als dit zo is, is er precies een oplossing.
    # als de determinant nul is, dan zijn er oneindig veel snijpunten als
    # (a, b, c) een veelvoud van (d, e, f) is, en anders geen.
    if det == 0 and a / d == b / e == c / f:
        return "Infinitely many intersection points, the 2 lines are equal."
    elif det == 0:
        return "Er is geen snijpunt"
    # de inverse zal nu zijn:
    # (e, -b)
    # (-d, a) * 1 / det.
    # Dus Ax = b ==> x = A^-1b
    x = (e * c + -b * f) / det
    y = (-d * c + a * f) / det
    return "Er is een snijpunt (x, y) met x = {} en y = {}".format(x, y)


print(Opgave3(1, 2, 3, 4, 5, 6))
print(Opgave3(1, 1, 1, 1, 1, -1))


# opgave 4
print("Opgave 4:")


def Opgave4(lijst):
    total = 0
    oneven = []
    for l in lijst:
        if l % 2 == 1:
            oneven.append(l)
        else:
            total += l
    print("De lijst van de oneven getallen in de lijst = {}".format(oneven))
    print("De som van de even getallen in de lijst = {}".format(total))


def fibonnacci(n):
    # jaja heel traag maar ken de formule niet :/
    a0, a1 = 1, 1
    for _ in range(n - 1):
        a0, a1 = a1, a0 + a1
    return a0

L = [2, 3, 12, 13, 1, 4, 5, 100, 120, 7]
Opgave4(L)

fib = [fibonnacci(n) for n in range(1000)]
Opgave4(fib)
