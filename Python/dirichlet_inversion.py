import math

g_array = ["" for _ in range(1000)]

def f(n):
	if n % 2 == 0:
		return 0
	return (-1) ** int((n - 1) / 2)

def g(n):
	if g_array[n - 1] != "":
		return g_array[n - 1]
	if n == 1:
		return int(1 / f(1))
	s = 0
	for d in range(1, n):
		if n % d == 0:
			s += f(int(n / d)) * g(d)
	s *= -1 / f(1)
	return int(s)

for i in range(len(g_array)):
	g_array[i] = g(i + 1)	

t = 0

for (x, y) in enumerate(g_array):
	print(x + 1, y)
	t += y / (x + 1)

print(t, 4 / math.pi)