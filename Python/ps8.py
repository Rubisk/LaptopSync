"""
Quick start guide:
Invoke like
python ps8.py <question>
Where <question> is in (a, b, c, d, e).

Simpson is clearly the most efficient,
Trapezoidal is second by a very small margin (its mostly more consistent.
Riemann middle is right behind Trapezoidal.
Riemann left/right is last.
"""
import os

import math
import numpy as np

import sys


# -- IO STUFF

def wsc():
    """
    alt: WaitScreenClear
    Waits for the user to press enter then clears the screen
    Should work for mac/linux but has not been tested :/
    :return:
    """
    print("Press any key to continue.")
    sys.stdin.read(1)  # wait for one keypress
    os.system("cls" if os.name == "nt" else "clear")


def pretty_ten_float(f):
    """
    Formats a float to a string with exactly (length)
    chars and some spaces.
    Basically ugly code that makes for pretty float printing.
    :param f: Float to format
    """
    if f >= 10**(-4):
        return "{:1.4f}".format(f)
    i = 1
    while f * i ** 10 < 1:
        i += 1
    return "1.0e-{0}".format(i + 1)


# -- Integration functions

def riemann_summation(f, a, b, pieces=1000, choice="right"):
    """
    Integrate a function using Riemann summation.
    :param f: Function to integrate
    :param a: Lower bound of the interval
    :param b: Upper bound of the interval
    :param pieces: Number of pieces to partition the interval in
    :param choice: one of "left", "right" or "middle"
    """
    assert choice in ("left", "right", "middle")
    extra = 0 if choice == "left" else 1 if choice == "right" else 1 / 2
    dx = (b - a) / pieces
    return sum([f(a + (i + extra) * dx) * dx for i in range(pieces)])


def trapezoidal_rule_summation(f, a, b, pieces=1000):
    """
    Integrate a function using the Trapezoidal rule.
    :param f: Function to integrate
    :param a: Lower bound of the interval
    :param b: Upper bound of the interval
    :param pieces: Number of pieces to partition the interval in
    """
    dx = (b - a) / pieces
    return dx * ((f(a) + f(b)) / 2 + sum([f(a + i * dx) for i in range(1, pieces)]))


def simpson_summation(f, a, b, pieces=1000):
    """
    Integrate a function using the Trapezoidal rule.
    :param f: Function to integrate
    :param a: Lower bound of the interval
    :param b: Upper bound of the interval
    :param pieces: Number of pieces to partition the interval in
    """
    dx = (b - a) / pieces
    return dx * (1 / 6 * (f(a) + f(b)) + 2 / 3 * (f(a + 1 / 2 * dx)) +
                 sum([1 / 3 * f(a + i * dx) + 2 / 3 * f(a + (i + 1 / 2) * dx)
                      for i in range(1, pieces)]))


def monte_carlo_integration(f, a, b, max_f=-1, pieces=1000000):
    """
    Integrates a function using the monte carlo method.
    :param f: Function to integrate.
    :param a: Lower bound of the interval
    :param b: Upper bound of the interval
    :param max_f: (optional) maximum value f can assume in the interval.
    If not given, it will approximate this automatically.
    :param pieces: (optional) sample size
    """
    if max_f == -1:
        max_f = max([f(dx * (b - a) / 1000 + a) for dx in range(1000)])
    min_f = 0
    x, y = np.random.uniform(a, b, pieces), np.random.uniform(min_f, max_f, pieces)
    correct_points = y[y < f(x)].size
    return correct_points / pieces * (max_f - min_f) * (b - a)


def probabilistic_integration(f, a, b, pieces=1000000):
    """
    Integrates a function using the probabilistic method.
    :param f: Function to integrate.
    :param a: Lower bound of the interval
    :param b: Upper bound of the interval
    :param pieces: (optional) sample size
    """
    x = np.random.uniform(a, b, pieces)
    return np.average(f(x)) * (b - a)


# -- Precision finding functions

def find_precision(sum_method, required_precision, expected_answer, *args, **kwargs):
    """
    Approximates the precision of an integration method.
    Uses binary search to find the "first" time it's calculating with required precision.
    Assumes the integrate converges to the expected_answer when the interval is partitioned evenly
    into smaller and smaller pieces.
    :param sum_method: Integration method to check.
    :param required_precision: Wanted precision for the answer
    :param expected_answer: Expected value of the integral.
    :param args: Additional arguments for the integration function.
    """
    pieces_needed = 1
    if "pieces" in kwargs:
        kwargs.pop("pieces")
    while abs(sum_method(*args, pieces=pieces_needed, **kwargs) - expected_answer) >= required_precision:
        pieces_needed *= 2
    upper_bound = pieces_needed
    lower_bound = pieces_needed / 2  # we multiplied by 2, so we know it went wrong here.
    while upper_bound - lower_bound > 1:  # binary search
        if abs(sum_method(*args, pieces=pieces_needed, **kwargs) - expected_answer) < required_precision:
            upper_bound = (upper_bound + lower_bound) / 2
        else:
            lower_bound = (upper_bound + lower_bound) / 2
    return int(upper_bound)


def find_precision_random(sum_method, required_precision, expected_answer, *args, iterations=100):
    """
    Approximates the precision of an integration method.
    Because it's random, binary search isn't right for this, as it will not converge evenly.
    Instead, we just keep calculating the integral with one extra sample point, until we
    acquire the required precision. We repeat this (iterations) times, and then ditch
    the lower and upper quarter (to prevent lucky/unlucky streaks from messing stuff up).
    The average of the middle values should be close enough.
    :param sum_method: Integration method to check.
    :param required_precision: Wanted precision for the answer
    :param expected_answer: Expected value of the integral.
    :param args: Additional arguments for the integration function.
    :param iterations: (optional) Number of times to approximate precision.
    """
    values = []
    for n in range(iterations):
        pieces_needed = 10
        diff = required_precision + 1
        while diff >= required_precision:
            diff = abs(sum_method(*args, pieces=pieces_needed) - expected_answer)
            pieces_needed += 1
        values.append(pieces_needed)
    values = sorted(values)[int(len(values) / 4):int(3 * len(values) / 4)]  # sort and slice
    return np.average(values)


if __name__ == "__main__":   # package protection
    # As I learned in problem set (6), providing code to calculate the answers isn't enough.
    # So here's some code that neatly prints out the answer to every question.
    # It's just formatting and printing stuff. Nothing fancy going on here.
    # I would have made a command interface but there's no easy way to let users input functions.
    # Code won't be documented further as it's just printing stuff.

    # Oh, and yes, I've thought about implementing these tables as a function,
    # but because every table needs something acting slightly different
    # all that would do is translate values into prints, which given all the different print
    # statements wouldn't clean this up. I guess I'll have to live with it being kinda messy.
    # The main issue is that all functions take slightly different arguments, such as different interval sizes,
    # different functions, different special stuff (left/right/middle), that having one "format"
    # function would be a big split of cases that wouldn't make stuff simpler.
    try:
        problem = sys.argv[1]
        assert problem in ("a", "b", "c", "d", "e")
        os.system('cls' if os.name == "nt" else "clear")
        # Note that there is no output for A as no questions were asked.
        # Running with a will call the test code instead.

        if problem == "a":
            print("Here follows the answer to problem (a):\n")
            for method, method_name in ((riemann_summation, "Riemann"),
                                        (trapezoidal_rule_summation, "Trapezoidal"),
                                        (simpson_summation, "Simpson")):
                if method == riemann_summation:
                    for direction in "left", "middle", "right":
                        answer = pretty_ten_float(abs(method(lambda x: 4 / (x ** 2 + 1),
                                                             0, 1, choice=direction) - math.pi))
                        print("Approximating the integral of 4 / (x^2 + 1) using {0} ({1}), "
                              "with 1000 slices has a deviation of {2}.".format(method_name,
                                                                                direction,
                                                                                answer))
                        answer = pretty_ten_float(abs(method(np.sin, 0, math.pi, choice=direction) - 2))
                        print("Approximating the integral of sin(x) using {0} ({1}), "
                              "with 1000 slices has a deviation of {2}.".format(method_name,
                                                                                direction,
                                                                                answer))
                else:
                    answer = pretty_ten_float(abs(method(lambda x: 4 / (x ** 2 + 1), 0, 1) - math.pi))
                    print("Approximating the integral of 4 / (x^2 + 1) using {0} "
                          "with 1000 slices has a deviation of {1}.".format(method_name, answer))
                    answer = pretty_ten_float(abs(method(np.sin, 0, math.pi) - 2))
                    print("Approximating the integral of sin(x) using {0} "
                          "with 1000 slices has a deviation of {1}.".format(method_name, answer))
            wsc()

        elif problem == "b":
            # Only testing for Riemann left as they're really similar, and adding keyword stuff in here
            # is just a lot of code and output spam for the same numbers.
            print("Here follows the answer to problem (b.1):\n")
            print("To approximate the integral of 4 / (x^2 + 1) with a precision of P "
                  "you need N iterations.")
            for method, method_name in ((riemann_summation, "Riemann"),
                                        (trapezoidal_rule_summation, "Trapezoidal"),
                                        (simpson_summation, "Simpson")):
                if method == riemann_summation:
                    for direction in "left", "middle", "right":
                        print("\n{0} ({1}):\n     P | N".format(method_name, direction))
                        for j in range(1, 6):
                            answer = find_precision(method, 10 ** -j, math.pi,
                                                    lambda x: 4 / (x ** 2 + 1), 0, 1, choice=direction)
                            print("{0} | {1}".format(pretty_ten_float(10 ** -j), answer))
                else:
                    print("\n{0}:\n     P | N".format(method_name))
                    for j in range(1, 6):
                        answer = find_precision(method, 10 ** -j, math.pi, lambda x: 4 / (x ** 2 + 1), 0, 1)
                        print("{0} | {1}".format(pretty_ten_float(10 ** -j), answer))
            wsc()

            print("Here follows the answer to problem (b.2):\n")
            print("To approximate the integral of sin(x) with a precision of P "
                  "you need N iterations.")
            for method, method_name in ((riemann_summation, "Riemann"),
                                        (trapezoidal_rule_summation, "Trapezoidal"),
                                        (simpson_summation, "Simpson")):
                if method == riemann_summation:
                    for direction in "left", "middle", "right":
                        print("\n{0} ({1}):\n     P | N".format(method_name, direction))
                        for j in range(1, 8):
                            answer = find_precision(method, 10 ** -j, 2, math.sin, 0, math.pi, choice=direction)
                            print("{0} | {1}".format(pretty_ten_float(10 ** -j), answer))
                else:
                    print("\n{0}:\n     P | N".format(method_name))
                    for j in range(1, 8):
                        answer = find_precision(method, 10 ** -j, 2, math.sin, 0, math.pi)
                        print("{0} | {1}".format(pretty_ten_float(10 ** -j), answer))
            wsc()

        elif problem == "c":
            print("Here follow the answers to problem (c):\n")
            answer = pretty_ten_float(abs(monte_carlo_integration(lambda x: 4 / (x ** 2 + 1), 0, 1) - math.pi))
            print("Approximating the integral of 4 / (x^2 + 1) using Monte Carlo "
                  "with 1000000 points has a deviation of {0}.".format(answer))
            answer = pretty_ten_float(abs(monte_carlo_integration(np.sin, 0, math.pi) - 2))
            print("Approximating the integral of sin(x) using Monte Carlo "
                  "with 1000000 points has a deviation of {0}.".format(answer))
            wsc()

        elif problem == "d":
            print("Here follows the answer to problem (d):\n")
            print("To approximate the integral of 4 / (x^2 + 1) with a precision of P "
                  "you need N points.")
            print("\nMonte Carlo:\n     P | N")
            for j in range(1, 5):
                answer = find_precision_random(monte_carlo_integration, 10 ** -j, math.pi,
                                               lambda x: 4 / (x ** 2 + 1), 0, 1, 4)
                print("{0} | {1}".format(pretty_ten_float(10 ** -j), answer))
            wsc()

        elif problem == "e":
            print("Here follows the answer to problem (e):\n")
            print("To approximate the integral of 4 / (x^2 + 1) with a precision of P "
                  "you need N points.")
            print("\nProbabilistic:\n     P | N")
            for j in range(1, 5):
                answer = find_precision_random(probabilistic_integration, 10 ** -j, math.pi,
                                               lambda x: 4 / (x ** 2 + 1), 0, 1)
                print("{0} | {1}".format(pretty_ten_float(10 ** -j), answer))
            wsc()

    except (IndexError, AssertionError) as e:
        print("Invalid argument. Invoke like:")
        print(">> python <p>")
        print("Where p in (a, b, c, d, e)")
        print("to get the answer to your question.")
