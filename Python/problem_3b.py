"""
Implementation of a little game "engine".
Games are defined by the number of coins john and sarah start with.
"""

import math
import random
import copy

# Apparently we don't need to write test cases, check 3c for a UI interface and test cases.


class Game(object):
    JOHN = 0
    SARAH = 1

    def __init__(self, john, sarah):
        """
        :param john: Number of coins john starts with every game.
        :param sarah: Number of coins sarah starts with every game.
        """
        self._state = [john, sarah]
        self._begin_state = [john, sarah]
        self._tosses = 0

    def reset(self):
        """
        Resets the game to the starting state.
        """
        for i in (0, 1):
            self._state[i] = self._begin_state[i]
        self._tosses = 0
        
    def play_game(self):
        """
        Simulates a full game.
        Note that if reset() isn't called after playing a game, this function consideres the game as
        ended and doesn't do anything.
        """
        while min(self._state) > 0:
            self._tosses += 1
            self._flip_coin()

    # properties for john/sarah coin counts.

    @property
    def john(self):
        return self._state[Game.JOHN]

    @john.setter
    def john(self, val):
        self._state[Game.JOHN] = val

    @property
    def sarah(self):
        return self._state[Game.SARAH]

    @sarah.setter
    def sarah(self, val):
        self._state[Game.SARAH] = val

    @property
    def tosses(self):
        """
        Retrieve the number of coins thrown.
        :return type: int
        :return: number of coins thrown.
        """
        return self._tosses

    @property
    def winner(self):
        return int(self.john == 0)

    @property
    def winner_string(self):
        """
        Find out who won.
        :return type: str
        :return: Name of the winner.
        """
        assert 0 in self._state
        return "Sarah" if self.john == 0 else "John"

    # private
    def _flip_coin(self):
        winner = random.randint(0, 1)
        loser = 1 - winner
        self._state[winner] += 1
        self._state[loser] -= 1
