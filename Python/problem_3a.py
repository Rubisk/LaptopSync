import math
import random


def flip_coin(coins):
    """
    Flip a coin once.
    :type coins: tuple (int)
    :param coins: (John, Sarah), number of coins they have before the flip.
    :return: (John, Sarah), number of coins they have after the flip.
    """
    winner = random.randint(0, 1)
    loser = 0 if winner == 1 else 1
    coins[winner] += 1
    coins[loser] -= 1
    return coins

if __name__ == "__main__":  # allow this to be used as a library too
    state = flip_coin([5, 10])
    print("Sarah has {0} coins and John has {1} coins after 1 flip.".format(*state))
