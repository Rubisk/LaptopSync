import sys
import math

max_n = int(sys.argv[-1])

min_diff = 0
max_diff = 0

for n in range(1, max_n+ 1):
	if (n % 2 == 0):
		continue
	s = "{0:b}".format(n ** 2)
	while len(s) < int(math.log(max_n ** 2) / math.log(2) + 1):
		s = "0" + s
	diff = - sum([int(i) for i in "{0:b}".format(n)]) + sum([int(i) for i in s])
	if (diff < 0):
		print(n)
		print("{0:b}".format(n))
		print(s)

	min_diff = min(diff, min_diff)
	max_diff = max(diff, max_diff)
	# print(s)

print(min_diff, max_diff)