"""
Finding the nth prime number.
The algorithm uses the fact that, with the exception of 2 and 3,
every prime is congruent with 1 or 5 mod 6.
"""
import math
import pytest
import sys


def is_prime(p):
    """
    Evaluates whether a given number is prime or not.
    Note that we only need to check all numbers up to the
    root of p, since any divisor d has a complementary divisor
    p / d.
    Also, we need to only check if it's divisible by 2, 3, or any number congruent 1 or 5 mod 6.
    :type p: int
    :param p: Number to check for primality.
    :return: True if p is prime, False otherwise.
    """
    if p == 1:
        # 1 is not prime
        return False
    elif p in (2, 3):
        # 2 and 3 are prime
        return True
    elif p % 6 not in (1, 5):
        # if 2 or 3 divides p it's obviously not prime
        return False
    i = 6
    # go over all numbers 1 and 5 mod 6 under sqrt(p)
    while i <= math.sqrt(p) + 1:
        if p % (i - 1) == 0 or p % (i + 1) == 0:
            return False
        i += 6
    # no number divides p, hence p is prime
    return True


def find_prime(n):
    """
    Finds the nth prime.
    :type n: int
    :param n: Index of the prime to find (n > 1).
    :return: The nth prime number.
    """
    assert n >= 1
    assert type(n) == int
    if n in (1, 2):
        return 2 if n == 1 else 3
    prime_counter = 2  # we already have 2 and 3
    six_tuple = 6
    while prime_counter < n:  # Since we're returning in our loop, we don't need a stop condition.
        for mod in (-1, 1):
            if is_prime(six_tuple + mod):
                prime_counter += 1
                if n == prime_counter:
                    return six_tuple + mod
        six_tuple += 6


def test_is_prime():
    # test code for is_prime
    # loop implementation instead of map for improved pytest output on failure
    primes = [2, 3, 5, 11, 19, 31, 199]
    composites = (1, 6, 25, 123456789, 49, 27, 35)
    for p in primes:
        assert is_prime(p)
    for c in composites:
        assert not is_prime(c)


def test_find_prime():
    # test code for find_prime
    assert find_prime(1) == 2
    assert find_prime(2) == 3
    assert find_prime(3) == 5
    assert find_prime(4) == 7
    assert find_prime(10) == 29
    assert find_prime(10000) == 104729


if __name__ == "__main__":  # allow this to be used as a library too
    if "-test" in sys.argv:
        pytest.main(__file__)
    try:
        user_input = int(sys.argv[1])
    except (ValueError, IndexError):
        print("Invalid argument. Invoke like:")
        print("    python <n> {-test}")
        print("To find the nth prime.")
        print("Now calculating for default value 1000.")
        user_input = 1000
    print("The {0!s}th prime equals {1!s}.".format(user_input, find_prime(user_input)))
