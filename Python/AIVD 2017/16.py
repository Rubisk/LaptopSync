from copy import deepcopy

class Board(object):
	def __init__(self):
		self.stones = [4 for _ in range(14)]
		self.stones[6] = 0
		self.stones[13] = 0
		self.player_turn = 0

	def all_moves(self):
		player_side = list(range(self.player_turn * 7, self.player_turn * 7 + 6))
		for start in player_side:
			new_board = deepcopy(self)

			hand = new_board.stones[start]
			new_board.stones[start] = 0
			i = start
			while hand > 0:
				i += 1
				i %= 14
				if (new_board.player_turn == 0 and i == 13) or (new_board.player_turn == 1 and i == 6):
					i += 1
					i %= 14
				new_board.stones[i] += 1
				hand -= 1

			if new_board.stones[i] == 1 and i not in (6, 13):
				grab = new_board.stones[i] + new_board.stones[12 - i]
				new_board.stones[i] = 0
				new_board.stones[12 - i] = 0
				if new_board.player_turn == 0:
					new_board.stones[6] += grab
				else:
					new_board.stones[13] += grab

			if not ((new_board.player_turn == 0 and i == 6)  or (new_board.player_turn == 1 and i == 13)):
				new_board.player_turn = 1 - new_board.player_turn

			yield new_board


	def __repr__(self):
		board = self.stones[0:6] + self.stones[7:13]
		scores = [self.stones[6], self.stones[12]]
		return " ".join(map(str, self.stones))


def iterate_board(board, turn):
	if turn == 5:
		if board.stones[0:6] != [6, 6, 0, 0, 0, 7] or board.stones[7:13] != [6, 6, 5, 0, 0, 1]:
			return
	if turn == 9:
		if board.stones[0:6] != [0, 0, 2, 2, 2, 1] or board.stones[7:13] != [1, 9, 7, 2, 2, 0]:
			return
		else:
			print("HI!")
	if turn == 11:
		if board.stones[0:6] != [0, 0, 2, 2, 2, 1] or board.stones[7:13] != [1, 9, 7, 2, 0, 0]:
			return
	if turn == 14:
		if board.stones[0:6] != [1, 1, 3, 2, 0, 0] or board.stones[7:13] != [2, 9, 0, 3, 1, 1]:
			return
	if turn == 18:
		if board.stones[0:6] != [0, 1, 0, 1, 1, 0] or board.stones[7:13] != [0, 10, 0, 3, 0, 0]:
			return
	if turn == 19:
		if board.stones[0:6] != [0, 1, 0, 1, 0, 0] or board.stones[7:13] != [0, 10, 0, 3, 0, 0]:
			return
		else:
			print("HI!", board)
	if turn == 26:
		if board.stones[0:6] != [0, 2, 0, 0, 0, 0] or board.stones[7:13] != [0, 0, 0, 0, 1, 0]:
			return
		else:
			print("HI!", board, board.player_turn)
	if turn == 27:
		if board.stones[0:6] != [0, 2, 0, 0, 0, 0] or board.stones[7:13] != [0, 0, 0, 0, 0, 1]:
			return
		else:
			print("HI!", board)

	for new_board in board.all_moves():
		if turn == 26:
			print(new_board)
		iterate_board(new_board, turn + 1)

iterate_board(Board(), 0)
