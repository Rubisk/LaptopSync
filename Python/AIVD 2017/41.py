import math
import os
from termcolor import colored

colors = ['red', 'green', 'yellow', 'magenta', 'cyan', 'blue', 'grey']

raw_grid = "! ! ! ! ! ! r F A n A n r J ! O r U U U U O E E E E E n F n n n J n J n E A A A A U N a N O n E O F O O J J J J t E r r r A U N a N O O a n F n a t E E J t E ! ! r A U r a N N N a n F O a t t E J t a r ! r A U O e e e e e n F O E t t t J t A A A J J U O O O O O O O F O e O g t J t t O O O O O g g g g g g J F t S g g E g g i i i i i i O g g g n U n F g n n E E n S S S S S S i e n n e n D D F D D D D D D n E n n S i i n D r D D n n F n n D D n n E E E n S S S r r D D D D D F n E E E E E E n D D S E S r r r J A n n F E U E E U U D D U S E E D E E E E E E E F E n A A D J U E E E E E A ! r r n n n r F E E n n J U U E E E E E J k E E E E E k F n n E n I U E E U E U E J ! J r w k E w F w w J U I I U w E E U E U r J w w k E w F J w w J w U I U U U U E E ! J w r k E w F J w w I I I w e e U E U E k E E E E E w F J w w w e e w J U E E E E ! ! ! ! r k w F w J w e e e w U E E E U E"
raw_grid = raw_grid.split(" ")

size = int(math.sqrt(len(raw_grid)))

grid = [[raw_grid[x + size * y] for x in range(size)] for y in range(size)]

# grid[x][y]


characters = set(raw_grid)

def fancy_print(grid, groups=None):
	os.system('cls' if os.name == 'nt' else 'clear')
	print("".join(characters))
	print("===")
	for y in range(size):
		for x in range(size):
			printed = False
			if groups != None:
				for i, g in enumerate(groups):
					if (x, y) in g:
						print(colored(grid[x][y], colors[i % len(colors)], 'on_white'), end=" ")
						printed = True
			if not printed:
				print(grid[x][y], end=" ")
		print()

def fall_down(grid):
	for x in range(len(grid)):
		column = grid[x]
		column = [x for x in column if x != "_"]
		column = ["_" for _ in range(size - len(column))] + column
		grid[x] = column
	return grid
		
def remove_empty_columns(grid):
	new_grid = []
	for column in grid:
		if set(column) != {"_"}:
			new_grid.append(column)

	for _ in range(size - len(new_grid)):
		new_grid.append(["_" for _ in range(size)])
	return new_grid

def find_all_neighbours(grid, x, y):
	character = grid[x][y]
	neighbours = {(x, y)}

	old_neighbour_size = 0
	while old_neighbour_size < len(neighbours):
		old_neighbour_size = len(neighbours)
		new_neighbours = set()
		for nx, ny in neighbours:

			for dx in [-1, 0, 1]:
				for dy in [-1, 0, 1]:
					if dx * dy != 0:
						continue
					cx = nx + dx
					cy = ny + dy
					if 0 <= cx < size and 0 <= cy < size:
						if grid[cx][cy] == character:
							new_neighbours.add((cx, cy))
		neighbours = neighbours.union(new_neighbours)
	return neighbours

def remove(grid, x, y):
	neighbours = find_all_neighbours(grid, x, y)
	for nx, ny in neighbours:
		grid[nx][ny] = "_"
	grid = fall_down(grid)
	grid = remove_empty_columns(grid)
	return grid

def find_groups(grid, character):
	groups = []
	for x in range(size):
		for y in range(size):
			if grid[x][y] != character:
				continue
			if any([(x, y) in g for g in groups]):
				continue
			groups.append(find_all_neighbours(grid, x, y))
	return groups

message = "FiJnEkErStEnEeNgOeDnIeUwJaAr!"


def try_clear_grid(grid, message, order=[]):
	grid_set = set()
	for column in grid:
		grid_set = grid_set.union(set(column))

	if message == "":
		if grid_set == {"_"}:
			print("SUCCESS!!!")
		return

	if "_" in grid_set:
		grid_set.remove("_")
	if grid_set != set(message):
		return
	letter = message[0]
	groups = find_groups(grid, letter)
	for i, g in enumerate(groups):

		elem = list(g)[0]
		new_grid = remove(grid, *elem)
		try_clear_grid(new_grid, message[1:], order=(order + [i]))

try_clear_grid(grid, message)
