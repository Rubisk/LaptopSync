string = "ACCTCGCCCTCAACTCCTATCCCTAATCCATCCCCC"
string = "".join([string[-i] for i in range(len(string))])
print(string)

codons = [string[i:i + 3] for i in range(0, len(string), 3)]
print(codons)
dna_table = {
 "A": ["GCT", "GCC", "GCA", "GCG, GCN"],
 "L": ["TTA", "TTG", "CTT", "CTC", "CTA", "CTG", "YTR", "CTN"],
 "R": ["CGT", "CGC", "CGA", "CGG", "AGA", "AGG", "CGN", "MGR"],
 "K": ["AAA", "AAG", "AAR"],
 "N": ["AAT", "AAC", "AAY"],
 "M": ["ATG"],
 "D": ["GAT", "GAC", "GAY"],
 "F": ["TTT", "TTC", "TTY"],
 "C": ["TGT", "TGC", "TGY"],
 "P": ["CCT", "CCC", "CCA", "CCG", "CCN"],
 "Q": ["CAA", "CAG", "CAR"],
 "S": ["TCT", "TCC", "TCA", "TCG", "AGT", "AGC", "TCN"],
 "E": ["GAA", "GAG", "GAR", "Thr"],
 "T": ["ACT", "ACC", "ACA", "ACG", "ACN"],
 "G": ["GGT", "GGC", "GGA", "GGG", "GGN"], 
 "W": ["TGG"],
 "H": ["CAT", "CAC", "CAY"],
 "Y": ["TAT", "TAC", "TAY"],
 "I": ["ATT", "ATC", "ATA", "ATH"],
 "V": ["GTT", "GTC", "GTA", "GTG", "GTN"],
 "START": ["ATG"],
 "STOP": ["TAA", "TGA", "TAG", "TAR", "TRA"],
}


answer = []
for codon in codons:
	for key, value in dna_table.items():
		if codon in value:
			answer.append(key)

print("".join(answer))