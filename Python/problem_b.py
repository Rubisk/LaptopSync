"""
Calculating the ratio of the product of all primes up to a given number,
to exp(n).
To do this, we first sieve out all primes up to a given maximum (since we have a fixed bound we can do this),
and then we calculate the individual log of all those primes.
Finally, we sum all the logs and divide by n.
"""
import numpy as np
import sys
import pytest
import math


def find_primes(maximum):
    """
    Finds all primes up to a given maximum.
    For efficiency this uses a sieve of Erastothenes.
    :type maximum: int
    :param maximum: Largest potential prime.
    :return: List of all primes up to maximum.
    """
    sieve = np.zeros(maximum + 1)  # use a numpy array over a list comprehension sinve maximum can be fairly large.
    sieve[0] = 1  # 0 is not prime
    sieve[1] = 1  # 1 is not prime
    primes = []
    i = 2
    while i <= maximum:
        if sieve[i] == 0:
            sieve[::i] = 1
            sieve[i] = 0
            primes.append(i)
        i += 1
    return primes


def get_ratio(n, v=False):
    """
    Returns the ratio of the product of all primes up to n to exp(n).
    :type n: int
    :param n: maximum value of primes.
    :type v: bool
    :param v: enables extra (debug) printing.
    :return: ratio of the product of all primes up to n to exp(n).
    """
    primes = find_primes(n)
    logs = map(math.log, primes)
    log_sum = sum(logs)
    if v:
        print("The sum of the logs equals:", log_sum)
    return log_sum / n


def test_find_primes():
    # test code for find_primes
    assert find_primes(5) == [2, 3, 5]
    assert find_primes(20) == [2, 3, 5, 7, 11, 13, 17, 19]


if __name__ == "__main__":  # allow this to be used as a library too
    if "-test" in sys.argv:
        pytest.main(__file__)
    verbose = "-verbose" in sys.argv
    try:
        user_input = int(sys.argv[1])
    except (ValueError, IndexError):
        print("Invalid argument. Invoke like:")
        print("    python <n> {-test} {-verbose}")
        print("To find the ratio..")
        print("Now calculating for default value 100000.")
        user_input = 100000
    if verbose:
        print("Input value <n> is equal to:", user_input)
    ratio = get_ratio(user_input, verbose)
    print("The ratio equals:", ratio)
