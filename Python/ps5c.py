"""
Literal implementation of problem 5c.
<Rant>
Please do not use lower-camel-case
for naming python variables.
Python variables should be named all lower case, using underscores
to separate words.
Good: some_function, some_value
Bad: someFunction, someValue
</Rant>
To stick with naming conventions I have changed the interface
of constrained_match_pair, this note is mainly here in case this is graded
using some automated tool.
"""
import sys
import pytest
import itertools as it
from problem_5b import substring_match_exact


def constrained_match_pair_gen(first_match, second_match, length):
    """
    Yields all values in first_match for which
    :param first_match:
    :param second_match:
    :param length:
    :return:
    """
    for n in first_match:
        if n + length + 1 in second_match:
            yield n


def constrained_match_pair(first_match, second_match, length):
    return tuple(constrained_match_pair_gen(first_match, second_match, length))


def substring_match_one_sub(target, key):
    """
    Search for all locations of key in target, with one substitution.
    For whatever reason I couldn't get the function provided to work properly,
    so I decided I'd just rewrite it.
    :type target: str
    :param target: String to search through.
    :type key: str
    :param key: String to find.
    """
    # Because we're using an algorithm that is capable of finding matches twice,
    # we need to delete the duplicate values.
    answers = set()

    for miss in range(len(key)):
        keys = key[:miss], key[miss + 1:]
        matches = (substring_match_exact(target, k) for k in keys)
        # print(list(matches), keys)  # debug
        answers |= set(constrained_match_pair(*matches, len(keys[0])))

    # For testing purposes we also sort our findings.
    return tuple(sorted(answers))


def test_constrained_match_pair():
    assert constrained_match_pair((5, 7, 9, 13), (10, 16, 18, 21), 2) == (7, 13)


def test_substring_match_one_sub():
    assert substring_match_one_sub("ATGACATGCACAAGTATGCAT", "ATGC") == (0, 5, 15)


if __name__ == "__main__":  # package protection
    if "-test" in sys.argv:
        pytest.main(__file__)
    else:
        try:
            positions = substring_match_one_sub(*sys.argv[1:])
            print("If we're allowing one wildcard character,\r\n"
                  "string \"{2}\" appears at indices {1} in \"{0}\".".format(sys.argv[1],
                                                                             positions,
                                                                             sys.argv[2]))
        except (IndexError, TypeError):
            print("Invalid argument. Invoke like:")
            print(">>  python <target> <key> {-test}")
