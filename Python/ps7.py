import numpy as np
import pytest
import sys


# -- code for (a)

def find_sequence(n):
    """
    Recursive solution to find the sequence starting with n
    :param n: Integer to find the sequence for
    :return: Sequence for n
    """
    if n == 1:
        return [1]
    if n % 2 == 0:
        return [n] + find_sequence(n / 2)
    return [n] + find_sequence(3 * n + 1)


# -- code for (b)

def list_of_sequences(max_n):
    """

    :param max_n:
    :return:
    """
    list_of_lists = [find_sequence(n) for n in range(1, max_n + 1)]
    return [(x, len(x)) for x in list_of_lists]


# -- code for (c)

def find_sequence_length_cache(n, cache=None):
    """
    Cache implementation of finding the sequence length.
    The idea of using a cache is that we don't recalculate the length of small
    numbers each time we encounter them in the sequence of a big number.
    :param n: sequence length to find right now
    :param cache: List of values for length of sequences
    :return: Length of sequence for n
    """
    n = int(n)
    if len(cache) > n and cache[n] != 0:
        return cache[n]  # if we know the answer no need to calculate.
    elif n == 1:
        return 1
    elif n % 2 == 0:
        length = 1 + find_sequence_length_cache(n / 2, cache)
    else:
        length = 1 + find_sequence_length_cache(3 * n + 1, cache)
    if len(cache) > n:
        # write to the cache. Because the cache is an array,
        # it will keep the reference and it won't be garbage
        # collected on leaving function scope, as find_longest_sequence
        # should have a reference to it.
        cache[n] = length
    return length


def find_longest_sequence(max_n):
    """
    Finds the longest sequence under max_n
    :param max_n: Upper bound for longest sequence.
    :return: longest sequence, length of longest sequence
    """
    cache = np.zeros(max_n)
    for n in range(1, max_n + 1):
        find_sequence_length_cache(n, cache)
    longest = max(cache)
    return np.where(cache == longest)[0][0], int(longest)


# -- code for (d)

def find_smallest_starting_point(length):
    """
    Finds the first sequence with exact length.
    :param length: Length for the sequence have.
    :return: Starting number of the sequence with that exact length.
    """
    cache = np.zeros(length)
    i = 0
    while cache[i] != length:
        i += 1
        if i >= len(cache):
            # print("[INFO] Increasing cache size with {0} cells.".format(i))

            # we want this to be as fast as possible
            # so let's make use of numpy's neat array functions
            # since we have no proper bound, we just double our cache every now and then
            cache = np.concatenate((cache, np.zeros(i)))
        find_sequence_length_cache(i, cache)  # automatically writes to the cache
    return i


# -- test code --

def test_find_sequence():
    assert len(find_sequence(27)) == 112


def test_find_longest_sequence():
    assert find_longest_sequence(30) == 27
    assert find_longest_sequence(100000) == 77031


if __name__ == "__main__":
    if "-test" in sys.argv:
        pytest.main(__file__)
    else:
        try:
            option = int(sys.argv[1])
            if option == 1:
                user_input = int(sys.argv[2])
                print("The length of sequence {0} is {1}."
                      .format(user_input, len(find_sequence(user_input))))
            elif option == 2:
                user_input = int(sys.argv[2])
                print("All sequence for n up to {0} are {1}."
                      .format(user_input, list_of_sequences(user_input)))
            elif option == 3:
                user_input = int(sys.argv[2])
                print("The longest sequence under {0} starts with {1}, and has length {2}."
                      .format(user_input, *find_longest_sequence(user_input)))
            elif option == 4:
                user_input = int(sys.argv[2])
                print("The first sequence with length {0} starts with {1}."
                      .format(user_input, find_smallest_starting_point(user_input)))
            else:
                raise ValueError
        except (ValueError, IndexError):
            print("Invalid argument. Invoke like:")
            print(">>  python <option> <n> {-test}")
            print("Where <option> is one of:")
            print("1: Find sequence length of <n>")
            print("2: Find all sequence up to <n>")
            print("3: Find longest sequence under <n>")
            print("4: Find first sequence of length <n>")
