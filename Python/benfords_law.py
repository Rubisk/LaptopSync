import sys


def first_digit(n):
    while n >= 10:
        n /= 10
    return int(n)


def count(max_n, k):
    total = 0
    for n in range(1, max_n + 1):
        if first_digit(2 ** n) == k:
            total += 1
    return total


if __name__ == "__main__":
	for k in range(1, 10):
		print(count(int(sys.argv[1]), k), end=", ")

