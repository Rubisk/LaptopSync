import sys
import pytest
import itertools as it
from problem_5b import substring_match_exact
from problem_5c import substring_match_one_sub


def substring_match_exactly_one_sub(target, key):
    return tuple([i for i in substring_match_one_sub(target, key)
                  if i not in substring_match_exact(target, key)])


def test_substring_match_exactly_one_sub():
    assert substring_match_exactly_one_sub("ATGACATGCACAAGTATGCAT", "ATGC") == (0,)


if __name__ == "__main__":  # package protection
    if "-test" in sys.argv:
        pytest.main(__file__)
    else:
        try:
            positions = substring_match_exactly_one_sub(*sys.argv[1:])
            print("If we demand exactly one wildcard character,\r\n"
                  "string \"{2}\" appears at indices {1} in \"{0}\".".format(sys.argv[1],
                                                                             positions,
                                                                             sys.argv[2]))
        except (IndexError, TypeError):
            print("Invalid argument. Invoke like:")
            print(">>  python <target> <key> {-test}")
