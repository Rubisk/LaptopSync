import sys


def greedy(n, d):
    assert(n * (n + 1) / 2 % d == 0)
    numbers = range(1, n + 1)
    partitions = []
    while len(numbers) > 0:
        new_partition = []
        partition_sum = 0
        while partition_sum < d:
            if min(numbers) > d - partition_sum:
                print(partitions, new_partition, partition_sum, d, numbers)
                return False
            possible_new_numbers = filter(lambda x: x <= d - partition_sum, numbers)

            new_number = max(possible_new_numbers)
            numbers = [n for n in numbers if n != new_number]
            new_partition.append(new_number)
            partition_sum += new_number

        partitions.append(new_partition)
    return True


def neat(n, d):



def can_all_greedy(n):
    for d in range(n, int(n * (n + 1) / 2) + 1):
        if (n * (n + 1) / 2) % d == 0:
            if not greedy(n, d):
                return False
    return True


if __name__ == "__main__":
    n = 1
    while True:
        if not can_all_greedy(n):
            print(n)
        n += 1
