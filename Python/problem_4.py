"""
Coin throw simulator to determine the optimal strategy.
Automaton simulates a single game.
simulate_games takes a large sample size of games and
determines the average winning chance.
optimize_for_alice_choice then goes over all possible choices,
and finds the one with the best winning chance.
"""
import random
import itertools as it
import pytest
import sys


HEAD = 0
TAIL = 1


class Automaton(object):
    """
    Automaton to simulate games.
    """
    NO_WINNER = -1
    ALICE = 0
    BOB = 1

    _recent_throws = []
    _game_length = 0

    def __init__(self, alice, bob):
        for i in alice, bob:
            # Input is kinda special so let's verify it.
            assert type(i) == tuple
            assert len(i) == 3
        assert bob != alice
        self.alice = alice
        self.bob = bob

    def reset(self):
        self._game_length = 0
        self._recent_throws = []

    @property
    def winner(self):
        if not self._game_ended:
            return self.NO_WINNER
        return self.ALICE if self._alice_won else self.BOB

    @property
    def winner_string(self):
        if not self._game_ended:
            return "No winner"
        return "Alice" if self._alice_won else "Bob"

    def play_game(self):
        while not self._game_ended:
            self._throw_coin()

    # private
    @property
    def _bob_won(self):
        if not len(self._recent_throws) == 3:
            return False
        return all([self._recent_throws[i] == self.bob[i] for i in range(3)])

    # private
    @property
    def _alice_won(self):
        if not len(self._recent_throws) == 3:
            return False
        return all([self._recent_throws[i] == self.alice[i] for i in range(3)])

    # private
    @property
    def _game_ended(self):
        return self._alice_won or self._bob_won

    # private
    def _throw_coin(self):
        self._recent_throws.append(random.randint(0, 1))
        # We can forget about everything but the last 3 throws.
        self._recent_throws = self._recent_throws[-3:]


def simulate_games(alice, bob, simulations=1000):
    """
    Simulates a bunch of games and determines bobs winrate.
    :type alice: tuple
    :param alice: Win condition of alice.
    :type bob: tuple
    :param bob: Win condition of bob.
    :param simulations: Number of simulations to run.
    :return type: int
    :return: Bobs win rate
    """
    bob_wins = 0
    game = Automaton(alice, bob)
    for _ in range(simulations):
        game.play_game()
        if game.winner == Automaton.BOB:
            bob_wins += 1
        game.reset()
    bob_win_chance = bob_wins / simulations
    return bob_win_chance


def optimize_for_alice_choice(alice):
    """
    Finds the optimal choice for bob through simulation.
    :type alice: tuple
    :param alice: Win condition of alice.
    :return type: tuple
    :return: Ideal win condition of bob
    """
    # to make sure the == test works in case it's somehow a list
    alice = tuple(alice)
    best = (None, 0)
    for bob in it.product((HEAD, TAIL), repeat=3):
        if bob == alice:
            continue
        chance = simulate_games(alice, bob)
        if chance > best[1]:
            best = (bob, chance)
    return best[0]


# test code
def test_simulate_games():
    assert abs(simulate_games((HEAD, HEAD, TAIL),
                             (TAIL, HEAD, HEAD)) - (3 / 4)) < 0.05
    assert abs(simulate_games((HEAD, HEAD, TAIL),
                             (TAIL, HEAD, TAIL)) - (3 / 8)) < 0.05
    assert abs(simulate_games((HEAD, HEAD, HEAD),
                             (TAIL, TAIL, TAIL)) - (1 / 2)) < 0.05


def test_optimize():
    assert optimize_for_alice_choice((HEAD, HEAD, HEAD)) == (TAIL, HEAD, HEAD)
    assert optimize_for_alice_choice((HEAD, HEAD, TAIL)) == (TAIL, HEAD, HEAD)


if __name__ == "__main__":  # package protection
    if "-test" in sys.argv:
        pytest.main(__file__)
    try:
        a = (0 if sys.argv[1][i] == 'h' else 1 for i in range(3))
    except IndexError:
        print("Invalid argument. Invoke like:")
        print(">>  python <alice> {-test}")
        print("Where alice is of the form 'hth'")
        print("To simulate a game.")
        print("Now running with default arguments 'hhh'.")
        a = (0, 0, 0)
    output = optimize_for_alice_choice(a)
    print("The best choice for bob is {0}, {1}, {2}."
          .format(*("head" if x == 0 else "tail" for x in output)))
