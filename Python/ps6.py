import pytest
import sys


def sgn(a):
    """
    Finds the sign of an integer.
    :param a: Integer to find the sign of.
    :return: -1 if a is negative, 1 otherwise.
    """
    return -1 if a < 0 else 1


def bisection_solve(f, a, b, tolerance=0.001, iterations=100):
    """
    Find the zero of a function f between a and b using the
    Bisection Method with tolerance (default: 0.001) and
    maximum number of iterations (default: 100)
    :param f: Function taking one argument and returning and integer.
    :param a: Minimum of the interval
    :param b: Maximum of the interval
    :param tolerance: Maximum distance of 0 to consider it 0.
    :param iterations: Maximum number of iterations to find the root. (-1) for infinite
    :return: (root, iterations used.
    """
    iterations_used = 1
    while iterations_used <= iterations or iterations == -1:
        x = (a + b) / 2
        if -tolerance < f(x) < tolerance:
            return x, iterations_used
        if f(x) < 0:
            a = x
        else:
            b = x
        iterations_used += 1


def regula_falsi_solve(f, a, b, tolerance=0.001, iterations=100):
    """
    Find the zero of a function f between a and b using the
    Regula Falsi Method with tolerance (default: 0.001) and
    maximum number of iterations (default: 100)
    :param f: Function taking one argument and returning and integer.
    :param a: Minimum of the interval
    :param b: Maximum of the interval
    :param tolerance: Maximum distance of 0 to consider it 0.
    :param iterations: Maximum number of iterations to find the root. (-1) for infinite
    :return: (root, iterations used.
    """
    iterations_used = 1
    while iterations_used <= iterations or iterations == -1:
        x = b - f(b) * (b - a) / (f(b) - f(a))
        if -tolerance < f(x) < tolerance:
            return x, iterations_used
        if sgn(f(x)) == sgn(f(b)):
            b = x
        else:
            a = x
        iterations_used += 1


def secant_solve(f, a, b, tolerance=0.001, iterations=100):
    """
    Find the zero of a function f between a and b using the
    Secant Method with tolerance (default: 0.001) and
    maximum number of iterations (default: 100)
    :param f: Function taking one argument and returning and integer.
    :param a: Minimum of the interval
    :param b: Maximum of the interval
    :param tolerance: Maximum distance of 0 to consider it 0.
    :param iterations: Maximum number of iterations to find the root. (-1) for infinite
    :return: (root, iterations used.
    """
    iterations_used = 1
    while iterations_used <= iterations or iterations == -1:
        a, b = b, b - f(b) * (b - a) / (f(b) - f(a))
        if -tolerance < f(b) < tolerance:
            return b, iterations_used
        iterations_used += 1


def newton_raphson_solve(f, fp, a, tolerance=0.001, iterations=100):
    """
    Find the zero of a function f with derivative fp starting
    With initial approximation x0 using the Secant Method with
    tolerance (default: 0.001) and maximum number of
    iterations (default: 1000)
    :param f: Function taking one argument and returning and integer.
    :param fp: Function taking one argument and returning and integer,
    representing the derivative of f
    :param a: Starting point of search
    :param tolerance: Maximum distance of 0 to consider it 0.
    :param iterations: Maximum number of iterations to find the root. (
    (-1) for any number of iterations.
    :return: (root, iterations used).
    """
    iterations_used = 1
    while iterations_used <= iterations or iterations == -1:
        a = a - (f(a) / fp(a))
        if -tolerance < f(a) < tolerance:
            return a, iterations_used
        iterations_used += 1


def get_iterations_used_for_tolerances(solver, max_precision, *args):
    """
    Ugly wrapper for all of the above functions.
    :param solver: Solver to use
    :type solver: function
    :param max_precision: maximum precision to use (10^-n)
    :param args: Call arguments for the solver
    :return:
    """
    return [solver(*args, tolerance=10**(-i),
                   iterations=-1)[1] for i in range(max_precision)]


def test_bisection_solve():
    # Verify for value approximated using calculator
    assert abs(0.45340 - bisection_solve(lambda x: x**3 + 2*x - 1, 0, 1)[0]) < 0.01


def test_regula_falsi_solve():
    # Verify for value approximated using calculator
    assert abs(0.45340 - regula_falsi_solve(lambda x: x**3 + 2*x - 1, 0, 1)[0]) < 0.01


def test_secant_solve():
    # Verify for value approximated using calculator
    assert abs(0.45340 - secant_solve(lambda x: x**3 + 2*x - 1, 0, 1)[0]) < 0.01


def test_newton_raphson_solve():
    # Verify for value approximated using calculator
    assert abs(0.45340 - newton_raphson_solve(lambda x: x ** 3 + 2 * x - 1,
                                              lambda x: 3 * x ** 2 + 2, 0)[0]) < 0.01


methods = {
    "secant": secant_solve,
    "newton raphson": newton_raphson_solve,
    "regula falsi": regula_falsi_solve,
    "bisection": bisection_solve
}


if __name__ == "__main__":
    if "-test" in sys.argv:
        pytest.main(__file__)
    else:
        # Because it's quite difficult to implement functions in a command-line interface
        # all code is writen for the standard function
        # x^3 + 2x - 1
        # To test other functions, use this file as a library instead.
        try:
            n, method = int(sys.argv[-1]), " ".join([s.lower() for s in sys.argv[1:-1]])
            if method != "newton raphson":  # newton raphson needs a derivative
                print(get_iterations_used_for_tolerances(methods[method], n,
                                                         lambda x: x**3 + 2 * x - 1,
                                                         0, 1))
            else:
                print(get_iterations_used_for_tolerances(methods[method], n,
                                                         lambda x: x ** 3 + 2 * x - 1,
                                                         lambda x: 3 * x ** 2 + 2,
                                                         0))
        except (IndexError, ValueError) as e:
            print("Invalid argument. Invoke like:")
            print(">>  python <method> <n> {-test}")
            print("Where <method> is one of {0}".format(methods.keys()))
            print("and <n> is the maximum precision.")
