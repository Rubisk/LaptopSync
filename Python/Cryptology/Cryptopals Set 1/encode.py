import base64
import itertools as it

frequencies = [8.34, 1.54, 2.73, 4.14, 12.60, 2.03, 1.92, 6.11, 6.71, 0.23, 0.87, 4.24, 2.53,
               6.80, 7.70, 1.66, 0.09, 5.68, 6.11, 9.37, 2.85, 1.06, 2.34, 0.20, 2.04, 0.06]
characters = b"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"


def challenge_one(string):
    return base64.b64encode(bytes.fromhex(string))


def xor(first, second):
    return bytes(map(lambda x: x[0] ^ x[1], zip(first, second)))


def score_plaintext(text):
    char_count = [0 for _ in range(26)]
    total_count = 0
    for c in text:
        i = (characters.find(c)) % 26
        if c in characters:
            total_count += 1
            char_count[i] += 1

    if total_count == 0:
        return 1

    distance = sum([((char_count[i] / total_count) - (frequencies[i] / 100)) ** 2 for i in range(26)])

    return distance


def single_key_decrypt(hex_string):
    encrypted_bytes = bytes.fromhex(hex_string)

    best_score = -1
    best_result = ""
    best_key = ""
    for c in range(256):
        decrypted_bytes = bytes([x ^ c for x in encrypted_bytes])

        score = score_plaintext(decrypted_bytes)
        if best_score == -1 or best_score > score:
            best_score = score
            best_result = decrypted_bytes
            best_key = c
    return best_result, best_key

def repeating_key_xor(string, key):
    new_string = bytes([string[i] ^ key[i % len(key)] for i in range(len(string))])
    return new_string


def hamming_distance(first, second):
    assert len(first) == len(second)
    distance = 0
    for b1, b2 in zip(first, second):
        i = 1
        for _ in range(8):
            if ((b1 & i) ^ (b2 & i)) != 0:
                distance += 1
            i = i << 1
    return distance


def decrypt_repeated_xor(string):
    keysizes = []
    for keysize in range(2, 40):
        first_distance = hamming_distance(string[0:keysize], string[keysize:2 * keysize])
        second_distance = hamming_distance(string[2 * keysize:3 * keysize], string[3 * keysize:4 * keysize])
        distance = (first_distance + second_distance) / 2 / keysize
        keysizes.append((keysize, distance))

    keysizes.sort(key=lambda x: x[1])

    for best_keysize, _ in keysizes:
        key = bytearray()
        decyphered_blocks = []
        for i in range(best_keysize):
            shifted_block = string[i::best_keysize]
            decyphered_block_shifted, k = single_key_decrypt(shifted_block.hex())
            key.append(k)
            decyphered_blocks.append(decyphered_block_shifted)

        print(key)

        print(repeating_key_xor(string, key))


def challenge_two():
    first = bytes.fromhex("1c0111001f010100061a024b53535009181c")
    second = bytes.fromhex("686974207468652062756c6c277320657965")

    print(xor(first, second).hex())


def challenge_three():
    print(single_key_decrypt("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736")[0])


def challenge_four():
    best_score = -1
    best_result = ""
    with open("detect_single_character.txt", "r") as fo:
        for line in fo:
            decrypted = single_key_decrypt(line.rstrip()[0])
            score = score_plaintext(decrypted)
            if best_score == -1 or score < best_score:
                best_score = score
                best_result = decrypted
    print(best_result)


def challenge_five():
    file = open("challenge_five.txt", "rb")
    text = file.read()
    file.close()
    print(repeating_key_xor(text, b"ICE").hex())

def challenge_six():
    file = open("challenge_six.txt", "rb")
    text = file.read()
    file.close()
    
    print(decrypt_repeated_xor(base64.b64decode(text)))

challenge_six()