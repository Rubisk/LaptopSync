"""
End results are very, very close to what the theory says.
Most of the time the answers are like 5% of with (25) steps,
although it varies a lot more for large amounts of steps.

the average distance for 2 dimensions is most likely 1.5 * n
(as that number would make the most sense, given its somewhere in between 1.4 ~ 1.6)
Sqrt(2) * N would also be possible.
"""
import numpy as np
import matplotlib.pyplot as plot
import sys

# for whatever reason we need to randomly generate 10,000 numbers
# and then discard them so let's do that first.
discarded_values = np.random.random_integers(0, 1, 100000) * 2 - 1
# let's just do some validation so it's not as pointless:
for i in (-1, 1):
    assert i in discarded_values
assert -100000 < sum(discarded_values) < 100000  # this should never fail as we have at least one 1 and -1
del discarded_values  # no reason to keep this big of an array from being gc'ed.


# -- 1 Dimension

def generate_1d_walks(number_of_steps, walks=1, random_step_size=False):
    """
    Generates one-dimensional walks.
    :param number_of_steps: Length of the walks.
    :param walks: Number of walks to generate
    :param random_step_size: Whether or not step size should be uniform-[0, 1].
    Step size is fixed at 1 otherwise.
    :return: np array, one walk per row. Every walk starts with the origin.
    """
    if random_step_size:
        steps = np.random.uniform(-1, 1, (walks, number_of_steps))
    else:
        steps = np.random.random_integers(0, 1, (walks, number_of_steps)) * 2 - 1
    # noinspection PyTypeChecker
    return np.insert(np.cumsum(steps, axis=1), 0, 0, axis=1)


def plot_1d_walk(number_of_steps, **kwargs):
    """
    Generates and displays a one-dimensional walk.
    :param number_of_steps: Length of the walk.
    """
    x = np.arange(number_of_steps + 1)  # Remember that walks contain the origin
    plot.plot(x, generate_1d_walks(number_of_steps, **kwargs)[0])
    plot.show()


def generate_1d_data(*args, **kwargs):
    """
    Generates statistical data for a one-dimensional random walk.
    """
    if "walks" not in kwargs:
        kwargs['walks'] = 1000
    walks = generate_1d_walks(*args, **kwargs)
    endpoints = walks[:, -1]    # The last *column* contains all endpoints
    distances = endpoints ** 2  # I think this is what they mean by d**2 (?)
    return endpoints, distances


# -- 2 Dimensions

def generate_2d_walks(number_of_steps, walks=1, method="von Neumann"):
    """
    Generates one-dimensional walks.
    :param number_of_steps: Length of the walks.
    :param walks: Number of walks to generate
    :param method: Method to se for the random walk.
    :return: np array, one walk per row. Every walk starts with the origin.
    """
    if method == "von Neumann":
        jumps = [(1, 0), (0, 1), (0, -1), (-1, 0)]
    else:
        jumps = [(1, 0), (0, 1), (0, -1), (-1, 0),
                 (1, 1), (-1, 1), (1, -1), (-1, -1)]
    steps = np.random.random_integers(0, len(jumps) - 1, (walks * number_of_steps))

    def random_to_jump(a):
        return jumps[a]

    # kinda hard to do this properly, just using list comprehensions
    steps = np.array([random_to_jump(w) for w in steps])
    steps = np.reshape(steps, (walks, number_of_steps, 2))
    return np.insert(np.cumsum(steps, axis=1), 0, [0, 0], axis=1)


def plot_2d_walk(*args, **kwargs):
    """
    Plots a 2d walk.
    """
    if "walks" in kwargs:
        kwargs.pop("walks")
    walk = generate_2d_walks(*args, **kwargs)[0]
    plot.plot(*zip(*walk), "ro")
    plot.show()


def generate_2d_data(*args, **kwargs):
    """
    Generates statistical data for a one-dimensional random walk.
    """
    if "walks" not in kwargs:
        kwargs['walks'] = 1000
    walks = generate_2d_walks(*args, **kwargs)
    endpoints = walks[:, -1]
    distances = endpoints[:, 0] ** 2 + endpoints[:, 1] ** 2
    return endpoints, distances

# -- UI Stuff


def histogram(data, x_label, y_label, title):
    """
    Displays a histogram.
    Calling 5 functions gets annoying quick, let's just call one.
    :param data: data to display.
    :param x_label: Text by X axis.
    :param y_label: Text by Y axis.
    :param title: Title of histogram.
    """
    # There's no point in drawing a histogram with all different values
    # so we round them to put all values in a small interval in the same box
    data = np.round(data, 0)
    plot.hist(data, 100, normed=True)
    plot.xlabel(x_label)
    plot.ylabel(y_label)
    plot.title(title)
    plot.show()


if __name__ == "__main__":
    try:
        mode = sys.argv[1]
        wanted_step_number = int(sys.argv[2])
        assert mode in ("1d-simple", "1d-uniform", "2d-Neumann", "2d-Moore")
        if mode in ("1d-simple", "1d-uniform"):
            uniform = mode == "1d-uniform"
            print("Simulating one walk and displaying the walk...")
            plot_1d_walk(wanted_step_number, random_step_size=uniform)
            print("Now simulating 1000 walks and statistically analyzing...")
            end, dist = generate_1d_data(wanted_step_number, random_step_size=uniform)

            print("The mean endpoint is {0}, and endpoint variance is {1}.".format(np.mean(end), np.var(end)))
            print("Displaying histogram of endpoints.")
            histogram(end, "Endpoint", "Probability", "Distribution of endpoints.")

            print("The mean square distance is {0}, and square distance variance is {1}."
                  .format(np.mean(dist), np.var(dist)))
            print("Displaying histogram of square distances.")
            histogram(dist, "Distances", "Probability", "Distribution of square distances.")
        else:
            m = "Moore" if mode == "2d-Moore" else "Neumann"
            print("Simulating one walk and displaying as point plot...")
            plot_2d_walk(wanted_step_number, method=m)
            print("Now simulating 1000 walks and statistically analyzing...")
            end, dist = generate_2d_data(wanted_step_number, method=m)

            # There's no real "mean point" unless mean x and y,
            # but since x and y are independent this isn't really
            # interesting, so we skip on to square distance.

            print("The mean square distance is {0}, and square distance variance is {1}."
                  .format(np.mean(dist), np.var(dist)))
            print("Displaying histogram of square distances.")
            histogram(dist, "Distances", "Probability", "Distribution of square distances.")

    except (ValueError, IndexError, AssertionError) as e:
        print("Invalid or missing argument. Invoke like:")
        print(">> python <mode> <number of steps>")
        print(u"Where mode is one of \"1d-simple\", \"1d-uniform\", \"2d-Neumann\", \"2d-Moore\"")
