
# opgave a
# gemiddelde van deelbare getallen 1-1000


def gemiddelde_deelbare(lijst, deler):
    som = 0
    aantal = 0
    for l in lijst:
        if l % deler == 0:
            som += l
            aantal += 1
    return som / aantal

# test de functie gemiddelde_deelbare
getallen = []
for i in range(1, 1001):
    getallen.append(i)
print("opgave a:")
print(gemiddelde_deelbare(getallen, 7))


# opgave b
# vogels en steenvruchten

def permute_and_print(target):
    left_target, right_target = target
    for left in left_target:
        for right in right_target:
            print("It is a {0} {1}".format(left, right))


# test de functie permute_and_print
print("opgave b:")
M = [['European', 'African'], ['swallow', 'coconut']]
permute_and_print(M)


# opgave c
# genereer een lijst priemgetallen
print("opgave c:")


def get_primes(count):
    primes = []
    i = 2
    while len(primes) < count:
        if 0 not in [i % p for p in primes]:
            primes.append(i)
        i += 1
    return primes

print(get_primes(1000)[-1])


# opgave d
# print de langste reeks aaneengesloten niet-priemgetallen
print("opgave d:")


def get_longest_non_prime_streak(number_of_primes):
    primes = get_primes(number_of_primes)
    # We don't have numpy or anything today so let's do it the slow way.
    best = 0
    end = -1
    streak = 0
    for j in range(primes[-1]):  # we can assume primes is sorted
        streak += 1
        if j in primes:
            streak = 0
        if streak > best:
            best = streak
            end = j
    assert end >= 0 and best > 0
    return best, list(range(end - best + 1, end + 1))


length, actual_streak = get_longest_non_prime_streak(1000)
print(length)
print(actual_streak)
