import sys
import pytest


def count_substring_match_recursive(target, key):
    """
    Recursive search for number of times key appears in target.
    :type target: str
    :param target: String to search through.
    :type key: str
    :param key: String to find.
    :return type: int
    :return: Number of times key appears in target.
    """
    if key not in target:
        return 0
    new_target = target[target.find(key) + 1:]
    return 1 + count_substring_match_recursive(new_target, key)


def count_substring_match(target, key):
    """
    Iterative search for number of times key appears in target.
    :type target: str
    :param target: String to search through.
    :type key: str
    :param key: String to find.
    :return type: int
    :return: Number of times key appears in target.
    """
    count = 0
    for i in range(len(target) - len(key) + 1):
        if target[i:len(key) + i] == key:
            count += 1
    return count


def test_count_substring_match_recursive():
    assert count_substring_match_recursive("abababacdababcdab", "ab") == 6


def test_count_substring_match():
    assert count_substring_match("abababacdababcdab", "ab") == 6


if __name__ == "__main__":  # package protection
    if "-test" in sys.argv:
        pytest.main(__file__)
    else:
        try:
            string_count = count_substring_match_recursive(*sys.argv[1:])
            print("String \"{2}\" appears {1} times in \"{0}\".".format(sys.argv[1],
                                                                        string_count,
                                                                        sys.argv[2]))
        except (IndexError, TypeError):
            print("Invalid argument. Invoke like:")
            print(">>  python <target> <key> {-test}")
