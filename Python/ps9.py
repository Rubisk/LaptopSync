import numpy as np
import sys


def problem_a():  # prints the answer to problem b
    a = np.array(((-3, -1, 0),
                  (4, 7, -10),
                  (4, 3, -3)))

    b = np.array(((-2, 2, -5),
                  (12, -7, 20),
                  (6, -4, 11)))

    c = np.array(((-2, 14, 8),
                  (-1, 10, 6),
                  (1, -13, -8)))
    print("Problem A verified successfully.")

    for n in range(1, 5):
        assert (np.linalg.matrix_power(a, 4*n + 1) == a).all()
        assert (np.linalg.matrix_power(b, n) == b).all()
        assert (np.linalg.matrix_power(c, n + 2) == 0).all()


def problem_b():  # prints the answer to problem a
    a = np.array(((5, -2, 2),
                  (-2, 5, 1),
                  (2, 1, 2)))
    print("A's cholesky decomposition is as follows:")
    print("L is:")
    print(np.linalg.cholesky(a))
    print("L transposed is:")
    print(np.linalg.cholesky(a).T)
    print("")
    print("A's QR decomposition is as follows:")
    print("Q is:")
    print(np.linalg.qr(a)[0])

    print("R is:")
    print(np.linalg.qr(a)[1])


if __name__ == "__main__":
    problem = sys.argv[1]
    if problem == "a":
        problem_a()
    elif problem == "b":
        problem_b()
