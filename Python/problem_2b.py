"""
Finds the largest integer n such that the equation:
ax + by + cz = n
has no solutions (a, b, c) > 0.
Very slow and straightforward algorithm, just go over all n until we can create min(x, y, z) in a row.
If gcd(x, y, z) != 1 it will not be possible to create all numbers, and we raise an exception.
"""
import sys
import pytest
import numpy as np


def gcd(a, b, *args):
    """
    Simple gcd algorithm.
    Note that it allows for any number of arguments.
    :type a: int
    :type b: int
    :return: the greatest common divisor of a, b, and every other arg.
    """
    if len(args) > 0:
        return gcd(gcd(a, b), args[0], *args[1:])
    if b > a:
        a, b = b, a
    if a % b == 0:
        return b
    return gcd(b, a % b)


class ComputationException(Exception):
    """
    Exception to differentiate from builtin exceptions.
    """
    pass


def can_partition(total, packages):
    """
    Checks whether or not total can be written as sum of elements in packages.
    :type total: int
    :param total: Number to partition.
    :param packages: tuple or list
    :param packages: Elements used to write total as sum.
    """
    packages = tuple(sorted(packages, reverse=True))  # Because tuples are more efficient (?).

    # We don't need to sort on every cycle, so wrap this inside an outer function.
    def _can_partition(_total, _packages):
        for p in _packages:
            if p > _total:
                continue
            elif p == _total:
                return True
            elif _can_partition(_total - p, _packages[_packages.index(p):]):
                return True
        return False

    return _can_partition(total, packages)


def find_biggest_without_solution(packages=(6, 9, 20)):
    """
    Finds the largest integer N that can't be written as sum of elements in packages.
    :param packages: tuple or list
    :param packages: Elements used to write N as sum.
    """
    streak = 0
    last = -1
    n = 1
    if gcd(*packages) != 1:
        raise ComputationException("There are infinitely many numbers that can't be written as a sum.")

    while streak < min(packages):  # Go until we find a long enough streak of numbers that can be made.
        if can_partition(n, packages):  # It can be done, extend our streak.
            streak += 1
        else:  # It can't be done, reset our streak.
            streak = 0
            last = n
        n += 1  # Note that we know a max N exists, since we verified the gcd.-

    if last == -1:  # Apparently 1 was possible, whoops.
        raise ComputationException("It is possible to create all numbers.")
    return last


def test_gcd():
    assert gcd(3, 5) == 1
    assert gcd(3, 5, 7) == 1
    assert gcd(20, 10, 15) == 5


def test_find_biggest_without_solution():
    assert find_biggest_without_solution((2, 3)) == 1
    with pytest.raises(ComputationException):
        find_biggest_without_solution((4, 6))
    assert find_biggest_without_solution((5, 7)) == 23


def test_can_partition():
    assert can_partition(5, (2, 1))
    assert can_partition(15, (6, 9, 20))
    assert not can_partition(10, (6, 9, 20))
    assert can_partition(29, (2, 3, 5, 9, 20))


if __name__ == "__main__":  # allow this to be used as a library too
    if "-test" in sys.argv:
        pytest.main(__file__)
    try:
        user_input = [int(sys.argv[i]) for i in (1, 2, 3)]
    except (ValueError, IndexError):
        print("Invalid argument. Invoke like:")
        print(">>  python <n1> <n2> <n3> {-test}")
        print("To find the largest number that can't be made.")
        print("Now running with default arguments (6, 9, 20).")
        user_input = (6, 9, 20)
    try:
        output = find_biggest_without_solution(user_input)
        print("Given package sizes {0}, {1}, and {2}, the largest number of "
              "McNuggets that cannot be bought in exact quantity is: {3}".format(*user_input, output))
    except ComputationException as e:
        print(e)
