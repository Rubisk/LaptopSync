import itertools as it
import math
import sys
import functools


@functools.lru_cache(maxsize=None)
def power_7(i):
    if i == 1:
        return 7
    return 7 * power_7(i - 1)


def max_7_powers_faculty(n):
    i = 1
    total = 0
    while power_7(i) <= n:
        total += int(n / power_7(i)) * i
        i += 1
    return total


def get_max_7_power_divisor(n, k):
    return max_7_powers_faculty(n) - max_7_powers_faculty(k) - max_7_powers_faculty(n - k)

if __name__ == "__main__":
    for n in range(int(sys.argv[-1])):
        line = ""
        for k in range(0, min(150, n + 1)):
            line += str(get_max_7_power_divisor(n, k) % 10)
        print(line)
