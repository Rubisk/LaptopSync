import itertools as it
import math
import sys


def find_n_counts(max_n):
    squares = [n ** 2 for n in range(0, int((max_n + 1) / 2))]
    solutions = [0 for _ in range(max_n + 1)]
    for s1, s2 in it.combinations(squares, 2):
        # s2 > s1
        if (s2 - s1) > max_n or s2 % 2 == 1 or s1 == 0:
            continue
        n = s2 - s1
        solutions[n] += 1
        k = math.sqrt(s2) / 2
        if k > math.sqrt(s2 - n):
            solutions[n] += 1
    return [(i, solutions[i]) for i in range(max_n + 1) if solutions[i] == 10]


if __name__ == "__main__":
    print(find_n_counts(int(sys.argv[1])))