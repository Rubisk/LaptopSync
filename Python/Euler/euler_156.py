import sys
import pytest


def check(n, digit):
    if n == 0:
        return 0
    elif n <= 10:
        if n == 10 and digit == 1:
            return 2
        return 1 if n >= digit else 0
    else:
        check_total = 0
        if n > 10 and n % 10 < digit:
            check_total -= 1
        check_total += int(n / 10 + 1)

        check_total -= str(n)[:-1].count(str(digit)) * (9 - (n % 10))
        check_total += 10 * check(int(n / 10), digit)

        return check_total


def sum_solutions(max_power, digit):
    n = 10
    total = -8 if digit == 1 else -9
    solution_sum = digit if digit == 1 else 0
    solution_count = 1 if digit == 1 else 0

    while n < 10 ** max_power:
        # check if we are at a solution
        if total == 0:
            assert check(n, digit) == n
            solution_count += 1
            solution_sum += n

        # generate list of decimals equal to digit
        digit_list = []
        for i in range(len(str(n))):
            if str(n)[-i - 1] == str(digit):
                digit_list.append(i)
        i = 0

        # find maximum increment no solutions can be in
        while n % 10 ** (i + 1) == 0:
            i += 1
        if total > 0:
            while total < 10 ** i:
                i -= 1
        else:
            while len(digit_list) * 10 ** i + total + i * 10 ** (i - 1) > 0:
                i -= 1
            if i < 0:
                i = 0

        # properly increase n and the total
        n_add = 10 ** i
        n += n_add
        total += len(digit_list) * n_add + i * 10 ** (i - 1) - n_add

        # go over some special total increment cases.
        if i in digit_list:
            total -= 1
        while str(n)[-i - 1] == "0":
            if str(n)[-i - 2] == "0":
                if digit == 9:
                    total -= 1
                i += 1
                continue
            elif str(n)[-i - 2] == str(digit):
                total += 1
            elif str(n)[-i - 2] == str(digit + 1):
                total -= 1
            break
        else:
            if str(n)[-i - 1] == str(digit):
                total += 1

    return solution_sum


def test_sum_solutions():
    assert sum_solutions(15, 1) == 22786974071

if __name__ == "__main__":
    if "-test" in sys.argv:
        pytest.main(__file__)
    else:
        maximum = int(sys.argv[-1])
        print(sum([sum_solutions(maximum, d) for d in range(1, 10)]))
