"""
Results look good I think. I'm kind of
surprised that only 25 points give such a good approximation.
Code scales up well, seems to handle 10^6 points decently.
"""
import numpy as np
import matplotlib.pyplot as plot
import sys


def generate_points(f, start, end, sample_size):
    """
    Generates function values in an interval
    :param f: Function to evaluate.
    :param start: Lower bound of the interval.
    :param end: Upper bound of the interval
    :param sample_size: Number of points to evaluate.
    Points will be evenly spaced along the interval.
    """
    return f(np.linspace(start, end, sample_size))


def approximate_derivative(sample, start, end):
    """
    Approximates the derivative of a function given by a bunch of points.
    :param sample: Sample points of the function
    :param start: Lower bound of the interval.
    :param end: Upper bound of the interval.
    """
    dx = (end - start) / len(sample)
    derivative_start = (-3 * sample[0] + 4 * sample[1] - sample[2]) / (2 * dx)
    derivative_end = (3 * sample[-1] - 4 * sample[-2] + sample[-3]) / (2 * dx)
    derivatives = (np.roll(sample, -1) - np.roll(sample, 1)) / (2 * dx)
    derivatives[0] = derivative_start
    derivatives[-1] = derivative_end
    return derivatives


def plot_samples(start, end, sample, derivative_sample):
    """
    Plots a function and it's derivative on an interval.
    :param start: Start of the interval.
    :param end: End of the interval.
    :param sample: Sample points of the function.
    :param derivative_sample: Sample points of the derivative.
    """
    x = np.linspace(start, end, len(sample))
    plot.plot(x, sample)
    plot.plot(x, derivative_sample)
    plot.xlim(start, end)
    plot.show()


if __name__ == "__main__":  # import protection
    try:
        # Just call the functions we implemented above.
        points = generate_points(np.sin, 0, 2 * np.pi, int(sys.argv[1]))
        derivative = approximate_derivative(points, 0, 2 * np.pi)
        plot_samples(0, 2 * np.pi, points, derivative)
    except (IndexError, ValueError):
        print("Invalid or missing argument. Invoke like:")
        print(">> python <points>")
        print("Where <points> is the number of points used for approximation.")
