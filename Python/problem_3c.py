from problem_3b import Game
import pytest
import sys


def test_game():
    """
    Ugly test code to "verify" random output.
    Runs 10000 simulations with john and sarah, and verifies that the average is
    close to what it 'should' be.
    Invoke this like Game().
    """
    game_test = Game(5, 10)
    results = []
    for _ in range(10000):  # simulate 10K games
        game_test.play_game()
        assert game_test.john + game_test.sarah == 15
        results.append(game_test.winner == Game.JOHN)
        game_test.reset()
    assert sum(results) / 10000 - 5 / 15 < 0.05


if __name__ == "__main__":
    if "-test" in sys.argv:
        pytest.main(__file__)
    else:
        try:
            user_input = [int(sys.argv[i]) for i in (1, 2)]
        except (ValueError, IndexError):
            print("Invalid argument. Invoke like:")
            print(">>  python <john> <sarah> {-test}")
            print("To simulate a game.")
            print("Now running with default arguments (5, 10).")
            user_input = (5, 10)
        game = Game(*user_input)
        game.play_game()
        print("Given that john starts with {0} coins, and Sarah with {1}, "
              "{2} wins after {3} flips.".format(*user_input, game.winner_string, game.tosses))
