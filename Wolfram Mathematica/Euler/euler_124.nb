(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      2029,         72]
NotebookOptionsPosition[      1669,         55]
NotebookOutlinePosition[      2016,         70]
CellTagsIndexPosition[      1973,         67]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"radical", "[", "n_", "]"}], " ", ":=", " ", 
  RowBox[{"Apply", "[", 
   RowBox[{"Times", ",", " ", 
    RowBox[{"Map", "[", 
     RowBox[{"First", ",", " ", 
      RowBox[{"FactorInteger", "[", "n", "]"}]}], "]"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.661702670547312*^9, 3.6617026761392927`*^9}, {
  3.6617027584257245`*^9, 3.661702831796831*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"SortBy", "[", 
    RowBox[{
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"n", ",", " ", 
         RowBox[{"radical", "[", "n", "]"}]}], "}"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"n", ",", " ", "1", ",", " ", "100000"}], "}"}]}], "]"}], ",",
      " ", "Last"}], "]"}], "[", 
   RowBox[{"[", "10000", "]"}], "]"}], "[", 
  RowBox[{"[", "1", "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.6617028371688557`*^9, 3.661702958508665*^9}}],

Cell[BoxData["21417"], "Output",
 CellChangeTimes->{{3.6617028513670373`*^9, 3.661702899744413*^9}, {
  3.661702933946341*^9, 3.6617029606417847`*^9}}]
}, Open  ]]
},
WindowSize->{759, 601},
WindowMargins->{{Automatic, 270}, {-7, Automatic}},
FrontEndVersion->"10.3 for Microsoft Windows (64-bit) (December 10, 2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 390, 9, 31, "Input"],
Cell[CellGroupData[{
Cell[973, 33, 526, 15, 31, InheritFromParent],
Cell[1502, 50, 151, 2, 31, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

