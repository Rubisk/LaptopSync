(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     31852,        846]
NotebookOptionsPosition[     29909,        785]
NotebookOutlinePosition[     30256,        800]
CellTagsIndexPosition[     30213,        797]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"Radical", "[", "n_", "]"}], " ", ":=", " ", 
  RowBox[{"Apply", "[", 
   RowBox[{"Times", ",", " ", 
    RowBox[{"Map", "[", 
     RowBox[{"First", ",", " ", 
      RowBox[{"FactorInteger", "[", "n", "]"}]}], "]"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.662785062048809*^9, 3.6627850870539684`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"radicals", " ", "=", " ", 
  RowBox[{"SortBy", "[", 
   RowBox[{
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"n", ",", " ", 
        RowBox[{"Radical", "[", "n", "]"}]}], "}"}], ",", " ", 
      RowBox[{"{", 
       RowBox[{"n", ",", " ", "1", ",", " ", "120000"}], "}"}]}], "]"}], ",", 
    " ", "Last"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.6627850890695734`*^9, 3.6627851519974117`*^9}, {
  3.662785185393094*^9, 3.66278518689318*^9}}],

Cell[BoxData[
 InterpretationBox[
  TagBox[
   FrameBox[GridBox[{
      {
       ItemBox[
        TagBox[
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"1", ",", "1"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"2", ",", "2"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"4", ",", "2"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"8", ",", "2"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"16", ",", "2"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"32", ",", "2"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"64", ",", "2"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"128", ",", "2"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"256", ",", "2"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"512", ",", "2"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"1024", ",", "2"}], "}"}], ",", 
           TemplateBox[{"119978"},
            "OutputSizeLimit`Skeleton",
            DisplayFunction->(FrameBox[
              RowBox[{"\" \[CenterEllipsis]\"", #, "\"\[CenterEllipsis] \""}],
               Background -> GrayLevel[0.75], 
              BaseStyle -> {
               "Deploy", FontColor -> GrayLevel[1], FontSize -> Smaller, 
                ShowStringCharacters -> False}, BaselinePosition -> Baseline, 
              ContentPadding -> False, FrameMargins -> 1, FrameStyle -> 
              GrayLevel[0.75], RoundingRadius -> 7]& )], ",", 
           RowBox[{"{", 
            RowBox[{"119983", ",", "119983"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"119985", ",", "119985"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"119986", ",", "119986"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"119987", ",", "119987"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"119989", ",", "119989"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"119991", ",", "119991"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"119993", ",", "119993"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"119994", ",", "119994"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"119995", ",", "119995"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"119998", ",", "119998"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"119999", ",", "119999"}], "}"}]}], "}"}],
         Short[#, 5]& ],
        BaseStyle->{Deployed -> False},
        StripOnInput->False]},
      {GridBox[{
         {
          TagBox[
           TooltipBox[
            StyleBox[
             StyleBox[
              DynamicBox[ToBoxes[
                FEPrivate`FrontEndResource[
                "FEStrings", "sizeBriefExplanation"], StandardForm],
               ImageSizeCache->{65., {3., 9.}}],
              StripOnInput->False,
              DynamicUpdating->True], "SuggestionsBarCategory",
             StripOnInput->False],
            StyleBox[
             DynamicBox[
              ToBoxes[
               FEPrivate`FrontEndResource["FEStrings", "sizeExplanation"], 
               StandardForm]], DynamicUpdating -> True, StripOnInput -> 
             False]],
           Annotation[#, 
            Style[
             Dynamic[
              FEPrivate`FrontEndResource["FEStrings", "sizeExplanation"]], 
             DynamicUpdating -> True], "Tooltip"]& ], 
          ButtonBox[
           PaneSelectorBox[{False->
            StyleBox[
             StyleBox[
              DynamicBox[ToBoxes[
                FEPrivate`FrontEndResource["FEStrings", "sizeShowLess"], 
                StandardForm],
               ImageSizeCache->{52., {1., 9.}}],
              StripOnInput->False,
              DynamicUpdating->True], "SuggestionsBarControl",
             StripOnInput->False], True->
            StyleBox[
             StyleBox[
              DynamicBox[ToBoxes[
                FEPrivate`FrontEndResource["FEStrings", "sizeShowLess"], 
                StandardForm],
               ImageSizeCache->{52., {1., 9.}}],
              StripOnInput->False,
              DynamicUpdating->True], "SuggestionsBarControlActive",
             StripOnInput->False]}, Dynamic[
             CurrentValue["MouseOver"]],
            Alignment->Center,
            FrameMargins->0,
            ImageSize->{Automatic, 25}],
           Appearance->None,
           
           ButtonFunction:>OutputSizeLimit`ButtonFunction[
            Identity, 6, 26968416858232046017, 5/2],
           Enabled->True,
           Evaluator->Automatic,
           Method->"Queued"], 
          ButtonBox[
           PaneSelectorBox[{False->
            StyleBox[
             StyleBox[
              DynamicBox[ToBoxes[
                FEPrivate`FrontEndResource["FEStrings", "sizeShowMore"], 
                StandardForm],
               ImageSizeCache->{62., {1., 9.}}],
              StripOnInput->False,
              DynamicUpdating->True], "SuggestionsBarControl",
             StripOnInput->False], True->
            StyleBox[
             StyleBox[
              DynamicBox[ToBoxes[
                FEPrivate`FrontEndResource["FEStrings", "sizeShowMore"], 
                StandardForm],
               ImageSizeCache->{62., {1., 9.}}],
              StripOnInput->False,
              DynamicUpdating->True], "SuggestionsBarControlActive",
             StripOnInput->False]}, Dynamic[
             CurrentValue["MouseOver"]],
            Alignment->Center,
            FrameMargins->0,
            ImageSize->{Automatic, 25}],
           Appearance->None,
           
           ButtonFunction:>OutputSizeLimit`ButtonFunction[
            Identity, 6, 26968416858232046017, 5 2],
           Enabled->True,
           Evaluator->Automatic,
           Method->"Queued"], 
          ButtonBox[
           PaneSelectorBox[{False->
            StyleBox[
             StyleBox[
              DynamicBox[ToBoxes[
                FEPrivate`FrontEndResource["FEStrings", "sizeShowAll"], 
                StandardForm],
               ImageSizeCache->{44., {1., 9.}}],
              StripOnInput->False,
              DynamicUpdating->True], "SuggestionsBarControl",
             StripOnInput->False], True->
            StyleBox[
             StyleBox[
              DynamicBox[ToBoxes[
                FEPrivate`FrontEndResource["FEStrings", "sizeShowAll"], 
                StandardForm],
               ImageSizeCache->{44., {1., 9.}}],
              StripOnInput->False,
              DynamicUpdating->True], "SuggestionsBarControlActive",
             StripOnInput->False]}, Dynamic[
             CurrentValue["MouseOver"]],
            Alignment->Center,
            FrameMargins->0,
            ImageSize->{Automatic, 25}],
           Appearance->None,
           
           ButtonFunction:>OutputSizeLimit`ButtonFunction[
            Identity, 6, 26968416858232046017, Infinity],
           Enabled->True,
           Evaluator->Automatic,
           Method->"Queued"], 
          ButtonBox[
           PaneSelectorBox[{False->
            StyleBox[
             StyleBox[
              DynamicBox[ToBoxes[
                FEPrivate`FrontEndResource["FEStrings", "sizeChangeLimit"], 
                StandardForm],
               ImageSizeCache->{78., {1., 9.}}],
              StripOnInput->False,
              DynamicUpdating->True], "SuggestionsBarControl",
             StripOnInput->False], True->
            StyleBox[
             StyleBox[
              DynamicBox[ToBoxes[
                FEPrivate`FrontEndResource["FEStrings", "sizeChangeLimit"], 
                StandardForm],
               ImageSizeCache->{78., {1., 9.}}],
              StripOnInput->False,
              DynamicUpdating->True], "SuggestionsBarControlActive",
             StripOnInput->False]}, Dynamic[
             CurrentValue["MouseOver"]],
            Alignment->Center,
            FrameMargins->0,
            ImageSize->{Automatic, 25}],
           Appearance->None,
           ButtonFunction:>FrontEndExecute[{
              FrontEnd`SetOptions[
              FrontEnd`$FrontEnd, 
               FrontEnd`PreferencesSettings -> {"Page" -> "Evaluation"}], 
              FrontEnd`FrontEndToken["PreferencesDialog"]}],
           Evaluator->None,
           Method->"Preemptive"]}
        },
        AutoDelete->False,
        FrameStyle->GrayLevel[0.85],
        GridBoxDividers->{"Columns" -> {False, {True}}},
        GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
        GridBoxSpacings->{"Columns" -> {{2}}}]}
     },
     DefaultBaseStyle->"Column",
     GridBoxAlignment->{
      "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
       "RowsIndexed" -> {}},
     GridBoxDividers->{
      "Columns" -> {{False}}, "ColumnsIndexed" -> {}, "Rows" -> {{False}}, 
       "RowsIndexed" -> {}},
     GridBoxItemSize->{
      "Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
       "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[0.5599999999999999]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], 
         Offset[1.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}],
    Background->RGBColor[0.9657, 0.9753, 0.9802],
    FrameMargins->{{12, 12}, {0, 15}},
    FrameStyle->GrayLevel[0.85],
    RoundingRadius->5,
    StripOnInput->False],
   Deploy,
   DefaultBaseStyle->"Deploy"],
  Out[6]]], "Output",
 CellChangeTimes->{{3.6627851156147594`*^9, 3.662785156824319*^9}, 
   3.6627851926590014`*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"findABCHits", "[", "maximum_", "]"}], " ", ":=", " ", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
      "aIndex", ",", " ", "a", ",", " ", "aRad", ",", " ", "bIndex", ",", " ",
        "b", ",", " ", "bRad", ",", " ", "c", ",", " ", "radicals"}], "}"}], 
     ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"cSum", " ", "=", " ", "0"}], ";", "\[IndentingNewLine]", 
      RowBox[{"radicals", " ", "=", " ", 
       RowBox[{"SortBy", "[", 
        RowBox[{
         RowBox[{"Table", "[", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"n", ",", " ", 
             RowBox[{"Radical", "[", "n", "]"}]}], "}"}], ",", " ", 
           RowBox[{"{", 
            RowBox[{"n", ",", " ", "1", ",", " ", "maximum"}], "}"}]}], "]"}],
          ",", " ", "Last"}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"bIndex", " ", "=", " ", "1"}], ";", "\[IndentingNewLine]", 
      RowBox[{"While", " ", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"Last", "[", 
          RowBox[{"radicals", "[", 
           RowBox[{"[", "bIndex", "]"}], "]"}], "]"}], " ", "\[LessEqual]", 
         " ", 
         RowBox[{"maximum", " ", "/", " ", "2"}]}], ",", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"b", " ", "=", " ", 
          RowBox[{"First", "[", 
           RowBox[{"radicals", "[", 
            RowBox[{"[", "bIndex", "]"}], "]"}], "]"}]}], ";", 
         "\[IndentingNewLine]", 
         RowBox[{"aIndex", " ", "=", " ", "1"}], ";", "\[IndentingNewLine]", 
         RowBox[{"While", "[", 
          RowBox[{
           RowBox[{
            RowBox[{"Last", "[", 
             RowBox[{"radicals", "[", 
              RowBox[{"[", "aIndex", "]"}], "]"}], "]"}], "  ", "<=", " ", 
            RowBox[{"b", " ", "/", " ", 
             RowBox[{"Radical", "[", "b", "]"}]}]}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"c", " ", "=", " ", 
             RowBox[{"b", " ", "+", " ", 
              RowBox[{"First", "[", 
               RowBox[{"radicals", "[", 
                RowBox[{"[", "aIndex", "]"}], "]"}], "]"}]}]}], ";", 
            "\[IndentingNewLine]", 
            RowBox[{"If", "[", 
             RowBox[{
              RowBox[{
               RowBox[{
                RowBox[{"First", "[", 
                 RowBox[{"radicals", "[", 
                  RowBox[{"[", "aIndex", "]"}], "]"}], "]"}], " ", "<", " ", 
                "b"}], " ", "&&", " ", 
               RowBox[{"c", " ", "<", " ", "maximum"}], " ", "&&", " ", 
               RowBox[{
                RowBox[{"Radical", "[", "c", "]"}], " ", "<", " ", 
                RowBox[{"(", 
                 RowBox[{"c", " ", "/", " ", 
                  RowBox[{"(", 
                   RowBox[{
                    RowBox[{"Radical", "[", "b", "]"}], "*", 
                    RowBox[{"Last", "[", 
                    RowBox[{"radicals", "[", 
                    RowBox[{"[", "aIndex", "]"}], "]"}], "]"}]}], ")"}]}], 
                 ")"}]}]}], ",", "\[IndentingNewLine]", 
              RowBox[{"cSum", " ", "+=", " ", "1"}]}], "]"}], ";", 
            "\[IndentingNewLine]", 
            RowBox[{"aIndex", " ", "+=", " ", "1"}], ";", 
            "\[IndentingNewLine]", 
            RowBox[{"While", "[", 
             RowBox[{
              RowBox[{
               RowBox[{"GCD", "[", 
                RowBox[{
                 RowBox[{"Last", "[", 
                  RowBox[{"radicals", "[", 
                   RowBox[{"[", "aIndex", "]"}], "]"}], "]"}], ",", " ", 
                 RowBox[{"Radical", "[", "b", "]"}]}], "]"}], " ", ">", " ", 
               "1"}], ",", " ", 
              RowBox[{"aIndex", " ", "+=", " ", "1"}]}], "]"}], ";"}]}], 
          "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
         RowBox[{"bIndex", " ", "+=", " ", "1"}], ";"}]}], 
       "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
      RowBox[{"Return", "[", "cSum", "]"}], ";"}]}], "\[IndentingNewLine]", 
    "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.662785195315409*^9, 3.662785214015847*^9}, {
   3.6627853087774534`*^9, 3.6627853470221477`*^9}, {3.662785638433193*^9, 
   3.662785884186841*^9}, {3.66278606735157*^9, 3.662786082781534*^9}, {
   3.6627861317787714`*^9, 3.6627861427203827`*^9}, {3.6627861982964497`*^9, 
   3.662786334525166*^9}, {3.6627865606147685`*^9, 3.6627865614898195`*^9}, {
   3.6627866927188587`*^9, 3.662786694453334*^9}, 3.662786761575318*^9, {
   3.662786859534235*^9, 3.6627868896066055`*^9}, 3.662786945825144*^9, {
   3.6627872443910303`*^9, 3.662787253305025*^9}, {3.6627872889165535`*^9, 
   3.662787320717806*^9}, {3.6627873642791777`*^9, 3.6627874051106052`*^9}, {
   3.6627874741793613`*^9, 3.6627874761645575`*^9}, {3.66278762673104*^9, 
   3.662787658658718*^9}, {3.662787697999891*^9, 3.662787782780698*^9}, {
   3.662787975653328*^9, 3.662788045862426*^9}, {3.6627883209897757`*^9, 
   3.6627883363924346`*^9}, {3.662788482118761*^9, 3.662788546138606*^9}, {
   3.662788594354494*^9, 3.6627886477202363`*^9}, {3.6627887813364534`*^9, 
   3.6627888284279203`*^9}, {3.6627888932746058`*^9, 3.6627889169894195`*^9}}],

Cell[BoxData[
 RowBox[{"findABCHits", "[", "120000", "]"}]], "Input",
 CellChangeTimes->{{3.6627858725299315`*^9, 3.6627858762177677`*^9}, {
   3.6627865679879413`*^9, 3.662786576512697*^9}, {3.662786985300197*^9, 
   3.662787002021164*^9}, 3.6627877722738314`*^9, 3.662788000325347*^9, {
   3.662788239980873*^9, 3.6627882400902524`*^9}, {3.6627885226073394`*^9, 
   3.662788525138648*^9}, {3.662788567784766*^9, 3.662788601729789*^9}, {
   3.6627888980506544`*^9, 3.6627888981132603`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FactorInteger", "[", "507", "]"}]], "Input",
 CellChangeTimes->{{3.6627863915439205`*^9, 3.662786451437117*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"3", ",", "1"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"13", ",", "2"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.6627863948687644`*^9, 3.6627864518276386`*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"forceABCHits", "[", "maximum_", "]"}], " ", ":=", " ", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"a", ",", " ", "b"}], "}"}], ",", "\[IndentingNewLine]", " ", 
     RowBox[{
      RowBox[{"b", " ", "=", " ", "1"}], ";", "\[IndentingNewLine]", 
      RowBox[{"While", "[", 
       RowBox[{
        RowBox[{"b", " ", "\[LessEqual]", " ", "maximum"}], ",", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"a", " ", "=", " ", "1"}], ";", "\[IndentingNewLine]", 
         RowBox[{"While", "[", 
          RowBox[{
           RowBox[{"a", " ", "<", " ", "b"}], ",", "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"If", "[", 
             RowBox[{
              RowBox[{
               RowBox[{
                RowBox[{"Radical", "[", "a", "]"}], " ", "*", " ", 
                RowBox[{"Radical", "[", "b", "]"}], " ", "*", " ", 
                RowBox[{"Radical", "[", 
                 RowBox[{"a", " ", "+", " ", "b"}], "]"}]}], " ", "<", " ", 
               RowBox[{"a", " ", "+", " ", "b"}]}], ",", 
              "\[IndentingNewLine]", 
              RowBox[{"Print", " ", "[", 
               RowBox[{
               "a", ",", " ", "\"\< \>\"", ",", " ", "b", ",", "\"\< \>\"", 
                ",", " ", " ", 
                RowBox[{"a", " ", "+", " ", "b"}]}], "]"}]}], "]"}], ";", 
            "\[IndentingNewLine]", 
            RowBox[{"a", " ", "+=", " ", "1"}], ";", "\[IndentingNewLine]", 
            RowBox[{"While", "[", 
             RowBox[{
              RowBox[{
               RowBox[{"GCD", "[", 
                RowBox[{"a", ",", " ", "b"}], "]"}], " ", ">", " ", "1"}], 
              ",", " ", 
              RowBox[{"a", " ", "+=", " ", "1"}]}], "]"}], ";"}]}], 
          "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
         RowBox[{"b", " ", "+=", " ", "1"}], ";"}]}], "\[IndentingNewLine]", 
       "]"}], ";"}]}], "\[IndentingNewLine]", "]"}]}], 
  "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.6627870459335375`*^9, 3.6627871782436314`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"forceABCHits", "[", "1000", "]"}]], "Input",
 CellChangeTimes->{{3.6627871551847534`*^9, 3.6627871587788343`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "8", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "9"}],
  SequenceForm[1, " ", 8, " ", 9],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.6627871963978024`*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "5", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "27", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "32"}],
  SequenceForm[5, " ", 27, " ", 32],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.6627871964048247`*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "48", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "49"}],
  SequenceForm[1, " ", 48, " ", 49],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787196428381*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "32", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "49", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "81"}],
  SequenceForm[32, " ", 49, " ", 81],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787196431388*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "63", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "64"}],
  SequenceForm[1, " ", 63, " ", 64],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787196461962*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "80", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "81"}],
  SequenceForm[1, " ", 80, " ", 81],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787196514082*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "4", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "121", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "125"}],
  SequenceForm[4, " ", 121, " ", 125],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787196659933*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "3", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "125", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "128"}],
  SequenceForm[3, " ", 125, " ", 128],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.6627871966794863`*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "81", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "175", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "256"}],
  SequenceForm[81, " ", 175, " ", 256],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.6627871969496307`*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "224", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "225"}],
  SequenceForm[1, " ", 224, " ", 225],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.6627871972979755`*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "242", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "243"}],
  SequenceForm[1, " ", 242, " ", 243],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787197456882*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "2", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "243", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "245"}],
  SequenceForm[2, " ", 243, " ", 245],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.6627871974779043`*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "7", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "243", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "250"}],
  SequenceForm[7, " ", 243, " ", 250],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787197480916*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "13", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "243", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "256"}],
  SequenceForm[13, " ", 243, " ", 256],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787197483918*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "100", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "243", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "343"}],
  SequenceForm[100, " ", 243, " ", 343],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.6627871974874325`*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "288", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "289"}],
  SequenceForm[1, " ", 288, " ", 289],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.6627871978889856`*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "32", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "343", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "375"}],
  SequenceForm[32, " ", 343, " ", 375],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.6627871984984255`*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "169", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "343", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "512"}],
  SequenceForm[169, " ", 343, " ", 512],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.6627871985140224`*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "5", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "507", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "512"}],
  SequenceForm[5, " ", 507, " ", 512],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787200826658*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "512", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "513"}],
  SequenceForm[1, " ", 512, " ", 513],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787200936034*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "27", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "512", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "539"}],
  SequenceForm[27, " ", 512, " ", 539],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787200936034*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "200", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "529", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "729"}],
  SequenceForm[200, " ", 529, " ", 729],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.6627872012485576`*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "81", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "544", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "625"}],
  SequenceForm[81, " ", 544, " ", 625],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787201529848*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "49", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "576", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "625"}],
  SequenceForm[49, " ", 576, " ", 625],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.6627872021392317`*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "624", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "625"}],
  SequenceForm[1, " ", 624, " ", 625],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787203131029*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "104", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "625", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "729"}],
  SequenceForm[104, " ", 625, " ", 729],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787203146656*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "343", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "625", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "968"}],
  SequenceForm[343, " ", 625, " ", 968],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787203162282*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "675", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "676"}],
  SequenceForm[1, " ", 675, " ", 676],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787204256091*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "25", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "704", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "729"}],
  SequenceForm[25, " ", 704, " ", 729],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787204959256*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "728", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "729"}],
  SequenceForm[1, " ", 728, " ", 729],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787205536269*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "640", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "729", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "1369"}],
  SequenceForm[640, " ", 729, " ", 1369],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.662787205598804*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "960", 
   "\[InvisibleSpace]", "\<\" \"\>", "\[InvisibleSpace]", "961"}],
  SequenceForm[1, " ", 960, " ", 961],
  Editable->False]], "Print",
 CellChangeTimes->{{3.6627871690672293`*^9, 3.6627872124897885`*^9}}]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1366, 685},
WindowMargins->{{-8, Automatic}, {Automatic, -8}},
FrontEndVersion->"10.3 for Microsoft Windows (64-bit) (December 10, 2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 339, 8, 31, "Input"],
Cell[CellGroupData[{
Cell[922, 32, 499, 13, 31, "Input"],
Cell[1424, 47, 9693, 249, 131, "Output"]
}, Open  ]],
Cell[11132, 299, 5226, 111, 372, "Input"],
Cell[16361, 412, 491, 7, 31, "Input"],
Cell[CellGroupData[{
Cell[16877, 423, 136, 2, 31, "Input"],
Cell[17016, 427, 242, 7, 31, "Output"]
}, Open  ]],
Cell[17273, 437, 2111, 48, 292, "Input"],
Cell[CellGroupData[{
Cell[19409, 489, 138, 2, 31, "Input"],
Cell[CellGroupData[{
Cell[19572, 495, 312, 7, 23, "Print"],
Cell[19887, 504, 316, 7, 23, "Print"],
Cell[20206, 513, 314, 7, 23, "Print"],
Cell[20523, 522, 316, 7, 23, "Print"],
Cell[20842, 531, 314, 7, 23, "Print"],
Cell[21159, 540, 314, 7, 23, "Print"],
Cell[21476, 549, 318, 7, 23, "Print"],
Cell[21797, 558, 320, 7, 23, "Print"],
Cell[22120, 567, 322, 7, 23, "Print"],
Cell[22445, 576, 320, 7, 23, "Print"],
Cell[22768, 585, 318, 7, 23, "Print"],
Cell[23089, 594, 320, 7, 23, "Print"],
Cell[23412, 603, 318, 7, 23, "Print"],
Cell[23733, 612, 320, 7, 23, "Print"],
Cell[24056, 621, 324, 7, 23, "Print"],
Cell[24383, 630, 320, 7, 23, "Print"],
Cell[24706, 639, 322, 7, 23, "Print"],
Cell[25031, 648, 324, 7, 23, "Print"],
Cell[25358, 657, 318, 7, 23, "Print"],
Cell[25679, 666, 318, 7, 23, "Print"],
Cell[26000, 675, 320, 7, 23, "Print"],
Cell[26323, 684, 324, 7, 23, "Print"],
Cell[26650, 693, 320, 7, 23, "Print"],
Cell[26973, 702, 322, 7, 23, "Print"],
Cell[27298, 711, 318, 7, 23, "Print"],
Cell[27619, 720, 322, 7, 23, "Print"],
Cell[27944, 729, 322, 7, 23, "Print"],
Cell[28269, 738, 318, 7, 23, "Print"],
Cell[28590, 747, 320, 7, 23, "Print"],
Cell[28913, 756, 318, 7, 23, "Print"],
Cell[29234, 765, 324, 7, 23, "Print"],
Cell[29561, 774, 320, 7, 23, "Print"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

