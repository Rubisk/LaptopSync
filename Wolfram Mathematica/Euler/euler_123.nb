(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      2516,         80]
NotebookOptionsPosition[      2164,         63]
NotebookOutlinePosition[      2511,         78]
CellTagsIndexPosition[      2468,         75]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"findFirstAbove", "[", "min_", "]"}], " ", ":=", " ", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", "n", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"n", "  ", "=", " ", "1"}], ";", "\[IndentingNewLine]", 
     RowBox[{"While", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"Mod", "[", 
         RowBox[{
          RowBox[{"2", "n", " ", 
           RowBox[{"Prime", "[", "n", "]"}]}], ",", " ", 
          SuperscriptBox[
           RowBox[{"Prime", "[", "n", "]"}], "2"]}], "]"}], "\[LessEqual]", 
        " ", "min"}], ",", " ", 
       RowBox[{"n", " ", "+=", " ", "2"}]}], "]"}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"Print", "[", "n", "]"}], ";"}]}], "\[IndentingNewLine]", 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.6617018672075377`*^9, 3.661702126828794*^9}, {
  3.6617021578197603`*^9, 3.6617022027464757`*^9}, {3.661702238220829*^9, 
  3.6617023040448866`*^9}, {3.661702409737361*^9, 3.6617024274727364`*^9}, {
  3.661702483210188*^9, 3.6617025690588827`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"findFirstAbove", "[", 
  SuperscriptBox["10", "10"], "]"}]], "Input",
 CellChangeTimes->{{3.6617021285945206`*^9, 3.661702136641906*^9}, {
  3.6617025756686316`*^9, 3.661702575840518*^9}}],

Cell[BoxData["21035"], "Print",
 CellChangeTimes->{
  3.6617021692412124`*^9, 3.6617022112193*^9, 3.661702243017975*^9, 
   3.661702274394752*^9, 3.661702305498088*^9, {3.6617024119561515`*^9, 
   3.6617024291446524`*^9}, 3.66170249696097*^9, {3.6617025589957347`*^9, 
   3.6617025870129404`*^9}}]
}, Open  ]]
},
WindowSize->{759, 601},
WindowMargins->{{Automatic, 295}, {24, Automatic}},
FrontEndVersion->"10.3 for Microsoft Windows (64-bit) (December 10, 2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 1053, 25, 119, "Input"],
Cell[CellGroupData[{
Cell[1636, 49, 212, 4, 33, "Input"],
Cell[1851, 55, 297, 5, 23, "Print"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

