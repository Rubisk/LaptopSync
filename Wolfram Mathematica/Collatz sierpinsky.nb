(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      4644,        142]
NotebookOptionsPosition[      4214,        124]
NotebookOutlinePosition[      4557,        139]
CellTagsIndexPosition[      4514,        136]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"torec", "[", "n_", "]"}], " ", ":=", " ", 
  RowBox[{"Total", "[", 
   RowBox[{"MapIndexed", "[", 
    RowBox[{"ip", ",", " ", 
     RowBox[{"Reverse", "[", 
      RowBox[{"IntegerDigits", "[", 
       RowBox[{"n", ",", " ", "2"}], "]"}], "]"}]}], "]"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.689272941079164*^9, 3.6892729613187785`*^9}, {
  3.689272996457819*^9, 3.6892730164463263`*^9}, {3.6892731003446465`*^9, 
  3.6892731280615664`*^9}, {3.689273184556587*^9, 3.6892731871626806`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"ip", "[", 
   RowBox[{"n_", ",", " ", "i_"}], "]"}], " ", ":=", " ", 
  FractionBox["n", 
   SuperscriptBox["2", 
    RowBox[{
     RowBox[{"i", "[", 
      RowBox[{"[", "1", "]"}], "]"}], " ", "-", " ", "1"}]]]}]], "Input",
 CellChangeTimes->{{3.689273080003338*^9, 3.6892730952940006`*^9}, {
  3.689273132579985*^9, 3.6892731329392557`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"recTable", "[", "m_", "]"}], " ", ":=", " ", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{"torec", "[", "n", "]"}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"n", ",", " ", "1", ",", " ", 
      SuperscriptBox["2", "m"]}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.6892731205140543`*^9, 3.6892731422492485`*^9}, {
  3.689273209317134*^9, 3.689273225875193*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"Collatz", "[", "1", "]"}], "[", "n_", "]"}], " ", ":=", " ", 
  RowBox[{"If", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"Mod", "[", 
      RowBox[{"n", ",", " ", "2"}], "]"}], " ", "\[Equal]", " ", "0"}], ",", 
    " ", 
    FractionBox["n", "2"], ",", " ", 
    RowBox[{
     RowBox[{"3", "n"}], " ", "+", " ", "1"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.6892732640823116`*^9, 3.689273297861115*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"Collatz", "[", "k_", "]"}], "[", "n_", "]"}], " ", ":=", " ", 
  RowBox[{
   RowBox[{
    RowBox[{"Collatz", "[", "k", "]"}], "[", "n", "]"}], " ", "=", " ", 
   RowBox[{"If", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"Mod", "[", 
       RowBox[{"n", ",", " ", "2"}], "]"}], " ", "\[Equal]", " ", "0"}], ",", 
     " ", 
     RowBox[{
      RowBox[{"Collatz", "[", 
       RowBox[{"k", " ", "-", " ", "1"}], "]"}], "[", 
      FractionBox["n", "2"], "]"}], ",", " ", 
     RowBox[{
      RowBox[{"Collatz", "[", 
       RowBox[{"k", " ", "-", " ", "1"}], "]"}], "[", 
      RowBox[{
       RowBox[{"3", "n"}], " ", "+", " ", "1"}], "]"}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.689273300157874*^9, 3.689273339552847*^9}}],

Cell[BoxData[
 RowBox[{"Manipulate", "[", 
  RowBox[{
   RowBox[{"ListPlot", "[", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{"torec", "[", "n", "]"}], ",", " ", 
        RowBox[{"torec", "[", 
         RowBox[{
          RowBox[{"Collatz", "[", "k", "]"}], "[", "n", "]"}], "]"}]}], "}"}],
       ",", " ", 
      RowBox[{"{", 
       RowBox[{"n", ",", " ", "1", ",", " ", 
        RowBox[{
         SuperscriptBox["2", "12"], " ", "-", " ", "1"}], ",", " ", "2"}], 
       "}"}]}], "]"}], "]"}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"k", ",", " ", "1", ",", " ", "150", ",", " ", "1"}], "}"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.6892733551629686`*^9, 3.6892734387736845`*^9}, {
  3.689273472796155*^9, 3.689273537526968*^9}, {3.6892736034981394`*^9, 
  3.689273703195779*^9}, {3.689273811752472*^9, 3.689273811830474*^9}, {
  3.6892738897886934`*^9, 3.6892739051539187`*^9}, {3.6892739383592234`*^9, 
  3.689273958993202*^9}, {3.6892739900374503`*^9, 3.6892739900687*^9}, {
  3.689274048018526*^9, 3.6892740487630324`*^9}}]
},
WindowSize->{759, 833},
WindowMargins->{{Automatic, 572}, {64, Automatic}},
FrontEndVersion->"11.0 for Microsoft Windows (64-bit) (July 28, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 528, 11, 30, "Input"],
Cell[1089, 33, 378, 10, 46, "Input"],
Cell[1470, 45, 409, 10, 33, "Input"],
Cell[1882, 57, 456, 13, 44, "Input"],
Cell[2341, 72, 779, 22, 64, "Input"],
Cell[3123, 96, 1087, 26, 55, "Input"]
}
]
*)

(* End of internal cache information *)

