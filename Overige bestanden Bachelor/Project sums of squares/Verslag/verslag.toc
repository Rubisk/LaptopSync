\select@language {dutch}
\contentsline {chapter}{\numberline {1}Inleiding}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Artikel: Wanneer is $n^2$ een som van kwadraten?}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Pythagorese drietallen}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Getallen als som van 5 kwadraten}{3}{section.1.3}
\contentsline {chapter}{\numberline {2}Mathematica}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Pythagorese drietallen}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Getallen als som van 5 kwadraten}{5}{section.2.2}
\contentsline {chapter}{\numberline {3}Conclusie}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Pythagorese drietallen}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Getallen als som van 5 kwadraten}{7}{section.3.2}
