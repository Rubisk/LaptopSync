\documentclass{article}
\usepackage[dutch]{babel}
\usepackage[a4paper, total={7in, 10in}]{geometry}
% \usepackage{cite}
\usepackage{csquotes}
\usepackage[numbers]{natbib}
\usepackage{titlesec}
\usepackage[utf8]{inputenc}

\titleformat{\section}
  {\normalfont\scshape\Large}{\thesection}{1em}{}



\makeatletter
\def\thickhrulefill{\leavevmode \leaders \hrule height 1pt\hfill \kern \z@}
\renewcommand{\maketitle}{
    \let\footnotesize\small
    \let\footnoterule\relax
    \parindent \z@
    \reset@font
    \hrule height 1pt
    \vspace{5pt}
    \begin{minipage}[t]{0.62\textwidth}
    \begin{flushleft}
      \vfill
      \huge \@title
      \vfill
    \end{flushleft}
    \end{minipage}
    \vrule
    \begin{minipage}[t]{0.37\textwidth}
    \begin{flushright}
    Wiskunde als Wetenschap \\
     \today \\
     \@author
    \end{flushright}
    \end{minipage}
    \vspace{5pt}

    \hrule height 0.5pt
    \vspace{20pt}
 
  \setcounter{footnote}{0}%
}

\makeatother

\date{\today}
\begin{document}
\begin{titlepage} % Suppresses headers and footers on the title page
	
	\centering % Centre everything on the title page
	
	%------------------------------------------------
	%	Top rules
	%------------------------------------------------
	
	\rule{\textwidth}{1pt} % Thick horizontal rule
	
	\vspace{2pt}\vspace{-\baselineskip} % Whitespace between rules
	
	\rule{\textwidth}{0.4pt} % Thin horizontal rule
	
	\vspace{0.1\textheight} % Whitespace between the top rules and title
	
	%------------------------------------------------
	%	Title
	%------------------------------------------------
	
	
		{\Huge Bewijzen in de Wiskunde} % Title line 3
	
	\vspace{0.005\textheight} % Whitespace between the title and short horizontal rule
	
	\rule{0.3\textwidth}{0.4pt} % Short horizontal rule under the title
	
	\vspace{0.1\textheight} % Whitespace between the thin horizontal rule and the author name
	
	%------------------------------------------------
	%	Author
	%------------------------------------------------
	
	{\Large \textsc{Wouter Rienks}} % Author name
	
	\vspace{12pt}
	
	{\today}
	
	\vfill % Whitespace between the author name and publisher
	
	%------------------------------------------------
	%	Publisher
	%------------------------------------------------
	
	{\large\textsc{Wiskunde Als Wetenschap}} % Publisher
	
	\vspace{0.1\textheight} % Whitespace under the publisher text
	
	%------------------------------------------------
	%	Bottom rules
	%------------------------------------------------
	
	\rule{\textwidth}{0.4pt} % Thin horizontal rule
	
	\vspace{2pt}\vspace{-\baselineskip} % Whitespace between rules
	
	\rule{\textwidth}{1pt} % Thick horizontal rule
	
\end{titlepage}


{\scshape\tableofcontents}

\newpage
\section{De reden van het bewijzen}
De wiskunde is dat wat we zeker weten, is een veelgehoorde uitspraak. Sterker nog, het waren de allereerste woorden die ik hoorde in mijn allereerste college op de universiteit. Er wordt vaak gesproken alsof de wiskunde een soort sport is waarin iedereen met (grofweg) dezelfde aannames begint, en dan zijn best doet om zo veel mogelijk stellingen te bewijzen. We herinneren wetenschappers puur voor de stellingen die ze bewezen hebben, en de eerste drie jaar van de wiskundeopleiding bestaan voor het grootste deel uit het formuleren en bewijzen van stellingen. 

Ongeveer honderd jaar geleden is de wiskunde in korte tijd veel formeler en stricter geworden dan alle jaren daarvoor, en nu wordt er gedaan alsof het ontzettend belangrijk is om alles heel formeel en precies na te gaan.

Maar is de rol van het bewijzen wel echt zo belangrijk? Of is het soms prima om af te gaan op onze intuïtie? Als we kijken naar hoe wiskunde ontstaat, dan speelt het bewijzen maar een kleine rol in het ontwikkelen van een stelling. De ontwikkeling van een stelling gaat in fases. Eerst wordt er een verband of algemeen patroon opgemerkt, bijvoorbeeld ``de integraal van de afgeleide van een functie lijkt erg veel op de oorspronkelijke functie''. Daarna gaat de wiskundige op zoek naar een formulering voor de stelling.  Een conceptversie van de hoofdstelling van de calculus zou bijvoorbeeld kunnen zijn
\[
\int_0^x f'(s)  ds = f(x),
\]
deze uitspraak is immers waar voor alle functies $x^n$.
Daarna gaat hij op zoek naar tegenvoorbeelden, zoals bijvoorbeeld $(x + 1)^n$. De wiskundige herfomuleert de stelling steeds opnieuw totdat hij lijkt te kloppen voor alle mogelijke voorbeelden. 

En pas daarna begint het zoeken naar een bewijs. Maar ook hier wordt eerst met weinig strictheid gewerkt. Eerst wordt gezocht naar een intuïtief idee waarom de stelling waar zou kunnen zijn, bijvoorbeeld ``de toename van de oppervlakte onder $f'(s)$ rond $x$ is precies $f'(x)$, dus het is te verwachten dat 
\[
\frac{\partial}{\partial x} \int_0^x f'(s)  ds = f'(x).
\]
Pas daarna wordt tenslotte gezocht naar een formeel bewijs (en moeten wellicht de eisen van de stelling licht worden aangepast).

Het is dus helemaal niet logisch om wiskunde puur als de sport van het bewijzen te zien. Toch is het aan de andere kant wel belangrijk om een hoog niveau van strictheid vast te houden in de wiskunde. Als we immers alleen op onze intuïtie vertrouwen is het heel makkelijk om fouten te maken. 

Een goed voorbeeld is de hoorn van Gabriël, de figuur verkregen door de grafiek van $\frac{1}{x}$ met $x \in (0, \infty)$ te roteren om de $x$-as. In een eerste les calculus leren studenten technieken om de inhoud van deze figuur uit te rekenen, en deze blijkt precies $\pi$ te zijn, wat in het bijzonder eindig is. Op de vraag of de oppervlakte dan ook eindig moet zijn, is een veelgehoord antwoord onder calculusstudenten ``Ja! Ik kan immers precies $\pi$ liter verf in de hoorn gieten, en dan verf ik de hele binnenkant''. De uitleg is dan niet heel precies, toch klinkt hij heel overtuigend, en het is misschien verleidelijk te denken dat dit een bewijs is. Toch blijkt dat de oppervlakte van de hoorn oneindig is (de denkfout zit in het feit dat de laag verf steeds dunner wordt, maar om de oppervlakte te bepalen is een constante dikte van de verflaag nodig). Om dit soort denkfouten te voorkomen zou het toch wenselijk zijn om een strict systeem te hebben waarin alles stap voor stap is na te gaan.

In dit werkstuk gaan we op zoek naar wat de rol van het bewijzen in de wiskunde is, en wat deze zou moeten zijn. We gaan hiervoor kijken bij de wiskunde in de wereld van het onderzoek, het onderwijs, en uiteraard in de toepassingen. Tenslotte zullen we een blik vooruitwerpen en onderzoeken welke rol het bewijzen in de toekomst zou kunnen spelen.
\section{Wiskundeonderzoek}
In het wiskundeonderzoek is te verwachten dat de rol van het bewijzen het grootst is. De meeste wiskundigen hier zijn immers op zoek naar het bewijzen van nieuwe stellingen, dus daarvoor dus dan moeten ze toch wel heel precies werken?

Maar daar zit de eerste denkfout al in: Wiskundigen zijn lang niet altijd op zoek naar bewijzen voor nieuwe stellingen. Een groot deel van het onderzoek wordt besteed aan het opbouwen van structuren, en het vinden van definities zo dat we bepaalde patronen die voort komen uit onze intuïtie. 

Om dit toe te lichten stel ik voor dat we de stellingen in de wiskunde in twee typen stellingen indelen. De eerste vorm zou ik de `natuurlijke' stellingen willen noemen. Dit zijn de stelling die waar \emph{blijken} te zijn, en waarvan een bewijs vooral uit rekenwerk bestaat. Een goed voorbeeld hier is het zwakke vermoeden van Goldbach,  wat zegt dat elk oneven geheel getal groter dan 7 kan worden uitgedrukt als de som van drie oneven priemgetallen. Dit is iets wat inherent en misschien wel `toevallig' waar is. Deze waarheid ontstaat uit onze constructie voor het systeem van de natuurlijke getallen. Als hij echter niet waar was, zouden we niet gaan zoeken naar een andere constructie voor de natuurlijke getallen waarin de stelling wel waar was, we zouden het simpelweg jammer vinden en verder gaan met andere stellingen.

Aan de andere kant zijn er de stellingen waarvan we \emph{willen} dat ze waar zijn, omdat dat overeenkomt met onze intuïtie. Een goed voorbeeld hier is de stelling van Stokes. Voor deze stelling merkte men op dat de hoofdstelling van de calculus vaak ook te generaliseren was naar integralen in meerdere variabelen, en oorspronkelijk werden er simpele uitbreidingen in twee (Greene) of drie (Stokes) variabelen gevonden. Men wilde de stelling graag generaliseren, en om precies te kunnen uitdrukken op welke manier dat goed werkte is het idee van differentiaalvormen en de differentiaal ontwikkelt. De rekenregels en definities voor deze wiskundige objecten zijn in zekere zin zo geconstrueerd dat de stelling van Stokes er precies uit volgt. Het bewijs voor de stelling van Stokes is dan uiteindelijk ook niet meer interessant, de constructie van de objecten is veel belangrijker.

Voor de stellingen die waar blijken te zijn is het vinden van het bewijs het absolute doel. Van deze stellingen willen we graag weten waarom en of ze waar zijn, en is het bewijs van de stelling vaak het allerbelangrijkst. Toch is het ook bij deze stellingen niet altijd nodig met grote striktheid te werken. Zoals \citet[Volume II]{polya54} al probeerde uit te leggen is het ook hier verstandig op zoek te gaan naar overtuiging door middel van een heuristisch argument. Alhoewel dit natuurlijk geen echte zekerheid geeft, laat het ons vaak in ieder geval zien waarom deze stelling waar blijken te zijn. Volgens Polya helpen deze technieken om in ieder geval op zoek te gaan naar de juiste stellingen, maar ik zou graag nog een stap verder gaan en zeggen dat ze vaak en voor zekere vergezochte getaltheoriestellingen voldoende zijn. Wat meer overtuiging heeft men nodig voor het sterke vermoeden van Goldbach dan een computer die laat zien dat getallen al gauw op meer dan 10 manieren voldoen? Zelfs in het ondenkbare geval dat er toch een tegenvoorbeeld is, zijn het er hoogstwaarschijnlijk maar een paar, en die worden dan toch afgedaan als ``toevallig ging het voor deze kleine (eindige) getallen toch nog fout, maar in de limiet geldt het wel''.

Voor de stellingen waarvan we \emph{willen} dat ze waar zijn is het bewijs vaak een resultaat van onze constructies. Het doel van de wiskundige hier is het bouwen van een rigide systeem om onze intuïtie te kunnen formaliseren. Dan kunnen we daarna dit rigide systeem gebruiken om dingen uit te rekenen en ons nooit meer zorgen te hoeven maken over welke bewerkingen wel en niet zijn toegestaan. In deze gevallen zullen de bewijzen vaak (relatief) makkelijk volgen in vergelijkingen met het vinden van de goede definities en axioma's.  Toch is het juist hier heel belangrijk om heel precies te werken, omdat deze resultaten heel vaak gebruikt zullen gaan worden, dan wel in het voortbouwen van de wiskunde of in de toepassing van de wiskunde. Dit zijn de belangrijke resultaten waarom we de wiskunde creëren.

Al met al denk ik dat het in het onderzoek wel heel belangrijk is om de standaard van strictheid hoog te houden. Maar hier blijft het toch belangrijk om het doel niet uit het oog te verliezen. Wetenschappers moeten zich vooral bezighouden met het vinden van technieken om problemen op te lossen, onze intuïtie te kunnen verduidelijken, of ideeën uit verschillende vakgebieden te combineren. De strictheid biedt dan uiteindelijk een laatste controle om na te gaan dat alles daadwerkelijk klopt, en is alleen maar nodig als we de stellingen ook vaak gaan gebruiken. In alle andere gevallen is het zoeken naar een perfect bewijs misschien wel een verspilling van onderzoeksgeld.
\section{Onderwijs}
In het onderwijs is er altijd al een duidelijke lijn getrokken in de mate van strictheid. Op de basis- en middelbare school is er puur aandacht voor de methoden en technieken, en wordt er hooguit wat intuïtie gegeven als achtergrond voor de resultaten. \citet[hoofdstuk 21]{hardy40} ging zelfs zo ver dat hij het onderscheidde als \emph{elementaire} en \emph{echte} wiskunde. Die scheiding zou ik niet willen maken, het is immers prima mogelijk om studenten op dezelfde manier als op de middelbare school te leren een differentiaalvergelijking op te lossen, een integraal over een manifold uit te rekenen met behulp van differentiaalvormen, of een t-toets toe te passen op een verzameling gegevens. 

Het grote verschil zit erin dat op de universiteit studenten ook wordt aangeleerd wat een echt bewijs is, en dat daarna ook bij elk vak verwacht wordt dat studenten met een kritische blik naar de resultaten kijken, en zelf kunnen nagaan dat de resultaten ook echt kloppen. Dat draagt verder echter naar mijn mening weinig bij aan het begrijpen van de ideeën. Het verheldert vaak veel meer als een professor duidelijk opschrijft wat hij bedoelt, en dan de belangrijke stellingen gewoon geeft. Uiteindelijk is het belangrijk dat je begrijpt wat de resultaten betekenen en waar ze vandaan komen. Het komt maar zelden voor dat de belangrijke ideeën in de bewijzen zitten, en het lijkt er meer op dat de bewijzen worden aangeleerd om studenten te dwingen met een hoge mate van strictheid te werken.

\citet{haimo95} ging zelfs nog een stap verder,
\blockquote{As we enter an era of great change, we need to recognize and stress the distinct nature of mathematics that differentiates it from all other sciences. In particular, as we return, in our educational approach, to the historic emphasis on problem- solving, along with our current focus on experimentation and conjecture, we must be careful to appreciate the fact that, laudable as this approach is, it is a start, but not enough!}

en doet de suggestie dat we kinderen veel eerder leren wat het verschil is tussen een beredeneert bewijs en een empirisch bewijs. Naar mijn mening is dit verloren moeite, het logisch redeneren is immers een vaardigheid die vooral nodig is \emph{binnen} de wiskunde, daarbuiten zullen mensen toch vooral empirische bewijzen geven. Het behandelen van bewijzen in het middelbaar onderwijs kunnen wel van toegevoegde waarde zijn, maar moeten dan ook echt iets bijdragen aan het begrijpen van de begrippen, of ideeën daarachter. Denk bijvoorbeeld aan het bewijzen van de commutatiteit van vermenigvuldiging met behulp van een raster. Of een bewijs van de stelling van pythagoras door het schuiven van driehoeken. Dit zijn mooie, toegankelijke bewijzen die ook kunnen bijdragen aan het inzicht van leerlingen. Een (in enige mate formeel) bewijs van de productregel is echter puur algebraïsche manipulatie, en voegt niets behalve een les in logisch redeneren.

Ik zou graag zien dat we het basis en middelbaar onderwijs laten wat het is in termen van strictheid. In het universitair onderwijs zou ik als suggestie willen geven studenten leren hoe ze logisch kunnen redeneren, maar het ook los te laten in gevallen waarin het niet nodig is. Ik kan mij immers niet voorstellen dat veel studenten echt iets leren van een formeel bewijs van de centrale limietstelling, maar het is juist heel belangrijk om deze toe te passen. Volgens mij gaat er niets verloren als we in dat soort gevallen simpelweg zeggen ``dit is waar, en als je wilt weten waarom dan kun je dat hier nalezen'', zolang we ze maar aanleren wat een geldige logische redenatie is (en hier zoveel mogelijk mee te laten oefenen in opgaven bijvoorbeeld). 
\section{Toepassingen}
In eerste opzicht lijkt het erop alsof in de toepassingen van de wiskunde de strictheid voornamelijk losgelaten wordt. En dit is gedeeltelijk waar, voor veel technieken is het alleen belangrijk \emph{dat} ze werken, maar niet \emph{hoe} ze werken. Maar in zekere zin werken we daarom nog wel strict, er zijn immers onderzoekers geweest die van alle methoden hebben gecontroleerd hebben dat ze werken. 

Toch is het vaak genoeg niet nodig om dingen niet strikt te bewijzen. Bij het numeriek benaderen van een nulpunt van een functie is het voldoende om ``Newtoniteratie toe te passen tot het convergeert''. En alhoewel het theoretisch gezien nog steeds niet bewezen is dat we echt een goede benadering hebben gevonden als deze al 10 iteratiestappen met hooguit $10^{-5}$ verandert, toch hebben we een zekere praktische kennis dat het een goede benadering is, zoals 
\citet[sectie 1]{hersch15} ook al zei: \blockquote{
In brief, we are virtually compelled by the practicalities to accept the number that computation
seems to give us, even though, by the standards of rigorous logic, there is still an admitted possibility that
we may be mistaken. This computational result is a kind of mathematical knowledge! It is practical
knowledge, knowledge sound enough to be the basis of practical decisions about things like designing
bridges and airplanes—matters of life and death. 
}
En dit is prima, het is volkomen tijdverspilling om alles netjes na te gaan, de gegevens die je oorspronkelijk gebruikt zijn immers toch empirisch.

Maar er bestaat ook een andere soort van toepassingen waar striktheid wel heel belangrijk is, namelijk het bouwen van een model door de werkelijkheid te axiomatiseren. Deze uit zich bijvoorbeeld in Einsteins droom (\citet{dijkgraaf99}) over het formaliseren van de natuurkunde. We generaliseren hier, zoals \citet[pagina 13, laatste paragraaf]{hersch15} al zei, en laten wat strictheid los om veel meer te kunnen. Om in het dagelijks leven wat te kunnen moeten we immers wel, want elke situatie is nieuw. Empirie is heel belangrijk om wiskunde te kunnen toepassen, en daarmee ook een onderdeel van de wiskunde! Maar juist als we dit doen is het wel weer belangrijk om netjes alle stappen na te gaan en te bewijzen, anders verliezen we immers meteen de hele reden dat we het model geaxiomatiseerd hebben.

\section{Een blik in de toekomst}
In de afgelopen honderd jaar is de complexiteit van bewijzen ontzettend hard toegenomen. Denk bijvoorbeeld aan het bewijs van Wiles voor de laatste stelling van Fermat, of de classificatie van de eindige groepen. Deze stellingen nemen honderden pagina's in beslag, waarvan een groot deel bestaat uit redelijk algoritmisch nagaan van verschillende gevallen. Het lijkt mij dan ook niet meer dan aannemelijk dat het op een gegeven moment niet meer bij te houden wordt voor de mensheid om al deze gevallen steeds na te gaan. Dit is waarschijnlijk een probleem wat we in de komende eeuw moeten gaan oplossen. Misschien remt het de groei van de wiskunde nu al ontzettend maar zien we dat nog niet.

Toch is het loslaten van de striktheid niet de oplossing. Zoals ik in hoofdstuk 2 al beargumenteerd heb is het in veel gevallen wel belangrijk dat we precies nagaan aan welke eisen stellingen moeten voldoen. 
 
Ik hoop echter dat een andere techniek de oplossing zal geven, en dat is het gebruik van de computer als ondersteuning voor de wiskundige. Er wordt al onderzoek gedaan in deze richting, en de bekende oplossing van Appel en Haken voor het vierkleurenprobleem is natuurlijk een mooi voorbeeld. 
 
 De droom is dan dat computers de kleine stapjes overnemen, zoals \citet[sectie 5]{bundy11} al voorstelde, en de wiskundigen er blijven om de computers aan te sturen en de grote lijnen te bepalen. De grote lijnen zijn immers altijd het lastigst te bedenken en vereisen de meeste creativiteit, dus het is logisch om die aan mensen over te laten.
\section{Conclusie}
Alhoewel de bewijzen in de wetenschap, toepassingen en het onderwijs dus verschillende rollen moeten spelen, is er toch één grenschoverschrijdend principe. We zien steeds weer terug dat de bewijzen vooral een hulpmiddel moeten zijn, zoals \citet[sectie 5]{wilder44} al zei:
\blockquote{
 Now, granted that the mathematical theorem comes
 from the intuition, what is the role of proof? It seems to me to be only a testing
 process that we apply to these suggestions of our intuition. We have various kinds
 of tests, some of them, like the syllogism, substitution, or finite selection, universally relied upon, but others, such as those making use of the choice axiom
 or the total second ordinal class, not generally admitted. 
}
Het bewijzen is enkel een gereedschap dat we gebruiken om onszelf te overtuigen dat wat we doen correct is. Maar er is een veel groter plaatje dat we niet uit het oog moeten verliezen - het ontwikkelen van technieken, maar vooral het vinden en begrijpen van structuren. Dit laatste is het doel, niet het bewijzen op zich.

\nocite{*}
\bibliographystyle{plainnat}
\bibliography{bronnen} 
\end{document}