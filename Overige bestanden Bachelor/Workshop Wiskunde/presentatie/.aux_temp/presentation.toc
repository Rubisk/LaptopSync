\select@language {dutch}
\beamer@sectionintoc {1}{Overzicht Bewijs}{10}{0}{1}
\beamer@sectionintoc {2}{Vinden van een Waardering}{16}{0}{2}
\beamer@sectionintoc {3}{Kleuren van het Vierkant}{27}{0}{3}
\beamer@sectionintoc {4}{Oppervlakte van een (Regenboog)Driehoek}{41}{0}{4}
\beamer@sectionintoc {5}{Vinden van een Regenboogdriehoek}{59}{0}{5}
