% Beamer-template voor de Universiteit van Amsterdam
% Gemaakt door Jolien Oomens
% Gebaseerd op de TUD-template van Maarten Abbink
\documentclass{beamer}
\usepackage{calc}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usepackage{graphicx}


\usepackage{xcolor}

\tikzset{main node/.style={circle,fill=blue!20,draw,minimum size=1cm,inner sep=0pt},
            }
\usepackage[absolute,overlay]{textpos}
\mode<presentation>{\usetheme{uva}}
\tikzset{
  invisible/.style={opacity=0},
  visible on/.style={alt=#1{}{invisible}},
  alt/.code args={<#1>#2#3}{%
    \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
  },
}

\title[Bachelor Project]{Approximate Counting by Markov Simulation}
\subtitle{Supervisor: Viresh Patel}
\institute[UvA]{Universiteit van Amsterdam}
\author{Wouter Rienks}
\date{June 19th, 2018}

\begin{document}

{
\usebackgroundtemplate{\includegraphics[width=\paperwidth,height=\paperheight]{images/background-titlepage.jpg}}%
\frame{\titlepage}
}

\begin{frame}{Contents}
\begin{enumerate}
  \item Introduction to subject
  \item Generating Random Colourings
  \item Counting Colourings using Random Colourings
  \item Mixing Time
  \item Applications: Statistical Physics (if time permits)
  \item Overview
\end{enumerate}
\end{frame}


\begin{frame}{Introduction}
\begin{itemize}
  \item Approximate counting in graph theory
  \pause
  \begin{itemize}
  	\item Colourings
  	\item Independent Sets
  	\item ((Hamilton) Cycles, Matchings, etc.)
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{General Idea}
\begin{itemize}
  \item Define a random walk on the set of all objects
  \item With this random walk, sample (approximately) uniform random objects by simulating it for a while
  \item Use this sampling process to approximate the total number of objects
\end{itemize}
\end{frame}

\begin{frame}{Step 1: Generating Random Colourings}
\begin{figure}[h]
\begin{tikzpicture}
    \node[main node] (1) [scale=0.5] {$$};
    \node[main node] (2) [scale=0.5, left = 1.1cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\end{figure}
\pause
\begin{itemize}
  \item Colour this graph using 3 colours: \textcolor{red}{red}, \textcolor{blue}{blue} and \textcolor{green}{green}.
  \pause
  \item We can do so in six ways:
\end{itemize}

\begin{figure}[h]
\begin{tikzpicture}
    \node[main node] (1) [fill=red,scale=0.5] {$$};
    \node[main node] (2) [fill=green,scale=0.5, left = 0.4cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\qquad 
\begin{tikzpicture}
    \node[main node] (1) [fill=red,scale=0.5] {$$};
    \node[main node] (2) [fill=blue,scale=0.5, left = 0.4cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\\
\vspace{10pt}
\begin{tikzpicture}
    \node[main node] (1) [fill=green,scale=0.5] {$$};
    \node[main node] (2) [fill=red,scale=0.5, left = 0.4cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\qquad 
\begin{tikzpicture}
    \node[main node] (1) [fill=green,scale=0.5] {$$};
    \node[main node] (2) [fill=blue,scale=0.5, left = 0.4cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\\
\vspace{10pt}
\begin{tikzpicture}
    \node[main node] (1) [fill=blue,scale=0.5] {$$};
    \node[main node] (2) [fill=red,scale=0.5, left = 0.4cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\qquad 
\begin{tikzpicture}
    \node[main node] (1) [fill=blue,scale=0.5] {$$};
    \node[main node] (2) [fill=green,scale=0.5, left = 0.4cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\end{figure}
\end{frame}
\begin{frame}{Step 1: Generating Random Colourings}
\begin{itemize}
\item For bigger graphs, this gets much harder. We can instead define a random walk on all $k$-colourings by
\begin{enumerate}
  \item<2-> Select a vertex $v$ uniformly at random.
  \item<3-> Select a colour $c \in \{1, \dots, k\}$ uniformly at random.
  \item<4-> Attempt to recolour $v$ with colour $c$.
\end{enumerate}
\begin{center}
\begin{tikzpicture}
    \node[main node] (1) [scale=0.5] {$1$};
    \node[main node] (2) [scale=0.5, above left = 0.5 cm and 2.8cm of 1]  {$3$};
    \node[main node] (3) [scale=0.5, below left = 0.3cm and 1.6cm of 1, label={[visible on=<2->]v}]  {$2$};
    \node[main node] (4) [scale=0.5, below left = 0.8cm and 2.6cm of 1]  {$4$};
    \node[main node] (5) [scale=0.5, below left = 0.8cm and 0.8cm of 1]  {$1$};
    
    \path[draw,thick]
    (1) edge node {} (2)
    (3) edge node {} (4)
    (1) edge node {} (3)
    (2) edge node {} (4)
    (2) edge node {} (3)
    (3) edge node {} (5)
    (5) edge node {} (4);
\end{tikzpicture}
$k = 5$, \visible<4>{$c = 5$}
\end{center}
\end{itemize}
\end{frame}
\begin{frame}{Step 1: Generating Random Colourings}
\begin{itemize}
\item For bigger graphs, this gets much harder. We can instead define a random walk on all $k$-colourings by:
\begin{enumerate}
  \item Select a vertex $v$ uniformly at random.
  \item Select a colour $c \in \{1, \dots, k\}$ uniformly at random.
  \item Attempt to recolour $v$ with colour $c$.
\end{enumerate}
\begin{center}
\begin{tikzpicture}
    \node[main node] (1) [scale=0.5] {$1$};
    \node[main node] (2) [scale=0.5, above left = 0.5 cm and 2.8cm of 1]  {$3$};
    \node[main node] (3) [scale=0.5, below left = 0.3cm and 1.6cm of 1, label=v]  {$5$};
    \node[main node] (4) [scale=0.5, below left = 0.8cm and 2.6cm of 1]  {$4$};
    \node[main node] (5) [scale=0.5, below left = 0.8cm and 0.8cm of 1]  {$1$};
    
    \path[draw,thick]
    (1) edge node {} (2)
    (3) edge node {} (4)
    (1) edge node {} (3)
    (2) edge node {} (4)
    (2) edge node {} (3)
    (3) edge node {} (5)
    (5) edge node {} (4);
\end{tikzpicture}
$k = 5$, $c = 5$
\end{center}
\end{itemize}
\end{frame}
\begin{frame}{Step 1: Generating Random Colourings}
\begin{center}
\includegraphics{images/configuration_step}
\end{center}
\end{frame}

\begin{frame}{Step 2: From Sampling to Counting}
\begin{itemize}
\item Let $\Omega_k(G)$ be the number of $k$-colourings on a graph $G$ (the number we wish to estimate).
\pause
\item Let $m$ be the number of edges of $G$. Construct a sequence of subgraphs $G_0 \subseteq G_1 \dots \subseteq G_m = G$, where $G_i$ is obtained from $G_{i + 1}$ by removing an arbitrary edge (thus $G_0$ contains no edges). 
\pause
\item Let $n$ be the number of vertices of $G$. We know $\Omega_k(G_0) = k^n$. Thus to estimate $\Omega_k(G)$, we need only estimate each of the ratios $\Omega_k(G_{i + 1})/ \Omega_k(G_i)$. 
\end{itemize}
\end{frame}

\begin{frame}{Step 2: From Sampling to Counting}
\begin{itemize}
\item To estimate $\Omega_k(G_i) / \Omega_k(G_{i + 1})$, note that $\Omega_k(G_{i + 1}) \subseteq \Omega_k(G_i)$. 
\pause
\item Use our sampler to pick $s$ random colourings $C_1, \dots, C_s \in \Omega_k(G_{i})$. Let $Z$ be the number $i$ such that $C_i$ is also in $\Omega_k(G_i)$. 
\pause
\item Then $\frac{Z}{s}$ is a good estimator for $\Omega_k(G_i) / \Omega_k(G_{i + 1})$.
\end{itemize}
\end{frame}


\begin{frame}{The mixing time}
\begin{itemize}
\item What people are really interested in, is how long you need to simulate the random walk before you arrive at a truly random distribution.
\pause
\item This time is called the \emph{mixing time}, and the main interest of my project. 
\end{itemize}
\end{frame}


\begin{frame}{Applications: Statistical Physics}
\begin{itemize}
\item The Potts model is a physical model for magnetism. 
\pause
\item In a grid of particles, each particle is assigned one of $k$ spins (like ``+'' or ``-'' on a magnet). 
\pause
\item At low temperatures, particles are likely to have different spins from their neighbours. The spins all particles have are randomly distributed by a distribution.
\pause
\item One can then sample from this distribution using similar random walks as we discussed earlier.
\end{itemize}
\end{frame}

\begin{frame}{Project Overview}
\begin{center}
\includegraphics[scale=0.45]{images/table_of_contents}
\end{center}
\end{frame}

\begin{frame}{Thanks for your attention!}
\begin{center}
\includegraphics{images/configuration_step}
\end{center}
\end{frame}

\end{document}
