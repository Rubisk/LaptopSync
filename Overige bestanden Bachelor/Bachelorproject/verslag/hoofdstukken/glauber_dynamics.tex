% !TeX root = ../verslag.tex
In this section we provide a so-called fpras for the number of $k$-colourings of a graph $G$, whenever $k > 2\Delta(G)$. This algorithm was originally introduced by \citet{jerrum95}. We will write $\Omega_k(G)$ for the set of proper $k$-colourings on $G$, we thus wish to estimate $|\Omega_k(G)|$. We start off by describing precisely what we mean by a fast approximation algorithm.
\begin{definition}[FPRAS]
Let $k \in \NN$. A randomised approximation scheme for $k$-colourings is a probabilistic algorithm that takes as input a graph $G$ and some $\varepsilon \in (0, 1)$, and outputs a random number $Y$ such that 
\[
\PP\((1 - \varepsilon)|\Omega_K(G)| \leq Y \leq (1 + \varepsilon)|\Omega_k(G)|\) \geq \frac{3}{4}.
\]
A randomised approximation scheme is called fully polynomial if it runs in time polynomial in $|G|$ and $\varepsilon^{-1}$. A fully polynomial randomised approximation scheme will be abbreviated as fpras. 
\end{definition}
\begin{remark}\label{rem:three_quarters}
The constant $\frac{3}{4}$ has no significance beyond lying strictly between $0$ and $\frac{1}{2}$. If we repeat the experiment $N$ times and take the median of the results, then the median $M$ is inside the interval $I = \((1 - \varepsilon)|\Omega_K(G)|,  (1 + \varepsilon)|\Omega_k(G)|\)$ whenever at least $N / 2$ of the results are. Therefore, if the probability of a result ending in $I$ is at least $\frac{3}{4}$, then the probability of less than $N / 2$ results ending in $I$ is at most the probability of a r.v. with distribution $\text{Bin}\(N, \frac{1}{4}\)$ being at least $\frac{N}{2}$. Thus if we let $Z \sim \text{Bin}(N, \frac{1}{4})$ be a random variable distributed accordingly, then we can bound the probability of $M$ lying outside $I$ using the \citet[Theorem 2]{hoeffding63} inequality by
\[
\PP(M \not \in I) \leq \PP\(Z \geq \frac{N}{2}\) \leq \PP\(Z - \EE[Z] \geq \frac{N}{4}\) \leq e^{-\frac{N^3}{8}}.
\]
From this we see that we can get a success probability $p$ arbitrarily close to one by simulating $\lceil (-8\log(1 - p))^\frac{1}{3} \rceil$ times and taking the median of the results.
\end{remark}
We now wish to find such an fpras. As said before, we will start by constructing a Markov chain on the space of all proper $k$-colourings to sample $k$-colourings almost-uniformly.
\begin{definition}[Glauber Dynamics]
Let $G$ be a graph and $k \in \NN$. Consider a Markov chain $(X_t)$ with state space $\Omega = \Omega_k(G)$ the set of all proper $k$-colourings on $G$, whose transition probabilities from state $X_t$ are described by the following procedure:
\begin{enumerate}
\item Pick a vertex $v \in V(G)$ and a colour $c \in \{1, \dots, k\}$ uniformly at random. 
\item Recolour vertex $v$ with colour $c$. If the resulting colouring $X'$ is a proper colouring, let $X_{t + 1} = X'$, otherwise let $X_{t+1} = X_t$.
\end{enumerate}
This process is often referred to as the \emph{Glauber Dynamics} (which originates from statistical physics). It turns out that this chain is almost always ergodic, as shown in the following proposition.
\end{definition}
\begin{proposition}\label{glauberergodic}
Let $G$ be a graph and $k \in \NN$. The Glauber Dynamics  Markov chain is ergodic whenever $k \geq \Delta(G) + 2$. 
\end{proposition}
\begin{proof}
By Theorem \ref{ergodictheorem}, it suffices to show the chain is aperiodic and irreducible. To see that it is aperiodic, note that given a coloring $\sigma$ and some $v \in V(G)$, attempting to recolour $v$ with colour $\sigma(v)$ will make the chain stay at colouring $\sigma$, therefore $P(\sigma, \sigma) > 0$ for all $\sigma \in \Omega$, hence the chain is aperiodic. To see that the chain is irreducible, note that we can travel from any colouring $\sigma$ to any colouring $\tau$ by enumerating the vertices of $G$ in some way, say $v_1, \dots, v_n$, and recolouring the $v_i$ from $\sigma(v_i)$ to $\tau(v_i)$ in order. It might be necessary to recolour some $v_j$ with $j > i$ along the way, but we always have a spare colour to do so since $k \geq \Delta(G) + 2$ (note that any $v_j$ with $j < i$ will never need to be recoloured since $\tau$ is a proper colouring). 
\end{proof}
If $k = \Delta(G) + 1$ it need not be ergodic, consider for example the graph containing two vertices and a single edge, if both are given a different colour then $k = 2, \Delta(G) = 1$ but it's impossible to swap the colours using the Glauber Dynamics Markov Chain.

It turns out that this chain is in fact rapidly mixing provided $k > 2\Delta(G)$.
\begin{theorem}\label{glaubermixing}
Let $G$ be a graph and $k \in \NN$. Provided $k > 2\Delta(G)$, the mixing time of the Glauber dynamics with $k$ colours on $G$ is bounded above by
\[
\tau(\varepsilon) \leq \frac{k}{k - 2\Delta}n\log(n \varepsilon^{-1}).
\]
\end{theorem}
We will prove this later on, in Corollary \ref{glaubermixingproof}. For now, we will use this bound on the mixing time to construct an fpras for approximating the number of $k$-colourings of a graph. 
\begin{theorem}[Counting $k$-colourings rapidly]\label{th:counting_colourings}
Let $G$ be graph and $k \in \NN$. If $k$ is such that $k > 2\Delta(G)$, then there exists an fpras for counting the number of $k$-colourings on $G$. The time complexity of this algorithm can be bounded above by
\[
\frac{50k}{k - 2\Delta}\times \frac{nm^2}{\varepsilon^2}\log\(\frac{4nm}{\varepsilon}\),
\]
where $m$ is the number of edges in $G$ and the time unit is a single simulation step of the Markov chain $(X_t)$ described above.
\end{theorem}
\begin{proof}
Our proof is entirely thanks to \citet{jerrum95}. Let $G_0 \subseteq G_1 \subseteq \dots \subseteq G_m$ be a sequence of subgraphs of $G$ in which $G_m = G$ and each $G_i$ is obtained from $G_{i + 1}$ by removing a single edge. Then clearly, since $|\Omega_k(G_0)| = k^n$, we have
\[
|\Omega_k(G)| = k^n \prod_{i = 1}^m \frac{|\Omega_k(G_i)|}{|\Omega_k(G_{i - 1})|}.
\]
Our goal is to estimate each of the ratios $\rho_i := |\Omega_k(G_i)| / |\Omega_k(G_{i - 1})|$ individually and combine those estimates to estimate $|\Omega_k(G)|$. 

We first bound the $\rho_i$ away from $0$, which will be needed later on. Suppose the graphs $G_{i - 1}$ is obtanied from $G_i$ by removing the edge $\{u, v\}$. Clearly $\Omega_k(G_{i}) \subseteq \Omega_k(G_{i - 1})$. Any colouring in $\Omega_k(G_{i - 1}) \setminus \Omega_k(G_i)$ assigns the same colour to $u$ and $v$, and may thus be changed to be a colouring in $\Omega_k(G_i)$ by recolouring vertex $u$ with one of at least $k - \Delta \geq \Delta + 1$ colours. On the other hand, each colouring of $\Omega_k(G_i)$ can be obtained in at most one way as the result of such a pertubation. Therefore
\[
|\Omega_k(G_{i - 1}) \setminus \Omega_k(G_i)| \leq \frac{1}{\Delta + 1} \Omega_k(G_i),
\]
and thus (note $k > 1$ hence $\Delta \geq 2$, the case $k = 1$ is trivial anyway)
\begin{equation}\label{rhoivergelijking}
\frac{3}{4} \leq \frac{\Delta + 1}{\Delta + 2} \leq \rho_i \leq 1.
\end{equation}
Now continue by defining 
\[
T = \left \lceil \frac{k}{k - 2\Delta} n \log\( \frac{4nm}{\varepsilon}\)\right \rceil.
\]
Let $C_i \in \{0, 1\}$ denote the random variable obtained by simulating the Glauber Dynamics Markov Chain with $k$ colours from an arbitrary fixed initial state $c_i \in \Omega_k(G_{i - 1})$ for $T$ steps and returning the final colouring. Then $C_i$ has distribution $P_i^T(c_i, \cdot)$, where \linebreak $P_i: \Omega_k(G_{i - 1}) \to \Omega_k(G_{i - 1})$ is the transition matrix for the Glauber Dynamics Markov Chain on $\Omega_k(G_{i - 1})$. Set $Z_i$ to be the random variable given by
\[
Z_i = \begin{cases}
0 \text{ if } C_i \not \in \Omega_k(G_i), \\
1 \text{ if } C_i \in \Omega_k(G_i).
\end{cases}
\]
Let $\mu_i = \EE Z_i$. The idea is that $Z_i$ is almost $\text{Bernoulli}(\rho_i)$-distributed. In fact, we have
\begin{align*}
|\mu_i - \rho_i| &= \left | \sum_{y \in \Omega_k(G_i)}P^T(c_i, y) - \sum_{y \in \Omega_k(G_i)} \frac{1}{\Omega_k(G_{i - 1})} \right | \\
&= \left | P^T(c_i, \Omega_k(G_i)) - \pi_i(\Omega_k(G_i)) \right | 
\end{align*}
where $\pi_i$ is the uniform (limit) distribution of $P_i$.
 
By Theorem \ref{glaubermixing} (inserting $\varepsilon = \varepsilon / 4m$, and using the global variation bound with $S = \Omega_k(G_i)$) we thus have
\[
\rho_i - \frac{\varepsilon}{4m} \leq \mu_i \leq \rho_i + \frac{\varepsilon}{4m}.
\]
Using equation \ref{rhoivergelijking} it follows that
\begin{equation}\label{mboundglauber}
\(1 - \frac{\varepsilon}{3m}\) \rho_i \leq \mu_i \leq \(1 + \frac{\varepsilon}{3m}\) \rho_i.
\end{equation}
Thus the mean of a sufficiently large number of independent copies of $Z_i$ will provide a good estimate of $\rho_i$. Set
\[
s := \lceil 37\varepsilon^{-2}m\rceil,
\]
we will see this choice is sufficiently large later on. Let $Z_i^{(1)}, \dots, Z_i^{(s)}$ be a sequence of i.i.d. copies of $Z_i$. Let $\overline{Z_i}$ denote their mean. Finally, let $Y := k^n \prod \overline{Z_i}$ be our estimator for $|\Omega_k(G_i)|$, since all $\overline{Z_i}$ are independent we have $\EE[Y] = k^n \prod \mu_i$. Since we already know the mean of $Y$ to be close to our actual answer (see \ref{mboundglauber}), if $Y$ has low variance as well we would expect the estimator $Y$ to end up near the actual answer with high probability. 

For convenience, we will bound the variance of $k^{-n}Y$ instead. We can do so by 
\begin{align*}
\frac{\Var \(\prod_{i = 1}^m \overline{Z_i}\)}{\prod_{i = 1}^m \mu_i^2} &= \frac{\EE\left [\prod_{i = 1}^m \overline{Z_i^2} \right ] - \prod_{i = 1}^m \mu_i^2}{\prod_{i = 1}^m \mu_i^2} \\
 &= \frac{\prod_{i = 1}^m\EE\left [ \overline{Z_i^2} \right ]}{\prod_{i = 1}^m \mu_i^2} - 1 &(\overline{Z_i} \text{ are independent})\\
&= \prod_{i = 1}^m \(1 + \frac{\Var \overline{Z_i}}{\mu_i^2}\) - 1. &(\Var \overline{Z_i} + \mu_i^2 = \EE[Z_i^2])
\end{align*}
Thus to bound the variance of $Y$ we need to bound $\mu_i^{-2}\Var \overline{Z_i ^2}$. Since $Z_i$ takes values in $\{0, 1\}$, by conditioning on both outcomes we see that for all $i \in \{1, \dots, s\}$ we have
\[
\mu_i^{-2}\Var Z_i = \mu_i^{-2}(\mu_i(1 - \mu_i)^2 + (1 - \mu_i)\mu_i^2) = \mu_i^{-1} - 1.
\]
We thus need to bound $\mu_i$ away from $0$. But we can do this, since by equations \ref{rhoivergelijking}, \ref{mboundglauber} we have $\mu_i \geq \frac{1}{2}$. Hence $\mu_i^{-2} \Var Z_i \leq 1$, and $\mu_i^{-2} \Var(\overline{Z_i^2}) \leq s^{-1}$. Substituting this in our original calculation, we obtain
\begin{align*}
\Var(k^{-n} Y) &\leq \(1 + \frac{1}{s}\)^m - 1 \\
&\leq \text{exp}\(\frac{\varepsilon^2}{37}\) - 1 \leq \frac{\varepsilon^2}{36}.
\end{align*}
By Chebyshev's inequality we conclude that (we use that $\Var(k^{-n}Y)^{\frac{1}{2}} \leq \frac{\varepsilon}{6}\prod_{i = 1}^m \mu_i$ and $\EE[k^{-n}Y] = \prod_{i = 1}^m \mu_i$)
\[
\PP\(\left | k^{-n} Y - \prod_{i = 1}^m \mu_i \right | < \frac{\varepsilon \alpha}{6} \prod_{i = 1}^m \mu_i\) \geq 1 - \frac{1}{\alpha^2}
\]
holds for any $\alpha > 0$. By setting $\alpha = \frac{1}{2}$ it follows that
\[
\PP\(\(1 - \frac{\varepsilon}{3}\)\prod_{i = 1}^m \mu_i \leq k^{-n} Y \leq \(1 + \frac{\varepsilon}{3}\)\prod_{i = 1}^m \mu_i\) \geq \frac{3}{4}.
\]
From equation \ref{mboundglauber}, we see using the fact that $(1 - \varepsilon /3m)^m \geq (1 - \varepsilon / 2)$ for $m \geq 1$ that 
\[
\(1 - \frac{\varepsilon}{2}\)\prod_{i = 1}^m \rho_i \leq \prod_{i = 1}^m \mu_i \leq \(1 + \frac{\varepsilon}{2}\)\prod_{i = 1}^m \rho_i.
\]
Combining these two inequalities implies the estimator $Y$ satisfies the required bounds. 

Since the computation of the estimator requires $ms$ simulations, each with $T$ steps, we need $msT$ steps in total. The constant factor 50 is chosen to absorb the ceiling functions.
\end{proof}