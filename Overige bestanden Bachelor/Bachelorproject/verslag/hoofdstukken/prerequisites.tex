% !TeX root = ../verslag.tex
In this chapter we introduce some notation and state some previously known theorems. Throughout this thesis, we will often consider a discrete time Markov chain on some state space $\Omega$. We will often write $P$ for its transition matrix. Given $x, y \in \Omega$, we will write $P^t(x, y)$ for the probability of moving from $x$ to $y$ in $t$ steps of the Markov chain. We say that $x$ and $y$ are \emph{adjacent} if $P^t(x, y) > 0$. 

Given a distrubition $\pi$ on $\Omega$, we write $P^t(\pi)$ for the distribution obtained after simulating the Markov chain for $t$ steps with initial distribution $\pi$. We will often write $P^t(x, \cdot)$ where $x \in \Omega$ for the distribution obtained after simulating the Markov chain for $t$ steps starting at $x$.

We say that the Markov chain is \emph{irreducible} if for all $x, y \in \Omega$ there exists some $t \in \NN$ such that $P^t(x, y) > 0$.

We say that a distribution $\pi$ is an \emph{invariant distribution} if $P\pi = \pi$. Recall that if an invariant distribution exists and the Markov chain is irreducible, it is in fact unique. Furthermore, we have the following result.
\begin{proposition}[Detailed Balance]\label{detailedbalance}
Let $P$ be the transition matrix of a Markov chain with state space $\Omega$. If $\pi$ is some distribution on $\Omega$ satisfying
\begin{align*}
\pi(x)P(x, y) &= \pi(y)P(y, x) &\forall x, y \in \Omega
\end{align*}
then $\pi$ is an invariant distribution of the Markov chain.
\end{proposition}
Note that if $P$ is symmetric and irreducible, then $\pi$ is the uniform distribution.

We say that the Markov chain is $\emph{aperiodic}$ if $P^t(x, x) > 0$ for all $x \in \Omega$ and $t \in \NN$ sufficiently large. This is equivalent to saying that $\gcd\{t \mid   P^t(x, x) > 0\} = 1$. This allows us to state the following general theorem about the limiting behaviour of Markov chains.
\begin{theorem}[Ergodic Theorem]\label{ergodictheorem}
Suppose $P$ is a transition matrix for some irreducible and aperiodic Markov chain. Then there exists a \emph{unique} limiting distribution $\pi$, and for any other distribution $\pi'$ we have for any $x \in \Omega$ that
\[
\lim_{t \to \infty} P^t (\pi')(x) = \pi(x).
\]
\end{theorem}
If we are in the situation of the Theorem \ref{ergodictheorem}, we say that the Markov chain is \emph{ergodic}.
