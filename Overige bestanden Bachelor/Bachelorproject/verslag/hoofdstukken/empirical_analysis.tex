% !TeX root = ../verslag.tex
In this chapter we try and analyze the approximation scheme for counting colourings empirically, to see how good it actually performs in practice. An implementation of the algorithm described in Theorem \ref{th:counting_colourings} is given (in C++) in the appendix, see file \ref{cd:colouring_approximator}. 

Unfortunately, we can't provide a good analysis of the mixing speed of the chain, as that would require us to perform calculations that scale exponentially in either $n$ or $\varepsilon$. Instead, we will verify that thee output of the algorithm agrees with the exact number of colourings, or at least doesn't vary too much between simulations.

\section{Single algorithm simulations on cyclic graphs}
We start off by running the algorithm on a cyclic graph, where we can find the exact answer using the chromatic polynomial. Recall that the chromatic polynomial of a cycle on $n$ vertices is given by
\[
\chi_{C_n}(k) = (k - 1)^n + (k - 1)(-1)^n.
\]

The following table shows some results of running the algorithm once, with $k = 6$, $\varepsilon = 0.1$ and minimum succes probability $\frac{3}{4}$, for various $n$. 

\begin{table}[htb]
\centering
\caption{Approximating colouring count, $k = 6$, $\varepsilon = 0.1$}
\begin{tabular}{cccccc}
\hline
n & 6 & 7 & 8 & 9  & 10  \\ \hline
Exact Answer & 15630 & 78120 & 390630 & 1953120 & 9803980 \\
Approximation & 15738 &  78160&  386467& 1963260  &  9765630
\end{tabular}
\end{table}

We see that in this case, the algorithm performs about ten times better than expected. The error margin is usually about $0.01$ instead of the $0.1$ it should be in three out of four times. 

We can't let $k$ be much smaller, but as the running time is decreasing in $k$ we can actually try this for very high $k$. This isn't a very interesting case, as for big $k$ the approximation $k^n$ gets really good anyway (as for $k$ much bigger than $n$, most colourings will be proper anyway). Nevertheless, we will still try it (fix $n = 10$ this time).

\begin{table}[htb]
\centering
\caption{Approximating colouring count, $n = 10$, $\varepsilon = 0.1$}
\begin{tabular}{cccccc}
\hline
k & 6 & 20 & 40    \\ \hline
Exact Answer & 1953120 &6131066257820 & 8140406085191640  \\
Approximation & 1963260 &  6058320000000 & 8104130000000000

\end{tabular}
\end{table}

Once again we see that the relative error bound remains at most $0.01$ instead of the $0.1$. (The reason there are so many trailing zeroes is because we're using the terminal to transfer information from C++ to python, and as C++ outputs floats with 6 digits in scientific notation we end up with only six digits of the actual answer, which is good enough for our tests here).

It is most interesting to vary epsilon (to see if the algorithm keeps performing about ten times better), but as the running time is square in $\varepsilon$ we can't do many of those simulations. Furthermore, running the algorithm once doesn't really give a good idea about the general distribution of outputs the algorithm can give. We will therefore simulate the algorithm repeatedly in the next sections.

\section{Repeated algorithm simulations on a cyclic graph}
The previous section seems to suggest the algorithm performs a lot better than expected, i.e. the bounds in Theorem \ref{th:counting_colourings} can be improved. To get a good idea of what the output distribution of the algorithm looks like and varies as $\varepsilon$ varies, we will now vary $\varepsilon$ and perform 100 simulations for each $\varepsilon$.

The following picture shows the output of these simulations.
\begin{center}
\includegraphics[scale=0.5]{pictures/colouring_cyclic_picture}
\end{center}
The long middle line is the actual answer (390630). The blue dots are the individual simulation results. The outer black lines are $(1 + \varepsilon)$ and $(1 - \varepsilon)$ times the median, and the short inner black lines are the median. The red lines indicate the middle $75 \%$ of the data, which is the only part we should be interested in as we have a succes probability of $\frac{3}{4}$. 


\section{Repeated algorithm simulations on a regular graph}
We can do the same thing for a regular graph, except that we don't know the exact answer. We can thus only see how much it varies between different simulations.

The following picture shows the output of these simulations on a $3$-regular graph with 6 vertices, when $k = 7$.
\begin{center}
\includegraphics[scale=0.5]{pictures/colouring_regular_picture}
\end{center}
Note that there is no long middle line, as we don't know the actual answer. This picture seems to suggest that our approximation is very good, as the algorithm output doesn't vary too much. However, even if the algorithm were to output the same number each time, it doesn't need to be the exact answer. The problem is that our estimator is biased (all we know is that $\EE[Y]$ is within $((1 - \varepsilon)|\Omega_k(G), (1 + \varepsilon)|\Omega_k(G)|$). 

However, the median of the results doesn't seem to vary as $\epsilon$ decreases. And if we let $\varepsilon \to 0$ the algorithm does start providing better approximations. In fact, we know for sure that $\lim_{\varepsilon \to 0} \EE[Y(\varepsilon)] = \Omega_k(G)$. So this at least suggests that the algorithm does in fact perform a bit better then the bounds provided in Theorem \ref{th:counting_colourings}.