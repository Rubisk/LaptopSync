% !TeX root = ../verslag.tex
In this section we aim to discuss the speed of convergence of a Markov chain to its limiting distribution. The fewer steps a Markov chain will need to get close to its limiting distribution, the faster its mixing time will be. Having a Markov chain converge to its limiting distribution fast is really useful, because it gives us a way to sample from its limiting distribution almost-uniformly, by simulating the chain for not too many steps. This will in turn give us a way to approximate the size of the state space, as we will see by means of an example in the next section. 

We start by making more precise what we mean by ``almost-uniformly'', by defining a metric on the set of all probability distributions on the state space.
\begin{definition}
Given two probability distributions $\pi_1, \pi_2$ on a finite set $\Omega$, we define the \emph{total variation distance} between $\pi_1$ and $\pi_2$ to be
\[
d_{\text{TV}}(\pi_1, \pi_2) = \max_{S \subseteq \Omega}|\pi_1(S) - \pi_2(S)|.
\]
\end{definition}
It follows immediately from the next remark that this is in fact a metric.
\begin{remark}\label{rem:tv_distance}
For any set $S \subseteq \Omega$, denote temporarily $d(S) := |\pi_1(S) - \pi_2(S)|$ and then define the sets $S^+ = \{s \in S \mid \pi_1(s) > \pi_2(s)\}$ and similarly $S^- = \{s \in S \mid \pi_1(s) < \pi_2(s)\}$. Then clearly
\[
d(S) = \max\{d(S^+), d(S^-)\} - \min\{d(S^+), d(S^-)\}.
\]
Thus for any maximal $S$, we have either $S = S^+$ or $S = S^-$. As $d(\Omega^+) \geq d(S^+)$ and $d(\Omega^-) \geq d(S^-)$, one of either $\Omega^+$ or $\Omega^-$ must be maximal. But as both $\pi_1$ and $\pi_2$ sum to $1$ over $\Omega$, we must have $d(\Omega) = 0$. Thus $d(\Omega^+) - d(\Omega^-) = 0$, which implies that $d(\Omega^+) = d(\Omega^-) = d_{\text{TV}}(\pi_1, \pi_2)$. Therefore we may finally conclude that we have $d_{\text{TV}}(\pi_1, \pi_2) = \frac{1}{2}\(d(\Omega^+) + d(\Omega^-)\)$, and we obtain
\[
d_{\text{TV}}(\pi_1, \pi_2) = \frac{1}{2} \sum_{y \in \Omega} |\pi_1(y) - \pi_2(y)|.
\]
\end{remark}
\begin{remark}
As $\Omega$ is a finite set, the ergodic theorem also holds with respect to this metric (as opposed to converging pointwise for any $x \in \Omega$). This means that for any irreducible and aperiodic matrix $P$ with invariant distribution $\pi$ we have for any distribution $\pi'$ that
\[
\lim_{t \to \infty} d_{\text{TV}}(P^t(\pi'), \pi) = 0.
\]
\end{remark}
Now given any initial state $x$, we can define the distance between the chain starting at $x$ and the limiting distribution $\pi$ at time $t$ to be
\[
d_x(t) := d_{\text{TV}}(P^t(x, \cdot), \pi).
\]
The ergodic theorem implies that $d_x(t)$ converges to $0$. The question remains however, how fast it converges. The mixing time of a Markov chain aims to describe this.
\begin{definition}\label{mixingtimedef}
The \emph{mixing time} of an irreducible and aperiodic Markov chain with transition matrix $P$ is defined as a function $\tau: (0, 1) \mapsto \NN$ assigning to every $\varepsilon > 0$ a time needed to get within distance $\varepsilon$ of $\pi$ by
\[
\tau(\varepsilon) := \max_{x \in \Omega} \min_{t \in \NN} \{t \mid d_x(t') \leq \varepsilon \text{ for all } t' \geq t\}.
\]
\end{definition}
We will say that a family of Markov chains $\mathcal{M}$ is \emph{rapidly mixing} if the mixing time of $M \in \mathcal{M}$ is bounded by some polynomial in the size $\log(|\Omega|)$, where $\Omega = \Omega(M)$ is the state space. For example, given a family of Markov chains on colourings or matchings of graphs, the size of the state space will be exponential in the number of vertices, hence we want our algorithm to be polynomial in the number of vertices.

