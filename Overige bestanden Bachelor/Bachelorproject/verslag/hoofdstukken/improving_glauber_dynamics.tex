% !TeX root = ../verslag.tex
In Section \ref{glauber_dynamics_sec} we described a Markov Chain on the set of all $k$-colourings $\Omega_k(G)$ of a graph $G$, which turned out to be rapidly mixing whenever $k > 2\Delta(G)$. In this section we aim to find an improvement, and provide a way to construct a Markov Chain on $\Omega_k(G)$ that mixes rapidly whenever $k > \frac{11}{6}\Delta(G)$. The chain we provide was originally introduced by \citet{vigoda00} and all described here can be found in the corresponding article.

The key idea is to look at so called `clusters' of vertices.
\begin{definition}
Let $G$ be a graph, and $\sigma: G \to [k]$ be a (not necessarily proper) colouring of $G$, and $c \in [k]$. We will say that $(x_0, \dots, x_l)$ is a $c_1, c_2$-\emph{alternating path} from $v$ to $w$ if $(x_i, x_{i + 1}) \in E(G)$ for all $i$, and $\sigma(x_i) = c_1$ whenever $2 \mid i$ and $\sigma(x_i) = c_2$ otherwise (i.e. $\sigma$ alternates between $c_1$ and $c_2$ along the path). 

A \emph{cluster} is a tuple $(c_1, c_2, S)$ where $c_1, c_2 \in [k]$ and $S \subseteq V(G)$ such that
\begin{itemize}
\item There exists $v \in S$ with $\sigma(v) = c_1$.
\item For any $w \in S$ there exists a $c_1, c_2$-alternating path from $v$ to $w$.
\item It is maximal, there is no strict superset $S' \subseteq V(G)$ satisfying the above two properties.
\end{itemize}
We further define the relation $(c_1, c_2, S) = (c_2, c_1, S)$ (so a cluster is actually an equivalence class of 2 tuples). 
Denote $\mathcal{C}_\sigma$ for the set of all clusters corresponding to a colouring $\sigma$.
\end{definition}
\begin{remark}
Our new definition for clusters is to describe the conditional probabilities better later on, and to make it more clear which clusters are considered the same. We define, as Vigoda did, for any $v \in V(G)$ and $c \in [k]$ the cluster $S_\sigma(v, c) = (\sigma(v), c, S_\sigma(v, c))$ where
\[
S_{\sigma}(v, c) = \{w \in V(G) \mid \text{There exists a $(\sigma(v), c)$-alternating path from $v$ to $w$.}\},
\]
with the special case that $S_\sigma(v, c) = \emptyset$ whenever $c = \sigma(v)$. 
\end{remark}
\begin{remark}
Note that for $x \in S_\sigma(v, c)$ with $\sigma(x) = \sigma(v)$, we have $S_\sigma(x, c) = S_\sigma(v, c)$. Similarly, for $x \in S_{\sigma}(v, c)$ with $\sigma(x) = c$, we have $S_\sigma(v, c) = S_\sigma(x, \sigma(v)$).
\end{remark}
We can now the define the actual Markov Chain.
\begin{definition}[Flip Dynamics]
Define the constants $p_0 = 0$, $p_1 = 1$, $p_2 = \frac{13}{42}$ and for $7 >\alpha > 2$
\[
p_\alpha = \max\left\{0, \frac{13}{42} - \frac{1}{7}\(1 + \frac{1}{2} + \dots + \frac{1}{\alpha - 2}\)\right\}.
\]
For $\alpha \geq 7$, set $p_\alpha = 0$.
Define a Markov Chain on $[k]^{V(G)}$ with transition probabilities given by the procedure: 
\begin{itemize}
\item Pick a cluster $(c_1, c_2, S) \in \mathcal{C}_\sigma$ with probablity $\frac{p_{|S|}}{nk}$. 
\item If none of the clusters are picked, do nothing.
\item Otherwise, flip the colours $c_1, c_2$ for all vertices in $S$.
\end{itemize}
\end{definition}
\begin{remark}
Note that any cluster can be described as some $S_\sigma(v, c)$, thus there are at most $nk$ clusters. Since all the $p_i$ are $\leq 1$, we have that
\[
\sum_{(c_1, c_2, S) \in \mathcal C_\sigma} \frac{p_{|S|}}{nk} \leq 1,
\]
thus our transition probabilities are well-defined.
\end{remark}
\begin{remark}
It is possible to simulate the above chain by selecting a vertex $v$ and colour $c$ uniformly at random, then flipping the cluster $S = S_\sigma(v, c)$ with probability $p_{|S|} / |S|$ or doing nothing otherwise. To see this, note that a cluster $S$  has $|S|$ different ways of being picked in this way, as for each vertex in the cluster, the colour $c$ required to flip the specific cluster is fixed. The probability of it being flipped by picking each specific vertex is $1 / nk$, hence it is flipped with total probability $p_{|S|} / nk$.
\end{remark}
\begin{proposition}
Let $G$ be a graph and $k \in \NN$. The flip dynamics on $[k]^{V(G)}$ are ergodic whenever $k \geq \Delta(G) + 2$, with stationary distribution $\pi$ that is nonzero and uniform on $\Omega_k(G) \subseteq [k]^V(G)$.
\end{proposition}
\begin{proof}
Note that as long as $k \geq \Delta(G) + 2$, for any colouring $\sigma$ we have $P(\sigma, \sigma) > 0$ since for any $v$ there exists a colour $c$ not adjacent to $v$ in the colouring, so that $S_\sigma(v, c) = \{v\}$ and $p_{|S_\sigma(v, c)|} > 0$. With similar reasoning as in Corollary \ref{glaubermixing} the chain will hit $\Omega_k(G)$ in finite time with probability 1. And once we are in $\Omega_k(G)$ the chain is aperiodic and irreducible, with similar reasoning as in Proposition \ref{glauberergodic}, thus ergodic.

To see that the stationary distribution is uniform, note that all transition probabilities are symmetric on $\Omega_k(G)$. 
\end{proof}
We aim to bound the mixing time of this chain using the path coupling theorem (Theorem \ref{pathcoupling}). We take $\Phi$ to be the Hamming distance on $\Omega$. To use the path coupling theorem, we need to define a coupling for any adjacent states $\sigma, \tau$ with $\Phi(\sigma, \tau) = 1$. These states differ in one vertex $v$, so from now on let $v = v_{\sigma, \tau}$ be the one vertex where $\sigma(v) \neq \tau(v)$. 

We proceed by analyzing the clusters that appear in only one of the colourings, which are most interesting for the coupling (since for the rest the identity coupling will usually suffice).
\begin{proposition}
Let $\sigma, \tau \in \Omega$ with $\Phi(\sigma, \tau) = 1$. For any $c \in [k]$, define
\begin{align*}
\Gamma_c &= \{w \in N(v) \mid \sigma(w) = \tau(w) = c\}, \\
D_{\sigma, c} &= \{S_\sigma(v, c)\} \cup \{S_\sigma(w, \tau(v)) \mid w \in \Gamma_c\}, \\
D_{\tau, c} &= \{S_\tau(v, c)\} \cup \{S_\tau(w, \sigma(v)) \mid w \in \Gamma_c\}.
\end{align*}
Then it holds that 
\begin{align*}
\mathcal C_\sigma \setminus \mathcal C_\tau &\subseteq  \bigcup_{c \in [k]} D_{\sigma, c}, \\
\mathcal C_\tau \setminus \mathcal C_\sigma &\subseteq \bigcup_{c \in [k]} D_{\tau, c}
\end{align*}
for all $v \in V(G)$. 
\end{proposition}
\begin{proof}
We show the first inclusion, the second is symmetrical in $\sigma$ and $\tau$. 

Suppose we have some cluster $S_\sigma(x, c) \in \mathcal{C}_\sigma \setminus \mathcal{C}_\tau$ for some $x \in V, c \in [k]$.  If $\sigma(x) = c$ and $\tau(x) = c$, then $S_\sigma(x, c) = (c, c, \emptyset)$. Since $S_\tau(x, c) \neq S_\sigma(x, c)$, we must have $\tau(x) \neq c$, thus $x = v$. This implies $S_\sigma(x, c) \in D_{\sigma, c}$ which suffices.

Otherwise, since $S_\tau(x, c) \neq S_\sigma(x, c)$, we must have $v \in S_\sigma(x, c)$ or $v \in S_\tau(x, c)$. If $v \in S_\sigma(x, c)$ then there exists $c' \in \{c, \sigma(x)\}$ such that $S_\sigma(x, c) = S_\sigma(v, c')$. Thus $S_\sigma(x, c) \in D_{\sigma, c'}$ which suffices.

Otherwise, since $v \in S_\tau(x, c)$ and $\tau$ and $\sigma$ agree everywhere except at $v$, there exists an $(\sigma(x), c)$-alternating path (w.r.t. both the colourings $\sigma$ and $\tau$) from $x$ to some vertex $w \in N(v) \cap S_\sigma(x, c)$. In particular, $\sigma(w) \in \{c, \sigma(x)\}$ and (we may also assume that) $\tau(w) \neq \tau(v)$.

If $\sigma(w) = c$, then also $\tau(w) = c$. Thus $\tau(v) \neq c$, and $\tau(v) = \sigma(x)$ because $v \in S_\tau(x, c)$. Thus $S_\sigma(x, c) = S_\sigma(w, \sigma(x)) = S_\sigma(w, \tau(v))$ as required.

Otherwise, if $\sigma(w) = \sigma(x)$, then also $\tau(w) = \sigma(x)$. Hence $\tau(v) \neq \sigma(x)$, so we also have $\tau(v) \neq \tau(x)$. Thus $\tau(v) = c$ since $v \in S_\tau(x, c)$, and $S_\sigma(x, c) = S_\sigma(w, \tau(v))$ as required. 
\end{proof}
\begin{remark}\label{rem:clusters_once}
Suppose a cluster were to appear twice, say in some $D_{\sigma, c}$ and $D_{\sigma, c'}$ with $c \neq c'$. It can't appear as $S_\sigma(v, c)$ and $S_\sigma(v, c')$ as both those clusters have different colours. Similarly, it can't appear in $S_\sigma(w, \tau(v))$ and $S_\sigma(w', \tau(v))$ for $w \in \Gamma_c, w' \in \Gamma_{c'}$ as the first cluster is a $(c, \tau(v))$-cluster and the second a $(c', \tau(v))$-cluster. 

Thus any such cluster appears (after possibly exchanging $c$ and $c'$) as $S_\sigma(v, c)$ and $S_\sigma(w, \tau(v))$ for some $w \in \Gamma_{c'}$. We conclude that $c = \tau(v)$ (as $\tau(v) \neq \sigma(v)$), and $\sigma(w) = \sigma(v)$. Thus $S_\sigma(v, \tau(v))$ is the only cluster that can appear in two of the $D_{\sigma, c}$, namely in $D_{\sigma, \tau(v)}$ and $D_{\sigma, \sigma(v)}$.

However, then there exists a $(\sigma(v), \tau(v))$-alternating path from $v$ to $w$. By removing $v$ from this path, we end up with a $(\sigma(v), \tau(v))$-alternating path between two neighbours of $v$. Name the other neighbour $u$, then we must have $\sigma(w) = \sigma(v)$ and $\sigma(u) = \tau(v)$. But then we also have $S_\tau(u, \sigma(v)) = S_\tau(v, \sigma(v))$ as the same path between $u$ and $w$ exists in $\tau$. 

Thus if this special case, of this special cluster appearing in two distinct $D_{\sigma, c}$, happens, it also happens for $\tau$ (and vice versa). Since this originates from Vigoda's case (*), we will refer to the clusters $S_\sigma(v, \tau(v)) = S_\sigma(w, \sigma(v))$ and $S_\tau(v, \sigma(v)) = S_\tau(u, \tau(v))$ as the $\sigma^*$ and $\tau^*$-clusters respectively.
\end{remark}
We can now define our coupling.
\begin{definition}[Flip Coupling]
To define a coupling all we need to do is define the probabilities of picking clusters $S, T$ to flip $\sigma$ and $\tau$ with. For brevity, write $\PP(s, t) = \PP(S = s, T = t)$. If $\Phi(\sigma, \tau) \neq 1$, we will simply select $S$ and $T$ independently using the original transition probabilities for the chain (with the extra requirement that if $\sigma = \tau$, we set $S = T$). Remember that to apply the path coupling theorem later on, we are only interested in `adjacent' states $\sigma, \tau$ anyway (which we will define to be the states with $\Phi(\sigma, \tau) = 1$). 

Set
\[
\mathcal E = \mathcal C_\sigma \setminus \(\bigcup_{c \in [k]} D_{\sigma, c} \cup D_{\tau, c}\) \subseteq \mathcal C_\sigma \cap \mathcal C_\tau,
\]
and let 
\[
\PP(s, s) = \frac{p_{|s|}}{nk}
\]
for any $s \in \mathcal{E}$, to couple all clusters that are in both $\sigma$ and $\tau$ together. For the other clusters, we will only couple together clusters in $D_{\sigma, c}$ and $D_{\tau, c}$ for each colour $c$. By Remark \ref{rem:clusters_once} we may later analyze each of these couplings independently, as long as we take care of the double cluster that might appear. For each $c \in [k]$, let $\delta_c = |\Gamma_c|$ and split two cases to define some transitions corresponding to the sets $D_{\sigma, c}$ and $D_{\tau, c}$ (we will refer to these as \emph{$c$-flips} for each $c$).
\begin{enumerate}[a)]
\item ($\delta_c = 0$) In this case $D_{\sigma, c} = \{S_\sigma(v, c)\}$ and $D_{\tau, c} = \{S_\tau(v, c)\}$. Then for this specific transition we set 
\[
\PP(S_\sigma(v, c), S_\tau(v, c))= \frac{1}{nk}.
\]

\item ($\delta_c > 0$) Let $w_1, \dots, w_{\delta_c} \in \Gamma_c$ denote all the neighbours of $v$ with colour $c$ in both $\sigma$ and $\tau$. 
Define 
\begin{align*}
a_i &= a_i(c) = |S_\tau(w_i, \sigma(v))|, \\
b_j &= b_j(c) = |S_\sigma(w_j, \tau(v))|, \\
A &= A(c) = |S_\sigma(v, c)|, \\
B &= B(c) = |S_\tau(v, c)|.
\end{align*}
Since $S_\tau(w_i, \sigma(v))$ might be equal to $S_\tau(w_j, \sigma(v))$ for some $j < i$, if such $j$ exists we redefine $a_i = 0$, and similar for the $b_j$. Furthermore, if we are in the special case $c = \tau(v)$ and $S_\sigma(v, c)$ is the $\sigma^*$ cluster, we redefine $A(c) = 0$. Similarly, if $S_\tau(w_j, \sigma(v))$ is the $\tau^*$-cluster, we will redefine $a_j(c) = 0$ (This way we only consider the flip of these $*$-clusters when $c = \sigma(v)$).

Let $i_{\max}$ be the least $i$ for which $a_i$ is maximal, and similarly $j_{\max}$. 

The idea is to couple the big clusters together as much as possible, thus define
\begin{align*}
\PP(S_\sigma(v, c), \ S_\tau(w_{i_{\max}}, \sigma(v))) &= \frac{p_A}{nk}, &(I)\\
\PP(S_\sigma(w_{j_{\max}}, \tau(v)), \ S_\tau(v, c)) &= \frac{p_B}{nk}. &(II)
\end{align*}
Then couple the remaining clusters together by defining
\begin{align*}
q_l = \begin{cases} p_{a_l} - p_A &\text{ if $l = i_{\max}$} \\ p_{a_l} &\text{ otherwise} \end{cases} \qquad 
q_l' = \begin{cases} p_{b_l} - p_B &\text{ if $l = j_{\max}$} \\ p_{b_l} &\text{ otherwise} \end{cases},
\end{align*}
and setting
\begin{align*}
\PP(S_\tau(w_l, \sigma(v)), S_\sigma(w_l, \tau(v))) &= \frac{\min\{q_l, q_l'\}}{nk}, &(IIIa) \\
\PP(S_\tau(w_l, \sigma(v)), \emptyset) &= \frac{q_l - \min\{q_l, q_l'\}}{nk}, &(IIIb) \\
\PP(\emptyset, S_\sigma(w_l, \tau(v))) &= \frac{q_l' - \min\{q_l, q_l'\}}{nk}. &(IIIc)
\end{align*}
\end{enumerate}
Let all other probabilities by 0, and pick $\PP(\emptyset, \emptyset)$ to be the exact number for the probabilities to sum to 1. 
\end{definition}
\begin{remark}
Note that for $c \neq \sigma(v)$ we have
\[
S_\sigma(v, c) = \left \{\bigcup_{i=1}^{\delta_c} S_\tau(w_i, \sigma(v)) \right \} \cup \{v\}, 
\]
and for $c \neq \tau(v)$ we have
\[
S_\tau(v, c) = \left \{\bigcup_{i=1}^{\delta_c} S_\sigma(w_i, \tau(v)) \right \} \cup \{v\}
\]
as sets of vertices (not cluster-triples). And if $c = \sigma(v)$ or $c = \tau(v)$ then $S_\sigma(v, c)$ resp. $S_\tau(v, c)$ consist of one element. Thus $A \leq 1 + \sum a_i$ and $B \leq 1 + \sum b_j$. Since the $p_i$ are decreasing, this shows the $q_l$ and $q_l'$ are at least nonnegative. 
\end{remark}
As the definition isn't very concise, here is an example to get a better feel for what is going on.
\begin{example}
Consider the graph
\begin{center}
\begin{tikzpicture}
    \node[main node] (1) [fill=red,scale=1.2, name=core, style={double color fill = {green}{red}}, shading angle=-45 ]{$\textbf{\textcolor{black}{v}}$};


    \node[main node] (2) [fill=green,scale=1.2, left = 0.4cm of core]  {$\textbf{\textcolor{black}{a}}$};
    \node[main node] (3) [fill=blue,scale=1.2, above = 0.4cm of core]  {$\textbf{\textcolor{white}{b}}$};
    \node[main node] (4) [fill=red,scale=1.2, right = 0.4cm of core]  {$\textbf{\textcolor{white}{c}}$};
    \node[main node] (5) [fill=green,scale=1.2, below = 0.4cm of core]  {$\textbf{\textcolor{black}{d}}$};

    \path[draw,thick]
    (core) edge node {} (2)
    (core) edge node {} (3)
    (core) edge node {} (4)
    (core) edge node {} (5)
    (2) edge node {} (3)
    (3) edge node {} (4)
    (4) edge node {} (5);
\end{tikzpicture}
\end{center}
denote $\sigma$ for the colouring that colours the middle node green, and $\tau$ for the colouring that colours it red ($\sigma$ and $\tau$ colour $a, b, c, d$ according to the shown colouring). We will use the labels B = Blue, G = Green, R = Red for the encodings. We explicitly write down all non-empty clusters (thus excluding, for example, the cluster $S_\sigma(a, G, G)$). 

\begin{table}[htb]
\centering
\caption{$\sigma$-clusters}
\begin{tabular}{cccccc}
\hline
Name & Encodings & Vertex Set & Colours & Size  & Flip Probability  \\ \hline
$\sigma_1$ & $S_\sigma(a, B), S_\sigma(b, G), S_\sigma(v, B)$ &  \{a, b, v\} & Green, Blue & 3 &  $1 / 90$ \\
$\sigma_2$ & $S_\sigma(a, R)$ &  \{a\} & Green, Red & 1 &  $1 / 15$ \\
$\sigma_3$ & $S_\sigma(b, R), S_\sigma(c, B)$ &  \{b, c\} & Blue, Red & 2 &  $13 / 630$ \\
$\sigma_4$ & $S_\sigma(d, B)$ &  \{d\} & Green, Blue & 1 &  $1 / 15$ \\
$\sigma_5$ & $S_\sigma(v, R), S_\sigma(d, R), S_\sigma(c, G)$ &  \{v, c, d\} & Green, Red & 3 &  $1 / 90$
\end{tabular}
\end{table}
\newpage
\begin{table}[htb]
\centering
\caption{$\tau$-clusters}
\begin{tabular}{cccccc}
\hline
Name & Encodings & Vertex Set & Colours & Size  & Flip Probability  \\ \hline
$\tau_1$ & $S_\tau(a, B), S_\tau(b, G)$ &  \{a, b\} & Green, Blue & 2 &  $13 / 630$ \\
$\tau_2$ & \stackanchor{$S_\tau(a, R), S_\tau(v, G)$,}{$S_\tau(d, R), S_\tau(c, G)$} &  \{a, v, d, c\} & Green, Red & 4 &  $2 / 315$ \\
$\tau_3$ & $S_\tau(d, B)$ &  \{d\} & Green, Blue & 1 &  $1 / 15$ \\
$\tau_4$ & $S_\tau(v, B), S_\tau(c, B), S_\tau(b, R)$ &  \{v, b, c\} & Blue, Red & 3 &  $1 / 90$ \\
\end{tabular}
\end{table}
We find that $\sigma_4 = \tau_3$, yet all other clusters are distinct. Now we wish to calculate the exact transition probabilities for the coupling. We start of by finding $\Gamma_R = \{c\}$, $\Gamma_B = \{b\}$ and $\Gamma_G = \{a, d\}$. Then we find all the sets $D_{\sigma, C}$ and $D_{\tau, C}$ for $C \in \{R, B, G\}$. 
\begin{align*}
D_{\sigma, R} &= \{\sigma_5\} &D_{\tau, R} &= \{\tau_2\}  \\
D_{\sigma, B} &= \{\sigma_1, \sigma_3\} &D_{\tau, B} &= \{\tau_4, \tau_1\} \\
D_{\sigma, G} &= \{\sigma_2, \sigma_5\} &D_{\tau, G} &= \{\tau_2\} 
\end{align*}
We thus see that the special case (*) does occur, with $\sigma_5$ as $\sigma^*$ cluster, and $\tau_2$ as $\tau^*$ cluster. Furthermore, we see that $\mathcal{E} = \{\sigma_4\} = \{\tau_3\}$. 

We will thus start of by coupling $\sigma_4$ and $\tau_3$ together by setting $\PP(\sigma_4, \tau_3) = \frac{1}{15}$. We then continue to find the $C$-flips for each $C \in \{R, G, B\}$.
\begin{itemize}
\item \textbf{(R-flips)}. We have $\delta_R = 1$ and $w_1 = c$. Furthermore, we have the special case $c = \tau(v)$. Thus we explicitly set $A = 0$ as $S_\sigma(v, R)$ is the $\sigma^*$ cluster. As $S_\tau(c, \sigma(v))$ is the $\tau^*$ cluster, we thus also set $a_1 = 0$. Furthermore we have $B = |\emptyset| = 0$, and $b_1 = |\emptyset| = 0$, and $i_{\max} = j_{\max} = 1$. As $p_0 = 0$, this case doesn't yield any positive probabilities.

\item \textbf{(B-flips)}. We have $\delta_B = 1$ and $w_1 = b$. In this case, $A = |\sigma_1| = 3$, $B = |\tau_4| = 3$, $a_1 = |\tau_1| = 2$ and $b_1 = |\sigma_3| = 2$. Once again $i_{\max} = j_{\max} = 1$ as $\delta_B = 1$. Thus we obtain
\begin{align*}
\PP(\sigma_1, \tau_1) &= \frac{1}{90}, &(I) \\
\PP(\sigma_3, \tau_4) &= \frac{1}{90}, &(II) \\
\PP(\sigma_3, \tau_1) &= \frac{1}{105}, &(IIIa)
\end{align*}
since $q_1 = q_1' = \frac{1}{7}$ cases (IIIb) and (IIIc) give probability zero.

\item \textbf{(G-flips)}. We have $\delta_G = 2$ and $w_1 = a, w_2 = d$. In this case, $A = |\emptyset| = 0$, $B = |\tau_2| = 4$, $a_1 = |\emptyset| = 0$, $a_2 = \emptyset = 0$, $b_1 = |\sigma_2| = 1, b_2 = |\sigma_5| = 3$. We have $i_{\max} = 1$ and $j_{\max} = 2$. As $A = 0$ case (I) yields a zero probability. We thus find
\begin{align*}
\PP(\sigma_5, \tau_2) &= \frac{2}{315}, &(II)
\end{align*}
Note now that $q_1 = 0, q_1' = p_1, q_2 = 0, q_2' = p_3 - p_4$. Hence only (IIIb) gives non-zero probability, which yields
\begin{align*}
\PP(\sigma_2, \emptyset) &= \frac{1}{15}, &(IIIb) \\
\PP(\sigma_5, \emptyset) &= \frac{1}{210}. &(IIIb)
\end{align*}
since $q_1 = q_1' = \frac{1}{7}$ cases (IIIb) and (IIIc) give probability zero.
\end{itemize}
We have thus found all the coupling probabilities. To make clear what is going on, the following table shows the probability of flipping each pair of clusters we just calculated.
\begin{table}[htb]
\centering
\caption{Coupling Probabilities}
\begin{tabular}{c|ccccc|c}
\hline
Clusters & $\tau_1$ & $\tau_2$ & $\tau_3$ & $\tau_4$ & $\emptyset$ & Total \\ \hline
$\sigma_1$ & $\frac{1}{90}$ & 0 & 0 & 0 & 0 & $\frac{1}{90}$\\
$\sigma_2$ & 0 & 0 & 0 & 0 & $\frac{1}{15}$ & $\frac{1}{15}$ \\
$\sigma_3$ & $\frac{1}{105}$ & 0 & 0 & $\frac{1}{90}$ & 0 & $\frac{13}{630}$ \\
$\sigma_4$ & 0 & 0 & $\frac{1}{15}$ & 0 & 0 & $\frac{1}{15}$ \\
$\sigma_5$ & 0 & $\frac{2}{315}$ & 0 & 0 & $\frac{1}{210}$ & $\frac{1}{90}$ \\ 
$\emptyset$ & 0 & 0 & 0 & 0 & 0 & \\\hline
Total & $\frac{13}{630}$ & $\frac{2}{315}$ & $\frac{1}{15}$ & $\frac{1}{90}$ & & 
\end{tabular}
\end{table}
The column/row named `Total' shows for each cluster the total probability it gets flipped by the coupling, by which we verify that at least in this particular case, the coupling is well defined.
\end{example}
\begin{lemma}
This procedure defines a coupling.
\end{lemma}
\begin{proof}
One can easily verify that equations (\ref{couplingconditional1}), (\ref{couplingconditional2}) hold for all clusters that actually change a colouring. Since this becomes tedious quickly we only provide a sketch. The probability that $S = s$ for some $s$ not in any of the $D_{\sigma, c}$ is obviously $\frac{p_{|s|}}{nk}$.. The probability of $S$ becoming a cluster of the form $S_\sigma(v, c)$ with $\delta_c = 0$ is $1 / nk$ by (a), and since $p_1 = 1$ this is exactly $\frac{p_{|S_\sigma(v, c)|}}{nk}$ as required. Finally per choice of the $A, a_i, B, b_i$ the clusters belonging to case (b) also appear with their corresponding probabilities, since they all belong to exactly one colour $c$, except in the special case of a $\sigma^*$-cluster, but then we redefined the $A, a_i$ for it to still work out exactly.
\end{proof}
Now to apply the path coupling theorem, we wish to bound
\[
\EE[\Delta \Phi(\sigma, \tau)] := \EE[\Phi(\sigma', \tau')] - \Phi(\sigma, \tau). 
\]
away from 1, where the $\sigma'$ and $\tau'$ are the colourings obtained by applying flips $S$ and $T$ to $\sigma$ and $\tau$ respectively. Since our coupling is defined by a bunch of disjoint events, we can analyze each of these events individually. It's clear that a choice of the form $(s, s)$ for $s \in \mathcal{E}$ leaves $\Phi(\sigma, \tau)$ intact. We thus only have to consider the $c$-flips for each colour $c$. For $c \in [k]$, let $\mathbf{1}_c$ be the r.v. that is 1 if and only if the coupling selects a $c$-flip, and 0 otherwise. Set $\Delta_c \Phi(\sigma, \tau) = \mathbf{1}_c \cdot \Delta \Phi(\sigma, \tau)$. Then clearly
\[
\EE[\Delta \Phi(\sigma, \tau)] = \sum_{c \in [k]} \EE[\Delta_c \Phi].
\]
Note that we lost the factor $nk$ that Vigoda had since we're using probabilities instead of weight, to make it easier to understand the actual conditioning. The following lemma contains the heart of the calculation. It is quite cumbersome to proof, and we will only sketch some parts of its proof.
\begin{lemma} \label{lem:vigoda_body} Let $\sigma, \tau \in \Omega$ with $\Phi(\sigma, \tau) = 1$. Then for each $c \in [k]$ we have
\begin{enumerate}[a)]
\item If $\delta_c = 0$, then $\EE[\Delta_c \Phi(\sigma, \tau)] = -\frac{1}{nk}$.
\item If $\delta_c > 0$, then $\EE[\Delta_c \Phi(\sigma, \tau)] \leq \frac{1}{nk}\(\frac{11}{6} \delta_c - 1\)$. 
\end{enumerate}
\end{lemma}
\begin{proof} We proof (a) and sketch (b). \\
\textbf{a)} In this case, remember that $D_{\sigma, c} = \{S_\sigma(v, c)\}$ and $D_{\tau, c} = \{S_\tau(v, c)\}$. There is only one $c$-flip, the move $(S_\sigma(v, c), S_\tau(v, c))$ which is selected with probability $1 / nk$. As $\delta_c = 0$, both $S_\sigma(v, c)$ and $S_\tau(v, c)$ only contain the point $v$. Before the flip $\sigma$ and $\tau$ disagree on $v$, and after the flip they agree on $v$ (and remain the same everywhere else. Hence
\[
\EE[\Delta_c\Phi(\sigma, \tau)] = \frac{1}{nk}\(\(\Phi(\sigma, \tau) - 1\) - \Phi(\sigma, \tau)\) = -\frac{1}{nk}.
\]
\textbf{b)} In the case $\delta_c > 0$, consider the effect of each of the possible $c$-flips seperately. Let us for now ignore the special case of a $\sigma^*, \tau^*$ cluster appearing.  For a move of type (I), the colourings still remain the same on the vertex set of the cluster $S_\tau(w_{i_{\max}}, \sigma(v)) \subseteq S_\sigma(v, c)$. As they also agree on $v$ now, their Hamming distance has increased by at most $A - a_{\max} - 1$. For a move of type (II) we conclude by similar reasoning that the Hamming distance increases by at most $B - b_{\max} - 1$. 

For a move of type (IIIa), the Hamming distance increases by at most $a_l + b_l - 1$ (as they both flip $w_l$ we only have to take it once into account). And finally moves (IIIb) and (IIIc) increase the hamming distance by $a_l$ and $b_l$ respectively. We denote the effect of the flips (IIIa,b,c) with the function $f(w_l)$, defined as
\[
f(w_l) = \frac{1}{nk}(a_lq_l + b_l q_l' - \min\{q_l, q_l'\}). 
\]
Then we thus have, by considering each of the events described above with their appropriate probability,
\begin{equation}\label{eq:delta_c_bound}
\EE[\Delta_c \Phi(\sigma, \tau)] \leq \frac{1}{nk}\((A - a_{\max} - 1)p_A + (B - b_{\max} - 1)p_B\) + \sum_{l=1}^{\delta_c} f(w_l).
\end{equation}
What remains is to carefully verify that for all of the variables $a_i, b_i$ that may appear, the parameter choices satisfy
\[
\frac{1}{nk}\((A - a_{\max} - 1)p_A + (B - b_{\max} - 1)p_B\) + \sum_{l=1}^{\delta_c} f(w_l) \leq \frac{1}{nk}\(\frac{11}{6}\delta_c - 1\).
\]
The entire calculation takes Vigoda about 4 pages. We will instead consider only the case $\delta_c = 1$ to give some idea of what remains to be done. In this case, the cluster $S_\sigma(v, c)$ consists (as long as it's not empty) of the vertex set $\{v\} \cup \{S_\tau(w_1, \sigma(v))\}$. Hence $|A| \leq a_1 + 1$ and similarly $|B| \leq b_1 + 1$. Thus $q_1 = p_{a_1} - p_A$ and $q_1' = p_{b_1} - p_B$. We may assume $q_1 \geq q_1'$ without loss of generality. This gives us
\begin{align*}
\EE[\Delta_c \Phi(\sigma, \tau) &\leq \frac{1}{nk}\((A - a_{1} - 1)p_A + (B - b_{1} - 1)p_B\) + f(w_1) \\
&\leq \frac{1}{nk}\((a_1 + 1 - a_{1} - 1)p_A + (b_1 + 1 - b_{1} - 1)p_B\) +  f(w_1) \\
&= \frac{1}{nk}(a_1q_1 + b_1q_1' - q_1')  \\
&= \frac{1}{nk}(a_1(p_{a_1} - p_A) +  (b_1 - 1)(p_{b_1} - p_B)) \\
&\leq \frac{1}{nk}(a_1(p_{a_1} - p_{a_1 + 1}) +  (b_1 - 1)(p_{b_1} - p_{b_1 + 1})).
\end{align*}
One easily verifies that for each of the $a, b \in \{1, \dots, 7\}$ (as otherwise all $p_a, p_b$ are 0 anyway) we have $(b - 1)(p_b - p_{b+ 1}) \leq \frac{1}{7}$ and $a(p_a - p_{a + 1}) \leq \frac{29}{42}$. From this we conclude
\[
\EE[\Delta_c \Phi(\sigma, \tau)] \leq \frac{1}{nk} \times \frac{5}{6} = \frac{1}{nk} \(\frac{11}{6} \delta_c - 1\),
\]
as desired.

Finally, consider the special case of a $\sigma^*, \tau^*$ cluster appearing. Remember that the only cluster probabilities we redefined were those for $c = \tau(v)$. But for this particular case, remember that we explicitly defined $|A| = 0$ and $a_{j} = 0$, where $j$ is such that $S_\tau(w_j, \sigma(v))$ is the $\tau^*$-cluster. As $c = \tau(v)$, we also have $|B| = 0$ and $b_i = 0$ for all $i$, as the cluster $S_\tau(v, \tau(v)) = \emptyset$, and for $u \in \Gamma_{\tau(v)}$ also $S_\sigma(u, \tau(v)) = \emptyset$. Hence the only terms in equation \ref{eq:delta_c_bound} that remain are
\[
\frac{1}{nk}\(\sum_{1 \leq l \leq \delta_{c}, l \neq j} a_lp_{a_l}\) \leq \frac{1}{nk}(\delta_c - 1)p_1 \leq \frac{1}{nk}\(\frac{11}{6}\delta_c - 1\),
\]
which shows the required bound holds.
\end{proof}
Using this lemma we can now bound the mixing time of the flip dynamics.
\begin{theorem}
Let $G$ be a graph and $k \in \NN$. Provided $k > \frac{11}{6} \Delta(G)$, the mixing time of the flip dynamics is bounded above by
\[
\tau(\varepsilon) \leq \frac{nk \log(n \varepsilon^{-1})}{k - \frac{11}{6}\Delta}.
\]
\end{theorem}
\begin{proof}
Call two colourings $\sigma, \tau$ adjacent ($\sigma \sim \tau$) if $\Phi(\sigma, \tau) = 1$, and consider two adjacent states $\sigma, \tau$. Let $v = v_{\sigma, \tau}$ the vertex in which $\sigma$ and $\tau$ disagree, and $\delta = \delta(v)$ the degree of $v$ in $G$. To apply the previous lemma, we need to find the number of colours $c$ such that $\delta_c = 0$. As $\delta = \sum_{c: \delta_c > 0} \delta_c$, we have the identity
\begin{equation}\label{eq:delta_c_zero_count}
\sum_{c: \delta_c = 0} 1 = k - \sum_{c: \delta_c > 0} 1 = k - \delta + \sum_{c: \delta_c > 0} (\delta_c - 1).
\end{equation}
This implies that
\begin{align*}
\EE[\Delta \Phi(\sigma, \tau)] &= \sum_{c: \delta_c = 0} \EE[\Delta_c \Phi(\sigma, \tau)] +  \sum_{c: \delta_c > 0} \EE[\Delta_c \Phi(\sigma, \tau)] \\
&\leq \(k - \delta + \sum_{c: \delta_c > 0} (\delta_c - 1)\)\(-\frac{1}{nk}\) +  \sum_{c: \delta_c > 0} \EE[\Delta_c \Phi(\sigma, \tau)] \\ 
&\leq \(k - \delta + \sum_{c: \delta_c > 0} (\delta_c - 1)\)\(-\frac{1}{nk}\) +  \sum_{c: \delta_c > 0} \frac{1}{nk}\(\frac{11}{6} \delta_c - 1\) \\ 
&= \frac{\delta - k}{nk} + \sum_{c: \delta_c > 0}\frac{5}{6} \times \frac{\delta_c}{nk} \\
&\leq \frac{\frac{11}{6}\Delta - k}{nk},
\end{align*}
where the second line follows from equation \ref{eq:delta_c_zero_count} and Lemma \ref{lem:vigoda_body}(a), the third line follows from Lemma \ref{lem:vigoda_body}(b), and in the last line we used $\delta \leq \Delta$ and $\sum_{c: \delta_c > 0} \delta_c = \delta$. Hence if $\sigma_t \sim \tau_t$, we have that ($\Phi(\sigma_t, \tau_t) = 1$)
\begin{align*}
\EE[\Phi(\sigma_{t + 1}, \tau_{t + 1})] &= \Phi(\sigma_t, \tau_t) + \EE[\Delta \Phi(\sigma, \tau)] \\
&\leq 1 - \frac{k - \frac{11}{6}\Delta}{nk} = \(1 - \frac{k - \frac{11}{6}\Delta}{nk}\)\Phi(\sigma_t, \tau_t).
\end{align*}
Thus, applying Theorem \ref{pathcoupling} with $\beta = 1 - \frac{k - \frac{11}{6}\Delta}{nk}$, we see that the mixing time of the flip dynamics is bounded above by
\[
\tau(\varepsilon) \leq \frac{\log(n \varepsilon^{-1})}{1 - \(1 - \frac{k - \frac{11}{6}\Delta}{nk}\)} = \frac{nk \log(n \varepsilon^{-1})}{k - \frac{11}{6}\Delta}. \qedhere
\]
\end{proof}