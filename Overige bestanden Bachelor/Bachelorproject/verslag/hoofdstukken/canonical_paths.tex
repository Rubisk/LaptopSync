% !TeX root = ../verslag.tex
In this section we discuss a different way of showing a Markov chain mixes rapidly, by constructing so-called canonical paths. The idea is that for any pair $x, y \in \Omega$, we have to route $\pi(x)\pi(y)$ units of flow from $x$ to $y$, using the transition steps of the matrix as the ``pipes'' to route the flow through, where the transition probabilities could be seen as the ``capacity'' of the pipes. If we can route the flow evenly without creating pipes that are particularly congested, we can get a good upper bound on the mixing time. 

We will use a lot of ideas from \citet[Section 5]{jerrum03} in this chapter. 
\begin{definition}
Let $P$ be the transition matrix of a Markov chain with state space $\Omega$. A a set of \emph{canonical paths} on $\Omega$ is a set $\Gamma = \{\gamma_{xy}\}_{x, y \in \Omega}$, where each $\gamma_{xy}$ is a tuple (path) of the form $(x = z_0, z_1, \dots, z_l = y)$ of states such that $P(z_i, z_{i + 1}) > 0$ for $i \in \{1, \dots, l - 1\}$. The \emph{congestion} of a set of canonical paths is defined as
\[
\rho(\Gamma) := \max_{t = (u, v)} \underbrace{\vphantom{\sum_{x, y: \gamma_{xy} \text{ uses } t}}\frac{1}{\pi(u)P(u, v)}}_{(\text{capacity of } t)^{-1}}\underbrace{\sum_{x, y: \gamma_{xy} \text{ uses } t} \pi(x)\pi(y)|\gamma_{xy}|}_{\text{ total flow through } t}, 
\]
where $t$ runs over all possible transitions in the Markov Chain, and $|\gamma_{xy}|$ denotes the length $l$ of $\gamma_{xy}$. 
\end{definition}

Later on, in Section \ref{sec:independent_sets} we will give an example of how to find a set of paths with low congestion. For now, we will try to find an upper bound on the mixing time if the congestion of a particular set of canonical paths is low. Thus assume $\rho$ is small for some choice of paths $\Gamma$, we aim to show that the mixing time is small as well. For an arbitrary function $f: \Omega \to \RR$, we define the \emph{variance and expectation with respect to the probability measure $\pi$} as 
\begin{align*}
\EE_\pi f := \sum_{x \in \Omega} \pi(x)f(x), \qquad 
\Var_\pi(f) := \sum_{x \in \Omega} \pi(x)(f(x) - \EE_\pi f)^2. 
\end{align*}

Recall that given two i.i.d. variables $X_1, X_2$ the identity $\Var(X_1) = \frac{1}{2} \EE[(X_1 - X_2)^2]$ holds. This gives us the identity
\begin{equation}\label{symmetricpivariance}
\Var_\pi f := \frac{1}{2} \sum_{x, y \in \Omega} \pi(x)\pi(y)(f(x) - f(y))^2, 
\end{equation}
which shows that $\Var_\pi f$ basically measures the difference between $f(x)$ and $f(y)$ over all possible states, i.e.\ it is a measure for the global variation of $f$. We can similarly define 
\[
\mathcal{E}_P(f, f) := \frac{1}{2} \sum_{x, y \in \Omega} \pi(x) P(x, y)(f(x) - f(y))^2 
\]
as a measure for the expected square change of $f$ after simulating the Markov Chain for one step (i.e.\ start at some point $x$, simulate the chain for one step and end up in $y$, then look at $(f(x) - f(y))^2$). Note that $\mathcal{E}_P(f, f)$ depends on $P$, whereas the variance only depends on $\pi$. Recall that the congestion $\rho(\Gamma)$ was a measure for ``how easy it is to move along the state space using transitions of $P$''. Now if the congestion is low, then the probability of jumping from any $x$ to $y$ in a small number of steps is high. If the expected square change of $f$ were also low, then one could imagine $f$ cannot vary too much accross the state space. In other words, if $\rho(\Gamma)$ and $\mathcal{E}_P(f, f)$ are both low, one would expect $\Var_\pi f$ to also be low. This is made more precise in the following proposition.
\begin{proposition}\label{localglobalvariance}
Let $P$ be the transition matrix of a Markov chain with state space $\Omega$. For any function $f: \Omega \to \RR$ and any set of canonical paths $\Gamma$, we have that 
\[
\mathcal{E}_P(f, f) \geq \frac{1}{\rho(\Gamma)}\Var_\pi f. 
\]
\end{proposition}
\begin{proof}
The proof is fairly straightforward and can be found in \citet[Section 5, Theorem 5.2]{jerrum03}.
\end{proof}
The general idea is to analyze how $f$ changes as we apply to $P$ to $f$. We can define an action from $P$ on $f$ in the same way we define an action from $P$ on any probability distribution, by setting 
\[
[Pf](x) := \sum_{y \in \Omega} P(x, y)f(y).
\]
This is basically just matrix-vector multiplication. Note that applying $P$ preserves the average, in other words $\EE_\pi P^t f = \EE_\pi f$ for all $t \in \NN$. If we think of $f$ as the amount of `flow' contained in a point, applying $P$ basically distributes the flow of $f$ over the state space by simulating the Markov chain for one step. To then show the Markov chain mixes rapidly, we want the `flow' to distribute evenly (with respect to the invariant measure $\pi$) accross the state space in a short amount of time. In other words, we want $\Var_\pi P^t f$ to decay rapidly as $t$ grows. 

Since we are analyzing a dynamical system, we would really like to be able to do calculus (so that we could differentiate w.r.t. $t$). We thus define a continuous-time version of the same Markov chain. Consider the continuous time Markov chain $(\tilde{X}_s)_{\mid s \in \RR}$ on $\Omega$ with transition rate matrix $Q = P - I$. This is a continuous Markov chain with exponentially-distributed holding times with mean $1$, and jump probabilities equal to the transition probabilities of $P$. 

Let $\tilde{P}^s(x, y) := \PP(\tilde{X}_s = y \mid \tilde{X}_0 = x)$ denote the transition probabilities after $s$ time has passed. By definition of $Q$ we have that $\tilde{P}^s = e^{Qs}$. We define the action from $\tilde{P}^s$ on $f$ in the same way we defined it for $P$. Now using calculus, we can show that the variance of this continuous chain decays rapidly. 
\newpage
\begin{lemma}\label{lem:continuousboundcongestion}
Let $P$ be the transition matrix of a Markov chain with state space $\Omega$. Let $\tilde{P}^s$ be its continuous-time version, as defined above. For any function $f: \Omega \to \RR$ and any set of canonical paths $\Gamma$, we have that 
\begin{equation}
\Var_\pi(\tilde P^s f) \leq e^{-2 \frac{s}{\rho(\Gamma)}} \Var_\pi f. 
\end{equation}
\end{lemma}
\begin{proof}
The proof we give is inspired by \citet[Section 5.5]{jerrum03}. Note that since both $\Var_\pi (\tilde{P}^s f)$ and $\Var_\pi f$ do not change when we add or subtract a constant to $f$, we may assume that $\EE_\pi f = \EE_\pi \tilde{P}^sf = 0$. 

Starting with \ref{symmetricpivariance}, we can now differentiate with respect to $s$ and obtain
\begin{align*}
\frac{d}{ds} \Var_\pi(\tilde{P}^s f) &= \frac{d}{ds}\frac{1}{2} \sum_{x, y \in \Omega}\pi(x)\pi(y)\([\tilde{P}^sf](x) - [\tilde{P}^sf](y)\)^2 \\
&= \sum_{x, y \in \Omega}\pi(x)\pi(y)\([\tilde{P}^sf](x) - [\tilde{P}^sf](y)\)\(\frac{d}{ds}([\tilde{P}^sf](x) - [\tilde{P}^sf](y))\) \\
&= \sum_{x, y \in \Omega}\pi(x)\pi(y)\(2 [\tilde{P}^sf](x) \frac{d}{ds} [\tilde{P}^s]f(x) -  2[\tilde{P}^sf](x) \frac{d}{ds} [\tilde{P}^s]f(y)\).
\end{align*}
The last line follows by multiplying out the product and using the symmetry in $x$ and $y$. Now, since $\EE_\pi \tilde P^sf = 0$ we have that
\[
\sum_{x, y \in \Omega}\pi(x)\pi(y)[\tilde{P}^sf](x) \frac{d}{ds} [\tilde{P}^sf](y) = \(\sum_{y \in \Omega}\pi(y)\frac{d}{ds} [\tilde{P}^sf](y)\) \(\sum_{x \in \Omega} \pi(x)[\tilde{P}^sf](x)\) = 0.
\]
Thus continuing, it follows that
\begin{align*}
\frac{d}{ds} \Var_\pi(\tilde{P}^s f) &= 2\sum_{x \in \Omega}\pi(x)[\tilde{P}^sf](x)\frac{d}{ds}[\tilde{P}^sf](x) \\
&= 2\sum_{x, y \in \Omega} \pi(x) [\tilde P^s f](x)f(y) \frac{d}{ds} \tilde P^s(x, y) \\
&= 2\sum_{x, y \in \Omega} \pi(x) [\tilde P^s f](x)f(y) \(((P - I)\tilde P^s)(x, y)\) \\
&= 2\sum_{x, y, z \in \Omega} \pi(x) [\tilde P^s f](x)f(y) (P(x, z) - I(x, z))\tilde P^s(z, y) \\
&= 2\sum_{x, z \in \Omega} \pi(x) (P(x, z) - I(x, z))[\tilde P^s f](x) [\tilde P^sf](z). 
\end{align*}
Setting $s = 0$ and summing over $x, y$ instead, we obtain
\begin{align*}
\frac{d}{ds} \Var_\pi(\tilde P^s f) \bigg \rvert_{s = 0} &= 2\sum_{x, y \in \Omega} \pi(x)(P(x, y) - I(x, y))f(x)f(y) \\
&= 2\sum_{x, y \in \Omega} \pi(x) P(x, y) f(x) f(y) - \sum_{x \in \Omega} \pi(x)f(x)^2 \\
&= 2\sum_{x, y \in \Omega} \pi(x) P(x, y) f(x) f(y) - \frac{1}{2}\sum_{x, y \in \Omega} \pi(x)P(x, y)\(f(x)^2 + f(y)^2\)\\
&= -2\mathcal{E}_P(f, f). 
\end{align*}
By Proposition \ref{localglobalvariance}, we obtain
\[
\frac{d}{ds} \Var_\pi(\tilde{P}^s f) \bigg \rvert_{s = 0} \leq -\frac{2}{\rho(\Gamma)}\Var_\pi f.
\]
Now since $f$ is an arbitrary function $\Omega \to \RR$, by setting $f = \tilde{P}^r f$ we obtain for any $r > 0$

\[
\frac{d}{ds} \Var_\pi (\tilde{P}^s f) \bigg \rvert_{s = r} = \frac{d}{ds} \Var_\pi \(\tilde{P}^s \(\tilde{P}^r f\)\) \bigg \rvert_{s = 0}  \leq - \frac{2}{\rho(\Gamma)} \Var_\pi \tilde{P}^r f.
\]
Hence for $s \geq 0$, the function $s \mapsto \Var_\pi \tilde P^s f$ is bounded above by the solution to the ordinary differential equation $\dot v = -(2 / \rho)v$ with $v(0) = \Var_\pi f$, therefore we obtain for $s \geq 0$ the inequality
\[
\Var_\pi(\tilde P^s f) \leq e^{-2 \frac{s}{\rho}} \Var_\pi f,
\]
as desired.
\end{proof}

This bound on the continuous time can be translated to a bound on the discrete-time chain.
\begin{lemma}\label{lem:congestion_bound_continuous}
Let $P$ be the transition matrix of a Markov chain with state space $\Omega$. For any function $f: \Omega \to \RR$ and any set of canonical paths $\Gamma$, we have that 
\[
\Var_\pi(Pf) \leq \(1 - \frac{2}{\rho}\)\Var_\pi f.
\]
\end{lemma}
\begin{proof}
Consider once again the continuous time $\tilde{P}^s$ corresponding to $P$. Conditioning on the number of jumps at time $s$ (which has distribution $\text{Poisson(s)}$) and dropping all terms but the first two, we obtain for any $s > 0$ the inequality
\begin{align*}
\Var_\pi \tilde P^s f &= \sum_{t = 0}^\infty \frac{s^t e^{-s}}{t!} \Var_\pi P^t f \\
&\geq e^{-s}\Var_\pi f + se^{-s}\Var_\pi Pf.
\end{align*}
Using the previous bound from Lemma \ref{lem:continuousboundcongestion}, we find that for any $s > 0$
\[
\(e^{-2\frac{s}{\rho}}-  e^{-s}\)\Var_\pi f  \geq se^{-s}\Var_\pi Pf,
\]
and thus
\[
\Var_\pi Pf \leq \Var_\pi f \cdot \frac{e^{\(1 - \frac{2}{\rho}\)s} - 1}{s}.
\]
Taking the limit $s \to 0$ yields the desired bound.
\end{proof}
We can now easily bound the mixing time using our bound on the variance.
\begin{theorem}[Canonical Paths]\label{canonical_paths}
Let $M$ be a Markov chain with state space $\Omega$. For any set of canonical paths $\Gamma$ on $\Omega$, the mixing time of $M$ is bounded above by
\[
\tau(\varepsilon) \leq \frac{\rho(\Gamma)}{2} (2 \log(\varepsilon^{-1}) + \sup_{x \in \Omega} \log(\pi(x)^{-1})). 
\]
\end{theorem}
\begin{proof}
Fix some initial starting state $x$. Let $A \subseteq \Omega$ be some subset of the state space, and let $\mathbf{1}_A$ be the indicator function on the set $A$. It's clear that $\Var_\pi \mathbf{1}_A \leq 1$, and therefore by using Lemma \ref{lem:congestion_bound_continuous} $t$ times we see that
\[
\Var_\pi(P^t \mathbf{1}_A) \leq \(1 - \frac{2}{\rho}\)^t \leq e^\frac{-2t}{\rho}.
\]
Set 
\[
t = \left \lceil \frac{\rho}{2}(2 \log \varepsilon^{-1} + \log(\pi(x)^{-1})) \right \rceil,
\]
then
\[
\Var_\pi P^t \mathbf{1}_A \leq e^{-2\log \varepsilon^{-1} - \log(\pi(x)^{-1})} = \varepsilon^2 \pi(x).
\]
Also
\begin{align*}
\Var_\pi P^t \mathbf{1}_A &\geq \pi(x) \([P^t\mathbf{1}_A](x) - \EE_\pi P^t \mathbf{1}_A\)^2 \\
&= \pi(x) \([P^t \mathbf{1}_A](x) - \EE_\pi \mathbf{1}_A\)^2.
\end{align*}
Now note that $[P^t \mathbf{1}_A](x) = \sum_{a \in A}P^t(x, a) = P^t(x, A)$. And obviously $\EE_\pi \mathbf{1}_A = \pi(A)$. This implies that combining the two bounds we get
\[
\varepsilon \geq |[P^t \mathbf{1}_A](x) - \EE_\pi \mathbf{1}_A| = |P^t(x, A) - \pi(A)|.
\]
Since $A$ was an arbitrary subset of $\Omega$, we conclude that $d_{\text{TV}}(P^t(x, \cdot),  \pi) \leq \varepsilon$. Taking the supremum over all $x$ gives the desired bound on the mixing time.
\end{proof}

Thus we have shown that if one can find a set of canonical paths with low congestion, then the mixing time will become low as well. In the next chapter we shall take a look at finding such a set of canonical paths.