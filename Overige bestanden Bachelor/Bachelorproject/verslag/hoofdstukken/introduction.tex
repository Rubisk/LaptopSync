% !TeX root = ../verslag.tex
In graph theory, it is hard to find exact answers for certain counting problems efficiently. Consider for example the problem of counting the number of proper $k$-colourings in a given graph $G$ on $n$ vertices. It is clear that any general exact formula will probably not be of any use, as there are many possible graphs on $n$ 
vertices. And if $G$ is sparse (say $\Delta(G) \leq k + 2$), it is not to hard to see that the number of $k$-colourings will grow exponentially in $n$, so enumerating all colourings with a computer will take a long time even for small $n$. However, there are algorithms to approximate the answer in polynomial time. These algorithms are so-called \emph{fpras}, which stands for \emph{fully polynomial randomised approximation scheme}. 

The methods we will consider have their origin in statistical physics. For example, the Glauber Dynamics (a Markov Chain process to select a graph colouring almost-uniformly, see Section \ref{glauber_dynamics_sec}) is related to the Potts model. The Potts model is used in statistical physics to model interacting spins on a graph $G = (V, E)$. In practice this graph will usually just be a lattice with vertex set $\{-L, \dots, L\}^D$ for some finite $L, D$, however we will consider an arbitrary graph. Every vertex is then assigned a spin, which we can model as assigning each vertex one of $q$ colours. A particular arrangement of spins will be referred to as a state (analogous to a single colouring $\sigma$). Each state corresponds to an energy level, which is called the Hamiltonian of the state, defined by 
\[
H(\sigma) = \sum_{(u, v) \in E} J(1 - \delta_{\sigma(u), \sigma(v)}).
\]
Here the $J$ is a fixed constant, and $\delta: [q]^2 \to \{0, 1\}$ is the Kronecker delta, which is equal $1$ if $\sigma(u) = \sigma(v)$ and $0$ otherwise. If $J > 0$ the model is said to be \emph{ferromagnetic} and \emph{antiferromagnetic} if $J < 0$. 

The Potts model then defines a probability measure on the space of all possible states. For a fixed constant $\beta = \frac{1}{k_B T}$ (known as the inverse temperature, here $k_B$ is the Boltzmann constant and $T$ is the absolute temperature), the relative weight of a particular state is given by $e^{-\beta H(\sigma)}$. If we thus define the normalizing constant (often called the partition function)
\[
Z(G) = \sum_{\sigma} e^{-\beta H(\sigma)}
\]
then the probability of a given state appearing is given by
\[
\PP(\sigma) = \frac{e^{\beta H(\sigma)}}{Z(G)}.
\]
The Potts model is a generalization of the Ising model, which is the exact same model but taking only the set $\{1, -1\}$ as possible spins instead of an arbitrary number $q$. By modifying the Glauber Dynamics with Metropolis-style acceptance probabilities (see for an explanation \citet[Section 5.5, p210]{norris97}, we provide an example of how to do this in Section \ref{sec:independent_sets}) to arrive at the desired invariant distribution, one can then construct a process to sample from the Potts model distribution. Such a sampler is called a Gibbs sampler, and has applications in statistical physics.

However, physicists are often interested in evaluating the partition function $Z(G)$ as well. It turns out, that if we set $K = J / k_B T$, the function $Z(G; q, K)$ is (up to some constant) the Tutte polynomial  in disguised form. The Tutte polynomial $T(G; x, y)$ is a two-variable polynomial assigned with graphs that can describe a lot of their properties. It is defined recursively by  contracting/removing edges from the graph. We will not define it here, one could look at \citet[Section 3.1]{goodall14} for a definition of the Tutte polynomial. If one then parametrizes
\begin{align*}
x &= 1 + \frac{qe^{-K}}{1 - e^{-K}} \\
y &= e^K,
\end{align*}
one has that $T(G; x, y) = C \times Z(G; q, K)$ for some constant $C$. The Tutte polynomial can thus be seen as a continuation of the partition function from some countable set of hyperbola (the image of $\NN \times \RR$ under the parametrization) to the whole plane. 

It is then no surprise that one can retrieve lots of properties of $G$ one could obtain from the Tutte polynomial directly from $Z(G)$ as well. For example, consider letting $\beta \to \infty$, which corresponds to letting the temperature tend to absolute zero, in the antiferromagnetic Potts model ($J < 0$). Then any proper colouring appears with weight $e^{-\beta |E| \times J}$, and any non-proper colouring will appear with weight at most $e^{-\beta (|E| - 1) \times J}$. Thus
\begin{equation}\label{eq:partition_estimate}
Z(G; q, K) = \#\{\sigma \mid \sigma \text{ is a proper colouring}\} (e^{-\beta J})^{|E|} + \mathcal{O}((e^{-\beta J})^{|E| - 1}).
\end{equation}
As the number of edges $|E|$ is easy to count in a graph, by letting $\beta \to \infty$ one can use $Z(G)$ to determine the number of proper colourings on a graph $G$ (which corresponds to evaluating $T(1 - q, 0)$). To do this, note that from equation \ref{eq:partition_estimate} we obtain
\[
\#\{\sigma \mid \sigma \text{ is a proper colouring}\} = \lim_{\beta \to \infty} \frac{Z(G; q, K)}{e^{-\beta J |E|}}
\]
as long as $J < 0$.

We will now start off with some prerequisites in Chapter \ref{ch:pre}. After that, in chapter \ref{ch:approximate_counting} we will describe the Glauber dynamics Markov Chain mentioned above in detail, and look at some of its properties. In particular we will look at its mixing time, the rate of convergence to its limiting distribution. In chapter \ref{ch:rapid_mixing} we expand the theory by looking at two distinct ways of showing a Markov chain mixes fast. Then in chapter \ref{ch:applications} we will use this to construct a new chain for sampling vertex covers, and improve upon the Glauber dynamics. Finally in chapter \ref{ch:empirical} we will perform an empirical analysis of the Glauber dynamics by implementing the algorithm on a computer.