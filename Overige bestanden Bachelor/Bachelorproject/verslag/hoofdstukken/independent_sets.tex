% !TeX root = ../verslag.tex

In this section we aim to construct a process to sample from the set of independent sets of some graph $G$. We will restrict our procedure to so-called claw-free graphs, since it's much easier to construct a rapidly mixing Markov chain on these graphs. We will adapt the method used in \citet[Section 5]{jerrum03} to sample independent sets instead of matchings. We start off by defining a claw-free graph.
\begin{definition}
The \emph{claw graph} is a graph with $4$ vertices: $\{v, w_1, w_2, w_3\}$ and 3 edges: $(v, w_i)$ for $i = 1, 2, 3$. A graph is said to be \emph{claw-free} if there exists no subset $A \subseteq V(G)$ such that the induced subgraph $G[A]$ is isomorphic to the claw graph.
\end{definition}
\begin{remark}\label{clawneighbours}
Note that in any claw-free graph $G$, if there exist 3 vertices $w_1, w_2, w_3$ that are all adjacent to some vertex $v$, then one of the edges $(w_i, w_j)$ for some $i \neq j$ must be present in $G$ since otherwise $\{w_1, w_2, w_3, v\}$ would induce a subgraph isomorphic to the claw graph.
\end{remark}
Recall that a \emph{independent set} is a subset $A$ of $V(G)$ such that the induced subgraph contains no edges. We will define the \emph{weight} of a independent set $A$ to be $\lambda^{|A|}$, where $\lambda$ is some positive constant. For many problems in statistical physics, one is then interested in sampling from the set of all independent sets with probability proportional to their weight, which gives us the distribution
\[
\pi(A) = \frac{\lambda^{|A|}}{\sum_{B \subseteq V(G)}\lambda^{|B|}},
\]
where the sum is over all independent sets $B \subseteq V(G)$. 
\newpage
We start by defining a Markov chain on the set of all independent sets.
\begin{definition}[Independent set chain]\label{vertexcoverchain}
Define a Markov chain $(X_t)$ with state space $\Omega = \Omega(G)$ the set of all independent sets on $G$, whose transition probabilities are described by the following procedure:
\begin{enumerate}
\item Select a vertex $v \in G$ uniformly at random.
\item There are four mutually exclusive possibilities:
\begin{enumerate}[a)]
\item If $v \in X_t$, let $X' = X_t \setminus \{v\}$. 
\item If $v \not \in X_t$ and $w \not \in X_t$ for any $w \in N(v)$, let $X' = X_t \cup \{v\}$.
\item If $v \not \in X_t$ and there is exactly one $w \in N(v)$ that is a member of $X_t$, let $X' = \(X_t \setminus \{w\}\) \cup \{v\}$. 
\item If $v \not \in X_t$ and $|N(v) \cap X_t|  >1$ let $X' = X_t$. 
\end{enumerate}
\item With \emph{acceptance probability} $\frac{1}{2}\min\{1, \pi(X_t)/\pi(X')\}$, set $X_{t + 1} = X'$, otherwise set $X_{t + 1} = X_t$. 
\end{enumerate}
\end{definition}
The acceptance probability in step (3) is there to ensure we arrive at the correct limiting distribution. It is often referred to as a Metropolis acceptance probability, see \citet[Section 5.5, p210]{norris97} for a more detailed description. The factor $\frac{1}{2}$ is to make the chain `lazy', to assure the chain is aperiodic.
\begin{proposition}
Let $G$ be a claw-free graph, and $\lambda > 0$. The independent set chain on $G$ is ergodic, with limiting distribution $\pi$ given by
\[
\pi(A) = \frac{\lambda^{|A|}}{\sum_{B \subseteq V(G)}\lambda^{|B|}}.
\]
\end{proposition}
\begin{proof}
The chain is clearly aperiodic, since $P(A, A) \geq \frac{1}{2}$ for any state $A$ since the acceptance probability in step (3) is at most $\frac{1}{2}$. And the chain is irreducible, since for any two independent sets $A, A'$ we can move from $A$ to $A'$ by first removing all vertices from $A$ using transitions of type (a), and then adding all of the vertices of $A'$ with transitions of type (b).

We now wish to find the limiting distribution. Consider two independent sets $A, A'$ with $P(A, A') > 0$ and assume without loss of generality $\pi(A) \leq \pi(A')$. Then by definition of the acceptance probability we have (note that there is exactly one vertex we can choose to move from $A$ to $A'$ or from $A'$ to $A$) 
\begin{align*}
P(A, A') = \frac{1}{2n}; \qquad 
P(A', A) = \frac{1}{2n}\frac{\pi(A)}{\pi(A')}.
\end{align*}
We thus obtain
\[
\pi(A)P(A, A') = \pi(A')P(A', A) = \frac{1}{2n}\min\{\pi(A), \pi(A')\}. 
\]
By Proposition \ref{detailedbalance} it follows that $\pi$ is the limiting distribution, since the limiting distribution is unique.
\end{proof}
We now aim to bound the mixing time of the independent set chain. We will use the canonical paths method, so we start by defining the set $\Gamma$ of canonical paths. Given two independent sets $I$ (initial) and $F$ (final), we need to construct a canonical path $\gamma_{IF}$ from $I$ to $F$ in the adjacency graph on $\Omega(G)$ of the independent set chain. We start by looking at the difference between the two independent sets with a helpful subgraph, think of this as a sort of ``symmetric difference''. 
\begin{definition}
Given a graph $G$ and two independent sets $I$, $F$ in $G$, we define the graph $\Delta_G(I, F)$ as the graph with vertex set $V(G)$, and edge set
\[
E = \{e \in E(G) \mid e = (v_1, v_2) \text{ for some } v_1 \in I, v_2 \in F\}.
\]
As $I$ and $F$ are independent sets it is clear that the edge set of $\Delta_G(I, F)$ is equal to the edge set of the subgraph of $G$ induced by $I \cup F$, but it is helpful to think about it using this definition.
\end{definition}
\begin{proposition}
If $G$ is a claw-free graph, then any vertex has degree at most $2$ in $\Delta_G(I,F)$. Thus $\Delta_G(I, F)$ thus partitions into a bunch of even cycles $C_1, \dots, C_k$, paths $P_1, \dots, P_l$, and a set of isolated vertices $Q_1, \dots, Q_r$. 
\end{proposition}
\begin{proof}
We show that any vertex in $v \in \Delta_G(I, F)$ has degree at most $2$, the partitioning follows immediately. If $v \not \in I$ and $v \not \in F$ then clearly $N_{\Delta_G(I, F)}(v) = \emptyset$. If $v \in I$ and $v \in F$, then any neighbour of $v$ can't be in $I$ or $F$ as both are independent sets, hence in this case we also have $N_{\Delta_G(I, F)}(v) = \emptyset$ by definition. 

Thus suppose without loss of generality $v \in I, v \not \in F$. If $v$ were to have three neighbours in $\Delta_G(I, F)$, then $v$ also has three neighbours in $G$, say $w_1, w_2, w_3$. By Remark \ref{clawneighbours}, there most exist some edge between the $w_i \in G$, so without loss of generality we may assume $(w_1, w_2) \in E(G)$. Since $v \in I$, we must have $w_i \in F$ by definition of $\Delta_G(I, F)$. But then $F$ is not a independent set since $(w_1, w_2) \in E(G)$. We conclude that $v$ has at most two neighbours in $\Delta_G(I, F)$. 

The partition follows immediately, to see that the cycles have even length note that they must alternate between vertices in $I$ and $F$.
\end{proof}
We now construct our set of canonical paths $\Gamma$. Fix some ordering of the vertices of $G$. Order the components of $\Delta_G(I, F)$ by smallest vertex member. For each component we may identify a ``start vertex'', in the case of a cycle this will be the lowest vertex that is in $I$, in the case of a path it will be the lowest endpoint (which can be in $I$ or $F$). We orient each path away from its start vertex. We orient each cycle by moving from the start vertex to the lowest of the two neighbours.

To get from $I$ to $F$, we process the components of $\Delta_G(I, F)$ in their respective order. We process each component by the following procedure:
\begin{itemize}
\item For any isolated vertex $Q_i$ that is contained in either $I$ or $F$ (but not both), we add or remove it using a transition of type (a) or (b). Any other isolated vertex $Q_i$ (which is in both $I$ and $F$ or neither) we will skip entirely.
\item For any path $P_i$, if the start vertex is in $I$, we first remove the start vertex using a transition of type (a). We then swap all vertices along the path using a transition of type (c), and in case of a path of odd length we will add the end vertex back using a transition of type (a). If the start vertex is not in $I$, we will start immediately with the type (c) transitions.
\item For any cycle $C_i$, the start vertex is in $I$. Thus remove the start vertex using a transition of type (a). Then move along the orientaion of the cycle, swapping vertices using a transition of type (c). Add the final vertex adjacent to the start vertex back using a transition of type (b).
\end{itemize}

Note that none of our canonical paths ever use a transition of type (d), they will only use transitions of the form $(A, A')$ with $A \neq A'$. Remember that the congestion involves a sum over all $(I, F)$ for which $\gamma_{IF}$ uses a specific transition $t = (A, A')$. We collect all those $(I, F)$ in the following definition. 
\begin{definition}
Let $G$ be a claw-free graph, and $\Gamma = \{\gamma_{IF}\}$ some set of canonical paths from $I$ to $F$ for each $I, F \in \Omega(G)$. Then we define for any transition $t = (A, A')$ in the chain with $A \neq A'$
\[
\text{cp}(t) := \{(I, F) \mid t \in \gamma_{IF}\},
\]
the set of all pairs $(I, F)$ whose canonical path $\gamma_{IF}$ uses transition $t$. 
\end{definition}

Since the congestion will eventually include a sum over all $(I, F) \in \text{cp}(t)$, in order to bound the congestion it is helpful to bound $|\text{cp}(t)|$. To bound the number of elements in $\text{cp}(t)$, we now construct an (almost) injective encoding $\eta_t: \text{cp}(t) \to \Omega$, which will allow us to bound $|\text{cp}(t)|$ from above by something in size similar to $|\Omega|$. However, as constructing this injection is quite cumbersome, we need a good way of describing the current `state', given some transition $t = (A, A')$ and $(I, F) \in \text{cp(t)}$. To make describing such a state easier, we  introduce some terminology. For now, we will always fix some transition $t = (A, A')$, and some $(I, F) \in \text{cp}(t)$.
\begin{definition}
Let $G$ be a claw-free graph and $\Gamma = \{\gamma_{IF}\}$ be the set of canonical paths for $G$ we just defined. For some fixed transition $t = (A, A')$, and $(I, F) \in \text{cp}(t)$ we define the following terminology:

Any vertex that is changed by $t$ will be referred to as the current vertex. Note that there can be two current vertices, in a transition of type (c). The \emph{current component} is the component in $\Delta_G(I, F)$ containing the current vertex (vertices). A component is \emph{processed} if it comes before the current component in the ordering of the components of $\Delta_G(I, F)$, and \emph{unprocessed} if it comes after the current component. If there are two current vertices, the current component is a cycle or a path. In that case, we will distinguish the first/second current vertex by order of appearance if we follow the cycle/path from the start vertex along its orientation.

A vertex in $\Delta_G(I, F)$ is called \emph{processed/unprocessed} if it is in a processed/unprocessed component. Furthermore, if the current component is a path $P_i = (p_1, \dots, p_k)$ with start vertex $p_1$, let $j$ be such that $p_j$ is the (first) current vertex. Any $p_l$ with $l < j$ is also said to be \emph{processed} and any $p_l$ with $l > j + 1$ is said to be \emph{unprocessed}. Similarly, if the current component is a cycle $C_i = (c_0, \dots, c_k, c_{k + 1} = c_0)$ (with start vertex $c_0$), let $j$ be such that $c_j$ is the (first) current vertex. Any $c_l$ with $0 < l < j$ is also said to be \emph{processed} and any $c_l$ with $l > j + 1$ is said to be \emph{unprocessed}. We will refer to $c_0$ as the \emph{special cycle vertex} (if it exists).
\end{definition}
We are now ready to construct a independent set $\eta_t$. 
\begin{definition}
Let $G$ be a claw-free graph and $\Gamma = \{\gamma_{IF}\}$ be the set of canonical paths for $G$ defined before. For some fixed transition $t = (A, A')$, and $(I, F) \in \text{cp}(t)$, define $\eta_t(I, F) \subseteq \Delta_G(I, F)$ as the set of vertices
\[
\eta_t(I, F) := \{w \in I \mid w \text{ is processed}\} \cup \{w \in F \mid w \text{ is unprocessed}\}.
\]
In particular, all of the current and special cycle vertices (that exist) are not contained in $\eta_t(I, F)$. 
\end{definition}

We verify that this is well-defined.

\begin{proposition}
Let $G$ be a claw-free graph and $\Gamma = \{\gamma_{IF}\}$ be a set of canonical paths for $G$. For some fixed transition $t = (A, A')$, and $(I, F) \in \text{cp}(t)$, the set of vertices $\eta_t(I, F)$ defined above defines a independent set in $G$ for any transition $t = (A, A')$ and $(I, F) \in \text{cp}(t)$. 
\end{proposition}

\begin{proof}
Since $\eta_t(I, F) \subseteq I \cup F$, it's clear that any edges present in the subgraph induced by $\eta_t(I, F)$ must be present in $\Delta_G(I, F)$, and thus contain one endpoint in $F$ and one endpoint in $I$. Therefore any such edges must be included in some component $P_i, C_i$ or $Q_i$. Since $I$ and $F$ are independent sets and $\eta_t(I, F)$ agrees with either $I$ or $F$ on any component that isn't the current component, the only way for $\eta_t(I, F)$ to include some edge from $G$ is if it contains an edge in the current component. Clearly the current component must be a path or a cycle since a single point doesn't contain any edges.

If the current component is a path, then the current vertex (vertices) of the current component are not in $\eta_t(I, F)$. And if we follow the path along it's orientation, it agrees with $I$ before the current vertex (vertices) and with $F$ after the current vertex (vertices). Since $I$ and $F$ are independent sets, it can't contain any edges. 

Finally, if the current component is a cycle, the argument for a path works in the same way, except that the edge involving $(c_0, c_{2k})$ might be present. But this can't happen since we explicitly let the special cycle vertex $c_0 \not \in \eta_t(I, F)$. 
\end{proof}
This shows that $\eta_t: \text{cp}(t) \to \Omega$ is well defined. The following proposition shows that knowing $\eta_t(I, F)$ and $(A, A')$, we can almost recover $I \cup F$ completely, which will help us in making $\eta_t$ injective. 
\newpage
\begin{proposition}\label{prop:vertex_set_partition}
Let $G$ be a claw-free graph and $\Gamma = \{\gamma_{IF}\}$ be a set of canonical paths for $G$. For some fixed transition $t = (A, A')$ and $(I, F) \in \text{cp}(t)$, and any vertex $w \in I \cup F$ (that is not a current vertex or the special cycle vertex), one of the following two cases holds.
\begin{enumerate}[I)]
\item $w$ is in exactly one of $I$ or $F$, and in exactly one of $A$ or $\eta_t(I, F)$. 
\item $w$ is in both $I$ and $F$, and in both $A$ and $\eta_t(I, F)$. 
\end{enumerate}
\end{proposition}
\begin{proof}
Note that if $w$ is processed, then $w \in A$ if and only if $w \in F$, and $w \in \eta_t(I, F)$ if and only if $w \in I$. And similarly, if $w$ is unprocessed, then $w \in A$ if and only if $w \in I$, and $w \in \eta_t(I, F)$ if and only if $w \in F$. 
\end{proof}

We can thus recover all of $I \cup F$, except for the special cycle vertex (if it exists). We can fix this by encoding an extra bit of information.

\begin{definition}
Let $G$ be a claw-free graph and $\Gamma = \{\gamma_{IF}\}$ be a set of canonical paths for $G$. Fix some transition $t = (A, A')$. Define $\mu_t: \text{cp}(t) \to \{0, \dots, n\}$ as follows: For any $(I, F) \in \text{cp}(t)$, let $\mu_t(I, F)$ be $0$ if the special cycle vertex does not exist, and otherwise be the index of the special cycle vertex in the vertex ordering of $G$.
\end{definition}

\begin{proposition}
Let $G$ be a claw-free graph and $\Gamma = \{\gamma_{IF}\}$ be a set of canonical paths for $G$. For every transition $t$, the function 
\[
\eta_t': \text{cp}(t) \to \Omega \times [n]: (I, F) \mapsto (\eta_t(I, F), \mu_t(I, F))
\]
is injective.
\end{proposition}
\begin{proof}
Let $t = (A, A')$ be some transition, and $(I, F) \in \text{cp}(t)$. We wish to reconstruct $I$ and $F$ knowing only $t$ and $\text{cp}(t)$, if we can do this then the mapping is clearly injective. By Proposition \ref{prop:vertex_set_partition}, $I \cup F$ can be recovered as a set since $A \cup A' \cup \eta_t(I, F)$ will contain all vertices in $I \cup F$, except the special cycle vertex (if it exists). But we can still find out if it exists and what vertex it is by looking at $\mu_t(I, F)$. 

Since $\Delta_G(I, F)$ is simply the subgraph induced by $I \cup F$, we can thus retrieve all of the components $Q_i, P_i$ and $C_i$. We know the order we were processing the components in since that was only determined by our fixed order of the vertices of $G$. We can thereby also figure out which vertices have been processed and which haven't, as that was only determined by our vertex ordering and we know the current vertex (the only vertex in $A \cap A'$).

We know if any vertex has been processed. Now any processed vertex is in $I$ if and only if it is in $\eta_t(I, F)$, and in $F$ if and only if it is in $A$. Any unprocessed vertex is in $F$ if and only if it is in $\eta_t(I, F)$, and in $I$ if and only if it is in $A$. We can thus tell, for any processed or unprocessed vertex whether it needs to be in $I$, $F$, or both. 

Any current vertex is in $I$ if and only if it is in $A$, and in $F$ if and only if it is in $A'$ (it can't be in both, because then we would have skipped it). 

Finally, if we found out in which sets all other vertices are, we can then figure out where the special cycle vertex would be. If its neighbours are in $I$ then it is in $F$ and vice versa. It can't be in both, because then it would be an isolated point in $\Delta_G(I, F)$.
\end{proof}
Thus we have found an injective mapping from the set of canonical paths to a set we can bound in size. We can now use this to bound the congestion, as we do in the following lemmas.
\begin{lemma}\label{pimpifbound}
Let $G$ be a claw-free graph, $\lambda \in \RR_{>0}$, and $\Gamma = \{\gamma_{xy}\}$ be a set of canonical paths on the collection of independent sets $\Omega(G)$. For all transitions $t = (A, A')$ and all pairs $(I, F) \in \text{cp}(t)$, it holds that
\[
\pi(I)\pi(F) \leq 2n \overline{\lambda}^2\pi(A) P(A, A') \pi(\eta_t(I, F)),
\]
where $\overline{\lambda} = \max\{1, \lambda\}$.
\end{lemma}
\begin{proof}
Write $C = \eta_t(I, F)$. By Proposition \ref{prop:vertex_set_partition} and since there are at most 2 current vertices and one special cycle vertex, $|I| + |F| \leq 3 + |A| + |C|$ (and the same holds for $A'$ as they only differ in the current vertex/vertices). Thus
\[
\pi(I)\pi(F) \leq \overline{\lambda}^3\pi(A) \pi(C), \qquad \pi(I)\pi(F) \leq \overline{\lambda}^3\pi(A') \pi(C),
\]
and
\[
\pi(I)\pi(F) \leq \overline{\lambda}^3\min\{\pi(A), \pi'(A)\}\pi(C) = 2n\overline{\lambda}^3\pi(A)P(A, A')\pi(C),
\]
as desired.
\end{proof}
We can now bound the congestion.
\begin{lemma}\label{congestionboundvertexcover}
Let $G$ be a claw-free graph and $\lambda \in \RR_{>0}$. With the set of canonical paths $\Gamma$ as defined above, the congestion $\rho(\Gamma)$ is bounded above by
\[
\rho(\Gamma) \leq 2n^2(n + 1)\overline{\lambda}^3.
\]
\end{lemma}
\begin{proof}
This follows from a straightforward calculation, we see that
\begin{align*}
\rho &= \max_{t = (A, A')} \(\frac{1}{\pi(A)P(A, A')} \sum_{(I, F) \in \text{cp}(t)} \pi(I)\pi(F) |\gamma_{IF}| \) \\
&\leq \max_{t = (A, A')}\(2n \overline{\lambda}^3 \sum_{(I, F) \in \text{cp}(t)} \pi(\eta_t(I, F)) |\gamma_{IF}| \) &(\text{Lemma \ref{pimpifbound}}) \\
&\leq 2n(n + 1)\overline{\lambda}^3 \pi(\Omega)n &(\text{$\eta_t'$ is injective and $|\gamma_{IF}| \leq n$}) \\
&= 2n^2(n + 1)\overline{\lambda}^3,
\end{align*}
which gives the desired bound.
\end{proof}
Note that our bound is weaker than Jerrum's by an extra factor of $n + 1$ and $\lambda$, this is because we had to modify $\eta_t$ a bit to be injective, and our chain can change two vertices at once adding an extra factor $\lambda$.
We can now bound the mixing time of the independent set chain with the canonical paths theorem.
\begin{theorem}
Let $G$ be a claw-free graph on $n$ vertices and $\lambda \in \RR_{>0}$. The mixing time of the independent set chain described in Definition \ref{vertexcoverchain} on $G$ is bounded above by
\[
\tau(\varepsilon) \leq n^2(n + 1) \overline{\lambda}^2\(2\log(\varepsilon^{-1}) + n (|\log(1 + \lambda)| + |\log(\lambda)|)\).
\]
\end{theorem}
\begin{proof}
By Theorem \ref{canonical_paths} and Lemma \ref{congestionboundvertexcover} together, we obtain the identity
\[
\tau(\varepsilon) \leq  n^2(n + 1)\overline{\lambda}^2\(2 \log(\varepsilon^{-1}) + \sup_{x \in \Omega} \log(\pi(x)^{-1})\).
\]
Now 
\[
\log(\pi(x)^{-1}) = \log\(\frac{\sum_{B \subseteq V(G)}\lambda^{|B|}}{\lambda^{|x|}}\) \leq \left|\log\( \sum_{B \subseteq V(G)}\lambda^{|B|}\)\right| + |\log(\lambda^{-|x|})|.
\]
In the worst case, $G$ doesn't have any edges and any $B \subseteq V(G)$ is an independent set. Hence we cannot do any better then crudely bounding the number of independent sets of size $i$ by $\binom{n}{i}$, yielding
\[
\sum_{B \subseteq V(G)}\lambda^{|B|} \leq \sum_{i = 0}^n \binom{n}{i} \lambda^i = (1 + \lambda)^n.
\]
As the empty set is always an independent set, we also have $1 \leq \sum_{B \subseteq V(G)}\lambda^{|B|}$, thus
\[
\left|\log\( \sum_{B \subseteq V(G)}\lambda^{|B|}\)\right| \leq n \log(1 + \lambda).
\]
Combined with the bound $|x| \leq n$ we obtain 
\[
\log(\pi(x)^{-1}) \leq n |\log(1 + \lambda)| + n |\log(\lambda)|,
\]
which gives the upper bound.
\end{proof}
\newpage