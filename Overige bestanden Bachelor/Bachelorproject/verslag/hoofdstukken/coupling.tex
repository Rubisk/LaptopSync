% !TeX root = ../verslag.tex
The first method to show that a Markov chain mixes rapidly is often referred to as coupling. A good intuition for a coupling is two people walking around randomly in a city until they meet, and once they meet they will walk together.
\begin{definition}
Suppose we are given a Markov chain $M$ with a transition matrix $P$ and state space $\Omega$. A coupling of $M$ is a Markov chain of the form $(x_t, y_t)_{t \in \NN}$ on $\Omega \times \Omega$ such that
\begin{enumerate}[1)]
\item The marginal chains $(x_t, \cdot)$ and $(\cdot, y_t)$ are copies of $M$. More precisely, at any time $t$ the identities
\begin{align}
\PP(x_{t + 1} = x' \mid x_t = x) &= P(x,x'),\label{couplingconditional1} \\
\PP(y_{t + 1} = y' \mid y_t = y) &= P(y,y') \label{couplingconditional2}
\end{align}
hold.
\item If $x_t = y_t$, then $x_{t + 1} = y_{t + 1}$.
\end{enumerate}
\end{definition}
\begin{remark}\label{couplingcoefficients}
We can look at a coupling as if it were a linear map between jump probabilities. Suppose $(x_t, y_t) = (\xi, \omega)$. Given a jump $\xi \to \xi'$ for $x_t$, we can look at the probability of each of the individual jumps $\omega \to \omega'$ appearing for $y_t$. More precisely, using equation (\ref{couplingconditional2}) we can identify a coupling with the coefficients
\[
a_{\xi \to \xi'| \omega \to \omega'}(t) = \PP(y_{t + 1} = \xi' | x_{t + 1} = \omega' \text{ and } (x_t, y_t) = (\omega, \xi)),
\]
since one can then compute the transition probabilities of the coupling $(x_t, y_t)$ immediately by
\begin{equation}
\label{bayescoupling}
\PP((x_{t + 1}, y_{t + 1}) = (\omega', \xi') \mid (x_t, y_t) = (\omega, \xi)) = a_{\xi \to \xi'| \omega \to \omega'}P(\omega, \omega').
\end{equation}
\end{remark}
However, not every coupling is very interesting. Consider for example the coupling that selects $x_{t + 1}$ and $y_{t + 1}$ independently if $x_t \neq y_t$, and selects $x_{t + 1}$ by simulating the chain for one step starting at $x_t$ then setting $y_{t + 1} = x_{t + 1}$ whenever $x_t = y_t$. One can easily verify that this is a coupling, however as long as $x_t \neq y_t$ we are basically just running the chain twice.

However, one could imagine that if coupling occurs rapidly (i.e. $X_T = Y_T$ occurs for small $T$ with high probability) for some particular coupling (even if the starting states are very far apart), then the distance between any two distributions should decline rapidly, since probability mass can move around the state space quickly. Then to establish rapid mixing, all one has to do is find a coupling that `couples fast'. 

The following lemma makes this intuition a bit more precise. 
\begin{lemma}[Coupling Lemma]\label{couplinglemma}
Let $P$ be the transition matrix of a Markov chain with state space $\Omega$. Remember that $P^t(x, \cdot)$ is defined as the distribution on $\Omega$ obtained by simulating the chain for $t$ steps with initial state $x$. Let $(x_t, y_t)$ be a coupling of the Markov chain with initial state $(x, y) \in \Omega \times \Omega$. Set 
\[
T_{xy} = \min \{t \in \NN \mid x_t = y_t\}.
\]
Then $T_{xy}$ is a stopping time, and we have the upper bound
\[
d_{\text{TV}}(P^t(x, \cdot), P^t(y, \cdot)) \leq \PP(x_t \neq y_t) = \PP(T_{xy} > t).
\]
\end{lemma}
\begin{proof}
The proof we give is thanks to \citet[Chapter 4, Lemma 4]{diaconis}. 

Let $\Delta = \{(s, s) \mid s \in \Omega\} \subset \Omega \times \Omega$ be the diagonal and $\Delta^c = \Omega^2 \setminus \Delta$, and pick any $A \subseteq \Omega$. Let $Q^t$ be the joint distribution of $(x_t, y_t)$, that is
\begin{align*}
Q^t(\hat x, \hat y) &= \PP(x_t = \hat x, y_t = \hat y) \qquad &\forall \hat x, \hat y \in \Omega.
\end{align*}
Note that $P^t(x, A) = \PP(x_t \in A) = Q^t(A \times \Omega)$ and similarly $P^t(y, A) = Q^t(\Omega \times A)$. The main idea is that therefore $|P^t(x, A) - P^t(y, A)|  \leq Q^t(\Delta^c \cap A)$. In fact, we have
\begin{align*}
&|P^t(x, A) - P^t(y, A)| = |Q^t(A \times \Omega) - Q^t(\Omega \times A)| \\
&= |Q^t((A \times \Omega) \cap \Delta) + Q^t((A \times \Omega) \cap (\Delta^c)) - (Q^t((\Omega \times A) \cap \Delta) + Q^t((\Omega \times A) \cap (\Delta^c)))|
\end{align*}
Now since $(A \times \Omega) \cap \Delta = \{(a, a) \mid a \in A\} = (\Omega \times A) \cap \Delta$, only the terms outside the diagonal remain. In other words, one has that
\[
|P^t(x, A) - P^t(A, y)| =  |Q^t((A \times \Omega) \cap (\Delta^c)) - Q^t((\Omega \times A) \cap (\Delta^c))|
\]
The right hand side is obviously at most $Q^t(\Delta^c)$, as it is a difference of two numbers between $0$ and $Q^t(\Delta^c)$. However, $Q^t(\Delta^c) = \PP(x_t \neq y_t)$, because
\begin{align*}
\PP(x_t = y_t) = \sum_{s \in \Omega} \PP(x_t = s \land y_t = s) = \sum_{s \in \Omega} Q^t(s, s) = Q^t(\Delta). 
\end{align*}
Thus $|P^t(x, A) - P^t(y, A)| \leq \PP(x_t \neq y_t)$. Since $A$ was an arbitrary subset of $\Omega$ we see that $d_{\text{TV}}(P^t(x, \cdot), P^t(y, \cdot)) \leq \PP(x_t \neq y_t)$. 

To see that $\PP(x_t \neq y_t) = \PP(T_{xy} > t)$, remember that if $(x_t, y_t)$ is a coupling and $x_s = y_s$ for some $s < t$, then also $x_t = y_t$. Thus if $x_t \neq y_t$ then also $T_{xy} > t$ and vice versa.
\end{proof}
It is in fact possible to show some Markov chains mix rapidly with purely the coupling lemma, however this becomes quite a lot of work. The problem is that you have to define transition probabilities for each pair of states $(x, y)$, even if they are very far apart. The main issue with the coupling lemma is that the initial states $x$ and $y$ might be very far apart, and it can thus be hard to find a good way to couple them together. The idea is to only consider states that are very close together in some sense that might be useful for our specific chain. We then use this to implicitly define our coupling for states that are farther apart.
\begin{definition}
A neighbour relation on a state space $\Omega$ is a graph with vertex set $\Omega$. If $x$ and $y$ are \emph{neighbours}, we write $x \sim y$. (In particular, we can choose this relation to be anything we want, $x$ and $y$ don't have to be adjacent in the sense that $P(x, y) > 0$).
\end{definition}
We can then consider paths in our state space obtained by jumping between neighbours. The general idea is that if one can couple two neighbours together quickly, and find a path of neighbours between any two arbitrary states, then by ``jumping between neighbours'' the two end states will eventually couple together. The precise construction will be shown in the next theorem.
\begin{theorem}[Path Coupling] \label{pathcoupling} Let $M$ be a Markov Chain with state space $\Omega$ and some neighbour relation $\sim$ on $\Omega$. Let $\Phi: \Omega \times \Omega \to \{0, \dots, D\}$ be a metric such that for all $\sigma, \xi \in \Omega$ there exist $\eta_1, \dots, \eta_k \in \Omega$ such that $\eta_1 = \sigma, \eta_k = \xi$, $\eta_1 \sim \eta_2 \sim \dots \sim \eta_k$ and
\[
\Phi(\sigma, \xi) = \sum_{i=1}^{k - 1} \Phi(\eta_i, \eta_{i + 1}).
\]
Suppose there exists $\beta < 1$ and a coupling $(\sigma_t, \xi_t)$ of $M$ such that if $\sigma_t \sim \xi_t$ we have
\begin{equation}
\EE[\Phi(\sigma_{t + 1}, \xi_{t + 1})] \leq \beta \Phi(\sigma_t, \xi_t). \label{pathcouplingbeta}
\end{equation}

Then
\[
\tau(\varepsilon) \leq \frac{\log(D\varepsilon^{-1})}{\log(\beta^{-1})} \leq \frac{\log(D\varepsilon^{-1})}{1 - \beta}.
\]
\end{theorem}
\begin{proof}
For any $\sigma, \xi \in \Omega$, let the coefficients $a_{\xi \to \xi' \mid \sigma \to \sigma'}(t)$ be the coefficients corresponding to the original coupling (as in Remark \ref{couplingcoefficients}).

We start by constructing a new coupling $(\tilde{\sigma}_t, \tilde{\xi}_t)$. 
Pick any state $(\tilde{\sigma}_t, \tilde{\xi}_t)$. We wish to define a process to select $(\tilde{\sigma}_{t + 1}, \tilde{\xi}_{t + 1})$.
If $\tilde{\sigma}_t= \tilde{\xi}_t$ pick $\tilde{\sigma}_{t + 1}$ by simulating the Markov chain for one step starting at $\tilde{\sigma}_t$, and let $\tilde{\xi}_{t + 1} = \tilde{\sigma}_{t + 1}$. 

Otherwise, pick some fixed path $\tilde \sigma_t = \eta_1, \dots, \eta_k = \tilde \xi_t$. We select new states $\eta_i'$ according to the probabilities (here $P$ denotes the matrix corresponding to the original Markov chain)
\begin{align*}
\PP(\eta_1' = x) &= P(\eta_1, x), \\
\PP(\eta_i' = y | \eta_{i - 1}' = x) &= a_{\eta_i \to y \mid \eta_{i - 1} \to x} \qquad \qquad \forall i \in \{2, \dots, k\}.
\end{align*}
We then let $(\tilde{\sigma}_{t + 1}, \tilde{\xi}_{t + 1}) = (\eta_1', \eta_k')$.  What we're doing here is basically coupling together all of the $\eta_i,\eta_{i + 1}$ according to the original coupling.

To see that this defines a coupling, we only need to show that equations (\ref{couplingconditional1}), (\ref{couplingconditional2}) hold for $\tilde \sigma_t, \tilde \xi_t$, since condition (2) holds by definition. To do this, we show by induction on $i$ that 
\[
\PP(\eta_i' = y') = P(\eta_i, y').
\]
The case $i = 1$ follows directly from the definition of $\eta_1'$. Now suppose it holds for $i - 1$. Conditioning on $\eta_{i - 1}'$ we get
\begin{align*}
\PP(\eta_i' = y') &= \sum_{x' \in \Omega} \PP(\eta_i' = y' \mid \eta_{i - 1}' = x')\PP(\eta_{i - 1}' = x') \\
&= \sum_{x' \in \Omega} a_{\eta_i \to y' \mid \eta_{i - 1} \to x'}\PP(\eta_{i - 1}' = x') \\
&= \sum_{x' \in \Omega} a_{\eta_i \to y' \mid \eta_{i - 1} \to x'}P(\eta_{i - 1}, x') \\
&= P(\eta_i, y'),
\end{align*}
where the third line follows from the induction hypotheses and the last line follows since the original coupling was a coupling. By taking $i = 1$ and $i = k$ respectively, we get that equations (\ref{couplingconditional1}), (\ref{couplingconditional2}) hold for $\tilde{\sigma}_t, \tilde{\xi}_t$; hence this procedure defines a coupling.

Now since $\eta_1' \sim \dots \sim \eta_k'$ is a way of traversing $\Omega$ from $\tilde{\sigma}_{t + 1}$ to $\tilde{\xi}_{t +1}$ (although not necessarily a path), we have by the triangle inequality that
\begin{align*}
\EE[\Phi(\tilde{\sigma}_{t + 1}, \tilde{\xi}_{t + 1})] &\leq \sum_{i = 1}^{k - 1} \EE[\Phi(\eta_i', \eta_{i + 1}')] \\
&\leq \sum_{i = 1}^{k - 1} \beta \Phi(\eta_{i}, \eta_{i + 1}) &\(\text{by \ref{pathcouplingbeta}}\)\\
&\leq \beta \Phi(\tilde \sigma_t, \tilde \xi_t),
\end{align*}
where we've used in the second line that we coupled $\eta_i$ and $\eta_{i + 1}$ together according to the original coupling. If we do this for all possible states $(\tilde \sigma_t, \tilde \xi_t)$, we end up with a coupling $(\tilde \sigma_t, \tilde \xi_t)$ such that
\[
\EE[\Phi(\tilde \sigma_t, \tilde \xi_t)] \leq \beta^t\Phi(\tilde \xi_0, \tilde \sigma_0) \leq \beta^tD.
\] 
Thus for $t > \frac{\log(D\varepsilon^{-1})}{\log(\beta^{-1})}$ we have
(since if $\tilde{\sigma}_t \neq \tilde{\xi}_t$ we have $\Phi(\tilde{\sigma}_t, \tilde{\xi}_t) \geq 1$)
\[
\PP(\tilde{\sigma}_t \neq \tilde{\xi}_t) \leq \EE[\Phi(\tilde{\sigma}_{t}, \tilde{\xi}_{t})] \leq \beta^{t}D = \(e^{\log(\beta^{-1})}\)^{-t}D < e^{-\log(D\varepsilon^{-1})}D = \varepsilon.
\]
So for any fixed state $x \in \Omega$, if we let $\tilde \sigma_0 = x$ and select $\tilde \xi_0$ according to the invariant distribution $\pi$, then since $\pi$ is an invariant distribution $\tilde \xi_t$ will still have the invariant distribution. Since we defined a coupling, the marginal chain $\tilde \sigma_t$ is a copy of the original chain, therefore $\tilde \sigma_t$ has the same distribution as $P^t(x, \cdot)$. 

Let $T = T_{\tilde{\sigma}_0, \tilde{\xi}_0}$ be the stopping time given by the smallest $t$ such that $\tilde{\sigma}_t = \tilde{\xi}_t$. Recall that by Lemma \ref{couplinglemma} we had 
\[
d_{\text{TV}}(\tilde{\sigma}_t, \tilde{\xi}_t) \leq \PP(T > t) = \PP(\tilde{\sigma}_t \neq \tilde{\xi}_t).
\]
Thus for $t > \frac{\log(D\varepsilon^{-1})}{\log(\beta^{-1})}$ we have
\[
d_{\text{TV}}(P^t(x, \cdot), \pi) = d_{\text{TV}}(\tilde \sigma_t, \tilde \xi_t) \leq \PP(\tilde \sigma_t \neq \tilde \xi_t) < \varepsilon,
\]
which is precisely what we needed to show by Definition \ref{mixingtimedef}.
\end{proof}
We can now immediately see why this theory is so powerful. To show that Glauber dynamics mix rapidly whenever $k > 2\Delta(G)$, all we need to do is define a coupling between two neighbouring states such that the expected distance of the states one step later is less than one. 
\begin{corollary}\label{glaubermixingproof}
Let $G$ be a graph and $k \in \NN$. Provided $k > 2\Delta(G)$, the mixing time of the Glauber dynamics with $k$ colours on $G$ is bounded above by
\[
\tau(\varepsilon) \leq \frac{k}{k - 2\Delta}n\log(n \varepsilon^{-1}).
\]
\end{corollary}
\begin{proof}
We extend the state space $\Omega$ to the set of all colourings $C^n$. Since any proper colouring will remain a proper colouring after any transition, the set of proper colourings is a closed class. Since any improper colouring will eventually become a proper colouring, any improper colouring is a transient state and thus has weight $0$ in any invariant distribution. Therefore, an invariant distribution of this extended chain must be assign $0$ weight to any non-proper colourings, hence the invariant distribution will remain the same. It thus suffices to show this chain with extended state space mixes rapidly, since we can always interpret the original chain as this chain because starting at a proper colouring we stay at a proper colouring.

We take $\Phi$ to be the Hamming distance of two states, i.e. $\Phi(\sigma, \tau)$ is the number of vertices $v$ such that $\sigma(v) \neq \tau(v)$. Since we extended the state space it's clear that all the paths we need for the path coupling theorem exist, simply recolour all different vertices one by one. We will define a neighbour relation on $\Omega$ by $\sigma \sim \tau$ if and only if $\Phi(\sigma, \tau) = 1$. We can obviously find a path with length $\Phi(\sigma, \tau)$ from $\sigma$ to $\tau$ by recolouring the vertices in which they differ one by one.

Now given any two adjacent states $\sigma_t, \tau_t$, note that they must differ in exactly one vertex $v$. We define our coupling $(\sigma_t, \tau_t)$ as follows. Obtain $\sigma_{t + 1}$ from $\sigma_t$ by simulating the Glauber Dynamics for one step. If we obtain $\sigma_{t+ 1}$ from $\sigma_t$ by trying to recolour any vertex $x$ not in $N(v)$ (in particular we might have $x = v$) with colour $c$, let $\tau_{t + 1}$ also be given by recolouring that $x$ with colour $c$. Given that $\sigma_{t + 1}$ is obtained from $\sigma_t$ by trying to color $w \in N(v)$ with colour $c$, we obtain $\tau_{t + 1}$ from $\tau_t$ by trying to color vertex $x$ in $\tau_t$ with color $c'$, where $c'$ is defined as
\[
c' = \begin{cases}
c &\text{if $c \not \in \{\sigma_t(v), \tau_t(v)\}$,} \\
\tau_t(v) &\text{if $c = \sigma_t(v)$,} \\
\sigma_t(v) &\text{if $c = \tau_t(v)$.}
\end{cases}
\]
One can easily verify that both $\tau$ and $\sigma$ still try to recolour each vertex with every colour with probability $\frac{1}{nk}$ (as we run the original chain on $\sigma$ and only permute some colours sometimes to get the transition probabilities for $\tau$), hence this procedure defines a coupling.
Now for any $w \not \in N(v)$, for any neighbour $x$ of $w$ we have $\sigma_t(x) = \tau_t(x)$. Hence attempting to color $w$ with color $c$ will succeed for $\sigma_t$ if and only if it succeeds for $\tau_t$, hence $\sigma_{t + 1}(w) = \tau_{t + 1}(w)$. Therefore, if $w \neq v$ we have $\Phi(\sigma_{t + 1}, \tau_{t + 1}) = \Phi(\sigma_t, \tau_t)$.

If $w = v$, then we have $\Phi(\sigma_{t + 1}, \tau_{t + 1}) = 0$ if there is no $x \in N(v)$ with $\sigma_t(x) = c$, and otherwise $\Phi(\sigma_{t + 1}, \tau_{t + 1}) = \Phi(\sigma_t, \tau_t)$. There are at most $\Delta$ colors $c$ for which such $x$ exists, so since there are $k$ colors we have
\[
\EE[\Phi(\sigma_{t + 1}, \tau_{t + 1}) | \text{ we attempt to recolour $v$ }] \leq \frac{\Delta}{k} \Phi(\sigma_t, \tau_t) + \frac{k - \Delta}{k} \times 0 = \frac{\Delta}{k}.
\]

Finally, if $w \in N(v)$ then if $c \not \in \{\sigma_t(v), \tau_t(v)\}$ we see that once again attempting to recolour $\sigma_t$ will fail if and only if it fails for $\tau_t$, so we still have $\Phi(\sigma_{t + 1}, \tau_{t + 1}) = \Phi(\sigma_t, \tau_t)$. If $c = \sigma_t(v)$ then by definition $c' = \tau_t(v)$ and we have $\sigma_{t + 1} = \sigma_t, \tau_{t + 1} = \tau_t$. And if $c = \tau_t(v)$ then we have $\Phi(\sigma_{t + 1}, \tau_{t + 1}) \leq \Phi(\sigma_t, \tau_t) + 1$ since both attempt to recolour $w$. Hence
\begin{align*}
\EE[\Phi(\sigma_{t + 1}, \tau_{t + 1}) | \text{ we recolour a fixed $w \in N(v)$ }] &\leq \frac{k - 1}{k}\Phi(\sigma_t, \tau_t) + \frac{1}{k}\(\Phi(\sigma_t, \tau_t) + 1\)\\
&= \Phi(\sigma_t, \tau_t) + \frac{1}{k}.
\end{align*}
By conditioning on the vertex $w$ we attempt to recolour we see that
\begin{align*}
\EE[\Phi(\sigma_{t + 1}, \tau_{t + 1})] &\leq \frac{1}{n} \(\sum_{w \not \in N(v) \cup \{v\}} \Phi(\sigma_t, \tau_t) + \sum_{w \in N(v)} \left[ \Phi(\sigma_t, \tau_t) +  \frac{1}{k}\right] + \sum_{w = v}  \frac{\Delta}{k}\) \\
&= \frac{n - 1}{n} \times \Phi(\sigma_t, \tau_t) + \frac{|N(v)|}{kn}  + \frac{\Delta}{kn} \\
&\leq \frac{n - 1}{n} \times \Phi(\sigma_t, \tau_t) + \frac{2\Delta}{kn} \qquad \qquad \qquad \qquad \(|N(v)| \leq k\) \\
&= \(1 - \frac{k - 2\Delta}{kn}\)\Phi(\sigma_t, \tau_t).
\end{align*}
Since the Hamming distance between any two colourings is at most $n$, by Theorem \ref{pathcoupling} we see that the mixing time of the Glauber dynamics is bounded above by
\[
\tau(\varepsilon) \leq \frac{k}{k - 2\Delta}n\log(n \varepsilon^{-1}),
\]
this completes the proof.
\end{proof}
\newpage
