/**
	graph_colouring_approximator.cpp
	Purpose: Calculates approximations for number of colourings on a graph.
	@author: Wouter Rienks
	@version: 1.2 14/05/2018
	
	Invoke like
		> Graph Colouring Approximator <n> <k> <t> <s> <trials> e1 e2 e3 ...
	Where
		(int) n = number of vertices
		(int) k = number of colours
		(int) t = number of steps to simulate glauber dynamics
		(int) s = number of samples to generate per approximator
		(int) trials = number of approximations to generate

		(2x int) ei = some edge

	For example:
		> Graph Colouring Approximator 5 5 100 200 10 0 1 1 2 2 3 3 4 4 0
	Will generate a cyclic graph with 5 vertices, simulate using 5 colours, using
	100 steps to sample a colouoring, 200 colourings to sample a ratio, and write
	10 (independent) approximations to cout. The part
	0 1 1 2 2 3 3 4 4 0
	specifies the list of edges.
*/


#include <list>
#include <vector>
#include <random>
#include <iostream>
#include <algorithm>
#include <cstdio> 

using namespace std;


struct Edge {
	// Struct used to represent an edge
	Edge(int v1, int v2) : v1(v1), v2(v2) {};

	// Endpoints
	int v1;
	int v2;
};


class ColouredGraph {
	// These are private as changing them at will might cause exceptions
	// vertex count
	int n;

	// number of colours
	int k;

	// edge count 
	int m = 0;

	// Matrix of edges present
	vector<vector<bool>> edges;

	// List of colours
	vector<int> colours;

	mt19937 rng;
	uniform_int_distribution<mt19937::result_type> n_dist;
	uniform_int_distribution<mt19937::result_type> k_dist;

public:
	/**
		Class used to represent a graph to simulate Glauber Dynamics on. Much 
		faster implementation than the old python one.

		@param n number of vertices
		@param k number of colours
		@param edge_list list of edges present in the graph
	*/
    ColouredGraph(int n, int k, vector<Edge> edge_list) : n(n), k(k), n_dist(0, n - 1), k_dist(0, k - 1)
	{
		// Setup random number generator for glauber dynamics
		rng.seed(random_device()());
		
		// Read edges from list into n x n boolean matrix
		edges = vector<vector<bool>>(n, vector<bool>(n, false));
		colours = vector<int>(n, 0);
		for (Edge e : edge_list)
			add_edge(e);

		// Generate initial colouring (greedy)
		for (int i = 0; i < n; ++i)
		{
			int c = 0;
			while (!update_colour(i, c))
				c += 1;
			if (c >= k)
				throw exception("Not enough colours!");
		}
	}

	/**
		Try and recolour a vertex.
		@param v the vertex index to recolour
		@param c the colour to recolour with

		@return true if v has been recoloured to c, false if it couldn't do so
		because a neighbour had colour c already.
	*/
	bool update_colour(int v, int c) {
		for (int i = 0; i < n; ++i)
		{
			if (edges[i][v] && colours[i] == c)
				return false;
		}
		colours[v] = c;
		return true;
	}

	/**
		Add edge to the graph.
		@param e edge to add.
	*/
	void add_edge(Edge e) {
		if (!edges[e.v1][e.v2])
			m += 1;
		edges[e.v1][e.v2] = true;
		edges[e.v2][e.v1] = true;
	}

	/**
		Remove edge from the graph.
		@param e edge to remove.
	*/
	void remove_edge(Edge e) {
		if (edges[e.v1][e.v2])
			m -= 1;
		edges[e.v1][e.v2] = false;
		edges[e.v2][e.v1] = false;
	}

	/**
		Simulate (simple) Glauber Dynamics, keeping a proper colouring.
		@param t number of steps to simulate for.
	*/
	void simulate_glauber_dynamics(int t) {
		for (int i = 0; i < t; ++i) {
			int v = n_dist(rng);
			int c = k_dist(rng);
			update_colour(v, c);
		}
	}

	/**
		Calculate maximum degree (non-cached).
		@return maximum degree of a vertex in the graph.
	*/
	int max_degree() {
		int best = 0;
		for (int i = 0; i < n; ++i) {
			int i_sum = 0;
			for (bool neighbour : edges[i])
				if (neighbour) i_sum += 1;
			best = max(1, i_sum);
		}
		return best;
	}

	/**
		Retrieve the number of edges in the graph (cached)
		@return number of edges in the graph.
	*/
	int number_of_edges() {
		return m;
	}
	
	/**
		Find the "first" edge in the graph 
		(wrt to lexicographic ordering on vertices)
		@return edge in the graph 
	*/
	Edge find_next_edge() {
		for (int x = 0; x < n; ++x) {
			for (int y = 0; y < x; ++y) {
				if (edges[x][y])
					return Edge(x, y);
			}
		}
		throw exception("No edges!");
	}

	/**
		Lookup colour of a vertex. 
		@param v vertex to lookup.
		@return colour of v.
	*/
	int get_colour(int v) {
		return colours[v];
	}

	/**
		Retrieve the number of vertices in the graph (cached)
		@return number of edges in the graph.
	*/
	int number_of_vertices() {
		return n;
	}

	/**
		Retrieve the maximum number of colours
		@return maximum number of colours used for colourng
	*/
	int max_number_colours() {
		return k;
	}
};


/**
	Approximate the total number of colourings on a graph using Jerrum's algorithm.
	@param graph graph to approximate on.
	@param k number of colours to use 
	@param time number of steps to simulate glauber dynamics for sampling colouring
	@param s number of samples used to estimate Omega_k(G_i) / Omega_k(G_(i + 1))
	@param trials total number of approximations to make (i.e. repeat the algorithm this many times)
	@return vector of approximations (of length <trials>)
*/
vector<float> approximate_colouring_count(ColouredGraph graph, int time, int s, int trials) {
	int d = graph.max_degree();
	int n = graph.number_of_vertices();
	int k = graph.max_number_colours();
	vector<float> results = vector<float>(trials, (float) pow(k, n));

	// Simulate all trials simultanuously
	while (graph.number_of_edges() > 0) {
		// Remove edge from graph
		Edge next_edge = graph.find_next_edge();
		graph.remove_edge(next_edge);

		for (int trial = 0; trial < trials; ++trial) {
			// Generate approximator for z_i
			float z_i_approximator = 0;
			for (int ss = 0; ss < s; ++ss) {
				graph.simulate_glauber_dynamics(time);

				// if colouring remains proper when adding edge back, z_i = 1, otherwise z_i = 0.
				int c1 = graph.get_colour(next_edge.v1);
				int c2 = graph.get_colour(next_edge.v2);
				if (c1 != c2)
					z_i_approximator += 1;
			}
			z_i_approximator /= s;

			// Multiply trial-th result by z_i
			results[trial] *= z_i_approximator;
		}
	}
	return results;
}


/**
	Entry point.
*/
int main(int argc, char* argv[])
{
	int n = atoi(argv[1]);
	int k = atoi(argv[2]);
	int t = atoi(argv[3]);
	int s = atoi(argv[4]);
	int trials = atoi(argv[5]);
	
	int edges = (argc - 6) / 2;

	// Retrieve edges from argv and store them in vector of edges
	vector<Edge> edge_list(edges, Edge(0, 0));
	for (int i = 0; i < edges; ++i) {
		edge_list[i] = Edge(atoi(argv[2 * i + 6]), atoi(argv[2 * i + 7]));
	}

	// Create graph with edges
	ColouredGraph graph(n, k, edge_list);

	// Approximate colouring count
	vector<float> results = approximate_colouring_count(graph, t, s, trials);

	// Write approximations back to console
	for (float result : results)
		cout << result << endl;

	return 0;
}

