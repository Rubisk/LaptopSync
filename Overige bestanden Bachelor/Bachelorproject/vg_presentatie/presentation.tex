% Beamer-template voor de Universiteit van Amsterdam
% Gemaakt door Jolien Oomens
% Gebaseerd op de TUD-template van Maarten Abbink
\documentclass{beamer}
\usepackage[dutch]{babel}
\usepackage{calc}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usepackage{xcolor}

\tikzset{main node/.style={circle,fill=blue!20,draw,minimum size=1cm,inner sep=0pt},
            }
\usepackage[absolute,overlay]{textpos}
\mode<presentation>{\usetheme{uva}}
\tikzset{
  invisible/.style={opacity=0},
  visible on/.style={alt=#1{}{invisible}},
  alt/.code args={<#1>#2#3}{%
    \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
  },
}

\title[Bachelorproject]{Approximate Counting by Markov Simulation}
\subtitle{Supervisor: Viresh Patel}
\institute[UvA]{Universiteit van Amsterdam}
\author{Wouter Rienks}
\date{8 maart 2017}

\begin{document}

{
\usebackgroundtemplate{\includegraphics[width=\paperwidth,height=\paperheight]{images/background-titlepage.jpg}}%
\frame{\titlepage}
}
%
\begin{frame}{Reminder}
\begin{itemize}
  \item Approximate counting in graph theory
  \pause
  \begin{itemize}
  	\item Matchings
  	\item Colourings
  	\item Independent Sets
  	\item (Hamilton) Cycles
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{General Idea}
\begin{itemize}
  \item Define a random walk on the set of all objects
  \item With this random walk, sample (approximately) uniform random objects by simulating it for a while
  \item Use this sampling process to approximate the total number of objects
\end{itemize}
\end{frame}

\begin{frame}{Example}
\begin{figure}[h]
\begin{tikzpicture}
    \node[main node] (1) [scale=0.5] {$$};
    \node[main node] (2) [scale=0.5, left = 1.1cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\end{figure}
\pause
\begin{itemize}
  \item Colour this graph using 3 colours: \textcolor{red}{red}, \textcolor{blue}{blue} and \textcolor{green}{green}.
  \pause
  \item We can do so in six ways:
\end{itemize}

\begin{figure}[h]
\begin{tikzpicture}
    \node[main node] (1) [fill=red,scale=0.5] {$$};
    \node[main node] (2) [fill=green,scale=0.5, left = 0.4cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\qquad 
\begin{tikzpicture}
    \node[main node] (1) [fill=red,scale=0.5] {$$};
    \node[main node] (2) [fill=blue,scale=0.5, left = 0.4cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\\
\vspace{10pt}
\begin{tikzpicture}
    \node[main node] (1) [fill=green,scale=0.5] {$$};
    \node[main node] (2) [fill=red,scale=0.5, left = 0.4cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\qquad 
\begin{tikzpicture}
    \node[main node] (1) [fill=green,scale=0.5] {$$};
    \node[main node] (2) [fill=blue,scale=0.5, left = 0.4cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\\
\vspace{10pt}
\begin{tikzpicture}
    \node[main node] (1) [fill=blue,scale=0.5] {$$};
    \node[main node] (2) [fill=red,scale=0.5, left = 0.4cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\qquad 
\begin{tikzpicture}
    \node[main node] (1) [fill=blue,scale=0.5] {$$};
    \node[main node] (2) [fill=green,scale=0.5, left = 0.4cm of 1]  {$$};

    \path[draw,thick]
    (1) edge node {} (2);
\end{tikzpicture}
\end{figure}
\end{frame}
\begin{frame}{Example}
\begin{itemize}
\item For bigger graphs, this gets much harder. We can instead define a random walk on all $k$-colourings by
\begin{enumerate}
  \item<2-> Select a vertex $v$ uniformly at random.
  \item<3-> Select a colour $c \in \{1, \dots, k\}$ uniformly at random.
  \item<4-> Attempt to recolour $v$ with colour $c$.
\end{enumerate}
\begin{center}
\begin{tikzpicture}
    \node[main node] (1) [scale=0.5] {$1$};
    \node[main node] (2) [scale=0.5, above left = 0.5 cm and 2.8cm of 1]  {$3$};
    \node[main node] (3) [scale=0.5, below left = 0.3cm and 1.6cm of 1, label={[visible on=<2->]v}]  {$2$};
    \node[main node] (4) [scale=0.5, below left = 0.8cm and 2.6cm of 1]  {$4$};
    \node[main node] (5) [scale=0.5, below left = 0.8cm and 0.8cm of 1]  {$1$};
    
    \path[draw,thick]
    (1) edge node {} (2)
    (3) edge node {} (4)
    (1) edge node {} (3)
    (2) edge node {} (4)
    (2) edge node {} (3)
    (3) edge node {} (5)
    (5) edge node {} (4);
\end{tikzpicture}
$k = 5$, \visible<4>{$c = 5$}
\end{center}
\end{itemize}
\end{frame}
\begin{frame}{Example}
\begin{itemize}
\item For bigger graphs, this gets much harder. We can instead define a random walk on all $k$-colourings by:
\begin{enumerate}
  \item Select a vertex $v$ uniformly at random.
  \item Select a colour $c \in \{1, \dots, k\}$ uniformly at random.
  \item Attempt to recolour $v$ with colour $c$.
\end{enumerate}
\begin{center}
\begin{tikzpicture}
    \node[main node] (1) [scale=0.5] {$1$};
    \node[main node] (2) [scale=0.5, above left = 0.5 cm and 2.8cm of 1]  {$3$};
    \node[main node] (3) [scale=0.5, below left = 0.3cm and 1.6cm of 1, label=v]  {$5$};
    \node[main node] (4) [scale=0.5, below left = 0.8cm and 2.6cm of 1]  {$4$};
    \node[main node] (5) [scale=0.5, below left = 0.8cm and 0.8cm of 1]  {$1$};
    
    \path[draw,thick]
    (1) edge node {} (2)
    (3) edge node {} (4)
    (1) edge node {} (3)
    (2) edge node {} (4)
    (2) edge node {} (3)
    (3) edge node {} (5)
    (5) edge node {} (4);
\end{tikzpicture}
$k = 5$, $c = 5$
\end{center}
\end{itemize}
\end{frame}
\begin{frame}{Highlight}
\begin{itemize}
\item The \emph{mixing time} describes how many steps we need to simulate the chain to arrive at a almost-uniform distribution.
\pause
\item Two methods to show rapid mixing (low mixing time): 
\begin{enumerate}
\item Coupling
\item Canonical Paths
\end{enumerate}
\end{itemize}
\end{frame}
\begin{frame}{Coupling: Theory}
\begin{itemize}
\item A coupling of a Markov Chain $(X_t)$ is a Markov Chain $(X^1_t, X^2_t)$ such that 
\pause
\begin{enumerate}
\item $X^1_t$ and $X^2_t$ both behave as the original chain
\item If $X^1_t = X^2_t$, then $X^1_{t + 1} = X^2_{t + 1}$ with probability 1.
\end{enumerate}
\pause
\item The coupling time is the least $T$ such that $X_T = Y_T$. 
\pause
\item \textbf{Coupling Lemma}: If the expectation of $T$ is low, then the mixing time is low.
\end{itemize}
\end{frame}
\begin{frame}{Coupling: Example}
\begin{itemize}
\item Take two colourings $\sigma, \tau$ of some graph $G$.
\pause
\item Attempt to recolour the same vertex $v$ in both $\sigma$ and $\tau$ with the same colour $c$.
\pause
\item This defines a coupling.
\pause
\item Using this, we can show that the graph colour chain mixes rapidly whenever $k \geq 2\Delta(G) + 1$. 
\end{itemize}
\end{frame}
\begin{frame}{Schedule}
Finished most of the reading for now, focus on writing.
\pause
\begin{center}\includegraphics[scale=0.6]{contentstable}\end{center}
\end{frame}
\begin{frame}
\begin{center}Thanks for listening!\end{center}
\end{frame}
\end{document}
