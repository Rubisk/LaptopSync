"""
Python program for approximately counting colourings.
"""
import statistics
import networkx as nx
import math
import matplotlib.pyplot as pyplot
import subprocess
import numpy as np


if nx.__version__[0] != "1":
    print("This script is written for an older version of networkx, please roll back to version 1.11")
    exit()

# Constant determining the number of trials used for generating boxplots when
# trying to find constant efficiency.
BOXPLOT_TRIALS = 100


def exact_count_cycle_colourings(n, k):
    """
    Exactly counts the number of colourings on a cyclic graph for comparing.
    :param n: (int) Length of cycle
    :param k: (int) Number of colours
    :return: (int) Number of colourings
    """
    return (k - 1) ** n + ((-1) ** n) * (k - 1)


def ap_co_custom_constants(graph, k, time, s, trials):
    """
    C++ wrapper for approximately counting colourings.
    Calls the corresponding C++ program to do the actual counting.
    :param graph: (nx.Graph) graph to count colourings on
    :param k: (int) number of colours
    :param time: (int) number of steps to simulate Glauber Dynamics for sampling
    :param s: (int) number of z_i's to approximate mu_i
    :param trials: (int) number of (simultaneous) approximations to compute
    :return: (list of float) list of approximations
    """

    # extract edges and vertices from graph
    n = graph.number_of_edges()
    edge_list = []
    for x, y in graph.edges():
        edge_list.extend([str(x), str(y)])

    # call C++ program
    arg_list = ["graph_colouring_approximator.exe", str(n), str(k), str(time), str(s), str(trials)]
    arg_list += edge_list
    pipe = subprocess.PIPE
    process = subprocess.Popen(arg_list, stdin=pipe, stdout=pipe, stderr=pipe)

    # retrieve output
    output, err = process.communicate()
    output = output.decode("utf-8")
    return list(map(lambda l: int(float(l)), output.split()))


def approximate_colouring_count(graph, k, epsilon, p_success):
    """
    Approximates the total number O of proper colourings for a graph G.
    Returns a number that lies in ((1 - e)O, (1 + e)O)
    :param graph: (nx.Graph) Graph to count colourings on
    :param k: (int) Number of colours to use for colouring
    :param epsilon: (float) error bound e
    :param p_success: (float) success probability p
    :return: (int) Approximation given by the procedure described in section 3.1
    """
    # For a good explanation of this implementation, look over section 3 in the report

    # Calculate number of approximations for the median
    trials = int(math.ceil((8 * math.log(1 / (1 - p_success))) ** 1 / 3))

    # Calculate all required constants
    m = len(graph.edges())
    n = len(graph.nodes())
    d = max(graph.degree(graph.nodes()).values())
    time = math.ceil((k / (k - 2 * d)) * n * math.log(4 * n * m / epsilon))  # big T in the report
    s = math.ceil(37 * m / (epsilon ** 2))

    print("Simulating trials of {0} samplings with simulation time {1}.".format(s, time))

    return statistics.median(ap_co_custom_constants(graph, k, time, s, trials))


def generate_plots_varying_epsilon(graph, k, epsilons, title="", exact=-1):
    """
    Generates a series of boxplots measuring precision of algorithm while varying time constant.
    To make things easier, we will fix the s constant to 500.
    :param graph: (nx.Graph) Graph to approximate on.
    :param k: (int) Number of colours to use
    :param epsilons: (list of float) epsilons to try, should be between 0 and 1
    :param title: (str) Title to display above the graph
    :param exact: (int) Exact answer to draw on the graph. If this is set to -1, this line isn't drawn.
    """

    m = len(graph.edges())
    n = len(graph.nodes())
    d = max(graph.degree(graph.nodes()).values())

    # List of lists of sample results for each k
    results = []

    fig, ax = pyplot.subplots()

    for i, epsilon in enumerate(epsilons):
        assert 0 < epsilon < 1
        print("Generating data for epsilon = {0}".format(epsilon))

        # Calculate T and s
        time = math.ceil((k / (k - 2 * d)) * n * math.log(4 * n * m / epsilon))
        s = math.ceil(37 * m / (epsilon ** 2))
        print("s = {0}, T = {1}".format(s, time))

        # Generate actual approximations
        result = ap_co_custom_constants(graph, k, time, s, trials=BOXPLOT_TRIALS)
        results.append(result)

        # Plot lines mean and (1 +- epsilon)*median
        median = statistics.median(result)
        pyplot.plot([i + 0.7, i + 1.3], [median, median], 'k-', lw=2)
        pyplot.plot([i + 0.8, i + 1.2], [(1 - epsilon) * median, (1 - epsilon) * median], 'k-', lw=2)
        pyplot.plot([i + 0.8, i + 1.2], [(1 + epsilon) * median, (1 + epsilon) * median], 'k-', lw=2)

        # Plot lines indicating middle 75% of data
        lower_percentile = np.percentile(result, 12)
        upper_percentile = np.percentile(result, 87)
        pyplot.plot([i + 0.8, i + 1.2], [lower_percentile, lower_percentile], 'k-', color='r')
        pyplot.plot([i + 0.8, i + 1.2], [upper_percentile, upper_percentile], 'k-', color='r')

    if exact != -1:
        pyplot.plot([0, len(epsilons) + 1], [exact, exact], 'k-', lw=2)

    # Plot all data points
    pyplot.plot(range(1, len(epsilons) + 1), results, "bo")

    # Label axes, title
    pyplot.xlabel("Epsilon")
    ax.set_xlim(0, len(epsilons) + 1)
    pyplot.ylabel("Approximation")
    pyplot.title(title)
    pyplot.xticks(range(1, len(input_epsilons) + 1), input_epsilons)

    pyplot.show()


if __name__ == "__main__":
    print("Select action by pressing number key:")
    print("[1] Approximately count colourings on cyclic Graph")
    print("[2] Measure precision by varying epsilon")
    option = input()

    # Approximately Count Colourings UI
    if option == "1":
        print("Length of cycle:")
        input_n = int(input())
        print("Number of colours:")
        input_k = int(input())
        assert input_k >= 3
        print("Error tolerance (epsilon):")
        input_epsilon = float(input())
        assert 0 < input_epsilon < 1
        print("Minimum success probability:")
        input_prob = float(input())
        assert 0 <= input_prob < 1
        test_graph = nx.cycle_graph(input_n)
        approx_answer = approximate_colouring_count(test_graph, input_k, input_epsilon, input_prob)
        print("Approximate answer: {0}".format(approx_answer))
        print("Exact answer: {0}".format(exact_count_cycle_colourings(input_n, input_k)))

    # Boxplots varying time UI
    elif option == "2":
        print("[1] Cyclic Graph")
        print("[2] Random Regular Graph")
        option = input()

        if option == "1":
            print("Length of cycle:")
            input_n = int(input())
            print("Number of colours:")
            input_k = int(input())
            assert input_k >= 3
            test_graph = nx.cycle_graph(input_n)
            print("List of epsilons to try:")
            input_epsilons = list(map(float, input().split()))
            exact_answer = exact_count_cycle_colourings(input_n, input_k)
            title_str = "Approx. Colour Counter (Cyclic Graph, n = {0}, k = {1})"
            title_input = title_str.format(input_n, input_k)
            generate_plots_varying_epsilon(test_graph, input_k, input_epsilons, title_input, exact_answer)

        elif option == "2":
            print("Number of vertices:")
            input_n = int(input())
            print("Degree:")
            input_d = int(input())
            print("Number of colours:")
            input_k = int(input())
            assert input_k >= input_d + 1
            assert (input_n * input_d) % 2 == 0
            assert input_d <= input_n - 1
            test_graph = nx.random_regular_graph(input_d, input_n)
            print("List of epsilons to try:")
            title_str = "Approx. Colour Counter ({2}-Regular Graph, n = {0}, k = {1})"
            title_input = title_str.format(input_n, input_k, input_d)
            input_epsilons = list(map(float, input().split()))
            generate_plots_varying_epsilon(test_graph, input_k, input_epsilons, title_input)
