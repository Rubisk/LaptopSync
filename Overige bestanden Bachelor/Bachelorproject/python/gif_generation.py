import random
import itertools as it
import networkx as nx
import math
import time
import numpy as np
import matplotlib.pyplot as pyplot
import matplotlib.colors as colors
import matplotlib.animation as animation

if nx.__version__[0] != "1":
    print("This script is written for an older version of networkx, please roll back to version 1.11 ")
    exit()


class DrawableGridColouredGraph(object):
    def __init__(self, width, height, colours):
        """
        Class used to generate Glauber Dynamics Anmiations.
        :param width: (int) width of grid
        :param height: (int) height of grid
        :param colours: (list of str) List of colours to use to draw the grid
        """

        self.height = height
        self.width = width

        # generate colour_grid, used to pass colours on to matplotlib
        self.colour_grid = np.zeros(height * width).reshape(width, height)
        self.colours = colours
        self.k = len(colours)

    def generate_initial_colouring(self):
        # Reset all colours
        for x, y in it.product(range(self.width), range(self.height)):
            self.set_colour((x, y), (x + y) % 2)

    def set_colour(self, v, k):
        """
        Tries to recolour node v with colour k
        :param v: Vertex to recolour
        :param k: Colour to use
        :return: (boolean) returns true if v has been recoloured and false otherwise
        """
        # Check if allowed to recolour
        x, y = v
        for dx in (-1, 1):
            if 0 <= x + dx < self.width:
                if self.colour_grid[x + dx][y] == k:
                    return False
        for dy in (-1, 1):
            if 0 <= y + dy < self.height:
                if self.colour_grid[x][y + dy] == k:
                    return False

        # Recolour
        self.colour_grid[x][y] = k
        return True

    def simulate_glauber_dynamics(self, t, k=1):
        """
        Simulate the Glauber dynamics for n steps with a graph G.
        :param t: (float) Minimum number of steps to simulate (in case of non-int)
        :param k: (int) Number of colours to use to recolour vertices. Works as if inputting range(k).
        :return: None
        """

        # First retrieve list of nodes and setup a list of colours to use
        new_colours = list(range(k))

        # Make sure we run for at least t steps
        t = int(math.ceil(t))
        for _ in range(t):
            # Pick a random vertex and colour
            x = random.randint(0, self.width - 1)
            y = random.randint(0, self.height - 1)
            colour = new_colours[random.randint(0, k - 1)]

            # Attempt to recolour
            self.set_colour((x, y), colour)

    def generate_animation(self, frames, steps_per_frame, interval, filename="animation.mp4"):
        """
        Generates the actual Glauber Dynamics animation and saves it as a file
        :param steps_per_frame: (int) Number of Glauber Dynamic steps to simulate per frame
        :param interval: (float) Time interval of a frame
        :param frames: (int) Number of frames to simulate for.
        :param filename: (str) Filename to save animation to
        """
        # generate random colouring to start on
        self.generate_initial_colouring()
        # We need one of each colour present or matplotlib will optimize our colours away
        for i in range(len(self.colours)):
            self.set_colour((0, i), i)

        # setup matplotlib context
        fig, ax = pyplot.subplots()
        colour_map = colors.ListedColormap(self.colours)
        ax = ax.imshow(self.colour_grid, cmap=colour_map, interpolation='none',
                       extent=[0, self.height, 0, self.width],
                       animated=True)

        pyplot.xticks([])
        pyplot.yticks([])

        old_time = time.time()

        def _update(f):
            nonlocal old_time
            if time.time() - old_time > 2:
                print("{0} / {1} frames generated".format(f, frames))
                old_time = time.time()
            # simulate glauber for a bit and update matplotlib plot
            ax.set_data(self.colour_grid)
            self.simulate_glauber_dynamics(steps_per_frame, self.k)
            return ax,

        # generate animation and save to file
        ani = animation.FuncAnimation(fig, _update, interval=interval, frames=frames)
        ani.save(filename, dpi=400)


if __name__ == "__main__":
    print("Frames:")
    input_frames = int(input())
    print("Steps per frame:")
    input_steps = int(input())
    print("Timestep:")
    input_interval = float(input())
    print("Filename:")
    input_filename = input()
    my_graph = DrawableGridColouredGraph(100, 50, ["red", "green", "blue"])
    my_graph.generate_animation(input_frames, input_steps, input_interval, input_filename)
