function [U, S, B, d] = MySVD(B, p)

d = zeros(1, p + 1);
d(1) = sqrt(diag(B)'*diag(B))/norm(B, 'fro');
[rows, columns] = size(B);
U = eye(size(B, 1));
S = eye(size(B, 2));

for i=1:p
    for col=2:columns
        M = Rot(columns, col-1, col, -B(col-1, col-1), B(col-1, col));
        S = S*M;
        B = B*M;
    end
    for col=2:columns
        M = Rot(rows, col-1, col, B(col-1, col-1), B(col, col-1));
        U = M*U;
        B = M*B;
    end
    d(i + 1) = sqrt(diag(B)'*diag(B))/norm(B, 'fro');
end
Rot(4, 1, 2, -1, 2)