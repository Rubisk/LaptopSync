function perm = Permanent_2(A)

perm = 0;
for i=1:size(A, 1)
    v = [1:i - 1 i+1:size(A, 1)];
    B = A(2:size(A, 1), v);
    perm = perm + Permanent_2(B) * A(1, i);
end

if size(A) == 0
    perm = 1;
end