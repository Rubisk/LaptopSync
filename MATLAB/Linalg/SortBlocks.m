function [J, X] = SortBlocks(J)

v = [0 diag(J, 1)' 0];
w = find(v == 0);
[~, b] = sort(diff(w));

u = [];
for k = 1:length(b)
    % yay for crappy memory management :D
    u = [u w(b(k)):w(b(k) + 1) - 1];
end

X = Perm(u, length(u));
J = X'*J*X;