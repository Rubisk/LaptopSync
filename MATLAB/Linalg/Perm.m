function Y = Perm(v, n)
Y = eye(n);
Y = Y(:, v);