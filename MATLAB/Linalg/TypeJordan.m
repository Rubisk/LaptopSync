function tau = TypeJordan(J)

blocks = []; diffs = diff(find(J));
n = 0; s = size(diff(find(J)))
for i=1:s
    if diffs(i)==2
        blocks = [blocks, n]; n = 0;
    end
    n = n + 1;
end
tau = sort(blocks)