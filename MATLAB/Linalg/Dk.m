function Y = Dk(k, h, n)
Y = eye(n);
Y(k, k) = h;