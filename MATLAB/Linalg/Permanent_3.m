function perm = Permanent_3(A)

A

if size(A) == 0
    perm = 1;
    return
end

perm = 0;
rowZeroes = zeros(size(A, 1));
columnZeroes = zeros(size(A, 1));
for i=1:size(A)
    rowZeroes(i) = size(find(A(i, :) == 0), 2);
    columnZeroes(i) = size(find(A(:, i) == 0), 2);
end
[bestRowCount, bestRow] = max(rowZeroes);
[bestColumnCount, bestColumn] = max(rowZeroes);

if bestRowCount > bestColumnCount
    
    A(bestRow, i)
    w = [1:bestRow-1 bestRow+1:size(A, 1)];
    for i=1:size(A, 1)
        v = [1:i - 1 i+1:size(A, 1)];
        B = A(w, v);
        perm = perm + Permanent_3(B) * A(bestRow, i);
    end
else
    i, bestColumn
    A(i, bestColumn)
    w = [1:bestColumn-1 bestColumn+1:size(A, 1)];
    for i=1:size(A, 1)
        v = [1:i - 1 i+1:size(A, 1)];
        B = A(v, w);
        perm = perm + Permanent_3(B) * A(i, bestColumn);
    end
end

perm