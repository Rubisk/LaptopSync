function [U, R] = Uptriang(A)

rotations = eye(size(A, 1));
for i=1:size(A, 2)
    for j=size(A, 1):-1:i+1
        rotations = Rot(size(A, 1), i, j, A(i, i), A(j, i)) * rotations;
        A = Rot(size(A, 1), i, j, A(i, i), A(j, i))*A;
    end
end

U = rotations';
R = A;