function p = Perminant(A)

n = size(A, 1);
permutations = perms(1:n);
p = 0;
for i=1:size(permutations, 1)
    product = Switch(permutations(i, :));
    for j = 1:n
        product = product * A(j, permutations(i, j));
    end
    p = p + product;
end
