function U = Rot(n, k, l, a, b)

s = 1 / sqrt(a^2 + b^2);
U = eye(n);
U(k, k) = s * a;
U(l, l) = s * a;
U(k, l) = s * b;
U(l, k) = - s * b;