function L = IndexLijst(R)

n = size(R, 1);
L = [];

for i=n:-1:1
    for j=i+1:n
        if R(i, i) ~= R(j, j)
            L = [L; i, j];
        end
    end
end