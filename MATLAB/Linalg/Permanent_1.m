function perm = Perminant(A)

permutations = perms(1:size(A, 1));
perm = 0;
for i=1:size(permutations, 1)
    product = 1;
    for j = 1:size(A, 1)
        product = product * A(j, permutations(i, j));
    end
    perm = perm + product;
end

