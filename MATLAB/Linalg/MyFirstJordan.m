function [X, J] = MyFirstJordan(R)


dim = size(R, 1);
[X, J] = CreateZeroes(R);
[Y, J, eigenvalues] = GroupEigenValues(J);
X = X * Y;

blockSizes = diff([0 find(diff(eigenvalues')), dim]);

start=0;
for block=blockSizes
    blockX = eye(block);
    M = J(start+1:start+block, start+1:start+block);
    M = M - diag(diag(M));
    for j=2:block
        [X1, ~] = ColumnEasyZeroes(M(1:j, 1:j));
        Y = [X1, zeros(j, block - j);
             zeros(block - j, j), eye(block - j)];
        blockX = blockX * Y;
        M = Y\M*Y;
        [X2, ~] = ColumnHardZeroes(M(1:j, 1:j));
        Y = [X2, zeros(j, block - j);
             zeros(block - j, j), eye(block - j)];
        blockX = blockX * Y;
        M = Y\M*Y;
        pause;
    end
    last = dim - block - start;
    Z = [eye(start), zeros(start, dim - start);
         zeros(block, start), blockX, zeros(block, last);
         zeros(last, start + block), eye(last)];
    J = Z\J*Z;
    X = X * Z;
    start = start + block;
end

% Create all easy zeroes in a column.
function [X, M] = ColumnEasyZeroes(M)

block = size(M, 1);
X = eye(block);
[todo, ~, ~] = find(M(:, 1:block - 1));
if ~isempty(todo)
    for j=todo'
        nonzeroColumn = find(M(j, :));
        X = X * Ejk(nonzeroColumn(1), block, -M(j, block), block);
    end
end

M(todo, block) = 0;


% Create the last, tough zeroes in a column.
function [X, M] = ColumnHardZeroes(M)

block = size(M,1);

nonzeroIndices = find(abs(M(:,block)) > 1e-14);

if isempty(nonzeroIndices)
    X = eye(block);
else
    % Make last entry in vector 1
    lastIndex = nonzeroIndices(end);
    X = Dk(block, 1/M(lastIndex, block), block);
    lastVector = M(1:block-1, block) / M(lastIndex, block);
    for j = nonzeroIndices(1:end-1)'
        valueToRemove = lastVector(j);
        cRow = lastIndex;
        cColumn = j;
        while sum(M(:, cColumn))~=0
            % keep adding the lowest one until we hit a
            % 0 preventing us from creating a new one.
            X = X * Ejk(cColumn, cRow, valueToRemove, block);
            cRow = cRow-1;
            cColumn = cColumn-1;
        end
        X = X * Ejk(cColumn, cRow, valueToRemove, block);
    end
    M(nonzeroIndices(1:end-1),block) = 0; 
    M(nonzeroIndices(end),block)=1;
end


% Groups all eigenvalues and sort them lowest to highest.
function [X, M, values] = GroupEigenValues(R)

dim = size(R, 1);
[values, order] = sort(diag(R));
X = Perm(order, dim);
M = X\R*X;

% Creates zeroes at all spots with different eigenvalues
% right below and to the side.
function [X, M] = CreateZeroes(R)

dim = size(R, 1);
X = eye(dim);
for i=dim-1:-1:1
    for j=i+1:dim
        if R(i, j)~= 0 && R(i, i) ~= R(j, j) 
            scalar = R(i, j) / (R(i, i) - R(j, j));
            E = Ejk(i, j, -scalar, dim);
            R = E\R*E;
            X = X*E;
        end
    end
end
M = R;

% Elementary row-operations.

% Multiply row k by scalar h
function Y = Dk(k, h, block)
Y = eye(block);
Y(k, k) = h;

% Add row j to row k
function X = Ejk(j, k, h, block)
X = eye(block);
X(j, k) = h;

% Permute rows in order v.
% V should be a vector specifying the new order of the rows
% 1 through n.
function Y = Perm(v, block)
Y = eye(block);
Y = Y(:, v);