function [X, M] = ColumnHardZeroes(M)

dim = size(M,1);

nonzeroIndices = find(abs(M(:,dim)) > 1e-14);

if isempty(nonzeroIndices)
    X = eye(dim);
else
    % Make last entry in vector 1
    lastIndex = nonzeroIndices(end);
    X = Dk(dim, 1/M(lastIndex, dim), dim);
    lastVector = M(1:dim-1, dim) / M(lastIndex, dim);
    for j = nonzeroIndices(1:end-1)'
        valueToRemove = lastVector(j);
        cRow = lastIndex;
        cColumn = j;
        while sum(M(:, cColumn))~=0
            % keep adding the lowest one until we hit a
            % 0 preventing us from creating a new one.
            X = X * Ejk(cColumn, cRow, valueToRemove, dim);
            cRow = cRow-1;
            cColumn = cColumn-1;
        end
        X = X * Ejk(cColumn, cRow, valueToRemove, dim);
    end
    M(nonzeroIndices(1:end-1),dim) = 0; 
    M(nonzeroIndices(end),dim)=1;
end

newOrder = [1:lastIndex, dim, lastIndex+1:dim-1];
X = X*Perm(newOrder, dim);
M = M(newOrder, newOrder);
[M, Z] = SortBlocks(M);
X = X * Z;
