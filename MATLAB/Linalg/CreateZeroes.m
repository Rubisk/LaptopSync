function [R,X] = CreateZeroes(R)

n = size(R,1); X = eye(n);
for i=n-1:-1:1
    for j=i+1:n
        if R(i, j)~= 0 && R(i, i) ~= R(j, j) 
            E = Ejk(i, j, R(i, j) / (R(i, i) - R(j, j)), n);
            R =  E*R/E;
            X = X/E;
        end
    end
end