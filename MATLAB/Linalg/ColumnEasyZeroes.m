function [M, X] = ColumnEasyZeroes(M)

m = size(M, 1);
X = eye(m);
[w, ~, ~] = find(M(:, 1:m - 1));

for k = w
    c = find(M(k, :));
    X = X * Ejk(c(1), m, -M(k, m), m);
end

M(w, m) = 0;