function [R, X] = GroupEigenvalues(R, n)

sorted = sort(diag(R));
X = eye(n);
for j=1:n
    if R(j, j) == sorted(j)
        continue
    end
    for i=j+1:n
        if R(i, i) == sorted(j)
            v = 1:n;
            v(i) = j; v(j) = i;
            E = Perm(v, n);
            R = E*R/E;
            X = X*E;
        end
    end
end