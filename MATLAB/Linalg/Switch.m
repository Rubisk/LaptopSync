function h = Switch(sigma)

% recursion approach
d = diff(sigma);
if size(d, 2) == 1
    h = d;
else
    h = Switch(d);
end