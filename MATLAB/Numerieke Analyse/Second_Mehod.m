function err = Second_Mehod(N) % N is the number of the time steps
f=@(x,y)(1+x)*(1+y.^2); % defines the function f
y=@(x) tan(x+x.^2/2); % defines exact solution
h=1/N; % time step
x=0:h:1;
xfine=0:0.01:1;
FE=zeros(1,N+1); % Forward Euler approximation solution
err=zeros(1,N+1); % Error values of Forward Euler method
FE(1)=0;
for i=1:N
    k1 = h*f(x(i), FE(i));
    k2 = h*f(x(i), FE(i) + k1);
    FE(i+1)=FE(i)+0.5*(k1 + k2);
end
for i=1:N+1
    err(i)=y(x(i))-FE(i);
end
plot(xfine,y(xfine));
hold on;
plot(x,FE);
