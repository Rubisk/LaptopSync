==E4==
For the regular interpolation points I obtained the value:
m = 3.6612e+04

For the chebyshev interpolation points I obtained the value:
m = 7.6294e+03

==E5==
For the regular version, I obtained the value:
m = 262144 (= 2^18)
error = 3.6307e-04

For the recursive version, I obtained the value:
m = 29007
error = 1.9849e-04

It seems like the recursive version is a lot better than the regular version.


NUMERICAL ANALYSIS - 17/3/2017
WOUTER RIENKS
11007745