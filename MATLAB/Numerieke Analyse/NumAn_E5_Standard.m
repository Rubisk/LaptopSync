function [m, error] = NumAn_E5_Standard(a, b, epsilon)

    % Constants
    k = 0;
    expected_value = -(1 / 2) * (1 / b^2 - 1 / a^2);

    while(true)
        % Calculate sample points
        sample_x = linspace(a, b, 2^k + 1);
        sample_y = 1 ./ sample_x .^3;

        % Half edge points
        sample_y(1) = sample_y(1) ./ 2;
        sample_y(2^k + 1) = sample_y(2^k + 1) ./ 2;

        % Sum and compare by expected value
        T = sum(sample_y) * (b - a) / 2^k;
        if abs(T - expected_value) < epsilon
            disp(k);
            break;
        end

        k = k + 1;
    end
    error = T - expected_value;
    m = 2^k;
end