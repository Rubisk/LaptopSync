function err = Forward_Euler(N) % N is the number of the time steps
f=@(x,y)(1+x)*(1+y.^2); % defines the function f
y=@(x) tan(x+x.^2/2); % defines exact solution
h=1/N; % time step
x=0:h:1;
xfine=0:0.01:1;
FE=zeros(1,N+1); % Forward Euler approximation solution
err=zeros(1,N+1); % Error values of Forward Euler method
FE(1)=0;
for i=1:N
    FE(i+1)=FE(i)+h*f(x(i),FE(i));
end
for i=1:N+1
    err(i)=y(x(i))-FE(i);
end
plot(xfine,y(xfine));
hold on;
plot(x,FE);
