function [m, approx] = NumAn_E5_Recursion(a, b, epsilon)
    % Calculate T(2)
    sample_x_2 = linspace(a, b, 3);
    sample_y_2 = 1 ./ sample_x_2.^3;
    sample_y_2(1) = sample_y_2(1) ./ 2;
    sample_y_2(3) = sample_y_2(3) ./ 2;
    approx_2 = sum(sample_y_2) * (b - a) / 2;
    
    % Calculate T(1)
    sample_x_1 = linspace(a, b, 2);
    sample_y_1 = 1 ./ sample_x_1.^3;
    sample_y_1(1) = sample_y_1(1) ./ 2;
    sample_y_1(2) = sample_y_1(2) ./ 2;
    approx_1 = sum(sample_y_1) * (b - a);
    
    % Approximate error
    error_approx = (approx_2 - approx_1) / 3;
    
    % Compare difference
    if abs(error_approx) < epsilon;
        % Deepest recursion reached, we sampled at 3 points here
        function_calls = 3;
        approx = approx_2;
    else
        % Split into two different sections
        [l_calls, l_approx] = NumAn_E5_Recursion(a, (a + b) / 2, epsilon / 2);
        [r_calls, r_approx] = NumAn_E5_Recursion((a + b) / 2, b, epsilon / 2);
        
        % Calculate integral approximation
        approx = l_approx + r_approx;

        % Find the maximum depth of our recursion
        m = l_calls + r_calls + 3;
    end
end
