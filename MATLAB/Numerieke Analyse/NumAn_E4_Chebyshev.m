function [m] = NumAn_E4_Chebyshev(n)

    % Find sample spots
    sample_x = zeros(n + 1);
    for k=0:n
        sample_x(k + 1) = 5 * cos((k + 0.5) * pi/(n + 1));
    end
    sample_y = 1 ./ (1 + sample_x.^2);

    % Calculate polynomial
    fitpol = polyfit(sample_x, sample_y, n);

    % Plot the absolute difference
    line_x = linspace(-5, 5, 150);
    fitpol_plot = polyval(fitpol, line_x);
    f_plot = 1 ./ (1 + line_x.^2);
    diff_plot = abs(fitpol_plot - f_plot);

    % Draw the plot
    plot(line_x, diff_plot);

    % Calculate (x - x_k) product in finalpol
    finalpol = 1;
    for i=1:n+1;
        finalpol = conv(finalpol, [1, -sample_x(i)]);
    end

    % Calculate the max value of the (x - x_k) product
    m = max(polyval(finalpol, line_x));
end