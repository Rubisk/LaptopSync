function [m] = NumAn_E4_Regular(n)

    % Find sample spots
    sample_x = linspace(-5, 5, n + 1);
    sample_y = 1 ./ (1 + sample_x.^2);

    % Calculate polynomial
    fitpol = polyfit(sample_x, sample_y, n);

    % Plot the absolute difference
    line_x = linspace(-5, 5, 150);
    fitpol_plot = polyval(fitpol, line_x);
    f_plot = 1 ./ (1 + line_x.^2);
    diff_plot = abs(fitpol_plot - f_plot);

    % Draw the actual plot
    plot(line_x, diff_plot);

    % Calculate (x - x_k) product in finalpol
    finalpol = 1;
    for i=1:n+1;
        finalpol = conv(finalpol, [1, -sample_x(i)]);
    end

    % Calculate max value of (x - x_k)
    m = max(polyval(finalpol, line_x));
end