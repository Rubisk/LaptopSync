\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\diam}{diam}


% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\HH}{\mathbb{H}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catO}{\mathcal{O}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Methods in Combinatorics Homework 3}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1.]
Let $\tilde{H_k}$ be defined exactly as $H_k$, but including the all ones vector. We will write $n = V(\tilde H_k) = 2^{k - 1}$. We say that a pair of vertices $(x, y)$, $x, y \in  \{0, 1\}^k$ is a \emph{good pair} if there exist $\alpha, \beta, \gamma \in \{1, \dots, k\}$ such that
\begin{align*}
x_\alpha &= 1, y_\alpha = 0 \\
x_\beta &= 0, y_\beta = 1 \\
x_\gamma &= 0, y_\gamma = 0.
\end{align*}
The idea is that for good pairs $(x, y)$, it is easy to count $N_{\tilde{H_k}}(x) \cap N_{\tilde{H_k}}(y)$. We claim that for any good pair $(x, y)$ one has $\# N_{\tilde{H_k}}(x) \cap N_{\tilde{H_k}}(y) = 2^{k - 3}$. To this end, fix any good pair $(x, y)$. We may relabel so that $\alpha = 1, \beta = 2, \gamma = 3$. Pick any choice of binary vector $(z_4, \dots, z_k) \in \{0, 1\}^{k - 3}$. Define the quantities
\begin{align*}
A = \sum_{i = 4}^k x_iz_i, \qquad
B = \sum_{i = 4}^k y_iz_i, \qquad
C = \sum_{i = 4}^k z_i.
\end{align*}
We claim there are unique $(a, b, c) \in \{0, 1\}$ such that $z = (a, b, c, z_4, \dots, z_k) \in N_{H_k}(x) \cap N_{H_k}(y)$. An easy computation shows that $z$ lies in $ N_{\tilde{H_k}}(x) \cap N_{\tilde{H_k}}(y)$ if and only if $A + a \equiv 1$, $B + b \equiv 1$ and $C + a + b + c \equiv 1 \pmod{2}$ (the restriction on $C + a + b + c$ is to make sure that $z$ actually lies in $V(\tilde{H_k})$). This clearly has one solution with $a, b, c \in \{0, 1\}$. Since there are $2^{k - 3}$ choices for $(z_4, \dots, z_k)$ and each yields one element of $N_{\tilde{H_k}}(x) \cap N_{\tilde{H_k}}(y)$, we conclude that indeed $\# N_{\tilde{H_k}}(x) \cap N_{\tilde{H_k}}(y) = 2^{k - 3}$. 

Next, we show that `almost all' pairs are good pairs. The idea is that since we can pick our $\alpha, \beta, \gamma$ freely, for most vectors it won't be hard to find them. Pick random $x, y \in \tilde{H_k}$ uniformly. An easy way of looking at this is picking random $x', y' \in \{0, 1\}^{k - 1}$ and choosing $x_k$ so that $(x'_0, \dots, x'_{k - 1}, x_k)$ has an odd number of ones (similarly for $y_k$). Set $\ell = \lfloor \frac{k - 1}{3}\rfloor$. Note that the events
\begin{align*}
\{x_{i + 1} = 1 \land x_{i + 2} = 0 \land x_{i + 3} = 0 \land y_{i + 1} = 0 \land y_{i + 2} = 1 \land y_{i + 3} = 0\}
\end{align*}
for $i \in \{0, \dots, \ell\}$ are all independent and occur with probability $\frac{1}{2^6}$ (since we picked $x'$ and $y'$ in  $\{0, 1\}^{k - 1}$ uniformly). Furthermore, if any of these events occurs then clearly $(x, y)$ is a good pair (with $\alpha = i + 1, \beta = i + 2, \gamma = i + 3$). Hence
\[
\PP\((x, y) \text{ is a good pair}\) \geq 1 - \(1 - \frac{1}{64}\)^{\ell}.
\]
We conclude that there are at least $n^2\(1 - \(1 - \frac{1}{64}\)^{\ell} \)$ good pairs.

We are now ready to compute $N_{\tilde{H_k}}(C_4)$. Our strategy is as follows: Pick any (ordered) pair of vertices $x \neq y \in \tilde{H_k}$. Then for any (ordered) pair of vertices $z \neq w \in  N_{\tilde{H_k}}(x) \cap N_{\tilde{H_k}}(y)$, we obtain a unique labeled copy $(x, z, y, w)$ of $C_4$, and any labeled copy of $C_4$ is obtained in this way exactly once.
For brevity, denote $f(x, y) = \#  N_{\tilde{H_k}}(x) \cap N_{\tilde{H_k}}(y)$. Then clearly
\begin{align*}
N_{\tilde{H_k}}(C_4) &= \sum_{x \neq y \in V(\tilde{H}_k)} f(x, y)(f(x, y) - 1) \\
&= \sum_{\substack{x \neq y \in V(\tilde{H}_k) \\ (x, y) \text{ a good pair}}} \frac{n}{4}\(\frac{n}{4} - 1\) + \sum_{\substack{x \neq y \in V(\tilde{H}_k) \\ (x, y) \text{ not a good pair}}} O(n^2) \\
&= n^2\(1 - \(1 - \frac{1}{64}\)^{\ell} \)\(\frac{n^2}{16} + O(n)\) + n^2\(1 - \frac{1}{64}\)^{\ell} O(n^2).
\end{align*}
Note that in the last sum we may have counted some good pairs as bad pairs instead, but that is fine since it only increases our error term. To estimate our result, we note that $n = \frac{\log(n)}{\log(2)} + o(1)$, hence
\[
\(1 - \frac{1}{64}\)^{\ell}  = \(1 - \frac{1}{64}\)^{\frac{\log(n) + o(1)}{3\log(2)}}  = o(1)
\]
as $k \to \infty$. Therefore
\begin{align*}
N_{\tilde{H_k}}(C_4)  &= n^2(1 - o(1))\(\frac{n^2}{16} + O(n)\) + n^2 o(1)O(n^2) \\
&= \(\frac{n}{2}\)^4 + o(n^3) + o(n^4) = \(\frac{n}{2}\)^4 + o(n^4).
\end{align*}
Since $H_k$ is simply $\tilde{H_k}$ but with one vertex removed,
\[
N_{H_k}(C_4)   = N_{\tilde{H_k}}(C_4)  + O(n^3) =  \(\frac{n}{2}\)^4 + o(n^4).
\]
Furthemore $H_k$ clearly has $2^{k - 1} - 1 = n - 1$ vertices. Since any $x \in H_k$ has at least one entry on which it is zero and one entry on which it is one, we may also conclude that $N_{H_k}(x) = 2^{k - 2} + O(1)$ for any $x \in H_k$. To see this, assume without loss of generality that $x_1 = 1$ and $x_2 = 0$. Then for any $(z_3, \dots, z_k) \in \{0, 1\}^k$, by similar reasoning as above there exist unique $a, b \in \{0, 1\}^k$ such that $(a, b, z_3, \dots, z_k) \in N_{H_k}(x)$ (the $O(1)$ term appears to deal with the exception of the all ones vector being counted as a neighbour).

We conclude that $e(H_k) = \frac{1}{2} (n - 1)(n / 2 + O(1)) = \frac{1}{2} \binom{n}{2} + o(n^2)$. Setting $m = n - 1$ we finally see that
\[
V(H_k) = m,\qquad e(H_k) = \frac{1}{2}\binom{m}{2} + o(m^2), \qquad N_{H_k}(C_4) = \frac{1}{2^4} m^4 + o(m^4),
\]
showing $H_k$ is $1/2$-quasirandom.
\end{opghand}
\begin{opghand}[2.]
Let $d_n$ denote the degree of $G_n$. Recall from the lectures that $d_n = pn + o(n)$. Note that any triangle can be obtained by picking a vertex $x$ and two elements $y, z \in N_{G_n}(x)$ such that $yz \in E(G_n)$. For fixed $x \in G_n$ this number is obviously equal to $e_{G_n}(N_{G_n}(x), N_{G_n}(x))$. Since $\#N_{G_n}(x)  = d_n$ for any $x \in G_n$, we conclude that
\begin{align*}
N_{G_n}(C_3) &= \sum_{x \in G_n} e_{G_n}(N_{G_n}(x), N_{G_n}(x)) \\
&= \sum_{x \in G_n} p d_n^2 + o(n^2) \\
&= \sum_{x \in G_n} p^3 n^2 + o(n^2) \\
&= p^3n^3 + o(n^3),
\end{align*}
as required.
\end{opghand}
\begin{opghand}[3.] \
\begin{enumerate}[(a)]
\item 
To show $\theta(G) \leq \alpha(\overline{G})$ it suffices to construct $\rho$ such that $\theta(G, \rho) \leq \alpha(\overline{G})$. In fact, it suffices to show there exists a vector $u \in \RR^n$ of length $1$ and an orthogonal representation $\rho: V \to \RR^n$ such that
\[
\max_{v \in V(G)} \frac{1}{\langle \rho(u), v\rangle^2} \leq \alpha(\overline G),
\]
since if $L$ is any orthogonal matrix mapping $u$ to $e_1$, then the representation $L \circ \rho$ satisfies $\theta(G, \rho) \leq \alpha(\overline{G})$. 

Write $k = \alpha(\overline G)$. By definition we may pick a ccolouring $c: V(G) \to [k]$ such that $c(v) \neq c(w)$ whenever $vw \not \in E(G)$ (as that implies $vw \in E(\overline{G})$). Let $\rho: V(G) \to \RR^k$ be given by $\rho(v) = e_{c(v)}$. Then clearly $||\rho(v)|| = ||e_{c(v)}||  = 1$. And whenever $vw \not \in E$ one has $c(v) \neq c(w)$ hence $\rho(v)$ and $\rho(w)$ are orthogonal (as they are different basis vectors). Therefore $\rho$ is an orthogonal representation. 

If we pick $u = \frac{1}{\sqrt{k}} (1, \dots, 1)$ then clearly $||u|| = 1$ and $\langle u, \rho(v) \rangle = 1 / \sqrt k$ for all $v \in V(G)$. Thus 
\[
\max_{v \in V(G)} \frac{1}{\langle \rho(u), v\rangle^2} = k = \alpha(\overline{G}),
\]
as desired.
\item If we enumerate the vertices $v_1, \dots, v_{2n}$ such that $v_i v_{i + 1} \in E$ then clearly $\{v_i \mid i \text{ odd}\}$ is an independent set, showing $\alpha(C_{2n}) \geq n$. Hence $\Theta(C_{2n}) \geq \alpha(C_{2n}) \geq n$. 

On the other hand, note that we can $n$-colour $\overline{C_{2n}}$ by setting $c(v_i) = \lfloor (i + 1) / 2 \rfloor$. Hence $\chi(\overline{C_{2n}}) \leq n$. Therefore $\Theta(C_{2n}) \leq \theta(C_{2n}) \leq \chi(\overline{C_{2n}}) \leq n$. 
\item Write $G = \overline{C_{2n}}$. Note that $\{v_1, v_2\}$ is an independent set of $G$ hence $\Theta(G) \geq \alpha(G) \geq 2$. On the other hand, $\overline{G} = C_{2n}$ is $2$-colourable (simply alternate colours), hence we conclude $\Theta(G) \leq \theta(G) \leq \chi(C_{2n}) \leq 2$. It follows that $\theta(G)  = 2$.
\end{enumerate}
\end{opghand}
\begin{opghand}[4.] \
\begin{enumerate}[(a)]
\item Let $k = \alpha(G)$, and pick any independent set $A = \{v_1, \dots, v_k\}$. Consider the subspace
\[
U = \{\lambda_1 e_{v_1} + \dots + \lambda_k e_{v_k} \mid \lambda_i \in \RR \} \subseteq \RR^V. 
\]
We claim that $M|_U$ is injective. Indeed, for $u = \lambda_1 e_{v_1} + \dots + \lambda_k e_{v_k}  \in U$ we compute
\begin{align*}
(Mu)_{v_j} &= \sum_{v \in V} M_{v_j v} u_v \\
&= \sum_{i = 1}^k M_{v_j v_i} u_{v_i} &\(u_v = 0 \text{ for } v  \not \in A\)  \\
&=  M_{v_j v_j} u_{v_j},
\end{align*}
since $M_{v_i v_j} = 0$ for $i \neq j$ as $v_iv_j \not \in E$ since $A$ is an independent set. Since $M_{v_j v_j} \neq 0$, $(Mu)_{v_j} = 0$ implies that $v_j = 0$. Thus $Mu = 0$ implies $u  =0$, and $M|_U$ is injective. It follows that $\dim \im M \geq \dim \im\(M|_U\) = \dim U = k$. Thus $f(G) \geq \alpha(G)$. By (b) we will obtain $f(G)^k \geq f(G^k) \geq \alpha(G^k) $, hence $\alpha(G^k)^{1 / k} \leq f(G)$. It follows that $f(G) \geq \Theta(G)$. 
\item Write $k = f(G)$ and $\ell = f(H)$. By definition there exist matrices $M \in \RR^{V(G) \times V(G)}$ and $N \in \RR^{V(H) \times V(H)}$ such that $\dim \im (M) = k$, $\dim \im (N) = \ell$, $M_{uv} = 0$ whenever $uv \not \in E(G)$, $N_{uv} = 0$ whenever $uv \not \in E(H)$, and $M_{uu} \neq 0$, $N_{vv} \neq 0$ for all $u \in V(G), v \in V(H)$. Denote the standard bases $v_1, \dots, v_{V(G)}$ of $\RR^{V(G)}$ and $u_1, \dots, u_{V(H)}$ of $\RR^{V(H)}$. Note that the vectors $v_i \otimes u_j = e_{i,j}$ form a basis for $\RR^{V(G) \times V(H) } = \RR^{V(G \cdot H)}$. We can thus define a linear map (matrix)
\[
L: \RR^{V(G) \times V(H)} \to \RR^{V(G) \times V(H)}: v_i \otimes u_j \mapsto Mv_i \otimes Nu_j.
\]
Since $L$ factors as a bilinear map $\RR^{V(G) \times V(H)}  \cong \RR^{V(G)} \otimes \RR^{V(H)} \to \im M \otimes \im N \to \RR^{V(G) \times V(H)}$, we know $\dim \im(L) \leq \dim \(\im M \otimes \im N\) =  k \ell$. On the other hand, clearly $L_{(v_1, u_1), (v_1, u_1)} = M_{v_1, v_1} L_{u_1 u_1} \neq 0$. And if $(v_1, u_1), (v_2, u_2) \not \in E(G \cdot H)$, then either $v_1 \neq v_2$ and $v_1v_2 \not \in E$ (hence $M_{v_1v_2} = 0$) or $N_{u_1 u_2} = 0$. Thus $L_{(v_1, u_1), (v_2, u_2)} = 0$. Therefore $L$ satisfies the required conditions, showing $f(G \cdot H) \leq k \ell = f(G) f(H)$. 
\item Enumerate elements of $X$ by $x_0, \dots, x_{4k - 1}$ where $k = m / 4$. Consider the subset of $V(G_m)$ given by all sets $X(a, i) = \{x_a, x_{a + 1}, x_{a + 2}, x_{a + 3}\} \setminus \{x_{a + i}\}$ for some $a \in \{0, \dots, k - 1\}$ and $i \in \{0, \dots, 3\}$. Note $\#X(a, i) = 3$ for all $a, i$. Furthermore, whenever $a \neq b$ the sets $X(a, i)$ and $X(b, j)$ are disjoint. If $a = b$ but $i \neq j$ then $X(a, i) \cap X(b, j)$ consists of $2$ elements (since we take out $x_{a + i}$ and $x_{a + j}$. Hence the set 
\[
\{X(a, i) \mid a \in \{0, \dots, k - 1\}, i \in \{0, \dots, 3\}\} \subseteq V(G_m)
\]
is an independent set. It clearly has $4k = m$ elements, showing $\alpha(G_m) \geq m$.
\item Clearly $N^T N$ is a matrix of dimension $V(G_m) \times V(G_m)$. Since it factors as a linear map $\RR^{X^{(3)}} \to \RR^X \to \RR^{X^{(3)}}$, we have $\dim \im N^T N \leq |X| = m$. Furthermore, if $A \in X^{(3)}$ then 
\[
N^TN_{AA} = \sum_{x \in A} N_{xA}^2 = \#A = 3 = 1 \neq 0.
\]
Furthermore if $A \neq B \in X^{(3)}$ and $AB \not \in E(G_m)$ then $A \cap B$ has exactly $2$ or $0$ elements (if it had $1$ there would be an edge, if it had $3$ then $A = B$), hence 
\[
N^TN_{AB} = \sum_{x \in A} N_{xA}N_{xB} = \#\{A \cap B\} = 0,
\]
since we are working over $\FF_2$. Thus $N$ satisfies the conditions and $f(G_m) \leq m$, so $\Theta(G_m) \leq m$. But by (c) $\Theta(G_m) \geq \alpha(G_m) \geq m$, hence $\Theta(G_m) = m$.
\end{enumerate}
\end{opghand}
\end{document}