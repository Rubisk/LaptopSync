\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\diam}{diam}


% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\HH}{\mathbb{H}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catO}{\mathcal{O}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Methods in Combinatorics Homework 2}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1.] \
\begin{enumerate}[(a)]
\item Recall that any eigenvalue of $A_{G_{p, t}}$ arises as a sum $\sum_{s \in S} \chi(S)$, where $\chi: \ZZ^{t + 1}_p$ is any character (and conversely any character gives an eigenvalue in this way). Note that the characters of $\ZZ^{t + 1}_p$ are all of the form
\[
\chi: \ZZ^{t +1}_p \to \CC: (a_1, \dots, a_{t + 1}) \mapsto \zeta^{c_1 a_1} \cdots \zeta_{t + 1}^{c_{t + 1} a_{t + 1}}
\]
where $\zeta$ is a primitive $p$-th root of unity and the $c_i \in \ZZ_p$ are fixed constants determining $\chi$. Thus any eigenvalue of $S$ can be written as
\begin{align*}
\sum_{a, b \in \ZZ_p} \zeta^{c_1b + c_2 ab + \dots + c_{t + 1} a^tb} &= \sum_{b \in \ZZ_p} \sum_{a \in \ZZ_p} \(\zeta^{c_1 + \dots + c_{t + 1}a^t} \)^b \\&= p \cdot \#\{a \in \ZZ_p \mid c_1 + \dots + c_{t + 1}a^t \equiv 0 \pmod{p} \}.
\end{align*}
for a choice of $c_i \in \ZZ_p$ (the second equality follows from the orthogonality relations). If all $c_i$ are set to $0$, then obviously all $a$ satisfy the equation and we obtain the eigenvalue $p^2$. For any other character at least one $c_i$ is nonzero, hence the equation $c_1 + \dots + c_{t + 1}a^t$ is a polynomial of degree $t$ in $a$. Since $\ZZ_p$ is a domain this has at most $t$ solutions, hence the eigenvalue is bounded above by $pt$. Since our last expression is obviously always positive we conclude the largest eigenvalue is equal to $p^2$, and that all others are in $[0, pt]$.

\item Let $p_n$ be the $(n + 2)$-th prime (unfortunately some inequality doesn't hold for $p = 3$). Set $t_n = \lfloor p_n / 2 \rfloor - 1$ and consider $G_n = G_{p_n, t_n}$. Note that $G_n$ has degree $p_n^2$ (as the set $S$ contains $p_n^2$ elements). Furthermore the second largest eigenvalue of the adjacency matrix is at most $p_n t_n$, hence the second eigenvalue of the Laplacian is at least $1 - p_n t_n / p_n^2 = 1 - t_n / p_n \geq 1 / 2 $. Therefore $\phi(G_n) \geq \lambda_2 / 2 \geq 1 / 4$.

Obviously the group $\ZZ_{p_n}^{t_n + 1}$ has $p_n^{\lfloor p_n / 2 \rfloor}$ elements, so $G_n$ has that many vertices. And $G_n$ has degree $p_n^2$ as shown in (a). One easily verifies that $2 \lfloor x / 2 \rfloor  \log(x) \geq x$ holds for $x \geq 5$. Thus since $p_n \geq 5$ we have $2 \log(|G_n|) = 2 \lfloor p_n / 2 \rfloor \log(p_n) \geq p_n$ hence $\(2 \log(|G_n|)\)^2 \geq p_n^2$ as required.
\end{enumerate}
\end{opghand}
\begin{opghand}[2.] \
\begin{enumerate}[(a)]
\item First fix a vertex $x \in V(G)$. Define inductively $S_0 = S_0(x) = \{x\}$ and $S_{k + 1} = S_k \cup N_G(S_k)$ (here $N_G(Y) = \{v \in V(G) \mid \exists y \in V(G): (v, y) \in E(G)\}$ is the set of neighbours of $Y$.)

Note that as long as $|S_k| < |V| / 2$ we have $e_G(S_k, \overline{S_k}) \geq d |S_k| \phi_0$ by definition of $\phi_0$. Since any vertex in $\overline{S_k} \cap N(S_k)$ can connect to at most $d$ vertices in $S_k$, we see that $|N(S_k) \cap \overline{S_k}| \geq |S_k| \cdot \phi_0$. Hence as long as $|S_k| < |V| / 2$ we have $|S_{k + 1}| \geq (1 + \phi_0)|S_k|$, or $|S_k| \geq (1 + \phi_0)^k$.

However note that by construction any $v \in S_k$ satisfies $d(v, x) \leq k$. Also note that if one has $(1 + \phi_0)^k \geq |V| / 2$ then $|S_k| \geq |V| / 2$. Hence if we set $k = \lceil \log(|V| / 2) / \log(1 + \phi_0) \rceil$ then $|S_k(x)| \geq |V| / 2$. It follows that if we pick $x, y \in V(G)$ arbitrarily, then $S_{k + 1}(x)$ and $S_{k + 1}(y)$ cannot be disjoint (theoretically it is possible that $S_k(x)$ and $S_k(y)$ have size exactly half the graph). Thus if we pick $v \in S_{k + 1}(x) \cap S_{k + 1}(y)$, then $d(v, x) \leq k + 1$ and $d(v, y) \leq k + 1$. Thus $d(x, y) \leq 2k + 2 \leq C \log(n)$ for some constant $C$ depending only on $\phi_0$. Since $x$ and $y$ were arbitrary we conclude $\diam(G) \leq C \log(n)$.
\item Let $C_k(G, v)$ be the number of vertices at exactly distance $k$ of $v$. Note that for $G$ to be $d$-regular, it must have been generated by an abelian group $\Gamma$ with a symmetric set $S$ satisfying $|S| = d$. Note that 
\begin{align*}
C_k(G, v) &= \{w \in \Gamma: \exists x_0 = v, x_1, \dots, x_{k - 1}, x_k = w \in \Gamma: x_i x_{i + 1}^{-1} \in S\} \\
&= \{w \in \Gamma: \exists s_1, \dots s_{k} \in S: s_1 \cdots s_{k} = vw^{-1}\} \\
&= \{w \in \Gamma: vw^{-1} \in S^k\}.
\end{align*}
Therefore $|C_k(G, v)| \leq |S^k|$. If we enumerate now $S = \{s_1, \dots, s_d\}$ then clearly (since $S$ is abelian) $S^k = \{s_1^{i_1} \cdots s_d^{i_d}\mid i_1 + \dots + i_d = k \}$. Hence we reduced the problem to counting the number of tuples of $d$ nonnegative integers with sum $k$, which is well known to be $\binom{k + d - 1}{d - 1}$. One way to see this is to represent such a tuple as $n$ bals seperated by $d - 1$ fences, clearly such a row of balls represents a tuple and we can place te fences in exactly $\binom{k + d - 1}{d - 1}$ ways. 

It follows that $|C_k(G, v)| \leq \binom{k + d - 1}{d - 1}$ and 
\begin{align*}
|B_k(G, v)| \leq \sum_{i = 0}^k C_i(G, v) &\leq \sum_{i = 0}^k \binom{i + d - 1}{d - 1} = \binom{k + d}{d}
\end{align*}
where the last equality is known as the hockey-stick identity. For completeness we will give a nice proof using a telescoping sum:
\begin{align*}
\sum_{i = 0}^k \binom{i + d - 1}{d - 1} &= \sum_{i = 0}^k \(\binom{i + d}{d} - \binom{i+ d - 1}{d}\) \\
&= \sum_{i = 0}^k \(\binom{i + d}{d} - \binom{(i - 1) + d}{d}\) \\
&= \binom{k + d}{d}.
\end{align*}
\item We assume the $c$ mentioned in the exercise refers to $\phi_0$. From (a) it follows that there exists a constant $C = C(\phi_0)$ such that any graph $G$ satisfying $\phi(G) \geq \phi_0$ satisfies $\text{diam}(G) \leq C\log(n)$. Therefore we must have $B_{\lceil C \log(n) \rceil}(G, v) \geq n - 1$ for all $v \in G$. Thus any such $G$ satisfies $\binom{\lceil C \log(n) \rceil + d}{d} \geq n - 1$. However $\binom{\lceil C \log(n) \rceil + d}{d} = O(\log(n)^d)$. Since $\log(n) = o(n^{1 / d})$ it follows that this cannot happen for $n$ sufficiently large. Therefore any such $G$ is generated by a group of bounded size.

Since there are only finitely many groups of a certain size, and only finitely many subsets $S$ of these groups, there can be only finitely many Cayley graphs $G$ satisfying $\phi(G) \geq \phi_0$.
\end{enumerate}

\end{opghand}
\begin{opghand}[3.] \
\begin{enumerate}[(a)]
\item Recall that $A^k_{vw}$ is simply the number of paths from $v$ to $w$ of length $k$. Thus  $A^k_{vw} = 0$ for all $k < d(v, w)$.

Now let $p = \sum_{i = 0}^m a_i x^i $ be any polynomial. Then $p(A)_{vw} = \sum_{i = 0}^m a_i A^i_{vw}$, hence if $m < d(v, w)$ this is zero. Therefore $p(A)$ can only have nonzero entries everywhere if $m \geq \Delta$.

\item We first show by induction on $k$ that for $v, w\in V$ with  $L_{vw}^k = 0$ if $k < d(v, w)$. If $k = 1$ this clearly holds, since the nonzero entries of $L$ are exactly given by the $v, w$ such that either $v = w$ (distance $0$) or $(v, w) \in E$ (distance $1$). Now suppose $k > 1$, and note that by definition
\begin{align*}
(D - A)^k_{vw} &= \sum_{x \in V} (D - A)^{k - 1}_{vx} \cdot (D - A)_{xw} \\
&= \deg(w) (D - A)^{k - 1}_{vw} - \sum_{x\in N(w)}(D - A)^{k - 1}_{vx}.
\end{align*}
If $d(v, w) > k$ then $d(v, x) > k - 1$ for all $x \in N(w) \cup \{w\}$, hence all terms vanish by induction. This completes our induction proof.

Now we can just repeat the proof given in (a). Thus let $p = \sum_{i = 0}^m a_i x^i $ be any polynomial. Then $p(L)_{vw} = \sum_{i = 0}^m a_i L^i_{vw}$, hence if $m < d(v, w)$ this is zero. Therefore $p(A)$ can only have nonzero entries everywhere if $m \geq \Delta$.
\item We can diagonalize $L$ by writing 
\[
L = \begin{pmatrix}u_1 & \dots & u_n \end{pmatrix} \begin{pmatrix}\lambda_1 & & 0 \\  & \ddots & \\ 0 & & \lambda_n \end{pmatrix}\begin{pmatrix}u_1 & \dots & u_n \end{pmatrix}^T.
\]
We can write this more compactly as $L = \sum_{i = 1}^n u_i e_i^T \lambda_i e_i u_i^T = \sum_{i = 1}^n u_i \lambda_i u_i^T$. Thus $L^k = \sum_{i = 1}^n \lambda_i^k u_i u_i^T$.

Now if $p(0) = 1$, then by linearity $p(L) = \sum_{i = 1}^n p(\lambda_i)u_i u_i^T = u_1 u_1^T + \sum_{i = 2}^n p(\lambda_i)u_i u_i^T$. Since the eigenvector corresponding to the eigenvalue $0$ is the all ones vectors which has norm $\sqrt{n}$, after normalzing $u_1u_1^T$ will be equal to $1 / n \times J$.
\item Suppose $0, \mu_1, \dots, \mu_r$ are the $r + 1$ distinct eigenvalues of $L$ and consider the polynomial $p(x) = (x - \mu_1) \cdots (x - \mu_r)$. Then clearly $p(\lambda_i) = 0$ for all $i > 1$, hence 
\[
p(L) = \frac{J}{n} +  \sum_{i = 2}^n p(\lambda_i)u_i u_i^T = \frac{J}{n}.
\]
Thus $p(L)$ has nonzero entries everywhere. Therefore $r = \deg(p) \geq \Delta$ by (b).
\end{enumerate}
\end{opghand}
\end{document}