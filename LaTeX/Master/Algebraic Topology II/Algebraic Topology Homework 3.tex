\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}
\usepackage[normalem]{ulem}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{claim}[1]{\par\noindent\textbf{Claim:}\space#1}{}


% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\Ext}{Ext}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\Map}{Map}


% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Topology Homework 3}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[2] We can express
\[
Cf = \(A \times [0, 1] \sqcup X\) / \sim
\]
where 
\begin{align*}
(a, 0) &\sim f(a) &\forall a \in A,\\
(a, 1) &\sim (a', 1) &\forall a, a' \in A, \\
(a_0, s) &\sim (a_0, t) &\forall s, t \in [0, 1].
\end{align*}
To show the inclusion $X \to Cf$ is a cofibration it suffices to show the pair $(Cf, X)$ satisfies HEP. Thus let $G\colon X \times [0, 1] \to Z$ be a pointed homotopy of spaces, and $g\colon Cf \to Z$ any continuous (pointed) map satisfying $g |_X = G|_{X \times \{0\}}$. We need to construct $H\colon Cf \times [0, 1] \to Z$ such that $H|_{Cf \times \{0\}} = g$ and $H|_{X \times [0, 1]} = G$. 

Note that we can lift $g$ to a map $\tilde g: A \times [0, 1] \sqcup X \to Z$ such that
\begin{align*}
\tilde{g}(a, 0) &= \tilde{g}(f(a)) = G(f(a), 0) & \forall a \in A,\\
\tilde{g}(a, 1) &= \tilde{g}(a', 1) &\forall a, a'\in A,\\
\tilde{g}(a_0, s) &= z_0 &\forall s \in [0, 1].
\end{align*}
Now, consider the map (of unpointed spaces)
\begin{align*}
Q\colon  \(A \times [0, 1] \cup X\) \times [0, 1] &\to Z \\
((a, s), t) &\mapsto \begin{cases}
G(f(a), t - 2s) & 2s \leq t \\
\tilde{g}\(a, 1 - \frac{2}{2 - t}(1 - s)\)  & 2s \geq t
\end{cases} \\
(x, t) &\mapsto G(x, t)
\end{align*}
It isn't very hard to verify that in fact $1 - \frac{2}{2 - t}(1 - s) \in [0, 1]$ for all $s \in [0, 1]$ and $t \in [0, 2s]$, which implies this is well-defined on $2s \neq t$.
Whenever $t = 2s$, one has $t - 2s = 0$ and $1 - \frac{2}{2 - t}(1 - s) = 0$. Thus 
\[
\tilde{g}\(a,  1 - \frac{2}{2 - t}(1 - s)\) = \tilde{g}(a, 0) = G(f(a), 0) = G(f(a), t - 2s),
\]
hence by the pasting lemma our case splitting glues to a continuous map $A \times [0, 1]^2 \to Z$. Therefore $Q$ is well-defined and continuous. Since $Cf \times [0, 1]$ is homeomorphic to $\(A \times [0, 1] \cup X\) \times [0, 1] / \sim$ as $[0, 1]$ is compact, it suffices to show $Q$ factors through $\sim$ to obtain a map $H: Cf \times [0, 1] \to Z$. We check all three conditions:
\begin{itemize}
\item For all $a \in A$ and $t \in [0, 1]$ one has $Q((a, 0), t) = G(f(a), t)= Q(f(a), t)$, hence $Q$ is compatible with identifying $((a, 0), t) \sim (f(a), t)$. 
\item For all $a, a' \in A$ and $t \in [0, 1]$ one has $2 \geq t$ hence 
\[
Q((a, 1), t) = \tilde{g}(a, 1) = \tilde{g}(a', 1) = Q((a', 1), t).
\]
Therefore $Q$ is compatible with identifying $((a, 1), t) \sim ((a', 1), t)$.
\item For all $s, t \in [0, 1]$, we see that $Q((a_0, s), t)$ is either $G(x_0, t - 2s)$ or $\tilde{g}(x_0, 1 - \frac{2}{2 - t}(1 - s))$ which always evaluates to $z_0$. Hence $Q((a_0, s_0), t) = Q((a_0, s_1), t)$ for all $s_0, s_1, t \in [0, 1]$ and $Q$ is compatible with identifying $((a_0, s_0), t) \sim ((a_0, s_1), t)$. 
\end{itemize}
Therefore $Q$ factors as a map $H(Cf \times [0, 1]) \to Z$. To check that $H|_{Cf \times \{0\}} = g$ it suffices to show $Q|_{A \times [0, 1] \sqcup X \times \{0\}}  = \tilde{g}$. For $(a, s) \in A \times [0, 1]$ we have $Q((a, s), 0) = \tilde{g}(a, 1- (1 - s)) = \tilde{g}(a, s)$. And for any $x \in X$ we have $Q(x, 0) = G(x, 0) = g(x) = \tilde{g}(x)$. Therefore $Q|_{A \times [0, 1] \sqcup X \times \{0\}}  = \tilde{g}$ and $H|_{Cf \times \{0\}} = g$. Since obviously $H_{X \times [0, 1]} = Q|_{X \times [0, 1]} = G$, our constructed map $H$ satisfies all the required properties.
\end{opghand}
\begin{opghand}[5] \
\begin{enumerate}[(a)]
\item To show a map is continuous, it suffices to show the inverse image of any base open is open, since then the inverse image of any open is a union of opens, hence open. 

Thus first suppose $f: X \to Y$ is a continuous map, and let $W(K, U)$ be a base open of $\Map(X, Z)$ for some $K \subseteq X$ compact, $U \subseteq Z$ open. Then 
\[
\(f^*\)^{-1}(W(K, U)) = \{[g] \mid g: Y \to Z, g \circ f(K) \subseteq U\}.
\]
But since $f$ is continuous, $f(K)$ is compact in $Y$. And hence this set is equal to
\[
\{[g] \mid g: Y \to Z, g(f(K)) \subseteq U\} = W(f(K), U),
\]
which is open in $\Map(Y, Z)$. Thus $f^*$ is continuous.

For the pointed case, now also assume $f$ is a continous map of pointed spaces. By definition of the subspace topology, any base open of $\Map^\bullet(X, Z)$ is of the form $W(K, U) \cap \Map^\bullet(X, Z)$. Hence pick any such base open. Then if we denote $f^*_\bullet: \Map^\bullet(Y, Z) \to \Map^\bullet(X, Z)$ for the induced map, one has
\begin{align*}
\(f_\bullet^*\)^{-1}\(W(K, U) \cap \Map^\bullet(X, Z)\) &= \{[g] \mid g: Y \to Z, g(y_0) = z_0, g \circ f(x_0) = z_0, g \circ f(K) \subseteq U\} \\
&= \{[g] \mid g: X \to Z, g(y_0) = z_0, g \( f(K)\) \subseteq U\}  \\
&= W(f(K), U) \cap \Map^\bullet(Y, Z),
\end{align*}
note that $g \circ f(x_0) = g(y_0) = z_0$ holds automatically as $f(x_0) = y_0$. Since the latter is open in $\Map^\bullet(Y, Z)$ by definition of the subspace topology, this shows $f^*_\bullet$ is continuous.
\item We first show that the map $\Phi: \Map^\bullet(S^1, X) \times \Map^\bullet(S^1, X) \to \Map^\bullet(S^1 \vee S^1, X)$ sending two functions $f, g \to f \vee g$ is continuous (the wedge two functions is well defined and continuous since we are working with pointed maps). Denote $\iota_1, \iota_2: S^1 \to S^1 \vee S^1$ for the two inclusions, and let $K \subseteq S^1 \vee S^1$ be compact, $U \subseteq X$ open. We need to show $\Phi^{-1}(W(K, U))$ is open. Let $K_i = \iota_i^{-1}(K)$ for $i = 1, 2$. Then clearly $K_i$ is compact in $S^1$, since $\iota_i$ is a homeomorphism onto its image, and $K \cap \im(\iota_i)$ is closed in $K$ hence compact. By construction, 
\[
\Phi^{-1}(W(K, U)) = W(K_1, U) \times W(K_2, U),
\]
and the latter is open in $\Map^\bullet(S^1, X) \times \Map^\bullet(S^1, X) $, which shows $\Phi$ is continuous.

Now consider the map 
\[
S^1 \to S^1 \vee S^1:t \mapsto \begin{cases} \iota_1(2t) &0 \leq 2t \leq 1, \\ \iota_2(2t - 1) &1 \leq 2t \leq 2. \end{cases}
\]
by the pasting lemma it is continuous (we identify $S^1 \cong [0, 1] / \sim$ in the above). Thus by (a) the induced map $ \Map^\bullet(S^1 \vee S^1, X) \to \Map^\bullet(S^1, X)$ is continous. Hence the composition of the two maps 
\[
\Map^\bullet(S^1, X) \times \Map^\bullet(S^1, X) \to \Map^\bullet(S^1 \vee S^1, X) \to \Map^\bullet(S^1, X)
\]
is continuous, but this is the definition of the ``concatenation of paths'' operation. Thus the ``concatenation of paths'' map $\Map^\bullet(S^1, X) \times \Map^\bullet(S^1, X)  \to \Map^\bullet(S^1, X)$ is continuous, as required.

The inverse map is much easier, if we consider the continuous map $S^1 \to S^1: t \mapsto -t$, then the  induced map $\Map^\bullet(S^1, X) \to \Map^\bullet(S^1, X)$ is clearly the inverse map, and continuous by (a).
\item We assume $X$ is a pointed space $(X, x_0)$ and $\pi_n(X) = \pi_n(X, x_0)$, as there seems to be no reason for $\pi_n$ to be basepoint-invariant. Write $S^n = [0, 1]^n / \partial [0, 1]^n$, so that an element of $\pi_n(X)$ can be written as a map $[0, 1]^n \to X$ that is constant to $x_0$ on the boundary. Recall that the group operation on $\pi_n(X)$ was given by 
\[
f * g(x_1, \dots, x_n) = \begin{cases}
f(x_1, \dots, x_{n - 1}, 2x_n) & 2x_n \leq 1 \\
g(x_1, \dots, x_{n - 1}, 2x_n - 1) & 2x_n \geq 1 \\
\end{cases}
\]
It follows that for $f, g, h, \ell \in \pi_n(X)$ and $(x_1, \dots, x_n) \in [0, 1]^n$ one has 
\begin{align*}
m(&f(x_1, \dots, x_n), g(x_1, \dots, x_n)) * m(h(x_1, \dots, x_n), \ell(x_1, \dots, x_n)) \\ 
&= \begin{cases}
m(f(x_1, \dots, 2x_n), g(x_1, \dots, 2x_n)) & 2x_n \leq 1 \\
 m(h(x_1, \dots, 2x_n - 1), \ell(x_1, \dots, 2x_n - 1))& 2x_n \geq 1
\end{cases}
\\
&= m(f * h(x_1, \dots, x_n), g * \ell(x_1, \dots, x_n)).
\end{align*}
Therefore $m$ and $*$ commute. By the lemma they must agree and be abelian.
\end{enumerate}
\end{opghand}
\end{document}
