\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}
\usepackage[normalem]{ulem}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{claim}[1]{\par\noindent\textbf{Claim:}\space#1}{}


% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\Ext}{Ext}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Topology Homework 2}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[4] We will suppress notation for the coefficients for this exercise, all coefficients are assumed to be in some commutative ring $R$.
\begin{enumerate}[(a)]
\item Recall that $SX = X \times [0, 1] / \sim$, where $(x, t) \sim (y, t)$ for all $x, y \in X$ and $t \in \{0, 1\}$. Denote $p: X \times [0, 1] \to SX$ for the quotient map and let $C_+ = p^{-1}(X \times (0, 1])$, $C_- = p^{-1}(X \times [0, 1))$. Note that $C_+$ and $C_-$ are contractible, for example the map 
\[
H: C_- \times [0, 1] \to C_-: (p(x, t), s) \mapsto p(x, t(1 - s))
\]
defines a deformation retraction from $C_-$ onto $\{p(x, 0)\}$.

Now consider the diagram
\[
\begin{tikzcd}
H^k(SX) \times H^\ell(SX) \dar{-\cup -}& \lar H^k(SX, C_-) \times H^\ell(SX, C_+) \dar{-\cup -}\\
H^{k + \ell}(SX)  &\lar H^{k + \ell}(SX, C_+ \cup C_-)
\end{tikzcd}
\]
in which the top and bottom map come from the long exact sequences of the pairs. Note that since the cup product and relative cup product are given by the exact same operation, the diagram commutes. 

Now consider the long exact sequence of the pair $(C_+ \cup C_-, SX)$.
\begin{align*}
\dots \to H^{k + \ell - 1}(SX) &\to H^{k + \ell - 1}(C_+ \cup C_-) \to H^{k + \ell}(SX, C_+ \cup C_-)  \\ &\to H^{k + \ell}(SX) \to H^{k + \ell}(C_+ \cup C_-) \to \dots
\end{align*}
Since $C_+ \cup C_- = SX$, the first and last maps are isomorphisms, hence $H^{k + \ell}(SX, C_+ \cup C_-) = 0$.

Next, consider the long exact sequence of the pair $(SX, C_-)$.
\begin{align*}
\dots \to H^{k - 1}(C_-) \to H^{k}(SX, C_-)  \to H^{k}(SX) \to H^{k}(C_-) \to \dots
\end{align*}
Since $C_-$ is contractible and $k > 0$, $H^k(C_-) = 0$ and the map $H^{k}(SX, C_-)  \to H^{k}(SX)$ must be surjective. By the exact same argument the map $H^{k}(SX, C_+)  \to H^{k}(SX)$ is surjective.

Hence in our first diagram, the map at the top is surjective. Thus given a pair of elements $(\alpha, \beta) \in H^k(SX) \times H^\ell(SX)$ we can find a pair $(\gamma, \delta)$ in $ H^k(SX, C_-) \times H^\ell(SX, C_+)$ mapping to $(\alpha, \beta)$. Going down and then left, noting that $H^{k + \ell}(SX, C_+ \cup C_-)  = 0$ we see that $(\gamma, \delta)$ maps to $0$ in $H^{k + \ell}(SX)$. It follows by commutativity of the diagram that $\alpha \cup \beta = 0$, which is what we needed to show.
\item Let $x_i \in H^{k_i}(Y)$ for $i \in \{1, \dots, n\}$ and all $k_i > 0$. Set $V_i = U_1 \cup \dots \cup U_i$ and $m_i = k_1 + \dots + k_i$. We show by induction on $i$ that there exists $\alpha_i \in H^{m_i}(Y, V_i)$ that maps to $x_1 \cup \dots \cup x_i$ under the map $H^{m_i}(Y, V_i) \to H^{m_i}(Y)$ obtained from the long exact sequence of the pair $(Y, V_i)$.

In the case $i = 1$, consider the long exact sequence of the pair $(Y, U_1)$:
\begin{align*}
\dots \to H^{k - 1}(U_1) \to H^{k_1}(Y, U_1)  \to H^{k_1}(Y) \to H^{k_1}(U_1) \to \dots
\end{align*}
Since $k_1 > 0$ we know $H^{k_1}(U_1) = 0$ as $U_1$ is contractible. Thus we find that the map $H^{k_1}(Y, U_1)  \to H^{k_1}(Y)$ is surjective as required.

Now suppose $i > 1$. Pick $\alpha_{i - 1} \in H^{m_{i - 1}}(Y, V_{i - 1})$ mapping to $x_1 \cup \dots \cup x_{i - 1} \in H^{m_{i - 1}}(Y)$. Consider the diagram
\[
\begin{tikzcd}
H^{m_{i - 1}}(Y, V_{i - 1}) \times H^{k_i}(Y, U_i) \dar{-\cup-} \rar & H^{m_{i - 1}}(Y) \times H^{k_i}(Y) \dar{-\cup-}\\ 
H^{m_{i}}(Y, V_i)   \rar & H^{m_i}(Y)
\end{tikzcd}
\]
Again the diagram commutes because the relative and absolute cup product are defined by the same operation on cochains. From the case $i = 1$ applied to $U_1 = U_i$ we see that the map $ H^{k_i}(Y, U_i) \to H^{k_i}(Y)$ is surjective. Hence we can find a pair $(\alpha_i, \beta)$ mapping to $(x_1 \cup \dots \cup x_{i - 1}, x_i)$ under the map at the top. By commutativity of the diagram, we can find an element $\alpha_i \in H^{m_i}(Y, V_i)$ mapping to $(x_1 \cup \dots \cup x_{i - 1}) \cup x_i$ by the map at the bottom. This completes our induction.

Hence we may conclude there exists $\alpha_n \in H^{m_n}(Y, V_n)$ mapping to $x_1 \cup \dots \cup  x_n \in H^{m_n}(Y)$. But note that $V_n = Y$ as the $U_i$ cover $Y$. By the exact same argument as used in (a) for the pair $(SX, C_+ \cup C_-)$, we have $H^{m_n}(Y, V_n) = 0$. Thus $\alpha_n = 0$ and $x_1 \cup \dots \cup x_n = 0$ as it is the image of $\alpha_n$.
\end{enumerate}
\end{opghand}
\begin{opghand}[5] Again we suppress coefficients for this exercise, however this time they will be in $\ZZ$ as usual.
\begin{enumerate}[(a)]
\item Recall from Algebraic Topology I that $\Sigma_g$ has a CW-complex structure with one $0$-cell, $2g$ $1$-cells and $1$ 2-cell, and that the cellular chain complex is given by 
\[
\begin{tikzcd}
\ZZ \rar{0}&  \ZZ^{2g} \rar{0} & \ZZ \rar{0}&  0 \rar & \dots
\end{tikzcd}
\]
Applying the Hom-functor yields the complex 
\[
\begin{tikzcd}
\dots \rar & 0 \rar{0} & \ZZ  \rar{0} &  \ZZ^{2g} \rar{0} & \ZZ
\end{tikzcd}
\]
We conclude that $H^0(\Sigma_g) \cong H^2(\Sigma_g) \cong \ZZ$ and $H^1(\Sigma_g) \cong \ZZ^{2g}$.
\item Consider the set $U$ of $\Sigma_g$ given by the following picture (modified from Hatcher's book)
\begin{center}
\includegraphics{genus_g.png}
\end{center}
Let $\sim$ be an equivalence relation on $\Sigma_g$ identifying all points in $U$, clearly $\Sigma_g / \sim$ is homotopic to $\bigvee_g \Sigma_1$. Denote $q: \Sigma_g \to \bigvee_g \Sigma_1$ for the quotient map.

\begin{claim}
Let $*$ denote the point at which the $g$ tori are wedged together. Then the map $q^*: H^k(\bigvee_g \Sigma_1, U) \to H^k(\Sigma_g, \{*\})$ induces an isomorphism on relative cohomology for any $k$.
\end{claim}
\begin{proof}
Let $U_0 \subseteq \Sigma_g$ be an open set containing $U$ that is homotopic to $U$, such that $V := q(U_0)$ is still contractible. Note that $V$ contains $*$. By homotopy invariance, it suffices to show $q^*: H^k(\bigvee_g \Sigma_1, U_0) \to H^k(\Sigma_g, V)$ is an isomorphism. Let $U \subseteq W \subseteq U_0$ be closed and $W' = q(W)$, note that since $q$ is a quotient map $q(W)$ is closed. The key point is that the spaces $\bigvee_g \Sigma_1 \setminus W'$ and $\Sigma_g \setminus W$ are homotopic. Indeed, if we let $Z \subseteq \Sigma_1$ be a small contractible closed disk, then both spaces are clearly homotopic to $\bigsqcup_g \Sigma_1 \setminus Z$ (picture below). 
\begin{center}
\includegraphics{disjoint_tori.png} \\ \emph{The space that remains after excising $V$ or $U_0$.}
\end{center}
Consider the inclusions of pairs 
\begin{align*}
\iota_0: \(\bigsqcup_g  \Sigma_1 \setminus \text{int}(Z), \bigsqcup_g  \partial Z\) &\cong (\Sigma_g \setminus W, U_0 \setminus W) \to (\Sigma_g, U_0) \\
\iota_1: \(\bigsqcup_g  \Sigma_1 \setminus \text{int}(Z), \bigsqcup_g  \partial Z\) &\cong \(\bigvee_g \Sigma_1 \setminus W', V \setminus W'\)  \to \(\bigvee_g \Sigma_1, V\)  
\end{align*}
Note that the diagram
\[
\begin{tikzcd}
(\Sigma_g \setminus W, U_0 \setminus W) \dar{\sim} \rar{\iota_0} & (\Sigma_g, U_0) \dar{q}\\
 \(\bigvee_g \Sigma_1 \setminus W', V \setminus W'\) \rar{\iota_1} &   \(\bigvee_g \Sigma_1, V\)
\end{tikzcd}
\]
obviously commutes since $q$ is just the identity outside $U$. Thus if we consider the diagram 
\[
\begin{tikzcd}
H^k\(\bigvee_g \Sigma_1, V\)  \dar{q^*} \rar{\iota_1^*} &   H^k\(\bigvee_g \Sigma_1 \setminus W', V \setminus W'\) \dar{\sim}  \\
H^k(\Sigma_g, U_0)  \rar{\iota_0^*} & H^k(\Sigma_g \setminus W, U_0 \setminus W) 
\end{tikzcd}
\]
then the right arrow is an isomorphism by homotopy invariance, and the horizontal arrows are isomorphisms by excision. It follows that $q^*$ is an isomorphism, this proves the claim. 
\end{proof}
To show that $q^*: H^1(\bigvee_g \Sigma_1) \to H^1(\Sigma_g)$ is an isomorphism as well, consider the LES of pairs
\[
\begin{tikzcd}
\dots \to H^0(\{*\}) \dar{\substack{q^* \\ \cong}} \rar&  H^1\(\bigvee_g \Sigma_1, \{*\}\) \dar{\substack{q^* \\ \cong}}  \rar&   H^1\(\bigvee_g \Sigma_1\) \dar{q^*} \rar&   0  \dar{q^*} \rar& \dots \\
\dots \to H^0(U) \rar&  H^1\(\Sigma_g, U\) \rar&   H^1\(\Sigma_g\) \rar&   H^1(U) \rar& \dots 
\end{tikzcd}
\]
\begin{claim}In the above diagram, the map  $H^1(\Sigma_g) \to H^1(U)$ is the zero map.
\end{claim}
\begin{proof}
In this diagram, the leftmost $q^*$ is an isomorphism since $U$ is path connected (both are just $\ZZ$). The rightmost $q^*$ is injective as $H^1(\{*\})  = 0$. Since the second $q^*$ is an isomorphism by our claim, by the four lemma the other map $q^*: H^1\(\bigvee_g \Sigma_1\)  \to H^1(\Sigma_g)$ is also injective. Since $H^1\(\bigvee_g \Sigma_1\)   \cong \ZZ^{2g}$ and $H^1(\Sigma_g) \cong \ZZ^{2g}$ we thus consider the diagram
\[
\begin{tikzcd}
\ZZ^{2g} \rar{q^*}  & \ZZ^{2g}   \rar&  H^1(U)
\end{tikzcd}
\]
in which $q^*$ is injective. By commutativity of the square on the right in our previous diagram, the composition of these two maps is $0$. However, since $H_0(U) \cong \ZZ$ as $U$ is path connected, by the universal coefficient theorem $H^1(U)$ must be torsion free, say $H^1(U) \cong \ZZ^a$. We thus have a diagram that looks like
\[
\begin{tikzcd}
\ZZ^{2g} \rar{A}  & \ZZ^{2g}   \rar{B} &  \ZZ^a
\end{tikzcd}
\]
for some matrices $A, B$ with coefficients in $\ZZ$ such that $BA = 0$ and $A$ has full rank (since $q^*$ is injective). But by tensoring over $\QQ$ we conclude from linear algebra that $B = 0$, therefore the map $\ZZ^{2g} \to H^1(U)$ has to be the zero map. Thus the map $H^1(\Sigma_g) \to H^1(U)$ is the zero map.
\end{proof}
We are now ready to complete our argument. It follows by exactness and our second claim that $H^1(\Sigma_g, U) \to H^1(\Sigma_g)$ is surjective. By commutativity of the middle square and the isomorphism on relative homology it follows that $q^*: H^1\(\bigvee_g \Sigma_1\) \to H^1(\Sigma_g)$ is surjective and hence an isomorphism, which is what we needed to show. 
\item Note that by naturality and using the fact that $f^*$ is an isomorphism, for any $\alpha, \beta \in H^1(\Sigma_g)$ we have $\alpha \cup \beta = f^*((f^*)^{-1}(\alpha) \cup (f^*)^{-1}(\beta))$. Note that if $i \neq j$, then since two elements $x \in \{\alpha_i, \beta_i\}$ and $y \in \{\alpha_j, \beta_j\}$ get mapped to something in different parts of the wedge sum, their cup product $x \cup y = 0$  by exercise 3.15. Remains to show $\alpha_i^2 = \beta_i^2 = 0$ and $\alpha_i\beta_i = \sigma$. 

Recall that $H^*(\Sigma_1) \cong \ZZ[x_1, y_1] / (x_1^2, y_1^2)$. 

Under the isomorphism $(f^*)^{-1}$ an $\alpha_i$ will map to a generator of some $H_1(\Sigma_1)$, hence $\alpha_i^2$ is zero as $(f^*)^{-1}(\alpha_i)^2 = 0$. Similarly $\beta_i^2 = 0$.

To show $\alpha_i\beta_i  = \sigma$, we will first need to show that the map $H^2(\bigvee_g \Sigma_1) \to H^2(\Sigma_g)$ is surjective. Consider another part of the LES in (b):
\[
\begin{tikzcd}
\dots \rar &   H^2\(\bigvee_g \Sigma_1, \{*\}\) \dar{\substack{q^* \\ \cong}}  \rar&   H^2\(\bigvee_g \Sigma_1\) \dar{q^*} \rar&   0  \dar{q^*} \rar& \dots \\
\dots \rar & H^2\(\Sigma_g, U\) \rar&   H^2\(\Sigma_g\) \rar&   H^2(U) \rar& \dots 
\end{tikzcd}
\]
If $H^2(U) = 0$, then the map $H^2(\Sigma_g, U) \to H^2(\Sigma_g) $ is surjective by exactness. As the left $q^*$ in the diagram is an isomorphism by our first claim in (b), this implies by commutativity of the square that $H^2(\bigvee_g \Sigma_1) \to H^2(\Sigma_g)$ is surjective. 

To this end, note that $U$ is homotopic to a disk with $g - 1$ holes in it. One can construct a CW-structure on this space by taking $g$ points, $p_1, \dots, p_g$, attaching a loop $\ell_g$ to each point $p_g$ and a path $r_g$ from each $p_i$ to $p_1$ for $i > 1$. Finally one glues a single disk to this structure with attaching map given by the symbol $\ell_1p_2\ell_2p_2^{-1} \dots p_g \ell_g p_g^{-1}$. The corresponding cellular chain complex then looks like
\[
\begin{tikzcd}
\dots \rar & 0 \rar{0}&  \ZZ \rar{(f, 0)}  & \ZZ^g \oplus \ZZ^{g - 1} \rar{(0, g)} & \ZZ^{g} \rar{0} & 0
\end{tikzcd}
\]
where $f(n) = (n, \dots, n)$ and $g(a_1, \dots, a_{g - 1}) = (a_1 + \dots + a_{g - 1}, -a_1, \dots, -a_{g - 1})$. 

It follows that $H_2(U) = 0$ and $H_1(U) \cong \ZZ^g / \{ (n, \dots, n ) \mid n \in \ZZ\} \cong \ZZ^{g - 1}$. By the universal coefficient theorem again, $H^2(U) = 0$ as well as $H_2(U) = 0$ and $H_1(U)$ is torsion-free. We conclude that the map $H^2(\bigvee_g \Sigma_1) \to H^2(\Sigma_g)$  is surjective.

Now under $(f^*)^{-1}$ the elements $\alpha_i, \beta_i$ get mapped to the two generators $x_1, y_1$ of some $H^1(\Sigma_1)$ (up to signs) as $(f^*)^{-1}$ is an isomorphism . The cup product $(f^*)^{-1}(\alpha_i) \cup (f^*)^{-1}(\beta_i)$ is thus a generator of $H^2(\Sigma_1)$ . But since $q^*: \bigoplus_g H^2(\Sigma_1) \to H^2(\Sigma_g)$ is an isomorphism, by symmetry these generators must all get mapped to generators of $H^2(\Sigma_g)$, hence $\alpha_i \cup \beta_i = f^*(f^*{-1}(\alpha_i) \cup f^{-1}(\beta_i)) = \pm \sigma$ as required.
\end{enumerate}
\end{opghand}
\end{document}
