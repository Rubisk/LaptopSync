\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}
\usepackage[normalem]{ulem}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{claim}[1]{\par\noindent\textbf{Claim:}\space#1}{}


% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\Ext}{Ext}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\Map}{Map}
\DeclareMathOperator{\const}{const}



% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Topology Homework 4}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[3]
Fix $n \geq 1$. Set $Y_{n + 1} = S^n$. Fix an arbitrary point $y_0 \in S^n$. For $k \geq n + 1$, let $J_k$ be the set of all maps $f: S^{k} \to Y_k$, and define inductively
\[
Y_{k + 1} = \sqcup_{J_k} D^{k + 1} \cup_{f} Y_k
\]
with attaching maps $\partial D^{k + 1} = S^k \to Y_k$ given by the correspond maps $f$. Set $Y = \bigcup_{k \geq n + 1} Y_k$. Then clearly $\(Y_k\)$ gives $Y$ the structure of a CW-complex. Since we only attached cells of dimension $n + 2$ or higher, by an exercise from Algebraic Topology I we know that $\pi_n(Y, y_0) = \pi_n(S^n, y_0) = \ZZ$, and $\pi_k(Y, y_0) = \pi_k(S^n, y_0) = 0$ for $k < n$. For $k \geq n + 1$, any map $S^k \to Y$ is homotopic (by cellular approximation) to a map $S^k \to Y_k$. Since we attached a disk to $Y_k$ with this exact boundary map in $Y_{k + 1}$, we can contract this map along this disk, hence $\pi_k(Y, y_0) = 0$ for $k > n$. Thus $Y$ is a $K(\ZZ, n)$. It follows that $[X, Y]^\bullet \cong H^{n}(X; \ZZ)$ for $n \geq 1$. 

Since $X$ is an $n$-dimensional CW-complex, by cellular approxmation we have $[X, Y]^\bullet \cong [X, S^n]^\bullet$. By a `slowly rotating homotopy', any map $X \to S^n$ is clearly homotopic to a map $X \to S^n$ mapping to a fixed basepoint, hence $[X, S^n]^\bullet \cong [X, S^n]$. We conclude $[X, S^n] \cong H^n(X; \ZZ)$ as required.
\end{opghand}
\begin{opghand}[4] 
For clarity, if $\alpha, \beta$ are two paths, we denote by $\alpha * \beta$ the path that first walks through $\beta$ and then through $\alpha$ (as with function composition).
\begin{enumerate}[(a)] 
\item An element of $\pi_n(F_{p(e_0)}, e_0)$ can be represented by a map $f: I^n \to E$, such that $p \circ f (x) = p(e_0)$ for all $x \in I^n$, and $f(x) = e_0$ for all $x \in \partial I^n$. To make sense of the case $n = 0$, one can identify $\pi_0(F_{p(e_0)}, e_0)$ with maps $\{\star\} \to  F_{p(e_0)}$ with no conditions (we simply drop the basepoint completely), hence it makes sense to define $\partial I^0 = \emptyset$. Note that Lemma 7.7 still holds with $J_0  = I^0 = \{\star\}$.  Write $J_n = I^ n \times \{0\} \cup \partial I^n \times [0, 1]$, and consider the map 
\[
G: J_n \to E: \begin{cases}
G(x, 0) = f(x) &\forall x \in I^n, \\
G(x, t) = \gamma(t) &\forall x \in \partial I^n, t \in I.
\end{cases}
\]
Clearly $G$ is continuous, and we have a commutative diagram
\[
\begin{tikzcd}
\dar J^n \rar{G} & E \dar{p} \\
I^{n + 1 } \rar{T} & B
\end{tikzcd}
\]
where $T(x, t) = p \circ \gamma(t)$. Hence get a map $H_\gamma: I^{n + 1} \to E$ such that everything commutes. Then clearly $H_\gamma|_{\partial I^n \times \{1\}} = \const_{e_1}$, and $p \circ H_\gamma|_{ I^n \times \{1\}} = \text{const}_{p(e_1)}$,  hence $H_\gamma|_{I^n \times \{1\}}$ represents an element of $\pi_n(F_{p(e_1)}, e_1)$. We define $\gamma_*(f)$ to be this element.

To see that this does not depend on the choice of $H$, suppose we are given two homotopies $H_1, H_2: I^{n + 1} \to E$ making the above diagram commute. We need to check that $H_i|_{I^n \times \{1\}}$ are homotopic for $i = 1, 2$ as pointed maps $S^n \to (F_{p(e_1)}, e_1)$. 

Write $J_{n + 1} = I^{n + 1} \times \{0\} \cup I^{n + 1} \times \{1\} \cup J_{n} \times [0, 1]$, and consider the map
\[
G: J_{n + 1} \to E: \begin{cases}
G(x, 0) = H_\alpha(x) &\forall x \in I^{n + 1}, \\
G(x, 1) = H_\beta(x) &\forall x \in I^{n + 1}, \\
G(x, t) = H_0(x) &\forall x \in J_n, t \in I.
\end{cases}
\]
Note that $H_0(x) = H_1(x)$ for all $x \in J_n$. Hence $G$ is continuous, and we have a diagram
\[
\begin{tikzcd}
\dar J^{n + 1} \rar{G} & E \dar{p} \\
I^{n + 2} \rar{R} & B
\end{tikzcd}
\]
where $R(x_1, \dots, x_{n + 1}, t) = p \circ \gamma(x_{n + 1})$.  The diagram commutes since by the previous diagram $p \circ H_0(x_1, \dots, x_{n + 1}) = p \circ H_1(x_1, \dots, x_{n + 1})  T(x_1, \dots, x_{n + 1}) = \gamma(x_{n + 1})$. We thus obtain a map $\tilde H: I^ {n + 1} \to E$, clearly $\tilde H |_{I^n \times \{1\} \times I}$ gives the required homotopy between $H_0|_{I^ n \times \{1\}}$ and $H_1|_{I^ n \times \{1\}}$. 

Next, we need to check that if $\alpha$ and $\beta$ are homotopic maps $S^n \to F_{p(e_0)}$ then so are $\gamma_*(\alpha)$ and $\gamma_*(\beta)$. Thus let $\Psi: I^n \times I$ be a homotopy from $\alpha$ to $\beta$ (again identifying $S^n \cong I^n / \sim$). Let $H_\alpha, H_\beta: I^{n + 1} \to E$ be the maps obtained when constructing $\gamma_*(\alpha)$ and $\gamma_*(\beta)$. Write $J_{n + 1} = I^{n + 1} \times \{0\} \cup I^{n + 1} \times \{1\} \cup I^n \times \{0\} \times [0, 1] \cup \partial I^ n \times I \times [0, 1]$ , and consider the map
\[
G: J^n \to E: \begin{cases}
G(x, 0) = H_\alpha(x) &\forall x \in I^{n + 1}, \\
G(x, 1) = H_\beta(x) &\forall x \in I^{n + 1}, \\
G(x, 0, t) = \Psi(x, t) &\forall x \in I_n, t \in I, \\
G(x, s, t) = \gamma(s) &\forall x \in I_n, t, s \in I.
\end{cases}
\]
One easily checks everything glues together nicely. Furthermore, the diagram 
\[
\begin{tikzcd}
\dar J^{n + 1} \rar{G} & E \dar{p} \\
I^{n +2} \rar{R} & B
\end{tikzcd}
\]
commutes if one sets $R(x_1, \dots, x_{n + 1}, t) = p \circ \gamma(x_{n + 1})$. We thus obtain a map $H: I^ {n + 2} \to E$, clearly $H|_{I^n \times \{1\} \times I}$ is the required homotopy $\gamma_*(\alpha) \to \gamma_*(\beta)$. It maps into $F_{p(e_1)}$ because $R(x_1, \dots, x_n, 1, t) =  p \circ \gamma(1) = p(e_1)$. 

Thus our definition makes sense. It clearly behaves well with composition of paths, since given two paths $\gamma, \gamma'$ with $\gamma'(0) = \gamma(1)$, we obtain $H_\gamma: I^n \times [0, 1] \to E$ and $H_{\gamma'}:  I^n \times [0, 1] \to E$. The map 
\[
H: I^n \times [0, 1]  : (x, t) \mapsto \begin{cases} H_\gamma(x, 2t) & t \leq 1 / 2 \\ H_{\gamma'}(x, 2t - 1) & t \geq 1/2 \end{cases}
\]
then satisfies the required properties of the map for $\gamma' * \gamma$ making the square commute. Hence the diagram of maps
\[
\begin{tikzcd}
\pi_n\(F_{p(\gamma(0))}, \gamma(0)\)  \arrow[bend left=20]{rr}{(\gamma' * \gamma)_*} \rar{\gamma_*} & \pi_n\(F_{p(\gamma(1))}, \gamma(1)\) \rar{\gamma'_*} & \pi_n\(F_{p(\gamma'(1))}, \gamma'(1)\)
\end{tikzcd}
\]
commutes, which shows our definition is compatible with composition of paths.
\item First pick $e \in E$ arbitrary, and let $b \in B$. Then since $B$ is path connected, we have a path $\gamma: [0, 1] \to B$ from $e$ to $b$. Since we have a commuting square
\[
\begin{tikzcd}
\{\star\} \dar{\const_0} \rar{\const_e} & E \dar{p} \\
I \rar{\gamma} & B
\end{tikzcd}
\]
since $p$ is a Serre fibration and $\{\star\} \cong D^0$ we get a map $I \to E$ making everything commute. Hence we get a path $\gamma'$ from $e$ to a point $b_0 \in F_b$. By walking via $e$ we obtain, for any $b_0, b_1 \in B$, a path $\gamma: I \to E$ such that $p \circ \gamma(i) = b_i$ for $i = 0,1$. By (a) we obtain induced maps $\gamma_*: \pi_n(F_{b_0}) \to \pi_n(F_{b_1})$. 

Note that the maps given in (a) are a morphism of groups, since if represent two elements of $\pi_n$, say $\alpha, \beta: I^n \to E$, and we obtain maps $H_\alpha, H_\beta: I^{n + 1} \to E$ making the square in (a) commute, then the map 
\[
H_{\alpha * \beta}(x_1, \dots, x_n, t) = \begin{cases}
H_\beta(x_1, \dots, 2x_n, t) & 2x_n \leq 1 \\
H_\alpha(x_1, \dots, 2x_n - 1, t) & 2x_n \geq 1
\end{cases}
\]
gives a map making the corresponding square for $\alpha*\beta$ commute. But by construction, $H_{\alpha * \beta} |_{I^n \times \{1\}} = \gamma_*(\alpha) * \gamma_*(\beta)$, so we may conclude $\gamma_*(\alpha * \beta) = \gamma_*(\alpha) * \gamma_*(\beta)$. 

By (a) we also obtain $\bar{\gamma}_*\colon \pi_n(F_{b_1}) \to \pi_n(F_{b_0}) $. But clearly $\bar{\gamma}_* \circ \gamma_* = \id$, since we can simply use $H_{\bar{\gamma}}(x, t) := H_\gamma(x, -t)$. 


\item We will assume $E = W(\{b_0\} \to B)$ instead. Pick an element $\gamma \in \pi_1(B, b_0)$. Define the lift $\gamma': I \to E: s \mapsto (b_0, t \mapsto \gamma(ts))$. We define for any $\alpha \in \pi_n(F_{b_0}, b_0)$ the action of $\gamma$ on $\alpha$ by  $\gamma_*(\alpha) := \gamma'_*(\alpha)$. We need to check that if $\gamma_0, \gamma_1: I \to B$ are homotopic loops from $b_0$ to $b_0$, then $(\gamma_0)_*(\alpha) = (\gamma_1)_*(\alpha)$.

We first show $\gamma_0'$ and $\gamma_1'$ are homotopic. Thus let $H: I^2 \to B$ be any basepoint preserving homotopy from $\gamma_0$ to $\gamma_1$. Consider the map $G: J_2 \to E$ given by
\begin{align*}
G(0, t) &= \gamma_0'(t), \\
G(1, t) &= \gamma_1'(t), \\
G(s, 0) &= (b_0, \const_{b_0}),
\end{align*}
this is clearly continuous. 

The diagram 
\[
\begin{tikzcd}
J_2 \dar \rar{G} & E \dar{p} \\
I^2 \rar{H} & B
\end{tikzcd}
\]
commutes, the diagonal map $I^2 \to E$ gives the required homotopy from $\gamma_0'$ to $\gamma_1'$.

It thus suffices to check that if $\gamma_0, \gamma_1: I \to E$ are homotopic, then $(\gamma_0)* = (\gamma_1)* $. Thus let $H$ be a basepoint preserving homotopy from $\gamma_0$ to $\gamma_1$ and $\alpha \in \pi_n(F_{b_0}, b_0)$. Again, represent $\alpha$ as a map $I^n \to E$. Let $H_{\gamma_0}, H_{\gamma_1}:I^{n + 1} \to E$ be the maps obtained in (a). Then the map $G: J_{n + 1} \to E$ given by 
\begin{align*}
G(x, 0, t) &= \alpha(x) & \forall x \in I^n, t \in I \\
G(x, s, 0) &= H_{\gamma_0}(x, s) &\forall x \in I^n, s \in I\\
G(x, s, 1) &= H_{\gamma_1}(x, s) &\forall x \in I^n, s \in I\\
G(x, s, t) &= H(s, t) &\forall x \in \partial I^n, s, t \in I \\
G(x, s, t) &= H(s, t) &\forall x \in \partial I^n, s, t \in I
\end{align*}
makes the diagram
\[
\begin{tikzcd}
J_{n  + 1} \dar \rar{G} & E \dar{p} \\
I^{n + 2} \rar{L} & F_{b_0}
\end{tikzcd}
\]
commute if one sets $L(x, s, t) = p \circ H(s, t)$. Restricitng the diagonal map $Q: I^{n + 2} \to E$ to the top face $I^n \times \{1\} \times I$ gives a homotopy $(\gamma_0)_*(\alpha) \to (\gamma_1)_*(\alpha)$. 

This gives an action of $\pi_1(B, b_0)$ on $\pi_n(F_{b_0}, b_0) = \pi_n(\Omega B, \const_{b_0}) = \pi_{n + 1}(B, b_0)$. Since we get such an action for all $n$, we get an action on $\pi_n(B, b_0)$. It is an action by the last remark in (a).

We show that in the case of $n = 1$, the action is given by composition. To this end, let $\alpha, \gamma \in \pi_1(B, b_0)$. We will show that the action $\gamma_*(\alpha) = \bar{\gamma} * \alpha * \gamma$. Under the isomorphism $\pi_1(B, b_0) \to \pi_0(F_{b_0}, \const_{b_0})$ we send $\alpha$ to the map mapping non-basepoint in $S^0$ to the element $(b_0, \alpha) \in E$. 
Our construction in (a) tells us to consider the diagram
\[
\begin{tikzcd}[column sep=large]
\{\star\} \dar{0} \rar{\const_{(b_0, \alpha)}} & E \dar{p} \\
I \rar{\gamma} & B
\end{tikzcd}
\]
and find a lift $\gamma'\colon I \to E$, so that $\gamma_*(\alpha) = \gamma'(1)$. It is important to note that this $\gamma'$ is a completely different lift from the one above, and furthermore that in the case $n = 0$ the construction in (a) only depends on $p \circ \gamma$ as a map to $B$, hence we never have to consider the lift to $E$. Clearly
\[
\gamma'(t) = (b_0, s \mapsto (\gamma * \alpha)(s/2(1 + t)))
\]
satisfies
\[
p \circ \gamma'(t) = \gamma * \alpha(1/2(1 + t)) = \gamma(t)
\]
and $\gamma'(0) = (b_0, \alpha)$. Hence $\gamma_*(\alpha) = \gamma'(1) = \gamma * \alpha$. 
\end{enumerate}
\end{opghand}
\end{document}
