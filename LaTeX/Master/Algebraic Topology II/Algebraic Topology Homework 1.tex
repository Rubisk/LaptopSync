\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}
\usepackage[normalem]{ulem}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\Ext}{Ext}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Topology Homework 1}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[4]
Consider the inclusion of pairs $\iota: (U, U \cap V) \to (X, V)$. Using the naturality of the long exact sequence of pairs for this map, we obtain a commutative diagram as follows:
\[
\begin{tikzcd}
\dots \rar & H^n(X, V) \dar{\iota^*} \rar{\pi_V} & H^n(X) \dar{i_U^*} \rar{i_V^*} & H^n(V)\dar{j_V^*}  \rar{\delta_{(X, V)}} &  H^{n + 1}(X, V) \dar{\iota^*}   \rar{\pi_V} & \dots \\
\dots \rar & H^n(U, U \cap V) \rar{\pi_U} & H^n(U) \rar{j_U^*} & H^n(U \cap V) \rar{\delta_{(U, U \cap V)}} &  H^{n + 1}(U, U \cap V)   \rar{\pi_U} & \dots
\end{tikzcd}
\]
Note that the set $W = X \setminus U \subseteq X$ is closed and contained in $V$. Since $V$ is open we have $\overline{W} \subseteq \text{int}(V)$. By the excision theorem, the inclusion of pairs $(X \setminus W, V \setminus W) \to (X, V)$ induces an isomorphism on the level of cohomology. Since $X \setminus W = U$ and $V \setminus W = U \cap V$ we conclude that the inclusion $(U, U \cap V) \to (X, V)$ induces an isomorphism on the level of cohomology. In other words, the maps $\iota^*$ are isomorphisms. This allows us to construct the connecting homomorphism of the Mayer-Vietoris sequence as  $\delta_{MV} := \pi_V \circ (\iota^*)^{-1} \circ \delta_{(U, U \cap V)}: H^n(U \cap V) \to H^{n + 1}(X)$.

Remains to show the obtained sequence
\[
\begin{tikzcd}
\dots \rar & H^n(X) \rar{(i_U^*, i_V^*)} & H^n(U) \oplus H^n(V) \rar{j_U^* - j_V^*} & H^n(U \cap V)  \rar{\delta_{MV}} &  H^{n + 1}(X)   \rar & \dots
\end{tikzcd}
\]
is exact. 

We first show exactness in $H^n(X)$. Note that clearly 
\[
i_U^* \circ \delta_{MV} = i_U^* \circ \pi_V \circ (\iota^*)^{-1} \circ \delta_{(U, U \cap V)} = \pi_U \circ \delta_{(U, U \cap V)} = 0
\]
by exactness of the long exact sequence of the pair $(U, U \cap V)$. Similarly 
\[
i_V^* \circ \delta_{MV} = i_V^* \circ \pi_V \circ (\iota^*)^{-1} \circ \delta_{(U, U \cap V)} = 0 \circ (\iota^*)^{-1} \circ \delta_{(U, U \cap V)} = 0
\]
since by exactness of the long exact sequence of the pair $(X, V)$ one has $i_V^* \circ \pi_V = 0$. Conversely, suppose $i_U^*(\sigma) = i_V^*(\sigma) = 0$ for some $\sigma \in H^n(X)$. Then $\sigma \in \im(\pi_V)$, thus since $\iota^*$ is an isomorphism there exists $\tau \in H^n(U, U \cap V)$ such that $\pi_V \circ (\iota^*)^{-1}(\tau) = 0$. By commutativity of the diagram it follows that $\pi_U(\tau) = 0$, which yields $\rho \in H^{n - 1}(U, U \cap V)$ such that $\delta_{(U, U \cap V)}(\rho) = \tau$. By constructing $\delta_{MV}(\rho) = \sigma$ hence $\sigma \in \im(\delta_{MV})$. This shows exactness in $H^n(X)$.

Next we show exactness in $H^n(U) \oplus H^n(V)$. First note that by commutativity of the first diagram we immediately have $j_U^* \circ i_U^* = j_V^* \circ i_V^*$, hence $(j_U^* - j_V^*) \circ (i_U^*, i_V^*) = 0$. Conversely, suppose $(\sigma_0, \sigma_1) \in H^n(U) \oplus H^n(V)$ is such that $j_U^*(\sigma_0) = j_V^* (\sigma_1)$. Then $\delta_{(U, U \cap V)} \circ j_U^*(\sigma_0) = 0$, hence $\delta_{(U, U \cap V)}(j_V^*(\sigma_1)) = 0$. Therefore, using commutativity and that $\iota^*$ is an isomorphism we conclude that $\sigma_1 \in \im(\iota_V^*)$. Hence there is $\tau \in H^n(X)$ such that $\iota_V^*(\tau) = \sigma_1$. Hence $j_U^* \circ i_U^*(\tau) = j_V^*(\sigma_1) = j_U^*(\sigma_0)$. Therefore $i_U^*(\tau) - \sigma_0 \in \ker(j_U^*) = \im(\pi_U)$. Since $\iota^*$ is an isomorphism we find $\rho \in H^n(X, V)$ satisfying $\pi_U \circ \iota^*(\rho) = i_U^*(\tau) - \sigma_0$. Therefore $\tau' = \tau - \pi_V(\rho)$ satisfies $i_U^*(\tau') = i_U^*(\tau)  - ( i_U^*(\tau) - \sigma_0) = \sigma_0$ and $i_V^*(\tau') = i_V^*(\tau) - i_V^*(\pi_V(\rho)) = \sigma_1 - 0 = \sigma_1$. Therefore $\tau'$ maps to $(\sigma_0, \sigma_1)$ under $(i_U^*, i_V^*)$. This shows exactness in $H^n(U) \oplus H^n(V)$.

Finally we show exactness in $H^n(U \cap V)$. First note that 
\begin{align*}
\delta_{MV} \circ j_U^*&= \pi_V \circ (\iota^*)^{-1} \circ \delta_{(U, U \cap V)} \circ  j_U^* = \pi_V \circ (\iota^*)^{-1} \circ 0 = 0, \\
\delta_{MV} \circ j_V^*&= \pi_V \circ (\iota^*)^{-1} \circ \delta_{(U, U \cap V)} \circ  j_V^* = \pi_V \circ \delta_{(X, V)} = 0.
\end{align*}
Conversely, suppose $\sigma \in H^n(U, U \cap V)$ is such that $\delta_{MV}(\sigma) = 0$. Then by definition $\pi_V \circ (\iota^*)^{-1} \circ \delta_{(U, U \cap V)}(\sigma) = 0$. Denote $\tau =(\iota^*)^{-1} \circ \delta_{(U, U \cap V)}(\sigma) $, then $\tau \in \ker(\pi_V) = \im(\delta_{(X, V)})$. Hence there is $\rho_0 \in H^n(V)$ such that $\delta_{(X, V)} (\rho_0) = \tau$. 
It follows that 
\[
\delta_{(U, U \cap V)}(\sigma - j_V^*(\rho_0)) = \delta_{(U, U \cap V)}(\sigma) - \iota^*(\tau) =  \delta_{(U, U \cap V)}(\sigma) - \iota^* \circ (\iota^*)^{-1} \circ\delta_{(U, U \cap V)}(\sigma) = 0.
\]
Hence $\sigma - j_V^*(\rho_0) \in \im(j_U^*)$, and there is $\rho_1 \in H^n(U)$ such that $j_U^*(\rho_1) = \sigma - j_V^*(\rho_0)$. It follows that 
\[(j_U^* - j_V^*)(\rho_1, - \rho_0) = j_U^*(\rho_1) + j_V^*(\rho_0) = \sigma - j_V^*(\rho_0) + j_V^*(\rho_0) = \sigma,
\]
which shows exactness in $H^n(U \cap V)$.

Note that our proof also works for the case $n = 0$ since one can extend all long exact sequences with $H^n(-) = 0$ for $n < 0$, then the diagram chase will still hold.
\end{opghand}
\begin{opghand}[5] \ 
\begin{enumerate}[(a)]
\item To save space, denote $\FF_2 = \ZZ / 2\ZZ$. Note that as a set, $\FF_2[x] / (x^2 - 1) = \{\overline{0}, \overline{1}, \overline{x}, \overline{x + 1}\}$ (easily shown using division with remainder). Consider the map
\[
\varphi: A \to A: f \mapsto (x + 1)f.
\]
Clearly $\varphi$ is an $A$-module homomorphism satisfying $\varphi \circ \varphi = 0$ as $\overline{x + 1}^2 = 0$. One easily computes $\varphi(\overline{0})  = \overline{0}, \varphi(\overline{1}) = \overline{x + 1}, \varphi(\overline{x}) = \overline{x + 1}, \varphi(\overline{x + 1}) = \overline{0}$. It follows that $\im(\varphi) = \ker(\varphi) = \langle \overline{x + 1} \rangle.$ Furthermore $A / \ker(\varphi) = \FF_2[x] / (x^2 - 1, x + 1) = \FF_2[x] / (x + 1) \cong \FF_2$. Note that the isomorphism is indeed an isomorphism of $A$-modules, as $x$ does act as $1$ on $\FF_2[x] / (x + 1)$ since $x \cdot 1 \equiv x \equiv -1 \equiv 1$. This gives us the free resolution
\[
\begin{tikzcd}
\dots \rar & A \rar{\varphi} & A \rar{\varphi} & A \rar{\varphi} & A. 
\end{tikzcd}
\]
Now note that $\Hom_A(A, \FF_2) = \FF_2$, as any such map is uniquely determined by the image of $1$ (as it must be an $A$-module homomorphism). The action of $\varphi$ is given by $f \mapsto f \circ \varphi$, which is the same as sending $f \mapsto 2f$ since $x$ acts as $1$ on $\FF_2$. Therefore applying the Hom-functor yields the chain complex
\[
\begin{tikzcd}
\dots \rar & \FF_2 \rar{0} & \FF_2 \rar{0} & \FF_2 \rar{0} & \FF_2. 
\end{tikzcd}
\]
The $n$-th homology of this complex is obviously $\FF_2$ for all $n$, hence $\Ext^n_A(\FF_2, \FF_2)  = \FF_2$ for all $n \geq 0$.
\item Consider the maps 
\begin{align*}
\varphi: A \to A: f \mapsto (x + 1)f, \\
\psi:  A \to A: f \mapsto (x - 1)f.
\end{align*}
Note that $\varphi \circ \psi(f)  = \psi \circ \varphi(f) = (x^2 - 1)f = 0$. Furthermore $\im(\psi) = \langle x - 1 \rangle$ hence $A / \im(\psi) = R[x] / (x^2 - 1) / (\overline{x - 1}) = R[x] / (x^2 - 1, x - 1) = R[x]  /(x - 1)  \cong R$ (note that indeed $x$ acts as $1$, hence this is an isomorphism of $A$-modules).

Now suppose $f \in \ker(\psi)$. Then we can write $f = (x + 1)g + r$ for some $r \in R$ and $g \in R[x]$. Then $\psi(f) = r(x - 1)$, hence $r(x - 1) = 0$. Thus there exist $h \in R[x]$ such that $(x^2 - 1)h = r(x - 1)$. If $r \neq 0$ then $h \neq 0$, but then the degrees don't match up. Therefore we may conclude $r = 0$, and $\ker(\psi) = \langle x - 1 \rangle = \im(\varphi)$. Similarly $\ker(\varphi) = \langle x + 1 \rangle = \im(\psi)$. We thus get a free resolution of $R$ that looks like
\[
\begin{tikzcd}
\dots \rar& A \rar{\varphi} & A \rar{\psi} & A \rar{\varphi} & A \rar{\psi}& A.
\end{tikzcd}
\]
Again, clearly $\Hom_A(A, R) = R$ since any map is uniquely determined by the image of $1$  and we can send $1$ wherever we like. Furthermore, since $x$ acts as $1$ we see that $(x - 1)$ will act as $0$ on $R$, while $x + 1$ acts as $2$ on $R$. Therefore by $A$-linearity of elements in $\Hom_A(A, R)$ we conclude that $\Hom(\varphi)(f) = 2f$ and $\Hom(\psi)(f) = 0$. Thus we get a chain complexthat looks like
\[
\begin{tikzcd}
\dots &\lar{0} R &\lar{2}  R &\lar{0}  R& \lar{2}  R & \lar{0} R.
\end{tikzcd}
\]
Therefore $\Ext^0_A(R, R) \cong R / 0 \cong R$, $\Ext^n_A(R, R) \cong R / 2R$ for $n$ even, and finally we have $\Ext^n_A(R, R) \cong \ker(R \to R: r \mapsto 2r) \cong \text{tor}_2R$ for $n > 0$ odd.
\end{enumerate}
\end{opghand}
\end{document}
