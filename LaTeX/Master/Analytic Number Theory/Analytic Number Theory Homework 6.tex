\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\rre}{Re}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\ord}{ord}
\DeclareMathOperator{\Li}{Li}
\DeclareMathOperator{\sqf}{sqf}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Analytic Number Theory Homework 6}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[4]
Let 
\[
M_2(x) = \#\left\{ n \leq x \ \bigg | \begin{array}{l} \text{$n$ has exactly 2 (not necessarily distinct)} \\ \text{prime factors, counted with multiplicity} \end{array}\right\}.
\]
Note that we can make any such $n = p_1p_2$ by first picking a prime $p_1 \leq \sqrt{x}$, and then another prime $p_2 \leq x / p_1$. We count each $n$ composed of two prime factors less than $\sqrt{x}$ twice this way, so we counted less than $\pi(\sqrt{x})^2$ numbers double. Therefore
\[
M_2(x) = \sum_{p \leq \sqrt{x}} \pi(x / p) + O(\pi(\sqrt{x})^2). 
\]
By the Prime Number Theorem, there is a function $r$ such that
\[
\pi(x) = \frac{x}{\log(x)}(1 + r(x)), \qquad \lim_{x \to \infty} r(x) = 0. 
\]
Note that for $p \leq \sqrt{x}$ one has $x / p \geq \sqrt{x}$, hence
\[
\lim_{x \to \infty} \sup_{p \leq x}  r(x / p) \leq \limsup\limits_{x \to \infty} r(\sqrt{x}) = 0.
\]
Therefore, as $x \to \infty$ (which we refrain from saying each time from now on)
\begin{align*}
\sum_{p \leq \sqrt{x}} \pi(x / p)  &= \frac{x}{\log(x)} \sum_{p \leq \sqrt{x}} \(\frac{1}{p} \frac{\log(x)}{\log(x / p)}\)(1 + r(x / p)) \\
&\leq \frac{x}{\log(x)} (1 + \sup_{t > \sqrt{x}} r(t))\sum_{p \leq \sqrt{x}} \frac{1}{p} \frac{\log(x)}{\log(x / p)}\\
&= \frac{x}{\log(x)} (1 + o(1))\sum_{p \leq \sqrt{x}}\frac{1}{p} \frac{\log(x)}{\log(x / p)}.
\end{align*}
By a symmetrical argument the other inequality holds, so we may conclude
\[
\sum_{p \leq \sqrt{x}} \pi(x / p)  = \frac{x}{\log(x)} (1 + o(1))\sum_{p \leq \sqrt{x}} \frac{1}{p} \frac{\log(x)}{\log(x / p)}.
\]
Remains to compute
\[
\sum_{p \leq \sqrt{x}} \frac{1}{p} \frac{\log(x)}{\log(x / p)} = \sum_{p \leq \sqrt{x}} \frac{1}{p} + \sum_{p \leq \sqrt{x}} \frac{\log(p)}{p}\frac{1}{\log(x) - \log(p)}.
\]
Remember that we have shown in a previous homework exercise (6.3) that
\[
\sum_{p \leq \sqrt{x}} \frac{1}{p}  \sim \log \log (x), \qquad \sum_{p \leq \sqrt{x}} \frac{\log(p)}{p} = O(\log(x)).
\]
It follows that
\[
\sum_{p \leq \sqrt{x}} \frac{1}{p} \sim \log \log(\sqrt x) = \log \(\log(x)\) - \log(2) \sim \log \log (x),
\]
and furthermore
\[
\frac{1}{\log(x)} \sum_{p \leq \sqrt{x}} \frac{\log(p)}{p} \leq \sum_{p \leq \sqrt{x}} \frac{\log(p)}{p}\frac{1}{\log(x) - \log(p)} \leq \frac{2}{\log(x)} \sum_{p \leq \sqrt{x}} \frac{\log(p)}{p}.
\]
Thus we may conclude
\[
\sum_{p \leq \sqrt{x}} \frac{\log(p)}{p}\frac{1}{\log(x) - \log(p)} = O(1),
\]
so that finally
\[
\sum_{p \leq \sqrt{x}} \frac{1}{p} \frac{\log(x)}{\log(x / p)} \sim \log \log(x),
\]
which gives
\[
\sum_{p \leq \sqrt{x}} \pi(x / p)  = \frac{x}{\log(x)} (1 + o(1))\log \log(x).
\]
Since
\[
\pi(\sqrt{x})^2 = \((1 + o(1)) \frac{\sqrt{x}}{\log(\sqrt x)}\)^2 = O\(\frac{x}{\log(x)^2}\) = o\(\frac{x \log \log(x)}{\log(x)}\),
\]
we may also conclude that
\[
M_2(x) = \frac{x \log \log(x)}{\log(x)} (1 + o(1)).
\]
Now note that $M_2(x)$ is almost $N_2(x)$, except that we also counted all the squares of primes less than $x$. Thus
\[
M_2(x) - N_2(x) = \sum_{p \leq \sqrt{x}} 1 = \pi(\sqrt{x}) = O\(\frac{\sqrt{x}}{\log(\sqrt{x})}\) = o\(\frac{x \log \log(x)}{\log(x)}\).
\]
Therefore we may finally conclude that
\[
N_2(x) = \frac{x}{\log(x)} (1 + o(1))\log \log(x),
\]
which is what we needed to show.
\end{opghand}
\end{document}
