\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\rre}{Re}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\ord}{ord}
\DeclareMathOperator{\Li}{Li}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Analytic Number Theory Homework 4}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[2] \
If $n$ is squarefree, then the only square dividing $n$ is $1$, hence $\sum_{d^2 \mid n} \mu(d) = \mu(1) = 1$. Otherwise, there exists some prime $p$, integer $m \in \NN$ and integer $k \geq 2$ such that $n = p^km$ with $p \nmid m$. Then any $d$ such that $d^2 \mid m$ is of the form $p^{2\ell}d'$ for some $\ell \in \ZZ_{\geq 0}$ and $d'\in \NN$, where $(d')^2 \mid m$. Since $\mu$ is multiplicative, we conclude that
\begin{align*}
\sum_{d^2 \mid n} \mu(d) = \sum_{0 \leq \ell \leq  \lfloor \frac{k}{2} \rfloor} \sum_{(d')^2 \mid m}\mu(p^\ell) \mu(d') &= \(\sum_{0 \leq \ell \leq  \lfloor \frac{k}{2} \rfloor} \mu(p^\ell) \)\(\sum_{(d')^2 \mid m}\mu(d') \) \\
&= (1 - 1) \(\sum_{(d')^2 \mid m}\mu(d') \) = 0.
\end{align*}
We claim that similarly 
\[
\sum_{d^m \mid n} \mu(d)= \begin{cases}
1 &\text{if $n$ is $m$-free}, \\
0& \text{otherwise.}
\end{cases}
\]
The case of $n$ being $m$-free is easy again, since the only $m$-th power dividing $n$ is $1$. Otherwise, there exists some prime $p$, integer $t \in \NN$ and integer $k \geq m$ such that $n = p^kt$ with $p \nmid t$. Again any $d$ such that $d \mid m$ is of the form $p^{m\ell} d'$ where $\ell \in \ZZ_{\geq 0}$ and $d' \in \NN$ such that $(d')^m \mid t$. Therefore again one has
\[
\sum_{d^2 \mid n} \mu(d) = \sum_{0 \leq \ell \leq  \lfloor \frac{k}{m} \rfloor} \sum_{(d')^m \mid t}\mu(p^\ell) \mu(d') = (1 - 1) \(\sum_{(d')^m \mid t}\mu(d') \) = 0,
\]
which proofs the claim.

Thus if $F(x)$ denotes the number of $m$-free number up to $x$, one clearly has
\begin{align*}
F(x)= \sum_{n \leq x} \sum_{d^m \mid n} \mu(d) &= \sum_{d \leq x^{1 / m}} \left \lfloor \frac{x}{d^m} \right \rfloor \\
&= \sum_{d \leq x^{1 / m}} \mu(d)\frac{x}{d^m} - \sum_{d \leq x^{1 / m}}\mu(d) \left\{\frac{x}{d^m} \right\} \\
&= \sum_{d \leq x^{1 / m}}\mu(d) \frac{x}{d^m} + O(x^{1 / m}) \\
&= \frac{x}{\zeta(m)} - x \sum_{d > x^{1 / m}} \mu(d)\frac{1}{d^m} + O(x^{1 / m}),
\end{align*}
note that since $m \geq 2$ the completion of the sum converges absolutely (and we know its value from the first half of the course). 

By the integral test one has (for $x$ sufficiently large for the integral to make sense)
\[
\sum_{d > x^{1 / m}} \frac{1}{d^m} \leq \int_{x^{1/m} - 1}^\infty \frac{1}{t^m} dt = \frac{1}{m + 1}(x^{1 / m} - 1)^{1 - m} = O(x^{1/m - 1}).
\]
Therefore 
\[
\left |x \sum_{d > x^{1 / m}} \mu(d)\frac{1}{d^m}\right | \leq x \sum_{d > x^{1 / m}} \frac{1}{d^m} = O(x^{1/m}),
\]
so that finally
\[
F(x) = \frac{x}{\zeta(m)} + O(x^{1 / m}),
\]
gives an asymptotic formula.
\end{opghand}
\begin{opghand}[3] \
We first show that for any $D \in \NN$ and $0 \leq z \leq x$ one has
\[
\pi(x + z) -  \pi(x) \leq \frac{\varphi(D)}{D}z + D.
\]
If $D \geq z$ it obviously holds since $\pi(x + z) - \pi(x) \leq z$. Otherwise one has $D < z \leq x$, thus any integer in $[x, x + z]$ that is not coprime to $D$ can't be prime. Let $n = \lceil x \rceil$, note that $n + \left \lfloor \frac{z}{D} \right \rfloor D  - 1 \leq x + z$. And the set $\{n, n + 1, \dots, n + \left \lfloor \frac{z}{D} \right \rfloor D  - 1\}$ runs through exactly $\left \lfloor \frac{z}{D} \right \rfloor$ complete residue systems modulo $D$. Hence there are at most $\varphi(D) \cdot \left \lfloor \frac{z}{D} \right \rfloor$ integers coprime to $D$ in this set.
Therefore
\begin{align*}
\pi(x + z) - \pi(x) &= \#\left \{p \text{ prime} \mid n \leq p \leq n + \left \lfloor \frac{z}{D} \right \rfloor D  - 1\right \} + \#\left \{p \text{ prime}  \mid \left \lfloor \frac{z}{D} \right \rfloor D \leq p \leq z\right \} \\
&\leq \varphi(D) \cdot \left \lfloor \frac{z}{D} \right \rfloor + D \leq \frac{\varphi(D)}{D}z + D.
\end{align*}
Now set
\[
D = \prod_{p \leq \sqrt{\log(z)}} p.
\]
Then by Mertens Theorem (from the lecture) one has as $z \to \infty$ that
\[
\frac{\varphi(D)}{D} = \prod_{p \leq \sqrt{\log(z)}} \(1 - \frac{1}{p}\) \sim \frac{2e^{-\gamma}}{\log \log z}.
\]
This implies that as $z \to \infty$
\[
\frac{\varphi(D)}{D}z \sim z\frac{2e^{-\gamma}}{\log \log z} \implies \frac{\varphi(D)}{D}z = O\(\frac{z}{\log \log(z)}\).
\]
Furthermore, with a result from one of the first lectures one has for $z$ sufficiently large
\[
\prod_{p \leq \sqrt{\log(z)}} p \leq 4^{\sqrt{\log(z)}} \leq 4^{\log(z) / 2} = z^{\log(4) / 2},
\]
the second inequality holds since for $y$ large enough one has $\sqrt{y} \leq y / 2$. 

Since $\log(4) / 2 < 1$ and $\log \log(z) = O(z^\varepsilon)$ for any $\epsilon > 0$, one has $z^{\log(4) / 2} = O\(\frac{z}{\log \log (z)}\)$ as $z \to \infty$. Therefore
\[
\pi(x + z) -  \pi(x) \leq \frac{\varphi(D)}{D}z + D =  O\(\frac{z}{\log \log (z)}\)
\]
as $z \to \infty$, for any $x \geq z$.
\end{opghand}
\end{document}
