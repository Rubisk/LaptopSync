\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\rre}{Re}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\ord}{ord}
\DeclareMathOperator{\Li}{Li}
\DeclareMathOperator{\sqf}{sqf}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Analytic Number Theory Homework 7}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1]
Recall from the previous exercise sheet/lecture that
\[
|S(\alpha)|^2 \leq \frac{1}{\delta} \int_{\alpha - \delta / 2}^{\alpha + \delta / 2} |S(t)|^2dt + \frac{1}{2} \int_{\alpha - \delta / 2}^{\alpha + \delta / 2} |(S(t)^2)'|dt.
\]
Define the indicator function
\[
I(\alpha, t) = \begin{cases}
1 &\text{ if } ||\alpha - t|| < \frac{\delta }{2}, \\
0 &\text{ if } ||\alpha - t|| \geq \frac{\delta }{2}.
\end{cases}
\]
Then using the fact that $S$ is $1$-periodic we have for all $1 \leq r \leq R$ that
\[
|S(\alpha_r)|^2 \leq \int_0^1 I(\alpha_r, t) \(\frac{1}{\delta} |S(t)|^2 + \frac{1}{2}|(S(t)^2)'|dt\).
\]
It follows that
\[
\sum_{1 \leq r \leq R} N_\delta(\alpha_r)^{-1} |S(\alpha_r)|^2 \leq \int_0^1 \sum_{1 \leq r \leq R} N_\delta(\alpha_r)^{-1} I(\alpha_r, t) \(\frac{1}{\delta} |S(t)|^2 + \frac{1}{2}|(S(t)^2)'|dt\).
\]
We claim that
\[
\sum_{1 \leq r \leq R} N_\delta(\alpha_r)^{-1} I(\alpha_r, t)  \leq 1
\]
for all $t \in [0, 1]$. Thus fix some $t \in [0, 1]$. Let $\{i_1, \dots, i_k\}$ enumerate those $\alpha_i$ such that $I(\alpha_i, t) = 1$ (thus $I(\alpha_{i_j}, t) = 1$ for all $j \in \{1, \dots, k\}$, if $I(\alpha_i, t) = 1$ then there is a $j$ with $i = i_j$ and each $i_j$ is unique). Then for $j \in \{1, \dots, k\}$ fixed, we have $||\alpha_{i_j} -\alpha_{i_\ell}|| \leq ||\alpha_{i_j} - t || + ||\alpha_{i_\ell} - t|| < \delta $ for each $\ell \in \{1, \dots, k\}$. Therefore $N_\delta(\alpha_{i_j}) \geq k$. We conclude 
\[
\sum_{1 \leq r \leq R} N_\delta(\alpha_r)^{-1} I(\alpha_r, t)  = \sum_{j = 1}^k N_\delta(\alpha_{i_j})^{-1} \leq \sum_{j = 1}^k \frac{1}{k} \leq 1.
\]
It follows that
\[
\sum_{1 \leq r \leq R} N_\delta(\alpha_r)^{-1} |S(\alpha_r)|^2 \leq \int_0^1 \frac{1}{\delta} |S(t)|^2 + \frac{1}{2}|(S(t)^2)'|dt.
\]
The result now follows exactly as in the lecture, we repeat the main lines of the argument for completeness. Note
\[
\int_0^1 |S(t)|^2 = \int_0^1 \sum_{M < n, m \leq M + N} a_n \overline{a_m} \int_0^1 e(t(n - m)) dt = \sum_{M < n, m \leq M + N} |a_n|^2.
\]
Furthermore, by Cauchy-Schwarz one has
\[
\int_0^1 \frac{1}{2}|(S(t)^2)'|dt = \int_0^1 |S'(t)||S(t)|dt \leq \(\int_0^1 |S'(t)|^2 dt \)^\frac{1}{2} \(\int_0^1 |S(t)|^2 dt \)^\frac{1}{2}.
\]
Now note that by relabeling the $a_n$ and multiplying by something of norm $1$, we can assume $M = - \lceil \frac{N}{2} \rceil$. 
\[
\int_0^1 |S'(t)|^2 dt = \int_0^1 \sum_{M < m, n \leq M + N} 4 \pi^2 i n m a_n \overline{a_m} e(t(n - m)) dt = \sum_{M < n \leq M + N}  4 \pi^2 n^2 |a_n|^2.
\]
We can bound the terms uniformly using our bound on $M$, so
\[
\sum_{M < n \leq M + N} 4 \pi^2\(\frac{N}{2}\)^2 |a_n|^2 \leq \pi^2 N^2 \sum_{M < n \leq M + N} |a_n|^2.
\]
This implies finally
\[
\int_0^1 \frac{1}{2}|(S(t)^2)'|dt \leq \(\int_0^1 |S'(t)|^2 dt \)^\frac{1}{2} \(\int_0^1 |S(t)|^2 dt \)^\frac{1}{2} \leq \pi N \sum_{M < n \leq M + N} |a_n|^2.
\]
Combining everything we find
\[
\sum_{1 \leq r \leq R} N_\delta(\alpha_r)^{-1} |S(\alpha_r)|^2 \leq \int_0^1 \frac{1}{\delta} |S(t)|^2 + \frac{1}{2}|(S(t)^2)'|dt \leq \(\frac{1}{\delta} + \pi N\)\sum_{M < n \leq M + N} |a_n|^2,
\]
which is what we needed to show.
\end{opghand}
\end{document}
