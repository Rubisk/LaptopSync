\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\rre}{Re}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\ord}{ord}
\DeclareMathOperator{\Li}{Li}
\DeclareMathOperator{\sqf}{sqf}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Analytic Number Theory Homework 5}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[2] \
\begin{enumerate}[a)]
\item We first show by induction on the number of distinct prime factors that for all $n \in \ZZ_{\geq 2}$ and all possible choices of sequences $(y_i)_{i = 1}^\infty, (z_i)_{i = 1}^\infty$ with $\mathcal{D}^+ = \mathcal{D}^+((y_i)_{i = 1}^\infty), \mathcal{D}^- = \mathcal{D}^-((z_i)_{i = 1}^\infty) $ that
\[
\sum_{\substack{d \mid n \\ d \in \mathcal{D}^+}} \mu(d) \geq 0, \qquad \sum_{\substack{d \mid n \\ d \in \mathcal{D}^-}} \mu(d) \leq 0.
\]
If $n$ is a prime power $p^k$, then the first sum is either $1$ or $0$ (depending on whether or not $p < y_1$), but we always have $p \in D^{-}$ so the right sum is $0$. This completes the base case of our induction. 

Now suppose $\omega(n) > 1$, and $p$ is the largest prime factor of $n$. Write $n = p^\alpha m$ with $p \nmid m$ and $m \in \NN$. Pick any two sequences $(y_i)_{i = 1}^\infty, (z_i)_{i = 1}^\infty$, and denote $\mathcal{D}^+ = \mathcal{D}^+((y_i)_{i = 1}^\infty)$ and $\mathcal{D}^- = \mathcal{D}^-((z_i)_{i = 1}^\infty)$. 
We claim there is a bijection
\[
\{d \in D^-((z_i)_{i = 1}^\infty) \mid  p \mid d, d \mid n\} \leftrightarrow \{d \in D^+((z_{i + 1})_{i = 1}^\infty) \mid d \mid m\}
\]
Note that for any squarefree $d \in \mathcal{D}^-((z_i)_{i = 1}^\infty)$ one can write $d = pp_1 \cdots p_\ell$, with $p > p_1 > \cdots > p_\ell$ primes and $p_i < z_{i + 1}$ for all odd $i$. Thus $d = pd'$ for some $d' \in \mathcal{D}^+((z_{i + 1})_{i = 1}^\infty)$, and clearly $d' \mid m$. Conversely, given $d' \in  D^+((z_{i + 1})_{i = 1}^\infty$ with $d' \mid m$, we have for $d= pd' $ that $d \mid n$ as $p \nmid d'$ since $p \nmid m$, and as there is no restriction on the largest prime factor also $d \in \mathcal{D}^-((z_i)_{i = 1}^\infty)$. Since the maps $pd' \mapsto d'$ and $d'\mapsto pd'$ are clearly inverses, this gives a bijection which proofs the claim.

Since our induction was on all sequences, one has
\[
\sum_{\substack{d \mid n \\ d \in \mathcal{D}^-}} \mu(d) = \sum_{\substack{d \mid m \\ d \in \mathcal{D}^-}} \mu(d) + \mu(p)\sum_{\substack{d' \mid m \\ d' \in \mathcal{D}^+((z_{i + 1})_{i = 1}^\infty)}} \mu(d') \leq 0 . 
\]
since by induction the left sum is negative, the right is positive and $\mu(p)  = -1$. In a similar fashion, if $p \geq y_1$ then
\[
\sum_{\substack{d \mid n \\ d \in \mathcal{D}^+}} \mu(d) = \sum_{\substack{d \mid m \\ d \in \mathcal{D}^+}} \mu(d) \geq 0
\]
by induction. And if $p < y_1$, then there is again a bijection 
\[
\{d \in D^+(y_i) \mid  p \mid d, d \mid n\} \leftrightarrow \{d \in D^-((y_{i + 1})_{i = 1}^\infty) \mid d \mid m\}
\]
sending $pd' \mapsto d'$ and $d' \mapsto pd'$ (the argument is exactly the same as before, except that one uses $p < y_1$ to show that $pd' \in \mathcal{D^+}(y_i)$). Thus by the same argument as before (with the signs flipped)
\[
\sum_{\substack{d \mid n \\ d \in \mathcal{D}^+}} \mu(d) = \sum_{\substack{d \mid m \\ d \in \mathcal{D}^+}} \mu(d) + \mu(p)\sum_{\substack{d' \mid m \\ d' \in \mathcal{D}^-((z_{i + 1})_{i = 1}^\infty)}} \mu(d') \geq 0.
\]
This concludes our induction. 

We thus have
\[
\sum_{\substack{d \mid n \\d \in \mathcal{D}^+}}\mu(d) \geq \sum_{d \mid n}\mu(d) \geq \sum_{\substack{d \mid n \\d \in \mathcal{D}^-}}\mu(d),
\]
since if $n = 1$ all terms are $1$, and otherwise the middle sum is $0$. Now write
\begin{align*}
S(\mathcal{A}, z) &= \sum_{\substack{n \leq x \\ \gcd(n, P(z)) = 1}} a_n = \sum_{n \leq x} \sum_{d \mid \gcd(n, P(z))} \mu(d) a_n.
\end{align*}
Then since $a_n \geq 0$ one has
\begin{align*}
S(\mathcal{A}, z) &\leq \sum_{n \leq x} \sum_{\substack{d \mid \gcd(n, P(z)) \\ d \in \mathcal{D}^+}} \mu(d) a_n \\
&= \sum_{\substack{d \mid P(z) \\ d \in \mathcal{D}^+}} \mu(d) \sum_{\substack{n \equiv 0 (\text{mod }d) \\ n \leq x}} a_n = \sum_{\substack{d \mid P(z) \\d \in \mathcal{D}^+}} \mu(d)A_d(x). 
\end{align*}
Similarly 
\begin{align*}
S(\mathcal{A}, z) &\geq \sum_{\substack{d \mid P(z) \\ d \in \mathcal{D}^-}} \mu(d) \sum_{\substack{n \equiv 0 (\text{mod }d) \\ n \leq x}} a_n = \sum_{\substack{d \mid P(z) \\d \in \mathcal{D}^-}} \mu(d)A_d(x),
\end{align*}
as desired.
\item If we choose $y_1 = \dots = y_{r - 1} = P(z)$ and $y_i = 1$ for $i \geq r$, then if $r$ is odd we have for any $d \leq P(z)$ that $d \in \mathcal{D}^+$ if and only if $\omega(d) < r$ (any prime factor of $d$ is at most $d \leq P(z)$, but at least $2$). Since any divisor of $P(z)$ is necessarily smaller or equal to $P(z)$, we thus have
\[
S(\mathcal{A}, z) \leq \sum_{\substack{d \mid P(z) \\d \in \mathcal{D}^+}} \mu(d)A_d(x) = \sum_{\substack{d \mid P(z) \\\omega(d) < r}} \mu(d)A_d(x). 
\]
Similarly if $r$ is even we have for any $d \leq P(z)$ that $d \in \mathcal{D}^-$ if and only if $\omega(d) < r$. Thus again
\[
S(\mathcal{A}, z) \geq \sum_{\substack{d \mid P(z) \\d \in \mathcal{D}^-}} \mu(d)A_d(x) = \sum_{\substack{d \mid P(z) \\\omega(d) < r}} \mu(d)A_d(x),
\]
as required.
\end{enumerate}
\end{opghand}
\end{document}
