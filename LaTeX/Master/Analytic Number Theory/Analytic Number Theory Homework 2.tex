\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\rre}{Re}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\ord}{ord}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Analytic Number Theory Homework 2}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[2.7] \
\begin{enumerate}[a)]
\item For any prime $p$ one has $1 < p^\varepsilon$. Note that for $x > 1$ one has $\frac{x}{\log(x)} \geq e$, since $\frac{x}{\log(x)}$ tends to $\infty$ as $x \to 1, x \to \infty$, and the derivative $\frac{\log(x) - 1}{\log(x)^2}$ is $0$ only if $x = e$ (on $(1, \infty)$ anyway). Thus it attains a minimum at $x = e$, where it is equal to $e$. Thus one has 
\[
e \leq  \frac{p^{\varepsilon(k + 1)}}{\log(p^{\varepsilon(k + 1)})} \implies e \varepsilon(k + 1)\log(p) \leq p^{\varepsilon(k + 1)}.
\]
Since $2^k \leq k + 1$ for any integer $k$, one thus has
\[
\frac{k + 1}{p^{\varepsilon k}} \leq \begin{cases} (e \varepsilon \log(p))^{-1} p^\varepsilon &\text{for every prime $p$,} \\ 1 & \text{ if } p^\varepsilon \geq 2. \end{cases}
\]
Now if $n = p_1^{k_1} \cdots p_t^{k_t}$ where the $p_i$ are different primes, then one thus has
\begin{align*}
\frac{\tau(n)}{n^\varepsilon} = \prod_{i = 1}^t \frac{k + 1}{p^{\varepsilon k}} \leq \prod_{p \text{ prime}, p^\varepsilon < 2} \frac{p^\varepsilon}{e\varepsilon\log(p)} \leq \prod_{p \text{ prime}, p^\varepsilon < 2} \frac{2}{e\varepsilon\log(p)}.
\end{align*}
For any prime $p > 2$ one has $e^2 \leq 3^e \leq p^e$, hence $\frac{2}{e\log(p)} \leq 1$. And for $p = 2$ one has $e\log(2) \geq 1$, thus
\[
\prod_{p \text{ prime}, p^\varepsilon < 2} \frac{2}{e\varepsilon\log(p)} \leq 2 \prod_{p \text{ prime}, p^\varepsilon < 2} \frac{1}{\varepsilon} \leq 2 (1 / \varepsilon)^{\pi(2^{1 / \varepsilon})},
\]
since there are $\pi(2^{1 / e})$ primes $p$ with $p^\varepsilon \leq 2$. We conclude
\[
\tau(n) \leq 2 (1 / \varepsilon)^{\pi(2^{1 / \varepsilon})} n^\varepsilon 
\]
as desired.
\item Define $f(n) = \log \log(n) - \log \log \log(n)$. Applying (a) with $\varepsilon = 1 / f(n)$ we find
\begin{align*}
\tau(n) &\leq 2(f(n))^{\pi(2^{f(n)})}\cdot n^\frac{1}{f(n)} \\
&= O\( f(n)^{O\(\frac{2^{f(n)}}{f(n)} \)} n^{\frac{1}{f(n)}}\) &\(\pi(2^{f(n)}) = O\(\frac{2^{f(n)}}{\log(2^{f(n)})}\) = O\(\frac{2^{f(n)}}{f(n)}\)\)
\end{align*}
Thus
\begin{align*}
\log \tau(n) &= O\(2^{f(n)} +\frac{\log(n)}{f(n)}\) = O\(\frac{\log(n)}{\log \log (n)} + \frac{\log(n)}{\log \log(n)(1 - o(1))}\) = O\(\frac{\log(n)}{\log \log n}\).
\end{align*}
as required.
\end{enumerate}
\end{opghand}
\begin{opghand}[3.3]
Suppose $f \mid q$. Consider the map
\begin{align*}
\{\text{primitive characters mod $f$ }\} &\to \{ \text{ characters mod ${q}$ with conductor } f\} \\
\chi &\mapsto \([a \pmod q] \mapsto \chi([a \pmod{d}])\).
\end{align*}
This is clearly injective (since the projection $\ZZ / q\ZZ \to \ZZ / d\ZZ$ is surjective, thus if $\chi_1(a) \neq \chi_2(a)$ then we can find a lift of $a$) and surjective by Theorem 3.9a). Since the conductor of any character mod $q$ divides $q$, we conclude that 
\[
\#\{\text{characters mod $q$}\} = \sum_{f \mid q} F(f). 
\]
We know that there are exactly $\varphi(q)$ different characters mod $q$, thus $\sum_{f \mid q} F(f) = \varphi(q)$. Therefore $F = \mu * \varphi$ and thus multiplicative. One has for any prime $p$ and $k \in \NN$ with $k \geq 2$
\begin{align*}
F(1) &= 1 \\
F(p) &= \varphi(p) - \varphi(1) = p - 2 \\
F(p^k) &= \sum_{d \mid p^k} \varphi(d)\mu(p^k / d) = \varphi(p^k) - \varphi(p^{k - 1}) = p^{k - 1}(p - 1) - p^{k - 2}(p - 1) = p^k - 2p^{k - 1} + p^{k - 2}.
\end{align*}
\begin{opghand}[4.2]
\end{opghand}
\begin{enumerate}[a)]
\item Let $f(z) = (qz + a)^{-2}$. Then $\frac{2\pi i f(z)}{e^{2 \pi i z} - 1}$ is a meromorphic functions with poles at the integers and $z = -a / q$. Thus
\[
\int_{S_N} \frac{2\pi i f(z)}{e^{2 \pi i z - 1}} dz = \sum_{m = -N}^N \Res_{z = m} \frac{2 \pi i f(z)}{e^{2 \pi i z } - 1} + \Res_{z = -a / q} \frac{2 \pi i f(z)}{e^{2 \pi i z} - 1}.
\]
Now $\frac{2 \pi i f(z)}{e^{2 \pi i z } - 1}$ has a simple pole in $z = m$ for any $m \in \ZZ$, hence by L'Hopital one has
\[
\Res_{z = m} \frac{2 \pi i f(z)}{e^{2 \pi i z } - 1} = \lim_{z \to m}(z - m)\frac{2 \pi i f(z)}{e^{2 \pi i z } - 1} = \lim_{z \to m}\frac{2 \pi i f(z)}{2 \pi i e^{2 \pi i z}} = f(m).
\]
Furthermore,
\[
\Res_{z = -a / q} \frac{2 \pi i f(z)}{e^{2 \pi i z} - 1} = \lim_{z \to -a / q} \frac{\partial}{\partial z}\frac{2 \pi i q^{-2}}{e^{2 \pi i z} - 1} = -\frac{4 \pi^2 q^{-2}e^{2 \pi i a / q}}{(e^{2 \pi i a / q} - 1)^2} = -\frac{4 \pi^2q^{-2}}{(e^{\pi i a / q} - e^{-\pi i a / q})^2} = -\frac{\pi^2 / q^2}{\sin(\pi a / q)^2}.
\]
Finally
\[
\left|\int_{S_N} \frac{2\pi i f(z)}{e^{2 \pi i z - 1}} dz\right| \leq 8(N + 1)(2 \pi \sup_{n \in S_N} f(n)) = O\(\frac{N + 1}{N^2}\) = O(1 / N).
\]
Thus the integral converges to $0$ as $N \to \infty$. Therefore the limit $\lim_{N \to \infty} \sum_{m = -N}^{m = N} f(m)$ exists, thus the sum converges and is equal to $\frac{\pi^2 / q^2}{\sin(\pi a / q)^2}$.
\item Note that (a) holds even if $a \in \{1, \dots, q - 1\}$. Thus if $q$ is odd one has
\begin{align*}
L(2, \chi) &= \sum_{n = 1}^\infty \frac{\chi(n)}{n^2} = \sum_{a = 1}^{q - 1} \sum_{n = 0}^\infty \frac{\chi(qn + a)}{(qn + a)^2} = \sum_{a = 1}^{\frac{q - 1}{2}} \sum_{n = 0}^\infty \chi(a)\(\frac{1}{(qn + a)^2} + \frac{1}{(q(n + 1) - a)^2}\) \\&= \sum_{a = 1}^{\frac{q - 1}{2}} \chi(a) \(\frac{\pi^2 / q^2}{\sin(\pi a / q)^2} + \frac{1}{a^2}\).
\end{align*}
Similarly if $q$ is even one has
\begin{align*}
L(2, \chi) &= \sum_{n = 1}^\infty \frac{\chi(n)}{n^2} = \sum_{a = 1}^{q - 1} \sum_{n = 0}^\infty \frac{\chi(qn + a)}{(qn + a)^2} \\
&= \sum_{a = 1}^{\frac{q - 2}{2}} \sum_{n = 0}^\infty \chi(a)\(\frac{1}{(qn + a)^2} + \frac{1}{(q(n + 1) - a)^2}\)  + \sum_{n = 0}^\infty \chi(q/ 2)\(\frac{1}{(qn + q/2)^2}\)
\\&= \sum_{a = 1}^{\frac{q - 2}{2}} \chi(a) \(\frac{\pi^2 / q^2}{\sin(\pi a / q)^2} + \frac{1}{a^2}\) + \frac{1}{2}\chi(q / 2) \frac{\pi^2 / q^2}{\sin(2\pi / q)^2}.
\end{align*}\end{enumerate}
\end{opghand}
\begin{opghand}[4.5] \
\begin{enumerate}[a)]
\item If $\rre(s) > 1$ we have 
\[
F_0(s) = \int_1^\infty x^{-s} dx = \frac{1}{1 - s} \left[ x^{1 - s}\right]_{x = 1}^{x = \infty} = \frac{1}{s - 1}.
\]
Using integration by parts we have
\begin{align*}
F_{n, k}(s) &= \int_n^{n + 1}(x - n)^k x^{-s - k}dx \\ 
&= \frac{1}{k + 1}\( \left[(x - n)^{k + 1}x^{-s - k} \right]_{x = n}^{x = n + 1} - \int_n^{n + 1}(x - n)^{k + 1}(-s - k)x^{-s - k - 1}\) \\
&= \frac{1}{k + 1}\((n + 1)^{-s - k} + (s + k)F_{n, k + 1}\).
\end{align*}
Thus
\begin{align*}
F_k(s) &= \sum_{n = 1}^\infty F_{n, k}(s) = \sum_{n = 1}^\infty \frac{1}{k + 1}\((n + 1)^{-s - k} + (s + k)F_{n, k + 1}(s)\) \\
&= \frac{1}{k + 1}\(\zeta(s + k) - 1 + (s + k)F_{k + 1}(s)\).
\end{align*}
\item Note that
\begin{align*}
G(s) &+ \sum_{k = 1}^r \frac{(s - 1) \cdots (s + k - 1)G(s + k)}{(k + 1)!} \\
&= G(s) + \sum_{k = 1}^r \frac{(s - 1) \cdots (s + k - 1)((k + 1)F_k(s) - (s + k)F_{k + 1}(s)}{(k + 1)!} \\
&= G(s) + \sum_{k = 1}^r \frac{(s - 1) \cdots (s + k - 1)F_k(s)}{k!} - \sum_{k = 2}^{r + 1} \frac{(s - 1) \cdots (s + k - 1)F_{k}(s)}{k!}\\
&= G(s) + (s - 1)F_1(s) - \frac{(s - 1) \cdots (s + r) F_{r + 1}(s)}{(r + 1)!} \\
&= G(s) + (s - 1)\(\frac{1}{s - 1} - \zeta(s) + 1\) - \frac{(s - 1) \cdots (s + r) F_{r + 1}(s)}{(r + 1)!} \\
&= 1 - \frac{(s - 1) \cdots (s + r) F_{r + 1}(s)}{(r + 1)!}.
\end{align*}
Rearranging gives the required identity.
\item If $r = 0$ then $U_r = \{s \in \CC \mid \rre(s) > 0\}$. We know that on $U_0$, $\zeta(s)$ has an analytic continuation except for a simple pole in $1$ (previous homework), thus $G(s) = (s - 1)(\zeta(s) - 1)$ is holomorphic on $U_0$. Now if $G_{r - 1}$ is holomorphic on $U_{r - 1}$, then from the identity in (b) we can see that the right hand side (with $G_{r - 1}$ instead of the $G$) defines a holomorphic function $G_r$ on $U_r$ as long as $F_{r + 1}$ is holomorphic on $U_r$ (since $G(s + k)$ is holomorphic on $U_{r -  k} \subseteq U_{r - 1}$), such that $G_r = G_{r - 1}$ on $U_{r - 1}$.

Thus let $f_{r + 1}(x, s) = (x - [x])^{r + 1}x^{-s - {r + 1}}$. This function is almost everywhere continuous hence measurable, and $s \mapsto f_{r + 1}(x, s)$ is clearly analytic if $\rre{s} > -r$ and $x > 1$. Finally if $K \subseteq U_r$ is compact then $\rre(s)$ has a minimum $q > -r$ on $U$. Thus $|f(x, s)| \leq M_K(t) := x^{q - r + 1}$ on $K$ and we have
\[
\int_1^\infty |x^{q - r + 1}| = \int_1^\infty x^{1 + r - q} < \infty
\]
since $q > -r$. 

By the theorem $F_{r + 1}$ is holomorphic, as required. Thus $G$ can be extended to an holomorphic function on all of $\CC$. By the homework of last week, $\zeta(s)$ has a pole with residue $1$ at $s = 1$, thus $G(1) = 1$. Finally one has
\[
G_{-1}(0) = 1 - \frac{-1}{2}G_0(1) - 0F_1(0) = 1 - \frac{-1}{2} = \frac{3}{2}.
\]
Thus $G(0) = \frac{3}{2}$. Therefore $-(\zeta(0) - 1) = \frac{3}{2}$ and $\zeta(0) = -\frac{1}{2}$.
\end{enumerate}
\end{opghand}
\end{document}
