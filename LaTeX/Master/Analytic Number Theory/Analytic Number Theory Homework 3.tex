\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\rre}{Re}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\ord}{ord}
\DeclareMathOperator{\Li}{Li}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Analytic Number Theory Homework 3}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1.3] \
\begin{enumerate}[a)]
\item Let $f(x) = \sqrt{2x}$. Then $2 \leq \sqrt{2x} \leq x$ for $x \geq 2$, hence
\begin{align*}
\int_2^x \frac{1}{\log(t)^A}dt &= \int_2^{f(x)} \frac{1}{\log(t)^A}dt + \int_{f(x)}^x \frac{1}{\log(t)^A}dt  \\
&\leq \frac{\sqrt{2x} - 2}{\log(2)^A} + \frac{x - \sqrt{2x}}{\log(\sqrt{2x})^A} \\
&\leq \frac{\sqrt{2x}}{\log(2)^A} + 2^A\frac{x}{\log(2x)^A}.
\end{align*}
Since $\log$ is increasing and $A > 0$, we have the crude bound $\log(2x)^{-A} \leq \log(x)^{-A}$, thus
\[
\int_2^x \frac{1}{\log(t)^A}dt \leq \frac{\sqrt{2}\sqrt{x}}{\log(2)^A} + 2^{A}\frac{x}{\log(x)^A}.
\]
For $x > 4$ one has $\sqrt{x} \geq 2$ thus $\sqrt{x}' = \frac{1}{2\sqrt{x}} \geq \frac{1}{x} = \log(x)'$. Since $2 < e$ we have $\log(2) < 1$ and $\log(4) < 2 = \sqrt{4}$. Hence for $x > 4$ we may conclude $\sqrt{x} \geq \log(x)$. 

In particular one has for $x > \max\{(2A)^2, 4\}$ that $x \geq \sqrt x \log(x) \geq 2A\log(x)$. Thus $\log(x^{2A}) \leq x$ for $x > \max\{(2A)^2, 4\}$. Since $x^{1 / 2A}$ is increasing in $x$ we find that for $x > \max\{(2A)^2, 4\}^{1 / 2A}$ we have $\log(x) \leq x^{1 / 2A}$, or equivalently $\sqrt{x} \geq \log(x)^A$. Thus for $x > \max\{(2A)^2, 4\}^{1 / 2A}$ we have
\[
\int_2^x \frac{1}{\log(t)^A}dt \leq \frac{\sqrt{2}x}{\sqrt{x}\log(2)^A} + 2^{A}\frac{x}{\log(x)^A} \leq \(\frac{\sqrt{2}}{\log(2)^A} + 2^{A}\)\(\frac{x}{\log(x)^A}\),
\]
which suffices.
\item 
We show by induction on $n$ that
\begin{equation}
\Li(x) = \sum_{i = 1}^{n - 1}(i - 1)!u^{-i}e^u\bigg|_{u = \log(2)}^{u = \log(x)} + \int_{\log(2)}^{\log(x)}(n - 1)!u^{-n}e^u du.
\end{equation}
For $n = 1$ note that by a change of variable we have
\[
\Li(x) = \int_2^x \frac{1}{\log(t)}dt = \int_{\log(2)}^{\log(x)}u^{-1}e^u du.
\]
Now suppose $n > 1$ and it holds for $n - 1$. Using integration by parts we see that
\[
\int_{\log(2)}^{\log(x)}(n - 2)!u^{-(n - 1)}e^u du = (n - 2)!u^{-(n - 1)}e^u\bigg|_{u = \log(2)}^{u = \log(x)} + \int_{\log(2)}^{\log(x)}(n - 1)!u^{-n}e^u du,
\]
from which (1) follows immediately.
Changing variables we thus have
\[
\Li(x) = \sum_{i = 1}^{n - 1}(i - 1)!\(\frac{x}{\log(x)^i} - \frac{2}{\log(2)^i}\) + \int_2^x(n - 1)!\frac{1}{\log(t)^n} dt.
\]
By (a) we have
\[
\int_2^x(n - 1)!\frac{1}{\log(t)^n} dt = O\(\frac{x}{\log (x)^n}\),
\]
since our constant may depend on $n$ hence we can drop the $(n - 1)!$. 
Setting 
\[
C_n = \sum_{i = 1}^{n - 1} \frac{2}{\log(2)^i},
\]
and recalling from (a) that for $n > \max\{n^2, 4\}^{1/n}$ one has $\log(x)^n \leq x$, thus $x / \log(x)^n \geq 1$ for $x$ suffiiciently large and $C_n =  O\(\frac{x}{\log (x)^n}\)$ since $C_n$ does not depend on $x$. Therefore
\[
\Li(x) = \sum_{i = 1}^{n - 1}(i - 1)!\frac{x}{\log(x)^i} +  O\(\frac{x}{\log (x)^n}\),
\]
as required.
\end{enumerate}
\end{opghand}
\begin{opghand}[5.4] \
\begin{enumerate}[a)]
\item If $\text{Re}(s) > 1$ then $L_f(s)$ and $\zeta(s)$ converges absolutely hence by Theorem 2.13 one has
\begin{align*}
\zeta(s)L_f(s) &= \prod_p \frac{1}{1 - p^{-s}} \cdot \prod_p \(\sum_{i = 0}^\infty f(p) p^{-si}\) \\
&= \prod_p \frac{1}{1 - p^{-s}} \(1 - p^{-s} + \sum_{i = 2}^\infty f(p) p^{-si}\) \\
&= \prod_p \(1 + \frac{\sum_{i = 2}^\infty f(p) p^{-si}}{1 - p^{-s}}\)
\end{align*}
where the combining of infinite products is allowed since $\lim_{n \to \infty} a_nb_n = \lim_{n \to \infty} a_n \cdot \lim_{n \to \infty} b_n$ for any converging sequences of complex numbers $(a_n), (b_n)$.

Clearly $1 \pm p^{-s} \neq 0$ if $\text{Re}(s) > 0$, since $|p^s| = 1$ implies that $\text{Re}(s) = 0$ for all primes $p$. Let $p_n$ be the $n$-th prime number, and define the functions
\[
f_n(s) := \sum_{i = 2}^\infty f(p_n) p_n^{-si}.
\]
By Corollary 0.46(i), $f_n(s)$ defines an analytic function on $U = \{s \in \CC \mid \text{Re}(s) >0\}$, since on any compact $K \subseteq U$ the function $\text{Re}(s)$ attains a minimum $\alpha > \frac{1}{2}$, and $|f(p_n) p_n^{-si}| \leq p_n^{-\alpha i}$. Now $|p_n^{-\alpha}| < 1$ and 
\[
\sum_{i = 2}^\infty p_n^{-\alpha i} = \frac{p_n^{-2\alpha}}{1 - p_n^{-\alpha}},
\]
thus the sum converges and $f_n(s)$ is analytic on $U$. Since once again $1 \pm p_n^{-s} \neq 0$ if $\text{Re}(s) > 0$, the functions $g_n(s) = \frac{f_n(s)}{1 - p_n^{-s}}$ are also analytic on $U$.

This also shows $|g_n(s)| \leq \frac{p_n^{-2\alpha}}{1 - p_n^{-\alpha}}$ on $K$ (pick $\alpha$ and $U$ as before). Since $p_n \geq 2$ one has $|1 - p_n^{-\alpha}| \leq |1 - 2^{-\alpha}|$ and 
\[
\sum_{n = 1}^\infty \left| \frac{p_n^{-2\alpha}}{1 - p_n^{-\alpha}} \right| \leq \sum_{n = 1}^\infty\left| \frac{p_n^{-2\alpha}}{1 - 2^{-\alpha}} \right| \leq \sum_{n = 1}^\infty\left| \frac{n^{-2\alpha}}{1 - 2^{-\alpha}} \right|,
\]
and the right hand side converges absolutely since $2\alpha > 1$, hence
\[
\sum_{n = 2}^\infty\left| n^{-2\alpha}\right| \leq \int_1^\infty x^{-2\alpha}dx < \infty.
\]
By Corollary 0.46(ii), the infinite product 
\[
\prod_{n = 1}^\infty (1 + g_n(s)) = \prod_p \(1 + \frac{\sum_{i = 2}^\infty f(p) p^{-si}}{1 - p^{-s}}\)
\]
defines an analytic function on $U$, which is what we needed to show.
\item Note that for $\text{Re}(s) > \frac{1}{2}$, the function $\zeta(s)L_f(s)$ is analytic. Since $\zeta(s)$ is nonzero if $\text{Re}(s) = 1$ by Theorem 4.5, by continuity there exists an open set $V$ containing $\{s \in \CC \mid \text{Re}(s) \geq 1\}$ with $\zeta(s) \neq 0$ on $V$. Hence $L_f(s) = \frac{\text{analytic}}{\zeta(s)}$ is an analytic function on $V \setminus \{1\}$. 

Let $g(n) = |f(n)|$. Then clearly $g(n) \geq 0$, and $\sum_{n \leq x} g(x) \leq x$. Furthermore $L_g(s)$ converges absolutely if $\text{Re}(s) > 1$ since
\[
\sum_{n = 1}^\infty |g(n)n^{-s}| \leq \sum_{n = 1}^\infty n^{-\text{Re}(s)} < \infty.
\]
Finally, note that from Exercise 5.2(b) we have
\[
L_g(s) = L_{|\mu|}(s) + \sum_{n = 2}^\infty g(n)n^{-2s} = \frac{\zeta(s)}{\zeta(2s)}+ \sum_{n = 2}^\infty g(n)n^{-2s}.
\]
Since the right hand side converges absolutely if $\text{Re}(s) > \frac{1}{2}$, and $\zeta(2s) \neq 0$ for $\text{Re}(s) > \frac{1}{2}$, we see that $L_g(s)$ has an analytic continuation to $\{s \in \CC \mid \text{Re}(s) > 0, s \neq 1\}$. Furthermore, 
\[
\lim_{s \to 1} L_g(s) = \lim_{s \to 1}(s - 1)\frac{\zeta(s)}{\zeta(2s)} + \sum_{n = 2}^\infty g(n)n^{-2} = \frac{1}{\zeta(2)} + \sum_{n = 2}^\infty g(n)n^{-2},
\]
so in particular the limit exists and is equal to some $\alpha$. Finally, 
\[
\lim_{s \to 1} L_f(s) = 0
\]
since $\zeta(s)$ has a pole at $1$. By exercise 5.3(b) with the roles of $f$ and $g$ reversed, we may conclude that 
\[
\lim_{x \to \infty} \frac{1}{x} \sum_{n \leq x} f(n) = \frac{0}{1} = 0,
\]
as required.
\item Let $f(n) = (-1)^{\omega(n)}$. Then clearly $f$ is multiplicative, $|f(n)| \leq 1$ for all $n$ and $f(p) = -1$ for any prime $p$. Thus by (b)
\[
\lim_{x \to \infty} \sum_{n \leq x} f(n) = 0.
\]
Note that $f(n) + 1 = 0$ if $\omega(n)$ is odd, and $\frac{f(n) + 1}{2} = 1$ if $\omega(n)$ is even. Thus
\[
\lim_{x \to \infty} \frac{A(x)}{x} = \lim_{x \to \infty} \frac{1}{x}\sum_{n \leq x} \frac{1 + f(n)}{2} = \lim_{x \to \infty}\frac{1}{x} \sum_{n \leq x} \frac{1}{2} = \frac{1}{2}.
\]
\end{enumerate}
\end{opghand}
\newpage
\begin{opghand}[6.3] \
\begin{enumerate}[a)]
\item Let $g(n) = \log(n)$ if $n$ is prime, and $g(n) = 0$ otherwise. By definition one has
\[
\theta(x) = \sum_{n \leq x} g(n). 
\]
Thus by partial summation one has
\[
\sum_{i \leq x} \frac{g(i)}{i} = \frac{\sum_{i \leq x} g(i)}{x} + \int_1^x \frac{\sum_{i \leq t} g(i)}{t^2} dt = \frac{\theta(x)}{x} + \int_1^x \frac{\theta(t)}{t^2}. 
\]
Note that $\Lambda(n) \geq 0$ for all $n$. We can crudely bound (recall that for $x > 4$ one has $\log(x) \leq \sqrt{x}$))
\begin{align*}
\psi(x) = \sum_{n \leq x} \Lambda(n) &= \sum_{p \leq x} \log(p) + \sum_{k \geq 2} \sum_{p^k \leq x} \log(p) \\
&\leq \pi(x) \log(x) + \sqrt{x} \log(x) \\
&\leq 2 \frac{x}{\log(x)}\log(x) + x = O(x) &\(\pi(x) \leq 2\frac{x}{\log(x)}\) .
\end{align*}
Thus $L_\Lambda$ satisfies condition (ii) of Theorem 5.3. We know from chapter 2 that $L_\Lambda$ converges for $\text{Re}(s) > 1$ and is equal to $-\frac{\zeta'(s)}{\zeta(s)}$. Since $\zeta(s)$ has an analytic continuation to $\{s \in \CC \mid s \neq 1\}$, by differentiating this continuation we find an analytic continuation for $\zeta'(s)$ to $\{s \in \CC \mid s \neq 1 \}$. Since $1 / \zeta(s) \neq 0$ if $\text{Re}(s) \geq 1$, by continuity there exists an open set $V$ containing $\{s \in \CC \mid \text{Re}(s) > 1\}$ such that $\zeta(s) \neq 0$. Therefore $L_\Lambda(s)$ has an analytic continuation to $V \setminus \{1\}$. By Lemma 0.34, $L_\Lambda$ has residue $1$ at $s = 1$. Thus $L_\Lambda$ satisfies (i)-(iv) of Theorem 5.3 with $\alpha = 1, \sigma = 1$. Therefore by Lemma 5.5,
\[
\int_1^\infty \frac{\psi(x) - x}{x^2} dx
\]
converges. 

Now note that
\[
\int_1^N \frac{\theta(x) - x}{x^2} dx = \int_1^N \frac{\psi(x) - x}{x^2} dx - \int_1^N \frac{\sum_{k \geq 2} \sum_{p^k \leq x} \log(p)}{x^2}dx.
\]
Since $\sum_{k \geq 2} \sum_{p^k \leq x} \log(p) \leq \sqrt{x} \log(x)$ we can bound the rightmost  integral by
\[
\int_1^N \left| \frac{\sum_{k \geq 2} \sum_{p^k \leq x} \log(p)}{x^2}\right|dx \leq \int_1^\infty \frac{\log(x)}{x \sqrt{x}}dx = \int_0^\infty \frac{u}{e^{u/2}}du = 4\Gamma(2) = 4.
\]
Letting $N \to \infty$ we conclude (by Dominated Convergence Theorem) that
\begin{equation}
\int_1^\infty \frac{\theta(x) - x}{x^2} dx
\end{equation}
converges to some constant $c_0$.
Therefore as $x \to \infty$ one has
\[
\sum_{p \leq x} \frac{\log(p)}{p} = \frac{\theta(x)}{x} + \int_1^x \frac{\theta(t)}{t^2} = \frac{\theta(x)}{x} + c_0 + \log(x) + o(1).
\]
By Lemma 5.6 and (2),
\[
\lim_{x \to \infty} \frac{\theta(x)}{x} = 1.
\]
We conclude that 
\[
\sum_{p \leq x} \frac{\log(p)}{p} = 1 + o(1) + c_0 + \log(x),
\]
which is precisely what we needed to show.
\item By partial summation one has
\begin{align*}
\sum_{p \leq x} \frac{1}{p} &= \frac{1}{x} + \int_2^x \(\sum_{p \leq t} \frac{\log(p)}{p} \)\frac{1}{t\log(t)^2}dt \\
&=\frac{1}{x} + \int_2^x \(\log(t) + c_1 + R(t) \)\frac{1}{t\log(t)^2}dt \\
&=\frac{1}{x} + \int_2^x \frac{1}{t \log(t)} dt + \int_2^x \frac{c_1 + R(t)}{t\log(t)^2}dt,
\end{align*}
where $R(t) = o(1)$ as $t \to \infty$. In particular $R(t)$ is bounded, hence there is $M > 0$ such that
\[
\int_2^\infty \left| \frac{c_1 + R(t)}{t\log(t)^2}\right| dt\leq \int_2^\infty \frac{M}{t \log(t)^2}dt = \int_2^{\infty}\frac{M}{t^2}dt < \infty.
\]
By the Dominated Convergence Theorem, the limit
\[
\lim_{x \to \infty}\int_2^x \frac{c_1 + R(t)}{t\log(t)^2}dt
\]
exists and is equal to some constant $c_2$. Thus as $x \to \infty$ one has
\[
\sum_{p \leq x} \frac{1}{p} = \frac{1}{x} + \int_2^{\log(x)} \frac{1}{t} dt +  c_2 + o(1) =\log \log(x)  +  c_2 + o(1),
\]
as required.
\end{enumerate}
\end{opghand}
\begin{opghand}[6.3] \
\begin{enumerate}[a)]
\item 
By Exercise 2.5 (first homework sheet) one has for $\text{Re}(s) > 1$
\[
L_\chi(s)^{-1} = \sum_{n = 1}^\infty \mu(n) \chi(n)n^{-s}.
\]
Note that $|\chi(n)| \leq 1$ for all $n, \chi$, and we know $\zeta(s)$ satisfies all conditions of Theorem 5.3 around $\sigma = 1$. Furthermore, we know from the theorems in chapter 4 that $L(s, \chi)$ does not vanish on $\{s \in \CC \mid \text{Re}(s) > 0, s \neq 1\}$ for all $\chi$. Therefore by continuity $L(s, \chi)$ does not vanish on an open set containg $V = \{s \in \CC \mid \text{Re}(s) > 0, s \neq 1\}$, and $L(s, \chi)^{-1}$ can be extended to an analytic function on $V$. From Theorem 4.7 it immediately follows that
\[
\lim_{s \to 1} (s - 1)L(s, \chi)^{-1} = 0
\]
for all $\chi \neq \chi_0^{(q)}$ in $G(q)$. From Theorem 4.3(iii) we also see that
\[
\lim_{s \to 1} (s - 1)L(s, \chi_0^{(q)})^{-1} = \prod_{p \mid q}(1 - p^{-1})^{-1} \lim_{s \to 1} (s - 1)\zeta(s)^{-1} = 0.
\]
By Exercise 5.3 (with $f = 1, g = \chi$) we conclude that for all characters $\chi$
\begin{equation}
\lim_{x \to \infty} \frac{1}{x} \sum_{n \leq x} \mu(n) \chi(n) = 0.
\end{equation}

Note that if $\gcd(a, q) = 1$ and $n \equiv a \pmod{q}$ we also have $\gcd(n, q) = 1$. Thus if $\gcd(an, q) = 1$, and by Theorem 3.8 one has
\[
\sum_{\chi \in G(q)} \overline{\chi(a)}\chi(n) = \varphi(q). 
\]
And if $n \not \equiv a \pmod{q}$, then either $\gcd(an, q) > 1$ (if $\gcd(n, q) > 1$) or $\gcd(an, q) = 1$ and $n \not \equiv a \pmod{q}$. Thus 
\[
\sum_{\chi \in G(q)} \overline{\chi(a)}\chi(n) = 0. 
\]
Therefore
\begin{align*}
\lim_{x \to \infty} \sum_{\substack{n \leq x \\ n \equiv a \text{mod}(q)}} \mu(n) &= \lim_{x \to \infty}  \frac{1}{\varphi(q)}\sum_{n \leq x} \mu(n)\sum_{\chi \in G(q)} \overline{\chi(a)}\chi(n) \\
&= \frac{1}{\varphi(q)}\sum_{\chi \in G(q)} \overline{\chi(a)}\(\lim_{x \to \infty}  \sum_{n \leq x} \mu(n)\chi(n)\) \\
&= \frac{1}{\varphi(q)}\sum_{\chi \in G(q)} \overline{\chi(a)} \(0\) = 0, 
\end{align*}
which is what we needed to show.
\item If $m = 1$ the statement holds since the sum is equal to the sum of (a), thus assume $m > 1$. By Theorem 3.8 one has
\[
\frac{1}{\varphi(m)}\sum_{\chi \in G(m)} \chi(n) \overline{\chi(1)} = \begin{cases} 1 & \gcd(n, m) = 1, \\ 0 &\gcd(n, m) > 1. \end{cases}
\]
Now since $\chi(1) = 1$ we thus have
\begin{align}
\lim_{x \to \infty}\frac{1}{x}\sum_{\substack{n \leq x \\ n \equiv a \text{ mod (q)} \\ \gcd(n, m) = 1}} \mu(n) &= \lim_{x \to \infty}\frac{1}{x} \sum_{\substack{n \leq x \\ n \equiv a \text{ mod (q)} }}\frac{1}{\varphi(m)}\sum_{\chi \in G(m)} \chi(n) \mu(n) \nonumber \\
&= \lim_{x \to \infty}\frac{1}{x} \sum_{\chi \in G(m)}  \sum_{\substack{n \leq x \\ n \equiv a \text{ mod (q)} }}\frac{1}{\varphi(m)}\chi(n) \mu(n) \nonumber\\
&= \sum_{\chi \in G(m)}  \sum_{j = 0}^{m - 1}\frac{\chi(j)}{\varphi(m)}\lim_{x \to \infty}\frac{1}{x}  \sum_{\substack{n \leq x \\ n \equiv a \text{ mod (q)} \\ n \equiv j \text{ mod}(m)}}\mu(n).
\end{align}
If $\gcd(j, m) \neq 1$ then $\chi(j) = 0$ hence the inner term in (4) vanishes completely. If there is no solution to the system $n \equiv a \pmod{q}, n \equiv j \pmod{m}$ then the inner sum in (4) is clearly $0$. Otherwise, let $a_j$ denote any solution, and $k$ the least common multiple of $q$ and $m$. Then since $\gcd(a, q) = 1$ and $\gcd(j, m) = 1$ we must have $\gcd(a_j, k) = 1$. Thus we have 
\[
\sum_{\substack{n \leq x \\ n \equiv a \text{ mod (q)} \\ n \equiv j \text{ mod}(m)}}\mu(n) = \sum_{\substack{n \leq x \\ n \equiv a_j \text{ mod (k)}}} \mu(n). \\\\
\]
Thus by (a) we have
\[
\lim_{x \to \infty} \frac{1}{x}\sum_{\substack{n \leq x \\ n \equiv a_j \text{ mod (k)}}} \mu(n) = 0.
\]
Thus all inner terms in (4) vanish, which suffices.
\item Let $e = q / d$ and $f = a / d$. Then clearly
\begin{align*}
\lim_{x \to \infty} \frac{1}{x}\sum_{\substack{n \leq x \\ n \equiv a \text{ mod (q)}}} \mu(n) = \lim_{x \to \infty} \frac{1}{x}\sum_{\substack{n \leq dx \\ n \equiv e \text{ mod (f)}}} \mu(dn).
\end{align*}
If $\gcd(d, n) > 1$ then $dn$ is not squarefree, hence $\mu(dn) = 0$. Otherwise $\mu(dn) = \mu(d)\mu(n)$, thus 
\begin{align*}
\lim_{x \to \infty} \frac{1}{x}\sum_{\substack{n \leq dx \\ n \equiv e \text{ mod (f)}}} \mu(dn) = \lim_{x \to \infty} \frac{\mu(d)}{x}\sum_{\substack{n \leq dx \\ n \equiv e \text{ mod (f)}\\ \gcd(d, n) = 1}} \mu(n) = 0
\end{align*}
by (b).
\end{enumerate}
\end{opghand}
\end{document}
