\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\ord}{ord}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Analytic Number Theory Homework 1}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1.4] \
\begin{enumerate}[a)]
\item We give a proof by induction on $m = \lfloor x \rfloor$. If $m = 2$ the result is true, since $2 \leq 16$. Now suppose $m > 2$ and it holds for all integers $n < m$. If $m$ is even it holds for $m - 1$. Since $m$ is even and $m > 2$, $m$ is not prime. Therefore
\[
\prod_{p \leq m} p = \prod_{p \leq m - 1} p \leq 4^{m - 1} \leq 4^m,
\]
as desired. Now suppose $m = 2n + 1$ is odd. Note that any prime $p$ satisfying $n + 1 < p \leq m$ does not divide $n!$ or $(n + 1)!$, but it does divide $(2n + 1)!$. Thus 
\[
\(\prod_{n + 1 < p \leq m} p\) \mid \binom{2n + 1}{n}, \text{ which implies } \prod_{n + 1 < p \leq m} p \leq \binom{2n + 1}{n}.
\]
By Lemma 1.8 (since $\lfloor(2n + 1) / 2\rfloor = n$) the right hand side is at most $2^{2n}$. Applying the induction hypotheses to $n + 1$, we see that
\[
\prod_{p \leq n + 1} p \leq 4^{n + 1}.
\]
Combining the two inequalities, we find that 
\[
\prod_{p \leq 2n + 1} = \(\prod_{n + 1 < p \leq m} \)\( \prod_{p \leq n + 1} p\) \leq 2^{2n} \cdot 4^{n + 1} = 4^{2n + 1} = 4^m,
\]
as needed.
\item Clearly $\ord_p\(\binom{2n}{n}\) = \ord_p((2n)!) - 2 \ord_p(n!)$. By Lemma 1.5 this is equal to
\begin{equation}
\sum_{j = 1}^\infty \left \lfloor \frac{2n}{p^j} \right \rfloor - 2 \sum_{j = 1}^\infty \left \lfloor \frac{n}{p^j} \right \rfloor.
\end{equation}
If $n > 4$, then $n \geq 5$ hence $p^2 > \frac{4}{9}n^2 \geq \frac{20}{9}n > 2n$ so $\lfloor n / p^j \rfloor = \lfloor 2n / p^j \rfloor = 0$ for any $j > 1$. Since $\frac{2}{3}n < p \leq n$ we have $1 \leq \frac{n}{p} < \frac{3}{2}$, hence $\lfloor n / p \rfloor = 1$. Similarly we have $2 \leq \frac{2n}{p} < 3$ hence $\lfloor 2n / p \rfloor = 2$. We conclude that $(1)$ evaluates to $0$, which clearly implies that $p$ does not divide $\binom{2n}{n}$. 
\item Factorize $\binom{2n}{n} = p_1^{k_1} \cdots p_t^{k_t}$ with the $p_i$ distinct primes and $k_i \in \ZZ$. By Lemma 1.7 we have $p_i^{k_i} \leq 2n$ for any $i$. Thus clearly
\[
\prod_{p_i \leq \sqrt{2n}} p_i^{k_i} \leq \prod_{p_i \leq \sqrt{2n}} (2n) \leq (2n)^{\pi(\sqrt{2n})}.
\]
Any $p_i > \sqrt{2n}$ divides $\binom{2n}{n}$ at most once, since in that case $p_i^2 > 2n$ hence $\lfloor n / p_i^j \rfloor = \lfloor 2n / p_i^j \rfloor = 0$ for $j > 1$, thus
\[
\ord_p\(\binom{2n}{n}\) = \left \lfloor \frac{2n}{p_i}\right \rfloor - 2 \left \lfloor \frac{n}{p_i} \right \rfloor \leq 1,
\]
since $\lfloor x \rfloor + \lfloor y \rfloor \geq \lfloor x + y \rfloor - 1$ for any real numbers $x, y$ (take $x = y = n / j$). Since any prime $p$ dividing $\binom{2n}{n}$ is at most $\frac{2}{3}n$ by (b) (we there are no primes $p$ satisfying $n < p \leq 2n$), we obtain
\[
\prod_{\sqrt{2n} < p_i} p_i^{t_i} \leq \prod_{p \leq \frac{2}{3}n} \leq 4^{2/3},
\]
where the left product is over all $p_i$ occuring in the prime factorization of $\binom{2n}{n}$ that are at least $\sqrt{2n}$, and the right product is over \emph{all} primes less than $2/3n$. Combining this with the first inequality we see that 
\[
\binom{2n}{n} = \(\prod_{p_i \leq \sqrt{2n}} p_i^{k_i}\)\(\prod_{\sqrt{2n} < p_i} p_i^{t_i}\) \leq (2n)^{\pi(\sqrt{2n})}4^{2n/3}
\]
as desired.
\item The above would imply 
\[
\frac{2^{2n}}{2n + 1} \leq \binom{2n}{n} \leq (2n)^{\pi(\sqrt{2n})}4^{2n / 3} \leq (2n)^{4\frac{\sqrt{2n}}{\log 2n}}4^{2n / 3} = e^{4\sqrt{2n}}4^{2n/3},
\]
where the first inequality follows from Lemma 1.8 and the third from Theorem 1.4. But for $n > 1000$ we clearly have
\[
\frac{4^{n / 3}}{2n + 1} > e^{4\sqrt{2n}}
\]
since one easily sees that the quotiënt LHS / RHS is increasing for $n \geq 1000$ and the inequality holds at $n = 1000$. 
\end{enumerate}
\end{opghand}
\begin{opghand}[1.9] \
\begin{enumerate}[a)]
\item Suppose there were only finitely many primes $p \equiv 3 \pmod{4}$, say $p_1, \dots, p_k$. Then clearly $(p_1\cdots p_k)^2 \cong 1 \pmod{4}$, hence $n = (p_1\cdots p_k)^2 + 2$ satisfies $n \equiv 2 \pmod{4}$ and $n > 1$. Since all of the $p_i$ are $3 \pmod{4}$ they are in particular not equal to $2$, so they do not divide $n$ as they do divide $(p_1 \cdots p_k)^2$. 

If we now factorize $n = q_1^{t_1} \cdots q_k^{t_k}$, then clearly all of the $q_i$ are odd as $n$ is odd. But since none of the $p_j $ divide $n$ all of the $q_i$ must be different from the $p_j$. Since we assumed that any prime $3 \pmod{4}$ was some $p_j$, all of the $q_i$ are $1 \pmod{4}$. But then $n \equiv 1^{t_1} \cdots 1^{t_k} \equiv 1 \pmod{4}$, which is a contradiction with $n \equiv 3 \pmod{4}$. Thus there are infinitely many primes $p \equiv 3 \pmod{4}$. 
\item Suppose there were only finitely many primes $p_i$ satisfying $p_i \not \equiv 1 \pmod{q}$. Then in particular there are only finitely many primes $p_i$ satisfying $p_i \not \equiv 1 \pmod{q}$, $p_i \nmid q, p_i \nmid 2$, say $p_1, \dots, p_k$. Let $\varphi$ denote Euler's totient function, then we know that $a^{\varphi(q)} \equiv 1 \pmod{q}$ for any $a$ coprime to $q$. As all our $p_i$ are coprime to $q$ we have $(p_1 \cdots p_k)^{\varphi(q)} \equiv 1 \pmod{q}$. 

Clearly one of $q - 1$ or $2q - 1$ is odd, hence contains an odd prime $r$ factor not equal to $1 \pmod{q}$ (since $1 \not \equiv -1 \pmod{q}$ as $q > 3$). Clearly $r$ is coprime to $q$, hence the product $p_1 \cdots p_k$ is not the empty product, and at least $2$.

Therefore $(p_1 \cdots p_k)^{\varphi(q)} \geq 2^2$, and $n := (p_1 \cdots p_k)^{\varphi(q)} - 2 \geq 2$. Since we assumed all the $p_i$ to be odd, $n$ is odd and none of the $p_i$ divide $n$. Since $n \equiv -1 \pmod{q}$, it has at least one prime factor $p \not \equiv 1 \pmod{q}$. Since $\gcd(n, q) = 1$ we have $p \nmid q$ and since $n$ is odd we have $p \nmid 2$. But that implies $p$ should be some $p_i$, which is a contradiction as none of the $p_i$ divide $n$. Thus there are infinitely many primes $p_i$ satisfying $p_i \not \equiv 1 \pmod{q}$. 

\item If $1 + x+ \dots + x^{q - 1} \equiv 0 \pmod{p}$ then $1 - x^q \equiv 0 \pmod{p}$. Thus $x^q \equiv 1 \pmod{p}$. Let $a$ be the smallest positive integer such that $x^a \equiv 1 \pmod{p}$, then clearly $a \mid q$ and by Fermat's little theorem also $a \mid p - 1$. Since $q$ is prime we either have $a = q$ or $a = 1$. But $q \nmid p - 1$ since $p \not \equiv 1 \pmod{q}$. Hence $a = 1$ and $x \equiv 1 \pmod{q}$. But then $1 + x + \dots + x^{q - 1} \equiv 1 + \dots + 1 \equiv q \pmod{p}$, so $p \mid q$. But we assumed $p$ and $q$ are distinct primes. Thus there can be no integer $x$ such that $1 + \dots + x^{q - 1} \equiv 0 \pmod{p}$. 

Now suppose there are only finitely many primes $p \equiv 1 \pmod{q}$, say $p_1, \dots, p_k$. Let us define $x = qp_1 \cdots p_k$, and let $n := 1 + x + \dots + x^{q - 1}$. Then clearly $n > 1$ as $x \geq q$, and $n$ is obviously coprime to any $p_i$ and $q$.   Thus there exists some prime $p$ dividing $n$ such that $p \not \equiv 1 \pmod{q}$ (as $p \neq p_i$ for all $i$) and $p \neq q$. But by the above no such prime $p$ can exist, since $p \mid n$ if and only if $1 + \dots + x^{q - 1} \equiv 0 \pmod{p}$. Contradiction, hence there are infinitely many primes $p \equiv 1 \pmod{q}$. 

\begin{opghand}[2.5]
Note that 
\[
|n^{-s}| \leq \int_{n - 1}^n \frac{1}{t^s}dt.
\]
Since $s > 1$ and $|f(n)| \leq 1$ we thus have
\[
\sum_{n = 1}^\infty \left | f(n)n^{-s}\right | \leq 1 + \sum_{n = 2}^\infty \int_{n - 1}^n \frac{1}{t^s} dt \leq 1 + \int_{1}^\infty \frac{1}{t^s} = 1 + \frac{1}{1 - s} < \infty.
\]
Thus $L_f(s)$ converges absolutely for $s > 1$. Since $\mu$ is multiplicative and $f$ is multiplicative, the function $g(n) := \mu(n)f(n)$ is also multiplicative. Since $|g(n)| \leq 1$ by the above argument $L_g(s)$ converges absolute for $s > 1$, and by Theorem 2.13 we have
\[
L_g(s) = \lim_{M \to \infty} \prod_{p \leq M} \(\sum_{j = 0}^\infty \mu(p^j)f(p^j)p^{-js}\) = \lim_{M \to \infty} \prod_{p \leq M} \(1 - f(p)p^{-s}\).
\]
By Corollary 2.14 we also have for any $s > 1$ the identity
\[
L_f(s) = \lim_{N \to \infty} \prod_{p \leq N} \frac{1}{1 - f(p)p^{-s}}.
\]
We know from real analysis that $\lim_{M \to \infty}f_1(M) \cdot \lim_{N \to \infty}f_2(N) = \lim_{x \to \infty} f_1(x)f_2(x)$ if both limits exist. Since finite products commute, we thus have
\[
L_g(s) \cdot L_f(s) = \lim_{x \to \infty} \prod_{p \leq x} (1 - f(p)p^{-s})\frac{1}{1 - f(p)p^{-s}} = \prod_p 1 = 1.
\]
Thus $L_g(s) = L_f(s)^{-1}$ as requested.

Since for any $s > 1$ we can find $s_0$ with $1 < s_0 < s$, by Corollary 2.3 we have
\[
L_f'(s) = \sum_{n = 1}^\infty (-\log n)f(n)n^{-s}. 
\]
Setting $h(n) := f(n)\log(n)$, we see that (using that $f$ is strongly multiplicative, i.e. $f(n / d)f(d) = f(n)$)
\[
h * g (n) = \sum_{d \mid n} \log(d)f(d)\mu(n / d) f(n / d) = f(n) \sum_{d \mid n}\log(d) \mu(n / d) = f(n) \Lambda(n)
\]
since $\Lambda = \mu * \log$. By Theorem 2.12 we have 
\[
-L_f'(s)L_f(s)^{-1} = -L_h(s)L_g(s) = -L_{f \cdot \Lambda}(s) = -\sum_{n = 1}^\infty f(n)\Lambda(n)n^{-s},
\]
as required.
\end{opghand}
\begin{opghand}[2.6]
We see that
\[
\sum_{n \leq x} \mu(n)G(x / n) = \sum_{n \leq x} \sum_{m \leq x / n}  \mu(n)F(x / nm).
\]
But clearly there is a bijection between the set $\{(n, m) \in \NN^2 \mid n \leq x, m \leq x / n\}$ and the set $\{(k, d) \in \NN^2 \mid k \leq x, d \mid k\}$ by mapping $(n, m) \to (nm, n)$. Thus 
\[
\sum_{n \leq x} \sum_{m \leq x / n}  \mu(n)F(x / nm) = \sum_{k \leq x} \sum_{d \mid k}  \mu(d)F(x / k) = F(x),
\]
where the last equality holds since 
\[
\sum_{d \mid n} \mu(d) = e(n).
\]
This completes the proof.
\end{opghand}
\begin{opghand}[2.8]
\begin{enumerate}[a)] \ 
\item We first show $f$ and $g$ converge for $s$ with $\text{Re}(s) > 0$. Write $t = \text{Re}(s)$. It suffices to show the partial sums form a Cauchy sequence. Set $F(x) = \sum_{n \leq x} f(n)$. Clearly $|F(x)| \leq 1$ for any $x$. For any $N, M > 0$ we find using summation by parts that
\begin{align*}
\left |\sum_{M \leq n \leq N} f(n)n^{-s} \right | &= \left | f(N)N^{-s} + \int_M^N F(x)(-s)\frac{1}{x^{s + 1}} dx \right| \\ &\leq \frac{1}{N^t} + \int_{M}^\infty \frac{|s|}{x^{t + 1}} = \frac{1}{N^t} + \frac{|s|}{|t|}\frac{1}{M^t},
\end{align*}
which tends to zero as $M, N \to \infty$. Similarly, if we set $G(x) = \sum_{n \leq x} g(n)$, then clearly $|G(x)| \leq 2$. Thus
\begin{align*}
\left |\sum_{M \leq n \leq N} g(n)n^{-s} \right | &= \left |g(N)N^{-s} + \int_M^N G(x)(-s)\frac{1}{x^{s + 1}} dx \right| \\ &\leq \frac{2}{N^t} + 2\int_{M}^\infty \frac{|s|}{x^{t + 1}} = \frac{2}{N^t} + \frac{2|s|}{|t|}\frac{1}{M^t},
\end{align*}
which also tends to zero as $N, M \to \infty$. Thus $L_f$ and $L_g$ converge for any $s \in \CC$ with $\text{Re}(s) > 0$. They don't converge in $0$, since neither of the sums
\[
\sum_{n = 1}^\infty f(n), \ \sum_{n = 1}^\infty g(n)
\]
converge (since the summands $f(n), g(n)$ dont tend to zero as $n \to \infty$). Thus $\sigma_0(f) = \sigma_0(g) = 0$. 
\item Let $h(n) = 1$ if $n$ is even and $0$ if $n$ is odd. Clearly $f(n) = 1 - 2h(n)$. If $\text{Re}(s) > 1$ then since all the below series converge absolutely ($\sigma_a(f) \leq \sigma_0(f) + 1 = 1$ and the same for $\sigma_a(g)$) the rearrangement is allowed, hence
\[
\(1 - \frac{2}{2^s}\)\sum_{n = 1}^\infty \frac{1}{n^s} = \sum_{n = 1}^\infty \frac{1}{n^s} -2 \sum_{n = 1}^\infty \frac{h(n)}{n^s} - \sum_{n = 1}^\infty \frac{1 - 2h(n)}{n^s} = \sum_{n = 1}^\infty \frac{f(n)}{n^s}.
\]
Similarly let $h'(n) = 1$ if $n$ is divisible by $3$ and $0$ otherwise. Clearly $g(n) = 1 - 3h'(n)$. Since everything converges absolutely again we can rearrange, thus for $s$ with $\text{Re}(s) > 1$ we have
\[
\(1 - \frac{3}{3^s}\)\sum_{n = 1}^\infty \frac{1}{n^s} = \sum_{n = 1}^\infty \frac{1}{n^s} -3 \sum_{n = 1}^\infty \frac{h'(n)}{n^s} - \sum_{n = 1}^\infty \frac{1 - 3h'(n)}{n^s} = \sum_{n = 1}^\infty \frac{g(n)}{n^s},
\]
as desired.
\item Suppose $2^{1 - s} = 1 = 3^{1 - s}$ for some $s = a + bi \in \CC$. Then $(1 - s) \log(2) = k_1\pi i$ and $(1 - s)\log(3) = k_2 \pi i$ for some $k_1, k_2 \in \ZZ$. By looking at the real and imaginary parts of the equation, we find that
\begin{align*}
(1 - a)\log(2) &= 0 \qquad (1 - a)\log(3) = 0, \\
b\log(2) &= k_1\pi \qquad b\log(3) = k_2\pi.
\end{align*}
The first two equations obviously imply $a = 1$. The second two equations imply that $b(k_2\log(2) - k_1\log(3)) = 0$. If $b \neq 0$, then $\log(2^{k_2}) = \log(3^{k_1})$. Since $\log$ is increasing it is in particular injective, hence $2^{k_2} = 3^{k_1}$, which implies $k_1 = k_2 = 0$ since $k_1, k_2 \in \ZZ$. But then from the original equations it still follows that $b = 0$. We conclude $s = 1$.

Now clearly $(1 - 2^{1 - s})^{-1}L_f(s)$ is an analytic continuation of $\zeta(s)$ to 
\[
\{s \in \CC \mid \text{Re(s)} > 0, 2^{1 - s} \neq 1\}.
\]
Similarly $(1 - 3^{1 - s})^{-1}L_g(s)$ is an analytic continuation of $\zeta(s)$ to 
\[
\{s \in \CC \mid \text{Re(s)} > 0, 3^{1 - s} \neq 1\}.
\]
By Corollary 0.40 there exists an analytic continuation of $\zeta(s)$ to the union of those two sets, which is exactly $\{s \in \CC \mid \text{Re}(s) > 0, s \neq 1\}$ as required.

Since $(1 - 2^{1 - s})^{-1} L_f(s)$ is analytic near $s = 1$ (since $2^{1 - s} = 1$ can only occur if $s = k \frac{\pi}{\log(2)}i$ for some $k \in \ZZ$), we can compute the residue of $\zeta(s)$ at $1$ by computing 
\begin{align*}
\lim_{s \to 1} (s - 1)(1 - 2^{1 - s})^{-1} L_f(s) &= \lim_{s \to 1} \frac{s - 1}{1 - 2^{1 - s}}L_f(1) \\
&= \frac{1}{\log(2)2^{1}}\(\sum_{n = 1}^\infty \frac{(-1)^n}{n}\) \\
&= \frac{1}{\log(2)} \log(2) = 1,
\end{align*}
where the second equality follows by applying L'Hospital's rule.

\end{enumerate}
\end{opghand}
\end{enumerate}
\end{opghand}
\end{document}
