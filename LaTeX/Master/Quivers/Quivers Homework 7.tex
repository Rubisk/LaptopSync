\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Opgave]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Opgve]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\rad}{rad}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catF}{\mathcal{F}}
\newcommand{\catE}{\mathcal{E}}
\newcommand{\catD}{\mathcal{D}}


% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Quivers Huiswerk 7}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1.1] 
We zullen de opgave interpreteren alsof
\[
F_1(\cdot, -) := F(\cdot) \oplus F(-) \qquad F_2(\cdot, -) := F(\cdot \oplus -),
\]
immers het tensorproduct hoeft niet per sé te bestaan in een additieve categorie. Ook zijn $F_1$ en $F_2$ nog geen welgedefiniëerde functoren, omdat er geen actie is gedefiniëerd op de morfismen. Omdat ik nergens in de notes een definitie van de functor $\oplus$ kon vinden, zullen we eerst voor een algemene additieve categorie $\catE$ de operatie $\oplus_\catE: \catE \times \catE \to \catE$ tot een functor maken door te stellen dat
\begin{align*}
\oplus_\catE: \Hom_{\catE \times \catE}((X, Y), (Z, W)) &\to \Hom_{\catE}(X \oplus Y, Z \oplus W) \\
(\varphi, \psi) &\mapsto \varphi \oplus \psi,
\end{align*}
waar $\varphi \oplus \psi$ is gedefiniëerd als de unieke afbeelding verkregen uit de universele eigenschap van $X \oplus Y$ die voldoet aan 
\begin{align*}
(\varphi \oplus \psi) \circ \iota_X = \iota_Z \circ \varphi, \\
(\varphi \oplus \psi) \circ \iota_Y =  \iota_W \circ \psi.
\end{align*}
(We gebruiken $\iota$ voor de `inclusies' verkregen uit de universele eigenschap voor de objecten $\cdot \oplus -$) Het lijkt ons dat het niet binnen de opgave valt om na te gaan dat dit een goedgedefiniëerde functor is. We zien nu dat $F_1$ en $F_2$ goedgedefiniëerde functoren zijn als we ze interpreteren als zijnde $F_1 = \oplus_\catD \circ (F, F)$ en $F_2 = F \circ \oplus_\catC$ (als men heel veel geduld heeft kan men nadenken over hoe de functor $(F, F): \catC \times \catC \to \catC \times \catC$ gedefiniëerd is, wij houden het bij `alles gaat componentsgewijs').

We gaan eerst na dat $\xi_{M, N}$ een morfisme van functoren is. Laat dus $(M, N), (M', N') \in \catC \times \catC$ zijn, en laat $(\varphi, \psi) \in \Hom_{\catC \times \catC}((M, N), (M', N'))$ zijn. We moeten nagaan dat het diagram
\[
\begin{tikzcd}[column sep=huge]
F(M) \oplus_\catD F(N) \rar{F(\varphi) \oplus_\catD F(\psi)} \dar{\xi_{M, N}} & F(M') \oplus_\catD F(N') \dar{\xi_{M', N'}}  \\
F(M \oplus_\catC N) \rar{F(\varphi \oplus_\catC \psi)} & F(M' \oplus_\catC N') 
\end{tikzcd}
\]
commuteert. Merk nu op dat
\begin{align*}
\xi_{M', N'} \circ ((F(\varphi) \oplus_\catD F(\psi)) \circ \iota_{F(M)} &= \xi_{M', N'} \circ \iota_{F(M')} \circ F(\varphi) &(\text{definitie van } F(\varphi) \oplus F(\psi)) \\
&= F(\iota_{M'}) \circ F(\varphi) &(\text{definitie van } \xi_{M', N'})  \\
&= F(\iota_{M'} \circ \varphi) &(F \text{ is een functor}) \\
&= F((\varphi \oplus_\catC \psi) \circ \iota_M) &(\text{definitie van } \varphi \oplus_\catC \psi) \\
&= F(\varphi \oplus_\catC \psi) \circ F(\iota_M) &(F \text{ is een functor}) \\
&= F(\varphi \oplus_\catC \psi) \circ \xi_{M, N} \circ \iota_{F(M)}  &(\text{definitie van } \xi_{M, N}) \\
\end{align*}
en bij een gelijksoortig argument 
\[
\xi_{M', N'} \circ ((F(\varphi) \oplus_\catD F(\psi)) \circ \iota_{F(N)} = F(\varphi \oplus_\catC \psi) \circ \xi_{M, N} \circ \iota_{F(N)}.
\]
Uit de universele eigenschap van $F(M) \oplus_\catD F(N)$ mogen we concluderen dat 
\[
\xi_{M', N'} \circ ((F(\varphi) \oplus_\catD F(\psi)) = F(\varphi \oplus_\catC \psi) \circ \xi_{M, N},
\]
waarmee we commutativiteit van het diagram hebben aangetoond.

Resteert te laten zien dat $\xi_{M, N}$ een isomorfisme is voor alle $M, N \in \catC$. Merk op dat 
\begin{align*}
F(\pi_M): F(M \oplus_\catC N) &\to F(M) \\
F(\pi_N): F(M \oplus_\catC N) &\to F(N)
\end{align*}
gedefiniëerd zijn aan de hand van Lemma 1.3 in de notes van week 2. Volgens Lemma 1.3(b) bestaat er ook een unieke afbeelding
\[
\eta_{M, N}:  F(M \oplus_\catC N) \to F(M) \oplus_\catD F(N)
\]
die voldoet aan
\begin{align*}
\pi_{F(M)} \circ \eta_{M, N} &= F(\pi_M), \\
\pi_{F(N)} \circ \eta_{M, N} &= F(\pi_N).
\end{align*}
We beweren dat $\eta_{M, N}$ de inverse van $\xi_{M, N}$ is. Om dit na te gaan berekenen we (met keer op keer hulp van  Lemma 1.3(a))
\begin{align*}
\pi_{F(M)} \circ \eta_{M, N} \circ \xi_{M, N} \circ \iota_{F(M)} &= F(\pi_M) \circ F(\iota_M) = F(\id_M) = \id_{F(M)}, \\
\pi_{F(N)} \circ \eta_{M, N} \circ \xi_{M, N} \circ \iota_{F(M)} &= F(\pi_N) \circ F(\iota_M) = F(0) = 0, \\
\pi_{F(M)} \circ \eta_{M, N} \circ \xi_{M, N} \circ \iota_{F(N)} &= F(\pi_M) \circ F(\iota_N) = F(0) = 0, \\
\pi_{F(N)} \circ \eta_{M, N} \circ \xi_{M, N} \circ \iota_{F(N)} &= F(\pi_N) \circ F(\iota_N) = F(\id_N) = \id_{F(N)},
\end{align*}
waar we expliciet gebruiken dat $F$ een additieve functor is. Verder merken we op dat ook uit Lemma 1.3(a) volgt dat
\begin{align*}
\pi_{F(M)} \circ  \iota_{F(M)} &=  \id_{F(M)}, \qquad
\pi_{F(N)} \circ \iota_{F(M)} =  0, \\
\pi_{F(N)} \circ  \iota_{F(N)} &= \id_{F(N)}, \qquad
\pi_{F(M)} \circ  \iota_{F(N)} =  0.
\end{align*}
Uit de universele eigenschap van $F(M) \oplus_\catC F(N)$ mogen we dus concluderen dat
\begin{align*}
\pi_{F(M)} \circ \eta_{M, N} \circ \xi_{M, N} &= \pi_{F(M)} = \pi_{F(M)} \circ \id_{F(M) \oplus_\catD F(N)} \\
\pi_{F(N)} \circ \eta_{M, N} \circ \xi_{M, N} &= \pi_{F(N)} = \pi_{F(N)} \circ \id_{F(M) \oplus_\catD F(N)},
\end{align*}
zodat uit Lemma 1.3(b) volgt dat
\[
\eta_{M, N} \circ \xi_{M, N} = \id_{F(M) \oplus_\catD F(N)}.
\]

Resteert de andere compositie. Merk op dat in de notes van week 2, onder Lemma 1.3, ook bewezen wordt dat $\iota_{F(M)} \circ \pi_{F(M)} + \iota_{F(N)} \circ \pi_{F(N)} = \id_{F(M) \oplus_\catD F(N)}$. Dus
\begin{align*}
\xi_{M, N} \circ \eta_{M, N} &= \xi_{M, N} \circ (\iota_{F(M)} \circ \pi_{F(M)} + \iota_{F(N)} \circ \pi_{F(N)}) \circ \eta_{M, N} \\
&= \xi_{M, N} \circ \iota_{F(M)} \circ \pi_{F(M)} \circ \eta_{M, N} + \xi_{M, N}  \circ \iota_{F(N)} \circ \pi_{F(N)} \circ \eta_{M, N} \\
&= F(\iota_M) \circ F(\pi_M) + F(\iota_N) \circ F(\pi_N) \\
&= F(\iota_M \circ \pi_M + \iota_N \circ \pi_N) \\
&= F(\id_{M \oplus_\catC N}) &(\text{onder Lemma 1.3})\\
&= \id_{F(M \oplus_\catC N)}
\end{align*}
waarmee is aangetoond dat $\xi_{M, N}$ een isomorfisme is.
\end{opghand}
\begin{opghand}[1.6] 
Aangezien $\text{Res}_e$ een functor $\text{Mod}_A \to \text{Mod}_{eAe}$ is en $\text{Ind}_e(M) \cong X$ volgt dat $\text{Res}_e \text{Ind}_e(M) \cong \text{Res}_e  X$ in $\text{Mod}_{eAe}$. Uit Lemma 1.4(ii) van de notes van week 11 volgt dus dat  $M \cong \text{Res}_e \text{Ind}_e(M) \cong\text{Res}_e X$
\end{opghand}
\end{document}
