\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Lemma]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\Gal}{Gal}
\DeclareMathOperator{\Cl}{\text{C}\ell}
\DeclareMathOperator{\Nm}{Nm}


% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\HH}{\mathbb{H}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catO}{\mathcal{O}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}
%% Document
\begin{document}


%% Kopregel
\title{Class Field Theory Homework 4}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[2.4.6] \
\begin{enumerate}[(1)]
\item Note that $-(\delta - \delta')\log(s - 1)$ has to remain bounded as $s \to 1^+$. However $\log(s - 1) \to -\infty$ as $s \to 1^+$, hence this can only happen if $\delta - \delta' = 0$ or $\delta = \delta'$.
\item If $S$ denotes the set of all finite primes, then as a corollary of Lemma 2.4.4
\[
\sum_{\frak{p} \in S} \Nm(\frak{p})^{-s} = \sum_{\frak{p}} \Nm(\frak{p})^{-s} \sim \log \zeta_K(S) \sim -\log(s - 1)
\]
hence $\delta(S) = 1$.

Now suppose $S$ is any finite set of primes. Then for $s \in [1, \infty)$ one has
\[
\sum_{\frak{p} \in S} \Nm(\frak{p})^{-s} \leq \sum_{\frak{p} \in S} 1 \leq |S|,
\]
hence the sum is bounded as $s \to 1^+$. Therefore $\sum_{\frak{p} \in S}  \sim 0$ and $\delta(S) = 0$.
\item Note that for any $s \in [1, \infty)$ one has $\Nm(\frak{p})^{-s} \geq 0$ for any prime $\frak{p}$. Thus since both sums are over positive terms only, then for $s \in [1, \infty)$
\[
\sum_{\frak{p} \in S} \Nm(\frak{p})^{-s} \leq  \sum_{\frak{p} \in S'} \Nm(\frak{p})^{-s}
\]
as the right sum contains more terms then the left sum. Now suppose $\delta(S') < \delta(S)$. Then by definition of $\sim$ there exists a constant $C$ such that 
\[
- (\delta(S) - \delta(S')) \log(s - 1) \leq C + \sum_{\frak{p} \in S} \Nm(\frak{p})^{-s} -  \sum_{\frak{p} \in S'} \Nm(\frak{p})^{-s} \leq C
\]
as $s \to 1^+$. However $-(\delta(S) - \delta(S')) < 0$ and $\log(s - 1) \to -\infty$ as $s \to 1^+$. Hence the right hand side tends to $+\infty$ as $s \to 1^+$ while the right hand side is bounded above by a constant. Contradiction, thus $\delta(S') \leq \delta(S)$ (if both exist).
\item Denote $f(\frak{p})$ for the residue degree of $\frak{p}$ in some field extension $K / \QQ$. To show $\delta(S) = \delta(S \cap S_1)$ it suffices to bound 
\[
\left | \sum_{\frak{p} \in S} \Nm(\frak{p})^{-s} - \sum_{\frak{p} \in S \cap S_1} \Nm(\frak{p})^{-s} \right |
\]
by a constant as $s \to 1^+$. But this quantity is equal to 
\[
\left | \sum_{\substack{\frak{p} \in S \\ f(\frak{p}) \geq 2}} \Nm(\frak{p})^{-s}\right | \leq \left | \sum_{\substack{\frak{p} \subset \catO_K \\ f(\frak{p}) \geq 2}} \Nm(\frak{p})^{-s}\right |
\]
Now note that any prime $\frak{p} \subseteq \catO_K$ lying above a prime $p \subseteq \ZZ$ with $f(\frak{p}) \geq 2$ will satisfy $\Nm(\frak{p}) \geq p^2$. Since there lie at most $[K : \QQ]$ primes above any prime $p$, we can thus bound 
\[
\left | \sum_{\substack{\frak{p} \subset \catO_K \\ f(\frak{p}) \geq 2}} \Nm(\frak{p})^{-s}\right | \leq [K : \QQ]  \sum_{p \text{ prime}} p^{-2s} \leq  [K : \QQ]\sum_{n = 1}^\infty n^{-2} < \infty.
\]
Hence 
\[
\left | \sum_{\frak{p} \in S} \Nm(\frak{p})^{-s} - \sum_{\frak{p} \in S \cap S_1} \Nm(\frak{p})^{-s} \right |
\]
is bounded above by a constant as $s \to 1^+$, which suffices.
\end{enumerate}
\end{opghand}
\end{document}
