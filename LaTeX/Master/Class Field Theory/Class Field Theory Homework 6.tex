\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\Gal}{Gal}
\DeclareMathOperator{\Cl}{\text{C}\ell}
\DeclareMathOperator{\Nm}{Nm}


% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\HH}{\mathbb{H}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catO}{\mathcal{O}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}
%% Document
\begin{document}


%% Kopregel
\title{Class Field Theory Homework 6}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[3.10] \
\begin{enumerate}[(1)]
\item This is almost obvious, since $v_{\frak q}(\pi)$ is simply the power of $\frak{q}$ in the ideal $\pi \catO_L$. If one writes $\pi \catO_k = \frak{p}^{v_{\frak p}(\pi)} \frak{a}$ for some ideal $\frak{a}$ coprime to $\frak{p}$, then clearly $\frak{a} \catO_L$ is coprime to $\frak{q}$. Hence the number of factors of $\frak{q}$ in the ideal $\pi \catO_L$ is  equal to the power of $\frak{q}$ in the factorization of $\frak p^{v_{\frak p}(\pi)}$. But the latter is equal to $e_{\frak p} v_{\frak p}(\pi)$ by definition of $e_{\frak p}$ (the exponent of $\frak q$ in $\frak p$). Thus $v_{\frak q}(\pi) = e_{\frak p} v_{\frak p}(\pi) = e_{\frak p}$.
\item We start with the diagram
\[
\begin{tikzcd}
1 \rar & \dar{\Nm} \catO_{L, \frak q}^* \rar & \dar{\Nm} L^*_{\frak q} \rar{v_{\frak q}} & \ZZ \rar \dar{\times f_{\frak p}} &  1 \\
1 \rar &  \catO_{K, \frak p}^* \rar & K^*_{\frak p} \rar{v_{\frak p}} & \ZZ \rar & 1
\end{tikzcd}
\]
which clearly has exact rows. The left square obviously commutes, the right square commutes since $\Nm(\frak{q}) = \frak{p}^{f_{\frak p}}$. Now note that since $G_{\frak q} \cong \Gal(L_{\frak q} / K_{\frak p})$, we actually know some of the cokernels:
\begin{align*}
\coker\(\Nm\colon \catO_{L, \frak q}^* \to \catO_{K, \frak p}^*\) &= \catO_{K, \frak p}^* / \Nm \catO_{L, \frak q}^* = H^0(G_{\frak q}, \catO_L^*), \\
\coker\(\Nm\colon L^*_{\frak q}  \to K^*_{\frak p} \) &= K^*_{\frak p}  / \Nm L^*_{\frak q} = H^0(G_{\frak q}, L^*_{\frak q}). 
\end{align*}
Hence by the snake lemma, we obtain an exact sequence
\[
\begin{tikzcd}
\dots \rar & 0 \rar & H^0(G_{\frak q}, \catO_L^*) \rar & H^0(G_{\frak q}, L^*_{\frak q}) \rar & \ZZ / f_{\frak p} \ZZ \rar & 0.
\end{tikzcd}
\]
Since we know (Corollary 3.3.10) that $\# H^0(G_{\frak q}, L^*_{\frak q}) = e_{\frak p} f_{\frak p}$, it follows that 
\[
\# H^0(G_{\frak q}, \catO_L^*) = \frac{e_{\frak p}f_{\frak p}}{f_{\frak p}} = e_{\frak p},
\]
as desired.
\item Consider the exact sequence of $G_{\frak q}$-modules
\[
\begin{tikzcd}
1 \rar &   \catO_{L, \frak q}^* \rar& L_{\frak q}^* \rar{v_{\frak q}} &  \ZZ \rar& 0.
\end{tikzcd}
\]
Note that $H^0(G_{\frak q}, \catO_{L, \frak q}^*) = e_{\frak p} = 0$ by assumption. By Corollary 3.3.8 (and the line above it) one has $q(\catO_{L, \frak q}^*) = 1$ and hence $H^1(G_{\frak q}, \catO_{L, \frak q}^*) = 0$ as well. Furthermore since $G_{\frak q}$ acts trivially on $\ZZ$ we have $H^0(G_{\frak q}, \ZZ) \cong \ZZ / \# G_{\frak q} \ZZ \cong \ZZ /  f_{\frak p} \ZZ$ since $e_{\frak p} = 1$. By the exact hexagon we obtain an isomorphism $H^0(G_{\frak q}, L_{\frak q}^*) \to H^0(G_{\frak q}, \ZZ) \cong \ZZ / f_{\frak p} \ZZ$ which suffices.
\end{enumerate}
\end{opghand}
\end{document}
