\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\Gal}{Gal}


% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\HH}{\mathbb{H}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catO}{\mathcal{O}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}
%% Document
\begin{document}


%% Kopregel
\title{Class Field Theory Homework 1}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1.11] \
\begin{enumerate}[(1)]
\item First note that any quadratic field extension of the rationals is of the form $\QQ(\sqrt{n})$ for some squarefree $n \in \ZZ$. To see this, note that any quadratic extension is of the form $\QQ(\alpha)$ with $\alpha \in \bar{\QQ}$, where $f^\alpha_\QQ = X^2 + aX + b$ for some $a, b \in \QQ$. But then $\QQ(\alpha) = \QQ(\beta)$ where $\beta$ is a root of $X^2 + b + a^2 / 4$. Hence any field extension is of the form $\sqrt{q}$ for some $q \in \QQ$. Now multiply $q$ with a square to turn it into a squarefree integer.

Now note that for any odd prime $p$, $\QQ(\zeta_p)$ contains a unique quadratic subfield, as $\Gal(\QQ(\zeta_p)/ \QQ)$ is cyclic of even order. Hence this must be of the form $\QQ(\sqrt{\pm n})$ for some $n \in \NN_{> 1}$ and sign. But as any prime $q$ dividing $n$ ramifies in $\QQ(\sqrt{\pm n})$ we find that $n = p$, since only $p$ ramifies in $\QQ(\zeta_p)$. Furthermore since $2$ ramifies in $\QQ(\sqrt{n})$ if and only if $n \equiv 3 \pmod{4}$ (for odd $n$), but $2$ may not ramify since $2$ does not ramify in $\QQ(\zeta_p)$, we find that the subfield is $\QQ(\sqrt{p})$ if $p \equiv 1 \pmod{4}$ and $\QQ(\sqrt{-p})$ if $p \equiv 3 \pmod{4}$. 

Hence for any $p \mid a$, $\sqrt{\pm p}$ is in $\QQ(\zeta_{p}) \subseteq \QQ(\zeta_{4|a|})$ (the latter holds since $x^p = 1$ implies $x^{4|a|} = 1$). But as $i \in \QQ(\zeta_{4}) \subseteq \QQ(\zeta_{4|a|})$ as well, and $\sqrt{2} \in \QQ(\zeta_{8}) \subseteq \QQ(\zeta_{4|a|})$ if $a$ is even, we may conclude $\sqrt{a} \in \QQ(\zeta_{4|a|})$ which suffices. 
\item In this case we find that $\sqrt{p} \subseteq \QQ(\zeta_p) \subseteq \QQ(\zeta_a)$ for any prime $p \mid a, p \equiv 3 \pmod{4}$ and $\sqrt{-p} \subseteq \QQ(\zeta_p) \subseteq \QQ(\zeta_a)$ for any prime $p \mid a, p \equiv 1 \pmod{4}$.

If $a$ is positive then the number of primes $p \equiv 3 \pmod{4}$ dividing $a$ is even (as $a \equiv 1 \pmod{4}$). Hence
\[
\prod_{p \mid a, p \equiv 3 \pmod{4}} \sqrt{-p} \times \prod_{p \mid a, p \equiv 1 \pmod{4}} \sqrt{p} = \sqrt{|a|} = \sqrt{a}.
\]
Conversely if $a$ is negative then the number of primes $p \equiv 3 \pmod{4}$ dividing $a$ is odd, hence 
\[
\prod_{p \mid a, p \equiv 3 \pmod{4}} \sqrt{-p} \times \prod_{p \mid a, p \equiv 1 \pmod{4}} \sqrt{p} = \sqrt{-|a|} = \sqrt{a}.
\]
In any case we find that $\sqrt{a} \in \QQ(\zeta_a)$ as required.
\item By (1) we may embed $\QQ(\sqrt{3}) \subseteq \QQ(\zeta_{12})$. Now $3$ is a square modulo $p$ if and only if $X^2 - 3$ factors into linear factors in $\mathbb{F}_p$, which happens if and only if the prime ideal $(p)$ factors completely in $\ZZ[\sqrt{3}] = \catO_{\QQ(\sqrt{3})}$. But the latter happens if and only if $(p, \QQ(\sqrt{3}) / \QQ)) = 1$, and since $(p, \QQ(\sqrt{3}) / \QQ)$ is the image of $(p, \QQ(\zeta_{12}) / \QQ)$ this only depends on the value of $p$ modulo $12$. 

We easily verify that $3$ is a square modulo $11$ and $13$ (since $6^2 \equiv 3 \pmod{11}$, $4^2 \equiv 3 \pmod{13}$) yet modulo $5$ and $7$ there are no solutions. This completes our proof.
\item Since $17 \equiv 1 \pmod{4}$ we find that $\QQ(\sqrt{17}) = \QQ(\zeta_{17})$. The primes that split completely in $\QQ(\zeta_{17})$ are precisely those $p$ for which $(p, \sqrt{17}) = 1$. These are the primes for which $(p, \QQ(\zeta_{17}) / \QQ)$ gets mapped to $0$ under the restriction map. Since $\Gal(\QQ(\zeta_{17}) / \QQ)$ is cyclic of order $16$, we see that these are exactly the primes for which $(p, \QQ(\zeta_{17}) / \QQ)$ is the square of some other morphism. We conclude that $p$ splits completely in $\QQ(\sqrt{17})$ if and only if $p$ is a square modulo $17$. And the squares modulo $17$ are $\{1, 2, 4, 8, 9 ,13, 15, 16\}$. Hence $p$ splits completely if and only if $p \equiv 1, 2, 4, 8, 9 ,13, 15, 16 \pmod{17}$ (note that $17$ ramifies, but $2$ does not therefore $2$ splits completely). 
\end{enumerate}
\end{opghand}
\end{document}
