\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\ord}{ord}
\DeclareMathOperator{\divs}{div}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\AAA}{\mathbb{A}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Elliptic Curves Homework 4}
\author{Wouter Rienks, 11007745}
\maketitle

\begin{opghand}[2] \
\begin{enumerate}[(a)]
\item Recall from the lecture that $\wp(z)$ is a meromorphic function with a pole of order $2$ at $0 \in \CC / \Lambda$, and that it is holomorphic everywhere else. It follows that $\wp'(z)$ is holomorphic at all points in $\CC / \Lambda$ except $0$ as well, and in particular it is defined in those points. Furthermore, since $\wp(z)$ is even, $\wp'(z)$ is odd. Hence if $\lambda = -\lambda \pmod{\Lambda}$, then since $\wp'(\lambda) = -\wp'(\lambda)$ we see that $\wp'(\lambda) = 0$ if it exists. Since $\wp'(\lambda)$ exists at any point outside $\Lambda$, we thus know $\wp'(\lambda) = 0$ for $\lambda \in \{\omega_1 / 2, \omega_2 / 2, \frac{\omega_1 + \omega_2}{2}\}$. 

Since we also know from the lecture that $\wp'(z)$ has a pole of order $3$ at $0$, and no other poles in $\CC / \Lambda$, it follows by the order sum formula that $\omega_1 / 2, \omega_2 / 2$ and $\frac{\omega_1 + \omega_2}{2}$ are the only zeroes of $\wp'(z)$ (with multiplicity $1$). 
\item Let $f(z) = \wp(z) - \wp(\omega_1 / 2)$. Then $f(\omega_1) = 0$ and $f'(\omega_1 / 2) = \wp'(\omega_1 / 2) = 0$. Hence $f$ has a zero of multiplicity $\geq 2$ at $\omega_1$. By repeating the argument for $\wp(z) - \wp(\omega_2 / 2)$ and $\wp(z) - \wp(\omega_1 / 2)$ we see that the function
\[
g(z) = (\wp(z) - \wp(\omega_1 / 2))(\wp(z) - \wp(\omega_2 / 2))(\wp(z) - \wp(\omega_1 / 2))
\]
has zeroes of order $2$ at $\omega_1 / 2, \omega_2 /2 , (\omega_1 + \omega_2) / 2$. Furthermore, it has a pole of order $6$ at $z = 0$ and no other poles (since $\wp(z)$ has a pole of order $2$ at $z = 0$ and no other poles). By the order sum formula it follows that we found all zeroes of $g(z)$. 

Hence the function $\wp'(z)^2 / g(z)$ has no zeroes or poles since they all cancel out. Thus the function is constant by Liouvilles theorem (as it is bounded, holomorphic as a function $\CC \to \CC$), and $\wp'(z)^2 = c g(z)$ for some $c \in \CC^*$ as required.
\end{enumerate}
\end{opghand}
\begin{opghand}[4] For brevity, we will drop the $\pmod{\Lambda}$ in our equations.
\begin{enumerate}[(a)]
\item We can rewrite the equation as
\[
\begin{pmatrix} \wp(z_1) & 1 \\ \wp(z_2) & 1 \end{pmatrix} \begin{pmatrix} a\\ b \end{pmatrix} = \begin{pmatrix} \wp'(z_1) \\ \wp'(z_2)\end{pmatrix}. 
\]
By elementary linear algebra, this has a unique solution $(a, b)$ if $\wp(z_1) - \wp(z_2) \neq 0$. Thus suppose that doesn't hold, i.e. $\wp(z_1) = \wp(z_2)$. Consider $g(z) = \wp(z) - \wp(z_1)$. Since $\wp$ is even and $\wp(z_1) = \wp(z_2)$ this function is zero on $\{z_1, z_2, -z_1, -z_2\}$. Furthermore, $g$ has a pole at $0$ and no other poles, hence at most $2$ zeroes. Since $z_1 \neq \pm z_2$ by assumption, the only way this can happen is if $z_1 = -z_1$ and $z_2 = -z_2$. But since $z_1, z_2 \neq 0 $ by 2(a) we have $g'(z_1) = g'(z_2) = 0$ as well. Hence $g$ has a double zero at $z_1$ and $z_2$, therefore $z_1 = z_2$ (since otherwise we have 4 zeroes), but we assumed $z_1 \neq z_2$.
\item Clearly $f$ has a pole of order $3$ at $0$ and no other poles. Hence $f$ has $3$ zeroes (counted with multiplicity), and they must sum to $0$. Therefore if $\alpha$ is the third zero, we have $z_1 + z_2 + \alpha = 0$, hence $\alpha = -z_1 -z_2$. 
\item Note that the assumptions all together imply that for any distinct $\alpha, \beta \in \{z_1, z_2 , z_1 + z_2\}$ we have $\alpha \neq \pm \beta$. Consider the function
\[
h(z) = 4\wp(z)^3 - (a\wp(z) + b)^2 - g_2(\Lambda)\wp(z) - g_3(\Lambda).
\] 
We claim that $h$ has a zero of order $2$ at any $\alpha \in \{z_1, z_2 , z_1 + z_2\}$ if $2\alpha = 0$, and else it has a zero at $\alpha$ and $-\alpha$. To this end, note that if $\wp'(z)^2 = (a\wp(z) + b)^2$ then $h(z) = 0$. Since $f(z_1) = f(z_2) = 0$, $h(z_1) = h(z_2) = 0$. Furthermore using that $\wp$ is even and $\wp'$ is odd, 
\[
\wp'(z_1 + z_2)^2 = (-\wp'(-z_1 -z_2))^2 = \wp'(-z_1 -z_2)^2 = (a\wp(-z_1 - z_2) + b)^2 = (a\wp(z_1 + z_2) + b)^2,
\]
hence $h(z_1 + z_2)$ is zero as well. Since $\wp$ is even, $h$ is even. Hence we may conclude that $h(\pm z_1) = h(\pm z_2) = h(\pm (z_1 + z_2)) = 0$. By similar reasoning as in 2(b), if $\lambda = -\lambda$ for some $\lambda \in \{z_1, z_2, z_1 + z_2\}$ then we have a zero of order $2$, as $h'(\lambda) = 0$ since $h'$ is odd. 

Next, consider
\[
k(z) = (\wp(z) - \wp(z_1))(\wp(z) - \wp(z_2))(\wp(z) - \wp(z_1 + z_2)),
\]
then clearly $k$ has a zero of order $2$ at any $\alpha \in \{z_1, z_2 , z_1 + z_2\}$ if $2\alpha = 0$, and else it has a zero at $\alpha$ and $-\alpha$. Since $k$ and $h$ both have a pole of order $6$ at $0$ and are holomorphic everywhere else, they have at most $6$ zeroes (counted with multiplicity). Therefore we may conclude all the zeroes of $h$ and $k$ are equal, and hence $h = c k$ for some $c \in \CC^*$. By looking at the residue in $z = 0$ we conclude $4k = h$. Hence the polynomials
\begin{align*}
p(x) &= 4x^3 - (ax + b)^2 - g_2(\Lambda)x - g_3(\Lambda) \\
q(x) &= 4(x - \wp(z_1))(x - \wp(z_2))(x - \wp(z_1 + z_2))
\end{align*}
agree on any $x$ in the image of $\wp$. Since $\wp$ is meromorphic, its image contains infinitely many points (e.g. by the open mapping theorem), hence $p = q$ as polynomials in $\CC[x]$. It follows that $4(\wp(z_1) + \wp(z_2) + \wp(z_1 + z_2)) = a^2$. Since clearly 
\[
0 - 0 = f(z_1) - f(z_2) = (\wp'(z_1) - \wp'(z_2)) - a(\wp(z_1) - \wp(z_w)),
\]
one has 
\[
a = \frac{\wp'(z_1) - \wp'(z_2)}{\wp(z_1) - \wp(z_2)},
\]
which yields the required equation.
\end{enumerate}

\end{opghand}
\end{document}
