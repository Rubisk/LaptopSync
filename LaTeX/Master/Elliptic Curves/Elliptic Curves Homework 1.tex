\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Elliptic Curves Homework 1}
\author{Wouter Rienks, 11007745}
\maketitle

\begin{opghand}[2] \
\begin{enumerate}[(b)]
\item Note that any $(x, y)$ where the line $y = \lambda x$ intersects the curve $y^2 = x^3 + 2x^2$ solves the equation $(\lambda x)^2 = x^3 + 2x^2$. We can rewrite this equation to $x^3 + 2x^2 - \lambda^2 x^2 = 0$, or $x^2(x + 2 - \lambda^2) = 0$. Hence $x = 0$ or $x = \lambda^2 - 2$. Since $y = \lambda x$ we conclude that the only intersection points are $(0, 0)$ and $(\lambda^2 - 2, \lambda(\lambda^2 - 2))$. Since $\sqrt{2} \not \in \QQ$ we see that $\lambda^2 - 2 \neq 0$ for any $\lambda \in \QQ$, hence the line always intersects the curve in one point $P_\lambda = (\lambda^2 - 2, \lambda(\lambda^2 - 2))$ that is different from $(0, 0)$. 
\end{enumerate}
\begin{enumerate}[(c)]
\item Suppose $(x_0, y_0)$ is a rational point on the curve. If $x_0 \neq 0$ then the line $y = \frac{y_0}{x_0}x$ passes through $(0, 0)$ and $(x_0, y_0)$. Hence by (b), there must exist a $\lambda \in \QQ$ such that $(x_0, y_0) = P_\lambda$ (in fact, $\lambda = \frac{y_0}{x_0}$). If $x_0 = 0$ then by the equation of the curve we find $y_0 = 0$. 

Since any point $P_\lambda$ is rational for $\lambda \in \QQ$ as well, we conclude that the set of rational points is given by 
\[
\{(0, 0)\} \cup \{ (\lambda^2 - 2, \lambda(\lambda^2 - 2)) \mid \lambda \in \QQ\}. 
\]
\end{enumerate}
\end{opghand}

\begin{opghand}[3] \
\begin{enumerate}[(b)]
\item We first see that the Jacobian matrix is given by 
\[
M = [2 Y, f'(X)]. 
\]
Note that $C$ is smooth in a point $(X, Y)$ if and only if $M$ has rank $1$ in $(X, Y)$. Therefore $C$ is not smooth in a point $(X, Y)$ if and only if $M$ has rank $0$ in $(X, Y)$. Hence $C$ is not smooth if and only if there exist $(X, Y) \in \bar{k}^2$ such that
\begin{align*}
Y^2 &= f(X), \\ 
2Y &= 0, \\
f'(X) &= 0.
\end{align*}
Since the characteristic of $k$ is not $2$, we conclude that $Y = 0$, and $f(X) = f'(X) = 0$. Hence if such $X$ exists then $f$ is not seperable by (a) with $c = X$, and if no such $X$ exists then $f$ is seperable. 
\end{enumerate}
\begin{enumerate}[(c)]
\item This follows trivially since the discriminant is known to be $4a^3 + 27b^2$, but I presume it is part of the exercise to do it by hand. 

First suppose $4a^3 + 27b^2 = 0$. If $a = 0$ then $b = 0$ as well, and $x = 0$ is a root of both $f$ and $f'$ hence $f$ is not seperable. Otherwise one easily verifies that $x = - \frac{3b}{2a}$ is a root of both, since
\begin{align*}
\(-\frac{3b}{2a}\)^3 - \frac{3b}{2} + b &= \frac{1}{8a^3}\(-27b^3  -12 ba^3 + 8ba^3\) = -\frac{b}{8a^3}\(27b^2 + 4a^3\) = 0, \\
3\(-\frac{3b}{2a}\)^2 + a &= \frac{1}{4a^2}\(27b^2 + 4a^3\) = 0.
\end{align*}
Therefore $f$ is not seperable (note that we may divide by powers of $2$ everywhere since the characteristic of $k$ is not $2$).

Conversely if $f$ is not seperable then there must be a common root $x$ of $f$ and $f'$. We find that such $x$ must satisfy $3(x^3 + ax + b) - x(3x^2 + a) = 0$, hence $3ax + 3b -ax = 0$ or $2ax + 3b = 0$. If $a = 0$ and the characteristic of $k$ is $3$, then clearly $4a^3 + 27b^2 = 0$ since $27 = 0$. If $a = 0$ and the characteristic of $k$ is not $3$ then we see from $3x^2 + a= 0$ that $x$ must be $0$, hence $b = 0$ as well (since $x^3 + ax + b = 0$) and $4a^3 + 27b^2 = 0$. Finally if $a \neq 0$ then $2a \neq 0$, hence $x = -\frac{3b}{2a}$. Since $3x^2 + a = 0$, multiplying through by $4a^2$ gives $27b^2 + 4a^3 = 0$ as required.

Hence $f$ is not seperable if and only if $4a^3 + 27b^2 = 0$. It follows that $C$ is smooth if and only if $f$ is seperable, if and only if $4a^3 + 27b^2 \neq 0$. 
\end{enumerate}
\end{opghand}
\begin{opghand}[6] \
\begin{enumerate}[(b)]
\item Note that $x \in R_v$ is a unit if and only if $x^{-1} \in R_v$. Since $v(x^{-1}) = -v(x)$ it follows that $x \in R_v$ is a unit if and only if $v(x) = 0$. 

Now consider the ideal $\frak{m} = \{x \in R_v \mid v(x) \geq 1\} \cup \{0\}$. We claim that $\frak{m}$ is the unique maximal ideal of $R_v$. Clearly $\frak{m}$ is an ideal: If $x, y \in \frak{m}$ then $v(x + y) \geq \min\{v(x), v(y)\} \geq 1$ hence $x + y \in \frak{m}$, and for $r \in R_v$ we have $v(rx) = v(r) +v(x) \geq v(x) \geq 1$ (since $r \in R_v$ one has $v(r) \geq 0$), hence $rx \in \frak{m}$. Thus suppose $\frak{a}$ is any other proper ideal in $R_v$. Then any $x \in \frak{a}$ (different from $0$) is not a unit, hence $v(x) > 0$ (as $v(x) \geq 0$ since $x \in R_v$), thus $x \in \frak{m}$. Thus $\frak{m}$ contains all proper ideals of $R_v$ and must be the unique maximal ideal. It follows that $R_v$ is a local ring.

Now if $\pi \in R_v$ satisfies $v(\pi) = 1$, then for any $x \in \frak{m}$ we have $v(x) \geq 1$. Hence $v(x / \pi) = v(x) - v(\pi) \geq 0$, therefore $x / \pi \in R_v$. It follows that $x \in (\pi)$ and thus $\pi$ generates $\frak{m}$. 
\end{enumerate}
\begin{enumerate}[(c)]
\item Suppose $\frak{a}$ is a non-zero ideal of $R_v$. Let $x \in \frak{a}$ be an element of minimal valuation, such an element exists since any set of positive integers has a minimal element. Set $i = v(x)$. If $i = 0$ then $x$ is a unit, hence $\frak{a} = R_v = (\pi^0)$. Otherwise $v(\pi^i) = i v(\pi) = i = v(x)$, hence $v(\pi^i / x) = 0$ and $\pi^i / x \in R_v$. Therefore $\pi^i = \pi^i / x \cdot x \in \frak{a}$. Since for any $y \in \frak{a} \setminus \{0\}$ one has $v(y) \geq v(x) = i$, we see that $v(y / \pi^i) \geq  0$ and $y / \pi^i \in R_v$, therefore $y \in (\pi^i)$. It follows that $(\pi^i) = \frak{a}$ as required.
\end{enumerate}
\end{opghand}
\end{document}
