\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Hom}{Hom}


% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catO}{\mathcal{O}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Number Theory Homework 7}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[10] \ 
\begin{enumerate}[a)]
\item Note that technically speaking $\Delta(B / A)$ depends on the choice of basis of $B$ over $A$, thus is not a well-defined element of $A$ (however if it is a unit or not does not depend on the choice of the basis, since it only differs by squares of units). If $A$ is a field, then any element in $A$ is a unit if and only if it is nonzero. Thus $B$ is separable over $A$ if and only if $\Delta(B / A) \neq 0$. Thus pick any $\alpha$ that generates $B / A$ (exists since $B / A$ is finite), then one has $\Delta(f^\alpha_A) = \Delta(1, \alpha, \dots, \alpha^{\deg{f^\alpha_A} - 1})$ (up to a unit), and the former is nonzero if and only if the polynomial $f^\alpha_A$ is seperable (hence the field extension $B / A$ is seperable in the `usual' sense). 
\item First suppose $B$ is seperable over $A$. Pick any $A$-basis $x_1, \dots, x_n$ of $B$, and let $T = (\Tr(x_ix_j))_{i, j = 1}^n$. Then $\Delta(B / A)$ is a unit, hence $\det(T)$ is a unit, and $T \in A^{n \times n}$ is invertible. We need to show the map $\phi: B \to \Hom_A(B, A): x \to \Tr_{B / A}(xb)$ is bijective, to do this we construct an inverse $\psi: \Hom_A(B, A) \to B$. Thus pick $\lambda \in \Hom_A(B, A)$. Define
\[
\psi(\lambda) = (x_1, \dots, x_n) T^{-1}\begin{pmatrix} \lambda(x_1) \\ \vdots \\ \lambda(x_n) \end{pmatrix}.
\]
Then $\psi$ is clearly $A$-linear, since any $\lambda \in \Hom_A(B, A)$ is $A$-linear. Furthermore, for any $j \in \{1, \dots, n\}$ we have
\begin{align*}
\psi(\phi(x_j))) &= (x_1, \dots, x_n) T^{-1}\begin{pmatrix} \Tr(x_j \cdot x_1) \\ \vdots \\ \Tr(x_j \cdot x_n) \end{pmatrix} \\
&=  (x_1, \dots, x_n)  e_j &(\text{$j$-th column nonzero}) \\
&= x_j.
\end{align*}
Since the $x_i$ form a basis we conclude $\psi \circ \phi = \id$. Similarly, for any $\lambda \in \Hom_A(B, A)$, write $(\lambda_1, \dots, \lambda^n)^T = T^{-1} (\lambda(x_1), \dots, \lambda(x_n))^T$. Then for any $j \in \{1, \dots, n\}$ one has
\begin{align*}
\phi(\psi(\lambda)))(x_j) &= \Tr_{B / A}\((x_1, \dots, x_n)(\lambda_1, \dots, \lambda_n)^T \cdot x_j\) \\
&= \Tr_{B / A} \(\sum_{i = 1}^n  x_i \lambda_i x_j  \) \\
&= \sum_{i = 1}^n \Tr_{B / A}\( x_i \cdot x_j\) \lambda_i &(\text{trace is  $A$-linear}) \\
&= (\Tr_{B / A}(x_1 \cdot x_j), \dots, \Tr_{B / A}(x_n \cdot x_j)) T^{-1}\begin{pmatrix} \lambda(x_1) \\ \vdots \\ \lambda(x_n) \end{pmatrix} \\
&= e_j\begin{pmatrix} \lambda(x_1) \\ \vdots \\ \lambda(x_n) \end{pmatrix} = x_j.
\end{align*}
Since $j$ was arbitrary we conclude $\phi \circ \psi(\lambda) = \lambda$. Thus $\phi$ is an isomorphism.

Conversely, if $\phi$ is an isomorphism, pick any $A$-basis $x_1, \dots, x_n$ of $B$. Then for any $i$ one has $x_i^* \in \Hom_A(B, A)$, hence there is $y_i \in B$ with $\phi(y_i) = x_i^*$, or $\Tr_{B / A}(y_i x_j) = \delta_{ij}$ for any $j$. Expanding the $y_i = \sum_{k = 1}^n y_{ik}x_k$ in the basis $x_1, \dots, x_n$, we find that
\begin{align*}
1 &= \det([\Tr_{B / A}(y_i x_j)]_{i,j=1}^n) = \det\(\left[\sum_{k = 1}^n \Tr_{B / A} (y_{ik} x_k x_j)\right]_{i,j=1}^n\) \\
&= \det\((y_{ij})_{i,j=1}^n\) \det\(\Tr_{B / A}(x_i x_j)_{i,j=1}^n\).
\end{align*}
In particular, $\det\(  \Tr_{B / A}(x_i x_j)_{i,j=1}^n\)$ is a unit hence $\Delta(B / A)$ is a unit.
\item If $f: A \to A'$ and $x_1, \dots, x_n$ is an $A$-basis for $B$, then one has by the bottom part of page 43 of the notes that $\Delta_{B' / A'} = \det(\Tr_{B / A}(f^*(x_1), \dots, f^*(x_n))) = f(\Delta_{B / A})$. Thus if $\Delta_{B / A}$ is a unit, then $f(\Delta_{B / A})$ is a unit (with inverse $f(\Delta_{B / A}^{-1})$, hence $\Delta_{B' / A'}$ is a unit. Hence if $B / A$ is seperable, so is $B' / A'$. 

The converse does not hold, consider $X = \ZZ \oplus 2i\ZZ$ and the extension of algebras $X / \ZZ$. Then $\{1, 2i\}$ is a $\ZZ$-basis for $X$, and
\begin{align*}
\Delta_{X / \ZZ} = \det \begin{pmatrix}\Tr_{X / \ZZ}(1) & \Tr_{X / \ZZ}(2i) \\ \Tr_{X / \ZZ}(2i) & \Tr_{X / \ZZ}(-4) \end{pmatrix}  = \det \begin{pmatrix}1 & 0 \\  0 & -4 \end{pmatrix} = -4
\end{align*}
is not a unit in $\ZZ$. Thus $X / \ZZ$ is not seperable. However under the inclusion $f: \ZZ \to \QQ$, $f(-4) = -4$ is a unit in $\QQ$, hence $X \otimes \QQ  / \QQ$ is seperable.
\end{enumerate}
\end{opghand}
\begin{opghand}[17]
First assume there is no $x \in \NN$ with $x^3 \mid d$. Let $\alpha = \sqrt[3]{d}$. Then $f^\alpha_\QQ = X^3 - d$ (since it has no rational roots if $\alpha \not \in \QQ$, thus  is irreducible). We find that $\Delta(\ZZ[\alpha]) = \Delta(f) = -27d^2$. 

Thus $\ZZ[\alpha]$ is regular above any prime $p$ with $p \nmid 3, d$. 

If $p \neq 3$, $p \mid d$ but $p^2 \nmid d$, then $X^3 - d$ factors as $X^3 \pmod{p}$. Dividing $X^3 - d$ by $X$ we end up with remainder $d$. If $p^2 \nmid d$ then by Kummer-Dedekind we see that $\ZZ[\alpha]$ is regular above $p$. Otherwise one notes that $X^3 - d = X(X^2) - d$, thus by Kummer-Dedekind $\beta := \frac{\alpha^2}{p} \in \catO_{K}$. Since $d$ is cubefree, we have $p \nmid d / p^2$. Thus since $(\beta)^2 = d\alpha / p^2$ and $p \nmid d / p^2$, we have $\ZZ[\alpha, \beta]_{(p)} = \ZZ[\beta]_{(p)}$. The minimum polynomial of $\beta$ is $X^3 - d^2 / p^2$. We see that this does not have a double root modulo $p$, since any double root with be a root of the derivative $3X^2$, which only has root $0$ (since $p \neq 3$). However $0$ is not a root since $p \nmid d^2 / p^2$. Thus $\ZZ[\beta]$ is regular above $p$. We conclude that 
\[
(\catO_K)_{(p)} = \begin{cases}
\ZZ[\alpha]_{(p)} &{p^2 \nmid d}, \\
\ZZ[\frac{\alpha^2}{p}]_{(p)} &{p^2 \mid d}.
\end{cases}
\]

Remains to compute $(\catO_K)_{(3)}$. Note that $X^3 - d$ factors as $(X - d)^3 \pmod{3}$, and that we have $X^3 - d = (X - d)(X^2 + dX + d^2) + d^3 - d$. Thus if $9 \nmid d^3 - d$, then $\ZZ[\alpha]$ is regular above $3$.  We split 4 cases
\begin{itemize}
\item If $3 \mid d$, $9 \nmid d$, then $d^3 - d$ is not divisible by $9$ hence $\ZZ[\alpha]$ is regular above $3$. 
\item If $3 \nmid d$ and $d \not \equiv \pm 1 \pmod{9}$, then $d^3 - d$ is again not divisible by $9$ hence $\ZZ[\alpha]$ is regular above $3$. 
\item If $d \equiv 1 \pmod{9}$, then by Kummer-Dedekind we have $\gamma = \frac{\alpha^2 + d\alpha + d^2}{3} \in \catO_K$. Note that $[\ZZ[\alpha, \gamma] : \ZZ[\alpha]] = 3$ since $\gamma \not \in \ZZ[\alpha]$, yet $3\gamma \in \ZZ[\alpha]$. Thus we have $-27d^2 = \Delta(\ZZ[\alpha]) = 9\Delta(\ZZ[\alpha, \gamma])$, and $9 \nmid 3 d^2 = \Delta(\ZZ[\alpha, \gamma])$. Therefore $\Delta(\ZZ[\alpha, \gamma])$ is regular above $3$, since if any $x \in \frac{1}{3} \ZZ[\alpha, \gamma] \setminus \ZZ[\alpha, \gamma]$ were in $\catO_K$, then $\bar{x}$ would have order $3$ in $\ZZ[\alpha, \gamma, x] / \ZZ[\alpha, \gamma]$, thus the index $I = [\ZZ[\alpha, \gamma, x] / \ZZ[\alpha, \gamma]]$ would be divisible by $3$. Therefore $\Delta(\ZZ[\alpha, \gamma, x]) = -3d^2 / I^2$ can't be an integer (since $3 \nmid d$), yet the discriminant is an integer for any order. Thus $\ZZ[\alpha, \gamma, x]$ is not an order which contradicts $x \in \catO_K$. 
\item If $9 \mid d$, then since $3^3 \nmid d$ by assumption, we have $d = 9m^2$ with $3 \nmid m$. Since $9 \nmid d^3 - d$, by Kummer-Dedekind we once again have $\gamma = \frac{\alpha^2 + d\alpha + d^2}{3} = \frac{\alpha^2}{3} + 3m\alpha + 27m^2 \in \catO_K$. But note that $\(\frac{\alpha^2}{3}\)^2 = \frac{d\alpha}{9} = \alpha m$. Since $3 \nmid m$ we thus have $\alpha \in \ZZ[\frac{\alpha^2}{3}]_{(3)}$, and therefore also $\gamma \in \ZZ[\frac{\alpha^2}{3}]_{(3)}$. Thus $\ZZ[\alpha, \gamma]_{(3)} = \ZZ[\frac{\alpha^2}{3}]_{(3)}$.  And the latter is regular above $3$, since the minimum polynomial of $\alpha^2 / 3$ is $X^3 - 3m^2$, which is of the form $3 \mid d, 9 \nmid d$ we discussed before (note that $m$ is cubefree since $d$ was). Thus $\ZZ[\alpha, \gamma]$ is also regular above $3$.
\end{itemize}
We conclude that
\[
(\catO_K)_{(3)} = \begin{cases}
\ZZ[\alpha]_{(3)} &{d \not \equiv 0, \pm 1 \pmod{9}}, \\
\ZZ[\alpha, \frac{\alpha^2 + d\alpha + d^2}{3}]_{(3)} &{d \equiv 0, \pm 1 \pmod{9}}. \\
\end{cases}
\]
Combining everything together, we find that if $d$ is cubefree and $p_1, \dots, p_k$ are all primes $\neq 3$ such that $p_i^2 \mid d$, then 
\[
\catO_{\QQ(\sqrt[3]{d})} = \begin{cases}
\ZZ[\alpha, \frac{\alpha^2}{p_1}, \dots, \frac{\alpha^2}{p_k}] &{d \not \equiv 0, \pm 1 \pmod{9}}, \\
\ZZ[\alpha, \frac{\alpha^2}{p_1}, \dots, \frac{\alpha^2}{p_k}, \frac{\alpha^2 + d\alpha + d^2}{3}]_{(3)} &{d \equiv 0, \pm 1 \pmod{9}}. \\
\end{cases}
\]
Finally if $d = x^3d'$ for $d'$ cubefree, then clearly $\QQ(\sqrt[3]{d}) = \QQ(\sqrt[3]{d'})$, thus $\catO_{\QQ(\sqrt[3]{d})} = \catO_{\QQ(\sqrt[3]{d'})}$, and the latter is given above. 

\end{opghand}
\end{document}
