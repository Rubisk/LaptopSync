\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Hom}{Hom}


% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\HH}{\mathbb{H}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catO}{\mathcal{O}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Number Theory Homework 8}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[5.14] 
First suppose $f$ is irreducible, and $\deg(f) > 0$ (the discriminant of a polynomial of degree $0$ is not defined!). Let $\alpha$ be any root of $f$, so that $\ZZ[\alpha] = \ZZ[X] / (f)$ and $\Delta(f) = \Delta(\ZZ[\alpha])$. Note that $\ZZ[\alpha]$ is an order since $f$ is monic. If $\ZZ[\alpha]$ has rank $1$ over $\ZZ$, then there is $x \in \ZZ[\alpha]$ and $n, m \in \ZZ$ such that $nx = \alpha, mx = 1$. Thus either $\alpha = 0$ or $\alpha = m / n$, therefore $\alpha \in \QQ$. Otherwise $\ZZ[\alpha]$ has rank $\geq 2$ over $\ZZ$, thus by Corollary 5.10 one has
\[
|\Delta(f) | = |\Delta(R)| \geq \pi^2 / 4 > 1
\]
We conclude that $|\Delta(f)| = 1$ for any irreducible polynomial $f \in \ZZ[X]$ if and only if $f$ has a rational root. By the rational root theorem $f$ must have an integral root (since $f$ is monic). Thus $f = X - k$ for some $k \in \ZZ$ since $f$ is irreducible. 

By the fundamental theorem of symmetric polynomials, the discriminant of any polynomial $g \in \ZZ[X]$ is an integer. To see this, note that if the roots of $g$ are $\alpha_1, \dots, \alpha_n$, the expression 
\[
\prod_{1 \leq i < j \leq n}(\alpha_i - \alpha_j)^2
\]
is symmetric in the $\alpha_j$, thus it can be expresed in terms of the elementary symmetric polynomials in $\alpha_1, \dots, \alpha_n$, which are precisely the coefficients of $g$. Furthermore, let $h \in \ZZ[X]$ be another polynomial with roots $\beta_1, \dots, \beta_m$. Consider the polynomial
\[
F_{hg}(X_1, \dots, X_n) = \prod_{1 \leq i \leq m, 1 \leq j \leq n}(\beta_i - X_j)^2.
\]
Its coefficients are symmetric polynomials in the $\beta_i$, hence can be expressed as polynomials in the coefficients of $h$, and are therefore integers. Thus $F_{hg} \in \ZZ[X]$. Since $F_{hg}$ is also a symmetric polynomial in the $X_i$, if we plug in the $\alpha_1, \dots, \alpha_n$ for the $X_i$ it can again be expressed as a polynomial the coefficients of $g$, thus is an integer again. We thus conclude $F_{hg}(\alpha_1, \dots, \alpha_n) \in \ZZ$. 

But then if $g, h \in \ZZ[X]$ have roots $\alpha_1, \dots, \alpha_n, \beta_1, \dots, \beta_m$ respectively, one has
\begin{align*}
\Delta(gh) &= \prod_{1 \leq i \leq n, 1 \leq j \leq n}(\alpha_i - \alpha_j)^2\prod_{1 \leq i \leq n, 1 \leq j \leq m}(\alpha_i - \beta_j)^2 \prod_{1 \leq i \leq m, 1 \leq j \leq m}(\beta_i - \beta_j)^2 \\
&= \Delta(h)\Delta(g)F_{hg}(\alpha_1, \dots, \alpha_n).
\end{align*}
Since $\Delta(h), \Delta(g)$ and $F_{hg}(\alpha_1, \dots, \alpha_n)$ are all integers, we conclude that $|\Delta(hg)| = 1$ can only happen if $|\Delta(h)| = |\Delta(g)| = 1$. Thus if $f \in \ZZ[X]$ satisfies $|\Delta(f)| = 1$, all irreducible factors $g$ must satisfy $|\Delta(g)| = 1$. 

We conclude that $f \in \ZZ[X]$ satisfies $|\Delta(f)| = 1$, that all roots of $f$ are integers. Thus $f = (X - k_1)\cdots(X - k_n)$ for some $k_1, \dots, k_n \in \ZZ$. If there are $i, j$ such that $k_i = k_j$ then clearly $\Delta(f) = 0$, and if $|k_i - k_j| \geq 2$ for some $i, j$ then obviously $|\Delta(f)| \geq 2$. We conclude that all $|k_i - k_j| = 1$ for all $i \neq j$, and therefore $f$ must clearly be of the specified form.
\end{opghand}
\begin{opghand}[5.20] 
Since $R \subseteq A$ clearly $R^* \subseteq A^* \cap R$, since if $x \in R^*$ one has $x^{-1} \in R$, thus $x^{-1} \in A$ and $x \in A^*$. Conversely, let $x \in A^* \cap R$. Then $x^{-1} \in A$, and since $A$ is integral over $R$ there are $a_1, \dots, a_n \in R$ such that
\[
(x^{-1})^n = a_1(x^{-1})^{n - 1} + \dots + a_{n - 1}(x^{-1}) + a_n.
\]
Thus
\[
x^{-1} = a_1 + a_2x + \dots + a_{n - 1}x^{n - 2} + a_nx^{n - 1},
\]
and the righthandside is in $R$ since $x \in R$. Thus $x^{-1} \in R$ and $x \in R^*$ as required.

The statement does not hold true if $A$ is not integral over $R$, for example take $R = \ZZ$ and $A = \QQ$, then $R^* = \{\pm 1\}$ yet $A^* \cap R = \ZZ \setminus \{0\}$. 
\end{opghand}
\end{document}
