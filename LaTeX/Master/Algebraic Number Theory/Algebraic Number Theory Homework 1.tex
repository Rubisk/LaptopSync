\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Number Theory Homework 1}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[7]
Let $R$ be a domain and $g$ an Euclidian function on $R$. Pick any ideal $I \subseteq R$. If $I = \{0\}$ then $0$ generates $I$ hence $I$ is a principal ideal. Otherwise, let $n \in \ZZ_{>0}$ be the least $n$ such that there exists $a \in I$ such that $g(a) = n$. Such $n$ exists since there exists $a \neq 0$ in $I$ hence $g(I) \cap \ZZ_{> 0}$ is nonempty, and any subset of the positive integers has a least element. Let $a$ be such that $g(a) = n$. We show that $(a) = I$, clearly $(a) \subseteq I$. Thus pick $b \in I$, we wish to show $b \in (a)$. By the property of $g$ there exist $q, r \in R$ such that $b = qa + r$ and either $r = 0$ or $g(r) < g(a)$. Since $r = qa - b \in I$, we must have $r = 0$, since otherwise $0 < g(r) < g(a)$, contradicting our assumption that $n$ was minimal. Therefore $b = qa \in (a)$ as desired, and $I = (a)$. Thus $R$ is a principal ideal domain.
\end{opghand}
\begin{opghand}[31] \ 
\begin{enumerate}[a.] 
\item Set 
\[
I := \{b \in B \mid ab \in B \quad \forall a \in A\}.
\]
Note that $I$ is an ideal since $B$ is an abelian group and if $b \in I$, then $a'(ab) = (a'a)b \in I$ for all $a' \in I$, hence $ab \in I$ for all $a \in A$ as required. Now suppose $A / I$ were infinite. Then we could find an infinite sequence $(b_i)_{i = 1}^\infty$ with $b_i \in B$ and $b_i + I \neq b_j + I$ for any $i \neq j$. Since $A / B$ is finite, we can also fix a finite (and minimal) set $\{a_1, \dots, a_k\} \subseteq A$ such that any coset in $A / B$ is of the form $a_i + B$. For any $b \in B$, define $f_b: [k] \to [k]$ by letting $f_b(i)$ be the unique $j \in [k]$ such that $a_i\cdot b + I = a_j + I$. Since there are at most $k^k < \infty $ functions $[k] \to [k]$, there exist $i, j \in \NN, i \neq j$ such that $f_{b_i} = f_{b_j}$. This implies that $a_\ell(b_i - b_j) \in I$ for all $\ell \in [k]$. But that implies that $a(b_i - b_j) \in B$ for any $a \in A$ (since $I \subseteq B$), and therefore $b_i - b_j \in B$, which contradicts $b_i + I \neq b_j + I$. 

\item The inclusion $B^* \subseteq A^* \cap B$ is trivial, to show the converse fix some $a \in A^* \cap B$. Consider the action of $a^{-1}$ on $A / I$ by $a^{-1}(b + I) = a^{-1}b + I$. As $A / I$ is a finite set, there must exist some $k \geq 2$ such that $(a^{-1})^k$ acts trivially on $A / I$. In particular this implies that $a^{-k}(a + I) = a + I$, thus $a^{1-k} \in B$ (as $a + I \subseteq B$). But also $a \in B$, thus since $B$ is a subring we have $a^{k - 2} \in B$ and finally $a^{k - 2} \cdot a^{1 - k} = a^{-1} \in B$. Therefore $a \in B^*$ as required. We conclude $B^* = A^* \cap B$. 

To show $A^* / B^*$ is finite, suppose it were not. Then we could find an infinite sequence \linebreak $(a_i)_{i = 1}^\infty \subseteq A^*$ with $a_ia_j^{-1} \not \in B^*$ for any $i \neq j$. As $a_ia_j^{-1} \in A^*$ for any $i, j$ we may even assume $a_ia_j^{-1} \not \in B$ for any $i \neq j$, since $B^* = A^* \cap B$. Since $A / I$ is finite, there exist $i, j \in \NN, i \neq j$ with $a_i + I = a_j + I$. But that implies $a_ia_j^{-1} + I = 1 + I$. Since $B$ is a subring, $1 \in B$. Thus $1 + I \subseteq B$. But that would imply $a_ia_j^{-1} \in B$, yet we assumed $a_ia_j^{-1} \not \in B$ for any $i \neq j$. 
\end{enumerate}
\end{opghand}
\end{document}
