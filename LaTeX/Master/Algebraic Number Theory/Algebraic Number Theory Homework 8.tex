\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Hom}{Hom}


% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\HH}{\mathbb{H}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catO}{\mathcal{O}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Number Theory Homework 8}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[5] 
First suppose $L = \ZZ x_1 \oplus \dots \oplus \ZZ x_n \subseteq \RR^n$ is a lattice. Consider the linear map given by $M: \RR^n \to \RR^n: e_i \mapsto x_i$. Note that the columns of (the matrix) $M$ are simply the vectors $x_i$, since they are linearly independent $M$ is invertible. Clearly $L = M^{-1}(\ZZ^n)$, it thus suffices to show the latter is discrete. Thus pick $\alpha = M^{-1}(a_1, \dots, a_n) \in L$ for some $a_i \in \ZZ$, we need to show there exists an open set $U \subseteq \RR^n$ such that $U \cap L = \{\alpha\}$. Write $N = M^{-1}$, then since $N$ is invertible $Nx = 0$ implies $x = 0$ for any $x \in \RR^n$. Therefore if $||x|| = 1$, $||Nx|| \neq 0$. Since the map $f: \RR^n \to \RR^n: x \mapsto ||Nx||$ is continuous and the unit disk is compact, $f$ attains a minimum $m > 0$ on the unit disk $\{x \in \RR^n \mid ||x|| = 1\}$. We claim that $U := \{\gamma \in \RR^n \mid ||\gamma- \alpha|| < m\}$ is a sufficient open set. Clearly $\alpha \in U$ as $m > 0$. Now suppose $\beta = M^{-1}(b_1, \dots, b_n) \in U$ for some $b_i \in \ZZ$. Denote $\vec{a} = (a_1, \dots, a_n)$ and similarly $\vec{b} = (b_1, \dots, b_n)$. Then let $\vec{c} = \vec{b} - \vec{a}$, by construction one has $||M^{-1} \vec{c}|| = ||\beta - \alpha||< m$. Thus $||\vec{c}|| < 1$, since $M^{-1}\frac{\vec{c}}{||\vec c||} \geq m$ per choice of $m$. However all entries of $\vec{c}$ are integers, thus the only way for $||\vec{c}||$ to be less than $1$ is if $\vec{c} = 0$. Therefor $\beta = \alpha$, and $U \cap L = \{\alpha\}$ as required.

Now suppose $L \subseteq \RR^n$ is discrete. Denote $\RR \cdot L$ for the linear subspace spanned by $L$, and fix an $\RR$-basis $\{x_1, \dots, x_k\} \subseteq L$ of $\RR \cdot L$. Consider the compact subset
\[
B = \left \{\sum_{i = 1}^n t_i x_i \mid t_i \in [0, 1]\right\} \subseteq \RR \cdot L.
\]
Since $L$ is discrete and $B$ compact, $B \cap L$ contains only finitely many points $y_1, \dots, y_\ell$. We claim that the set $\{x_1, \dots, x_k, y_1, \dots, y_\ell\}$ generates $L$. Indeed, if $z \in L$ then $z \in \vspan_\RR\{x_1, \dots, x_k\}$ thus one can write $z =\alpha_1x_1 + \dots + \alpha_kx_k$ for certain $\alpha_i \in \RR$. Set $a_i = \lfloor \alpha_i \rfloor$ and $b_i = \alpha_i - a_i$. Then $b_i \in [0, 1)$, hence $b_1x_1+ \dots + b_kx_k \in B$, thus is equal to some $y_j$. Therefore $z = a_1x_1 + \dots + a_k x_k + y_j$, which proofs the claim.

By the classification of finitely generated abelian groups, there exist $n_i$ and $m \in \ZZ$ and an isomorphism 
\[
L \cong \ZZ^m \oplus (\ZZ / n_1 \ZZ) \oplus \dots \oplus (\ZZ / n_t \ZZ).
\]
However $L$ is clearly torsion-free, since it is a subgroup of the (additive) group $\RR^n$ which is torsion-free. Thus there is an $m \in \NN$ and an isomorphism $\varphi: L \to \ZZ^m$. If we redefine $x_i = \varphi^{-1}(e_i)$, then we thus have $L = \ZZ x_1 \oplus \dots \oplus \ZZ x_m$. To show $L$ is a lattice it thus suffices to show the $x_i$ are linearly independent (over $\RR$!).

Thus suppose we have some equation $a_1x_1 + \dots + a_\ell x_\ell = 0$ (with $a_i \in \RR$). By taking $\ell$ to be minimal, relabeling the $x_i$ such that $a_1 \neq 0$ and multiplying by $a_1^{-1}$ we may assume $x_2, \dots, x_\ell$ are linearly independent and $a_1 = 1$. If after this all $a_i$ are in $\QQ$, then by multiplying with the product of the denominators we obtain an equation over $\ZZ$. However since $L$ was a direct sum of $\ZZ$-modules such an equation can't exist. Thus without loss of generality we may assume $a_2 \not \in \QQ$. Therefore the map
\[
\ZZ \to (\RR / \ZZ): t \mapsto \overline{t a_2}
\]
is injective since if $t_1a_2 - t_2 a_2 = k$ for some $k \in \ZZ$, then there $a_2 = k / (t_1 - t_2) \in \QQ$. But for any $t \in \ZZ$, note that $tx_1 = -t(a_2x_2 + \dots + a_nx_n)$. If we set $b_{i, t} = ta_i - \lfloor ta_i\rfloor$, then $b_{i, t} \in [0, 1)$ and
\[
v_{t} := \sum_{i = 2}^\ell b_{i, t} x_i = \sum_{i = 2}^\ell ta_ix_i - \lfloor ta_i\rfloor x_i = -x_1 + \sum_{i = 2}^\ell \lfloor ta_i\rfloor x_i  \in L.
\]
Setting 
\[
B' = \left \{\sum_{i = 2}^\ell t_i x_i \mid t_i \in [0, 1]\right\} \subseteq \RR \cdot L.
\]
then $v_{t} \in B'$ for all $t \in \ZZ$. If $v_t = v_{t'}$ for some $t \neq t'$, then by linear independence of the $x_2, \dots, x_\ell$ we have $b_{2, t} = b_{2, t'}$, however that implies that $\overline{ta_2} = \overline{t'a_2}$ which we have shown to be impossible. Therefore all $v_t$ are distinct, since $v_t \in L$ for all $t \in \ZZ$ we conclude $L$ contains infinitely many points in $B'$. This contradicts compactness of $B'$, we therefore conclude $x_1, \dots, x_n$ are linearly independent over $\RR$.

Finally consider the case $n = 1$. If $L \subseteq \RR$ is a lattice we are done. Otherwise $L$ is not discrete by the above, thus there is some $a \in L$ such that $U_\varepsilon = \{b \in L: |b - a| < \varepsilon \} \neq \{a\}$ for all $\varepsilon > 0$. We need to show $L$ is dense in $\RR$, thus pick $x \in \RR$ and $\delta > 0$. Since $U_\delta \neq \{a\}$, there is $b \in L, b \neq a$ such that $|b - a| < \delta$. Set $c = b - a$ if $b > a$ and $c = a - b$ otherwise, then $c \in L \cap (0, \delta)$. Now $\lfloor x / c \rfloor c \in L$, and $x \geq \lfloor x  / c \rfloor c \geq  x - c > x - \delta$, thus $\lfloor x / c \rfloor c \in (x - \delta, x + \delta)$ which shows $L$ is dense in $\RR$. 
\end{opghand}
\begin{opghand}[12] \
\begin{enumerate}[a.]
\item If $p = 2$ the statement holds since $u = 0, v = 1$ suffices. Recal that if $x$ and $y$ are not a square modulo $p$, then $xy$ is (this follows from the multiplicativity of the Legendre symbol). If $-1$ is a square modulo $p$, that is there is $n \in \ZZ$ such that $n^2 \equiv -1 \pmod{p}$, then $(u, v) = (0, n)$ suffices. Otherwise, let $k \in \{1, \dots, p - 1\}$ be such that $-k$ is a square modulo $p$, but $-(k - 1)$ is not a square modulo $p$. Such a $k$ exist since $-(p - 1) \equiv 1^2 \pmod{p}$ is a square modulo $p$ (therefore to find such a $k$, one keeps trying $k = 1, k = 2, k = 3, \dots$ until $-k$ is a square modulo $p$, then by assumption $-i$ is not a square for all $i < k$ hence in particular $-(k - 1)$ is not a square).  Since neither $-1$ nor $-(k - 1)$ are squares modulo $p$, $(k - 1)$ is a square. In other words, there is $u \in \ZZ$ such that $u^2 \equiv k - 1\pmod{p}$. And by choice of $k$ there is $v  \in \ZZ$ such that $v^2 \equiv -k \pmod{p}$. Thus $u^2 + v^2 + 1 = (k - 1) - k + 1 \equiv 0 \pmod{p}$. This shows the first part.

For the second part, note that we can write
\begin{align*}
L_{u, v} &= \{(a, b, c, d) \in \ZZ^4 \mid \exists \ell, k: c - (ua + vb) = kp, d - (ub - va) = \ell p\} \\
&= \{(a, b, ua + vb + kp, ub - va + \ell p) \mid (a, b, k, \ell) \in \ZZ^4\} \\
&= \ZZ \begin{pmatrix}1 \\ 0 \\ u \\ -v\end{pmatrix} + \ZZ \begin{pmatrix}0 \\ 1 \\ v \\ u\end{pmatrix}  + \ZZ \begin{pmatrix}0 \\ 0 \\ p \\ 0\end{pmatrix}  + \ZZ \begin{pmatrix}0 \\ 0 \\ 0 \\ p\end{pmatrix}.
\end{align*}
Therefore
\[
\text{covol}(L_{u, v}) = \text{vol}\(\RR^2 / L_{u, v}\) = \det\begin{pmatrix}1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\ u & v & p & 0 \\ -v & u & 0 & p\end{pmatrix} = p^2,
\]
as desired.
\item Clearly $1 = 1^2 + 0^2 + 0^2 + 0^2$ is the sum of four squares. Now suppose $p$ is prime, pick $u, v \in \ZZ$ such that $u^2 + v^2 + 1 \equiv 0 \pmod{p}$. The disk $D = \{x \in \RR^4 \mid ||x|| \leq \sqrt{2p}\}$ has volume $\text{vol}(D) = \frac{\pi^2}{2}4p^2 = 2\pi^2p^2$. Since $2\pi^2 p^2 \geq 2^4 p^2$, by Minkowski's theorem there is a lattice point $ 0 \neq \alpha \in L_{u,v}$ such that $||\alpha|| < \sqrt{2p}$. Now note that writing $\alpha = (a, b, ua + vb + kp, ub - va + \ell p)$ for some $(a, b, k, \ell) \in \ZZ^4$ one clearly has $||\alpha||^2 \in \ZZ$, and 
\[
||\alpha||^2 \equiv a^2 + b^2 + (ua + vb)^2 + (ub - va)^2 \equiv (u^2 + v^2 + 1)(a^2 + b^2) \equiv 0 \pmod{p}.
\]
On the other hand $\alpha \in D$ hence $0 < ||\alpha||^2 < 2p$. Thus $||\alpha||^2 = p$ (since it is divisible by $p$), and $p = a^2 + b^2 + (ua + vb)^2 + (ub - va)^2$ is the sum of four squares. Since $p$ was an arbitrary prime, we conclude any prime is the sum of 2 squares.

Finally, note that for any $a + bi + cj + dk \in \HH$ one has $||a + bi + cj + dk|| = a^2 + b^2 + c^2 + d^2$. And if $x, y \in \ZZ[i, j, k] \subseteq \HH$, then clearly $xy \in \ZZ[i, j, k]$ (I have done this in my introductory algebra class once and will omit the two required calculations here, one simply expands the parantheses). Therefore if $n, m \in \ZZ$ can be written as the sum of four squares, say $n = a_0^2 + b_0^2 + c_0^2 + d_0^2$ and similarly $m = a_1^2 + b_1^2 + c_1^2 + d_1^2$, set $x = a_0 + b_0i + c_0j + d_0k$ and $y =  a_1 + b_1i + c_1j + d_1k$. Then writing $xy = a_2 + b_2i + c_2j + d_2k \in \ZZ[i, j, k]$, by multiplicativity of the norm we have $a_2^2 + b_2^2 + c_2^2 + d_2^2 = nm$. Thus if $n, m \in \ZZ$ can be written as the sum of four squares, so can $nm$.

The statement now follows  by induction on the number of (possibly repeated) prime factors, since any $n > 1$ can be written as a product of finitely many primes.
\end{enumerate}
\end{opghand}
\end{document}
