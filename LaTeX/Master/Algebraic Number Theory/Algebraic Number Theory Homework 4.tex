\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Number Theory Homework 4}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[8]
We first show any prime $\frak{q} \subseteq B_{\frak{p}}$ contains $\frak{p}$. The contraction $\frak{q} \cap A$ is a prime ideal in $A$. If $\frak{q} \cap A = \{0\}$ then $\frak{q} \cap \ZZ = \{0\}$, but we know $\frak{q} \cap B$ is nonzero since $\frak{q} \subseteq B_{\frak{q}}$ is a prime thus nonzero by assumption. Since nonzero ideal in a number ring contains some prime number $p$, $\frak{q} \cap B$ contains a prime number $p$, and thus $p \in \frak{q} \cap A$ as well. Now note that $(A \setminus \frak{p})^{-1} \frak{q} \subseteq \frak{q}$, hence $\frak{q} \cap (A \setminus \frak{p}) = \emptyset$ since otherwise $1 \in \frak{q}$, but $\frak{q} \cap A$ is a prime ideal. Thus $\frak{q} \cap A \subseteq \frak{p}$, since both are primes in $A$ they are maximal ideals, hence $\frak{q} \cap A = \frak{p}$ and $\frak{p} \subseteq \frak{q}$. 

Thus any prime $\frak{q} \subseteq B_{\frak{p}}$ corresponds is an extension of a $\frak{p}$. It corresponds to the extension $\frak{q} \cap B$ of $\frak{p}$ in $B$. More formally, we have a bijection
\begin{align*}
\{\frak{q} \subseteq B \text{ prime with } \frak{p} \subseteq \frak{q} \} &\leftrightarrow \{\frak{q} \subseteq B_{\frak{p}} \text { prime}\} \\
\frak{q} &\rightarrow (A \setminus \frak{p})^{-1} \frak{q} \\
\frak{q} \cap B &\leftarrow \frak{q}
\end{align*}
By our first remark the maps are actually well-defined between the two sets. To see they are a bijection, we verify both compositions are the identity. For any $\frak{q} \subseteq B$ prime with $\frak{p} \subseteq \frak{q}$, one has clearly $\frak{q} \subseteq \((A \setminus \frak{p})^{-1} \frak{q}\) \cap B$. The latter is a prime ideal in $B$ containing $\frak{q}$, since $\frak{q}$ and $B$ is maximal is nonzero it must be maximal hence equal to $\frak{q}$. For the other direction we need to show $(A \setminus \frak{p})^{-1}(\frak{q} \cap B) = \frak{q}$ for any $\frak{q} \subseteq B_{\frak{p}}$. Since $(A \setminus \frak{p})^{-1} \subseteq B_{\frak{p}}$ and $\frak{q}$ is a $B_{\frak{p}}$ ideal, one obviously has $(A \setminus \frak{p})^{-1}(\frak{q} \cap B) \subseteq \frak{q}$. The other inclusion follows since any $x \in \frak{q}$ can be written $b / a$ with $b \in B$ and $a \in A \setminus \frak{p}$. Then $b \in \frak{q} \cap B$ and thus $b / a \in (A \setminus \frak{p})^{-1}(\frak{q}$ as required.

Thus any prime $\frak{q} \subseteq B_{\frak{p}}$ corresponds to an extension of $\frak{p}$ in $B$. There are only finitely many of these extensions, since $B / B\frak{p}$ is finite ($B$ is a number ring) and there is a bijection of ideals $\frak{q} \subseteq B$ containing $\frak{p}$ and ideals in $B / B\frak{p}$. Therefore there are only finitely many primes $\frak{q} \subseteq B_{\frak{p}}$, thus the latter is a semi-local number ring.
\end{opghand}
\begin{opghand}[15]
Suppose $\ZZ[\sqrt[3]{d}]$ is not a Dedekind domain. Then there exists a singular prime $\frak{p} \subseteq \ZZ[\sqrt[3]{d}]$. Let $p$ be a prime number with $\frak{p} \subseteq (p)$. By the Kümmer-Dedekind Theorem, the polynomial $X^3 - d$ then has a factor with multiplicity $\geq 2$ in its factorization in $\FF_p[X]$. Since $X^3 - d$ has degree $3$, we conclude it must have a double root $\pmod{p}$. But any such root also is a root of the derivative $3X^2$. If $p \neq 3$ then the only root of the derivative is $0$. Thus for $0$ to also be a root of $X^3 - d \pmod{p}$ we must have $p \mid d$. But then $X^3 - d$ factors as $X^3 \pmod{p}$. Now $X^3 - d = X(X^2) - d$, hence the remainder after dividing out the factor is $d$. If $d$ is squarefree, it has at most one factor $p$, and in particular $p \nmid d$, thus $\frak{p}$ is regular.

Thus any singular prime $\frak{p}$ must contain $(3)$, and $X^3 - d$ must have a double root $\pmod{3}$. But obviously $X^3 - d \equiv (X - d)^3 \pmod{3}$. We see that $X^3 - d = (X - d)(X^2 + dX + d^2) + d^3 - d$, thus for $\frak{p} = (3, \sqrt[3]{d} - d)$ to be singular we must have $3^2 \mid d^3 - d = (d - 1)d(d + 1)$. Since at most one of $\{d, d - 1, d + 1\}$ is divisible by $3$, we may assume $9 \mid d$ or $9 \mid d - 1, 9 \mid d + 1$. Since $d$ is squarefree, we must have $9 \mid d - 1$ or $9 \mid d + 1$. And conversely, if $9 \mid d - 1$ or $9 \mid d + 1$ then $9 \mid d^3 - d$ (the remainder after dividing $X^3 - d$ by $(X - d)$), thus by the Kümmer-Dedekind theorem $\ZZ[\sqrt[3]{d}]$ is not a Dedekind domain. 

We see that if $\ZZ[\sqrt[3]{d}]$ is not Dedekind the singular prime ideal is $(3, \sqrt[3]{d} - d)$ if $d$ is squarefree. And if $d$ is not squarefree, then for any prime $p$ with $p^2 \mid d$ the prime ideal $(p, \sqrt[3]{d})$ is singular since $X^3 - d$ has a double factor $X \pmod{p}$, yet if we divide $X^3 - d$ with remainder by $X$ the remainder ($d$) is divisible by $p^2$. 
We conclude that $\ZZ[\sqrt[3]{d}]$ is not a Dedekind domain if and only if $d$ is not squarefree, $9 \nmid d + 1$ or $9 \nmid d - 1$. If it is not Dedekind the singular primes are of the form $(3, \sqrt[3]{d} - d)$ if $9 \mid d - 1$ or $9 \mid d + 1$, and of the form $(p, \sqrt[3]{d})$ for any prime $p$ with $p^2 \mid d$. 
\end{opghand}
\end{document}
