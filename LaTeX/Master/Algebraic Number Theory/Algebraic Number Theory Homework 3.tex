\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Number Theory Homework 3}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[33]
We first show that $M$ is a sub-$\ZZ$-module. If $\frac{a}{b} \in M$ and $b$ is square free, then $b$ divides the denominator of $\frac{ac}{b}$ for any $c \in \ZZ$, hence $c \cdot \frac{a}{b}$ remains in $M$. And if $\frac{a}{b}, \frac{c}{d} \in M$, then $\frac{ad + bc}{bd}$ is also squarefree in lowest terms, since any prime $p$ can divide $bd$ at most twice ($b$ and $d$ are squarefree), but in that case $p \mid ad$ and $p \mid bc$ hence in lowest terms the denominator will have at most one factor $p$.

For any prime $p$, the localization
\[
S_p^{-1}M = \left\{ \frac{a}{bd} \in \QQ \mid a, b, d \in \ZZ, b \text{ squarefree }, p \nmid d\right \} = S_p^{-1} \(\frac{1}{p}\ZZ\)
\]
is a principal $S_p^{-1}\ZZ$-ideal.

Clearly $M$ is not an invertible $\ZZ$-ideal, since otherwise by Theorem 2.14 there would only be finitely many localizations for which $S_p^{-1} M$ is not generated by $(1)$, but $S_p^{-1}M$ is not generated by $(1)$ for any prime $p$ and there are infinitely many primes. And there is no other subring for which $M$ is an invertible $R$-ideal either, since any subring $R \subseteq \QQ$ must contain $1$, thus contains $\ZZ$. And if it contains some element in $\frac{a}{b} \in \QQ \setminus \ZZ$ then $\frac{a^2}{b^2} \in R$ has a denominator that is not squarefree. Thus $\frac{a^2}{b^2} \not \in  M$, and $M$ is not an $R$-ideal (since $\frac{a^2}{b^2} \in R, 1 \in M$). 

Now fix a prime number $q$. For any prime number $p \neq q$, the localization
\[
S_p^{-1} M_q = \left\{\frac{a}{b} \bigg | p \nmid b\right \} = S_p^{-1} \(\ZZ\)
\]
is a principal $S_p^{-1} \ZZ$-ideal. And if $p = q$ then 
\[
S_q^{-1} M_q = \QQ = S_q^{-1} \(1, q^{-1}, q^{-2}, \dots \) \ZZ
\]
is clearly not a principal $S_p^{-1} \ZZ$-ideal. However, if $R = M_q$ then $M_q$ is an $R$-ideal (since $M_q$ is a ring!), and clearly satisfies $M_q^2 = R$, thus is an invertible $R$-ideal.
\end{opghand}
\begin{opghand}[36]
Since $2I \subseteq R$ by definition $I$ is a fractional $R$-ideal. It is not integral, since $\frac{1 + \sqrt{-3}}{2}$ is in $I$ but not in $R$, thus $I \not \subseteq R$. 
We compute
\[
I^3 = x^3R = \(\frac{1 + \sqrt{-3}}{2}\)^3R = \(\frac{-2 + 2\sqrt{-3}}{4}\)\(\frac{1 + \sqrt{-3}}{2}\)R = \frac{-8}{8}R = R.
\]
Thus $I$ is an invertible ideal (since $I^2 \cdot I = R$). We see that $\mathcal{I}(R)$ has a nontrivial element of finite order ($I$ has order $3$), thus its torsion subgroup is nontrivial. Thus $R$ cannot be a Dedekind domain, since the group $\bigoplus_{\mathfrak{p}} \ZZ$ has trivial torsion subgroup, thus cannot be isomorphic to $\mathcal{I}(R)$. 

Now let us consider the ideal $J = I + I^2 = \(x, \sqrt{-3}\)$. Using the obvious equality $Q(\ZZ[\sqrt{-3}]) \cong \QQ(\sqrt{-3})$, we see that any $\frac{a + b\sqrt{-3}}{c} \in J^{-1}$ must satisfy $\frac{a}{c} \sqrt{-3} -3 \frac{b}{c} \in \ZZ[\sqrt{-3}]$. Therefore $\frac{a}{c}$ must be integer, and $\frac{b}{c}$ is of the form $\frac{k}{3}$ for some $k \in \ZZ$. Thus 
\[
J^{-1} \subseteq \{m + \frac{k}{3} \sqrt{-3} \mid m, k \in \ZZ\}.
\]
But any element in $J$ must also satisfy $\(m + \frac{k}{3} \sqrt{-3}\)\(\frac{1 + \sqrt{-3}}{2}\) = \frac{m - k}{2} + \frac{k + 3m}{6}\sqrt{-3} \in \ZZ[\sqrt{-3}]$. Thus $\frac{k + 3m}{6}$ is integer and $\frac{k}{3} \in \ZZ$, and $\frac{k - m}{2}$ is integer so $k$ and $m$ are both odd, or both even. Thus $J^{-1} \subseteq \ZZ[\sqrt{-3}](1 + \sqrt{-3})$. But then $J^{-1} \cdot J \subseteq (1 + \sqrt{-3})J = (2, 1 + \sqrt{-3})$, and this last ideal is a strict ideal of $R$. To see this, consider
\[
\varphi: R \to \ZZ / 2\ZZ: (a + b\sqrt{-3}) \to \overline{a + b},
\]
clearly $\varphi$ is a ringhomomomorphism, surjective and yet $\ker(\varphi) = (2, 1 + \sqrt{-3})$. Since $\varphi$ is surjective its kernel cannot be $R$, so $(1 + \sqrt{-3}, 2)$ is a strict $R$-ideal. We conclude that $R \not \subseteq J^{-1} \cdot J$, thus $J$ is not invertible.

\end{opghand}
\end{document}
