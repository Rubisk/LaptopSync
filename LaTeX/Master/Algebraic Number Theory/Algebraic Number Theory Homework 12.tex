\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Hom}{Hom}


% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\HH}{\mathbb{H}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catO}{\mathcal{O}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Number Theory Homework 12}
\author{Wouter Rienks, 11007745}
\maketitle
Consider the polynomial 
\[
f(x) = x^5 - 9x -6.
\]
Note that it is Eisenstein bij $3$ hence irreducible. The reason for choosing this polynomial is that  it has $3$ real roots (I checked this using my calculator, I assume we may use such a tool as well), therefore its Galois group contains a two-cycle (complex conjugation). Since it is irreducible the Galois group contains an element of order $5$, therefore the Galois group is $S_5$. Thus there is no radical expression for the roots of $f$ (which makes things interesting in my opinion).

Denote $K = \QQ[X] / (f)$, $n = \deg(f) = 5$. We start by computing the discriminant of $f$. Denote $a_k$ for the $k$-the coefficient of $f$. Recall from Exercise 4.19 that if $\alpha_1, \dots, \alpha_n$ are the roots of $f$ in $\bar{\QQ} \subseteq \CC$, then if one sets 
\[
p_k = \sum_{i = 1}^{n} \alpha_i^k,
\]
one has $\Delta(f) = \det(p_{i + j - 2})_{i, j = 1}^n$. We compute this determinant, using that we can compute the $p_k$ as inductively as 
\[
p_k = -\frac{1}{a_0}\(\sum_{j = 1}^{k - 1} a_j p_{k - j} + ka_k\).
\]
We find the power sums (see (A) in sage):
\begin{center}
\begin{tabular}{|l|l|} \hline
$k$ & $p_k$ \\ \hline \hline
$0$ & $5$ \\ \hline
$1$ & $0$ \\ \hline
$2$ & $0$ \\ \hline
$3$ & $0$ \\ \hline
$4$ & $36$ \\ \hline
$5$ & $30$ \\ \hline
$6$ & $0$ \\ \hline
$7$ & $0$ \\ \hline
$8$ & $324$ \\ \hline
\end{tabular}


\end{center}
And the determinant  (see (B) in sage) equals $\Delta(f) = -11066544 = -2^4 3^4 8539$.

We continue to find $\catO_K$, which contains $\ZZ[\alpha] := \ZZ[X] / (f)$. Note that the only primes whose square divides the discriminant are $2$ and $3$. We can easily factor $f$ modulo these primes by hand
\begin{align*}
f \equiv (x - 1)^4 x \pmod{2} \\
f \equiv x^5 \pmod{3}
\end{align*}
After dividing $f$ with remainder by $x$ we are left with $-6$, and after dividing $f$ with remainder by $x - 1$ we are left with $-14$. Since neither $2^2$ nor $3^2$ divides either, we conclude that $\catO_K = \ZZ[\alpha]$ (unfortunately we lucked out but I won't restart for time reasons, let's hope the rest doesn't become too easy). 

Note that $f$ has $3$ real embeddings and $2$ complex embeddings. We approximate the Minkowski bound (see (C))
\[
M_K = \(\frac{4}{\pi}\) \frac{5!}{5^5} \sqrt{11066544} \approx 162.647484372506.
\]
To compute the class group it thus suffices to find all prime ideals of norm at most $162$. Using Sage we factor $f$ modulo all primes up to 162 (see (D)). 
\begin{center}
\begin{tabular}{ll}
$2$ & $\left[\left(y, 1\right), \left(y + 1, 4\right)\right]$ \\
$3$ & $\left[\left(y, 5\right)\right]$ \\
$5$ & $\left[\left(y + 2, 1\right), \left(y^{2} + 4 y + 1, 1\right), \left(y^{2} + 4 y + 2, 1\right)\right]$ \\
$7$ & $\left[\left(y + 4, 1\right), \left(y + 6, 1\right), \left(y^{3} + 4 y^{2} + 6 y + 5, 1\right)\right]$ \\
$11$ & $\left[\left(y^{2} + 8 y + 3, 1\right), \left(y^{3} + 3 y^{2} + 6 y + 9, 1\right)\right]$ \\
$13$ & $\left[\left(y^{2} + 4 y + 9, 1\right), \left(y^{3} + 9 y^{2} + 7 y + 8, 1\right)\right]$ \\
$17$ & $\left[\left(y^{2} + 12 y + 14, 1\right), \left(y^{3} + 5 y^{2} + 11 y + 2, 1\right)\right]$ \\
$19$ & $\left[\left(y^{5} + 10 y + 13, 1\right)\right]$ \\
$23$ & $\left[\left(y + 6, 1\right), \left(y^{4} + 17 y^{3} + 13 y^{2} + 14 y + 22, 1\right)\right]$ \\
$29$ & $\left[\left(y + 24, 1\right), \left(y^{2} + 13 y + 9, 1\right), \left(y^{2} + 21 y + 4, 1\right)\right]$ \\
$31$ & $\left[\left(y + 22, 1\right), \left(y^{4} + 9 y^{3} + 19 y^{2} + 16 y + 11, 1\right)\right]$ \\
$37$ & $\left[\left(y + 3, 1\right), \left(y^{4} + 34 y^{3} + 9 y^{2} + 10 y + 35, 1\right)\right]$ \\
$41$ & $\left[\left(y^{2} + 36 y + 18, 1\right), \left(y^{3} + 5 y^{2} + 7 y + 27, 1\right)\right]$ \\
$43$ & $\left[\left(y + 27, 1\right), \left(y^{4} + 16 y^{3} + 41 y^{2} + 11 y + 38, 1\right)\right]$ \\
$47$ & $\left[\left(y + 29, 1\right), \left(y^{4} + 18 y^{3} + 42 y^{2} + 4 y + 16, 1\right)\right]$ \\
$53$ & $\left[\left(y + 29, 1\right), \left(y + 48, 1\right), \left(y^{3} + 29 y^{2} + 32 y + 45, 1\right)\right]$ \\
$59$ & $\left[\left(y^{5} + 50 y + 53, 1\right)\right]$ \\
$61$ & $\left[\left(y^{5} + 52 y + 55, 1\right)\right]$ \\
$67$ & $\left[\left(y + 7, 1\right), \left(y + 21, 1\right), \left(y + 24, 1\right), \left(y + 33, 1\right), \left(y + 49, 1\right)\right]$ \\
$71$ & $\left[\left(y + 4, 1\right), \left(y^{4} + 67 y^{3} + 16 y^{2} + 7 y + 34, 1\right)\right]$ \\
$73$ & $\left[\left(y + 49, 1\right), \left(y + 57, 1\right), \left(y^{3} + 40 y^{2} + 48 y + 65, 1\right)\right]$ \\
$79$ & $\left[\left(y + 45, 1\right), \left(y + 56, 1\right), \left(y^{3} + 57 y^{2} + 18 y + 60, 1\right)\right]$ \\
$83$ & $\left[\left(y + 8, 1\right), \left(y + 18, 1\right), \left(y^{3} + 57 y^{2} + 34 y + 38, 1\right)\right]$ \\
$89$ & $\left[\left(y^{5} + 80 y + 83, 1\right)\right]$ \\
$97$ & $\left[\left(y^{5} + 88 y + 91, 1\right)\right]$ \\
$101$ & $\left[\left(y^{2} + 62 y + 94, 1\right), \left(y^{3} + 39 y^{2} + 13 y + 73, 1\right)\right]$ \\
$103$ & $\left[\left(y + 68, 1\right), \left(y^{4} + 35 y^{3} + 92 y^{2} + 27 y + 9, 1\right)\right]$ \\
$107$ & $\left[\left(y^{2} + 11 y + 2, 1\right), \left(y^{3} + 96 y^{2} + 12 y + 104, 1\right)\right]$ \\
$109$ & $\left[\left(y + 34, 1\right), \left(y^{4} + 75 y^{3} + 66 y^{2} + 45 y + 96, 1\right)\right]$ \\
$113$ & $\left[\left(y^{5} + 104 y + 107, 1\right)\right]$ \\
$127$ & $\left[\left(y + 62, 1\right), \left(y^{4} + 65 y^{3} + 34 y^{2} + 51 y + 4, 1\right)\right]$ \\
$131$ & $\left[\left(y^{5} + 122 y + 125, 1\right)\right]$ \\
$137$ & $\left[\left(y^{2} + 21 y + 122, 1\right), \left(y^{3} + 116 y^{2} + 45 y + 110, 1\right)\right]$ \\
$139$ & $\left[\left(y^{5} + 130 y + 133, 1\right)\right]$ \\
$149$ & $\left[\left(y + 53, 1\right), \left(y^{2} + 100 y + 17, 1\right), \left(y^{2} + 145 y + 63, 1\right)\right]$ \\
$151$ & $\left[\left(y^{5} + 142 y + 145, 1\right)\right]$ \\
$157$ & $\left[\left(y^{2} + 68 y + 112, 1\right), \left(y^{3} + 89 y^{2} + 116 y + 42, 1\right)\right]$ \\
\end{tabular}


\end{center}

Note that for any prime $p$, we only need to consider primes $\frak{p} \mid p$ with $N(\frak{p}) < 163$, so we can drop any factors with $p^{\deg ()}\geq 163$. We do this in (E) using Sage. We end up with the following primes (I edited some of the table formatting manually to save time):
\begin{center}
\begin{tabular}{ll}
$\frak{p}_{2}$ & $(2, \alpha)$ \\
$\frak{q}_{2}$ & $(2, \alpha + 1)$ \\
$\frak{p}_{3}$ & $(3, \alpha)$ \\
$\frak{p}_{5}$ & $(5, \alpha + 2)$ \\
$\frak{p}_{25}$ & $(5, \alpha^2 + 4\alpha + 1)$ \\
$\frak{q}_{25}$ & $(5, \alpha^2 + 4\alpha + 2)$ \\
$\frak{p}_{7}$ & $(7, \alpha + 4)$ \\
$\frak{q}_{7}$ & $(7, \alpha + 6)$ \\
$\frak{p}_{121}$ & $(11, \alpha^2 + 8\alpha + 3)$ \\
$\frak{p}_{23}$ & $(23, \alpha + 6)$ \\
$\frak{p}_{29}$ & $(29, \alpha + 24)$ \\
$\frak{p}_{31}$ & $(31, \alpha + 22)$ \\
$\frak{p}_{37}$ & $(37, \alpha + 3)$ \\
$\frak{p}_{43}$ & $(43, \alpha + 27)$ \\
$\frak{p}_{47}$ & $(47, \alpha + 29)$ \\
$\frak{p}_{53}$ & $(53, \alpha + 29)$ \\
$\frak{q}_{53}$ & $(53, \alpha + 48)$ \\
\end{tabular}
\begin{tabular}{ll}
$\frak{p}_{67}$ & $(67, \alpha + 7)$ \\
$\frak{q}_{67}$ & $(67, \alpha + 21)$ \\
$\frak{r}_{67}$ & $(67, \alpha + 24)$ \\
$\frak{s}_{67}$ & $(67, \alpha + 33)$ \\
$\frak{t}_{67}$ & $(67, \alpha + 49)$ \\
$\frak{p}_{71}$ & $(71, \alpha + 4)$ \\
$\frak{p}_{73}$ & $(73, \alpha + 49)$ \\
$\frak{q}_{73}$ & $(73, \alpha + 57)$ \\
$\frak{p}_{79}$ & $(79, \alpha + 45)$ \\
$\frak{q}_{79}$ & $(79, \alpha + 56)$ \\
$\frak{p}_{83}$ & $(83, \alpha + 8)$ \\
$\frak{q}_{83}$ & $(83, \alpha + 18)$ \\
$\frak{p}_{103}$ & $(103, \alpha + 68)$ \\
$\frak{p}_{109}$ & $(109, \alpha + 34)$ \\
$\frak{p}_{127}$ & $(127, \alpha + 62)$ \\
$\frak{p}_{149}$ & $(149, \alpha + 53)$ \\
\end{tabular}
\end{center}
To eliminate generators for the class group, we employ the following strategy. Note that for any element of the form $a + b\alpha$ Sage can easily compute the norm (we could do this ourself using linear algebra if we had a lot of paper and time). We will write a function that factors $N = N_{K / \QQ}(a + b\alpha)$ for small $a$ and $b$, and select only those $a$ and $b$ for which
\begin{enumerate}
\item $N$ is divisible by a given prime.
\item All prime powers dividing $N$ are at most $163$. 
\end{enumerate}
We then know that the ideal $a + b\alpha$ decomposes into prime ideals in the above list. By manually changing the given prime (starting at the largest primes), we hope to find relations. The function is called find useful primes in our Sage document, and we will manually change the input. We find the following results:
\begin{itemize}
\item We find $N(-3 \alpha - 10) = -1 \cdot 2^{3} \cdot 79 \cdot 149$. Since there is only one prime ideal with norm $149$, we see that we do not need it.
\item We find $N(4 \alpha - 6) = 3 \cdot 127$. Since there is only one prime ideal with norm $127$, we see that we do not need it.
\item We find $N(-3 \alpha + 7) = 2 \cdot 47 \cdot 109$. Since there is only one prime ideal with norm $109$, we see that we do not need it.
\item We find $N(6 \alpha - 4) = 2^{7} \cdot 7 \cdot 103$. Since there is only one prime ideal with norm $103$, we see that we do not need it.
\end{itemize}
To deal with the case where there might be more primes of particular norm, we notice that $(k, \alpha + m)$ divids $(a + b \alpha)$ if and only if $a + b\alpha \in  (p, \alpha + m)$. This happens if and only if $bm - a$ is divisible by $p$. Therefore to look for ideals divisible by a prime ideal of the form $(p, \alpha + m)$ of norm $p$, we need to look for ideals $(a + b\alpha)$ in which $p$ occurs in the factorization of $N$ exactly once (to exclude ideals of index $p^2$ that we may have thrown away), and for which  $bm  - a$ is divisible by $p$. We do this in our function find useful primes improved (G). Unfortunately we learn that we cannot longer demand that all prime powers appear with low multiplicity, so we drop this condition for the program and are a bit more careful in selecting what we use.

We find the following results:
\begin{itemize}
\item We find $N(-5 \alpha - 7) = -2 \cdot 23 \cdot 83$, and therefore we do not need $\frak{q}_{83}$ to generate the class group.
\item We find $N(-27 \alpha - 50) = -1 \cdot 2^{12} \cdot 7 \cdot 67 \cdot 83$, and therefore we do not need $\frak{p}_{83}$ to generate the class group (since all primes above $2$ have norm $2$ and we will use all of those).
\item We find $N(15 \alpha - 29) = -1 \cdot 2 \cdot 7 \cdot 37 \cdot 67 \cdot 79$, therefore we do not need $\frak{q}_{79}$ to generate the class group.
\item We find $N(\alpha - 34) = -1 \cdot 2^{3} \cdot 29 \cdot 37 \cdot 67 \cdot 79$, therefore we do not need $\frak{p}_{79}$ to generate the class group.
\item We find $N(-25 \alpha - 38) = -1 \cdot 2^{5} \cdot 7^{2} \cdot 37 \cdot 73$, therefore we do not need $\frak{q}_{73}$ to generate the class group (we don't have to worry about the $7^2$ as there are only primes above $7$ of norm $7$ or $7^3$).
\item We find $N(-25 \alpha - 38) = -1 \cdot 2^{5} \cdot 7^{2} \cdot 37 \cdot 73$, thus drop $\frak{p}_{73}$.
\item We find $N(-2 \alpha - 8) =-1 \cdot 2^{6} \cdot 7 \cdot 71$, thus drop $\frak{p}_{71}$.
\item We find $N(\alpha - 18) = -1 \cdot 2^{3} \cdot 3 \cdot 5^{2} \cdot 47 \cdot 67$, thus drop $\frak{t}_{67}$. Note that we do not have to worry about the $5^2$ as we included the ideals of norm $25$ in our list. 
\item We find $N(4 \alpha - 2) = 2^{5} \cdot 5 \cdot 67$, dropping $\frak{s}_{67}$.
\item We find $N(11 \alpha - 4) = 2 \cdot 7 \cdot 37 \cdot 43 \cdot 67$, dropping $\frak{t}_{67}$.
\item We find $N(3 \alpha - 4) = 2 \cdot 5^{2} \cdot 67$, dropping $\frak{q}_{67}$.
\item Using the factorization of $(67)$ we may drop $\frak{p}_{67}$. 
\item We find $N(-\alpha + 5) = 2 \cdot 29 \cdot 53$, dropping $\frak{q}_{53}$. 
\item We find $N(2 \alpha + 5) = 7^{2} \cdot 53$, dropping $\frak{p}_{53}$.
\item We find $N(10 \alpha + 8) = -1 \cdot 2^{6} \cdot 29 \cdot 47$, dropping $\frak{p}_{47}$.
\item We find $N(-3 \alpha + 5) = -1 \cdot 2 \cdot 23 \cdot 43$, dropping $\frak{p}_{43}$.
\item We find $N(\alpha + 3) = 2 \cdot 3 \cdot 37$, dropping $\frak{p}_{37}$.
\item We find $N(3 \alpha + 4) = -1 \cdot 2 \cdot 7 \cdot 31$, dropping $\frak{p}_{31}$.
\item We find $N(8 \alpha + 18) = 2^{5} \cdot 3 \cdot 7 \cdot 29 \cdot 73$. Since we dit not use primes of index $29$ to conclude we may drop the primes above $73$, we could've first dropped $\frak{p}_{29}$. Thus we may conclude we do not need $\frak{p}_{29}$.
\item We find $N(\alpha + 6) = 2^{4} \cdot 3 \cdot 7 \cdot 23$, dropping $\frak{p}_{23}$.
\item By hand (see (H)) we find that $N(\alpha^2 + 8\alpha + 3) = 2^2 \cdot 3 \cdot 11^2 \cdot 43$. Since we did not use the ideal of norm $11^2$ to get rid of the ideal of norm $43$, we may drop $\frak{p}_{121}$.
\item Similarly we find by hand that $N(\alpha^2 + 4\alpha + 2) = 2 \cdot 5^2 \cdot 7$. Clearly $\frak{q}_{25}$ divides this ideal hence we may drop it (as we didn't drop anything of norm $7$ yet.
\item We find by hand that $N(\alpha^2 - \alpha + 1) = 5^2 \cdot 7$. Clearly $\frak{p}_{25}$ divides this ideal hence we may drop it.
\item Continuing using (G) again, we find that $N(\alpha - 1) = 2 \cdot 7$, dropping $\frak{q}_7$.
\item We find that $N(\alpha - 3) = -1 \cdot 2 \cdot 3 \cdot 5 \cdot 7$, dropping $\frak{p}_7$.
\item We find that $N(\alpha + 2) = 2^2 \cdot 5$, dropping $\frak{p}_5$.
\item We know that $N(\alpha) = 6 = 2 \cdot 3$, hence we may drop $\frak{p}_3$.
\item Finally since $\frak{q}_2^4 \frak{p}_2 = (5)$, we know that $[\frak{p}_2] = -4[\frak{q}_2]$ and may drop $\frak{p}_2$.
\end{itemize}
We conclude that the class group is generated by $[\frak{q}_2]$ (surprisingly, checking all these ideals only took 30 minutes after writing the code). Running (G) for (2, 1), Sage instantly tells us that $N(\alpha + 1) = -2$, and we conclude that $\alpha + 1 = \frak{q}_2$. Thus the class group is trivial and $\catO_K$ is a principal ideal domain.

Remains to compute the unit group. Using Sage, we can find ideals of the form $(a + b\alpha)$ whose norm is only divisible by small primes, by simply computing all the norms and keeping only the ones we want (See (I) in our worksheet). We find the following results:
\begin{center}
\begin{tabular}{|l|l|} \hline
\text{ideal} & \text{norm} \\ \hline
$-\alpha + 1$ & $-1 \cdot 2 \cdot 7$ \\ \hline
$\alpha + 1$ & $-1 \cdot 2$ \\ \hline
$2 \alpha + 1$ & $7^{2}$ \\ \hline
$-\alpha + 2$ & $2^{3}$ \\ \hline
$\alpha + 2$ & $2^{2} \cdot 5$ \\ \hline
$3 \alpha + 2$ & $2^{5}$ \\ \hline
$-\alpha + 3$ & $2 \cdot 3 \cdot 5 \cdot 7$ \\ \hline
$2 \alpha + 3$ & $3$ \\ \hline
$4 \alpha + 3$ & $-1 \cdot 3 \cdot 5^{2} \cdot 7$ \\ \hline
\end{tabular}
\end{center}
Recall that an ideal of the form $(p, \alpha + k)$ divides $(a + b\alpha)$ if and only if $bk - a$ is divisible by $p$. Using this we can factor (we drop the last one)
\begin{center}
\begin{tabular}{|l|l|} \hline
$-\alpha + 1$ & $\frak{q}_2 \frak{q}_7$ \\ \hline
$\alpha + 1$ & $\frak{q}_2$ \\ \hline
$2 \alpha + 1$ & $\frak{p}_7^2$ \\ \hline
$-\alpha + 2$ & $\frak{p}_2^3$ \\ \hline
$\alpha + 2$ & $\frak{p}_2^2 \frak{p}_5$ \\ \hline
$3 \alpha + 2$ & $\frak{p}_2^5$ \\ \hline
$-\alpha + 3$ & $\frak{q}_2 \frak{p}_3 \frak{p}_5 \frak{p}_7$ \\ \hline
$2 \alpha + 3$ & $\frak{p}_3$ \\ \hline
\end{tabular}
\end{center}
Also recall that $N(\alpha + 1) = -2$ hence $(\alpha + 1) = \frak{q}_2$, and $\frak{p}_2 = (2)\frak q_2^{-4}$. We find our first unit ideal since we immediately see that (start with the $(3 \alpha + 2)^2$ and just cancel everything using smaller terms)
\[
\frac{(-\alpha + 3)^2(2)^4(\alpha + 1)^{-16}}{(2 \alpha + 1)(\alpha + 2)^2(\alpha + 1)^2(2 \alpha + 3)^2} = \frac{(\frak{q}_2 \frak{p}_3 \frak{p}_5 \frak{p}_7)^2\frak{p}_2^4}{\frak p_7^2 (\frak p_2^2 \frak p_5)^2\frak{q}_2^2 \frak{p}_3^2}= (1).
\]
Using Sage, we find (see (J)) that
\[
u_1 = -79722620433 \alpha^{4} + 131828627644 \alpha^{3} - 205658588763 \alpha^{2} + 313223514708 \alpha + 245437567345.
\]
is a unit.

An easier one to see is that $(-\alpha + 2) = (2)^3(\alpha + 1)^{-12}$. Therefore
\[
u_2 = \frac{2^3}{(\alpha + 1)^{12}(-\alpha + 2)} = -368215 \alpha^{4} + 251017 \alpha^{3} - 170675 \alpha^{2} + 115379 \alpha + 3236941
\]
is a unit.

Increasing our bound of small primes in (I), we also find the factorizations 
\begin{align*}
N(\alpha + 3) &= 2\cdot3 \cdot 37 \\
N(3\alpha + 4) &= -2\cdot7 \cdot 31 \\
N(16 \alpha + 11) &= 31^2 \cdot 37
\end{align*}
Since $31$ has one prime of index $31^4$ above it, the $31^2$ appearing must be $\frak{p}_{31}$, and clearly the 37's must come from $\frak{p}_{37}$. We conclude
\begin{align*}
(\alpha + 3) &= \frak{q}_2 \frak{p}_3  \frak{p}_{37} \\
(3\alpha + 4) &= \frak{p}_2  \frak{q}_7 \frak{p}_{31}\\
(16 \alpha + 11) &= \frak{p}_{31}^2 \frak{p}_{37}.
\end{align*}
It follows that
\[
\frac{(16 \alpha + 11)(-\alpha +  1)^2(2\alpha + 3)(4)}{(3\alpha + 4)^2(\alpha + 3)(\alpha + 1)^9} = \frac{(\frak p_{31}^2 \frak p_{37})(\frak q_2 \frak q_7)^2\frak{p}_3(4)}{(\frak p_2 \frak q_7 \frak p_{31})^2(\frak{q}_2 \frak{p}_3  \frak{p}_{37})\frak q_2^9}= (1),
\]
and 
\[
u_3 = -187 \alpha^{4} - 181 \alpha^{3} + 585 \alpha^{2} - 1089 \alpha + 3457
\]
is also a unit.

To see if these are fundamental, we compute the regulator in Sage (see (K)). We do this by first numerically approximating computing the three real roots of f, then evaluating the $u_i(\alpha)$ in each of the 3 real roots gives us the three $\sigma_j(u_i)$ we need. Unfortunately Sage's standard precision turned out not to be good enough (giving infinity issues) so we had to increase the precision to 200 bits. It turns out that
\[
\text{Reg}(u_1, u_2, u_3) \approx -245.9.
\]

To see if this is what it should be, we approximate the residue of $\zeta_K$ at $t = 1$. To do this we need to figure out how $f$ factors modulo $p$, and compute the corresponding norms of prime ideals lying above $f$. Let $r(p)$ denote the number of roots of $f \pmod{p}$. If $p \neq 2, 3, 8539$, using Exercise 15 it is not too hard to see that
\[
\prod_{\frak{p} \mid p}  (1 - N_{K / \QQ}(\frak{p})^{-1}) = \begin{cases}
(1 - p^{-1})^5 & r(p) = 5 \\
(1 - p^{-1})^3(1 - p^{-2}) & r(p) = 3 \\
(1 - p^{-1})^2(1 - p^{-3}) & r(p) = 2 \\
(1 - p^{-1})(1 - p^{-4}) & r(p) = 1, \(\frac{\Delta(f)}{p}\) = -1. \\
(1 - p^{-1})(1 - p^{-2})^2 & r(p) = 1, \(\frac{\Delta(f)}{p}\) = 1. \\
(1 - p^{-2})(1 - p^{-3}) & r(p) = 0, \(\frac{\Delta(f)}{p}\) = -1. \\
(1 - p^{-5}) & r(p) = 0, \(\frac{\Delta(f)}{p}\) = 1. \\
\end{cases}
\]
Using this we can approximate the Euler product in Sage, see (L) (we will not go above $8000$ so we don't have to worry about the 8539). The results are as follows:
\begin{center}
\begin{tabular}{ll}
$i$ & $\prod_{p < i}E(p)$ \\ \hline
$10$ & $2.53923171160927$ \\
$50$ & $1.89502037386979$ \\
$100$ & $2.01740096517944$ \\
$1000$ & $1.85367755848942$ \\
$2000$ & $1.85193995490091$ \\
$3000$ & $1.85372282838463$ \\
$5000$ & $1.85151324779133$ \\
$8000$ & $1.86119545341866$ \\
\end{tabular}
\end{center}
Since $K$ has real embeddings, the roots of unity are simply $\{\pm 1\}$. Thus if $\{u_1, u_2, u_3\}$ were a fundamental system of units, we the residue would equal (see (M) in Sage)
\[
\frac{2^3(2\pi)^1\cdot 1 \cdot 245.9}{2 \cdot \sqrt{11066544}} \approx 1.85.
\]
It is very unlikely for our limit computation to be a factor $2$ off, therefore we conclude $\{u_1, u_2, u_3\}$ is a fundamental system of units, so that $\catO_K^* = \langle -1 \rangle \times \langle u_1 \rangle \times \langle u_2 \rangle \times \langle u_3 \rangle$.
\end{document}