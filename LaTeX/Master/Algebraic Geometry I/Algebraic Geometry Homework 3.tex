\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{stackengine}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\AAA}{\mathbb{A}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catF}{\mathcal{F}}
\newcommand{\catG}{\mathcal{G}}
\newcommand{\catO}{\mathcal{O}}
\newcommand{\catE}{\mathcal{E}}
\newcommand{\catH}{\mathcal{H}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand{\twomatrix}[4]{\begin{pmatrix} #1 & #2 \\ #3 & #4 \end{pmatrix}}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Geometry Homework 3}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[4.6.25] \
\begin{enumerate}[i.]
\item Define $\ker(f)(U)$ to be $\ker(f(U)): \catF(U) \to \catG(U)$. Then $\ker(f)$ is a presheaf, since
\begin{itemize}
\item f(U) is a morphism of abelian groups, hence by elementary group theory we know $\ker(f(U))$ to be a well-defined abelian group. 
\item For each $U \subseteq V \subseteq X$ open, we have a group homomorphism $r_U^V: \catF(V) \to \catF(U)$. Thus we can define a grouphomomorphism $\tilde{r}_U^V: \ker(f)(V) \to \ker(f)(U)$ by defining explicitly $\tilde{r}_U^V = r_U^V |_{\ker(f(U))}$. This is well defined, since if $a \in \ker(f)(V)$ then $f(V)(a) = 0$, thus if we denote $t_U^V: \catG(V) \to \catG(U)$ for the restriction map of $\catG$, then because $t_U^V$ is a group homomorphism we have $t_U^V(f(V))(a) =0$. Since $f$ is a morphism of presheaves we have $f(U) \circ r_U^V = t_U^V \circ f(V)$, thus $f(V)(r_U^V(a)) = 0$ and $r_U^V(a) \in \ker(f)(V)$ as required. And clearly for any $U \subseteq V \subseteq W \subseteq X$ open one has $\tilde{r}_U^W = \tilde{r}_U^V \circ \tilde{r}_V^W$ since all $r_X^Y$ satisfies this identity and the $\tilde{r_X^Y}$ are obtained by restricting $r_X^Y$.
\end{itemize}
Similarly, define $\im(f)(U)$ to be $\im(f(U): \catF(U) \to \catG(U)$. Then $\im(f)$ is a presheaf, since 
\begin{itemize}
\item f(U) is a morphism of abelian groups, hence by elementary group theory we know $\im(f(U))$ to be a well-defined abelian group. 
\item For each $U \subseteq V \subseteq X$ open, we have a group homomorphism $t_U^V: \catG(V) \to \catG(U)$. Thus we can define a grouphomomorphism $\tilde{t}_U^V: \im(f)(V) \to \im(f)(U)$ by defining explicitly $\tilde{t}_U^V = t_U^V |_{\ker(f(U))}$. This is well defined, since if $a \in \im(f)(V)$ then there is $b \in \catF(V)$ with $f(V)(b) = a$. Thus if we denote $r_U^V: \catF(V) \to \catF(U)$ for the restriction maps of $\catF$, since $f$ is a morphism of presheaves we have $f(U) \circ r_U^V = t_U^V \circ f(V)$, thus $\tilde{t}_U^V(a) = t_U^V(f(V)(b)) = f(U)(r_U^V(b))$ which shows $\tilde{t}_U^V(a) \in \im(f)(U)$. And clearly for any $U \subseteq V \subseteq W \subseteq X$ open one has $\tilde{t}_U^W = \tilde{t}_U^V \circ \tilde{t}_V^W$ since all $t_X^Y$ satisfies this identity and the $\tilde{t}_X^Y$ are obtained by restricting $t_X^Y$.
\end{itemize}
Finally we check that $\ker(f)$ is a sheaf. Denote once again, for $U \subseteq V \subseteq X$ open $r_U^V$ for the restriction map of $\catF$ and $\tilde r_U^V$ for the restriction map of $\ker(f)$ (obtained by restricting $r_U^V$). Denote $t_U^V$ for the restriction map of $\catG$.
\begin{itemize}
\item Since $\catF, \catG$ are sheaves we have $\catF(\emptyset) = \catG(\emptyset) = 0$ and $f(\emptyset) = 0$. Thus $\ker(f)(\emptyset) = 0$ as well.
\item For each $U \subseteq X$ open and open cover $\{U_i\}$ of $U$, if $s, t \in \ker(f)(U)$ and $\tilde r_{U_i}^U(t) = \tilde r_{U_i}^U (s)$ for all $i$, then by definition $s, t \in \catF(U)$ (since $\ker(f)(U) \subseteq \catF(U)$). Since $\tilde r_{U_i}^U$ is obtained by restricting $r_{U_i}^U$, one also has $r_{U_i}^U(t) = r_{U_i}^U(s)$ for all $i$. Since $\catF$ is a sheaf we may conclude $s = t$.
\item For each $U \subseteq X$ open, open cover $\{U_i\}$ of $U$ and set of functions $s_i \in \ker(f)(U_i)$ such that $\tilde r_{U_i \cap U_j}^{U_i} (s_i) = \tilde r_{U_i \cap U_j}^{U_j} (s_j)$ for all $i, j$, once again using $\ker(f)(U) \subseteq \catF(U)$ one finds $s_i \in \catF(U_i)$. Again one has $r_{U_i \cap U_j}^{U_i} (s_i) = r_{U_i \cap U_j}^{U_j} (s_j)$ as well for all $i, j$, thus there is $s \in \catF(U)$ such that $s_i = r^U_{U_i}(s)$ for all $i$. It now suffices to show that $f(U)(s) = 0$, since then $s \in \ker(f)(U)$ and $\tilde r^U_{U_i}(s) = s_i$ for all $i$. Using that $f$ is a morphism of presheaves, we have for all $i$ that $t_{U_i}^U(f(U)(s)) = f(U_i)(r^U_{U_i}(s)) = f(U_i)(s_i) = 0$ since $s_i \in \ker(f)(U)$. Thus $t^U_{U_i}(f(U)(s)) = t^U_{U_i}(0)$ for all $i$, and since $\catG$ is a sheaf we may conclude $f(U)(s) = 0$ as desired.
\end{itemize}
\item To make things more readable, for any presheaf on $X$, $U \subseteq X$ open and a section $s$ in the presheaf we will denote $s|_U$ for the restriction map of the sheaf applied to $s$, we will assume it to be understood which restriction map is meant.

Now given a morphism $f: \catF \to \catG$, consider the set of abelian groups
\[
\catE(U) = \{s \in \catG(U) \mid \exists \text{ open cover } \{U_i\} \text{ of } U, \exists s_i \in \im(f)^p(U_i): s_i = s|_{U_i}\}.
\]
We claim $\catE(U)$ (with the restriction maps obtained from $\catG$) is a subsheaf of $\catG(U)$:
\begin{itemize}
\item Clearly $\catE(U)$ is an abelian subgroup of $\catE(U)$, since $0 \in \im(f)^p(U)$ and $\{U\}$ is an open cover of $U$, hence $0 \in \catE(U)$. Furthermore, if $s, t \in \catE(U)$, then there exist open covers $\{U_i\}_{i \in I}$ and $\{V_j\}_{j \in J}$ and $s_i \in \im(f)^p(U_i)$, $t_j \in \im(f)^p(V_j)$ such that $s|_{U_i} = s_i$ and $t|_{U_j} = t_j$. Thus for any $i \in I, j \in J$ one has $(s - t)|_{U_i \cap V_j} = s_i|_{U_i \cap V_j} - t_j|_{U_i \cap U_j}$. Since $\{U_i \cap V_j\}_{i \in I, j \in J}$ is still an open cover of $U$ and $s_i|_{U_i \cap V_j} - t_j|_{U_i \cap U_j} \in \im(f)^p(U_i \cap V_j)$ ($\im(f)^p$ is a presheaf) we may thus conclude $s - t \in \catE(U)$. 
\item The restriction maps are well-defined, since if $U \subseteq V \subseteq X$ are open and $s \in \catE(V)$, then there is an open cover $\{V_i\}$ of $V$ and there are $s_i \in \im(f)^p(V_i)$ such that $s_i = s|_{V_i}$. Clearly $\{V_i \cap U\}$ is an open cover of $U$ and one has $s_i|_{V_i \cap U} = s|_U|_{V_i}$. Since $s_i \in \im(f)^p(V_i)$ and $\im(f)^p$ is a presheaf we see that $s_i|_{V_i \cap U} \in \im(f)^p(V_i \cap U)$, thus we may conclude $s|_U \in \catE(U)$. Thus $\catE$ is a presheaf.
\item Clearly $\catE(\emptyset) = 0$ since it is a subgroup of $\catG(\emptyset) = 0$. 
\item For each $U \subseteq X$ open and $s, t \in \catE(U)$, open cover $\{U_i\}$ of $U$ such that $s|_{U_i} = t_{U_i}$, we may immediately conclude $s = t$ since $s, t \in \catG(U)$ and $\catG$ is a sheaf (techincally one needs to be careful with the restrictions as we did in (i), we will not do this here for brevity).
\item For each $U \subseteq X$ open, open cover $\{U_i\}$ of $U$ and $s_i \in \catE(U_i)$ such that $s_i|_{U_i \cap U_j} = s_j|_{U_i \cap U_j}$ for all $i, j$, since $\catG$ is a sheaf we can find $s \in \catG(U)$ such that $s|_{U_i} = s_i$ (techinically one has to be careful with the restriction maps again, we did this carefully in the last part of (i)). We claim that $s$ is in fact in $\catE(U)$. To see this, note that for any $i$ we have $s_i \in \catE(U_i)$, thus there exists an open cover $\{V^i_j\}_{j \in J_i}$ and sections $s^i_j \in \im(f)^p(V^i_j)$ such that $s_i|_{V^i_j} = s^i_j$. Clearly $\{V^i_J \mid i \in I, j \in J_i\}$ is an open cover of $U$ for which $s|_{V^i_j}  = s|_{U_i}|_{V^i_j} = s^i_j$. Since all $s^i_j \in \im(f)^p(V^i_j)$ we may conclude $s \in \catE(U)$. This shows that $\catE$ is a sheaf.
\end{itemize}
Note that there is an obvious inclusion $\iota: \im(f)^p \to \catE$. We show that for any $x \in X$, the morphism $\iota_x: \im(f)^p_x \to \catE_x$ is an isomorphism. 
\begin{itemize}
\item To see that $\iota_x$ is injective, suppose $\iota_x(s_x) = \iota_x(t_x)$ for some $s_x, t_x \in \im(f)^p_x$. Then there exist $U, V \subseteq X$ open with $x \in U, x \in V$ such that $s_x = [(s, U)]$ and $t_x = [(t, V)]$. Then since $\iota_x(s_x) = \iota_x(t_x)$ we have $[(s, U)] = [(t, V)]$ (as equality in $\catE_x$). Thus there exists $W  \subseteq U \cap V$ open such that $x \in W$ and $s|_W = t|_W$. But since the restriction maps of $\im(f)^p$ and $\catE$ are both obtained from the restriction maps of $\catG$, we thus have $s|_W = t|_W$ as sections in $\im(f)^p$, and thus $s_x = t_x$ by definition.
\item To see that $\iota_x$ is surjective, pick $s \in \catE_x$. Then there is open set $U$ and $s' \in \catE(U)$ such that $x \in U$ and $s = (s', U)$. Thus there exists an open cover $\{U_i\}$ of $U$ and $s_i \in \im(f)^p(U_i)$ such that $s'|_{U_i} = s_i$. Let $i$ such that $x \in U_i$, such $i$ exist since the $U_i$ cover $U$. Then clearly $\iota_x([(s_i, U_i)]) = [(s', U)]$ since $s_i|_{U_i} = s'|_{U_i}$. Thus $\iota_x$ is surjective.
\end{itemize}
Since $\iota_x$ is a grouphomomorphism it is an isomorphism. By Exercise 4.6.24, $\iota$ also induces an isomorphism $j: \im(f) \to \catE$, which is the isomorphism we were looking for (we can compose with the inclusion $\catE \to \catG$ to obtain the morphisim $\im(f) \to \catG$). 
\newpage
\item We first show that if $\varphi: \catE \to \catF$ is a morphism of sheaves, then for any $\ker(\varphi_P) = \ker(\varphi)_P$ (technically speaking they are not equal, but there is a canonical identification). Note that one can write
\begin{align*}
\ker(\varphi_P) &= \{[(s, U)] \in \catE_P \mid [(\varphi(U)(s), U)] = 0 \text{ in $\catF_P$} \} \\
\ker(\varphi)_P &= \{[(s, U)] \mid s \in \catE , P \in U \subseteq X \text{ open } \mid \varphi(U)(s) = 0\}.
\end{align*}
To see that $\ker(\varphi_P) \subseteq \ker(\varphi)_P$, note that if $[(s, U)] \in \ker(\varphi_P)$, there exists $V \subseteq X$ open with $P \in V$ and $(\varphi(U)(s), U) \sim (0, V)$. Thus there is $W \subseteq V \cap U$ open with $\varphi(W)(s|_W) = 0$. Clearly $[(s, U)] = [(s|_W, W)]$ and the latter is clearly in $\ker(\varphi)_P$. To see $\ker(\varphi)_P \subseteq \ker(\varphi_P)$, suppose $[(s, U)] \in \ker(\varphi)_P$. Then $\varphi(U)(s) = 0$ hence $[(\varphi(U)(s), U)] = 0$ and therefore $[(s, U)] \in \ker(\varphi_P)$. 

We now show that if $\im(\varphi_P) = \im(\varphi)_P$. Note that by (ii) we can write
\begin{align*}
\im(\varphi_P) &= \{[(\varphi(V)(t), V)] \mid [(t, V)] \in \catE_P\}. \\
\im(\varphi)_P &= \{[(s, U)] \mid s \in \im(\varphi)(U) , P \in U \subseteq X \text{ open }\} \\
&= \left\{[(s, U)] \mid  \stackanchor{$s \in \catF_P , P \in U \subseteq X \text{ open}, \exists \text{ open cover $\{U_i\}$ of } U$}{$\exists g_i \in \catE(U_i): \varphi(U_i)(g_i) = s|_{U_i}$} \right\}.
\end{align*}
To see that $\im(\varphi_P) \subseteq \im(\varphi)_P$, to see that $[\varphi(V)(t), V)] \in \im(\varphi)_P$ one can take the open cover $\{V\}$ of $V$ with $t \in \catE(V)$. For the converse, note that if $[(s, U)] \in \im(\varphi)_P$, then there exists an $U_i$ in the open cover of $U$ that contains $P$. Then clearly $[(s, U)] = [(g_i, U_i)] \in \im(\varphi_P)$, since $s|_{U_i} = g_i|_{U_i}$.

Now consider any sequence of morphisms of sheaves $0 \to \catE \xrightarrow{\varphi} \catF \xrightarrow{\psi} \catG \to 0$. By exercise 4.6.24 we have
\begin{align*}
\varphi \text{ injective} &\leftrightarrow  \ker(\varphi) = 0 \\
&\leftrightarrow \ker(\varphi)_P = 0 \ \forall P \in X &(*)\\
&\leftrightarrow \ker(\varphi_P) = 0\  \forall P \in X \\
&\leftrightarrow \varphi_P \text{ injective } \ \forall P \in X.
\end{align*}
For (*), note that $\rightarrow$ follows from Exercise 4.6.22, since functors map isos to isos. The converse follows from Exercise 4.6.23, since the zero map $\ker(\varphi) \to 0$ is clearly a morphism of sheaves that restricts to the $0$ map on stalks, hence the zero map is an isomorphism.
Similarly
\begin{align*}
\psi \text{ surjective} &\leftrightarrow  \im(\psi) = \catG \\
&\leftrightarrow \im(\psi)_P = \catG_P \ \forall P \in X &(**)\\
&\leftrightarrow \im(\psi_P) = \catG_P\  \forall P \in X \\
&\leftrightarrow \psi_P \text{ surjective } \ \forall P \in X.
\end{align*}
For (**), note that $\to$ once again follows from Exercise 4.6.22,  since functors map isos to isos (which implies the induced map is bijective). The converse follows since the natural inclusion $\im(\psi)^p \to \catG$ clearly induces the natural inclusions $\im(\psi)_P \to \catG_P$. Since the latter are isomorphisms, by Exercise 4.6.24 we may conclude the inclusion $\im(\psi) \to \catG$ is as well.
Finally
\begin{align*}
\ker(\varphi) = \im(\psi) &\leftrightarrow  \im(\psi)_P = \ker(\varphi)_P \ \forall P \in X \\
&\leftrightarrow \im(\psi_P) = \ker(\varphi_P) \ \forall P \in X
\end{align*}
here the first line holds once again by Exercise 4.6.22 and 4.6.23 (since the natural bijection on the subsheaves corresponds to the natural bijections on the stalks).

From this it follows that $0 \to \catE \xrightarrow{\varphi} \catF \xrightarrow{\psi} \catG \to 0$ is exact if and only if $0 \to \catE_P \xrightarrow{\varphi_P} \catF_P \xrightarrow{\psi_P} \catG_P \to 0$ is exact for all $P \in X$.
\end{enumerate}
\end{opghand}
\begin{opghand}[5.5.2] \
\begin{enumerate}[i.]
\item If $g^n \in \ker(f^*)$ for some $n \in \NN$ and $g \in \catO_Y$, then $f^*(g^n) = 0$, and $(g \circ f)^n(x) = g^n \circ f(x) = 0$ for any $x \in X$. Since $k$ is a field we conclude $g \circ f(x) = 0$ for any $x \in X$, hence $f^*(g) = 0$.
\item Let $\pi: \catO_Y(Y) \to \catO_Z(Z)$ be the restriction map of Exercise 4.6.8(ii), we know it is surjective. Clearly $\ker(\pi) = \ker(f^*)$ by Exercise 4.6.8, since $Y$ corresponds to the ideal $\ker(f^*)$. The first isomorphism theorem for algebras induces an isomorphism $\alpha: \catO_Y(Y) / \ker(\pi) \to \catO_Z(Z)$, which is the same as an isomorphism $\alpha: \catO_Y(Y) / \ker(f^*) \to \catO_Z(Z)$. But since $f$ is surjective it also gives an isomorphism $\beta: \catO_Y(Y) / \ker(f^*)  \to \catO_X(X)$. Now $\beta \circ \alpha^{-1}$ is our desired isomorphism.
\item Let $\iota: Z \to Y$ be the inclusion of varieties, then clearly $\iota$ is a morphism (Exercise 4.6.7). Furthermore for $\varphi \in \catO_Y(Y)$ one clearly has $\iota^*(\varphi) = \varphi \circ \iota = \pi(\varphi)$, so $\iota^* = \varphi$. Let $\beta$ as in (ii), then clearly from the first isomorphism theorem of $k$-algebras we know $\beta \circ \pi = f^*$ (since $\ker(f^*) = \ker(\pi)$). Denote $f'$ for the image of $\beta$ under the inverse map of Theorem 5.1.5. Then $(\iota \circ f')^* = \beta \circ \pi = f^*$. Since the map $(-)^*$ is a bijection by Theorem 5.1.5, we may conclude $\iota \circ f' = f$ as required. 
\end{enumerate}
\end{opghand}
\end{document}
