\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{stackengine}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\AAA}{\mathbb{A}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catF}{\mathcal{F}}
\newcommand{\catG}{\mathcal{G}}
\newcommand{\catO}{\mathcal{O}}
\newcommand{\catE}{\mathcal{E}}
\newcommand{\catH}{\mathcal{H}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand{\twomatrix}[4]{\begin{pmatrix} #1 & #2 \\ #3 & #4 \end{pmatrix}}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Geometry Homework 4}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[6.6.7] \ \newline
\begin{center}\textbf{Finding a presentation of $X$} \end{center}
Let $U_i = D(x_i) \subseteq \PP^2_k$. For $i \in \{0, 1\}$, define the open sets $X_i = X \cap U_i$, and put $X_{01} = X_0 \cap X_1$. These cover $X$, since if $(x_0 : x_1 : x_2)$ is in $X \setminus (U_1 \cup U_0)$, then $x_0 = x_2 = 0$. But the point $(0 : 1 : 0)$ does not satisfy the equation ($1^n \neq 0$), thus $(0 : 1 : 0) \not \in X$ and $X \subseteq U_1 \cup U_0$.

Recall that the map $\varphi_0: U_0 \to \AAA^2: (x_0 : x_1 ; x_2) \mapsto (\frac{x_1}{x_0}, \frac{x_2}{x_0})$ is an isomorphism of $k$-spaces. In particular (since $\AAA^2$ is affine) we have an isomorphism of $k$-algebras $\varphi_0^*: \catO_{\AAA^2}(\varphi_0(X_0)) \to \catO_X(X_0)$. Pick coordinates $c, d$ on $\AAA^2$, then we compute
\[
\varphi_0(X_0) = Z(c^n - d + d^n) \subseteq \AAA^2.
\]
To compute the $k$-algebra, we first need to compute $I(Z(c^n - d + d^n)) = \sqrt{(c^n - d + d^n)}$. To do this, note that the element $d \in k[c][d]$ is irreducible. Since $d \mid d + d^n$ but $d^2 \nmid d + d^n$, the polynomial $c^n - d + d^n \in (k[c])[d]$ is Eisenstein at $d$, hence irreducible. Thus the ideal $(c^n - d + d^n)$ is radical. We conclude that the map
\[
\varphi_0^*: k[c, d] / (c^n - d + d^n) \to \catO_X(X_0): f(c, d) \mapsto f\(\frac{x_1}{x_0}, \frac{x_2}{x_0}\)
\]
is an isomorphism of $k$-algebras.

Similarly, we have the map $\varphi_1: U_1 \to \AAA^2: (x_0 : x_1 : x_2) \mapsto (\frac{x_0}{x_1}, \frac{x_2}{x_1})$, which induces an isomorphism of $k$-algebras $\varphi_1^*: \catO_{\AAA^2}(\varphi_1(X_1)) \to \catO_X(X_1)$. Then if one picks coordinates $a, b$ on $\AAA^2$, one has
\[
\varphi_1(X_1) =  Z(1 - ba^{n - 1} + b^n) \subseteq \AAA^2.
\]
Once again we need to compute $I( Z(1 - ba^{n - 1} + b^n)) = \sqrt{ (1 - ba^{n - 1} + b^n)}$. To do this, we use the reversed Eisenstein criterion. This states that for any unique factorization domain $R$, $p \in R$ irreducible and $f \in R[X]$ such that $u$ does not divide the constant term of $f$, $u$ divides all other coefficients of $f$ and $u^2$ does not divide the lead coefficient of $f$, then $f$ is irreducible (it is easily proved by applying the Eisenstein criterion to $X^{\deg(f)}f(1 / X) \in R[X]$, which is irreducible if and only if $f$ is irreducible). Picking $R = k[b]$ we see that $b$ is irreducible in $R$, so $(-b)a^{n - 1} + b^n + 1$ is (reverse) Eisenstein at $b$ in $(k[b])[a]$ thus irreducible. We conclude that
\[
\varphi_1^*: k[a, b] / (1 - ba^{n - 1} + b^n) \to \catO_X(X_1): f(a, b) \mapsto f\(\frac{x_0}{x_1}, \frac{x_2}{x_1}\)
\]
is an isomorphism of $k$-algebras.

Next, note that $X_1 \cap X_0 \subseteq U_0$, hence $\varphi_0^*: \catO_{\AAA^2}(\varphi_0(X_0 \cap X_1)) \to \catO_{X}(X_0 \cap X_1)$ is again an isomorphism of $k$-algebras. We compute
\[
\varphi(X_0 \cap X_1) = Z(c^n - d + d^n) \cap D(c).
\]
Recall from Theorem 5.1.6 that the map
\[
\psi: Z(c^n - d + d^n) \cap D(c) \to Z(c^n - d + d^n, tc - 1) \subseteq \AAA^3: (c, d) \mapsto (c, d, 1 / c)
\]
is an isomorphism of varieties. Thus the map
\[
\varphi_0^* \circ \psi^*: \catO_{\AAA^3}(Z(c^n - d + d^n, tc - 1)) \to \catO_{X}(X_0 \cap X_1)
\]
 is an isomorphism of $k$-algebras. We thus again need to compute $I(Z(c^n - d + d^n, tc - 1))$. To show the ideal $(c^n - d + d^n, tc - 1)$ is a radical ideal, it is sufficient to show 
\[
k[c, d, t]/ (c^n - d + d^n, tc - 1) \cong k[c, d, 1 / c](c^n - d + d^n)
\]
is reduced, or $c^n - d + d^n \in k[c, d, 1 / c]$ is irreducible. To do this, suppose we have $g, h \in k[c, d, 1/ c]$ such that $g(c, d)\cdot h(c, d) = c^n - d + d^n$. Write $g = c^l\tilde{g}$ and $h = c^m \tilde{h}$ for some $\tilde g, \tilde h \in k[c, d]$ with $c \nmid \tilde g, \tilde h$. Since $c^n - d + d^m \in k[c, d]$ but $c^{-1}(c^n - d + d^m) \not \in k[c, d]$, we have $c^{l + m}\tilde{g}\tilde{h} \in k[c, d]$ yet $c^{-1}(c^{l + m} \tilde g \tilde h) \not \in k[c, d]$. Since $c \nmid \tilde g, \tilde h$ we can therefore conclude that $l + m \geq 0, l + m - 1 < 0$ respectively. Thus $l +m = 0$ and $\tilde g \tilde h = c^n - d + d^n$. But this is a factorization in $k[c, d]$, where we already knew $c^n - d + d^n$ to be irreducible. We conclude either $\tilde g$ or $\tilde h$ is a unit in $k[c, d]$, thus $g$ or $h$ is a unit in $k[c, d, 1/c]$. Since $g$ and $h$ were arbitrary, we may conclude $c^n - d + d^n$ is irredicuble in $k[c, d, 1/c]$ as well. 

Therefore the map 
\[
\varphi_0^* \circ \psi^*: k[c, d, t] / (c^n - d + d^n, tc - 1) \to \catO_{X}(X_0 \cap X_1)
\]
is an isomorphism of $k$-algebras.
We can thus find a presentation of $X$ by setting
\begin{align*}
\catO_{X, \text{pres}}(X_0) &= k[c, d] / (c^n - d + d^n), \\
\catO_{X, \text{pres}}(X_0) &= k[a, b] / (1 - ba^{n - 1} + b^n), \\
\catO_{X, \text{pres}}(X_{0, 1}) = \catO_{X, \text{pres}}(X_{1, 0}) &= k[c, d, t] / (c^n - d + d^n, tc - 1).
\end{align*}
The reason that we pick $\catO_{X, \text{pres}}(X_{0, 1}), \catO_{X, \text{pres}}(X_{1, 0})$ to be equal is that it saves us from computing another radical, with the disadvantage of our restriction maps becoming less clean. To compute the difficult restriction map, we compute for $f \in k[a, b] / (1 - ba^{n - 1} + b^n)$ the image
\begin{align*}
((\psi^*)^{-1} \circ (\varphi_0^*)^{-1})((\varphi_1^*)(f)|_{X_0 \cap X_1}) &= ((\psi^*)^{-1} \circ (\varphi_0^*)^{-1})\(f\(\frac{x_0}{x_1}, \frac{x_2}{x_1}\)\) \\
&= ((\psi^*)^{-1} \circ (\varphi_0^*)^{-1})\(f\(\frac{x_0}{x_1}, \frac{x_2 / x_0}{x_1 / x_0}\)\) & (x_0 \neq 0) \\
&= ((\psi^*)^{-1})\(f\(\frac{1}{c}, \frac{d}{c}\)\) \\
&= f(t,dt).
\end{align*}
We conclude that the restriction maps are given by 
\begin{align*}
 \catO_{X, \text{pres}}(X_0) &\to \catO_{X, \text{pres}}(X_{0, 1}): f(c, d) \mapsto f(c, d), \\
\catO_{X, \text{pres}}(X_1) &\to \catO_{X, \text{pres}}(X_{0, 1}) : f(a, b) \mapsto f(t, dt).
\end{align*}
Finally the isomorphism $\catO_{X, \text{pres}}(X_{0, 1}) \to \catO_{X, \text{pres}}(X_{1, 0})$ is obviously the identity. At last set $\catO_{X, \text{pres}}(X_{i, i}) = \catO_{X, \text{pres}}(X_i)$ with restriction map the identity again.
\newpage
\begin{center}\textbf{Is $X$ smooth?} \end{center}
Remember that $\varphi_0: X_0 \to Z(c^n - d + d^n)$ is an isomorphism of varieties. The Jacobian is given by
\[
J_{X_0} = [nc^{n - 1}, nd^{n - 1} - 1].
\]
Suppose $J_{X_0} = 0$. If $\text{char}(k) \mid n$ the second entry is nonzero. Thus we must have $c = 0$, but then we must have $d^n = d$ for $(c, d)$ to lie on the curve. Thus $d^{n - 1} = 1$ or $d = 0$. We can't have $d = 0$ since then the second entry of $J_{X_0}$ is nonzero. Thus $d^{n - 1} = 1$, and for the second entry to be zero we must have $n \equiv 1 \pmod{\text{char}(k)}$. We conclude that $X$ is smooth (of dimension $1$) on all $P \in X_0$, unless \textbf{(special case 1)}  $P = (1 : 0 : 1)$ if  $n \equiv 1 \pmod{\text{char}(k)}$ (since the only root of $X^{n - 1} - 1 = (X - 1)^{n - 1}$ is $1$). 

Remains to check that $X$ is smooth on any point in $X_1 \setminus X_0$ (since $X_0$ and $X_1$ cover $X$). These points are of the form $(0 : 1 : b)$, where $b^n = -1$. Remember that $\varphi_1: X_1 \to Z(1 - ba^{n - 1} + b^n)$ is an isomorphism of varieties. Here the Jacobian is given by
\[
J_{X_1} = [-(n - 1)a^{n - 2}b, -a^{n - 1} + nb^{n - 1}].
\]
The points of the form $(0 : 1 : b)$ correspond to the case $a = 0, b \neq 0$. If $n \not \equiv 0 \pmod{\text{char}(k)}$ we conclude that the second entry of $J_{X_1}$ is nonzero for the points we needed to check. Thus $X$ is smooth at all $P$ in $X_1$ everywhere, unless \textbf{(special case 2)}  $P = (0 : 1 : b)$ if  $n \equiv 0 \pmod{\text{char}(k)}$, for any $b \in k$ with $b^n = -1$ (note that since $k$ is algebraically closed such a $b$ always exists). 

To show that $X$ is not smooth in the special cases, note that $X = q(Z(g) \setminus \{0\})$, where $Z(g) \subseteq \AAA^3$ is given by the polynomial $g(x_0, x_1, x_2) = x_1^n - x_2(x_0^{n - 1} - x_2^{n - 1})$, and $q: \AAA^3 \setminus \{0\} \to \PP^2$ is the quotient map. Since $g$ is Eisenstein by $x_2$ in $(k[x_0, x_2])[x_1]$ it is irreducible, therefore $X$ is irreducible by Theorem 2.2.3. We show that $\dim(X) = 1$. Since $\{(1 : 0 : 1)\} \subsetneq X$ is a strict chain we have $\dim(X) \geq 1$. If $\dim(X) \neq 1$, then there is a chain of irreducible closed subsets $X_0 \subsetneq X_1 \subsetneq X_2$. Set $Y_i = q^{-1}(X_i) \cup \{0\}$. Since $X_0 \neq \emptyset$, the chain $\{0\} \subsetneq Y_1 \subsetneq Y_2 \subsetneq Y_3 = Z(g)$ would then imply that $\dim(Z(g)) \geq 3$. However, this contradicts the fact that $\dim(Z(g)) = 2$ by Proposition 1.6.6. We conclude that $\dim(X) = 1$. 

By Theorem 6.4.5(ii), $X$ is smooth at any $P \in X$ if and only if the dimension of the tangent space $X$ at $P$ is $d$. Note that if $U \subseteq X$ open and $\chi: U \to Y$ an isomorphism of varities to some $Y \subseteq \AAA^m$ closed, by the remark under Definitiion 7.2.5 the map $T_\chi: T_X(P) \to T_Y(\chi(P))$ is an isomorphism (with inverse $T_\chi^{-1}(\chi(P))$). Applying this in the special cases with $U = X_0, X_1$ resp., and noting that the dimension of the tangent space of a closed affine $Y \subseteq \AAA^m$ is $m$ minus the rank of the Jacobian, we see that in both special cases we must have $\dim T_X(P) = 2 - 0 = 2$. Thus the dimension of the tangent space of $X$ is not $1$ at any of the points $P$ in the special cases, hence by $6.4.5(ii)$ we may finally conclude that $X$ is not smooth at those points.

We conclude that $X$ is smooth unless $n \equiv 0, 1  \pmod{\text{char}(k)}$. 
\newpage
\begin{center}\textbf{Finding a presentation for $X \times X$} \end{center}
Set $Y_0 = X_0 \times X_0, Y_1 = X_0 \times X_1, Y_2 = X_1 \times X_0, Y_3 = X_1 \times X_1$. By Corollary 5.2.8, all $Y_i$ are open in $X \times X$, and they clearly cover $X \times X$. By Remark 5.2.4 we can thus set 
\begin{align*}
\catO_{X \times X, \text{pres}}(X_i \times X_j) &= \catO_{X, \text{pres}}(X_i) \otimes \catO_{X, \text{pres}}(X_j) 
\end{align*}
for any $i, j \in \{0, 1\}$ (since everything is already of the form in Definition 5.2.2). 
Note that $Y_{i, j} = A_{i,j} \times B_{i,j} $ for some $A_{i,j}, B_{i,j} \in \{X_0, X_1, X_{0, 1}\}$. Then set 
\[
\catO_{X \times X, \text{pres}}(A_{i,j} \times B_{i,j}) = \catO_{X, \text{pres}}(A_{i,j}) \otimes \catO_{X, \text{pres}}(B_{i,j}).
\]
Finally, note that if $Y_i = A \times B$ and $Y_j = C \times D$ for $A, B, C, D \in \{X_0, X_1, X_{0, 1}\}$, when creating the presentation for $X$ we already found some restriction maps 
\begin{align*}
r_{i,j}: \catO_{X, pres}(A) \to \catO_{X, pres}(A \cap C) = \catO_{X, pres}(A_{i,j}),\\
t_{i,j}: \catO_{X, pres}(B) \to \catO_{X, pres}(B \cap D) = \catO_{X, pres}(B_{i,j}) .
\end{align*}
Thus we can create a restriction map 
\[
r_{i,j} \otimes t_{i,j}: \catO_{X \times X, \text{pres}}(Y_i) \to \catO_{X \times X, \text{pres}}(Y_{i,j}).
\]
Finally, one can set the isomorphism $\catO_{X \times X, \text{pres}}(Y_{i,j}) \to \catO_{X \times X, \text{pres}}(Y_{j,i}) := \id \otimes \id$ (here we once again profit from the fact that all isomorphisms on our presentation for $X$ were the identity). This completes our presentation of $X$.
\end{opghand}

\begin{opghand}[6.6.8] \
\begin{enumerate}[i)]
\item To show that $\delta_0$ is injective, suppose $f \in \ker(\delta_0)$. Then $f|_{X_i} = 0$ for all $i \in I$. Since the $X_i$ cover $X$ and we know $\catO_X$ to be a sheaf, we conclude (since $0|_{X_i} = 0$ for all $i$ as well) by the uniqueness property of sheaves that $f = 0$. Thus $\delta_0$ is injective.

To show that $\im(\delta_0) \subseteq \ker(\delta_1)$, note that 
\[
(\delta_1 \circ \delta_0(f))_{ij} = (f|_{X_i}|_{X_{ij}} - f|_{X_j}|_{X_{ij}})_{ij} = ( (f|_{X_{ij}} - f|_{X_{ij}})_{ij} = (0)_{ij}.
\]

Conversely, to show $\ker(\delta_1) \subseteq \im(\delta_0)$, pick $(f_i)_{i \in I} \in \ker(\delta_1)$. Then for all $i, j \in I$ we have $(\delta_1(f))_{ij} = 0$, so $f_i|_{X_{ij}} = f_{j}|_{X_{ij}}$. By the extension property of the sheave $\catO_X$, there is $f \in \catO_X(X)$ with $f|_{X_i} = f_i$. Thus $\delta_0(f) = (f_i)_{i \in I}$ and $\ker(\delta_1) \subseteq \im(\delta_0)$. We conclude $\im(\delta_0) = \ker(\delta_1)$.
\item Let $f \in \catO_X(X)$. By Theorem 4.2.5, $f$ is locally constant. Write $f_i = \delta_0(f)_i$. Since we know that $\catO_X(X_0) \cong k[c, d] / (c^n - d + d^n)$, and we have shown the polynomial $(c^n - d + d^n)$ to be irreducible, we know that $Z(c^n - d + d^n) \subseteq \AAA^2$ is irreducible. For any $x \in \im(f)$, the set $Z(f(a, b) - x)$ is closed, but also open (since for any $x \in X$ there is $U \subseteq X$ open containing $x$ such that $f|_U$ is constant). Thus $Z(c^n - d + d^n) = Z(f(a, b) - x) \sqcup Z(f(a, b) - x)^c$. Since $Z(c^n - d + d^n)$ is irreducible, we conclude that either $ Z(f(a, b) - x)^c = Z(c^n - d + d^n) $ or $Z(f(a, b) - x) = Z(c^n - d + d^n)$. Thus $f|_{X_0}$ is constant, and similarly $f|_{X_1}$ is constant. Since $(1, 2^\frac{1}{n - 1}, 1) \in X_0 \cap X_1$ we conclude that $f$ is constant.

\item First consider the set
\[
C = \{c^id^j \mid j \in \ZZ_{\geq 0}, i \in \{0,\dots, n - 1\}\} \subseteq k[c, d] / (c^n - d + d^n).
\]
We claim that $C$ is a basis for $k[c, d] / (c^n - d + d^n)$. To see that it is spanning, it suffices to show the set $\{c^i d^j \mid i, j \in \ZZ_{\geq 0}\}$ is in its span. And this can easily be shown by induction on $i$, since if $i> n$ then $c^i d^j = -c^{i - n}d^i(d - d^n)$, in which the power of $c$ is of order $i - n < i$ in all summands. To see that it is linearly independent, note that if there is some finite sum $\sum a_{i,j} c^i d^j \in (c^n - d + d^n)$, then there is $g(c, d) \in k[c, d]$ with $\sum a_{i,j} c^i d^j = g(c, d)(c^n - d + d^n)$. Since $\deg_{(k[c])[d]}(\sum a_{i,j} c^i d^j) < n$, we conclude $g = 0$, therefore all $a_{i, j}$ are $0$.

By a completely argument, the sets
\begin{align*}
D = \{a^ib^j \mid i \in \ZZ_{\geq 0}, j \in \{0,\dots, n - 1\}\} \subseteq k[a, b] / (1 - ba^{n - 1} + b^n), \\
E = \{c^id^j \mid i \in \ZZ, j \in \{0,\dots, n - 1\}\} \subseteq k[c, d, c^{-1}] / (c^n - d + d^n).
\end{align*}
also form bases (to show they are spanning one actually needs that $n > 1$).

Note that $\delta_1$ is a linear map, and $\delta_1((a^i b^j), (c^l d^k)) = c^{i - j}d^j - c^ld^k$. It is then not too hard to see that $\text{im}(\delta_1) = \text{span}\{c^i d^j \mid j \in \{0, \dots, n - 1\}, i \in \ZZ \setminus \{-1, \dots, -j\}\}$. Thus the set $\{c^i d^j \mid j \in \{0, \dots, n - 1\}, i \in \{-1, \dots, -j\}\}$ forms a basis for the cokernel, and the cokernel has dimension
\[
\sum_{j = 0}^{n - 1} |\{-1, \dots, -j\}| = \sum_{j = 1}^{n - 1}j = \frac{(n - 1)(n - 2)}{2},
\]
as required.
\end{enumerate}
\end{opghand}
\end{document}
