\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{stackengine}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\AAA}{\mathbb{A}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catF}{\mathcal{F}}
\newcommand{\catG}{\mathcal{G}}
\newcommand{\catO}{\mathcal{O}}
\newcommand{\catE}{\mathcal{E}}
\newcommand{\catH}{\mathcal{H}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand{\twomatrix}[4]{\begin{pmatrix} #1 & #2 \\ #3 & #4 \end{pmatrix}}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Geometry Homework 6}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[8.6.5] \
\begin{enumerate}[i.]
\item If $f$ is surjective we are done. Otherwise, there is $(a : b) \in \PP^1$ that is not in the image of $f$. Note that $\PP^1 \setminus (a : b) = D(xb - ya)$ is an open subvariety of $\PP^1$. Therefore, let
\[
\varphi: \PP^1 \setminus (a : b) \to \AAA^1: (x : y ) \mapsto \begin{cases}
y / x & \text{ if } a = 0, \\
x / y & \text{ if } b = 0, \\
\frac{x}{xb - ay} & \text{ otherwise.}
\end{cases}
\]
In the case $a = 0$ or $b = 0$ we see that $\varphi$ is just one of our usual charts for $\PP^1$, which we know to be an isomorphism of varieties. In the case $a, b \neq 0$ we see that $\varphi$ is an injective morphism of varieties, since if $\varphi((x : y)) = \varphi((z : w))$, then if $x = 0$ we must have $\varphi((z : w)) = 0$, hence $z = 0$, and $(x : y) = (z : w)$. Similarly if $z = 0$ we have $x = 0$. Finally if $x, z \neq 0$ then
\begin{align*}
\frac{1}{b - a \frac{y}{x}} &= \frac{1}{b - a \frac{w}{z}} \implies b - a \frac{y}{x} = b - a \frac{w}{z} \implies \frac{y}{x} = \frac{z}{w} &(a \neq 0),
\end{align*}
hence $(y : x) = (z : w)$. 

Therefore $\varphi \circ f: X \to \AAA^1$ is a morphism of varieties. Since $X$ is projective it is isomorphic to a closed subset of a projective variety. Therefore $\varphi \circ f$ is locally constant by some theorem. It follows that $\varphi \circ f$ is constant since $X$ is irreducible. Since $\varphi$ is always injective, $f$ must be constant.
\item Note that $(1 : 0) = Z(y)$ is closed in $\PP^1$. Therefore $f^{-1}((1 : 0))$ is closed in $X$, thus $U$ is open in $X$. Since $f|_U$ maps to $k$ by definition of U, it follows that $\tilde f = [(f|_U, U)]$ defines an element of $K(X)$. 

\item Clearly $f \mapsto \tilde{f}$ is injective, since if the image of $f$ and $g$ is not $\{(1 : 0)\}$, then $U_f = f^{-1}((1 : 0))^c$ and $U_g = g^{-1}((1 : 0))^c$ are nonempty. Thus if $\tilde{f} = \tilde{g}$ one has $[(f|_{U_f}, U_f)] = [(g|_{U_g}, U_g)]$, or $f|_{U_f \cap U_g} = g|_{U_f \cap U_g}$. Since $X$ is irreducible, $U_f \cap U_g$ is nonempty. Thus $f$ and $g$ agree on a nonempty open subset. Since $X$ is irreducible it follows that $f = g$ (the set $Z(f - g) \subseteq X$ is closed, and $X$ is covered by $(U_f \cap U_g)^c \cup Z(f - g)$. Since $U_f \cap U_g$ is nonempty, by irreducibility of $X$ it follows that $X = Z(f - g)$). 

Remains to show $f \mapsto \tilde{f}$ is surjective. Thus let $[(g, U)] \in K(X)$. If $g = 0$, then $g = \tilde{0}$. Otherwise $g \in K(X)^*$, thus let $U_1 = \{P \in \PP^1 \mid v_P(g) \leq 0\}$ and $U_2 = \{P \in \PP^1 \mid v_P(g) \geq 0\}$. Obviously $U_1$ and $U_2$ cover $X$ since any integer is either at least or at most $0$. Since $U_1$ and $U_2$ only miss finitely many points of $\PP^1$ by Lemma 8.2.8 and any point is closed, $U_1$ and $U_2$ must be open. Consider the morphisms of varieties
\begin{align*}
\varphi_1: k &\to \PP^1: x \mapsto (x : 1) \\ 
\varphi_2: k &\to \PP^1: x \mapsto (1 : x).
\end{align*}
Note that $\varphi_1 \circ g= \varphi_2 \circ 1 / g$ as morphisms $U_1 \cap U_2 \to \PP^1$. By the universal property of a glued variety, they glue to a unique morphism $f: X \to \PP^1$ (since $U_1$ and $U_2$ cover $X$) satisfying $f|_{U_1} = \varphi_1 \circ g$ and $f|_{U_2} = \varphi_2 \circ 1 / g$. Note that $f|_{U_1}$ can also be considered as a morphism $U_1 \to k$ as it does not hit $(1 : 0)$. Since $\varphi_1$ is injective we actually have $f|_{U_1} = g$, hence $\tilde{f} = g$. Note that $\im(f) \neq \{(0 : 1)\}$ since in that case $U_2$ would be empty, which cannot happen as $v_P(g) \neq 0$ on only finitely many points $P$, yet $\PP^1$ has infinitely many points as $k$ is algebraically closed. We thus have proved surjectivitiy of $f \mapsto \tilde{f}$, which completes what we needed to show.
\item Let $U = f^{-1}((1 : 0))^c$. By definition $\tilde{f} = [(f|_U, U)] \in K(\PP^1)$. Since $x = [(x, \AAA^1)] \in K(\PP^1)$ corresponds to the identity map $\AAA^1 \to k$, the map $f^*(x)|_U = \id_{k} \circ f|_U = f|_U = \tilde{f}$. Since $f$ is bijective, $U$ is nonempty. Thus $f^*(x)$ and $\tilde{f}$ agree on a nonempty open subset of $\PP^1$, since $\PP^1$ is irreducible we conclude they must be equal in $K(\PP^1)$. 

\item Since $f$ is an isomorphism of varieties, one has $\text{div}(f^*(x)) = f^{-1}(0) - f^{-1}(\infty)$ (since one has $v_P(x) = v_{f^{-1}(P)}(f^*(x))$, as the definition of the order is independent of isomorphisms). Since $f$ is surjective the image of $f$ is definitely not $\{(1 : 0)\}$, hence by (iii) the map $\tilde{f}$ is in $K(X) = Q(k[x])$. Since $k$ is algebraically closed and $k[x]$ is a unique factorization domain, there are thus $\alpha_1, \dots, \alpha_n \in k[x]$ and $\beta_1, \dots, \beta_m \in k[x]$ and $\lambda \in k$ such that
\[
\tilde f = \lambda \frac{(x - \alpha_1) \cdots (x - \alpha_n)}{(x - \beta_1) \cdots (x - \beta_m)}.
\]
Cancelling common factors we may assume that none of the $\alpha_i$ are equal to any of the $\beta_j$. Note that the order of $f$ is precisely $1$ in one point, $-1$ in one other point, and $0$ everywhere else, since $\text{div}(f^*(x)) = f^{-1}(0) - f^{-1}(\infty)$. If any of two of the $\alpha_i, \alpha_j$ were to be equal, then $v_{\alpha_i}(f) \geq 2$, which isn't the case. Similarly none of the two $\beta_i$ are equal. Since $\tilde{f}$ has order $1$ at all of the $\alpha_i$ and order $-1$ at all of the $\beta_i$, we conclude that there can be at most one $\alpha_i$ and $\beta_i$. We conclude that $\tilde{f}$ has one of the forms
\[
\lambda, \qquad \lambda(x - \alpha), \qquad \frac{\lambda}{x - \beta}, \qquad \lambda\frac{x - \alpha}{x - \beta}.
\]
The first case cannot appear since $f$ is not constant. The other cases are of the required form by picking 
\begin{align*}
(a, b, c, d) &= (\lambda, - \lambda \alpha, 0, 1) \\
(a, b, c, d) &= (0, \lambda, 1, -\beta) \\
(a, b, c, d) &= (\lambda, -\lambda \alpha, 1, -\beta)
\end{align*}
respectively. (Note that the first and second form have their pole/zero at infinity). 

Now note that on $\AAA^1 \subseteq \PP^1$, one has 
\[
\tilde{f}(x : 1) = (ax + b, cx + d) = q\(\begin{pmatrix} a & b \\ c & d \end{pmatrix}\begin{pmatrix}x \\ 1 \end{pmatrix}\).
\]
Therefore on $\AAA^1$, $\tilde{f}$ and the class of the element $\(\begin{smallmatrix} a & b \\ c & d \end{smallmatrix}\) \in \text{PGL}_2(k)$ act as the same function. Since $\AAA^1$ is a nonempty open set of $\PP^1$ and $\PP^1$ is irreducible, we conclude that $f = \(\begin{smallmatrix} a & b \\ c & d \end{smallmatrix}\)$ (as automorphisms of $K(\PP^1)$).
\end{enumerate}
\end{opghand}
\end{document}
