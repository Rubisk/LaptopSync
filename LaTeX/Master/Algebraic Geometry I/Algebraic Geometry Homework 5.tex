\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{stackengine}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\AAA}{\mathbb{A}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catF}{\mathcal{F}}
\newcommand{\catG}{\mathcal{G}}
\newcommand{\catO}{\mathcal{O}}
\newcommand{\catE}{\mathcal{E}}
\newcommand{\catH}{\mathcal{H}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand{\twomatrix}[4]{\begin{pmatrix} #1 & #2 \\ #3 & #4 \end{pmatrix}}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Geometry Homework 5}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[7.9.12]
Before anything, we start by giving a presentation for $X$ and introducing some notation. Choose coordinates $(x_0 : x_1 : x_2)$ on $\PP^2$ and denote as usual $U_0 = D(x_0) \cap X$, $U_2 = D(x_2) \cap X$. Let $f_2(x_0, x_1) = x_1^n - x_0^{n - 1} + 1$, then $f_2$ is irreducible (Eisenstein by $y - 1$ as $\text{char}(k) \nmid n$), and the map
\begin{align*}
\varphi_2: U_2 &\to Z(f_2)  \\
(x_0 : x_1 : x_2) &\mapsto \(\frac{x_0}{x_2}, \frac{x_1}{x_2}\)
\end{align*}
is clearly an isomorphism of varieties, inducing an isomorphism $\varphi_2^*: k[x_0, x_1] / (f_2) \to \catO_X(U_2)$. Similarly, set $f_0(x_1, x_2) = x_1^n - x_2 + x_2^n$. Then $f_0$ is irreducible (Eisenstein by $x_2$), and the map 
\begin{align*}
\varphi_0: U_0 &\to Z(f_0)  \\
(x_0 : x_1 : x_2) &\mapsto \(\frac{x_1}{x_0}, \frac{x_2}{x_0}\)
\end{align*}
is again an isomorphism of varieties, inducing an isomorphism $\varphi_0^*: k[x_1, x_2] / (f_0) \to \catO_X(U_0)$. Note that
\begin{align*}
(\varphi_0^{-1})^* \circ \varphi_2^*: k[x_0, x_1] / (f_2) \to k[x_1, x_2] / (f_0): f(x_0, x_1) \mapsto f\(\frac{1}{x_2}, \frac{x_1}{x_2}\), \\
(\varphi_2^{-1})^* \circ \varphi_0^*: k[x_1, x_2] / (f_0) \to k[x_0, x_1] / (f_2): f(x_1, x_2) \mapsto f\(\frac{x_1}{x_0}, \frac{1}{x_0}\).
\end{align*}
Finally $\varphi_2$ also induces an isomorphism of varieties
\begin{align*}
\varphi_2: U_2 \cap U_0 &\to Z(f_2) \cap D(x_0) \\
(x_0 : x_1 : x_2) &\mapsto \(\frac{x_0}{x_2}, \frac{x_1}{x_2}\)
\end{align*}
which (as shown in the previous homework) gives an induced isomorphism 
\[
\varphi_2^*: k[x_0, x_1, \frac{1}{x_0}] / (f_2) \to \catO_X(U_2 \cap U_0).
\]
Note that in the exercise, one has $U = U_2$, $f = f_2$ and $(x, y) = (x_1, x_2)$. We are now ready to start computing the solutions. We will often drop the isomorphisms $\varphi_0$ and $\varphi_2$ between the zero sets and open sets (and talk about those as if they are ``the same''), only once we use multiple isomorphisms will we explicitly state it.
\newpage
\begin{enumerate}[i.] 
\item By Example 7.3.6 we have
\[
\Omega_U^1(U) = \Omega_{k[x_0, x_1]}^1 \bigg / \(k[x_0, x_1]\cdot df_2\).
\]
We compute (using the Leibniz rule)
\begin{align*}
df_2 = n x_1^{n - 1}dx_1 - (n - 1)x_0^{n - 2}dx_0.
\end{align*}
Since we quotient out by $df_2 $, we have $df_2  = 0$ in $\Omega^1(U)$. But the latter is equivalent to the statement that $ n x_1^{n - 1}dx_1 =(n - 1)x_0^{n - 2}dx_0$.
\item On $U_2 \cap D(x_1)$, the function $\frac{1}{(n - 1)x_1^{n - 2}}$ is regular and $dx_2$ is a regular $1$-form, hence $\frac{1}{(n - 1)x_1^{n - 2}}dx_2$ is a regular $1$-form. By the exact same argument $\frac{dx_1}{nx_2^{n - 1}}$ is a regular $1$-form on $U \cap D(x_2)$. Since they agree on $U \cap D(x_1) \cap D(x_2)$ by (i) and $U = (U \cap D(x_1)) \cup (U \cap D(x_2))$ since $(0, 0) \not \in U$, we conclude (using the sheaf property of $\Omega_1(U)$ that there exists a regular $1$-form $\omega_0$ on $U$. 
\item Let $P = (a, b) \in  U \cap D(x_0)$. Note that
\[
\frac{d}{dx_0}f_2 \bigg \rvert_{(a, b)} = -(n - 1)a^{n - 2} \neq 0,
\]
since $(a, b) \in D(x)$. Thus by Exercise 7.9.10, $x_1- x_1(P)$ is a uniformiser at $P$. 
\item Let $P = (a, b) \in  U \cap D(y)$. Note that
\[
\frac{d}{dx_1}f_2\bigg \rvert_{(a, b)} = nb^{n - 1} \neq 0,
\]
since $(a, b) \in D(y)$. Thus by Exercise 7.9.10, $x_0 - x_0(P)$ is a uniformiser at $P$. 
\item Note that $(0, 0) \not \in U$, thus $U = (U \cap D(x_0)) \cup (U \cap D(x_1))$. 

If $P = (a, b) \in U \cap D(x_1)$, then $x_0 - a$ is a uniformiser at $P$, and $d(x_0 - a) = dx_0$. Thus write $\omega_0 = \frac{1}{nx_1^{n - 1}}dx_0$. Now $1 / nx_1^{n - 1}$ is regular at $P$ (since $P \in D(x_1)$) and nonzero at $P$. Thus the order of $\omega_0$ at $P \in U \cap D(x_1)$ is $0$. 

Similarly, if $P = (a, b) \in U \cap D(x_0)$, then $x_1 - b$ is a uniformiser at $P$, and $d(x_1 -  b) = dx_1$. Thus write $\omega_0 = \frac{1}{(n - 1)x_0^{n - 2}}dx_1$. Now $\frac{1}{(n - 1)x_0^{n - 2}}$ is regular at $P$ (since $P \in D(x_0)$) and nonzero at $P$. Thus the order of $\omega_0$ at $P \in U \cap D(x_0)$ is $0$ as well. We conclude that $v_P(\omega_0) = 0$ for all $P \in U$. 
\item We claim that $x_1$ is a uniformiser for the point $(0, 0) \in U_0$. Indeed, one computes
\[
\frac{d}{dx_2}(x_1^n - x_2 + x_2^n) \bigg \rvert_{(0, 0)} = (nx_2^{n - 1} - 1) \bigg \rvert_{(0, 0)} = 0,
\]
since $n > 1$. By exercise $7.9.10$, $x_1$ is a uniformiser at ${(0, 0)}$ in $U_0$. Thus we may conclude that $(\varphi_2^{-1} \circ \varphi_0)^*(x_1) = x_1 / x_0$ is a uniformiser at $Q$ in $U_2$. 
\item Clearly $(\varphi_0^{-1} \circ \varphi_2)^*(x_0) = 1 / x_2$. Note that in $\catO_X(U_0)$ one has $x_2 = x_1^n + x_2^n$, hence
\[
x_1^n \cdot 1 / x_2 = \frac{x_1^n}{x_1^n + x_2^n} = \frac{1}{1 + (x_2 / x_1)^n} = \frac{1}{1 + (x_2^n / (x_2 - x_2^n))} =  1 - x_2^{n - 1}. 
\]
Since $1 - x_2^{n - 1}$ is a regular function on $\catO_X(V)$ that is not zero in $(0, 0)$, and it has been shown above that $x_1$ is a uniformiser at $(0, 0)$ in $U_0$, we conclude that 
\[
v_{Q, U_2}(x_0) = v_{(0, 0), U_0}(1  / x_2) = -n.
\]

Similarly $(\varphi_0^{-1} \circ \varphi_2)^*(x_1) = x_1 / x_2$. Thus 
\[
v_Q(x_1) = v_{(0, 0)}(x_1 / x_2) = v_{(0, 0)}(x_1) + v_{(0, 0)}(1 / x_2) = 1 - n
\]
where we use that $v_{(0, 0)}$ is a valuation, see Remark 7.8.5. 

\item We compute
\[
(\varphi_0^{-1} \circ \varphi_2)^*(\omega_0) = \frac{d(1 / x_2)}{n(x_1 / x_2)^{n - 1}} = -\frac{x_2^{n - 3}}{nx_1^{n - 1}}dx_2,
\]
Now note that
\[
dx_2 = d(x_2^n + x_1^n) = nx_2^{n - 1}dx_2 + nx_1^{n - 1}dx_1 \implies dx_2 = \frac{nx_1^{n - 1}}{1 - nx_2^{n - 1}}dx_1.
\]
Thus
\[
(\varphi_0^{-1} \circ \varphi_2)^*(\omega_0)  = -\frac{x_2^{n - 3}}{1 - nx_2^{n - 1}}dx_1. 
\]
Therefore 
\[
v_Q(\omega_0) = v_{(0, 0)}((\varphi_0^{-1} \circ \varphi_2)^*(\omega_0)) = v_{(0, 0)}\(-\frac{x_2^{n - 3}}{1 - nx_2^{n - 1}}\)
\]
since $x_1$ is a uniformiser at $(0, 0)$. Since $v_{(0, 0)}$ is a valuation and $\frac{1}{1 - nx_2^{n - 1}}$ is regular and nonzero at $(0, 0)$, we have 
\[
v_Q(\omega_0) = v_{(0, 0)}(x_2^{n - 3}) =  n(n - 3),
\]
which is what we needed to compute.
\item First consider the set
\[
C = \{x_0^ix_1^j \mid i \in \ZZ_{\geq 0}, j \in \{0,\dots, n - 1\}\} \subseteq \catO(U_2).
\]
We claim that $C$ is a basis for $\catO(U_2)$. To see that it is spanning, it suffices to show the set $\{x_0^ix_1^j \mid i, j \in \ZZ_{\geq 0}\}$ is in its span. Any this can easily be shown by induction on $j$, since if $j > n$ then $x_0^i x_1^j = x_0^i(x_0^{n - 1} - 1)x_1^{j - n}$, in which the power of $y$ is of order $j - n < j$ in all summands. To see that it is linearly independent, note that if there is some finite sum $\sum a_{i,j} x_0^ix_1^j \in (x_1^n - x_0^{n - 1} + 1)$, then there is $g \in k[x_0, x_1]$ with $\sum a_{i,j} x_0^i x_1^j = g(x_1^n - x_0^{n - 1} + 1)$. Since $\deg_{(k[x_0])[x_1]}(\sum a_{i,j} x_0^i x_1^j) < n$, we conclude $g = 0$, therefore all $a_{i, j}$ are $0$.

Now using that $v_Q$ is a valuation, we compute $v_Q(x_0^ix_1^j) = iv_Q(x_0) + jv_Q(x_1) = -ni + j(1 - n)$. Now suppose $v_Q(x_0^ix_1^j) = v_Q(x_0^{i_0}x_1^{j_0})$ for some $i, i_0 \geq 0$ and $j, j_0 \in \{0, \dots, n - 1\}$. Then $-ni + j(1 - n) = -ni_0 + j_0(1 - n)$, and $j \equiv j_0 \pmod{n}$. Since $0 \leq j, j_0 < n$ we conclude $j = j_0$, it follows that $i = i_0$ as $n \neq 0$. Thus all $v_Q(x_0^ix_1^j)$ are distinct.
\item Note that on $Z(f_0) \cap D(x_1)$ one has
\[
dx_1 = \frac{1 - nx_2^{n - 1}}{nx_1^{n - 1}}dx_2
\]
thus $\Omega^1(Z(f_0) \cap D(x_1))$ is a free $\catO(Z(f_0)  \cap D(x_1))$-module with basis 
\[
\omega_{1,0} := \frac{1}{nx_1^{n - 1}}dx_2.
\]
Similarly, on $Z(f_0)  \cap D(1 - nx_2^{n - 1})$ one has
 \[
dx_2 = \frac{nx_1^{n - 1}}{1 - nx_2^{n - 1}}dx_1,
\]
thus $\Omega^1(Z(f_0)  \cap D(1 -n x_2^{n - 1}))$ is a free $\catO(Z(f_0)  \cap D(1 - nx_2^{n - 1}))$-module with basis 
\[
\omega_{1,1} :=  \frac{1}{1 - nx_2^{n - 1}}dx_1.
\]
Since the forms $\omega_{1,0}$ and $\omega_{1,1}$ agree on $Z(f_0) \cap D(x_1) \cap D(1 - nx_2^{n - 1})$ (see above) they glue to a unique form $\omega_1$ on $Z(f_0) $. Thus $\Omega^1(Z(f_0) )$ is a free $\catO(Z(f_0) )$-module with basis $\omega_1$. 

By definition, any form $\alpha \in \Omega^1(X)$ is a pair of forms $(\alpha_{U_0}, \alpha_{U_2}) \in \Omega^1(U_0) \oplus \Omega^1(U_2)$ for which $\alpha_{U_0}|_{U_0 \cap U_2} = \alpha_{U_2}|_{U_0 \cap U_2}$. 
Thus there is an isomorphism of $k$-vector spaces
\[
\Omega^1(X) \cong \ker\(\begin{array}{l}\Omega^1({U_0}) \oplus \Omega^1({U_2}) \to \Omega^1({U_0} \cap {U_2})\\(\alpha_{U_0}, \alpha_{U_2}) \mapsto \alpha_{U_0}|_{{U_0} \cap {U_2}} - \alpha_{U_2}|_{{U_0} \cap {U_2}}\end{array}\).
\]
Recall that $C$ was a basis for $\catO(Z(f_2) )$. By a  similar argument, the sets
\begin{align*}
D &= \{x_1^ix_2^j \mid i \in \ZZ_{\geq 0}, j \in \{0,\dots, n - 1\}\} \subseteq \catO(Z(f_0) ) \cong \catO(U_0), \\
E &= \{x_0^ix_1^j \mid i \in \ZZ, j \in \{0,\dots, n - 1\}\} \subseteq \catO(Z(f_2) \cap D(x_0)) \cong \catO(U_0 \cap U_2).
\end{align*}
are bases. Note that under the map we need to analyze,
\[
(f(x_0, x_1) \omega_0, g(x_1, x_2) \omega_1) \mapsto 0 \iff (\varphi_2^{-1} \circ \varphi_0)^*(g(x_1, x_2) \omega_1) =  f(x_0, x_1) \omega_0. 
\]
We now recall from (viii) that
\[
(\varphi_2^{-1} \circ \varphi_0)^*(-x_2^{n - 3}\omega_1)  = \omega_0.
\]
Since $\omega_0$ also gives a nowhere vanishing regular form on $Z(f_2) \cap D(x_0)$, $\Omega^1(Z(f_2) \cap D(x_0))$  is a free $k[x_0, x_1, 1 / x_0]$-module with basis $\omega_0$. One thus has
\[ 
(f(x_0, x_1) \omega_0, g(x_1, x_2) \omega_1) \mapsto 0 \iff (\varphi_2^{-1} \circ \varphi_0)^*\(-\frac{1}{x_2^{n - 3}}g(x_1, x_2)\) =  f(x_0, x_1). 
\]
Therefore our problem is reduced to finding a basis of 
\begin{equation}
\left \{(f, g) \in \catO(Z(f_0)) \oplus \catO(Z(f_2)) \mid (\varphi_2^{-1} \circ \varphi_0)^*\(-\frac{1}{x_2^{n - 3}}g\) =  f\right \},
\end{equation}
since if $\{(\alpha_\eta, \beta_\eta)\}_{\eta=1}^\theta$ is a basis for this vector space, then $\{(\alpha_\eta \omega_0, \beta_\eta \omega_1)\}_{\eta=1}^\theta$ is a basis for $\Omega^1(X)$. 

Next, we compute for any basis element $x_1^i x_2^j\in D$ that
\[
 (\varphi_2^{-1} \circ \varphi_0)^*\(-\frac{1}{x_2^{n - 3}}x_1^i x_2^j\) = -\frac{(x_1 / x_0)^i(1 / x_0)^j}{(1/x_0)^{n - 3}} = -x_0^{n - 3 - i - j}x_1^{i}.
\]
Thus if one writes in terms of bases $D$ and $C$ respectively (the infinite sums terminate at some point)
\begin{align*}
g(x_1, x_2) &= \sum_{i = 0}^\infty \sum_{j = 0}^{n - 1} a_{ij}x_1^ix_2^j , \qquad 
f(x_0, x_1) = \sum_{i = 0}^\infty \sum_{j = 0}^{n - 1} b_{ij}x_0^ix_1^j
\end{align*}
then we need to solve
\[
\sum_{i = 0}^\infty \sum_{j = 0}^{n - 1} -a_{ij}x_0^{n - 3 - i - j}x_1^{i} = \sum_{i = 0}^\infty \sum_{j = 0}^{n - 1} b_{ij}x_0^ix_1^j.
\]
By comparing the coefficients of the monomials, one sees that the dimension of the solution space of these equations is equal to 
\[
\#\{(i, j) \in \{0, \dots, n - 1\}^2 \mid n - 3 - i - j \geq 0\}. 
\]
If $ n = 2$, there are no such pairs. If $n = 3$ we only have the solution $(i, j) = (0, 0)$, hence the dimension is $1$. If $n = 4$, there are $3$ pairs ($(0, 0), (1, 0), (0, 1)$) hence the dimension is $3$.
\end{enumerate}
\end{opghand}
\end{document}
