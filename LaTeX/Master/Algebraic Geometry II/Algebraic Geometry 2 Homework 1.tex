\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}

% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\cont}{cont}
\DeclareMathOperator{\Spec}{Spec}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\catO}{\mathcal{O}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Geometry 2 Homework 1}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1] \
\begin{enumerate}[(i)] 
\item Clearly $V(P)$ is a closed set containing $P$ hence $\overline{\{P\}} \subseteq V(P)$. Conversely, let $C \subseteq \Spec(R)$ be a closed set containing $P$. Then there is $I \subseteq R$ such that $V(I) = C$. Hence $I \subseteq P$ and $V(P) \subseteq V(I) = C$. It follows that $V(P) \subseteq \overline {\{P\}}$ since the closure is the intersection of all closed sets containing $P$.
\item Suppose $V(P) = C_1 \cup C_2$ for some closed sets $C_1, C_2 \in \Spec(R)$. Let $I_1, I_2 \subseteq R$ such that $V(I_i) = C_i$. Without loss of generality we may assume $P \in C_1$ as $P \in V(P)$. Hence $I_1 \subseteq P$ and $V(P) \subseteq V(I_1) = C_1$. Thus $V(P) = C_1$ and $V(P)$ must be irreducible. 

By (i) $P$ is a generic point of $V(P)$. Conversely, let $Q$ be any generic point of $V(P)$. Then $\overline {\{Q\}} = V(P)$. But by (1), $V(Q) = \overline{\{Q\}}$.  Hence $V(Q) = V(P)$ and $P \in V(Q)$, thus $Q \subseteq P$. Since $Q \in V(P)$ also $P \subseteq Q$, hence $P = Q$. Thus $P$ is the unique generic point.
\item Let $Z \subseteq R$ be an irreducible closed subset. Let $I \subseteq R$ be such that $V(I) = Z$. We clam $V(I) = V(\sqrt{I})$. Recall from commutative algebra that $\sqrt{I} = \bigcap _{I \subseteq P} P$ (this is equal to the statement that $\text{Nil}(R) = \bigcap_P P$ after passing to $R / I$, which was one of the proofs we had to learn for the exam). Thus if $P \in V(I)$, then $I \subseteq P$ hence $\sqrt{I} \subseteq P$ as well. Conversely if $\sqrt{I} \subseteq P$ then $I \subseteq \sqrt{I} \subseteq P$. Hence $V(I) = V(\sqrt{I})$. 

We know claim $\sqrt{I}$ is prime. Thus suppose $a, b \in R$ such that $ab \in \sqrt{I}$. Then there exists an $n > 0$ such that $(ab)^n \in I$. Therefore $V(I) \subseteq V(((ab)^n)) \subseteq V((a)) \cup V((b))$ (since any prime ideal containing $(ab)^n$ contains either $a$ or $b$ as it is prime. Hence 
\[
V(I) = (V((a)) \cap V(I)) \cup (V((b)) \cap V(I)) = V((a) + I) \cup V((b) + I).
\]
Since $V(I) = Z$ is irreducible, either $V(I) = V((a) + I)$ or $V(I) = V((b) + I)$. Thus either $a$ or $b$ is contained in any prime ideal $P$ containing $I$, and hence one of $a$ or $b$ is contained in $\bigcap_{I \subseteq P}P = \sqrt{I}$. Therefore $\sqrt{I}$ is prime.

Thus $Z = V(\sqrt{I})$ suffices.
\end{enumerate}
\end{opghand}
\begin{opghand}[2] Recall that 
\[
\Gamma(U, \catO_X) := \left \{\{s_P\}_{P \in U} \mid \substack{\exists \text{ open cover } \bigcup X_{f_\alpha} = U \\ \exists\bigcup X_{f_\alpha} = U  \text{ such that $s_P$ is the image} \\\ \text{ of $s_\alpha$ whenever $[P] \in X_{f_\alpha}$} \\  } \right \}
\]
It thus suffices to, given a sequence $\{s_P\}_{P \in U}$ with an open cover $\bigcup X_{f_\alpha} = U $ and elements $s_\alpha \in R_{f_\alpha}$ restricting to $s_P$ whenever $[P] \in X_{f_\alpha}$, construct an open cover $V = \bigcup X_{h_\delta}$ and elements $s_\delta \in R_{h_\delta}$ restricting to $s_P$ whenever $[P] \in X_{h_\delta}$. 

Let thus $V = \bigcup X_{g_\beta}$ be any cover of $V$ by base opens. Since $V \subseteq U$ we can also cover $V$ by $\bigcup_{\alpha, \beta} X_{f_\alpha} \cap X_{g_\beta} = \bigcup_{\alpha, \beta} X_{f_\alpha g_\beta}$. We can then pick $s_{\alpha \beta} \in R_{f_\alpha g_\beta}$ to be the image of $s_\alpha$ under the natural map $R_{f_\alpha} \to  R_{f_\alpha g_\beta}$. This works since if $[P] \in R_{f_\alpha g_\beta}$ then $f_\alpha g_\beta \not \in P$. Therefore $f_\alpha \not \in P$ hence $s_\alpha$ restricts to $s_P$ under $R_{f_\alpha} \to R_P$, and the diagram
\[
\begin{tikzcd}
R_{f_\alpha} \arrow[rr,bend right] \rar & R_{f_\alpha g_\beta} \rar & R_P
\end{tikzcd}
\]
commutes, thus $s_\alpha \to s_{\alpha \beta} \to s_P$ shows $s_{\alpha \beta} \to s_P$.

\end{opghand}
\begin{opghand}[3] First suppose all the stalks have no nilpotent elements. Let $U \subseteq X$ open, and suppose $f \in \Gamma(U, \catO_X)$ satisfies $f^n = 0$. We wish to show $f = 0$ to conclude $\Gamma(U, \catO_X)$ has no nilpotent elements. For any $x \in U$, note that $[(U, f)]^n \in \catO_{X,x}$ is zero as well. Since the stalks $\catO_{X, x}$ contains no nilpotent elements, it follows that $[(U, f)] = [(U, 0)]$, hence there is some $W_x \subseteq U$ open with $x \in W_x$ and $f|_{W_x} = 0$. Since the $W_x$ will cover $U$, by the uniqueness axiom of a sheaf it follows that $f = 0$.

Now suppose $\Gamma(U, \catO_X)$ contains no nilpotent elements for all $U \subseteq X$. Let $[(U, f)] \in \catO_{X, x}$ for some $x \in X$ such that $[(U, f)]^n = 0$. Then $[(U, f^n)] = 0$, hence there is $x \in W \subseteq U$ open such that $f^n |_W = 0$. Thus $f|_W = 0$ as $\Gamma(W, \catO_X)$ contains no nilpotents by assumption. We conclude $[(U, f)] = [(W, 0)] = 0 \in \catO_{X, x}$ as required.

We now show that if some ring $R$ has no nilpotents, neither do any of the stalks in the affine scheme $(\Spec(R), \catO_{\Spec(R)})$. Thus let $[P] \in \Spec(R)$ and $[(r / f^k, X_f)] \in \catO_{\Spec(R), [P]}$ (note that any element of the stalk can be written in this way as we can always restrict an open $U$ to some base open $X_f$ containing $[P]$). If $[(r / f^k, X_f)]^n = 0$ for some $n$, then (after restricting to a base open again) there must exist $g \in R \setminus P$ such that $r^n / f^{nk} = 0$ in $R_g$. Hence $g^m r^n = 0$ in $R$ for some $m$ large enough. Therefore $(gr)^{nm} = 0$ in $R$, and since $R$ contains no nilpotents we must have $gr = 0$. Thus $r / f^k = 0$ in $R_g$, hence $[(r / f^k, X_f)] = [(r / f^k, X_g)] = 0 \in \catO_{\Spec(R), [P]}$. 

Finally suppose $X$ has an affine cover $\bigcup U_i = X$ such that $\Gamma(U_i, \catO_X)$ has no nilpotents, we need to show $X$ is reduced. It suffices to show none of the stalks of $X$ have any nilpotents by the above. Thus let $x \in X$ and suppose $[(U, f)^n] = 0$ in $\catO_{X, x}$. Let $i$ such that $x \in U_i$, $V = U \cap U_i$, then clearly $[(V, f)^n] = 0$ in $\catO_{X, x}$ as well. Since $V \subseteq U_i$, after passing through an isomorphism $(U_i, \catO_X|_{U_i}) \to (\Spec(R_i), \catO_{\Spec(R_i)})$ we see that $[(V, f)]$ must be zero in $\catO_{X|_{U_i},x}$ as none of the stalks in the scheme $(\Spec(R_i), \catO_{\Spec(R_i)})$ can have nilpotent elements. Hence there is $x \in W \subseteq V$ with $f|_W = 0$, and  $[(V, f)] = 0$ in $\catO_{X, x}$ as well. This completes the proof.
\end{opghand}
\begin{opghand}[4] We recall from the first homework sheet of the course Commutative Algebra I that all maximal ideals of the ring $\ZZ[X]$ are of the form $(p, f)$ where $p$ is a prime number and $f$ is a polynomial irreducible mod $p$. 

First consider the intersection $\overline{\{(p)\}} \cap \overline{\{(q)\}}$ for $p \neq q$ primes. By 1.(i) this is equal to the set $V((p)) \cap V((q)) = V((p) + (q)) = V((1)) = \emptyset$ as $p$, $q$ are coprime. 

Next consider the intersection $\overline{\{(f)\}} \cap \overline{\{(g)\}}$ for $f, g$ primitive and irreducible polynomials. Since $f$ and $g$ are primitive, they are neither contained in any prime ideal of the form $(p)$. We also can't have $(f, g) \subseteq (h)$ for some primitive irreducible polynomial $h$, since then $h \mid f$ and $h \mid g$. Since $f, g$ are primitive and irreducible we have $(f) = (h) = (g)$, but we assumed $(f) \neq (g)$.  Finally a maximal ideal $(p, h)$ contains $(f, g)$ if and only if $\bar{h} \mid \bar{f}, \bar{g} \pmod{p}$. We conclude that
\[
\overline{\{(f)\}} \cap \overline{\{(g)\}} = \{(p, h) \mid p \text{ prime}, h \text{ irreducible mod $p$}, \bar{h} \mid \bar{f}, \bar{g} \pmod{p}\}.
\]

Finally consider the intersection  $\overline{\{(f)\}} \cap \overline{\{(p)\}} = V((p, f))$ for $f$ some primitive, irreducible polynomial and $p$ prime. Clearly this can contain nothing of the form $(q)$ or $(g)$ since $p \not \in (g)$ for any primitive polynomial $g$, and $f \not \in (q)$ for any prime $q$ since $f$ is primitive. Remains to consider the maximal ideals $(q, f)$ where $q$ is a prime and $g$ is irreducible mod $q$. Since $g$ is irreducible, $\deg(g) \geq 1$. Hence $p \in (q, f)$ if and only if $p = q$. Thus we only need to consider maximal ideals of the form $(p, g)$. Clearly $f \in (p, g)$ if and only if $g \mid f \pmod{p}$. Hence the ideal $(p, g)$ contains $(p, f)$ if and only if $g$ is an irreducible factor of $f$ modulo $p$. We conclude
\[
V((p, f)) = \{ (p, g) \mid \bar{g} \text{ is an irreducible factor of $\bar{f}$ modulo $p$}\},
\]
this completes the last case.
\end{opghand}
\newpage
\emph{The exercise from Commutative algebra, in case this was part of exercise 4}
\begin{center}
\includegraphics[scale=0.8]{comalg.png}
\end{center}
\begin{opghand}[3]\
\begin{enumerate}[a)]
\item The definition clearly does not depend on $N$, since $\cont(Nf) = N\cont(f)$ for $f \in \ZZ[X]$ and $N \in \NN$ (one can thus take $N$ to be minimal). Now note that $\cont(f)$ is the unique positive number such that $1 / \cont(f) \cdot f$ is a primitive polynomial in $\ZZ[X]$ (even if $f \in \QQ[X]$). Thus if $f, g \in \ZZ[X]$ then $f / \cont(f), g / \cont(f)$ are primitive. By Exercise 2(iv) from chapter 1, one may deduce that $fg / \cont(f)(g)$ is primitive. From the uniqueness of $\cont$ it follows that $\cont(fg) = \cont(f)\cont(g)$. 

\item Suppose there exists an ideal $\frak{m} \subseteq \ZZ[X]$ such that $\varphi^{-1}(\frak{m}) = 0$. Consider the $\QQ[X]$-ideal $\frak{n}$ generated by $\frak{m}$. Since $\QQ[X]$ is a principal ideal domain (it is an Euclidean ring with Euclidean function $f \mapsto \deg(f)$), there exists some $f \in \QQ[X]$ such that $\QQ[X]f = \frak{n}$. Let $g = f / \cont(f)$, then clearly $\QQ[X]g = (f)$ in $\QQ[X]$, and $g \in \ZZ[X]$. Note that $\ZZ[X]g$ is a $\ZZ[X]$-ideal containing $\frak{m}$. To see this, note that if $h \in \frak{m}$, then $h = qg$ for some $q \in \QQ[X]$, since $\frak{m} \subseteq (g)$. But then $\cont(h) = \cont(q)\cont(g)$. Since $\cont(g) = 1$ and $\cont(h)$ is an integer (as $h \in \frak{m} \subseteq \ZZ[X]$), $\cont(q)$ is an integer and $q \in \ZZ[X]$. Thus $h \in \ZZ[X]g$. By maximality of $\frak{m}$, one must have either $\frak{m} = \ZZ[X]g$, or $\ZZ[X]g = \ZZ[X]$. 

If $\ZZ[X]g = \ZZ[X]$ then $g = \pm 1$, thus $f = \cont(f)\cdot g \in \QQ$. Therefore $\frak{n} = \QQ[X]$. But then there exist $q_1, \dots, q_n \in \QQ$ and $x_1, \dots, x_n \in \frak{m}$ with $\sum_{i = 1}^n q_ix_i = 1$. Multiplying by the denominators of the $q_i$, we find there exist $a_i \in \ZZ$ and $N \in \ZZ \setminus \{0\}$ such that $\sum_{i = 1}^n a_ix_i = N$. But then $N \in \varphi^{-1}(\frak{m})$, which contradicts $\varphi^{-1}(\frak{m}) = \{0\}$. 

Thus $\frak{m} = \ZZ[X]g$ is maximal. Then $\ZZ[X] / \ZZ[X]g$ is a field, thus $2$ is invertible (since $2 \not \in \frak{m}$ as $\varphi^{-1}(\frak{m}) = 0$) and there exist $a, b \in \ZZ[X]$ such that $2a = 1 + gb$. Dividing $a$ with remainder by $g$ (and changing $b$), we may assume that $\deg(a) < \deg(g)$. But then we must have $\deg(g) = 0$ since otherwise $\deg(2a) = \deg(a) < \deg (g) \leq \deg(1 + gb)$. Since $\deg(g) = 0$ we conclude that $g \in \ZZ$, and clearly $g \neq 0$ since $\{0\}$ is not a maximal ideal (strictly contained in $2\ZZ[X]$). Hence $\varphi^{-1}(\frak{m}) \neq \{0\}$ since it contains $g$. 

\item Since $\frak{m}$ is maximal, it is prime. Hence $\varphi^{-1}(\frak{m})$ is prime as well. Since $\varphi^{-1}(\frak{m}) \neq \{0\}$ it is of the form $(p)$ for some prime $p$. Hence $\frak{m}$ contains the prime number $p$. Consider the image of $\frak{m}$ in $\FF_p[X]$ under the projection $\pi: \ZZ[X] \to \FF_p[X]: \pi(f) = \overline{f \pmod{p}}$. Since $\FF_p$ is a field $\FF_p[X]$ is a principal ideal domain, hence the image of $\frak{m}$ is of the form $(w)$ for some $w \in \FF_p[X]$ irreducible (in particular $\deg(w) \geq 1$). Since $p \in \frak{m}$ and $w \in \frak{\pi(m)}$ we have $\tilde{w} + p\ZZ[X] \subseteq \frak{m}$ as well for any lift $\tilde{w} \in \ZZ[X]$ such that $\pi(\tilde{w}) = w$. Thus $\frak{m} \subseteq (p, \tilde{w})$. Since $\frak{m}$ is maximal and $(p, \tilde{w})$ is a proper $\ZZ[X]$-ideal (the quotient is isomorphic to $\FF_p[X] / (w)$ which is nonzero by choice of $w$), we conclude $\frak{m} = (p, \tilde w)$ which suffices (since $w$ is irreducible).
\end{enumerate}
\end{opghand}
\end{document}
