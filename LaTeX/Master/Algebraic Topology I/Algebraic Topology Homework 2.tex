\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}

% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Topology Homework 2}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1]
We will often write $H_n(A) := H_n(A, \CC)$ for a set $A$. We first show that $X$ is connected, it suffices to produce continuous maps $f_Y: [0, 1] \to X$ satisfying $f_Y(0) = N$ and $f_Y(1) = Y$ for all $Y \in X$. Let
\[
f_N(t) = N \  F_O(t) = \begin{cases}O \ t \leq \frac{1}{2} \\ N \ t > \frac{1}{2}\end{cases} F_W(t) = \begin{cases}W \ t \leq \frac{1}{2} \\ N \ t > \frac{1}{2}\end{cases} F_Z(t) = \begin{cases}Z \ t < \frac{1}{2} \\ O \ t = \frac{1}{2} \\ N \ t > \frac{1}{2}\end{cases}.
\]
One can easily verify all these maps are continuous by inputting all open sets that generate the topology on $X$ and checking that their inverse image is open under all the $F_Y$.

Set $X'= \{N, Z, O\}$ and $X'' = \{N, Z\}$. First note that $\{W\}$ is a closed set, since $\{N, Z, O\}$ is open. Thus $\overline{\{W\}} \subseteq \{N, Z, W\}^\circ$ (since $\{N, Z, W\}$ is an open set containing $\{W\}$), and by the excision property we have
\[
H_n(X, X') \cong H_n(X, \{N, Z, W\}) \cong H_n(X', X'').
\]
(the first equality follows since $W$ and $O$ are interchangeable in the definition of $X$). We now show $X'$ is contractible. To do this, consider the maps
\begin{align*}
f: X' \to \{N\}&:  N, Z, O \mapsto N, \\
g: \{N\} \to X'&:  N \mapsto N.
\end{align*}
Clearly $f \circ g = \id_{\{N\}}$, thus we only need to show $g \circ f$ is homotopic to $\id_{X'}$. Consider the homotopy
\[
H: [0, 1] \times X' \to X': H(t, N) = N, \quad H(t, O) = \begin{cases}O \ t \leq \frac{1}{2} \\ N \ t > \frac{1}{2}\end{cases} \quad H(t, Z) = \begin{cases}Z \ t < \frac{1}{2} \\ O \ t = \frac{1}{2} \\ N \ t > \frac{1}{2}\end{cases}.
\]
Now $H^{-1}(\{N\}) = [0, 1] \times \{N\} \cup (\frac{1}{2}, 1] \times X'$ is open in $X'$, and $H^{-1}(\{Z\}) = [0, \frac{1}{2}) \times \{Z\}$ which is also open in $X'$. From this it immediately follows that $H^{-1}(\{N, Z\})$ is open, and because $H^{-1}(\emptyset)$ and $H^{-1}(X')$ are trivially open we see that $H$ is continuous. Since $H(0, -) = \id_{X'}$ and $H(1, -) = g \circ f$, we conclude that $g \circ f$ is homotopic to $\id_{X'}$.

Thus $X'$ is homotopy equivalent to the topological space with one element. By Corollary 3.19, we see that $H_n(X', \CC) \cong H_n(\{\star\}, \CC)$. With a perfectly symmetrical argument in $O$ and $W$ we also obtain $H_n(\{N, Z, W\}, \CC) \cong H_n(\{\star\}, \CC)$. 

Next, note that the sets $\{N\}$ and $\{Z\}$ are clopen in $X''$, hence $X'' = \{N\} \sqcup \{Z\}$. By Lemma 2.1 this implies $H_n(X'', \CC) \cong H_n(\{\star\}) \oplus H_n(\{\star\})$. We thus have $H_0(X') = \CC, H_0(X'') = \CC \oplus \CC$ and $H_n(X') = H_n(X'') = 0$ for $n > 0$. 

Consider the long exact sequence
\begin{center}
\begin{tikzcd}[column sep = tiny]
\dots \to H_2(X'') \rar & H_2(X') \rar &  H_2(X', X'')\ar[out=0, in=180, looseness=2]{dll} \\
H_1(X'') \rar & H_1(X') \rar & H_1(X', X'') \ar[out=0, in=180, looseness=2]{dll}\\
H_0(X'') \rar & H_0(X') \rar & H_0(X', X'') \to 0.
\end{tikzcd} 
\end{center}
If we fill in what we know, we obtain the sequence 
\begin{center}
\begin{tikzcd}[column sep = tiny]
\dots \to0 \rar & 0 \rar &  H_2(X', X'')\ar[out=0, in=180, looseness=2]{dll} \\
0 \rar & 0 \rar & H_1(X', X'') \ar[out=0, in=180, looseness=2]{dll}\\
\CC \oplus \CC \rar & \CC \rar & H_0(X', X'') \to 0.
\end{tikzcd} 
\end{center}
Since the map $\CC \oplus \CC \to \CC$ is induced by the inclusion map $X'' \to X'$ it is given by $(a, b) \mapsto a + b$. In particular, it is surjective, so the map $\CC \to H_0(X', X'')$ is the $0$ map. By exactness we may conclude $H_0(X', X'') = 0$. Since the kernel of $\CC \oplus \CC \to \CC$ is isomorphic to $\CC$, we may also conclude that $H_1(X', X'') \cong \CC$. And obviously $H_n(X', X'') \cong 0$ for $n > 1$. 

Now consider the long exact sequence
\begin{center}
\begin{tikzcd}[column sep = tiny]
\dots \to H_2(X') \rar & H_2(X) \rar &  H_2(X, X')\ar[out=0, in=180, looseness=2]{dll} \\
H_1(X') \rar & H_1(X) \rar & H_1(X, X') \ar[out=0, in=180, looseness=2]{dll}\\
H_0(X') \rar & H_0(X) \rar & H_0(X, X') \to 0.
\end{tikzcd} 
\end{center}

Recall that $X'$ is contractible and that $H_i(X, X') \cong H_i(X', X'')$. Also remember that $X$ is connected, thus $H_0(X) = \CC$. Thus we obtain the long exact sequence 
\begin{center}
\begin{tikzcd}[column sep = tiny]
\dots \to 0 \rar & H_2(X) \rar &  0 \ar[out=0, in=180, looseness=2]{dll} \\
0 \rar & H_1(X) \rar & \CC \ar[out=0, in=180, looseness=2]{dll}\\
\CC \rar & \CC \rar & 0 .
\end{tikzcd} 
\end{center}

Clearly the bottom map $\CC$ is the identity as it is induced by the inclusion $X' \to X$. Hence the snake map must be the $0$ map for exactness in $H_0(X')$ to be true. We may thus conclude $H_1(X) = \CC$ and $H_2(X) = 0$. 

\end{opghand}
\begin{opghand}[2.4] \qquad
\begin{enumerate}[(i)]
\item Define $\partial_2$ by 
\begin{align*}
T_0 &\mapsto E_{23} - E_{13} + E_{12}, \qquad
T_1 \mapsto E_{23} - E_{03} + E_{02}, \\
T_2 &\mapsto E_{13} - E_{03} + E_{01}, \qquad 
T_3 \mapsto E_{12} - E_{02} + E_{01}.
\end{align*}
Similarly, define $\partial_1$ by 
\begin{align*}
E_{01} &\mapsto V_1 - V_0, \qquad E_{02} \mapsto V_2 - V_0, \qquad E_{03} \mapsto V_3 - V_0,  \\
E_{12} &\mapsto V_2 - V_1, \qquad E_{13} \mapsto V_3 - V_1, \qquad E_{23} \mapsto V_3 - V_2.
\end{align*}
Then we verify
\begin{align*}
\partial_1 \circ \partial_2 (T_0) &= (V_3 - V_2) - (V_3 - V_1) + (V_2 - V_1) = 0, \\ 
\partial_1 \circ \partial_2 (T_1) &= (V_3 - V_2) - (V_3 - V_0) + (V_2 - V_0) = 0, \\
\partial_1 \circ \partial_2 (T_2) &= (V_3 - V_1) - (V_3 - V_0) + (V_1 - V_0) = 0, \\
\partial_1 \circ \partial_2 (T_3) &= (V_2 - V_1) - (V_2 - V_0) + (V_1 - V_0) = 0.
\end{align*}
\item Note that for any $i < j$ we have $V_i - V_j \in \im(\partial_1)$. Hence $\QQ[\{V_0, \dots, V_3\}] / \im(\partial_1)$ is at most one-dimensional, since it is generated by only $V_0$. On the other hand, the map \linebreak $\phi: \QQ[\{V_0, \dots, V_3\}] \to \QQ$ induced by $V_i \mapsto 1$ is clearly nonzero, but $\phi \circ \partial_1 = 0$. Thus $\partial_1$ is not surjective, hence $\dim \im(\partial_1)  = 3$. By the dimension theorem it follows that $\dim \ker(\partial_1) = 3$ as well. 

Since $\im(\partial_2) \subseteq \ker(\partial_1)$ we must have $\dim \im(\partial_2) \leq 3$. But $\partial_2(T_0), \partial_2(T_1)$ and $\partial_2(T_2)$ are linearly independent: If
\[
a(E_{23} - E_{13} + E_{12}) + b(E_{23} - E_{03} + E_{02}) + c(E_{13} - E_{03} + E_{01}) = 0
\]
then by looking at the coefficients of $E_{12}$, $E_{02}$ and $E_{01}$ we can immediately conclude $a = b = c = 0$. Thus $\dim \im(\partial_2) = 3$ and $\dim \ker(\partial_2) = 1$ by the dimension theorem.

Now $H_2(C) = \ker(\partial_2) / \ker(0)$, hence $\dim H_2(C) = \dim \ker(\partial_2) - 0 = 1$. Furthermore $H_1(C) = \ker(\partial_1) / \im(\partial_2)$, hence $\dim H_1(C) = 3 - 3 = 0$. Finally $H_0(C) = \QQ[\{V_0, \dots, V_3\}] / \im(\delta_1)$, hence $\dim H_0(C) = 1$. 

\item We can compute it in two ways:
\begin{align*}
\sum_{n \geq 0} (-1)^n \dim_\QQ(C_n) &= 4 - 6 + 4 = 2, \\
\sum_{n \geq 0} (-1)^n \dim_\QQ(H_n(C)) &= 1 - 0 + 1 = 2.
\end{align*}
Note that both sums agree, as they should by Exercise 2.1.
\end{enumerate}
\end{opghand}
\end{document}
