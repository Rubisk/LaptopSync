\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}

% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Topology Homework 7}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[8.1] \
\begin{enumerate}[i)]
\item We first show that for any chain complex $X = \bigcup X_n$, all the sets $X_n$ are closed. It suffices to show $X_n \cap X_m$ is closed in $X_m$ for all $m, n$. If $n \geq m$ then $X_n = X_m$ is closed in $X_m$. Consider the diagram
\[
\begin{tikzcd}
X_n \ar{dr}{i} & & \ar{dl}{f} J_{n + 1} \times D^{n + 1} \\
&  X_n \cup_{J_{n + 1} \times \partial D^{n + 1}} J_{n + 1} \times D^{n + 1}
\end{tikzcd}
\]
where $i$, $f$ are the natural inclusions. To show $X_n$ is closed in $X_{n + 1}$, by the quotient topology on $X_{n + 1}$ it suffices to show $i^{-1}(i(X_n))$ and $f^{-1}(i(X_n))$ are closed. But clearly the set $i^{-1}(i(X_n)) = X_n$ is closed in $X_n$, and $f^{-1}(i(X_n)) = J_{n + 1} \times \partial D^{n + 1} $ is closed in $J_{n + 1} \times D^{n + 1}$. Thus $X_n$ is closed in $X_{n + 1}$. Since closed in closed is closed, we conclude by induction that $X_n$ is closed in $X_m$ for all $m > n$, thus $X_n$ is closed in $X$.

Now fix $n$ and take a fixed open $n$-cell $\{j\} \times (D^n)^\circ \subseteq J_n \times D^n$.  Let $\iota: J_n \times D^n \to X$ be the inclusion, and denote $Y = \iota\(\{j\} \times (D^n)^\circ\)$ for the inclusion of this $n$-cell, $Z = \overline{Y}$ for its closure. Almost by definition one has $\iota(Y) \subseteq X_n$. Since $X_n$ is closed in $X$ by the above, we also have $Z = \overline{Y} \subseteq X_n$. 

By Lemma 8.9, $X$ is Hausdorff, thus $X_n, Z, Y \subseteq X$ are Haussdorf. Remains to show $Z$ is semicompact. Consider the diagram
\[
\begin{tikzcd}
X_{n - 1} \ar{dr}{f} & & \ar{dl}{g} J_{n} \times D^{n} \\
&  X_{n - 1} \cup_{J_{n} \times \partial D^{n}} J_{n} \times D^{n}
\end{tikzcd}
\]
then $g^{-1}(Z)$ is closed since $Z$ is closed and $g$ continuous. Since $\{j\} \times (D^n)^\circ \subseteq g^{-1}(Z)$, we also have $\{j\} \times D^n = \overline{\{j\} \times (D^n)^\circ} \subseteq g^{-1}(Z)$. Hence $g(\{j\} \times D^n) \subseteq Z$, and since clearly $Z = \overline{g(\{j\} \times (D^n)^\circ)} \subseteq \overline{g(\{j\} \times (D^n))}$ we have $\overline{g(\{j\} \times (D^n))} = Z$. Since $g$ is continuous and $\{j\} \times D^n$ is semicompact, $g(\{j\} \times D^n)$ is also semicompact. Since $X$ is Haussdorf any semicompact set in $X$ is closed, hence $g(\{j\}) \times D^n)$ is closed and equal to $Z$, so $Z$ is semicompact.
\item If $U$ is closed then the implication clearly holds since the intersection of any two closed sets is closed again. Now assume $U$ intersected with the closure of every cell is closed in $X$. To show $U$ is closed in $X$, we show $U \cap X_n$ is closed in $X_n$ by induction on $n$. 

If $n = -1$ then $X_{-1} = A$, since $A \subseteq U$ we have $A \cap U = A$ is closed in $A$.

Now suppose $X_{n - 1} \cap U$ is closed, and consider again the diagram:
\[
\begin{tikzcd}
X_{n - 1} \ar{dr}{f} & & \ar{dl}{g} J_{n} \times D^{n} \\
&  X_{n - 1} \cup_{J_{n} \times \partial D^{n}} J_{n} \times D^{n}
\end{tikzcd}
\]
We need to show $f^{-1}(U)$ and $g^{-1}(U)$ are closed. Clearly $f^{-1}(U)$ is closed since we know that $f^{-1}(U) = U \cap X_{n - 1}$ is closed by our induction hypothesis. To show $g^{-1}(U)$ is closed, since $J_n$ has the discrete topology by definition of the product topology it suffices to show $g^{-1}(U) \cap (\{j\} \times D^n)$ is closed in the subspace $\{j\} \times D^n$
 for all $j \in J_n$, since $\{j\} \times D^n$ is closed in $J_n \times D^n$ hence then
\[
g^{-1}(U) = \bigcap_{j \in J_n} \((J \setminus \{j\}) \times D^n \cup \(g^{-1}(U) \cap (\{j\} \times D^n)\)\)
\]
is closed in $J_n \times D^n$. 

Note that $g(g^{-1}(U) \cap (\{j\} \times D^n)) = g(\{j\} \times D^n) \cap U$. By assumption $\overline{g(\{j\} \times (D^n)^\circ)} \cap U$ is closed, but remember from (i) that $\overline{g(\{j\} \times (D^n)^\circ)} =  g(\{j\} \times D^n)$. Hence $g(\{j\} \times D^n) \cap U$ is closed, and $g^{-1}(g(\{j\} \times D^n) \cap U)$ is closed in $J_n \times D^n$ since $g$ is continuous. Therefore $g^{-1}(g(\{j\} \times D^n) \cap U) \cap (\{j\} \times D^n) = g^{-1}(U) \cap (\{j\} \times D^n)$ is closed in $\{j\} \times D^n$ as required.
\end{enumerate}
\end{opghand}
\begin{opghand}[8.4] \
Consider the long exact sequence of the pair $(S^1, M_k)$:
\[
\begin{tikzcd}
& H_0(S^1; A) \ar{r}  & H_0(M_k; A) \ar{r} & H_0(S^1, M_k; A) \ar{r} & 0 \\
& H_1(S^1; A) \ar{r}  & H_1(M_k; A) \ar{r} &  H_1(S^1, M_k; A)  \ar[overlay, out=0, in=180, looseness=2]{llu}  \\
\dots \ar{r} & H_2(S^1; A) \ar{r}  & H_2(M_k; A) \ar{r} &  H_2(S^1, M_k; A)  \ar[overlay, out=0, in=180, looseness=2]{llu}
\end{tikzcd}
\] 
Since $S^1$ is a neighbourhood deformation retrtact of $M_k$ by Exercise 7.3, by Exercise 5.5 we have $H_n(S^1,  M_k; A) \cong \tilde{H_n}(M_k / S^1; A)$. Note that $M_k / S^1$ is simply $D^2 / \partial D^2$, which is homotopy equivalent to $S^2$. Recalling the (reduced) homology groups of the spheres we obtain the exact sequence
\[
\begin{tikzcd}
& A \ar{r}  & H_0(M_k; A) \ar{r} & 0 \ar{r} & 0 \\
& A \ar{r}  & H_1(M_k; A) \ar{r} & 0 \ar[overlay, out=0, in=180, looseness=2]{llu}  \\
\dots \ar{r} & 0  \ar{r}  & H_2(M_k; A) \ar{r} & A  \ar[overlay, out=0, in=180, looseness=2]{llu}
\end{tikzcd}
\] 
Since $H_n(M_k; A)$ is between two zeroes for $n > 2$, we conclude $H_n(M_k; A) = 0$ for $n > 2$, similarly $H_0(M_k ; A) = A$. To compute the remaining two homology groups, we compute the second snake map $A \to A$ which is induced by the the snake map $H_2(M_k; S^1; A) \to H_1(S^1; A)$. 

Consider a simplex $\sigma: \Delta^1 \to S^1$ that generates the homology group $H_1(\partial D^2; A)$. Then let $\iota: \partial D^2 \to D^2$ be the inclusion, then $\iota_*[\sigma] = 0$ since $H_1(D^2) = 0$. Thus there exists a simplex $\tau: \Delta^2 \to D^2$ with $\partial \tau = \iota_* \sigma$.  Since the map $\partial_*: H_2(D^2) \to H_1(\partial D^2)$ defines a homomorphism on homology that maps $\tau$ to a generator of $H_1(\partial D^2; A)$ (and both groups are isomorphic to $A$), $\tau$ must be a generator of $H_2(D^2; A)$. Consider the pushout diagram 
\[
\begin{tikzcd}
\partial D^2 \ar{d}{d_k} \ar{r}{\iota} &  D^2 \ar{d}{i} \\
S^1 \ar{r}{j} & M_k 
\end{tikzcd}
\]
The image under the isomorphism $H_2(M_k; S^1; A) \to H_1(S^1; A)$ of $[i_*(\tau)]$ is then given by $[j^{-1}_*(\partial \tau)]$, since to follow the snake map one considers $\iota_*(\tau)$ as an element in $C_n(M_k; A)$, takes it boundary and realizes this is in $H_1(S^1; A)$. From the commutativity of the diagram, we conclude that this is equal to $[j^{-1}_*(\partial \tau)] = [d_k(\sigma)]$. Since $\tau$ gets mapped to a generator of $S^2$ under the homotopy from $D^2 / \partial D^2$ to $S^2$, we may finally conclude that the induced map $\tilde{H_2}(S^2) \to H_1(S^1)$ is given by $d_k \circ \partial_*$. 

Thus the snake map in the diagram is multiplication by $k$, and we have a short exact sequence
\[
\begin{tikzcd}
0 \ar{r} & H_2(M_k) \ar{r} &  A \ar{r}{k} & A \ar{r} & H_1(M_k) \ar{r} & 0.
\end{tikzcd}
\]
We conclude $H_2(M_k) \cong \prescript{}{k}{A}$ and $H_1(M_k) \cong A / kA$. Thus for the reduced homologies we have 
\[
\tilde{H_n}(M_k) \cong \begin{cases}
A / kA & n = 1 \\
\prescript{}{k}{A} & n = 2\\
0 & \text{ otherwise}
\end{cases}
\]
\end{opghand}
\end{document}
