\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Topology Homework 1}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1]
For brevity, we will always denote an element of $\ZZ / 2\ZZ[S(X)_j]$ as a finite sum $\sum a_i \sigma_i$. By this we mean the function $f: S(X)_j \to \ZZ / 2\ZZ$ given by $f(\sigma_i) = a_i$ if $\sigma_i$ appears in the sum, and $0$ otherwise.

Clearly there are only two functions $\Delta^0 \to \{a, b\}$, since we can either send $\star \to a$ or $\star \to b$ (and both are continuous as $\emptyset, \{\star\}$ are both open in $\{\star\}$). We will denote these functions by $a$ and $b$. 

Any function $f: [0, 1] \to \{a, b\}$ is continuous as long as $f^{-1}(\{a\})$ is an open subset of $[0, 1]$. Now for any $f: [0, 1] \to \{a, b\}$, we have $\partial_1 f = \delta_0(f) - \delta_1(f) = f(1) - f(0)$, where by $f(i)$ we mean the function $\star \to f(i)$. By trying all four possibilities for $f(0)$ and $f(1) \in \{a, b\}$ we see that $\partial_1 f$ will always be either $0$ or $a + b$ (as $a - b = b - a = a + b$, since we work in $\ZZ / 2\ZZ[S(X)_0]$). The functions
\begin{align*}
f_0: \Delta^1 \to \{a, b\}: f(te_0 + (1 - t)e_1) &= \begin{cases} a & \text{ if } 0 \leq t < \frac{1}{2} \\ b &\text{ if }  \frac{1}{2} \leq t \leq 1\end{cases}, \\
f_1: \Delta^1 \to \{a, b\}: f(te_0 + (1 - t)e_1) &= a,
\end{align*}
are both continuous and satisfy $\partial_1 f_0 = 0, \partial_1 f_1 = a + b$. Hence $\im \partial_1 = \{0, a + b\}$. 

It is also clear that $C_0(X; \ZZ / 2\ZZ) = \ZZ / 2\ZZ[\{a, b\}] = \{0, a, b, a + b\}$. Thus we conclude that $H_0(X; \ZZ / 2\ZZ) = C_0 / \{0, a + b\} = \{\{0, a + b\}, \{a, b\}\} \cong \ZZ / 2\ZZ$. 
\end{opghand}
\begin{opghand}[2] \qquad
\begin{enumerate}[1.]
\item We first define a fifth simplex $e: \Delta^1 \to \RR^2$ by
\[
e(te_0 + (1 - t)e_1) = pe_0 + qe_1 + t(e_0 + e_1). 
\]
Now let $\alpha, \beta: \Delta^2 \to \RR^2$ be defined by
\begin{align*}
\alpha(t_0e_0 + t_1e_1 + t_2e_2) &= t_0(p, q) + t_1(p + 1, q) + t_2(p + 1, q + 1) \\
\alpha(t_0e_0 + t_1e_1 + t_2e_2) &= t_0(p, q) + t_1(p + 1, q + 1) + t_2(p, q + 1)
\end{align*}
for all $(t_0, t_1, t_2) \in \RR^3$ satisfying $t_0 + t_1 + t_2 = 1$ and $t_i \geq 0$. Let $s_{p, q}$ be given by $s_{p, q}(\alpha) = s_{p, q}(\beta) = 1$ and $s_{p, q}(\eta) = 0$ for any other simplex $\eta \neq \alpha, \beta$. We immediately see that \linebreak $\im(\alpha) \cup \im(\beta) = [p, p + 1] \times [q, q + 1]$. Now note that for any simplex $\eta \in \Delta^1$, by definition we have $\partial_2 s_{p, q}(\eta) = \partial_2 \alpha (\eta) + \partial_2 \beta(\eta)$. If we identify elements of $\ZZ[S(\RR^2)_n]$ with formal $\ZZ$-linear combinations of $n$-simplices again, we see that
\begin{align*}
\partial_2 \alpha  = \delta_0(\alpha) - \delta_1(\alpha) + \delta_2(\alpha) = b - e + a, \\
\partial_2 \beta  = \delta_0(\beta) - \delta_1(\beta) + \delta_2(\beta) = c - d + e.
\end{align*}
Thus $\partial_2 s_{p, q} = a + b + c - d$, as required.
\item Fix $\alpha = \alpha_{0, 0}$ and $\beta = \beta_{0, 0}, a = a_{0, 0}, \dots, d = d_{0,0}$ (the elements obtained from (a) by setting $p = q = 0$). Then clearly $s_{0, 0} = \alpha + \beta$ and $\partial_2 s_{0, 0} = a + b + c - d$. 
\item Note that since $\partial_2 \sum_{p, q = 0}^7  s_{p, q} \in \im(\partial_2)$, by definition the homology class of $\partial_2 s_{p, q}$ is $0$ for any $p, q$. Also, $s_{0, 0} \in \im(\partial_2)$ hence $s_{0, 0}$ also has homology class $0$. Therefore we have
\[
\left[ \partial_2 \sum_{p, q = 0}^7 s_{p, q} \right ] = [0] = [a_{0, 0} + b_{0, 0} + c_{0, 0} - d_{0, 0}]
\]
and the rightmost ``function'' identified with the element takes exactly four nonzero values.
\end{enumerate}
\end{opghand}
\end{document}
