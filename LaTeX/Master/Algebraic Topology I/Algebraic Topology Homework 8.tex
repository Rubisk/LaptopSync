\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}

% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Topology Homework 8}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[10.1]
Note that we have a series of homeomorphisms
\[
\varphi_n: \RR P^{n} \to S^n / \sim: (x_0 : \dots : x_n) \mapsto \left[\frac{1}{\sqrt{x_0^2 + \dots + x_n^2}}\(x_0, \dots, x_n\)\right]
\]
(here the square brackets denote the equivalence class in $S^n / \sim$, where $x \sim y \iff x = \pm y$). To see $\varphi_n$ is a homeomorphism, note that it is a continuous bijection, $S^n$ is compact (hence $S^n / \sim$), and $\RR P^n$ is easily shown to be Hausdorff. Furthermore, all of the diagrams
\[
\begin{tikzcd}
\RR P^{n} \dar{i_n} \rar{\varphi_n} & S^n / \sim \dar{j_n} \\
\RR P^{n + 1} \rar{\varphi_{n + 1}} & S^{n + 1} / \sim
\end{tikzcd}
\]
commute, where $i_n((x_0 : \dots : x_n)) = (x_0 : \dots x_n : 0)$ and $j_n$ is induced by the inclusion $S^n \to S^{n + 1}$. Since $\RR P^{n + 1}$ arises from $\RR P^n$ by attaching an $(n + 1)$-cell (as in the proof of Theorem 10.9), $S^{n + 1} / \sim$ arises from $S^n / \sim$ by attaching an $(n + 1)$-cell. This gives us a CW-structure on $\RR P^\infty = S^\infty / \sim \ =\bigcup_n S_n / \sim$. More explicitly, one can obtain $S^{n} / \sim $ from attaching an $n$-cell to $S^{n - 1} / \sim$ with attaching map $\partial D^n \to S^{n - 1} / \sim$ simply the identitification $\partial D^n \to S^{n - 1}$ composed with the quotient map $S^{n - 1} \to S^{n - 1} / \sim$.

To compute $\tilde H_n(\RR P^\infty, \ZZ)$ we first note that we have, for each $n$, exactly one $n$-cell in the CW-structure of $\RR P^\infty$ (note that $S^0 / \sim = \{\overline{1}\}$). Therefore the cellular chain complex $\tilde{C}(\RR P^\infty, \ZZ)$ is of the form
\[
\begin{tikzcd}
\dots \rar{\tilde \partial_{n + 1}} & \ZZ \rar{\tilde \partial_n} & \ZZ \rar{\tilde \partial_{n - 1}}  & \dots \ZZ \rar{\tilde \partial_1} & \ZZ \rar{\tilde \partial_0} & \ZZ
\end{tikzcd}
\]

Since the differentials $\tilde{\partial_n}$ only depend on the CW-structure of $S^{n + 1}$, and a homeomorphism induces an isomorphism on the level of homology, we conclude that the $\tilde{\partial_n}$ correspond (up to a sign) to the $\tilde{\partial_n}$ in the proof of Theorem 10.9. Therefore the chain complex $\tilde{C}(\RR P^\infty, \ZZ)$ is of the form
\[
\begin{tikzcd}
\dots \rar{0} & {2k} \atop {\ZZ} \rar{2} & {2k - 1} \atop {\ZZ} \rar{0}  & \dots  \rar{2} & {1} \atop {\ZZ} \rar{0} & {0} \atop {\ZZ}
\end{tikzcd}
\] 
Therefore
\[
H_n(\RR P^\infty, \ZZ) = \begin{cases}
\ZZ & \text{if }n = 0, \\
0 & \text{if }n > 0 \text{ and $n$ is odd,} \\
\ZZ / 2\ZZ & \text{if } n > 0 \text{ and $n$ is even.}
\end{cases}
\]
\end{opghand}
\begin{opghand}[ ] \
\begin{enumerate}[a.]
\item  Call the $0$-cell $p$, the $2$-cell $U$, and pick generators $e_x$ for the homology groups of any cell $x$. The cellular chain complex is of the form
\[
\begin{tikzcd}
\ZZ[e_U] \rar{\tilde \partial_1} & \ZZ[e_a] \oplus \ZZ[e_b] \oplus \ZZ[e_c] \oplus \ZZ[e_d]\rar{\tilde \partial_0} & \ZZ[e_p].
\end{tikzcd}
\]
Since there is only one $1$-cell, we have $\tilde \partial_0(e_a) = \dots = \tilde \partial_0(e_d) = e_p - e_p = 0$, and $\tilde \partial_0$ must be $0$. To compute $\tilde \partial_1$, we need to compute $\tilde \partial_1 e_U$. Note that it is given by running first through $d, c$, then through $d$ and $c$ in the opposite direction, then through $b$ and $a$, and then through $b$ and $a$ in the opposite direction. By the theorem we know $\tilde \partial_1 e_U$ is of the form $x_a \cdot e_a + x_b \cdot e_b + x_c \cdot e_c + x_d \cdot e_d$. If we collapse $a, b$ and $c$ to a point, then we are left with walking through $d$ and then through $d$ in the opposite direction. Thus $x_d = 0$, and by a similar argument $x_a = x_b = x_c = 0$. Thus $\tilde \partial_1 = 0$, and we conclude that
\[
H_n(X; \ZZ) = \begin{cases}
\ZZ & \text{if $n = 0$ or $n = 2$,} \\
\ZZ^4 & \text{if $n = 1$}, \\
0& \text{if $n > 2$.}
\end{cases}
\]
\item Note that $X$ is obtained from gluing an $8$-gon with scheme $dcd^{-1}c^{-1}bab^{-1}a^{-1}$. By Theorem 2 of Jorge's notes, this is a connected sum of $2$ tori (i.e. 2 hollow donuts glued together into one delicious megadonut). 
\end{enumerate}
\end{opghand}
\end{document}
