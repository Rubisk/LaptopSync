\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}

% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Topology Homework 9}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1]
\begin{enumerate}[(a)] \
\item  Denote $X = S^2 / \sim$ for the space we need to consider. We can construct this as a CW-complex with $C_0 = J_0 = \{p\}$, $C_1 = C_0 \cup_{J_1 \times \partial D^1} J_1 \times D^1$ where $J_1 = \{e\}$ and the gluing map $f^1: \{u\} \times \partial D^1 \to \{p\}$ is given by $f^1(-1) = f^1(1) = p$. Then let $C_2 = C_1 \cup_{J_2 \times \partial D^2} J_2 \times D^2$, where $J_2 = \{N, S\}$. To give the second gluing map we identify $C_1 \cong S^1$ and $\partial D^2 \cong S^1$, and define $g: \partial D^2 \to C_1: z \mapsto z^2$. We then let $f^2: \{N, Z\} \times \partial D^1$ be defined by $f^2(N, z) = f^2(S, z) = z^2$. 

The resulting CW-complex is clearly homemorphic to the quotient space of $S^2$. If we also denote $N, S$ and $u$ for the generators of the homology groups in the cells, then one obviously has $\partial N = \partial S = 2 u$ (since the mapping degree of $S^1 \to S^1: z \mapsto z^2$ is 2), and $\partial u = 0$. Thus the cellular chain complex takes the following form:
\[
\begin{tikzcd}
\dots \rar & 0 \rar{0} & \ZZ^2 \rar{[2, 2]}&  \ZZ \rar{0} & \ZZ
\end{tikzcd}
\] 
Therefore $H_0(X; \ZZ) = \ZZ, H_1(X; \ZZ) = \ZZ / 2\ZZ, H_2(X; \ZZ)  = 0$, and $\chi(X) = 1 - 1 + 2 = 2$. 
\\ \\ \\ \\ \\ \\
\item Denote $Y = S^1 \times (S^1 \vee S^1)$. We can construct $Y$ as a $CW$-complex with $C_0 = J_0 = \{p\}$,  $C_1 = C_0 \cup_{J_1 \times \partial D^1} J_1 \times D^1$ where $J_1 = \{a, b, c\}$ and the gluing map $f^1: \{a, b, c\} \times \partial D^1 \to \{p\}$ is given by $f^1(a, \pm 1) = f^1(b \pm 1) = f^1(c, \pm 1) = p$. Then $C_2 = C_1 \cup_{J_2 \times \partial D^2} J_2 \times D^2$, where $J_2 = \{N, S\}$ and the gluing map $f^2: J \times \partial D^2 \to C_1$ is given as follows (here we identify $\partial D^2 = S^1 = [0, 1] / \sim$, and $\chi_x: D^1 = [-1, 1] \to C_1$ denotes the characteristic map for $x \in J_1$):
\begin{align*}
f^2(N, t) = \begin{cases}
\chi_a(8t - 1) & t \in [0, 1 / 4] \\
\chi_b(8t - 3) & t \in [1/4, 2/4] \\ 
\chi_a(5 - 8t) & t \in [2/4, 3 / 4] \\
\chi_b(7 - 8t) & t \in [3/4, 1]
\end{cases} 
 \qquad 
f^2(S, t) = \begin{cases}
\chi_c(8t - 1) & t \in [0, 1 / 4] \\
\chi_b(8t - 3) & t \in [1/4, 2/4] \\ 
\chi_c(5 - 8t) & t \in [2/4, 3 / 4] \\
\chi_b(7 - 8t) & t \in [3/4, 1]
\end{cases}
\end{align*}
If we also denote $N, S, a, b, c$ for the generators of the homology groups in the cells, then we see that $\partial N = a + b - a - b = 0, \partial S = c + b - c - b = 0$, $\partial a = \partial b = \partial c = 0$. Therefore the cellular chain complex takes the following form:
\[
\begin{tikzcd}
\dots \rar & \rar{0} & \ZZ^2 \rar{0}&  \ZZ^3 \rar{0} & \ZZ
\end{tikzcd}
\] 
Thus $H_0(Y; \ZZ) = \ZZ, H_1(Y; \ZZ) = \ZZ^3$ and $H_2(Y; \ZZ) = \ZZ^2$. Finally $\chi(Y) = 1 - 3 + 2 = 0$. 

Unfortunately I have little faith in my 3D drawing skills for this one.
\end{enumerate}
\end{opghand}
\end{document}
