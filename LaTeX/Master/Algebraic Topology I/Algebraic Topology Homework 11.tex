\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}

% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Algebraic Topology Homework 11}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[12.1]
Denote $\iota_m: X_m \to X$ for the inclusion. We first show $\iota$ induces a map
\[
\iota_{m,*}: \pi_n(X_m, x_0) \to \pi_n(X, x_0): [f] \mapsto [\iota_m \circ f].
\]
To see that this is well defined we need to check that if $f, g: (S^n, s_0) \to (X_m, x_0)$ are such that $[f] = [g]$, then $[\iota_m \circ f] = [\iota_m \circ g]$. If $[f] = [g]$, then there is a homotopy $H: S^n \times [0, 1] \to X_m$ such that $H |_{X \times \{0\}} = f, H |_{X \times \{1\}} = g$ and $H(s_0, t) = x_0$ for all $t \in [0, 1]$. Therefore the map $H' := \iota_m \circ H: S^n \times [0, 1] \to X$ is a map such that $H' |_{S^n \times \{0\}} = \iota_m \circ f, H' |_{S^n \times \{1\}} = \iota_m \circ g$ and $H'(s_0, t) = \iota_m(x_0) = x_0$, so that we may conclude $[\iota_m \circ f] = [\iota_m \circ g]$. 

We now show that $\iota_{m, *}$ is surjective if $m \geq n$. Note that $S^n$ is a CW-complex with one $0$-cell $\{s_0\}$ and one $n$-cell (the attaching map is unique). Thus if $f: (S^n, s_0) \to (X, x_0)$, by Theorem 12.1 there is a cellular map $g: (S^n, s_0) \to (X, x_0)$ such that $g(s_0) = x_0$ and $g$ is homotopic to relative to $\{x_0\}$ to $f$. Since $g$ is cellular, $g(S^n) \subseteq X_n \subseteq X_m$ (as $m \geq n$) thus $g$ factors as $\iota_m \circ g'$ for $g': S^n \to X_m$. It follows that $[f] = [g] = \iota_{m, *}([g'])$ thus $\iota_{m, *}$ is surjective.

Finally we show that $\iota_{m, *}$ is injective if $m \geq n + 1$ (bijectivity follows by the above). Thus suppose $[\iota_m \circ f] = [\iota_m \circ g]$ for some $f, g: (S^n, s_0) \to (X_m, x_0)$. Then there is a homotopy $H: S^n \times [0, 1] \to X$ such that  $H|_{S^n \times \{0\}} = \iota_m \circ f, H |_{S^n \times \{1\}} = \iota_m \circ g$ and $H(s_0, t) =  x_0$. Let $T \subseteq S^n \times [0, 1]$ be the subspace given by
\[
T = S^n \times \{0\} \cup \{s_0\} \times [0, 1] \cup S^n \times \{1\}.
\]
We claim that $S^n \times [0, 1]$ is a chain complex relative to $T$ with a single $n + 1$-cell. To this end, identify $D^{n + 1}$ with $[0, 1]^{n + 1}$, $S^n$ with $[0, 1]^n / \partial [0, 1]^n$ and denote $\varphi: [0, 1]^n \to S^n$ for the quotient map. By the symmetry of $S^n$ we  may assume that $\varphi(\partial [0, 1]^n) = \{s_0\}$. Then identify $[0, 1]^{n + 1} = [0, 1]^n \times [0, 1]$, so that 
\[
\partial [0, 1]^{n + 1} = [0, 1]^n \times \{0\} \cup [0, 1]^n \times \{1\} \cup \partial [0, 1]^n \times [0, 1].
\]
Then define $\alpha: \partial [0, 1]^{n + 1} \to T$ as the map obtained from pasting lemma from the maps
\begin{align*}
\alpha_0:  [0, 1]^n \times \{0\} \to T: (x, 0) \mapsto (\varphi(x), 0), \\
\alpha_1:  [0, 1]^n \times \{1\} \to T: (x, 1) \mapsto (\varphi(x), 1), \\
\alpha_2:  \partial [0, 1]^n \times [0, 1] \to T: (x, t) \mapsto (s_0, t).
\end{align*}
The resulting space is clearly homeomorphic to $S^n \times [0, 1]$.

By Theorem 12.1, there is a map $G: \(S^n \times [0, 1]\) \times [0, 1]$ such that $G |_{S^n \times [0, 1] \times \{0\}} = H$, $G |_{S^n \times [0, 1] \times \{1\}}$ is cellular and $G((x, t), s) =  H(x, t)$ for all $(x, t) \in T$. If we set $H'(x, t) = G((x, t), 1)$ then by construction $H'(S^n \times [0, 1]) \subseteq X_{n + 1} \subseteq X_m$, $H'$ is a homotopy from $\iota_m \circ f$ to $\iota_m \circ g$ and $H'(s_0, t) = H(s_0, t) = x_0$ for all $t$ (since $(s_0, t) \in T$ for all $t$). It follows that $H'$ also gives a homotopy from $f$ to $g$ in $X_m$ (since its image is contained in $X_m$), therefore $[f] = [g]$ and $\iota_{m, *}$ is injective. 
\end{opghand}
\end{document}
