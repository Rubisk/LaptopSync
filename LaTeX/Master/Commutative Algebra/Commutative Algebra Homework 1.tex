\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\cont}{cont}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\catC}{\mathcal{C}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Commutative Algebra Homework 1}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1]
Consider the map $\varphi: A_f \to A[x] / (xf - 1): a / f^r \mapsto ax^r (a \in A, r \in \ZZ_{\geq 0})$. Then $\varphi$ is obviously an $A$-linear map. It is well-defined, since if $a / f^r = b / f^s$ for some $a, b \in A$ and $r, s \in \ZZ_{\geq 0}$, then there exists $n \in \ZZ_{\geq 0}$ such that $f^n(bf^r - af^s) = 0$. Now $xf \equiv 1$ hence 
\[
ax^r - br^s \equiv (xf)^{n + r + s}(ax^r - br^s) \equiv x^{n + r + s}f^n(af^s - bf^r) \equiv 0 \pmod{xf - 1}.
\]
Clearly it is surjective since $\sum a_i / f^i\mapsto \overline{\sum a_ix^i} \in A[x] / (xf - 1)$. 

To check that it is injective, suppose $\varphi(a / f^r) = 0$ for some $a \in A, r \in \ZZ_{\geq 0}$. We wish to show that there exists $n \in \ZZ_{\geq 0}$ such that $af^n = 0$, since then $a / f^r \sim 0  /1$. By assumption $ax^r \in (xf - 1)$, hence there is $g(x) \in A[x]$ such that $ax^r = (xf - 1)g(x)$. If one writes $g(x) = \sum_{i = 0}^k g_ix^i$, one thus has
\[
ax^r = (xf - 1)\(\sum_{i = 0}^k g_i x^i\) = fg_k x^{k + 1} + \sum_{i = 1}^k (fg_{i - 1} - g_i) x^{i - 1} + g_0.
\]
Since $A$ is not necessarily a domain, we cannot instantly conclude that $k = r - 1$. However, one must have $r \leq k + 1$, and for all $i \neq r$ the coefficients on the right hand side must be $0$. We split three cases:
\begin{itemize}
\item If $r = 0$ then by induction one has $g_i = f^i g_0$, and also $fg_k = 0$. Thus $0 = f^{k + 1}g_0 = f^{k + 1}a$, and $a / f^r \sim 0 / 1$. 
\item If $k + 1 > r > 0$, in this case one has $g_0 = 0$. But then for all $i$ such that $i - 1 \neq r$ one also has $g_i = fg_{i - 1}$. Thus for all $i \leq r$ one has $g_i = 0$. One thus has $g_{r + 1} = -a$ and once again $g_i = -f^{i - r - 1}a$ for $i > r + 1$. Since once again $fg_k = 0$ (as $k + 1 > r$) one has $f^{k - r}a = 0$, hence $a / f^r \sim 0 / 1$ as required. 
\item If $k + 1 = r$ then one has $fg_k = a$, but also $g_0 = 0$ and $g_i = fg_{i - 1}$ for all $i \in \{1, \dots, k\}$. By induction all $g_i$ are $0$, hence $a = fg_k$ is also $0$, which suffices.
\end{itemize}
\end{opghand}
\begin{opghand}[2] \
\begin{enumerate}[a)]
\item Since the sequence $0 \to aA \to A \to A / aA \to 0$ (inclusion and projection) is exact and $A / aA$ is flat, tensoring by $(A / aA)$ obtains the exact sequence
\[
0 \to aA \otimes_A (A / aA) \xrightarrow{\iota \otimes \id} A \otimes_A (A / aA) \xrightarrow{\pi \otimes \id} aA \otimes_A (A / aA) \to 0.
\]
However, for a standard tensor $ax \otimes \overline{y} \in aA \otimes_A(A / aA)$ ($x, y \in A$) one has 
\[
\iota \otimes \id(ax \otimes \overline{by}) = ax \otimes \overline{y} = x \otimes \overline{ay} = x \otimes 0.
\]
Thus $\iota \otimes \id$ is the zero map, and for exactness in $aA \otimes_A (A / aA)$ to hold $\iota \otimes \id$ must be injective. Therefore we must have that $aA \otimes_A(A / aA) = 0$. 
\item Since the sequence $0 \to aA \to A \to A / aA \to 0$ is exact and tensor is a right exact functor, tensoring by $aA$ one obtains the exact sequence
\[
aA \otimes_A aA \xrightarrow{\id \otimes \iota} aA \otimes_A A \xrightarrow{\id \otimes \pi} aA \otimes_A (A / aA) \to 0.
\]
By Proposition 2.14 (iv), the map $\phi: aA \otimes_A A \to aA: ax \otimes b \mapsto axb$ is an isomorphism. Since the map $f: a^2A \to aA \otimes aA: a^2x \mapsto (ax) \otimes a$ is surjective ($a^2xy \mapsto ax \otimes ay$), the sequence
\[
a^2A \xrightarrow{\phi \circ (\id \otimes \iota) \circ f} aA \xrightarrow{(\id \otimes \pi) \circ \phi^{-1}} aA \otimes_A (A / aA) \to 0
\]
is also exact.
\item Noting that $aA \otimes_A(A / aA) = 0$ we obtain an exact sequence $a^2A \to aA \to 0 \to 0$. We compute 
\[
\phi \circ (\id \otimes \iota) \circ f(a^2x) = \phi \circ (\id \otimes \iota)(ax \otimes a) = \phi(ax \otimes a) = a^2x
\]
Since the sequence is exact, the map $\phi \circ (\id \otimes \iota) \circ f: a^2A \to aA$ surjective. Hence there is some $a^2b \in a^2A$ that gets mapped to $a \cdot 1$, thus there is $b \in A$ such that $a^2b = a$. 
\end{enumerate}
\end{opghand}
\begin{opghand}[3]\
\begin{enumerate}[a)]
\item The definition clearly does not depend on $N$, since $\cont(Nf) = N\cont(f)$ for $f \in \ZZ[X]$ and $N \in \NN$ (one can thus take $N$ to be minimal). Now note that $\cont(f)$ is the unique positive number such that $1 / \cont(f) \cdot f$ is a primitive polynomial in $\ZZ[X]$ (even if $f \in \QQ[X]$). Thus if $f, g \in \ZZ[X]$ then $f / \cont(f), g / \cont(f)$ are primitive. By Exercise 2(iv) from chapter 1, one may deduce that $fg / \cont(f)(g)$ is primitive. From the uniqueness of $\cont$ it follows that $\cont(fg) = \cont(f)\cont(g)$. 

\item Suppose there exists an ideal $\frak{m} \subseteq \ZZ[X]$ such that $\varphi^{-1}(\frak{m}) = 0$. Consider the $\QQ[X]$-ideal $\frak{n}$ generated by $\frak{m}$. Since $\QQ[X]$ is a principal ideal domain (it is an Euclidean ring with Euclidean function $f \mapsto \deg(f)$), there exists some $f \in \QQ[X]$ such that $\QQ[X]f = \frak{n}$. Let $g = f / \cont(f)$, then clearly $\QQ[X]g = (f)$ in $\QQ[X]$, and $g \in \ZZ[X]$. Note that $\ZZ[X]g$ is a $\ZZ[X]$-ideal containing $\frak{m}$. To see this, note that if $h \in \frak{m}$, then $h = qg$ for some $q \in \QQ[X]$, since $\frak{m} \subseteq (g)$. But then $\cont(h) = \cont(q)\cont(g)$. Since $\cont(g) = 1$ and $\cont(h)$ is an integer (as $h \in \frak{m} \subseteq \ZZ[X]$), $\cont(q)$ is an integer and $q \in \ZZ[X]$. Thus $h \in \ZZ[X]g$. By maximality of $\frak{m}$, one must have either $\frak{m} = \ZZ[X]g$, or $\ZZ[X]g = \ZZ[X]$. 

If $\ZZ[X]g = \ZZ[X]$ then $g = \pm 1$, thus $f = \cont(f)\cdot g \in \QQ$. Therefore $\frak{n} = \QQ[X]$. But then there exist $q_1, \dots, q_n \in \QQ$ and $x_1, \dots, x_n \in \frak{m}$ with $\sum_{i = 1}^n q_ix_i = 1$. Multiplying by the denominators of the $q_i$, we find there exist $a_i \in \ZZ$ and $N \in \ZZ \setminus \{0\}$ such that $\sum_{i = 1}^n a_ix_i = N$. But then $N \in \varphi^{-1}(\frak{m})$, which contradicts $\varphi^{-1}(\frak{m}) = \{0\}$. 

Thus $\frak{m} = \ZZ[X]g$ is maximal. Then $\ZZ[X] / \ZZ[X]g$ is a field, thus $2$ is invertible (since $2 \not \in \frak{m}$ as $\varphi^{-1}(\frak{m}) = 0$) and there exist $a, b \in \ZZ[X]$ such that $2a = 1 + gb$. Dividing $a$ with remainder by $g$ (and changing $b$), we may assume that $\deg(a) < \deg(g)$. But then we must have $\deg(g) = 0$ since otherwise $\deg(2a) = \deg(a) < \deg (g) \leq \deg(1 + gb)$. Since $\deg(g) = 0$ we conclude that $g \in \ZZ$, and clearly $g \neq 0$ since $\{0\}$ is not a maximal ideal (strictly contained in $2\ZZ[X]$). Hence $\varphi^{-1}(\frak{m}) \neq \{0\}$ since it contains $g$. 

\item Since $\frak{m}$ is maximal, it is prime. Hence $\varphi^{-1}(\frak{m})$ is prime as well. Since $\varphi^{-1}(\frak{m}) \neq \{0\}$ it is of the form $(p)$ for some prime $p$. Hence $\frak{m}$ contains the prime number $p$. Consider the image of $\frak{m}$ in $\FF_p[X]$ under the projection $\pi: \ZZ[X] \to \FF_p[X]: \pi(f) = \overline{f \pmod{p}}$. Since $\FF_p$ is a field $\FF_p[X]$ is a principal ideal domain, hence the image of $\frak{m}$ is of the form $(w)$ for some $w \in \FF_p[X]$ irreducible (in particular $\deg(w) \geq 1$). Since $p \in \frak{m}$ and $w \in \frak{\pi(m)}$ we have $\tilde{w} + p\ZZ[X] \subseteq \frak{m}$ as well for any lift $\tilde{w} \in \ZZ[X]$ such that $\pi(\tilde{w}) = w$. Thus $\frak{m} \subseteq (p, \tilde{w})$. Since $\frak{m}$ is maximal and $(p, \tilde{w})$ is a proper $\ZZ[X]$-ideal (the quotient is isomorphic to $\FF_p[X] / (w)$ which is nonzero by choice of $w$), we conclude $\frak{m} = (p, \tilde w)$ which suffices (since $w$ is irreducible).
\end{enumerate}
\end{opghand}
\end{document}
