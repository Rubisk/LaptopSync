\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}

% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Opgave]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Oplossing]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\ob}{ob}
\DeclareMathOperator{\ggd}{ggd}
\DeclareMathOperator{\Fix}{Fix}
\DeclareMathOperator{\Orb}{Orb}


% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Modulen en Categorieën Huiswerk 9}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[8.6]
Zij $\mathcal{C}$ een categorie. Beschouw de diagonaalfunctor
\[
\Delta: \catC \to \catC \times \catC
\]
gedefiniëerd door $X \mapsto (X, X)$ en $f \mapsto (f, f)$. Wanneer heeft $\Delta$ een linksadjunct? En een rechtsadjunct?
\end{opghand}
\begin{oplossing}
We bewijzen dat $\Delta$ een linksadjunct heeft dan en slechts dan als voor elke $X, Y \in \catC$ het coproduct $X \amalg Y \in \catC$ bestaat.

Stel eerst dat $\Delta$ een linksadjunct $F$ heeft, we beweren dat $F(X, Y)$ dan het coproduct van $X$ en $Y$ is. Aangezien $F$ een linksadjunct is, bestaat er een isomorfisme van functoren
\[
\alpha_{(X, Y), Z}: \Hom(F(X, Y), Z) \to \Hom((X, Y), (Z, Z)).
\]
Merk op dat $\alpha_{(X, Y), F(X, Y)}(\id)$ een paar afbeeldingen $(X, Y) \to (F(X, Y), F(X, Y))$ is, kies deze als $\iota_X$ en $\iota_Y$. We beweren dat $(F(X, Y), \iota_X, \iota_Y)$ voldoet aan de eisen van het coproduct. Gegeven een $T \in \catC$ en afbeeldingen $t_X: X \to T, t_Y: Y \to T$ vinden we een nieuwe afbeelding in $\catC \times \catC$ door $t = (t_X, t_Y): (X, Y) \to (T, T)$. Dan is $h := \alpha^{-1}_{(X, Y), T}(t)$ een afbeelding $F(X, Y) \to T$. Aangezien $\alpha$ een morfisme van functoren is commuteert het diagram (hier $FXY = F(X, Y)$)
\[
\begin{tikzcd}[column sep=huge]
\Hom(FXY, FXY) \arrow{r}{\alpha_{(X, Y), FXY}} \arrow{d}{h \circ -} &  \Hom((X, Y), (FXY, FXY)) \arrow{d}{(h, h) \circ -}\\
\Hom(FXY, T) \arrow{r}{\alpha_{(X, Y), T}} &  \Hom((X, Y), (T, T))
\end{tikzcd}
\]
Als we $\id$ volgen door het diagram, dan zien we als we naar rechts en naar beneden gaan dat $\id \mapsto (\iota_X, \iota_Y) \mapsto (h \circ \iota_X, h \circ \iota_Y)$. Als we naar beneden en dan naar rechts gaan zien we $\id \mapsto h \mapsto (t_X, t_Y)$. Er volgt dat $t_X = h \circ \iota_X, t_Y = h \circ \iota_Y$ dus $h$ voldoet aan de eisen van het coproduct. En $h$ is de enige die voldoet, als er immers nog een $h'$ is zodat $h' \circ \iota_X = t_X, h'\circ \iota_Y = t_Y$, dan zien we door het bijbehorende diagram met $h'$ op te schrijven en nogmaals de identiteit te tracen dat $\alpha_{(X, Y), T}(h') = (t_X, t_Y)$, maar $\alpha_{(X, Y), T}$ is injectief. Dus er is een unieke $h: F(X, Y) \to T$ zodat $h \circ \iota_X = t_X, h \circ \iota_Y = t_Y$ en $(F(X, Y), \iota_X, \iota_Y)$ is dus het coproduct van $X$ en $Y$ in $\catC$. 

Andersom, als het coproduct van $X$ en $Y$ bestaat, beschouw dan $F: \catC \to \catC: (X, Y) \mapsto X \amalg Y$. Om van $F$ een functor te maken moeten we, gegeven een afbeelding $(f, g): (X_1, Y_1) \to (X_2, Y_2)$ een afbeelding $F(f, g): X_1 \amalg Y_1 \to X_2 \amalg Y_2$ definiëren. Merk op dat $\iota_{X_2} \circ f, \iota_{Y_2} \circ g$ afbeeldingen $X_1 \to X_2 \amalg Y_2, Y_1 \to X_2 \amalg Y_2$ zijn. Per definitie van het coproduct is er een unieke $h: X_1 \amalg Y_1 \to X_2 \amalg Y_2$ zodat $h \circ \iota_{X_1} = \iota_{X_2} \circ f, h \circ \iota_{Y_1} = \iota_{Y_2} \circ g$, definiëer $F(f, g)$ als die unieke $h$. 

Om in te zien dat $F$ een functor is, zien we dat $\id_{X \amalg Y}$ voldoet aan $\id_{X \amalg Y} \circ \iota_X = \iota_X$, en $\id_{X \amalg Y} \circ \iota_Y = \iota_Y$. Dus $F(\id_X, \id_Y) = \id_{X \amalg Y}$ zoals benodigd. En gegeven $f_1: X_1 \to X_2, f_2: X_2 \to X_3$ en $g_1: Y_1 \to Y_2, g_2: Y_2 \to Y_3$, met $h_1 = F(f_1, g_1), h_2 = F(f_2, g_2)$ zien we dat
\begin{align*}
(h_2 \circ h_1) \circ \iota_{X_1}&= h_2 \circ \iota_{X_2} \circ f_1 = \iota_{X_3} \circ (f_2 \circ f_1) \\
(h_2 \circ h_1) \circ \iota_{Y_1}&= h_2 \circ \iota_{Y_2} \circ g_1 = \iota_{Y_3} \circ (g_2 \circ g_1)
\end{align*}
dus $F((f_2 \circ f_1, g_2 \circ g_1))$ is inderdaad $h_2 \circ h_1$. 


We gaan vervolgens na dat $F$ een linksadjunct van $\Delta$ is door te definiëren
\begin{align*}
\alpha_{(X, Y), Z}: \Hom(X \amalg Y, Z) &\to \Hom((X, Y), (Z, Z)) \\
\varphi &\mapsto (\varphi \circ \iota_X, \varphi \circ \iota_Y).
\end{align*}
Om te zien dat dit een morfisme van functoren is moeten we nagaan dat voor alle $(f, g): (X_1, Y_1) \to (X_2, Y_2)$ commuteert
\[
\begin{tikzcd}[column sep=6em]
\Hom(X_1 \amalg Y_1, Z) \arrow{r}{(- \circ \iota_{X_1}, - \circ \iota_{Y_1})} &  \Hom((X_1, Y_1), (Z, Z)) \\
\Hom(X_2 \amalg Y_2, Z) \arrow{r}{(- \circ \iota_{X_2}, - \circ \iota_{Y_2})}  \arrow{u}{- \circ F(f, g)}  &  \Hom((X_2, Y_2), (Z, Z))\arrow{u}{ - \circ(f, g)}
\end{tikzcd}
\]
wat volgt omdat per constructie $F(f, g) \circ \iota_{X_1} = \iota_{X_2} \circ f$ en $F(f, g) \circ \iota_{Y_1} = \iota_{Y_2} \circ g$. Verder moeten we nagaan dat voor alle $g: Z_1 \to Z_2$ 
\[
\begin{tikzcd}[column sep=6em]
\Hom(X \amalg Y, Z_1) \arrow{r}{(- \circ \iota_{X}, - \circ \iota_{Y})} \arrow{d}{g \circ -}   &  \Hom((X, Y), (Z_1, Z_1)) \arrow{d}{(g, g) \circ -}\\
\Hom(X \amalg Y, Z_2) \arrow{r}{(- \circ \iota_{X}, - \circ \iota_{Y})} &  \Hom((X, Y), (Z_2, Z_2))
\end{tikzcd}
\]
commuteert maar dat is triviaal aangezien we aan verschillende kanten samenstellen.

Resteert te laten zien dat $\alpha_{(X, Y), Z}$ een bijectie is voor alle $(X, Y) \in \catC \times \catC$ en $Z \in \catC$. Maar dat volgt omdat alle $(f, g): (X, Y) \to (Z, Z)$ corresponderen met afbeeldingen $f: X \to Z$ en $g: Y \to Z$, dus (per definitie!) van het coproduct bestaat er een unieke $h: X \amalg Y \to Z$ zodat $h \circ \iota_X = f$ en $h \circ \iota_Y = g$. We concluderen dat $F$ een linksadjunct van $\Delta$ is.

We bewijzen vervolgens dat $\Delta$ een rechtsadjunct heeft dan en slechts dan als alle $X, Y \in \catC$ het product $X \times Y$ bestaat. 

Stel eerst dat $\Delta$ een rechtsadjunct $F$ heeft, we gaan na dat $F(X, Y)$ een product van $X$ en $Y$ is. Aangezien $F$ een rechtsadjunct is, bestaat er een isomorfisme van functoren
\[
\alpha_{Z, (X, Y)}: \Hom(Z, F(X, Y)) \to \Hom((Z, Z), (X, Y)).
\]
Merk op dat $\alpha_{F(X, Y), (X, Y)}(\id)$ een paar afbeeldingen $(F(X, Y), F(X, Y)) \to (X, Y)$ is, kies deze als $\pi_X$ en $\pi_Y$. We beweren dat $(F(X, Y), \pi_X, \pi_Y)$ voldoet aan de eisen van het product. Gegeven een $T \in \catC$ en afbeeldingen $t_X: T \to X, t_Y: T \to Y$ vinden we een nieuwe afbeelding in $\catC \times \catC$ door $t = (t_X, t_Y): (T, T) \to (X, Y)$. Dan is $h := \alpha^{-1}_{T, (X, Y)}(t)$ een afbeelding $T \to F(X, Y)$. Aangezien $\alpha$ een morfisme van functoren is commuteert het diagram (schrijf weer $FXY = F(X, Y)$)
\[
\begin{tikzcd}[column sep=huge]
\Hom(FXY, FXY) \arrow{r}{\alpha_{FXY, (X, Y)}} \arrow{d}{- \circ h} &  \Hom((FXY, FXY), (X, Y)) \arrow{d}{- \circ (h, h)}\\
\Hom(T, FXY) \arrow{r}{\alpha_{T, (X, Y)}} &  \Hom((T, T), (X, Y))
\end{tikzcd}
\]
Als we $\id$ volgen door het diagram, dan zien we als we naar rechts en naar beneden gaan dat $\id \mapsto (\pi_X, \pi_Y) \mapsto (\pi_X \circ h, \pi_Y \circ h)$. Als we naar beneden en dan naar rechts gaan zien we $\id \mapsto h \mapsto (t_X, t_Y)$. Er volgt dat $t_X = \pi_X \circ h, t_Y = \pi_Y \circ h$ dus $h$ voldoet aan de eisen van het product. En $h$ is de enige die voldoet, als er immers nog een $h'$ is zodat $\pi_X \circ h' = t_X, \pi_Y \circ  h' = t_Y$, dan zien we door het bijbehorende diagram met $h'$ op te schrijven en nogmaals de identiteit te volgen dat $\alpha_{T, (X, Y)}(h') = (t_X, t_Y)$, maar $\alpha_{T, (X, Y)}$ is injectief. Dus er is een unieke $h: T \to F(X, Y)$ zodat $t_X = \pi_X \circ h, t_Y = \pi_Y \circ h$ en $(F(X, Y), \pi_X, \pi_Y)$ is dus het product van $X$ en $Y$ in $\catC$. 

Andersom, als het product van $X$ en $Y$ bestaat, beschouw dan $F: \catC \to \catC: (X, Y) \mapsto X \times Y$. Om van $F$ een functor te maken moeten we, gegeven een afbeelding $(f, g): (X_1, Y_1) \to (X_2, Y_2)$ een afbeelding $F(f, g): X_1 \times Y_1 \to X_2 \times Y_2$ definiëren. Merk op dat $f \circ \pi_{X_1}, g \circ \pi_{Y_1}$ afbeeldingen $X_1 \times Y_1 \to X_2, X_1 \times Y_1 \to Y_2$ zijn. Per definitie van het product is er een unieke $h: X_1 \times Y_1 \to X_2 \times Y_2$ zodat $\pi_{X_2} \circ h = f \circ \pi_{X_1}, \pi_{Y_2} \circ h= g \circ \pi_{Y_1}$, definiëer $F(f, g)$ als die unieke $h$. 

Om in te zien dat $F$ een functor is, zien we dat $\id_{X \times  Y}$ voldoet aan $\pi_X \circ \id_{X \times Y} = \pi_X$, en $\pi_Y \circ \id_{X \times Y}  = \pi_Y$. Dus $F(\id_X, \id_Y) = \id_{X \times Y}$ zoals benodigd. En gegeven $f_1: X_1 \to X_2, f_2: X_2 \to X_3$ en $g_1: Y_1 \to Y_2, g_2: Y_2 \to Y_3$, met $h_1 = F(f_1, g_1), h_2 = F(f_2, g_2)$ zien we dat
\begin{align*}
\pi_{X_3} \circ (h_2 \circ h_1)&= f_2 \circ \pi_{X_2} \circ h_1= (f_2 \circ f_1) \circ \pi_{X_1} \\
\pi_{Y_3} \circ (h_2 \circ h_1)&= g_2 \circ \pi_{Y_2} \circ h_1= (g_2 \circ g_1) \circ \pi_{Y_1}
\end{align*}
dus $F((f_2 \circ f_1, g_2 \circ g_1))$ is inderdaad $h_2 \circ h_1$. 

We gaan vervolgens na dat $F$ een rechtsadjunct van $\Delta$ is door te definiëren
\begin{align*}
\alpha_{Z, (X, Y)}: \Hom(Z, X \times Y) &\to \Hom((Z, Z), (X, Y)) \\
\varphi &\mapsto (\pi_X \circ \varphi, \pi_Y \circ \varphi).
\end{align*}
Merk op dat deze $\alpha$ de andere kant op gaat dan de $\alpha$ van de definitie, maar als we laten zien dat dit een isomorfisme van functoren is dan bestaat er ook wel een isomorfisme van functoren de andere kant op. 
Om te zien dat dit een morfisme van functoren is moeten we nagaan dat voor alle $(f, g): (X_1, Y_1) \to (X_2, Y_2)$ commuteert
\[
\begin{tikzcd}[column sep=6em]
\Hom(Z, X_1 \times Y_1) \arrow{r}{(\pi_{X_1} \circ -, \pi_{Y_1} \circ -)} \arrow{d}{F(f, g) \circ -} &  \Hom((Z, Z), (X_1, Y_1)) \arrow{d}{(f, g) \circ -}\\
\Hom(Z, X_2 \times Y_2) \arrow{r}{(\pi_{X_2} \circ -, \pi_{Y_2} \circ -)} &  \Hom((Z, Z), (X_2, Y_2))
\end{tikzcd}
\]
wat volgt omdat per constructie $\pi_{X_2} \circ F(f, g) = f \circ \pi_{X_1}$ en $\pi_{Y_2} \circ F(f, g) = g \circ \pi_{Y_1}$. Verder moeten we nagaan dat
\[
\begin{tikzcd}[column sep=6em]
\Hom(Z_1, X \times Y) \arrow{r}{(\pi_X \circ -, \pi_Y \circ -)} &  \Hom((X, Y), (Z_1, Z_1)) \\
\Hom(Z_2, X \times Y) \arrow{r}{(\pi_X \circ -, \pi_Y \circ -)}  \arrow{u}{- \circ g}  &  \Hom((X, Y), (Z_2, Z_2))\arrow{u}{- \circ (g, g)}
\end{tikzcd}
\]
commuteert maar dat is triviaal aangezien we aan verschillende kanten samenstellen. (Merk op dat onze diagrammen andersom staan dan in de syllabus omdat we eigenlijk een $\alpha^{-1}$ definiëren).

Resteert te laten zien dat $\alpha_{Z, (X, Y)}$ een bijectie is voor alle $(X, Y) \in \catC \times \catC$ en $Z \in \catC$. Maar dat volgt omdat alle $(f, g):(Z, Z) \to (X, Y)$ corresponderen met afbeeldingen $f: Z \to X$ en $g: Z \to Y$, dus (per definitie!) van het coproduct bestaat er een unieke $h: Z \to X \times Y$ zodat $\pi_X \circ h = f$ en $\pi_Y \circ h = g$. We concluderen dat $F$ een rechtsadjunct van $\Delta$ is.
\end{oplossing}
\begin{opghand}[8.16] Zij $G$ een groep en $BG$ de categorie van Voorbeeld 4.7. Zij $F: BG \to \mathbf{Set}$ een functor.
\begin{enumerate}[(1)]
\item Laat zien dat $F(\star)$ een verzameling $X$ is, uitgerust met een werking van $G$. 
\begin{oplossing}
Aangezien $F$ een functor is, is $F(\star)$ inderdaad een verzameling $X$. We definiëren een werking van $G$ op $X$ door $g\cdot x = (Fg)(x)$, dit werkt immers $F$ is een functor en $\Hom(\star, \star) = G$ dus $Fg \in \Hom(X, X)$. En
\begin{itemize}
\item $\forall x \in X$: $e = \id_{\star}$, dus $e \cdot x = (Fe)(x) = (\id_X)(x) = x$,
\item $\forall x \in X, \forall g, h \in G$: $F(gh) = Fg \circ Fh$, dus $(gh) \cdot x = (Fgh)(x) = (Fg \circ Fh)(x) =  g \cdot (h \cdot x)$,
\end{itemize}
dus $\cdot$ is een correcte werking.
\end{oplossing}
\newpage

\item Laat zien dat $\lim F$ de verzameling fixpunten van de werking is.
\begin{oplossing}
Laat $\pi: \Fix_{G}(X) \to X$ de inclusie (!). Aangezien $g \cdot x = x$ voor alle $x \in \Fix_G(X)$ zal $\pi (x) = Fg \circ \pi(x)$ voor alle $x \in \Fix_G(X)$, dus $\pi = Fg \circ \pi$ voor alle $g \in G$. 

En als $t: T \to X$ een afbeelding is die voldoet aan $t = Fg \circ t$ voor alle $g \in G$, dan moet wel $\im(t) \subseteq \Fix_G(X)$, immers als er een $s \in T$ is met $t(s) \not \in \Fix_G(X)$, dan is er een $g \in G$ met $g \cdot t(s) \neq t(s)$, wat betekent dat $Fg \circ t(s) \neq t(s)$, maar $Fg \circ t = t$. Er volgt dat $\tilde{t}: T \to \Fix_G(X): s \mapsto t(s)$ een welgedefiniëerde afbeelding is en duidelijk de unieke afbeelding zodat $t = \pi \circ \tilde{t}$ (uniciteit volgt omdat $\pi$ injectief is).

We concluderen dus dat $\lim F = (\Fix_G(X), \pi)$ per definitie.
\end{oplossing}

\item Laat zien dat $\text{colim}(F)$ de verzameling banen van de werking is.
\begin{oplossing}
Laat $\Orb_G(X)$ de verzameling banen van de werking, en schrijf $[x]$ voor de baan van $x \in X$. Laat $\iota: X \to \Orb_G(X): x \mapsto [x]$. Dan zal duidelijk voor alle $g \in G$ gelden $\iota \circ Fg = \iota$, immers voor alle $x \in X$ zal $[x] = [g \cdot x]$. 

En als $t: X \to T$ voldoet aan $t \circ Fg = t$, dan zal $t(g \cdot x) = t(x)$ voor alle $x \in X$. Dus $t(x) = t(y)$ als $[x] = [y]$, er bestaat immers een $g$ zodat $x = gy$. Dus de afbeelding $h: \Orb_G(X) \to T: [x] \mapsto t(x)$ is goed gedefiniëerd, en voldoet duidelijk aan $h \circ \iota = t$. 

We concluderen dus dat $\text{colim}(F) = (\Orb_G(X), \iota)$ per definitie van colim.
\end{oplossing}
\end{enumerate}
\end{opghand}
\end{document}
