\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}

% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Opgave]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Oplossing]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Modulen en Categorieën}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1.7]
Zij $R$ een ring, en laat $M$ en $N$ twee $R$-modulen. Laat zien dat de puntsgewijze optelling $\Hom_R(M, N)$ de structuur van een abelse groep geeft. Laat zien dat als $R$ commutatief is, dat $\Hom_R(M, N)$ een natuurlijke structuur van een $R$-moduul heeft.
\end{opghand}
\begin{oplossing}
We tonen eerst aan dat $(\Hom_R(M, N), +)$ een abelse groep is. 
\begin{enumerate}
\item Het is duidelijk dat voor $f, g, h \in \Hom_R(M, N)$ geldt $(f + g) + h = f + (g + h)$ want optelling is associatief in $N$. 
\item Het is duidelijk dat voor $f, g \in \Hom_R(M, N)$ geldt $f + g = g + f$ want optelling is commutatief in $N$. 
\item De afbeelding $e: M \to N: m \mapsto 0$ is een eenheidselement (hij zit trivialiter in $\Hom_R(M, N)$ want alles is $0$), want voor alle $f \in \Hom_R(M, N)$ en $m \in M$ geldt $(f + e)(m) = f(m) + e(m) = f(m) + 0 = f(m)$, en trivialiter in $\Hom_R(M, N)$ zit.
\item Voor alle $f \in \Hom_R(M, N)$ geldt dat $(-f): M \to N: m \mapsto -f(m)$ duidelijk voldoet aan $-f + f = e$. 
\end{enumerate}
Nu tonen we aan dat als $R$ commutatief is, dat $\Hom_R(M, N)$ een $R$-moduulstructuur heeft door $(r \cdot f)(m) :=  rf(m)$. Om in te zien dat $r \cdot f \in \Hom_R(M, N)$, merken we op dat voor alle $s \in R$ en $m \in M$ geldt
\[
(r \cdot f)(sm) = rf(sm) = r s f(m) = srf(m) = s \cdot (r\cdot f)(m),
\]
waar we eerst gebruiken dat $f \in \Hom_R(M, N)$ en dan dat $R$ commutatief is (dat $(r \cdot f)(m_1 + m_2) = (r\cdot f)(m_1) + (r\cdot f)(m_2)$ volgt gemakkelijk). Om te zien dat dit een moduulstructuur is, gaan we alle axioma's na. Voor willekeurige $f, g \in \Hom_R(M, N)$, $m \in M$ en $r, s \in R$.
\begin{enumerate}
\item $(r \cdot (f + g))(m) = rf(m) + rg(m) = (r \cdot f)(m) + (r \cdot g)(m)$, dus $r \cdot (f + g) = r \cdot f + r \cdot g$.
\item $((r  + s) \cdot f)(m) = (r + s)f(m) = (r \cdot f)(m) + (s \cdot f)(m)$, dus $(r + s)\cdot f = r \cdot f + s \cdot f$.
\item $(rs \cdot f)(m) = rsf(m) = r(s \cdot f)(m) = (r \cdot (s \cdot f))(m)$, dus $(rs)\cdot f = r \cdot (s \cdot f)$. 
\item $(1 \cdot f)(m) = f(m)$, dus $1 \cdot f = f$.
\end{enumerate}
\end{oplossing}
\newpage
\begin{opghand}[1.11]
Zij $R$ een ring, en laat $I \subset R$ een (tweezijdig) ideaal. Laat $M$ een $R$-moduul. Laat zien dat
\[
IM := \{\sum r_i x_i \mid r_i \in I, x_i \in M\}
\]
een sub-$R$-moduul is van $M$, en laat zien dat $M / IM$ een $R/I$-moduul is. Toon aan dat als $M$ vrij is met rang $n$, dat $M / IM$ vrij is met rang $n$ als $R/I$-moduul.
\end{opghand}
\begin{oplossing}
We tonen eerst aan dat $IM$ een sub-$R$-moduul is van $M$. Het is duidelijk dat $IM$ een ondergroep is van $M$, want de som van twee sommen is als één som te schrijven dus zit in $IM$, de lege som is $0$ en door minnetjes voor de $r_i$ te zetten vinden we een additieve inverse. En voor $r \in R$ zal $t_i = r \cdot r_i$ ook in $R$ zitten voor alle $r$, dus
\[
r\(\sum r_i x_i\) = \sum r r_i x_i = \sum t_i x_i
\]
zit ook in $IM$. Dus $IM$ is een sub-$R$-moduul.

Nu tonen we aan dat $M / IM$ een $R/I$-moduul is. Gegeven $m + IM \in M / IM$ en $r + I \in R/I$ definiëren we
\[
(r + I)(m + IM) = (rm + IM).
\]
Dit hangt niet af van de keuze voor $m$, want als $m_1 + IM = m_2 + IM$ dan $m_1 - m_2 \in IM$. Dus $r(m_1 - m_2) \in IM$, en $rm_1 + IM= rm_2 + IM$. Net zo hangt het niet af voor de keuze van $r$, want als $r_1 + I = r_2 + I$, dan zal $r_1 - r_2 \in I$ dus $(r_1 - r_2)m \in IM$, dus $r_1m + IM = r_2m + IM$. En deze werking van $R/I$ op $M / IM$ voldoet aan alle eisen voor een moduul want het is verkregen uit de werking van $R$ op $M$ en dat is een moduulwerking.

Tenslotte laten we zien dat als $M$ vrij is van rang $n$, dat $M / IM$ ook vrij is met rang $n$ als $R / I$ moduul. Aangezien $M$ vrij is van rang $n$ is er een $S \subset M$ met $|S| = n$ (schrijf $S = (s_1, \dots, s_n)$) en een isomorfisme $\varphi: R^{(S)} \to M: (r_1, \dots, r_n) \mapsto \sum r_is_i$. Laat $\tilde{S} \subset M / IM$ (als tupel, niet verzameling!) door $\tilde{s_i} = s_i + IM$. Laat 
\[
\tilde{\varphi} (R / I)^{(\tilde{S})} \to M / IM: (r_1 + I, \dots, r_n + I) \mapsto \sum r_i \tilde{s_i}.
\]
Dan volgt dat $\tilde{\varphi}$ welgedefiniëerd is, want als $r_i + I = t_i + I$ dan $r_i - t_i \in I$ dus $(r_i - t_i)\tilde{s_i} \in IM$. En $\tilde{\varphi}$ is een moduulhomomorfisme, want
\begin{align*}
\tilde{\varphi}((r_i + t_i)_{i = 1}^n) &= \sum (r_i + t_i)\tilde{s_i} = \sum r_i \tilde{s_i} + \sum t_i \tilde{s_i} = \tilde{\varphi}((r_i)_{i = 1}^n) + \tilde{\varphi}((t_i)_{i = 1}^n), \\
\tilde{\varphi}(t(r_i)_{i = 1}^n) &= \sum (tr_i)\tilde{s_i} = t\sum r_i \tilde{s_i} = t\tilde{\varphi}((r_i)_{i = 1}^n).
\end{align*}
En $\tilde{\varphi}$ is surjectief, want $\varphi$ is surjectief, dus gegeven $m \in M$ zijn er $r_i$ met $\varphi(r_1, \dots, r_n) = m$, dus $\tilde{\varphi}(r_1 + I, \dots, r_n + I) = m + IM$. 

Om te zien dat $\tilde{\varphi}$ injectief is, kiezen we $r_i \in R$ en tonen we aan dat $\tilde{\varphi}(r_1 + I, \dots, r_n + I) = 0 + IM$ impliceert dat $r_i \in I$ voor alle $i$. Dus stel $\sum r_i \tilde{s_i} = 0 + IM$, dan $\sum r_i s_i \in IM$. Dus $\sum r_i s_i = \sum t_j m_j$ voor zekere $t_j \in I, m_j \in M$. Aangezien $m_j \in M$ en $\varphi$ surjectief is zijn er $a_{ji} \in M$ met $\sum_i a_{ji}s_i = m_i$. Dus $\sum r_i s_i = \sum_{i, j} t_ja_{ji}s_i$, en $\sum (r_i - \sum_j t_ja_{ji})s_i = 0$. Dus $r_i - \sum_j t_ja_{ji} = 0$ want $\varphi$ is injectief, en $r_i \in I$ voor alle $i$ want $t_j \in I$ dus $\sum t_j a_{ji} \in I$. 

Dus $\tilde{\varphi}$ is een bijectie. Aangezien $|\tilde{S}| = n$ volgt dat $M / IM$ vrij is van rang $n$.
\end{oplossing}
\end{document}
