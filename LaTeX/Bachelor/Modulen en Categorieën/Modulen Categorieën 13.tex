\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}

% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Opgave]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Oplossing]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\ob}{ob}
\DeclareMathOperator{\ggd}{ggd}
\DeclareMathOperator{\ab}{ab}
\DeclareMathOperator{\Fix}{Fix}
\DeclareMathOperator{\Orb}{Orb}
\DeclareMathOperator{\opp}{opp}


% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Modulen en Categorieën Huiswerk 13}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[10.4]
Zij $n$ een positief geheel getal en beschouw de ring $R := \ZZ[X] / (X^n - 1)$. Zij $M$ het quotiëntmoduul $R / (X - 1)$, $\pi: R \to M$ de quotiëntafbeeldingen. Laat zien dat
\[
\begin{tikzcd}
\dots \arrow{r}{\beta} & R \arrow{r}{\alpha} & R \arrow{r}{\beta} & R \arrow{r}{\alpha} & R \arrow{r}{\pi} & M \arrow{r}  & 0,
\end{tikzcd}
\]
met $\alpha(r) = (X - 1)r$ en $\beta(r) = (X^{n - 1} + \dots + X + 1)r$ een vrije resolutie van het $R$-moduul $M$ is.
\end{opghand}
\begin{oplossing}
De afbeelding $\pi$ is duidelijk surjectief dus de rij is exact in $M$.

Als $\pi(\overline{a(X)}) = 0$, dan $\overline{(X - 1)} \mid \overline{a(X)}$ in $R$, dus $\overline{a(X)} = \overline{b(X)}\overline{(X - 1)}$ voor zekere $b \in R$. Dus $\alpha(\overline{b(X)}) = \overline{a(X)}$, en $\overline{a(X)} \in \im(\alpha)$. Aan de andere kant, $\pi \circ \alpha(\overline{b(X)}) = \pi(\overline{(X - 1)b(X)}) = \overline{0}$, dus $\im(\alpha) = \ker(\pi)$. 

Als $\alpha(\overline{a(X)}) = 0$, dan $X^n - 1 \mid (X - 1)a(X)$ in $\ZZ[X]$, dus $1 + X + \dots + X^{n - 1} \mid a(X)$, en $a(X) = (X^{n - 1} + \dots + 1)b(X)$ voor zekere $b \in \ZZ[X]$. Dus $\beta(\overline{b(X)}) = \overline{a(X)}$, en $\overline{a(X)} \in \im(\beta)$. Aan de andere kant, $\alpha \circ \beta(\overline{b(X)}) = \alpha(\overline{(X^{n - 1} + \dots + 1)b(X)}) = \overline{(X^n - 1)b(X)} = \overline{0}$, dus $\im(\beta) = \ker(\alpha)$. 

Als $\beta(\overline{a(X)}) = 0$, dan $X^n - 1 \mid (X^{n - 1} + \dots + 1)a(X)$ in $\ZZ[X]$, dus $(X - 1) \mid a(X)$, en $a(X) = (X - 1)b(X)$ voor zekere $b \in \ZZ[X]$. Dus $\alpha(\overline{b(X)}) = \overline{a(X)}$, en $\overline{a(X)} \in \im(\alpha)$. Aan de andere kant, $\beta \circ \alpha(\overline{b(X)}) = \beta(\overline{(X - 1)b(X)}) = \overline{(X^n - 1)b(X)} = \overline{0}$, dus $\im(\alpha) = \ker(\beta)$. 

We concluderen dat de rij inderdaad exact is. Aangezien $R$ inderdaad een vrij $R$-moduul is, volgt dat dit een vrije resolutie van $M$ is.
\end{oplossing}
\begin{opghand}[10.8]
Zij $0 \to M_1 \to M_2 \to M_3 \to 0$ een kort exacte rij van $R$-modulen. Laat zien dat er vrije resoluties
\[
\dots \to F_{i, 2} \to F_{i, 1} \to F_{i, 0} \to M_0 \to 0
\]
bestaan voor $i = 1, 2, 3$ en een kort exacte rij
\[
0 \to F_{1, \bullet} \to F_{2, \bullet} \to F_{3, \bullet} \to 0
\]
van ketencomplexen compatibel met de exacte rij $0 \to M_1 \to M_2 \to M_3 \to 0$. 
\end{opghand}
\begin{oplossing}
Met opgave 10.7 vinden we vrije $R$-modulen $F_{1, 1}, F_{2, 1}, F_{3, 1}$ en een commutatief diagram
\[
\begin{tikzcd}
0 \arrow{r} & F_{1, 1} \arrow[two heads]{d}{f_{1, 1}} \arrow{r} & F_{2, 1}\arrow[two heads]{d}{f_{2, 1}} \arrow{r} & F_{3, 1} \arrow[two heads]{d}{f_{3, 1}}\arrow{r} & 0 \\
0 \arrow{r} & M_1 \arrow{r} & M_2 \arrow{r} & M_3 \arrow{r} & 0
\end{tikzcd}
\]
met exacte rijen. Met het slangenlemma vinden we een commutatief diagram
\[
\begin{tikzcd}
0 \arrow{r} & \ker f_{1, 1} \arrow{d} \arrow{r} & \ker f_{2, 1} \arrow[d] \arrow{r} & \ker f_{3, 1} \arrow[d] \arrow{r} & 0 \\
0 \arrow{r} & F_{1, 1} \arrow[two heads]{d}{f_{1, 1}} \arrow{r} & F_{2, 1}\arrow[two heads]{d}{f_{2, 1}} \arrow{r} & F_{3, 1} \arrow[two heads]{d}{f_{3, 1}}\arrow{r} & 0 \\
0 \arrow{r} & M_1 \arrow{r} & M_2 \arrow{r} & M_3 \arrow{r} & 0
\end{tikzcd}
\]
met exacte rijen (de cokernen zijn $0$ want de $f_{1, i}$ zijn surjectief). Passen we nogmaals opgave 10.7 toe, dan vinden we vrij $R$-modulen $F_{1, 2}, F_{2, 2}, F_{3, 2}$ en een commutatief diagram
\[
\begin{tikzcd}
0 \arrow{r} & F_{1, 2} \arrow[two heads]{d}{f_{1, 2}} \arrow{r} & F_{2, 2}\arrow[two heads]{d}{f_{2, 2}} \arrow{r} & F_{3, 2} \arrow[two heads]{d}{f_{3, 1}}\arrow{r} & 0 \\
0 \arrow{r} & \ker f_{1, 1} \arrow{r} & \ker f_{2, 1}  \arrow{r} & \ker f_{3, 1} \arrow{r} & 0
\end{tikzcd}
\]
met exacte rijen. Als we de $f_{i, 2}$ samenstellen met de inclusie $\ker(f_{i, 1}) \to F_{i, 1}$, dan vinden we een commutatief diagram 
\[
\begin{tikzcd}
0 \arrow{r} & F_{1, 2} \arrow{d}{f_{1, 2}} \arrow{r} & F_{2, 2}\arrow{d}{f_{2, 2}} \arrow{r} & F_{3, 2} \arrow{d}{f_{3, 2}}\arrow{r} & 0 \\
0 \arrow{r} & F_{1, 1} \arrow[two heads]{d}{f_{1, 1}} \arrow{r} & F_{2, 1}\arrow[two heads]{d}{f_{2, 1}} \arrow{r} & F_{3, 1} \arrow[two heads]{d}{f_{3, 1}}\arrow{r} & 0 \\
0 \arrow{r} & M_1 \arrow{r} & M_2 \arrow{r} & M_3 \arrow{r} & 0
\end{tikzcd}
\]
met exacte rijen, en per constructie zijn de kolommen ook exact in de $F_{i, 1}$. Vervolgens kunnen we weer het slangenlemma toepassen om een diagram 
\[
\begin{tikzcd}
0 \arrow{r} & \ker f_{1, 2} \arrow{d} \arrow{r} & \ker f_{2, 2} \arrow[d] \arrow{r} & \ker f_{3, 2} \arrow[d] \arrow{r} & 0 \\
0 \arrow{r} & F_{1, 2} \arrow[two heads]{d}{f_{1, 2}} \arrow{r} & F_{2, 2}\arrow[two heads]{d}{f_{2, 2}} \arrow{r} & F_{3, 2} \arrow[two heads]{d}{f_{3, 2}}\arrow{r} & 0 \\
0 \arrow{r} & \ker f_{1, 1} \arrow{r} & \ker f_{2, 1}  \arrow{r} & \ker f_{3, 1} \arrow{r} & 0
\end{tikzcd}
\]
te vinden. Net als hierboven kunnen we dan met opgave 10.7 weer vrije $R$-modulen $F_{1, 3}, F_{2, 3}, F_{3, 3}$ en een commutatief diagram
\[
\begin{tikzcd}
0 \arrow{r} & F_{1, 3} \arrow[two heads]{d}{f_{1, 2}} \arrow{r} & F_{2, 3}\arrow[two heads]{d}{f_{2, 2}} \arrow{r} & F_{3, 3} \arrow[two heads]{d}{f_{3, 1}}\arrow{r} & 0 \\
0 \arrow{r} & \ker f_{1, 1} \arrow{r} & \ker f_{2, 1}  \arrow{r} & \ker f_{3, 1} \arrow{r} & 0
\end{tikzcd}
\]
vinden met exacte rijen, dit induceert een commutatief diagram
\[
\begin{tikzcd}
0 \arrow{r} & F_{1, 3} \arrow{d}{f_{1, 3}} \arrow{r} & F_{2, 3}\arrow{d}{f_{2, 3}} \arrow{r} & F_{3, 3} \arrow{d}{f_{3,3}}\arrow{r} & 0 \\
0 \arrow{r} & F_{1, 2} \arrow{d}{f_{1, 2}} \arrow{r} & F_{2, 2}\arrow{d}{f_{2, 2}} \arrow{r} & F_{3, 2} \arrow{d}{f_{3, 2}}\arrow{r} & 0 \\
0 \arrow{r} & F_{1, 1} \arrow[two heads]{d}{f_{1, 1}} \arrow{r} & F_{2, 1}\arrow[two heads]{d}{f_{2, 1}} \arrow{r} & F_{3, 1} \arrow[two heads]{d}{f_{3, 1}}\arrow{r} & 0 \\
0 \arrow{r} & M_1 \arrow{r} & M_2 \arrow{r} & M_3 \arrow{r} & 0
\end{tikzcd}
\]
met exacte rijen en kolommen. Door dit te herhalen vinden we inderdaad een kort exacte rij van ketencomplexen
\[
0 \to F_{1, \bullet} \to F_{2, \bullet} \to F_{3, \bullet} \to 0
\]
compatibel met $0 \to M_1 \to M_2 \to M_3 \to 0$ (immers alles commuteert), merk op dat de ketencomplexen exact zijn in $M_i$ omdat de afbeeldingen $f_{i, 1}$ naar $M_i$ surjectief zijn. Om te zien dat de $F_{i, \bullet}$ vrije resoluties van de $M_i$ zijn, volstaat het op te merken dat ze per constructie eindigen in $M_i$ en overal exact zijn omdat de kolommen in het laatste diagram exact waren.
\end{oplossing}
\end{document}
