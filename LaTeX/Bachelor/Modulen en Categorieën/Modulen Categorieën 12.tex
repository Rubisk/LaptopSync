\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}

% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Opgave]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Oplossing]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\ob}{ob}
\DeclareMathOperator{\ggd}{ggd}
\DeclareMathOperator{\ab}{ab}
\DeclareMathOperator{\Fix}{Fix}
\DeclareMathOperator{\Orb}{Orb}
\DeclareMathOperator{\opp}{opp}


% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Modulen en Categorieën Huiswerk 12}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[9.7]
Zij $R$ en $S$ ringen en $A$ een $(R, S)$ bimoduul. Voor een ketencomplex $M_\bullet$ in $\prescript{}{R}{\mathbf{Ch}}$ definiëer een ketencomplex $M'_\bullet$ door
\begin{enumerate}[(1)]
\item $M_i' := \Hom_R(M_{-i}, A)$
\item $d_i: M_i' \to M_{i - 1}'$ de afbeelding geïnduceerd door $d_{1 - i}: M_{1 - i} \to M_{-i}$. 
\end{enumerate}
Ga na dat $M'_\bullet$ een ketencomplex van rechtse $S$-modulen is, en dat de operatie $M_\bullet \to M_\bullet'$ functoren $\prescript{}{R}{\mathbf{Ch}} \to \mathbf{Ch}_S$ en $\prescript{}{R}{\mathbf{Ho}^{\opp}} \to \mathbf{Ho}_S$ definiëert.
\end{opghand}
\begin{oplossing}
Het is evident dat $M_i'$ een rechts $S$-moduul is door $(fs)(x) := f(x)s$ voor $x \in M_{-i}$, $f \in M_i'$ en $s \in S$. Merk op dat met de geïnduceerde afbeelding $d^{M'}_i(f) := f \circ d^M_{1 - i}$ wordt bedoeld. Omdat $M_\bullet$ een ketencomplex van rechts-$R$-modulen was, is $d^M_{1 - i}$ een $R$-lineaire afbeelding, dus $f \circ d^M_{1 - i} \in \Hom_R(M_{-i}, A)$. En $d^{M'}_i$  is $S$-lineair, immers voor $x \in M_{-i}$ en $s \in S$ zal 
\[
d^{M'}_i(fs)(x) =  (fs) \circ d^M_{1 - i}(x) = f(d^M_{1 - i}(x))s = \((f \circ d^M_{1 - i})s\)(x) = \(d^{M'}_i(f)s\)(x).
\]
Tenslotte volgt dat 
\[
d_i^{M'} \circ d_{i + 1}^{M'} (f) = f \circ d^M_{1 - ( i + 1)} \circ d^M_{1 - i} = f \circ 0 = 0,
\]
dus $M'_\bullet$ is een ketencomplex van rechtse $S$-modulen. 

Om er een functor $\prescript{}{R}{\mathbf{Ch}} \to \mathbf{Ch}_S$ van te maken, definiëren we gegeven een $f_\bullet: N_\bullet \to M_\bullet$ een afbeelding $f'_\bullet: M'_\bullet \to N'_\bullet$ door
\begin{align*}
f'_i: \Hom_R(M_{-i}, A) &\to \Hom_R(N_{-i}, A) \\
g &\mapsto g \circ f_{-i},
\end{align*}
dit is goed gedefiniëerd want $f_{-i}$ is een $R$-lineaire afbeelding. Dit is een ketencomplex-morfisme want
\begin{align*}
f_i' \circ d_i^{M'}(g) &= g \circ d_i^M \circ f_{-i} \\
&= g \circ f_{1 - i} \circ d_{1 - i}^N \\
&= d_i^{N'} \circ f_{i - 1}(g),
\end{align*}
dus $f_i' \circ d_i^{M'} = d_i^{N'} \circ f_{i - 1}$ zoals benodigd. Het is evident dat $(\id_{M_\bullet})' = \id_{M_\bullet'}$ en $(f_\bullet g_\bullet)' = g'_\bullet \circ f'_\bullet $, dus dit induceert inderdaad een functor $\prescript{}{R}{\mathbf{Ch}} \to \mathbf{Ch}_S$.

Om tenslotte te zien dat het ook een functor $\prescript{}{R}{\mathbf{Ho}^{\opp}} \to \mathbf{Ho}_S$ definiëert, moeten we nagaan dat als $f_\bullet, g_\bullet: N_\bullet \to M_\bullet$ equivalent zijn $(f_\bullet \sim g_\bullet$), dan ook $f'_\bullet \sim g'_\bullet$. Aangezien $f_\bullet \sim g_\bullet$ bestaan er $R$-lineaire $h_i: N_i \to M_{i + 1}$ zodat $g_i - f_i = d_{i + 1}^N \circ h_i + h_{i - 1} \circ d_i^M$. Definiëren we
\begin{align*}
h_i': \Hom_R(M_{-i}, A) &\to \Hom_R(N_{-i - 1}, A), \\
g &\mapsto g \circ h_{-i - 1},
\end{align*}
(dit is welgedefiniëerd want $h_i$ is altijd $R$-lineair) dan zien we dat dit een homotopie van $f_\bullet'$ naar $g_\bullet'$ is, immers voor $\varphi \in \Hom_R(M_{-i}, A)$ zal
\begin{align*}
(g_i' - f_i')(\varphi) &= \varphi \circ (g_{-i} - f_{-i}) \\
&= \varphi \circ d_{1 - i}^N \circ h_{-i} + \varphi \circ h_{-i - 1} \circ d_{-i}^M &(\varphi \text{ lineair}) \\
&= h_{i + 1}' \circ d_i^{N'} (\varphi) + d_{i - 1}^{M'} \circ h_i'(\varphi),
\end{align*}
dus $g_i' - f_i' = d_{i - 1}^{M'} \circ h_i' +  h_{i + 1}' \circ d_i^{N'}$ zoals verlangd. We concluderen dat inderdaad $f_\bullet' \sim g_\bullet'$.
\end{oplossing}
\begin{opghand}[9.8]
Zij $G$ een groep. Beschouw de abelse groepen $G_n = \ZZ^{G^n}$, met $G_0 = \ZZ[[\ ]] \cong \ZZ$ en $G^n = 0$ als $n < 0$. Schrijf $[g_1, \dots, g_n] := e_{(g_1, \dots, g_n)}$ voor de basisvectoren. Definiëer de afbeeldingen
\begin{align*}
d_1: C_1(G) \to C_0(G): [g] &\mapsto [\ ] - [\ ], \\
d_2: C_2(G) \to C_1(G): [g_1, g_2] &\mapsto [g_1] - [g_1g_2] + [g_2],
\end{align*}
(de rest van de $d_n$ zijn irrelevant) en $d_n = 0$ voor $n \leq 0$. Noteer $H_n(G, \ZZ) := H^n(C_\bullet(G))$. 
\end{opghand}
\begin{enumerate}[1)]
\setcounter{enumi}{1}
\item Laat zien dat $H_0(G, \ZZ) \cong \ZZ$. 
\begin{oplossing}
Aangezien $d_0$ de $0$-afbeelding is zien we dat $\ker(d_0) \cong \ZZ$. En $d_1$ stuurt ook alles naar $0$, dus $\im(d_1) = \{0\}$. Er volgt dat $H_0(G, \ZZ) \cong \ZZ / \{0\} \cong \ZZ$. 
\end{oplossing}
\item Laat zien dat $H_1(G, \ZZ) \cong G^{\ab}$. 
\begin{oplossing}
Aangezien $d_1$ de $0$-afbeelding is zien we dat $\ker(d_1) = C_1(G)$. Beschouw de afbeelding  (tussen verzamelingen)
\[
\varphi: C_1(G) \to G^{\ab}: a_1[g_1] + \dots + a_k[g_k] \mapsto \overline{g_1^{a_1} \cdots g_k^{a_k}},
\] 
dit is welgedefiniëerd omdat $G^{\ab}$ abels is, en duidelijk surjectief. De geinduceerde afbeelding
\[
\tilde \varphi: C_1(G) / \im(d_2) \to G^{\ab}
\]
is welgedefiniëerd omdat
\[
\varphi([g_1] - [g_1g_2] + [g_2]) := \overline{g_1(g_1g_2)^{-1}g_2} = \overline{e},
\]
en ook daadwerkelijk een groepshomomorfisme, want
\begin{align*}
\tilde \varphi(\overline{[g_1] + [g_2]}) &= \tilde \varphi(\overline{[g_1 g_2]}) = \overline{g_1g_2} = \tilde \varphi(\overline{[g_1]})\tilde \varphi(\overline{[g_2]}), \\
\tilde \varphi(\overline{-[g_1]}) &= \overline{g_1^{-1}} = \tilde \varphi(\overline{[g_1]})^{-1}, \\
\tilde \varphi(\overline{0}) &= \overline{e}. &(\text{definiëer $\varphi(0) = e$})
\end{align*}
Deze afbeelding blijft surjectief, dus resteert na te gaan dat $\tilde \varphi$ injectief is. Door gebruik te maken van het feit dat $\overline{[a] + [b]} = \overline{[ab]}$ volstaat het na te gaan dat $\tilde \varphi(\overline{[x]} ) = \overline{0} \implies x \in [G, G]$, maar dat is evident want $\overline{x} = \overline{0}$ desda $x \in [G, G]$. We concluderen dat $\tilde \varphi$ een isomorfisme $H_1(G, \ZZ) \to G^{\ab}$ is.
\end{oplossing}
\end{enumerate}
\end{document}
