\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}

% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Opgave]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Oplossing]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\ggd}{ggd}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Modulen en Categorieën Huiswerk 5}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[4.18]
Zij $f: X \to Y$ een morfisme in $\catC$. Laat zien dat $f$ een isomorfisme is dan en slechts dan als voor alle objecten $T$ in $\catC$ the afbeelding
\[
\Hom_\catC(T, X) \to \Hom_\catC(T, Y): h \mapsto fh
\]
een bijectie is.
\end{opghand}
\begin{oplossing}
Neem eerst aan dat $f$ een isomorfisme is. Schrijf $g$ voor de afbeelding $Y \to X$ zodat $gf = \id_X, fg = \id_Y$. Dan is de afbeelding surjectief, want voor elke $h \in \Hom_\catC(T, Y)$ geldt dat $gh \in \Hom_\catC(T, X)$ en $fgh =\id_Y h = h$. En de afbeelding is injectief, want als $fh_1 = fh_2$ voor zekere $h_1, h_2 \in \Hom_\catC(T, X)$, dan zal $g(fh_1) = g(fh_2)$, dus uit associativiteit volgt $\id_X h_1 = \id_X h_2$ en dus $h_1 = h_2$.

Neem nu aan dat de bovenstaande afbeelding een bijectie is voor elke $T \in \text{ob}_\catC$. Als we $T = Y$ nemen, dan bestaat er, omdat de afbeelding surjectief is, een $g \in \Hom_\catC(Y, X)$ met $fg = \id_Y$. Omdat $f \in \Hom_\catC(Y, X)$ en $g \in \Hom_\catC(X, Y)$ zal $gf \in \Hom_\catC(X, X)$. Nemen we nu $T = X$, dan mogen we uit injectiviteit concluderen dat $gf = \id_X$, immers 
\[
f(gf) = (fg)f = \id_Yf = f = f\id_X
\]
dus $gf$ en $\id_X$ hebben hetzelfde beeld onder de afbeelding met $T = X$. Omdat de afbeelding injectief was concluderen we $gf = \id_X$, en dus is $f$ een isomorfisme.
\end{oplossing}
\begin{opghand}[5.1]
Laat $\mathcal{C}$ en $\mathcal{D}$ categorieën, en $F: \mathcal{C} \to \mathcal{D}$ een functor. Zij $f$ een isomorfisme in $\catC$. Laat zien dat $F(f)$ een isomorfisme is in $\mathcal{D}$. Evenzo, laat zien dat $F$ gespleten monomorfismen afbeeldt op gespleten monomorfismen en gespleten epimorfismen afbeeldt op gespleten epimorfismen. 

Geef voorbeelden om te laten zien dat $F$ niet per sé monomorfismen op monomorfismen afbeeldt, of epimorfismen op epimorfismen.
\end{opghand}
\begin{oplossing}
Als $f: X \to Y$ een isomorfisme is, pak dan $g: Y \to X$ zodat $fg = \id_Y$ en $gf = \id_X$. Dan $F(f): F(X) \to F(Y)$ en 
\begin{align*}
F(f)F(g) &= F(fg) = F(\id_Y) = \id_{F(Y)}, \\
F(g)F(f) &= F(gf) = F(\id_X) = \id_{F(X)},
\end{align*}
hieruit volgt dat $F$ een isomorfisme is.

Stel vervolgens dat $f$ een gespleten monomorfisme is. Dan is er een $g: Y \to X$ zodat geldt $gf = \id_X$. En nu zal weer
\[
F(g)F(f) = F(gf) = F(\id_X) = \id_{F(X)},
\]
dus $F(f)$ is een gespleten monomorfisme.

Analoog, stel dat  $f$ een gespleten epimorfisme is. Dan is er een $g: Y \to X$ zodat geldt $fg = \id_Y$. En nu zal
\[
F(f)F(g) = F(fg) = F(\id_Y) = \id_{F(Y)},
\]
dus $F(f)$ is een gespleten epimorfisme.
\newpage
Beschouw vervolgens de categorieën
\begin{center}
$\mathcal{C}$: 
\begin{tikzcd}
X
	\arrow[rr, bend right]{}{h}
	\arrow{r}{f}
&
Y
	\arrow[r]{}{g_1}
&
Z
\end{tikzcd}
en $\mathcal{D}:$
\begin{tikzcd}
X
	\arrow{r}{f}
	\arrow[rr, bend right]{}{h}
&
Y
	\arrow[r]{}{g_1}
	\arrow[r, bend left]{}{g_2}
&
Z
\end{tikzcd}
\end{center}
met natuurlijk de identiteitafbeeldingen $\id_X, \id_Y, \id_Z$, en de samenstellingen gedefiniëerd door voor $i = 1, 2$ te zeggen $f \circ g_i = h$. Laat $F: \mathcal{C} \to \mathcal{D}$ de inclusiefunctor, dan is $f$ wel een monomorfisme in $\mathcal{C}$ (dit is triviaal want de ruimtes $\Hom(Y, Z)$ en $\Hom(Y, Y)$ bevatten in $\mathcal{C}$ maar één element, dus als $fg_1 = fg_2$ dan moet wel $g_1 = g_2$). Maar $f$ is geen monomorfisme in $\mathcal{D}$, immers in $\mathcal{D}$ zal $fg_1 = fg_2$ maar $g_1 \neq g_2$.

Op eenzelfde wijze, beschouw de categorieën
\begin{center}
$\mathcal{C}$: 
\begin{tikzcd}
X
	\arrow[r]{}{g_1}
	\arrow[rr, bend right]{}{h}
&
Y
	\arrow{r}{f}
&
Z
\end{tikzcd}
en $\mathcal{D}:$
\begin{tikzcd}
X
	\arrow[r]{}{g_1}
	\arrow[r, bend left]{}{g_2}
	\arrow[rr, bend right]{}{h}
&
Y
	\arrow{r}{f}
&
Z
\end{tikzcd}
\end{center}
met weer de identiteitafbeeldingen $\id_X, \id_Y, \id_Z$, en de samenstellingen gedefiniëerd door voor $i = 1, 2$ te zeggen $g_i \circ f = h$. Neem $F$ weer de inclusiefunctor. Dan is het (omdat $\Hom(X, Y)$ en $\Hom(Y, Y)$ weer één element bevatten) wel duidelijk dat $f$ een epimorfisme is in $\mathcal{C}$ maar $F(f)$ niet, immers $g_1 f = g_2f$ maar $g_1 \neq g_2$ in $\mathcal{D}$.
\end{oplossing}
\end{document}
