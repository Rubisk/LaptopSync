\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}
\usepackage{tikz-cd}

% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Opgave]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Oplossing]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\ggd}{ggd}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\catC}{\mathcal{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Modulen en Categorieën Huiswerk 8}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[6.20]
Zij $f: R \to S$ een ringhomomorfisme. Laat verder $M$ een eindig voortgebracht $R$-moduul zijn. Toon aan dat $S \otimes_R M$ een eindig voortegebracht $S$-moduul is.
\end{opghand}
\begin{oplossing}
We weten dat $M$ eindig wordt voortgebracht, dus pak voortbrengers $m_1, \dots, m_n \in M$. We bewijzen dat $\{1 \otimes m_i\}$ een verzameling voortbrengers is voor $S \otimes_R M$. Het volstaat aan te tonen dat $s \otimes m \in \text{span}\{1 \otimes m_i\}$ voor alle $s \in S$ en $m \in M$, deze $s \otimes m$ brenger immers $S \otimes_R M$ voort. Aangezien de $m_i$ voortbrengers zijn van $M$ zijn er $r_i \in R$ met $m = \sum r_im_i$. Dan 
\[
s \otimes m = \sum (sf(r_i) \otimes m_i) = \sum sf(r_i) (1 \otimes m_i),
\]
dus $s \otimes m \in \text{span}\{1 \otimes m_i\} $ zoals verlangd.
\end{oplossing}
\begin{opghand}[6.22]
Laat $f: R \to R'$ en $g: R' \to R''$ ringhomomorfismen zijn, en beschouw de extensie-van-scalairenfunctor
\[
\prescript{}{R}{\mathbf{Mod}} \to \prescript{}{R'}{\mathbf{Mod}} \to \prescript{}{R''}{\mathbf{Mod}}.
\]
Laat zien dat de samenstelling van deze functoren isomorf is aan de extensie van scalairen functor
\[
\prescript{}{R}{\mathbf{Mod}} \to \prescript{}{R''}{\mathbf{Mod}}
\]
van het ringhomomorfisme $gf: R \to R''$. 
\end{opghand}
\begin{oplossing}
Noteer de functoren met $f_*, g_*$ en $gf_*$. We willen bewijzen dat er isomorfismen \linebreak $\eta_M: R'' \otimes_{R'} (R' \otimes_R M) \to R'' \otimes_R M$ bestaan zodat voor elk moduulhomomorfisme $\varphi: M \to N$
\[
     \begin{tikzcd}[column sep=large]
     R'' \otimes_{R'} (R' \otimes_R M) \arrow{r}{(g_* \circ f_*)(\varphi)} \arrow{d}{\eta_M} & R'' \otimes_{R'} (R' \otimes_R N)   \arrow{d}{\eta_N} \\
     R'' \otimes_R M \arrow{r}{gf_*(\varphi)} &  R'' \otimes_R N
     \end{tikzcd}
\]
commuteert. De afbeelding $\xi: R' \times M \to R'' \otimes_R M: (r', m) \mapsto g(r')\otimes m$ is $R$-bilineair, immers
\[
\xi(r'r, m) = \xi(r'f(r), m) = g(r'f(r)) \otimes m = g(r')(g \circ f)(r) \otimes m = g(r') \otimes rm = \xi(r', rm).
\]
We vinden dus een homomorfisme $R' \otimes_R M \to R'' \otimes_R M$ dat $r' \otimes m$ afbeeldt op $g(r') \otimes m$. Uit de universele eigenschap van extensie van scalairen vinden we een (unieke) $R''$-lineaire afbeelding  $\eta_M: R'' \otimes_{R'} (R' \otimes_R M) \to R'' \otimes_R M$ die $1 \otimes (r' \otimes m)$ afbeeldt op $g(r') \otimes m$. 

We laten zien dat deze $\eta_M$ het diagram doet commuteren. Het volstaat aan te tonen dat het commuteert voor elke $r'' \otimes (r' \otimes m)$ deze brengen immers de ruimte voort (door net als in het vorige huiswerk de $r''$ eerst vast te houden) en alle afbeeldingen zijn wel groepshomomorfismen. Nu
\begin{align*}
\eta_N \circ (g_* \circ f_*)(\varphi) (r'' \otimes (r' \otimes m)) &= \eta_N(r'' \otimes (r' \otimes \varphi(m))) \\
&= r'' g(r) \otimes \varphi(m) &(\eta_N \text{ is $R''$-lineair})\\
&= gf_*(r''g(r) \otimes m) \\
&= gf_* \circ \eta_M(r'' \otimes (r \otimes m)) &(\eta_M \text{ ook}),
\end{align*}
zoals benodigd.

Resteert te laten zien dat $\eta_M$ een isomorfisme is. Merk op dat als we uit opgave 6.11 en voorbeeld 6.7 weten dat de afbeeldingen 
\begin{align*}
\mu&: R'' \otimes_{R'} (R' \otimes_R M) \to (R'' \otimes_{R'} R') \otimes_R M: x \otimes (y \otimes z) \mapsto (x \otimes y) \otimes z \\
\nu&: (R'' \otimes R') \to R'': x \otimes y \mapsto xy
\end{align*}
isomorfismen van $R''$-modulen zijn. Dus $\nu' := \nu \otimes \id$ is ook een isomorfisme (met als inverse $\nu^{-1} \otimes \id$). En $\nu' \circ \mu = \eta_M$, immers
\[
\nu' \circ \mu(r'' \otimes (r' \otimes m)) = \nu'((r'' \otimes r') \otimes m) = r'' r \otimes m = r''g(r') \otimes m = \eta_M(r''\otimes(r'\otimes m)),
\]
dus $\eta_M$ is een samenstelling van isomorfismen en dus ook een isomorfisme.
\end{oplossing}
\end{document}
