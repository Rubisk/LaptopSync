\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}

% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Opgave]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Oplossing]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\id}{id}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Modulen en Categorieën Huiswerk 2}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1.16]
Bewijs de volgende generalisatie van Propositie 1.15: Zij $R$ een ring, en laat $(M_i)_{i \in I}$ een verzameling $R$-modulen geïndexeerd door een verzameling $I$. Zij $N$ een $R$-moduul, en laat $(f_i: M_i \to N)_i$ een verzameling $R$-lineaire afbeeldingen. Dan bestaat er een unieke $R$-lineaire afbeelding
\[
f: \bigoplus_{i \in I} M_i \to N
\]
such that for all $i \in I$ and $x \in M_i$ we have $f(\iota_i(x)) = f_i(x)$.
\end{opghand}
\begin{oplossing}
Merk op dat we elke element in $M := \bigoplus_{i \in I} M_i$ op een unieke manier (!) kunnen schrijven als een eindige som $\sum \iota_i(x_i)$ met $x_i \in M_i$. Definiëer dan $f(\sum \iota_i(x_i)) = \sum f_i(x_i)$, dit kan omdat het een eindige som is en de schrijfwijze uniek is. Merk op dat deze definitie direct volgt uit de eis dat $f(\iota_i(x_i)) = f_i(x)$ en de eis dat $f$ een homomorfisme van abelse groepen is, dus de keuze voor $f$ is in ieder geval uniek.

Dit is een $R$-lineaire afbeelding, want als we nog zo'n eindige som $\sum \iota_i(y_i)$ hebben, dan bestaat $\sum \iota_i(x_i) + \iota_i(y_i)$ ook uit eindig veel termen die niet nul zijn. Dus 
\begin{align*}
f\(\sum \iota_i(x_i) + \sum \iota_i(y_i)\) &= f\(\sum \iota_i(x_i + y_i)\) = \sum f_i(x_i + y_i) \\
&= \sum f_i(x_i) + \sum f_i(y_i) = f\(\sum \iota_i(x_i)\) + f\(\sum \iota_i(y_i)\).
\end{align*}
En het is duidelijk dat voor $r \in R$
\[
f\(r \sum \iota_i(x_i)\) = \sum f_i(rx_i) = r\sum f_i(x_i) = rf\(\sum \iota_i(x_i)\).
\]

Tenslotte is duidelijk dat per definitie $f(\iota_i(x_i)) = f_i(x_i)$.
\end{oplossing}
\newpage
\begin{opghand}[2.9] Zij $R$ een ring, laat
\[
M_1 \xrightarrow{\varphi_1} M_2 \xrightarrow{\varphi_2} M_3 \xrightarrow{\varphi_3} 0
\]
een exacte rij van $R$-modulen, en $N$ een $R$-moduul. Toon aan dat er een exacte rij van abelse groepen bestaat die eruit ziet als
\[
0 \xrightarrow{\phi_1} \Hom_R(M_3, N) \xrightarrow{\phi_2} \Hom_R(M_2, N) \xrightarrow{\phi_3} \Hom_R(M_1, N).
\]
Geef een voorbeeld om aan te tonen dat de exactheid van $0 \longrightarrow M_1 \longrightarrow M_2 \longrightarrow M_3 \longrightarrow 0$ niet hoeft te impliceren dat de afbeelding $\Hom_R(M_2, N) \longrightarrow \Hom_R(M_1, N)$ surjectief is.

\emph{Merk op dat de afbeeldingen namen hebben gekregen om dingen overzichtelijk te houden}.
\end{opghand}
\begin{oplossing}
We construeren eerst de afbeeldingen door
\begin{align*}
\phi_1: f &\mapsto (\phi_1(f): x \mapsto 0), \\
\phi_2: g_3 &\mapsto (\phi_2(g_3): m_2 \mapsto g_3 \circ \varphi_2(m_2)), \\
\phi_3: g_2 &\mapsto (\phi_3(g_2): m_1 \mapsto g_2 \circ \varphi_1(m_1)).
\end{align*}
Aangezien $g_3, g_2$ en $\varphi_1, \varphi_2$ allemaal $R$-lineair zijn volgt dat $\phi_2(g_2)$ en $\phi_3(g_3)$ altijd $R$-lineaire afbeeldingen zijn, dus de $\phi_i$'s beelden af in de $\Hom$-ruimten. En alle $\phi_i$ zijn duidelijk homomorfismes van abelse groepen (voor $i = 2, 3$ geldt $\phi(f_i + g_i) = f_i \circ \varphi_{i - 1} + g_i \circ \varphi_{i - 1}$).

Resteert na te gaan dat dit exact is. Voor exactheid in $\Hom_R(M_3, N)$ volstaat het aan te tonen dat $\phi_2$ injectief is, dus pak $g_3 \in \Hom_R(M_3, N)$ met $\phi_2(g_3) \equiv 0$, we moeten bewijzen dat $g_3 \equiv 0$. Voor alle $m_2 \in M_2$ geldt nu $g_3 \circ \varphi_2(m_2) = 0$. Uit exactheid van de oorspronkelijke rij in $M_3$ volgt dat $\phi_2$ surjectief is, dus voor alle $m_3 \in M_3$ zal $g_3(m_3) = 0$, immers $m_3 = \varphi_2(y)$ voor een $y \in M_2$. Dus $g_3 \equiv 0$.

Nu tonen we exactheid in $\Hom_R(M_2, N)$ aan. Pak dus $g_2 \in \ker(\phi_3)$, we bewijzen dat dan ook $g_2 \in \im(\phi_2)$. Aangezien $g_2 \in \ker(\phi_3)$ zal $g_2 \circ \varphi_1 \equiv 0$. Dit betekent dat $\im(\varphi_1) \subseteq \ker(g_2)$. Vanwege exactheid in $M_2$ zal $\im(\varphi_1) = \ker(\varphi_2)$, dus $\ker(\varphi_2) \subseteq \ker(g_2)$. Aangezien $\varphi_2$ surjectief is, bestaat er voor alle $m_3 \in M_3$ een $m_2 \in M_2$ met $\varphi_2(m_2) = m_3$. Dus laat $g_3(m_3) := g_2(m_2)$, dit is welgedefiniëerd precies omdat $\ker(\varphi_2) \subseteq \ker(g_2)$ (het hangt dus niet van de keuze voor $m_2$ af). En nu geldt duidelijk dat $\phi_2(g_3) = g_2$, dus $\ker(\phi_3) \subseteq \im(\phi_2)$. En andersom zal $\im(\phi_2) \subseteq \ker(\phi_3)$, immers $\phi_3 \circ \phi_2(g_3) = g_3 \circ \varphi_2 \circ \varphi_1$ en $\varphi_2 \circ \varphi_1 \equiv 0$ want de oorspronkelijke rij is exact. Dus $\im(\phi_2) = \ker(\phi_3)$ en de nieuwe rij is exact.

Als tegenvoorbeeld, beschouw $M_1 = M_2 = \ZZ$, $M_3 = \ZZ/2\ZZ$ en $\varphi_1(x) = 2x$, $\varphi_2(x) = \overline{x \pmod{2}}$. Het is duidelijk dat $\varphi_1$ injectief is, $\varphi_2$ surjectief en $\im(\varphi_1) = 2\ZZ = \ker(\varphi_2)$ dus dit is een kort exacte rij $0 \longrightarrow M_1 \longrightarrow M_2 \longrightarrow M_3 \longrightarrow 0$. Maar als we ook $N = \ZZ$ nemen dan is $\phi_3$ niet injectief. Immers de afbeelding $\text{id} \in \Hom(\ZZ, \ZZ)$ maar $\text{id} \not \in \phi_3(\Hom(\ZZ, \ZZ))$. Immers voor $g_2 \in \Hom(\ZZ, \ZZ)$ zal $(\phi_3(g_2))(1) = g_2 \circ \varphi_1(1) = g_2(2) = 2 g_2(1)$. Dus $(\phi_3(g_2))(1)$ is een even geheel getal (want $g_2(1) \in \ZZ$), maar $\id(1) = 1$ is oneven. Dus $\phi_3(g_2) \neq \id$ en $\phi_3$ is niet surjectief.
\end{oplossing}
\end{document}
