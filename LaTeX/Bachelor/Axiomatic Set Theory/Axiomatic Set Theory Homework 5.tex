\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Lemma]{lemhand}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\On}{On}
\DeclareMathOperator{\cf}{cf}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

\title{Axiomatic Set Theory Homework 5}
\author{Mike Daas, Wouter Rienks}
%% Document
\begin{document}
\maketitle
\begin{lemhand}[3.8.4]
Show that for any ordinal $\lambda$, $\cf(\lambda)$ is a regular ordinal.
\end{lemhand}
\begin{proof}
We already know that for any ordinal $\omega$ we have $\cf(\omega) \leq \omega$, it thus suffices to show that we can't have $\cf(\cf(\lambda)) < \cf(\lambda)$. Thus suppose $\mu = \cf(\cf(\lambda)) < \cf(\lambda)$. By definition of cofinality, we have increasing sequences 
\begin{align*}
(a_i)_{i < \mu} &\text{ cofinal in } \cf(\lambda), \\
(b_i)_{i < \cf(\lambda)} &\text{ cofinal in } \lambda,
\end{align*}
with $a_i < \cf(\lambda)$ and $b_i < \lambda$. Since $a_i < \cf(\lambda)$ the sequence $(b_{a_i})_{i < \mu}$ is well defined. We show this sequence is cofinal in $\lambda$, which contradicts $\mu < \cf(\lambda)$ since $b_{a_i} < \lambda$ for all $i$. Thus pick $\beta < \lambda$. Since $(b_i)_{i < \cf(\lambda)}$ is cofinal in $\lambda$, there exists $i < \cf(\lambda)$ such that for $b_i > \beta$. Since $(a_i)_{i < \mu}$ is cofinal in $\cf(\lambda)$, there exists $j < \mu$ such that $a_j > i$. Therefore $b_{a_j} > b_i > \beta$ since $(b_i)_{i < \cf(\lambda)}$ is increasing. This shows $(b_{a_i})_{i < \mu}$ is cofinal in $\lambda$ as desired. 
\end{proof}
\begin{opghand}[3.11.2A]
Let $\beta$ be an ordinal and, and let $\kappa_\alpha, \alpha < \beta$ be infinite cardinals with $\kappa = \sum_{\alpha < \beta} \kappa_\alpha$. Prove that, for any cardinal $\lambda$,
\[
\lambda^\kappa = \prod^\#_{\alpha < \beta}\lambda^{\kappa_\alpha}.
\]
\end{opghand}
\begin{oplossing}
It suffices to find a bijection $\prescript{\kappa}{}\lambda \to \prod_{\alpha < \beta} \lambda^{\kappa_\alpha}$. By using the existing bijections $\lambda^{\kappa_\alpha} \to \prescript{\kappa_\alpha}{}\lambda$ in each component, we find a bijection $\prod_{\alpha < \beta} \lambda^{\kappa_\alpha} \to \prod_{\alpha < \beta} \prescript{\kappa_\alpha}{}\lambda$. Similarly, by using the bijection $\kappa \to \bigcup_{\alpha < \beta} (\kappa_\alpha \times \{\alpha\})$, we find a bijection $\prescript{\bigcup_{\alpha < \beta} (\kappa_\alpha \times \{\alpha\})}{} \lambda \to \prescript{\kappa}{}\lambda$.  It thus suffices to find a bijection
\[
\prescript{\bigcup_{\alpha < \beta} (\kappa_\alpha \times \{\alpha\})}{} \lambda \to \prod_{\alpha < \beta} \prescript{\kappa_\alpha}{}\lambda.
\]

Use $(g_\alpha)_{\alpha < \beta}$ to denote an arbitrary element in the cartesian product, where $g_\alpha: \kappa_\alpha \to \lambda$. Consider the function
\begin{align*}
\prescript{\bigcup_{\alpha < \beta} (\kappa_\alpha \times \{\alpha\})}{} \lambda &\to \prod_{\alpha < \beta} \prescript{\kappa_\alpha}{}\lambda \\
f &\mapsto \(x \mapsto f(x, \alpha)\)_{\alpha < \beta}.
\end{align*}
It's easily seen that the function
\begin{align*}
\prod_{\alpha < \beta} \prescript{\kappa_\alpha}{}\lambda &\to \prescript{\bigcup_{\alpha < \beta} (\kappa_\alpha \times \{\alpha\})}{} \lambda \\
(g_\alpha)_{\alpha < \beta} &\mapsto \((x, \alpha) \mapsto g_\alpha(x)\)
\end{align*}
is a two-sided inverse, which shows the above function is a bijection.
\end{oplossing}
\newpage
\begin{opghand}[3] Complete the following tasks:
\begin{enumerate}[1.]
\item Show that, if 	$\kappa$ is a strongly inaccessible cardinal then $V_\kappa$ is closed under the powerset operation: i.e. if $x \in V_\kappa$, then $\mathcal{P}(x) \in V_\kappa$. 
\begin{oplossing}
Suppose $x \in V_\kappa$. Since $\kappa$ is a limit ordinal, there is $\mu < \kappa$ such that $x \in V_\mu$. Since $\alpha < \beta$ implies $V_\alpha \subseteq V_\beta$ for any $y \in x$ we have $y \in V_\mu$. Thus for any subset $a$ of $x$ we have $a \in V_{\mu + 1}$, and therefore $\mathcal{P}(x) \in V_{\mu + 2}$. Since $\kappa$ is a limit ordinal, $\mu + 2 < \kappa$, thus $\mathcal{P}(x) \in V_\kappa$. 
\end{oplossing}
\item Show that, if $\kappa$ is a strongly inaccessible cardinal then $V_\kappa$ is closed under the union operation: i.e. if $x \in V_\kappa$, then $\bigcup x \in V_\kappa$. 
\begin{oplossing}
Once again, for any $x \in V_\kappa$ we can find $\mu < \kappa$ such that $x \in V_\mu$. As above, for any $y \in x$ we have $y \in V_\mu$. Thus for any $z \in y \in x$ we have $z \in V_\mu$. Hence $\bigcup x \in \mathcal{P}(V_\mu) = V_{\mu + 1}$, thus $\bigcup x \in V_\kappa$ since $\mu + 1 < \kappa$. 
\end{oplossing}
\item Show that, if $\kappa$ is a strongly inaccessible cardinal then every subset of $V_\kappa$ of size strictly smaller than $\kappa$ is in $V_\kappa$: i.e. if $A \subseteq V_\kappa$ and $|A| < \kappa$ then $A \in V_\kappa$. 
\begin{oplossing}
If $A \subseteq V_\kappa$, then since $\kappa$ is a limit ordinal for any $a \in A$ there exists $\mu_a < \kappa$ such that $a \in V_{\mu_a}$. Let $\mu = \bigcup_{a \in A} \mu_a$. Then $A \in V_\mu$, and $\mu < \kappa$ since $\kappa$ is regular, $|A| < \kappa$ and $\mu = \bigcup_{a \in A} \mu_A$. Thus $A \in V_\kappa$. 
\end{oplossing}
\item Show that if $\kappa$ is a strongly inaccessible cardinal then $V_\kappa$ is a model of all the axioms of ZFC. 
\begin{oplossing}
We verify all axioms.
\begin{itemize}
\item \emph{Existentionality.} This clearly holds, since for $x \in V_\kappa$ we have $z \in x \land z \in V_\kappa \iff z \in x$.
\item \emph{Null set Axiom.} This is clear, since $x \not \in \emptyset$ for all sets $x$ then we also have $x \not \in \emptyset$ for 
all $x \in V_\kappa$. 
\item \emph{Axiom of Infinity.} Since we assume $\kappa$ to be uncountable, for any countable set $A$ we have $|A| < \kappa$. Thus $\omega < \kappa$ and ${\omega + 1} \leq \kappa$, and $V_{\omega + 1} \subseteq V_\kappa$. Let $x_0 = \emptyset$ and $x_{n + 1} = \{x_n\}$. By induction we have $x_n \in V_{n + 1}$, thus $\{x_n \mid n \in \NN\} \in V_{\omega + 1} \subseteq V_\kappa$ is a set satisfying the axiom of infinity. 
\item \emph{Power set Axiom.} This follows directly from 1.
\item \emph{Axiom of Union.} This follows directly from 2.
\item \emph{Axiom of Replacement.} We show by induction on ordinals that for any $\mu < \kappa$ we have $|V_\mu| < \kappa$. If $\mu = 0$ it's true since $\kappa$ is uncountable. If $\mu = \eta + 1$, then $|V_\mu| = 2^{\eta} < \kappa$ since $|\eta| < \kappa$ by induction. Finally if $\mu$ is a limit ordinal, then $|V_\mu| \leq \sum_{\lambda < \mu} |V_\lambda| < \kappa$ since $\kappa$ is regular (and $|V_\lambda| < \kappa$ by induction). Thus for any $x \in V_\kappa$ we have $x \in V_\mu$ for some $\mu < \kappa$. Thus since $x \subseteq V_\mu$ we have $|x| < \kappa$. Therefore, since any set obtained from $x$ by the axiom of replacement is obviously in bijection with $x$, any set $y$ obtained from axiom of replacement applied to some $x \in V_\kappa$ satisfies $|y| < \kappa$. By 3, we thus have $y \in V_\kappa$ as well (note that $y \subseteq V_\kappa$ since we apply the axiom of replacement restricted to elements of $V_\kappa$, hence all the $b$'s must be in $V_\kappa$). 
\item \emph{Axiom of Subset Selection.} Is induced by the axiom of replacement.
\item \emph{Axiom of Foundation.} If $x \in V_\kappa$, then for any $a \in x$ we have $a \in V_\kappa$, thus the $a$ we get from the original axiom also works for our restricted axiom.
\item \emph{Axiom of Choice.} Given $I$ and $(X_i)_{i \in I}$ all sets in $V_\kappa$, we know by the axiom of choice that there exists some $f: I \to \bigcup X_i$ such that $f(i) \in X_i$. Clearly $|f| = |I|$ (since it's a set of pairs with a unique first element in $I$), thus since $I \in V_\kappa$ we have $|I| < \kappa$. Thus $|f| < \kappa$, and $f \in V_\kappa$ as needed.
\end{itemize}
\end{oplossing}
\end{enumerate}
\end{opghand}
\end{document}
