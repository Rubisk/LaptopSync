\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Lemma]{lemhand}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\On}{On}
\DeclareMathOperator{\cf}{cf}
\DeclareMathOperator{\Def}{Def}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

\title{Axiomatic Set Theory Homework 8}
\author{Mike Daas, Wouter Rienks}
%% Document
\begin{document}
\maketitle
\begin{opghand}Prove the two main results on slide 14 of Lecture Notes 14:
\begin{enumerate}[1]
\item Show that, if $M$ is a transitive class, then $M$ is a first-order universe
iff $M$ satisfies all the axioms of ZF (without the axiom of choice).
\begin{oplossing}
First suppose $M$ satisfies all the axioms of ZF. We show it satisfies all the axioms of a first-order universe.
\begin{enumerate}[1)]
\item For $x \in M, y \subseteq x$, $y \in \Def(M)$ by subset selection there exists some set $a \in M$ such that $a = y$ wrt to $M$. In other words, if $\varphi$ is a formula defining $y$ over $M$, then $t \in a$ iff $\varphi(t)$ for all $t \in M$. Thus $a \cap M = y$. Since $a \in M$ and $M$ is transitive we also have $a \subseteq M$. Thus $a = y$ as required.
\item $\emptyset \in M$ is satisfied by the null set axiom since $M$ is transitive, since if $a \in M$ satisfies $x \not \in a$ for all $a \in M$, we must have $a = \emptyset$. 
\item If $x, y \in M$ then by the pair axioms there exists a set $P \in M$ such that 
\[
a \in P \land a \in M \iff a = x \lor a = y
\]
Thus $P = \{x, y\}$, since for any set $a \in P$ we have $a \in M$ as $M$ is transitive, and clearly $x, y \in P$ since $x, y \in M$. 
\item If $x \in M$ then by the union axiom there exists a set $U$ such that $a \in U$ if and only if there exists $b \in x$ such that $a \in b$. Thus $U = \bigcup x$ since all elements (and elements of elements in $x$) are also in $M$ as $M$ is transitive.
\item If $x \in M$ then by the powerset axiom there exists some $P \in M$ such that $y \in P \land y \in M$ iff $y \in M \land y  \subseteq x$. Clearly this is $P(x) \cap M$, since again if $a \in P$ then $a \in M$. 
\item By repeatedly applying (2) and (3), we can assume $n \in M$ for any $n \in \omega$. Since there exists some infinite set $y$ by the infinity axiom, we can construct a class function $F$ that maps $y$ surjectively onto $\omega$. Hence there exists some set $f$ such that $f = F[y]$ wrt to $M$. Since $n\in M$ for any $n \in \omega$, we have $n \in f$. And since $F[y] = \omega$, for any $x \not \in \omega$ with $x \in f$, we have $x \in M$ (as $M$ is transitive), therefore also $x \not \in f$, contradiction. Thus $\omega = f$ (as sets). 
\item For any definable $F: M \to M$ there exists a formula $\varphi$ such that $\varphi(s, y)$ if and only $y = F(s)$. Since $M$ satisfies replacement axiom, there exists a set $X \in M$ such that $a \in M \land a \in X \iff \exists t\in x:  \varphi(t, a)$. Since $X \in M$ we have $X \subseteq M$, thus $X \subseteq F[x]$. Since $F$ maps into $M$, we have $F[x] \subseteq M$, thus $F[x] = X$. 
\end{enumerate}

Now suppose $M$ satisfies all the axioms of a first-order universe, we show it satisfies all the axioms of ZF.
\begin{itemize}
\item \emph{Existentionality.} Let $x, y \in M$. Suppose $a \in x \iff a \in y$ for all $a \in M$. Then for all $a \not \in M$ we have $a \not \in x, y$ as $M$ is transitive, thus $a \in x \iff  a \in y$ for all $a \in M$, hence $x = y$. 
\item \emph{Null set Axiom.} By (2) there exists some set $\emptyset \in M$ such that $x \not \in \emptyset$ for all $x \in M$. As above, we can't have $a \in \emptyset$ for $a \not \in M$, hence this is in fact the empty set.
\item \emph{Axiom of Infinity.} Clearly $\omega \in M$ by (6).  
\item \emph{Power set Axiom.} Let $x \in M$. Then $\mathcal{P}(x) \cap M \in M$ by (5). Clearly $\mathcal{P}(x) \cap M \in M = \{y \in M \mid y \subseteq x\}$ thus the definition of $\mathcal{P}(x)$ (in $M$). 
\item \emph{Axiom of Union.} Fix $x \in M$. By (4), there exists some set $U \in M$ such that $b \in U$ iff there exists $a \in x$ with $b \in a$ (as $M$ is transitive), thus $U$ is clearly the union of $x$ in $M$. 
\item \emph{Axiom of Replacement.} Fix $x \in M$ and some (definable) class function $F: M \to M$ over $M$ (since we only need to show replacement holds for $M$, we may indeed assume it is definable over $M$). By (7) the image is in $M$. 
\item \emph{Axiom of Foundation.} Suppose $x \in M$. Then $x$ is a set, so there is a set $a \in x$ such that $a \cap x = \emptyset$. Since $M$ is transitive, $a \in M$, hence there is $a \in M$ such that $a \cap x = \emptyset$. 
\end{itemize}
\end{oplossing}
\item Show that, if $M$ is a transitive class such that $\Def(M) \subseteq M$, then $M$ is a first order universe.
\begin{oplossing}
We show $M$ satisfies all the axioms.
\begin{enumerate}[1)]
\item For $x \in M, y \subseteq x$, $y \in \Def(M)$ we have $y \in M$ since $\Def(M) \subseteq M$.
\item Consider $\varphi(x): \lnot(x = x)$, then $\varphi$ defines the empty set over $M$.
\item If $x, y \in M$ then consider
\[
\varphi(t): t = x \lor t = y.
\]
Clearly $\varphi$ defines $\{x, y\}$, hence $\{x, y\} \in M$. 
\item For $x \in M$, consider
\[
\varphi(t): \exists(y \in x)(t \in y)
\]
this defines $\bigcup x$, hence $\bigcup x \in M$. 
\item For $x \in M$, let
\[
\varphi(y) = (\forall z \in y)(z \in x)\land y \in M
\]
this defines $\mathcal{P}(x) \cap M$ over $M$, thus $\mathcal{P}(x) \cap M \in M$. 
\item Clearly any finite number is definable over $M$ (as $0 = \emptyset \in M$, and if $n \in M$ that $n + 1 = n \cup \{n\}$ is definable over $M$), thus any $n \in \omega$ is in $M$. Consider the formula
\[
\varphi(x) = \forall w((\emptyset \in w) \land (\forall z \in w)(z \cup \{z\} \in w)) \to (x \in w),
\]
this defines $\omega$ over $M$, which shows $\omega \in \Def(M)$ thus $\omega \in M$. 
\item For any $x \in M$ and $F: M \to M$ (definable over $M$), let $\varphi(x, y)$ define $F$ over $M$. Consider
\[
\psi(t) = \exists x: \varphi(x, t),
\]
this defines $F[x]$ (over $M$), hence $F[x] \in M$.
\end{enumerate}
\end{oplossing}
\end{enumerate}
\begin{opghand}[2] Answer the following questions:
\begin{enumerate}
\item Let $A = \{1, \omega, \omega^2, \{\omega\}, \{\omega^3\}\}$. If $F$ is the Mostowski-Shepherdson mapping on $(A, \ni)$, compute the image set $F[A]$. Note that $(F[A], \ni)$ is not isomorphic to $(A, \ni)$. Explain why this does \emph{not} contradict the Mostowski-Shepherdson theorem.
\begin{oplossing}
We see that
\begin{align*}
1^\ni = \emptyset \implies F(1) &= F[\emptyset] = \emptyset = 0\\
\omega^\ni = \{1\} \implies F(\omega) &= F[\{1\}] = \{\emptyset\} = 1\\
\(\omega^2\)^\ni = \{1, \omega\} \implies F(\omega^2) &= F[\{1, \omega\}] = \{0, 1\} = 2 \\
\(\{\omega\}\)^\ni = \{\omega\} \implies F(\{\omega\}) &= F[\{\omega\}] = \{1\} \\
\(\{\omega^3\}\)^\ni = \emptyset \implies F(\{\omega^3\}) &= F[\emptyset] = 0.
\end{align*}
Clearly it's not isomorphic since $\{\omega^3\} \not \in \omega$ yet $F(\{\omega^3\}) \in F(\omega)$. It does not contradict the theorem as $A$ is not extensional, since both $1$ and $\{\omega^3\}$ have no predecessors yet $1 \neq \{\omega^3\}$. 
\end{oplossing}
\item  Show that for a finite set $A$ of ordinals, the Mostowski-Shepherdson mapping on $(A, \ni)$ maps $A$ onto the natural number $n$ (i.e. $F[A] = n$), where $n = |A|$ is the number of elements of $A$.
\begin{oplossing}
We give a proof by induction on $|A|$. If $|A| = 0$ then $F[A] = \emptyset$ as $A = \emptyset$. Now suppose it holds for all $B$ with $|B| < |A|$. Let $\eta$ be the largest ordinal in $A$, and set $A' = A \setminus \{\eta\}$. Write $n = |A|$. Then $\eta^\ni = A'$ and by induction $F[A'] = n - 1$. Thus $F(\eta) = F[A'] = n - 1$, and $F[A] = (n - 1) \cup \{n - 1\} = n$, as required. 
\end{oplossing}
\item Show that, if $A$ is an infinite set of natural numbers, then the Mostowski-Shepherdson mapping on $(A, \ni)$ maps $A$ onto $\omega$ (i.e. $F[A] = \omega$). 
\begin{oplossing}
First note that for $a \in A, a^\ni \subseteq a$, thus $a^\ni$ is a finite set of natural numbers. By 2, $F[a^\ni]$ is a natural number, hence $F(a) = F[a^\ni]$ is a natural number. Thus $F[A] \subseteq \omega$. Since $A$ is infinite (and well-ordered by $\in$), there exists $a \in A$ having $n \in \omega$ predecessors. Then $a^\ni$ has $n$ elements, so $F(a) = F[a^\ni] = n$ by (2). Thus $n \in F[A]$ for any $n \in \omega$, and $F[A] = \omega$.
\end{oplossing}
\item Show that, if $A$ is a proper class of ordinals (i.e. not a set!), then the Mostowski-Shepherdson mapping on $(A, \ni)$ maps $A$ onto the class $\On$ of all ordinals.
\begin{oplossing}
Since $A \subseteq \On$, the relation $\ni$ well-orders $A$. Thus $F[A]$ is a well-ordered proper class as $F$ is an isomorphism by the theorem about the MS-map (since $F$ is bijective, $F[A]$ can't be a set as than $A$ would be a set by replacement). Furthermore, $F[A]$ has to be transitive by the theorem about the MS-map. 

For any $x, a \in A$, $F(a) \in F(x) \iff a \in x$, thus $F(x) = F[x^\ni] = \{F(a) \mid a \in x\} = \{t \in F[A] \mid t \in F(x)\}$. Thus $F(x)$ is well-ordered by $\ni$ as it is simply the set of predecessors of $F(x)$ wrt the well-ordering $\ni$ of $F[A]$. 

Since $F[A]$ is transitive, for any $x \in A$ and $t \in F(x)$ there is $a \in A$ with $F(a) = t$. Thus $\{b \in x \mid b \in t \} = \{b \in x \mid b \in F(a)\} = \{F(c) \mid c \in x \land c \in a \land c \in A\} = \{F(c) \mid c \in a\} = F[a^\ni] = F(a) = t$, and the set of predecessors of any $t \in x$ is equal to $t$. This shows that $F(x)$ is an ordinal for all $x \in A$.

Hence $F$ maps into $\On$. As the image of $F$ is transitive by the theorem about the MS-map, we only need to show that $F$ is unbounded to conclude $F$ maps onto $\On$. But that is obvious, since if $F[A] \subseteq \lambda$ for some $\lambda \in \On$, then $F[A]$ is a set. As $F$ is an isomorphism by the MSTV theorem, $A$ would also be a set, but $A$ is a proper class. Contradiction, we conclude $F[A] = \On$. 
\end{oplossing}
\end{enumerate}
\end{opghand}
\end{opghand}
\end{document}
