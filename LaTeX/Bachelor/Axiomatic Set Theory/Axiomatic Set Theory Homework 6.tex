\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Lemma]{lemhand}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\On}{On}
\DeclareMathOperator{\cf}{cf}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

\title{Axiomatic Set Theory Homework 6}
\author{Mike Daas, Wouter Rienks}
%% Document
\begin{document}
\maketitle
\begin{opghand}[1]
Prove the following:
\begin{enumerate}[A]
\item The elements $b \land -b$ are all equal and denoted by $0$ (zero).
\begin{oplossing}
Note that given $b, c \in B$, by applying (B5), (B1) and (B5) again we have
\[
b \land -b = (c \land -c)  \lor (b \land -b) = (b \land -b) \lor (c \land -c) = c \land -c.
\]
This shows all the elements $b \land -b$ are equal.
\end{oplossing}
\item The elements $b \lor -b$ are all equal and denoted by $1$ (unity).
\begin{oplossing}
Similarly, given $b, c \in B$, by applying (B5), (B1) and (B5) again we have
\[
b \lor -b = (c \lor -c)  \land (b \lor -b) = (b \lor -b) \land (c \lor -c) = c \lor -c.
\]
This shows all the elements $b \lor -b$ are equal.
\end{oplossing}
\item Any nonempty subset $\mathcal{F}$ of subsets of a set $X$ that is closed under union, intersection and complement with respect is a Boolean algebra under the operations meet = intersection, join = union, complement = complement in $X$. Such an $\mathcal{F}$ is called a field of sets.
\begin{oplossing}
First note that since $\mathcal{F}$ is closed under union, intersection and complement, for any sets $B, C \in \mathcal{F}$ we have $B \land C = B \cap C \in \mathcal F$, $B \lor C = B \cup C \in \mathcal{F}$, and $-B = X \setminus B \in \mathcal F$. We show $\mathcal{F}$ satisfies all the axioms (B1) - (B5). 
\begin{enumerate}[(B1)]
\item Clearly $B \cup A = A \cup B$ and $B \cap A = A \cap B$.
\item We know for sets that $A \cup (B \cup C) = (A \cup B) \cup C$ and $(A \cap B) \cap C = A \cap (B \cap C)$.
\item Note that $(B \cap C) \subseteq C$, thus $(B \cap C) \cup C = C$. Similarly $C \subseteq B \cup C$, thus $(B \cup C) \cap C = C$. 
\item Note that
\begin{align*}
x \in B \cap (C \cup D) &\iff x \in B \text{ and } (x \in C \text { or } x \in D) \\
&\iff x \in B \cap C \text{ or } x \in B \cap D \iff x \in (B \cap C) \cup (B \cap D).
\end{align*}
Thus $B \cap (C \cup D) =  (B \cap C) \cup (B \cap D)$. Similarly, the other identitiy holds since
\begin{align*}
x \in B \cup (C \cap D) &\iff x \in B \text{ or } (x \in C \text { and } x \in D) \\
&\iff x \in B \cup C \text{ and } x \in B \cup D \iff x \in (B \cup C) \cap (B \cup D).
\end{align*}
\item Note that $B \cap -B = B \cap (X \setminus B) = \emptyset$, thus $(B \cap -B) \cup C = \emptyset \cup C = C$. Similarly, $B \cup -B = B \cup (X \setminus B) = X$, thus $(B \cap -B) \cap C = X \cap C = C$. 
\end{enumerate}
Thus $\mathcal{F}$ is indeed a Boolean algebra.
\end{oplossing}
\item Let $X$ be a topological space. Let $\mathcal{C}$ denote the set of all clopen subsets of $X$. Show that $\mathcal{C}$ is a field of sets.
\begin{oplossing}
It suffices to show that if $A, B$ are clopen, then so are $A \cup B$ and $A \cap B$, and $X \setminus A$. 
\begin{itemize}
\item Since $A, B$ are open, so is $A \cup B$. Since $A, B$ are closed, so is $A \cup B$. Thus $A \cup B$ is clopen.
\item Since $A, B$ are open, so is $A \cap B$. Since $A, B$ are closed, so is $A \cap B$. Thus $A \cap B$ is clopen.
\item Since $A$ is open, $X \setminus A$ is closed, and since $A$ is closed, $X \setminus A$ is open. Thus $X \setminus A$ is clopen.
\end{itemize}
\end{oplossing}
\end{enumerate}
\end{opghand}
\begin{opghand}[2]
Prove the following:
\begin{enumerate}[A]
\item $I \subseteq B$ is an ideal if and only if (a) and (b)' hold, where
\[
(b)' \qquad [b \in I \text{ and } c \leq b] \rightarrow c \in I.
\]
\begin{oplossing}
It suffices to show (b) holds iff (b)' holds. If (b) holds, and $c \leq b$, then $b \land c = c$. Since (b) holds, $b \land c \in I$, thus $c \in I$ and (b)' holds.

For the converse, if (b)' holds and $c \in B$, then $(b \land c) \land b =  (b \land b) \land c$ by (B1) and (B2). We see that
\begin{align*}
b \land b &= (b \land -b) \lor (b \land b) &(B5) \\
&= b \lor (-b \land b) &(B4) \\
&= b \lor (b \land -b) &(B1) \\
&= b. &(B5)
\end{align*}
Thus $(b \land c) \land b = (b \land b) \land c = b \land c$. Thus $b \land c \leq b$ and $b \land c \in I$ by (b)'. Thus (b) holds.
\end{oplossing}
\item $0 \in I$ for every ideal $I$. If $1 \in I$, then $I = B$. 
\begin{oplossing}
Since $I \neq \emptyset$, pick some $b \in I$. Then $0 \land b = (b \land -b) \land b = (b \land b) \land -b = b \land -b = 0$ since we showed in A that $b \land b = b$. Thus $0 \leq b$, and by (b)' we have $0 \in I$.

Now suppose $1 \in I$. Then for any $c \in B$, we have $1 \land c = (c \lor -c) \land c = c$ by (B5), thus $c \leq 1$ and $c \in I$ by (b)'. Thus $B \subseteq I$, since $I \subseteq B$ we have $I = B$. 
\end{oplossing}
\item If $b \in B$, then $\{c \in B \mid c \leq b\}$ is an ideal. Such an ideal is called an \emph{principal} ideal.
\begin{oplossing}
Fix some $b \in B$, and let $I = \{c \in B \mid c \leq b\}$.  Then (a) holds, since for $a, c \in I$ we have $(a \lor c) \land b = (a \land b) \lor (c \land b) = a \lor c$, since $a, c \leq b$. Thus $a \lor c \leq b$ and $a \lor c \in I$. 

For (b)', Note that if $a \leq b$ and $b \leq c$, then $a \leq c$, since
\[
a \land c = (a \land b) \land c = a \land (b \land c) = a \land b = a.
\]
Thus (b)' holds, since if $c \in I$ and $a \in B$ satisfies $a \leq c$, then $a \leq c \leq b$, thus by the above $a \leq b$ and therefore $a \in I$.
\end{oplossing}
\item Let $X$ be an infinite set. Let $I$ be the set of all finite subsets of $X$. $I$ is a nonprincipal ideal in the field of sets $\mathcal{P}(X)$. 
\begin{oplossing}
Clearly $I$ is an ideal, since (a) holds because if $A, B \in I$, then $A \cup B$ is finite so $A \cup B \in I$. Furthermore, given $B \in I$ and $C \subseteq X$, since $B \cap C \subseteq B$ the set $B \cap C$ is finite, so $B \cap C \in I$ and (b) holds. 

Note that $A \leq B \iff A \cap B = A \iff A \subseteq B$. Now suppose $I$ were a principal ideal for some upper bound $B$. Then for any $x \in X$ the set $\{x\}$ is finite, so $\{x\} \subseteq B$. Thus $X \subseteq B$. But then $X \in I$, yet $X$ is infinite. Contradiction, therefore $I$ is no principial ideal. 
\end{oplossing}
\item Show that if $\mu$ is a measure on $B$, then $\{b \in B \mid \mu(b) = 0\}$ is an ideal in $B$. 
\begin{oplossing}
Let $I = \{b \in B \mid \mu(B) = 0\}$. Now if $a, b \in I$ then $a \land (b \land -a) = (a \land -a) \land b = 0 \land b = 0$. Thus since $a \lor (b \land -a) = (a \lor b) \land (a \lor -a) = (a \lor b) \land 1 = (a \lor b)$, we have 
\[
\mu(a \lor b) = \mu(a) + \mu(b \land -a) = 0 + \mu(b \land -a). 
\]
Now $(b \land -a) \lor (b \land a) = b \land (a \lor -a) = b \land 1 = b$, and $(b \land -a) \land (b \land a) = (b \land b) \land (a \land -a) = (b \land b) \land 0 = 0$. Thus
\begin{equation}
0 = \mu(b) = \mu(b \land -a) + \mu(b \land a),
\end{equation}
and since $\mu(b \land a) \geq 0$ we have $\mu(b \land -a) = 0$. Thus $\mu(a \lor b) = 0$. Therefore (a) holds.

Now if $a \leq b$, then since $a \land b = a$ we see from (1) that
\[
\mu(b) \geq \mu(a \land b) = \mu(a).
\]
Thus if $b \in I$ and $a \leq b$ then $\mu(a) = 0$, so $a \in I$ and (b)' holds. Thus $I$ is an ideal.
\end{oplossing}
\item Let $B$ be a boolean algebra. Show that, if $I_t, t \in T$ are ideals in $B$, so too is
\[
\bigcap_{t \in T} I_t.
\]
Deduce that if $X \subseteq B$, there is a unique smallest ideal containing $X$; it is called the ideal generated by $X$.
\begin{oplossing}
Let $I = \bigcap_{t \in T} I_t$. Given $b, c \in I$, we have $b, c \in I_t$ for all $t \in T$. Thus since $I_t$ is an ideal for all $t \in T$, we have $b \lor c \in I_t$ for all $t \in T$. Thus $b \lor c \in I$, and (a) holds. 

Similarly, given $b \in I$ and $c \in B$, we have $b \in I_t$ for all $t \in T$. Thus since $I_t$ is an ideal for $t \in T$, we have $b \land c \in I_t$. Thus $b \land c \in I$, and (b) holds.

Now for any set $X \subseteq B$, let $\mathcal{I} := \{I \text{ is an ideal in $B$ and } X \subseteq I\}$, and let $I(X) = \bigcap \mathcal{I}$. By the above $I(X)$ is an ideal, and it is by definition contained in all ideals containing $X$, thus the smallest.
\end{oplossing}
\end{enumerate}
\end{opghand}
\end{document}
