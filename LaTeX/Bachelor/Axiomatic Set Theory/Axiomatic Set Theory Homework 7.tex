\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Lemma]{lemhand}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\On}{On}
\DeclareMathOperator{\cf}{cf}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

\title{Axiomatic Set Theory Homework 7}
\author{Mike Daas, Wouter Rienks}
%% Document
\begin{document}
\maketitle
\begin{opghand}[1]
Prove the following:
\begin{enumerate}[1]
\item Prove that, for any ordinal $\alpha$, we have $L_{\alpha + 1} \neq L_\alpha$, (i.e. $L_{\alpha + 1}$ contains at least one element not in $L_\alpha$). 
\begin{oplossing}
Since $L_\alpha$ is a set, by the axiom of foundation we can't have $L_\alpha \in L_\alpha$, (consider $x = \{L_\alpha\}$). But $L_\alpha \in L_{\alpha + 1}$, since $L_\alpha = \{x \mid (x = x)^{L_\alpha}\}$ thus $L_\alpha$ is defineable over $L_\alpha$.
\end{oplossing}
\item Prove that for finite $\alpha$, $L_\alpha =V_\alpha$. 
\begin{oplossing}
We give a proof with induction on $\alpha$. If $\alpha = 0$ then $V_\alpha = \emptyset = L_\alpha$. Now suppose $L_\alpha = V_\alpha$, we show $L_{\alpha + 1} = V_{\alpha + 1} = \mathcal{P}(V_\alpha)$. By definition $L_{\alpha + 1} \subseteq V_{\alpha + 1}$. Now for any $x \subseteq V_{\alpha}$, $x$ is finite since $\alpha$ is finite. Let $x = \{x_1, \dots, x_n\}$ with $x_i \in V_\alpha$. Then the formula 
\[
\varphi^{L_\alpha}(y) = (u \in y) \leftrightarrow (u = x_1) \lor \dots \lor (u = x_n)
\]
defines $x$ over $L_\alpha$, which shows $x \in L_{\alpha + 1}$. Thus $V_{\alpha + 1} = L_{\alpha + 1}$ as required.
\end{oplossing}
\item What is the smallest ordinal $\alpha$ such that $L_\alpha \neq V_\alpha$. For this $\alpha$, does $V_\alpha$ have higher cardinality then $L_\alpha$. 
\begin{oplossing}
Clearly by the previous we have $V_\omega = \bigcup_{\alpha < \omega} V_\alpha = \bigcup_{\alpha < \omega} L_\alpha = L_\omega$. Now $|\mathcal{P}(V_{\omega + 1})| > |\mathcal{P}(V_\omega)|$, but $|L_{\omega + 1}| = |L_\omega|$ since there are at most countably many formulas defining elements over $L_\omega$ (since the choice of constants is countable as well, as we must pick our constants from $L_\omega$), and every element in $L_{\omega + 1}$ must be defined by some such formula. 
\end{oplossing}
\item Show that, for any infinite set $A$, the set of all finite sequences of elements of $A$ has the same cardinality as $A$. 
\begin{oplossing}
Denote $\mathcal{S}(A)$ for the set of all finite sequences of elements of $A$, and $A^k$ for the set of all sequences of length $k$ of elements of $A$. Clearly $\mathcal{S}(A) = \sqcup A^k$. Thus $|\mathcal{S}(A)| = \sum_k |A^k| = \sum_k |A| = \omega \cdot |A| = |A|$, using some  ordinal arithmetic theorems.
\end{oplossing}
\item Show, therefore, that for any infinite set $A$ of cardinality $|A| = \kappa \geq \aleph_0$, the set of all formulas in LAST with constants in $A$ has cardinality $\kappa$. 
\begin{oplossing}
Since $A$ is not the universe, we can find a countable set $B$ such that $A \cap B = \emptyset$. Clearly $|A + B| = |A|$. We can represent any formula as some tuple in $(A + B)^k$ for some $k \in \omega$, by reading the formula from left to right, mapping all symbols to some corresponding element in $B$ (note that last contains countably many symbols), and all constants to their corresponding element in $A$. Clearly this is injective, thus (by the previous exercise) there are at most $|A + B| = |A|$ such formulas. Since for any $a \in A$, the formula $\varphi_a: a = a$ is a formula, thus there are at least $|A + B|$ formulas. We conclude there are exactly $|A + B| = |A|$ formulas.
\end{oplossing}
\item Show that, if $A$ is transitive, then $A \subseteq Def(A)$. 
\begin{oplossing}
Note that any element $a \in A$ is defined over $A$ by the formula $\varphi_a(x): x = a$. If $A$ is transitive, then also any element $a \in A$ satisfies $a \subseteq A$. Thus any element $a \in A$ is contained in $Def(A) = \{x \subseteq A \mid x \text{ is defineable over } A\}$, therefore $A \subseteq Def(A)$. 
\end{oplossing}
\item Show that, if $A$ is transitive of infinite cardinality $\kappa \geq \alpha_0$, then $Def(A)$ has the same cardinality $\kappa$. 
\begin{oplossing}
By (6) we have $A \subseteq Def(A)$ so $\kappa \leq |Def(A)|$. On the other hand, since by (5) there are $\kappa$ formulas and any element in $Def(A)$ corresponds to at least one formula (and one formula can define at most one element), we have $|Def(A)| \leq \kappa$. We conclude $|Def(A)| = \kappa$. 
\end{oplossing}
\item Show that, for every infinite ordinal $\alpha$, $L_{\alpha + 1}$ has the same cardinal as $L_\alpha$. 
\begin{oplossing}
Note that $L_\alpha$ is transitive, and for infinite $\alpha$ we have $|L_\alpha| \geq \aleph_0$. By the previous, we have $|Def(L_\alpha)| = |L_\alpha|$ for infinite $\alpha$, so that $|L_{\alpha + 1}| = |L_\alpha|$. 
\end{oplossing}
\item Show that for each infinite ordinal $|\alpha|$, we have $|L_\alpha| \leq |\alpha|$. 
\begin{oplossing}
We give a proof by transfinite induction on ordinals (starting at $\omega$). Note that $|L_\omega|$ is countable, thus $|L_\omega| = |\omega|$. Now if it holds for $\alpha$ then it holds for $\alpha + 1$ as well, since by the previous we have $|L_{\alpha + 1}| = |L_\alpha| \leq |\alpha| = |\alpha + 1|$ for infinite $\alpha$. Finally, if $\alpha$ is a limit ordinal and it holds for all $\eta < \alpha$, we see that
\[
|L_\alpha| = | \bigcup_{\eta < \alpha} L_\eta| \leq \sum_{\eta < \alpha} |\eta| \leq |\alpha| \cdot |\alpha| = |\alpha|,
\]
as required.
\end{oplossing}
\item Show that for each infinite ordinal $\alpha$, we have $|L_\alpha| = |\alpha|$. 
\begin{oplossing}
We give a proof by transfinite induction on ordinals (starting at $\omega$), by the previous it suffices to show $|L_\alpha| \geq |\alpha|$. Note that $|L_\omega|$ is countable, thus $|L_\omega| = |\omega|$. Now if it holds for $\alpha$ then it holds for $\alpha + 1$ as well, since by the previous we have $|L_{\alpha + 1}| = |L_\alpha| \geq |\alpha| = |\alpha + 1|$ for infinite $\alpha$. Finally, if $\alpha$ is a limit ordinal and it holds for all $\eta < \alpha$, we see that each $L_{\alpha + 1}$ contains some $x_\alpha \not \in L_\alpha$. Thus $\bigcup_{\eta < \alpha} \{x_\eta\} \subseteq \bigcup_{\eta < \alpha} L_\eta$, and since all $x_\eta$ are distinct
\[
|L_\alpha| = |\bigcup_{\eta < \alpha} L_\eta | \geq \sum_{\eta < \alpha} |x_\eta| = |\alpha| \cdot 1 = \alpha,
\]
as required.
\end{oplossing}
\end{enumerate}
\end{opghand}
\begin{opghand}[2]
Show that the following notions are $\Delta_0$ (hence absolute):
\begin{enumerate}[1.]
\item $x = (y, z)$.
\begin{oplossing}
First note that $a = b$ is $\Delta_0$, since
\[
a = b: (\forall x \in a)(x \in b) \land (\forall x \in b)(x \in a). 
\]
Note that $v = \{y\}$ and $v = \{y, z\}$ are $\Delta_0$, since they can be written as
\[
v = \{y\}: \forall(x \in v)(x = y) \land (y \in v).
\]
\[
v = \{y, z\}: \forall(x \in v)((x = y) \lor (x = z)) \land (y \in v) \land (z \in v).
\]
Now finally $x = (y, z)$ is $\Delta_0$ since
\[
x = (y, z): \forall(v \in x)((v = \{y\}) \lor (v = \{y, z\}) \land (\{y\} \in x) \land (\{y, z\} \in x). 
\]
\end{oplossing}
\item $x$ is a binary relation.
\begin{oplossing}
First note that $\bigcup x$ can be described in $\Delta_0$, since 
\[
A = \bigcup x: \forall(y \in A)(\exists z \in x)(y \in z) \land (\forall z \in x)(\forall y \in z)(y \in A). 
\]
Now let $A_x := \bigcup \bigcup x$, then $A_x$ can also be described in $\Delta_0$. And any pair $(a, b) \in x$ satisfies $a \in A_x, b \in A_x$, so that we can specify 
\[
x \text{ is a binary relation }: \forall (z \in x)(\exists(a \in A_x)(\exists(b \in A_x)(z = (a, b))))
\]
which shows this is $\Delta_0$.
\end{oplossing}
\item $f$ is a function.
\begin{oplossing}
Note that
\begin{align*}
f \text{ is a function }: \ &(f \text{ is a binary relation})\land \\
&\lnot(\exists(a, b, c \in A_f)(\lnot(b = c) \land ((a, b) \in f) \land (a, c) \in f)),
\end{align*}
since the domain is unspecified anyway. Thus this is $\Delta_0$. 
\end{oplossing}
\item $x = dom(f)$
\begin{oplossing}
We can write this as
\begin{align*}
x = dom(f): (f \text{ is a function }) &\land (\forall z \in x)(\exists a \in A_f)((z, a) \in f)  \\ &\land (\forall(a, b \in A_f)((a, b) \in f \to a \in x)). 
\end{align*}
Thus this is $\Delta_0$. 
\end{oplossing}
\item $y = range(f)$.
\begin{oplossing}
We can write this as
\begin{align*}
y = range(f): (f \text{ is a function }) &\land (\forall z \in y)(\exists a \in A_f)((a, z) \in f)  \\ &\land (\forall(a, b \in A_f)((a, b) \in f \to b \in y)). 
\end{align*}
Thus this is $\Delta_0$. 
\end{oplossing}
\item $x$ is transitive
\begin{oplossing}
Clearly this can be written as
\[
\forall(a \in x)(\forall b \in a)(b \in x),
\]
thus is $\Delta_0$. 
\end{oplossing}
\newpage
\item $x$ is totally ordered by $\in$. 
\begin{oplossing}
We can write this as
\begin{align*}
&\forall(a, b, c \in x)((a \in b) \land (b \in c) \to (a \in c)) \\
&\land \forall(a, b \in x)((a \in b) \lor (b \in a) \lor (a = b)),
\end{align*}
(note that we don't need antisymmetry, since we can't have $a \in b$ and $b \in a$ by the foundation axiom). This shows it is $\Delta_0$. 
\end{oplossing}
\item $x$ is an ordinal.
\begin{oplossing}
This follows from the previous two, since by those the formula $(x \text{ is transitive}) \land (x \text{ is totally ordered by } \in)$ is $\Delta_0$, by Lemma 3.1.1 this is equivalent to $x$ is an ordinal.
\end{oplossing}
\item $x$ is a successor ordinal
\begin{oplossing}
We can write this as
\[
(x \text{ is an ordinal }) \land (\exists y \in x)(x = y\cup \{y\}). 
\]
This shows it is $\Delta_0$. 
\end{oplossing}
\item $x$ is a limit ordinal.
\begin{oplossing}
We can write this as
\[
(x \text{ is an ordinal }) \land \lnot(x \text{ is a successor ordinal}),
\]
which shows it is $\Delta_0$. 
\end{oplossing}
\end{enumerate}
\end{opghand}
\end{document}
