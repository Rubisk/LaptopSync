\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\On}{On}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

\title{Axiomatic Set Theory Homework 3}
\author{Mike Daas, Wouter Rienks}
%% Document
\begin{document}
\maketitle
\begin{opghand}[2.8.1]
\end{opghand}
\begin{enumerate}[A.]
\item Prove that if $A$ is a class of sets such that, for every $x$
\[
(\forall y \in x)(y \in A) \rightarrow (x \in A)
\]
(i. e. $x \subseteq A \rightarrow x \in A$), then $A = V$. (This is the Principle of Proof by $\in$-Induction).
\begin{oplossing}
Suppose there exists a set $x \not \in A$. Then there must exist some $x_1 \in x$ with $x_1 \not \in A$, since otherwise $x \in A$. Now given $x_n \not \in A$, by the same argument there exists $x_{n + 1} \in x_n$ with $x_{n + 1} \not \in A$. Now consider the set $X = \{x_n \mid n \in \NN\}$. This is a set by the axiom of replacement (with the map $F: \NN \to V: n \mapsto x_n$, this map can be constructed using recursion on ordinals). Then there exists no $a \in X$ such that $a \cap X = \emptyset$, since there would be $m \in \NN$ such that $a = x_m$, but then $x_{m + 1} \in a \cap X$. We thus have a contradiction with the axiom of foundation, hence no such $x$ can exist.
\end{oplossing}
\item Show that whenever $h: V \times V \to V$, there exists a unique $f: V \to V$ such that, for every set $x$,
\[
f(x) = h(x, f''(x)).
\]
(Here $f''(x) = \{f(y) \mid y \in x\}$)
(This is the principle of $\in$-Recursion.)
\begin{oplossing}
Let $s: V \to \On$ be the class function mapping any set to the least ordinal $\alpha$ such that $x \in V_\alpha$, this exists since the ordinals are well-ordered and $V = \bigcup V_\alpha$. Define $f_0 = \emptyset$, i.e. the function with empty domain. Given an ordinal $\alpha$, suppose there exists $f_\beta: V_\beta \to V$ for all $\beta < \alpha$. Define, for all $x \in V_\alpha$, 
\[
f_\alpha(x) := h\(x, \{f_{s(y)}(y) \mid y \in x\}\).
\]
We can define such function since for any $y \in x$ we have $s(y) < \alpha$ (using recursion on ordinals). Then let $f = \bigcup f_\alpha$. We first show that $f: V \to V$ is a function. Since if $\alpha < \beta$ we have $f_\alpha(x) = f_\beta(x)$ for all $x \in V_\beta$, the image of $x$ is unique under $f$. Since any $x$ is contained in at least one $V_\alpha$, the domain of $f$ is all of $V$. Hence $f$ is a function.

Now cleary $f(x) = h(x, f''(x))$, since $f_{s(y)}(y) = f(y)$ by definition of $f$. Finally we show $f$ is unique, so take any other function $g$ satisfying $g(x) = h(x, g''(x))$. We show $g \rvert_{V_\alpha} = f \rvert_{V_\alpha}$ for all $\alpha \in \On$, then we may conclude $f = g$. Clearly $f \rvert_{\emptyset} = g\rvert_{\emptyset}$ since the domain is empty. Now suppose $\alpha$ is some ordinal such that $f \rvert_{V_\beta} = g \rvert_{V_\beta}$ for all $\beta < \alpha$. Given $x \in V_\alpha$, for any $y \in x$ we have $g(y) = f(y)$ since there exists some $\gamma < \alpha$ with $y \in V_\gamma$. Hence $f''(x) = g''(x)$, and therefore $f(x) = h(x, f''(x)) = h(x, g''(x)) = g(x)$.
\end{oplossing}
\item Define the function $\rho: V \rightarrow \On$ by the $\in$-recursion
\[
\rho(x) = \bigcup\{\rho(y) + 1 \mid y \in x\}.
\]
Show that for any set $x$, $\rho(x)$ is the least $\gamma$ such that $x \in V_{\gamma + 1}$. ($\rho$ is called the \emph{rank} function, and $\rho(x)$ is called the \emph{rank} of $x$.)
\begin{oplossing}
Let $\gamma$ be an ordinal, and let $x$ be a set such that $\gamma$ is the least ordinal such that $x \in V_{\gamma + 1}$. We show by induction on ordinals that $\rho(x) = \gamma$. 

If $\gamma = 0$ then $x = \emptyset$, since $\emptyset$ is the only set in $V_1$ but not in $V_0$. And $\rho(\emptyset) = \emptyset = \gamma$, so in this case it holds.

For any $y \in x$ we must have $y \in V_\gamma = V_{\eta + 1}$, hence $\rho(y) \leq \eta$ by induction. We may therefore conclude $\rho(x) \leq \eta + 1 = \gamma$. If there were no $y \in x$ with $y \in V_\gamma$, then we would have $x \in V_\gamma$, since any $y \in x$ would be contained in some $V_\beta$ with $\beta < \gamma$. But we assumed $x \not \in V_\gamma$, hence there exists $y \in x$ with $y \in V_\gamma$, for this $y$ we have by induction $\rho(y) = \eta$, which implies $\rho(x) \geq \rho(y) + 1= \eta + 1 = \gamma$. Therefore $\rho(x) = \gamma$.

Finally suppose $\gamma$ is a limit ordinal. For any $x \in V_{\gamma + 1}$ we must have $y \in V_\gamma$. Since $V_\gamma = \bigcup_{\beta < \gamma} V_\beta$, for any $y \in V_\gamma$ there exists $\beta < \gamma$ such that $y \in V_\beta$, since $\gamma$ was a limit ordinal we must then also have $\beta + 1 < \gamma$ and also $y \in V_{\beta + 1}$. Hence for any $y \in V_\gamma$, the least ordinal $\alpha$ such that $y \in V_{\alpha + 1}$ satisfies $\alpha + 1 < \gamma$, so by induction we have $\rho(y) + 1 \leq \gamma$, and therefore $\rho(x) \leq \gamma$. 

Finally, for any $y \in x$ we have per definition of $\rho$ that $\rho(y) + 1 \leq \rho(x)$, so for any $y \in x$ we must have by induction that $y \in V_{\rho(x)}$. We can thus conclude that $x \in V_{\rho(x) + 1} = \mathcal{P}(V_{\rho(x)})$, hence $\gamma \leq \rho(x)$. We conclude $\gamma = \rho(x)$. 
\end{oplossing}
\end{enumerate}
\begin{opghand}
Prove Lemma 3.3.1 (page 70), by defining an order-isomorphism between the ordinals on the two sides of the equation.
\end{opghand}
\begin{oplossing}
First we write out the left hand side, step by step. We have that, as a set,
\[
\beta + \gamma = (\beta \times \{ 0 \}) \cup (\gamma \times \{ 1 \}),
\]
equipped with the order
\[
((\mu_1,i_1) < (\mu_2,i_2)) \leftrightarrow ((i_1 < i_2) \lor ((i_1 = i_2) \land (\mu_1 < \mu_2))).
\]
Therefore we have that, as a set,
\[
\alpha \cdot (\beta + \gamma) = \alpha \times ((\beta \times \{ 0 \}) \cup (\gamma \times \{ 1 \})),
\]
equipped with the order
\begin{gather*}
((a_1,(\mu_1,i_1)) < (a_2,(\mu_2,i_2))) \leftrightarrow \\ (((i_1 < i_2) \lor ((i_1 = i_2) \land (\mu_1 < \mu_2))) \lor (((\mu_1,i_1) = (\mu_2,i_2)) \land (a_1 < a_2))).
\end{gather*}
Now we do the same for the right hand side, noting that as sets, we simply have that
\[
\alpha \cdot \beta = \alpha \times \beta \quad \text{and} \quad \alpha \cdot \gamma = \alpha \times \gamma,
\]
both ordered according to
\[
((a_1,\mu_1) < (a_2,\mu_2)) \leftrightarrow ((\mu_1 < \mu_2) \lor ((\mu_1 = \mu_2) \land (a_1 < a_2))),
\]
where $\mu_1,\mu_2 \in \beta$ in the former case, and $\mu_1,\mu_2 \in \gamma$ in the latter. Now we see that, as a set,
\[
\alpha\cdot\beta+\alpha\cdot\gamma = ((\alpha \times \beta)\times \{0\})\cup((\alpha\times\gamma)\times\{1\}),
\]
equipped with the ordering
\begin{gather*}
(((a_1,\mu_1),i_1) < ((a_2,\mu_2),i_2)) \leftrightarrow \\ ((i_1 < i_2) \lor ((i_1 = i_2) \land ((\mu_1 < \mu_2) \lor ((\mu_1 = \mu_2) \land (a_1 < a_2))))).
\end{gather*}
We now define a function $f$ that will serve as our order isomorphism between these two sets;
\begin{gather*}
f : \alpha \times ((\beta \times \{ 0 \}) \cup (\gamma \times \{ 1 \})) \to  ((\alpha \times \beta)\times \{0\})\cup((\alpha\times\gamma)\times\{1\}) : \\ (a,(\mu,i)) \mapsto ((a,\mu),i).
\end{gather*}
It is easy to see that $f$ is a bijection between these two sets, so it suffices to show that $f$ preserves the order as well. To show this, we prove that
\[
((a_1,(\mu_1,i_1)) < (a_2,(\mu_2,i_2))) \leftrightarrow (((a_1,\mu_1),i_1) < ((a_2,\mu_2),i_2)).
\]
To see this, we will split cases. If $i_1 > i_2$, then both sides of the claim are false. If $i_1 < i_2$, then both sides are true. Henceforth we will assume that $i_1 = i_2$, so that it suffices to show that 
\[
((a_1,(\mu_1,i)) < (a_2,(\mu_2,i))) \leftrightarrow (((a_1,\mu_1),i) < ((a_2,\mu_2),i)).
\]
If $\mu_1 > \mu_2$, then both sides of the claim are false. If $\mu_1 < \mu_2$, then both sides are true. Hence we may assume that $\mu_1 = \mu_2$, so that it suffices to prove that
\[
((a_1,(\mu,i)) < (a_2,(\mu,i))) \leftrightarrow (((a_1,\mu),i) < ((a_2,\mu),i)).
\]
But both sides hold precisely when $a_1 < a_2$, which proves that $f$ is indeed an order isomorphism. Formally, this only shows that the two sets to which the ordinals $\alpha\cdot(\beta+\gamma)$ and $\alpha\cdot\beta+\alpha\cdot\gamma$ are defined to be order-isomorphic, are order-isomorphic. But this immediately implies that the two ordinals are equal, since any well-ordered set is order-isomorphic to a unique ordinal. This completes the proof.
\end{oplossing}
\begin{opghand}
Let $\alpha, \beta, \gamma$ be ordinals. Show that $\alpha^\beta \cdot \alpha^\gamma = \alpha^{\beta + \gamma}$.
\end{opghand}
\begin{oplossing}
It's clear that for any ordinal $\mu$, the set $\mu \cdot 1 :=  \mu \times \{0\}$ with the anti-lexicographic ordering is order-isomorphic to $\mu$, with the isomorphism $(m, 0) \mapsto m$. And similarly, $\mu + 0$ which is defined as the union of the sets $\{(m, 0) \mid m \in \mu\}$ and $\{(n, 1) \mid n \in \emptyset\} = \emptyset$ is order-isormorphic to $\mu$ as wel, with the isomorphism $(m, 0) \mapsto m$. We thus have for any ordinal $\mu$ that $\mu \cdot 1 = \mu + 0 = \mu$.

The case $\alpha = 0$ is trivial, so we assume $\alpha \neq 0$. We will now provide a proof by induction on $\gamma$. If $\gamma = 0$ then $\alpha^\gamma = 1$ by definition. Hence $\alpha^\beta \cdot \alpha^\gamma = \alpha^\beta \cdot 1 = \alpha^\beta = \alpha^{\beta + 0} = \alpha^{\beta + \gamma}$ as desired. Now suppose it holds for all ordinals $\mu < \gamma$. 

If $\gamma = \mu + 1$ for some ordinal $\mu$, then by definition we have $\alpha^\gamma = \alpha^\mu \cdot \alpha$. Similarly, by associativity of addition we have 
\[
\alpha^{\beta + \gamma} = \alpha^{\beta + (\mu + 1)} = \alpha^{(\beta + \mu) + 1} = \alpha^{\beta + \mu}\cdot \alpha.
\]
Using associativity of multiplication, we have that $\alpha^\beta\cdot \alpha^{\mu + 1} = \alpha^\beta\cdot (\alpha^{\mu} \cdot \alpha) = (\alpha^\beta \cdot \alpha^\mu) \cdot \alpha$. Since by induction we may assume $\alpha^{\beta + \mu} = \alpha^\beta \cdot \alpha^\mu$, we thus have that
\[
\alpha^\beta \cdot \alpha^\gamma = (\alpha^\beta \cdot \alpha^\mu)\cdot \alpha = \alpha^{\beta + \mu} \cdot \alpha = \alpha^{\beta + \gamma},
\]
as desired.

Finally suppose $\gamma$ is a limit ordinal. In this case we have that $\alpha^\beta \cdot \alpha^\gamma = \alpha^\beta \cdot \lim_{\mu < \gamma} \alpha^\mu$. Since $\gamma$ is a limit ordinal, $\beta + \gamma$ must be a limit ordinal as well. To see this, note that if $\beta + \gamma = \mu + 1$, we could write $\gamma = \mu + 1 - \beta$. Then by associativity of addition we have that 
\[
\beta + ((\mu - \beta) + 1) = (\beta + (\mu - \beta)) + 1 = \mu + 1. 
\]
Since addition is strictly increasing in the second argument, and we just showed that \linebreak $\beta + ((\mu - \beta) + 1) = \mu + 1 =  \beta + \gamma$, we therefore must have that $\gamma = \mu - \beta + 1$, but we assumed $\gamma$ was a limit ordinal. We may thus conclude $\beta + \gamma$ is a limit ordinal. Therefore, $\alpha^{\beta + \gamma} = \lim_{\mu < \beta + \gamma} \alpha^\mu$.

We first show that $\alpha^\beta \cdot \lim_{\mu < \gamma}\alpha^\mu = \lim_{\mu < \gamma} \alpha^\beta \cdot \alpha^\mu$. Since $\alpha \neq 0$, the sequence $\mu \mapsto \alpha^\mu$ is strictly increasing, hence $\lim_{\mu < \gamma}\alpha^\mu$ exists. Since the map $\eta \mapsto \alpha^\beta \cdot \eta$ is also strictly increasing (since multiplication is strictly increasing in the second argument and $\alpha^\beta \neq 0$, the limit $\lim_{\mu < \gamma} \alpha^\beta \cdot \alpha^\mu$ also exists.

Note that for $\mu < \gamma$ we have by definition of the limit that $\alpha^\mu \leq \lim_{\mu' < \gamma} \alpha^{\mu'}$, hence by monotonicity of multiplication we see $\alpha^\beta \cdot \alpha^\mu \leq \alpha^\beta \cdot \lim_{\mu' < \gamma} \alpha^{\mu'}$. It thus suffices to show that for any $\eta < \alpha^\beta \cdot \lim_{\mu' < \gamma} \alpha^{\mu'}$ there exists $\xi < \gamma$ such that for $\xi < \mu \leq \gamma$ we have $\alpha^\beta \cdot \alpha^\mu > \eta$. Since $\eta < \alpha^\beta \cdot \lim_{\mu' < \gamma} \alpha^{\mu'}$, we have $\eta + 1 \leq \alpha^\beta \cdot \lim_{\mu' < \gamma} \alpha^{\mu'}$, therefore there exists an order-preserving injection
\[
F: (\eta + 1) \to \alpha^\beta \times \(\lim_{\mu' < \gamma} \alpha^{\mu'}\): x \mapsto (x_1, x_2)
\]
where $(\alpha^\beta) \times \(\lim_{\mu' < \gamma} \alpha^{\mu'}\)$ is ordered with the anti-lexicographic ordering. Let $F(\eta) = (\eta_1, \eta_2)$. We then must have that $\eta_2 < \lim_{\mu' < \gamma} \alpha^{\mu'}$, and $\eta_1 < \alpha^\beta$. Hence there exists $\xi < \gamma$ such that for all $\xi \leq \mu < \gamma$ we have the inequalities $\eta_2 < \alpha^{\mu} \leq \lim_{\mu' < \gamma} \alpha^{\mu'}$ by definiton of the limit. Consider for fixed $\mu$ with $\xi \leq \mu < \gamma$ the function
\[
G_\mu: \eta \to \alpha^\beta \times \alpha^\mu: \lambda \mapsto F(\lambda) .
\]
It's clear that $G$ is order-preserving (if we give $\alpha^\beta \times \alpha^\mu$ the anti-lexicographic ordering) since $F$ is order-preserving. To see that $G$ is well defined, note that for any $\lambda < \eta$ we have $F(\lambda) < F(\eta)$. Therefore we have $\lambda_2 < \eta_2$ or $\lambda_2 = \eta_2$ and $\lambda_1 < \eta_1$. Hence $\lambda_2 \leq \eta_2 < \alpha^\mu$, and clearly $\lambda_1 < \alpha^\beta$ by definition of $F$, so $G$ is well-defined. We may thus conclude $\eta < \alpha^\beta \cdot \alpha^\mu$ for all $\xi \leq \mu < \gamma$. Therefore we may finally conclude $\alpha^\beta \cdot \lim_{\mu < \gamma}\alpha^\mu = \lim_{\mu < \gamma} \alpha^\beta \cdot \alpha^\mu$.

Now by induction, for any $\mu < \gamma$ we have $\alpha^\beta \cdot \alpha^\mu = \alpha^{\beta + \mu}$. Hence $\lim_{\mu < \gamma} \alpha^\beta \cdot \alpha^\mu = \lim_{\mu < \gamma} \alpha^{\beta + \mu}$. It therefore remains to show that $\lim_{\mu < \gamma} \alpha^{\beta + \mu} = \lim_{\mu < \beta + \gamma} \alpha^{\mu}$. But this follows  from Lemma 3.4.3, we verify both conditions:
\begin{enumerate}[a)]
\item For $\mu < \gamma$ we have $\beta + \mu < \beta + \gamma$, and since $\beta + \gamma$ is a limit ordinal $\beta + \mu + 1 < \beta + \gamma$, and $\alpha^{\beta + \mu} < \alpha^{\beta + \mu + 1}$ by monotonicity for exponentiation. 

\item For any $\mu < \beta + \gamma$ we have $\mu - \beta < \gamma$. Since $\gamma$ is a limit ordinal we thus have $\mu - \beta + 1 < \gamma$. Recall that as shown earlier, we once again have $\beta + ((\mu - \beta) + 1) = \mu + 1$. Thus we see that $\alpha^{\beta + ((\mu - \beta) + 1)} = \alpha^{\mu + 1} > \alpha^\mu$ as desired.
\end{enumerate}
This completes the proof.
\end{oplossing}
\begin{opghand}[3.5.1]
Show that for any ordinal $\alpha$, we have $\alpha+\alpha\cdot \omega = \alpha \cdot \omega$ and $\alpha \cdot \alpha^{\omega} = \alpha^{\omega}$.
\end{opghand}
\begin{oplossing}
We will first show that $1+\omega = \omega$. By definition, we have since $1 = \{ \emptyset \}$, that $1+\omega$ is the order type of the set
\[
(1 \times \{ 0 \}) \cup (\omega \times \{ 1 \}), 
\]
equipped with the order
\[
(\emptyset,0) < (n,1) \text{ for all } n \in \omega, \text{ and } (n,1) < (m,1) \longleftrightarrow n < m \text{ for all } n,m \in \omega.
\]
Then the function $f : (1 \times \{ 0 \}) \cup (\omega \times \{ 1 \}) \to \omega$ that satisfies
\[
f(\emptyset,0) = 0 \quad \text{and} \quad f(n,1) = n+1 \text{ for all } n \in \omega,
\]
is clearly an order isomorphism. This proves the claim.

As shown earlier, for any ordinal $\alpha$, we have that $1 \cdot \alpha = \alpha = \alpha \cdot 1$. Now we are ready to tackle the first part of the problem. Recall the distributive law $\alpha \cdot (\beta+\gamma) = \alpha \cdot \beta + \alpha \cdot \gamma$. Applied to the case that $\beta = 1$ and $\gamma = \omega$, we find that
\[
\alpha+\alpha\cdot \omega = \alpha \cdot 1 + \alpha \cdot \omega =\alpha \cdot (1+\omega) = \alpha \cdot \omega,
\]
as desired. For the second part of the problem, we recall that $\alpha^0 = 1$ by definition, and so we find that, noting that $0+1=1$ since they are both 1-element sets,
\[
\alpha^1 = \alpha^{0+1} := \alpha^0 \cdot \alpha = 1 \cdot \alpha = \alpha.
\]
Now we will use Lemma 3.5.1(i) to compute that
\[
\alpha \cdot \alpha^{\omega} = \alpha^1 \cdot \alpha^{\omega} = \alpha^{1+\omega} = \alpha^{\omega},
\]
which completes the proof.
\end{oplossing}
\end{document}
