\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=0.5em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Problem.]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\renewcommand{\P}{\mathbb{P}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Markov Chains Homework 1}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}
Let $(X_n)_{n=0,1,2,\dots}$ be a discrete-time Markov chain with probability transition matrix:
\[
P = \begin{pmatrix}
0 & \frac{2}{5} & \frac{1}{5} & 0 & \frac{1}{5} & \frac{1}{5} \\
0 & 0 & 0 & 0 & 1 & 0 \\
0 & 0 & 1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 & 0 & 0 \\
0 & \frac{1}{3} & 0 & \frac{2}{3} & 0 & 0 \\
1 & 0 & 0 & 0 & 0 & 0
\end{pmatrix}
\]
\end{opghand}
\begin{enumerate}[a)]
\item Determine the communicating classes of $X_n$. Which of them are closed, and which are open?
\begin{oplossing}
Clearly $3$ only maps to itself, hence $\{3\}$ is one of the classes. We see directly that $1 \leftrightarrow 6$, $2 \leftrightarrow 5$. Since also $2 \to 4$ (through 5) and $4 \to 2$ we may also conclude $2 \leftrightarrow 5$. We conclude that the communicating classes are
\[
\{3\}, \{1, 6\}, \{2, 4, 5\}.
\]
Now $\{3\}$ and $\{2, 4, 5\}$ are closed, but $\{1, 6\}$ is not since $1 \to 2$ as well.
\end{oplossing}
\item Calculate the probability of hitting state 2 starting from state 1.
\begin{oplossing}
We start by reducing the diagram by simply observing. Note that if we land at state $4$ at some point we will immediately jump to state $2$, and if we land at state $5$ we will also hit state $2$ (either right after or by going through $4$ first). If we hit $6$ we immediately jump back to $1$. So if we set $A = \{1, 6\}, B = \{3\}, C = \{2, 4, 5\}$ then the question is equivalent to the chance that we will hit $C$ starting from $A$, where the transition matrix is given by
\[
\tilde{P} = \begin{pmatrix}
\frac{1}{5} & \frac{1}{5} & \frac{3}{5} \\
0 & 1 & 0 \\
0 & 0 & 1 \\
\end{pmatrix}.
\]
So if we set $h = \PP_A(\text{hit C})$, then we see that $h$ satisfies $h = \frac{3}{5} + \frac{1}{5}h$, hence $h = \frac{3}{4}$.
\end{oplossing}
\item Calculate the expected time to hit state $4$ starting from state $2$.
\begin{oplossing}
Since the class $\{2, 4, 5\}$ is closed, we only have to look at the transition between states $2, 4$ and $5$, which is given by
\[
\tilde{P} = \begin{pmatrix}
0 & 0 & 1 \\
1 & 0 & 0 \\
\frac{1}{3} & \frac{2}{3} & 0.
\end{pmatrix}
\]
So if we set $k_i = \EE_i(\text{time to hit }4)$, we find the following system of equations
\begin{align}
k_2  &= k_5 + 1, \\
k_4 &= 0, \\
k_5 &= \frac{1}{3}(k_2 + 1) + \frac{2}{3}(k_4 + 1).
\end{align}
Substituting (1) and (2) into (3) yields $k_5 = \frac{1}{3}k_5 + \frac{4}{3}$, so $k_5 = 2$. From (1) we may then conclude $k_2 = 3$.
\end{oplossing}

\item Compute $\PP(X_n = 2 \mid X_0 = 2)$ for all $n \in \NN$.
\begin{oplossing}
As noted above, the class $\{2, 4, 5\}$ is closed so we can focus on the transition probabilities between these states only, given by 
\[
\tilde{P} = \begin{pmatrix}
0 & 0 & 1 \\
1 & 0 & 0 \\
\frac{1}{3} & \frac{2}{3} & 0.
\end{pmatrix}
\]
We compute
\[
\chi_{\tilde{P}}(\lambda) = -\lambda^3 + \frac{1}{3}\lambda + \frac{2}{3} = (1 - \lambda)(\lambda^2 + \lambda + \frac{2}{3}),
\]
so the eigenvalues are $\{1, \frac{-3 \pm i\sqrt{15}}{6}\}$.

Hence we may conclude (we write $\zeta = \arctan(\sqrt{5/3}) = \arg(\frac{3 + i\sqrt{15}}{6})$)
\begin{align*}
p_{22}^{(n)} &= \alpha + \beta \(\frac{-3 + i\sqrt{15}}{6}\)^n + \gamma\(\frac{-3 - i\sqrt{15}}{6}\)^n \\
&= \alpha + \beta \(-\frac{1}{3}\sqrt{6}\)^ne^{-n i \zeta} + \gamma\(-\frac{1}{3}\sqrt{6}\)^ne^{n i \zeta} \\
&= \alpha + \(-\frac{1}{3}\sqrt{6}\)^n((\beta + \gamma)\cos(n\zeta) + (\gamma - \beta) i \sin(n \zeta)).
\end{align*}
Since $p_{22}^{(0)} = 1, p_{22}^{(1)} = 0, p_{22}^{(2)} = \frac{1}{3}$, we find the following linear system of equations
\begin{align}
\alpha + \beta + \gamma &= 1, \\
\alpha - \frac{1}{2}(\beta + \gamma) + i\frac{\sqrt{15}}{6}(\beta - \gamma) &= 0, \\
\alpha - \frac{1}{6}(\beta + \gamma)  -  i\frac{\sqrt{15}}{6}(\beta - \gamma) &= \frac{1}{3}.
\end{align}
Adding (5) + (6), then substituting (4) yields $2\alpha - \frac{2}{3}(1 - \alpha) = \frac{1}{3}$, hence $\alpha = \frac{3}{8}$. That leaves us with
\begin{align}
\frac{1}{2}(\beta + \gamma) - i\frac{\sqrt{15}}{6}(\beta - \gamma) &= \frac{3}{8}, \\
\frac{1}{6}(\beta + \gamma) + i\frac{\sqrt{15}}{6}(\beta - \gamma) &= \frac{1}{24}.
\end{align}
Adding them together gives us $\beta + \gamma = \frac{5}{8}$, and subtracting (8) from (7) in threefold yields $(\beta - \gamma) = \frac{i\sqrt{15}}{40}$. Finally we conclude that
\[
p_{22}^{(n)} = \frac{3}{8} + \(-\frac{1}{3}\sqrt{6}\)^n\(\frac{5}{8}\cos(n\zeta) + \frac{\sqrt{15}}{40} \sin(n \zeta)\).
\]
\end{oplossing}
\item What is the average return time of state 2?
\begin{oplossing}
By exhausting all possibilities in the matrix we see that starting in state $2$, $(X_n)$ either follows the path $2 \to 5 \to 2$ with probability $\frac{1}{3}$, or follows the path $2 \to 5 \to 4 \to 2$ with probability $\frac{2}{3}$. Hence the average return time is $\frac{1}{3}\cdot 2 + \frac{2}{3}\cdot 3 = \frac{8}{3}$. 
\end{oplossing}
\end{enumerate}
\end{document}
