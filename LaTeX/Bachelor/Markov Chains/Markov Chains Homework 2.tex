\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=0.5em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Problem.]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Markov Chains Homework 2}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}
Let $(X_n)_{n=0,1,2,\dots}$ be a discrete-time Markov chain with probability transition matrix:
\[
P = \begin{pmatrix}
0 & \frac{2}{5} & \frac{1}{5} & 0 & \frac{1}{5} & \frac{1}{5} \\
0 & 0 & 0 & 0 & 1 & 0 \\
0 & 0 & 1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 & 0 & 0 \\
0 & \frac{1}{3} & 0 & \frac{2}{3} & 0 & 0 \\
1 & 0 & 0 & 0 & 0 & 0
\end{pmatrix}
\]
\end{opghand}
\begin{enumerate}[a)]
\item Determine the communicating classes of $X_n$. Which of them are closed, and which are open?
\begin{oplossing}
Clearly $3$ only maps to itself, hence $\{3\}$ is one of the classes. We see directly that $1 \leftrightarrow 6$, $2 \leftrightarrow 5$. Since also $2 \to 4$ (through 5) and $4 \to 2$ we may also conclude $2 \leftrightarrow 5$. We conclude that the communicating classes are
\[
\{3\}, \{1, 6\}, \{2, 4, 5\}.
\]
Now $\{3\}$ and $\{2, 4, 5\}$ are closed, but $\{1, 6\}$ is not since $1 \to 2$ as well.
\end{oplossing}
\item Find the invariant distribution for each closed class.
\begin{oplossing}
Clearly $(0, 0, 1, 0, 0, 0)$ is an invariant distribution for the class $\{3\}$. If $(0, h_2, 0, h_4, h_5)$ were an invariant measure for the class $\{2, 4, 5\}$, then we see using some theorem that the $h_i$ must satisfy
\begin{align}
h_2 &= h_4 + \frac{1}{3}h_5 \\
h_4 &= \frac{2}{3}h_5 \\
h_5 &= h_2.
\end{align}
If we assume $h_2 = 1$ then we conclude $h_5 = 1$, $h_4 = \frac{2}{3}$, it is then easily verified that $(0, 1, 0, \frac{2}{3}, 1, 0)$ is an invariant measure for the class. To turn it into a distribution we simply normalize the measure, so $(0, \frac{3}{8}, 0, \frac{2}{8}, \frac{3}{8}, 0)$ is an invariant distribution for $\{2, 4, 5\}$. 
\end{oplossing}
\newpage
\item For each transient state $i$ and each closed class $C$ find the probability that the process is eventually
absorbed in $C$ conditional on $X_0 = i$.
\begin{oplossing}
Since all classes are finite, and a finite class is recurrent if and only if it's closed, we see that the only transient states are $\{1, 6\}$. We know from (a) that the only closed classes are $\{3\}$ and $\{2, 4, 5\}$. Since $X_n = 6$ implies $X_{n + 1} = 1$ with probability $1$, the chance of getting absorbed in either class is the same if $X_0 = 1$ or $X_0 = 6$. By looking at the first row of the matrix (and using that $X_n = 6$ implies $X_{n + 1} = 1$ again) we see
\[
\PP_1(\text{hit } \{3\}) = \frac{1}{5} + \frac{1}{5}\PP_1(\text{hit } \{3\}), \qquad \PP_1(\text{hit } \{2, 4, 5\}) = \frac{3}{5} + \frac{1}{5}\PP_1\text{hit } \{2, 4, 5\}),
\] 
so we conclude that the process gets absorbed in $\{3\}$ with probability $\frac{1}{4}$ and in $\{2, 4, 5\}$ with probability $\frac{3}{4}$. 
\end{oplossing}
\item What is the average return time to state 2?
\begin{oplossing}
By exhausting all possibilities in the matrix we see that starting in state $2$, $(X_n)$ either follows the path $2 \to 5 \to 2$ with probability $\frac{1}{3}$, or follows the path $2 \to 5 \to 4 \to 2$ with probability $\frac{2}{3}$. Hence the average return time is $\frac{1}{3}\cdot 2 + \frac{2}{3}\cdot 3 = \frac{8}{3}$. 
\end{oplossing}
\item Let $N$ be the last visit of $X_n$ to state $1$ given $X(0) = 1$. Compute $\EE[z^N]$ for $z \in [0, 1]$. 
\begin{oplossing}
Since the process can never go back to $\{1, 6\}$ once it reaches $\{2, 3, 4, 5\}$, we see that if $X_n = 1$, then $X_{n - 1} = 6, X_{n - 2} = 1, \dots, X_1 = 6, X_0 = 1$. And the only way for $N = n$ to hold is if $X_n = 1, X_{n + 1} \in \{2, 3, 4, 5\}$. So we conclude
\[
\PP_1(N = n) = \begin{cases}
0 &\text{ if } $n$ \text{ is odd,} \\
\(\frac{1}{5}\)^\frac{n}{2}\frac{4}{5} &\text{ if } $n$ \text{ is even.}
\end{cases}
\]

Now if $z = 1$ the expectation is clearly 1, otherwise $z < 1$ hence the below expressions converge, therefore
\begin{align*}
\EE[z^N] &= \sum_{n = 0}^\infty \PP(N = n)z^n = \sum_{n = 0}^\infty \PP(N = 2n)z^{2n}\\
&= \(\frac{1}{5}\)^n\frac{4}{5}z^{2n} = \frac{4}{5 - z^2}.
\end{align*}
\end{oplossing}
\end{enumerate}
\end{document}
