\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{enumitem}
\usepackage{bbm}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=0.5em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Problem.]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\pois}{Pois}
\DeclareMathOperator{\id}{id}
\newcommand{\ii}{\mathbbm{1}}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\renewcommand{\P}{\mathbb{P}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Markov Chains Homework 4}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}
Let $(X_n)_{n = 0, 1, 2, \dots}$ be a discrete-time Markov chain with probability transition matrix:
\[
\begin{pmatrix}
0 & \frac{2}{5} & \frac{1}{5} & 0 & \frac{1}{5} & \frac{1}{5} \\
0 & 0 & 0 & 0 & 1 & 0 \\
0 & 0 & 1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 & 0 & 0 \\
0 & \frac{1}{3} & 0 & \frac{2}{3} & 0 & 0 \\
1 & 0 & 0 & 0 & 0 & 0
\end{pmatrix}.
\]
$X_n$ is the embedded chain at jumptimes of a continuous-time Markov chain $X(t)_{t \geq 0}$ with exponentially distributed sojourn times in all states. For $X(t)$, the intensity parameter of the sojourn in state 1 is 5, in state 2 it is 3, in state 3 it is 0, in state 4 it is 1 and in both states 5 and 6 it is 3. Note that state 3 is special, because $X(t)$ never exits that state.
\end{opghand}
\begin{enumerate}[wide, labelindent=0pt,label=(\alph*)]
\item Give the infinitesemal generator $Q$ of the process $X(t)$.
\begin{oplossing}
We find
\[
Q = \begin{pmatrix}
-5 & 2 & 1 & 0 & 1 & 1 \\
0 & -3 & 0 & 0 & 3 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 \\
0 & 1 & 0 & -1 & 0 & 0 \\
0 & 1 & 0 & 2 & -3 & 0 \\
3 & 0 & 0 & 0 & 0 & -3
\end{pmatrix}.
\]
\end{oplossing}
\item For $X(t)$ calculate the expected time to hit $4$ starting from $2$.
\begin{oplossing}
Note that $\{2, 4, 5\}$ is a closed class. We can thus only look at the Markov chain obtained by restricting $X(t)$ to $\{2, 4, 5\}$. It's generator matrix is given by
\[
Q = \begin{pmatrix}
-3 & 0 & 3 \\
1 & -1 & 0 \\
1 & 2 & -3
\end{pmatrix}.
\]
Let $k_i = \EE_i[\text{time to hit $4$}]$. Then clearly $k_4 = 0$ and by some theorem the $k_i$ satisfy
\begin{align*}
3k_2 - 3k_5 &= 1, \\
-k_2 - 2k_4 + 3k_5 &= 1.
\end{align*}
We thus obtain $k_4 = 1$. Solving the system yields $k_2 = 1, k_5 = \frac{2}{3}$, so we conclude $\EE_2[\text{time to hit 4}] = 1$.
\end{oplossing}
\item Find an invariant distribution for $X(t)$.
\begin{oplossing}
Since $\{2, 4, 5\}$ is closed, we can simply find an invariant distribution $\lambda$ for $\{2, 4, 5\}$ and then set $\lambda_i = 0$ for all $i \not \in \{2, 4, 5\}$. Solving
\[
(\lambda_2, \lambda_4, \lambda_5)\begin{pmatrix}
-3 & 0 & 3 \\
1 & -1 & 0 \\
1 & 2 & -3
\end{pmatrix} = 0
\]
together with $\lambda_2 + \lambda_4 + \lambda_5 = 1$ yields $\lambda_2 = \frac{1}{4}, \lambda_4 = \frac{1}{2}, \lambda_5 = \frac{1}{4}$. Hence
\[
\lambda = \(0, \frac{1}{4}, 0, \frac{1}{2}, \frac{1}{4}, 0\)
\]
is an invariant distribution for $Q$.
\end{oplossing}
\item Compute $\lim_{t \to \infty} \PP(X(t) = 2 \mid X(0) = 1)$.
\begin{oplossing}
It's obvious that with probability $1$, $X(t)$ either gets absorbed in $\{3\}$ or in $\{2, 4, 5\}$ since these are the only closed classes. If $X(t)$ gets absorbed in $\{3\}$ it will obviously never hit $2$. Hence 
\begin{align*}
&\lim_{t \to \infty}\PP(X(t) = 2 | X(0) = 1) \\
&= \PP(X(t) \text{ gets absorbed in $\{2, 4, 5\}$} | X(0) = 1)\cdot 
\(\lim_{t \to \infty} \PP\(X(t) = 2|X(0) \in \{2, 4, 5\}\)\)
\end{align*} Since $\{2, 4, 5\}$ is  irreducible, by theorem 3.6.2  we know that 
\[
\PP(X(t) = 2|X(0) \in \{2, 4, 5\}) \to \lambda_2 = \frac{1}{4}.
\]
Finally we compute $\PP(X(t) \text{ gets absorbed in $\{2, 4, 5\}$} | X(0) = 1)$. Let 
\[
h_i = \PP(X(t) \text{gets absorbed in $\{2, 4, 5\}$}|X(0) = i)
\]
Then the $h_i$ satisfy (using the minimality we find $h_3 = 0$)
\begin{align*}
h_1 &= \frac{2}{5}h_2 + \frac{1}{5}h_3 + \frac{1}{5}h_5 + \frac{1}{5}h_6 \\
h_2 &= h_4 = h_5 = 1 \\
h_3 &= 0 \\
h_6 &= h_1.
\end{align*}
Solving for $h_1$ gives us $h_1 = 3/4$, so we obtain 
\[
\lim_{t \to \infty}\PP(X(t) = 2 | X(0) = 1) = 3/4 \cdot 1/4 = 3/16.
\]
\end{oplossing}
\item What is the average return time of $X(t)$ to state 2?
\begin{oplossing}
By theorem 3.5.3, this is given by $1 / (\lambda_2 q_2) = 1/(\frac{1}{4}\cdot 3) = \frac{3}{4}$.
\end{oplossing}
\end{enumerate}
\end{document}
