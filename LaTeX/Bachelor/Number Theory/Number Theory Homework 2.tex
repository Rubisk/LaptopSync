\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm, mathrsfs }
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\renewcommand{\P}{\mathbb{P}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\newcommand{\ord}[2]{\mathcal{O}_{#1}(#2)}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}
\newcommand{\legendre}[2]{\(\frac{#1}{#2}\)}

%% Document
\begin{document}

%% Kopregel
\title{Number Theory Homework 2}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[2.14] Let $p$ be an odd prime, $e \in \NN$, and $g \in \ZZ$ a primitive root modulo $p^e$. Show that $g$ or $g + p^e$ is a primitive root modulo $2p^e$. 
\end{opghand}
\begin{oplossing}
We write $\ord{x}{y}$ for the order of $y \pmod{x}$. 

Note that $\varphi(2p^e) = \varphi(2)\varphi(p^e) = \varphi(p^e)$. Now if for some $x \in \ZZ$ and some $k \in \NN$ satisfying $\gcd(2p^e, x) = 1$ we have $\ord{2p^e}{x} = k$, then $x^k \equiv 1 \pmod{2p^e}$. Therefor $x^k \equiv 1 \pmod{p^e}$, hence $\ord{p^e}{x} \mid k$, which implies $\ord{p^e}{x} \mid \ord{2p^e}{x}$.

In particular, if $g \in \ZZ$ is a generator for $(\ZZ / p^e\ZZ)^*$, then $\ord{p^e}{g} = \varphi(p^e)$. By Lagrange we have (as long as $g \in (\ZZ / 2p^e\ZZ)^*)$) the identity $\ord{p^{2e}}{g} \mid \varphi(2p^e)$, hence by our first observation we find (still under the assumption $g \in (\ZZ / 2p^e\ZZ)^*)$) the sequence of relations
\[
\varphi(p^e) = \ord{p^e}{g} \mid \ord{2p^e}{g} \mid \varphi(2p^e) = \varphi(p^e).
\]
Therefore $\ord{2p^e}{g} = \varphi(2p^e)$ for all primitive roots $g \in \ZZ$ modulo $p^e$ that satisfy $\gcd(g, 2p^e) = 1$. Hence if $g$ is a primitive root modulo $p^e$ and $\gcd(g, 2p^e) = 1$ then $g$ is also a primitive root modulo $2p^e$. 

Now if $g$ is a primitive root modulo $p^e$, then $p \nmid g$. Hence if $g$ is odd then $\gcd(g, 2p^e) = 1$, so $g$ is also a primitive root modulo $2p^e$. Otherwise $g + p^e$ is odd (since p is odd), hence $\gcd(g + p^e, 2p^e) = 1$. Since $g + p^e$ is also a primitive root modulo $p^e$ (it's congruent with $g$), we conclude $g + p^e$ is a primitive root modulo $2p^e$.
\end{oplossing}
\begin{opghand}[3.4](Fermat Numbers). For $n \in \ZZ_{\geq 0}$ denote $F_n := 2^{2^n} + 1.$
\end{opghand}
\begin{enumerate}[a)]
\item Prove that for all $n \in \ZZ_{\geq 0}$: $F_n = \prod_{i=0}^{n - 1} F_i + 2$ (where as usual the empty product equals 1).
\end{enumerate}
\begin{oplossing}
We proceed by induction. For $n = 0$ the statement is equivalent to $2^{2^0} + 1 = \prod_{\emptyset} + 2$ which clearly holds. Now suppose the statement holds for $n$, then
\begin{align*}
F_{n + 1} &= 2^{2^{n + 1}} + 1 = (2^{2^{n + 1}} - 1) + 2 = (2^{2^n} - 1)(2^{2^n} + 1) + 2 \\
&=  (F_n - 2)F_n + 2 = \(\prod_{i = 0}^{n - 1} F_i\)F_n + 2 = \prod_{i = 0}^n F_i + 2,
\end{align*}
hence the statement holds for $n + 1$.
\end{oplossing}
\newpage
\begin{enumerate}[b)]
\item Show that for any $n \in \ZZ_{\geq 0}$ either $F_n$ is prime or a (Fermat) pseudoprime to base 2.
\end{enumerate}
\begin{oplossing}
Note that
\[
2^{F_n - 1} \equiv 2^{2^{2^n}} \equiv (2^{2^n})^{2^{2^n - n}} \equiv (-1)^{2^{2^n - n}} \equiv 1 \pmod{F_n},
\]
hence if $F_n$ is not prime it is a Fermat pseudoprime by definition.
\end{oplossing}
\begin{opghand}[5.7.5] Let $p$ be a prime and suppose $p \equiv 1 \pmod{3}$.
\end{opghand}
\begin{enumerate}[1.]
\item Prove that $x^2 + x + 1 \equiv 0 \pmod{p}$ has a solution.
\end{enumerate}
\begin{oplossing}
Since $p \equiv 1 \pmod{3}$ we have $\varphi(p) = p - 1 = 3k$ for some $k \in \ZZ$. By Cauchy's theorem for groups, there is an element $x$ of order $3$ in $(\ZZ / p\ZZ)^*$, since $3$ is prime. Note that $(x - 1)(x^2 + x + 1) \equiv x^3 - 1 \equiv 0 \pmod{p}$. Since $x$ has order $3$, in particular $x \neq 1$, hence $x^2 + x + 1 \equiv 0 \pmod{p}$ since $p$ is prime and therefore dividies $x^2 + x + 1$.
\end{oplossing}
\begin{enumerate}[2.]
\item Prove, using 1), that $\legendre{-3}{p} = 1$ if $p \equiv 1 \pmod{3}$.
\end{enumerate}
\begin{oplossing}
If $p \equiv 1 \pmod{3}$, then $x^2 + x + 1 \equiv 0 \pmod{p}$ for some $x \in \ZZ$. Therefore $(2x + 1)^2 \equiv 4(x^2 + x + 1) - 3 \equiv -3 \pmod{p}$, hence $y^2 \equiv -3 \pmod{p}$ has a solution, so $\legendre{-3}{p} = 1$.
\end{oplossing}
\begin{enumerate}[3.]
\item Determine the discriminant of $x^2 + x + 1$.
\end{enumerate}
\begin{oplossing}
A simple calculation shows $D = 1^2 - 4 \cdot 1 \cdot 1 = -3$.
\end{oplossing}
\begin{enumerate}[4.]
\item Prove, using considerations such as in 1) and 2) that $\legendre{-3}{p} = -1$ if $p \equiv -1 \pmod{3}$. 
\end{enumerate}
\begin{oplossing}
The statement is false if $p = 2$, so we assume $p > 2$. 

If $x^2 + x + 1 \equiv 0 \pmod{p}$ for some $x \in \ZZ$, then $x^3 - 1 = (x - 1)(x^2 + x + 1) = 0$, hence $x^3 \equiv 1$. Since $p \neq 3$ (because $p \equiv -1 \pmod{3}$), we see $p \nmid 1 + 1 + 1$ and therefore $x \not \equiv 1 \pmod{p}$, hence $x$ has order $3$. But then $3 \mid \varphi(p) = p - 1$ by Lagrange's theorem, so $p \equiv 1 \pmod{3}$, which is a contradiction. Hence $x^2 + x + 1$ has no solution if $p \equiv -1 \pmod{3}$. 

On the other hand, if $y^2 \equiv -3 \pmod{p}$ for some $y \in \ZZ$, then $x = 2^{-1}(-1 - y)$ is a root of $x^2 + x + 1$ (here $2^{-1}$ denotes a multiplicative inverse of $2$ modulo $p$, such number exists as long as $2 \nmid p$). To see this, expand
\begin{align*}
(2^{-1}(-1 - y))^2 + 2^{-1}(-1 - y) + 1 &= 4^{-1}(1 + y^2 + 2y) - 2^{-1} - 2^{-1}y + 1 \\
&= 4^{-1}(1 + y^2) - 2^{-1} + 1 \\
&= -2^{-1} - 2^{-1} + 1 = -2\cdot2^{-1} + 1 = 0,
\end{align*}
or note that the abc-formula also holds in $(\ZZ / p\ZZ)^*$ as long as we can solve the square root  and ``divide''. Hence if $y^2 \equiv -3 \pmod{p}$ has a solution then $x^2 + x + 1 \equiv 0 \pmod{p}$ also has a solution. Since the latter does not hold if $p \equiv -1 \pmod{3}$, neither does the former.

\end{oplossing}
\end{document}
