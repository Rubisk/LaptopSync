\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\ord}{ord}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\renewcommand{\P}{\mathbb{P}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Number Theory Homework 1}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[1.9] Show that an odd perfect number is divisible by at least 3 primes.
\end{opghand}
\begin{oplossing}
Suppose that $n = p_1^{e_1}p_2^{e_2}$ is an odd perfect number. Then $2 = \frac{\sigma{(n)}}{n}$, hence by multiplicity of $\sigma$ we see that
\begin{align*}
2 &= \frac{\(\sum_{i = 0}^{e_1}p_1^i\)\(\sum_{i = 0}^{e_2}p_2^i\)}{p_1^{e_1}p_2^{e_2}} = \(\frac{p_1^{e_1 + 1} - 1}{p_1^{e_1}(p_1 - 1)}\)\(\frac{p_2^{e_2 + 1} - 1}{p_2^{e_2}(p_2 - 1)}\) \\
&< \frac{p_1}{p_1 - 1} \frac{p_2}{p_2 - 1}.
\end{align*}
Now this last expression gets smaller if the $p_i$ get bigger, so it's maximal when $p_1 = 3$, $p_2 = 5$, therefore
\[
\frac{p_1}{p_1 - 1} \frac{p_2}{p_2 - 1} \leq \frac{3}{3 - 1}\frac{5}{5 - 1} = \frac{15}{8} < 2,
\]
which is a contradiction.
\end{oplossing}
\begin{opghand}[2.4] Determine all $n \in \NN$ such that
\begin{enumerate}[a)]
\item $\varphi(n) = 14$.
\begin{oplossing}
Note that if $n = \prod p_i^{e_i}$, we have $\varphi(n) = \prod (p_i^{e_i - 1} - 1)$. Hence if $\varphi(n) = 14$, we have in particular $p_i^{e_i - 1}(p_i - 1) \leq 14$ for any prime power $p_i^{e_i}$ dividing $n$. We can thus simply list all prime powers such that $p_i^{e_i - 1}(p_i - 1) < 15$ (with their corresponding $\varphi$ value), then any solution must be a product of some choice of these (to see that we consider all prime powers in the table below, remark that $p_i^{e_i - 1}(p_i - 1) < 15$ implies $p_i < 15$ so we only have to consider small primes).
\begin{center}
\begin{tabular}{l | c c c c | c c | c | c | c | c}
$p^k$ & 2 & 4 & 8 & 16 & 3 & 9 & 5 & 7 & 11 & 13\\
\hline
$\varphi(p^k)$ & 1 & 2 & 4 & 8 & 2 & 6 & 4 & 6 & 10 & 12
\end{tabular}
\end{center}
Since none of the $\varphi(p^k)$ are divisible by $7$ but $14$ is, we find that no such $n$ exist.
\end{oplossing}

\item $\varphi(n) = 24$.
\begin{oplossing}
We can simply extend our solution from $n = 14$ all the way to 24.
\begin{center}
\begin{tabular}{l | c c c c c | c c c | c c | c | c | c | c | c}
$p^k$ & 2 & 4 & 8 & 16 & 32 & 3 & 9 & 27 & 5 & 25 & 7 & 11 & 13 & 17 & 23\\
\hline
$\varphi(p^k)$ & 1 & 2 & 4 & 8 & 16 & 2 & 6 & 18 & 4 & 20 & 6 & 10 & 12 & 16 & 22
\end{tabular}
\end{center}
Through exhausting all possible combinations, we find the set of solutions
$
\{26, 72, 40, 39, 45, 35, 78, 90, 70, 94\}.
$
\end{oplossing}
\item $\varphi(n) \mid n$.
\begin{oplossing}
If we write $n = p_1^{e_1}\cdots p_k^{e_k}$ then this implies that
\begin{align}
\prod_{i = 1}^k \frac{p_i}{p_i - 1} = \prod_{i = 1}^k \frac{p_i^{e_i}}{p_i^{e_i - 1}(e_i - 1)} = \frac{n}{\varphi(n)}
\end{align}
is an integer. For any odd prime $p$ it's clear that $p - 1$ is even. Now since at most one of the $p_i$ is even (only $2$ can appear), $\prod p_i$ is divisible by at most one power of $2$. Hence $n$ has at most one odd prime factor, since otherwise $\prod (p_i - 1)$ would be divisble by $4$, and the leftmost expression in (1) would be nonintegral. Also clearly $n$ has at least 2 prime factors (or $n = 1$), since $\frac{p}{p - 1}$ is not an integer for any $p > 1$. So $n$ is of the form $2^x$ (with $x \geq 0$) or $2^xp^y$ (with $x \geq 1$) and with $p$ an odd prime, and $\frac{2p}{p - 1}$ an integer. But then $p - 1 = 2$ since $\gcd(p, p - 1) = 1$. We conclude $p = 3$, and therefore all solutions are given by $n = 2^x3^y$ with $x, y \in \NN$, or $n = 2^x$ with $x \in \NN \cup \{0\}$.
\end{oplossing}
\end{enumerate}
\end{opghand}
\begin{opghand}[2.16] Let $q$ be an odd prime and $n \in \NN$.
\begin{enumerate}[a)]
\item Prove that a prime $p$ dividing $1 + n + \dots + n^{q - 1}$ satisfies $p \equiv 1 \pmod{q}$ or $p = q$.
\begin{oplossing}
Multiplying by $n - 1$ yields $p \mid n^q - 1$ or $n^q \equiv 1 \pmod{p}$. Since $q$ is prime, we conclude $\ord_p(n) = q$ or $n \equiv 1 \pmod{p}$. If $\ord_p(n) = q$, then from Euler we may also conclude $n^{p - 1} \equiv 1 \pmod{p}$, hence $q \mid p - 1$ and $p \equiv 1 \pmod{q}$. Otherwise $1 + n + \dots n^{q - 1} \equiv 1 + \dots + 1 \pmod{p}$, hence $p \mid q$ therefore $p = q$.
\end{oplossing}
\item Prove that there are infinitely many primes $\equiv 1 \pmod{q}$.
\begin{oplossing}
We provide a proof by contradiction, so suppose there were only finitely many, say $p_1, \dots, p_n$. Then let $n = q \cdot p_1 \cdots p_n$. Then clearly none of $q, p_1, \dots, p_n$ can divide $n^q - 1$ since they all divide $n^q$. But by (a) all prime divisors are either $q$ or some $p_i$, which is a contradiction with the fundamental theorem of arithmetic.
\end{oplossing}
\end{enumerate}
\end{opghand}
\end{document}
