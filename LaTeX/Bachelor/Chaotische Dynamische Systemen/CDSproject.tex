\documentclass[10pt]{extarticle}

\usepackage{fullpage}
\usepackage[dutch]{babel}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{bbm}
\usepackage{mathrsfs}
\usepackage{float}
\usepackage{natbib}  
\usepackage{a4wide}
\usepackage{amsthm}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{bbm}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage[pdfborder={0 0 0}]{hyperref}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{Id}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\GL}{GL}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\expo}{exp}
\newcommand{\ii}{\mathbbm{1}}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\HH}{\mathbb{H}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand{\twomatrix}[4]{\left(\begin{smallmatrix} #1 & #2 \\ #3 & #4 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

\setlength\parindent{0pt}

\newtheorem{mylem}{Lemma}
\newtheorem{mythe}{Stelling}
\theoremstyle{definition}
\newtheorem{mydef}{Definitie}

\title{Equipartitie en ergodiciteit}
\date{\today}
\author{Michiel Lieftink, Wouter Rienks, Mike Daas}

\begin{document}
\maketitle
\begin{section}{Inleiding}
Beschouw een situatie waarin men een grote verzameling numerieke data tot zijn beschikking heeft. Een vraag die men zich zou kunnen stellen is de volgende:
\[
\textit{Wat is de relatieve frequentie van de eerste cijfers van de getallen uit onze dataset?}
\]
Op het eerste gezicht zou men verwachten dat de dataset een (quasi-)arbitraire verzameling natuurlijke getallen is, voor zover daarvan te spreken valt, zodat elk cijfer tussen 1 en 9 gelijke kans zou hebben om als eerste op te duiken. Het opmerkelijke feit dat dit in de praktijk \textbf{niet} het geval is, werd reeds in 1881 opgemerkt door Simon Newcomb, maar is later vernoemd naar Frank Benford die er meer over publiceerde. Deze heren observeerden dat de kans voor een getal om te beginnen met het cijfer $k \in \{1,\ldots,9\}$ gelijk leek te zijn aan $\log_{10}(1+1/k)$. Ter illustratie hieronder een tabel.
\begin{table}[h]
\centering
\begin{tabular}{ c || c | c | c | c | c | c | c | c | c | c}
k & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9\\ \hline
$\mathbb{P}(k)$ & 0.301 & 0.176 & 0.125 & 0.097 & 0.079 & 0.067 & 0.058 & 0.051 & 0.046\\
\end{tabular} 
\end{table}
\\
Een korte check welke men kan verrichten is dat de kansen inderdaad optellen tot 1;
\[
\mathbb{P}(1)+\ldots+\mathbb{P}(9) = \log_{10}(2/1)+\log_{10}(3/2)+\ldots\log_{10}(10/9) = \log_{10}(10/1) = 1.
\]
De precieze redenen achter deze onverwachte kansverdeling in grote datasets zijn nog altijd veelal onbekend. In specifieke gevallen kan men deze bewering echter wel bewijzen. In dit korte verslag zal de wet van Benford worden aangetoond voor bekende meetkundige rijen met behulp van de equidistributietheorie modulo 1. Bovendien zal een stelling van Weyl bewezen worden welke een uitspraak doet over de equidistributie van polynomen modulo 1 met minstens 1 irrationale co\"{e}ffici\"{e}nt.\\
Veel materiaal is ontleend aan het boek \textit{Ergodic Theory (with a view towards number theory)} van Manfred Einsiedler en Thomas Ward. Hiernaar zal omwille van de leesbaarheid gedurende de tekst niet naar worden gerefereerd.
\end{section}

\begin{section}{De meetkundige rij}
Allereerst formuleren we een klassieke stelling die noodzakelijk gaat zijn op meerdere punten.
\begin{mythe}[Fourier]\label{Fourier}
Zij $\lambda$ de Lebesgue-maat op $\TT$. Schrijf $L^2(\mathbb{T})$ voor de verzameling integreerbare functies op $\TT$ met de eigenschap dat $\int_{\TT}f^2 \ \mathrm{d}\lambda < \infty$. Deze verzameling omdat $C(\TT)$, daar $\TT$ compact is. Ook vormt de verzameling afbeeldingen 
\[
\{\chi_n: \TT \to \CC, x \mapsto e^{2 \pi i n x} | n \in \ZZ\}
\]
een orthogonale basis van $L^2(\TT)$. 
\end{mythe}
We vervolgen onze verhandeling met een aantal definities.
\begin{mydef}
Zij $X$ een compacte metrische ruimte en laat $T \colon X \mapsto X$ een continue afbeelding zijn. Een maat $\mu$ op $X$ heet een \emph{Borel-maat} als $\mu$ gedefinieerd is op alle open verzamelingen van $X$. We noemen een Borel-maat $\mu$ op $X$ \emph{invariant} onder $T$ als voor elke meetbare verzameling $A \subset X$, geldt dat $\mu(T^{-1}(A)) = \mu(A)$. Noteer $\mathscr{M}^T(X)$ voor de verzameling van alle $T$-invariante Borel-maten op $X$. We noemen $T$ dan \emph{uniek ergodisch} als $\mathscr{M}^T(X)$ uit slechts 1 maat bestaat.
\end{mydef}

Op het eerste gezicht lijkt dit een vrij onnatuurlijke definitie, maar de volgende stelling geeft aan dat de unieke ergodiciteit van een functie direct samenhangt met de equipartitie van zijn beeldpunten in $X$. Deze stelling zullen wij slechts formuleren en gebruiken, maar niet bewijzen.

\begin{mythe}\label{Ergodic}  In de situatie van de definitie zijn equivalent:
\begin{enumerate}[(1)]
\item $T: X \rightarrow X$ is een uniek ergodische afbeelding.

\item Voor alle $f \in C(X)$ en $x \in X$ geldt
\[
\frac{1}{N} \sum_{n=0}^{N-1} f(T^n(x)) \longrightarrow \int_X f \mathrm{d} \mu.  
\]

Hier is $\mu$ de unieke invariante maat.
\item De convergentie in (2) geldt voor een dichte deelverzameling van $C(X)$.
\end{enumerate}
\end{mythe}

Nu zijn we geïnteresseerd in het eerste cijfer van $2^n$ voor verscheidene $n$. Hiervoor merken we eerst op dat vermenigvuldigen met $2$ door de logaritme te nemen te beschouwen is als een rotatie op de cirkel, gegeven door
\[
R_{\log_{10}(2)}(t) := t+ \log_{10}(2) \mod 1.
\]
Hierbij is het eerste cijfer van $2^n$ gelijk aan $k$ precies als
\[
\log_{10}(k) \leq  R_{\alpha}^n(0) < \log_{10}(k+1).
\]
Ons doel is te bepalen wat de frequentie is van een zekere $k$ als eerste cijfer van $2^n$. In andere woorden, we dienen de limiet te bepalen van
\[
\frac{\# \{ n \mid 0 \leq n \leq N-1, \text{ eerste cijfer van } 2^n \text{ is } k \} }{N} = \frac{1}{N} \sum^{N-1}_{n=0} \mathbbm{1}_{\left[\log_{10}(k),\log_{10}(k+1)\right]} \left( R_{\log_{10}(2)}^n(0) \right).
\]
Hiertoe bewijzen we het volgende lemma.
\begin{mylem}\label{Rotatie}
De rotatie op de cirkel $\mathbb{T}$ gegeven door $R_{\alpha}(t)= t + \alpha$ is uniek ergodisch als $\alpha \notin \mathbb{Q}$. 
\end{mylem}

\begin{proof}
We bewijzen dat de unieke invariante maat de Lebesque maat $m_{\mathbb{T}}$ is. Dit doen we door punt (3) van stelling \ref{Ergodic} te bewijzen.  Daar $\alpha$ irrationaal is, volgt $e^{2 \pi i k \alpha}=1$ alleen als $k=0$, voor $k \in \mathbb{Z}$. Voor $\chi_k$ geldt
\[
\frac{1}{N} \sum^{N-1}_{n=0} \chi_k \left( R_{\alpha}^n(t) \right) = \frac{1}{N} \sum^{N-1}_{n=0} e^{2 \pi i k (t+n \alpha)} = 
\begin{cases}
1 & \text{als } k=0; \\
\frac{1}{N} e^{2 \pi i k t }\frac{e^{2 \pi i N k \alpha}-1}{e^{2 \pi i k \alpha}-1} & \text{als } k \neq 0.
\end{cases}
\] 
Maar dan is duidelijk dat
\[
\frac{1}{N} \sum^{N-1}_{n=0} \chi_k \left( R_{\alpha}^n(t) \right) \longrightarrow \int f \ \mathrm{d} m_{\mathbb{T}}=
\begin{cases}
1 \text{ als } k = 0; \\
0 \text{ als } k \neq 0. 
\end{cases}
\]
Wegens lineariteit geldt dit voor alle trigonometrische polynomen; deze functies liggen dicht vanwege stelling \ref{Fourier}. Dus punt (3) van stelling \ref{Ergodic} is bewezen; derhalve is $R_{\alpha}$ uniek ergodisch.
\end{proof}

We weten nu dat $R_{\log_{10}(2)}$ uniek ergodisch is, want $\log_{10}(2)$ is irrationaal. Maar nu levert punt (2) van stelling \ref{Ergodic} ons dat
\[
\frac{\# \{ n \mid 0 \leq n \leq N-1, \text{ eerste cijfer van } 2^n \text{ is } k \} }{N} \longrightarrow \int_{\mathbb{T}} \mathbbm{1}_{\left[\log_{10}(k),\log_{10}(k+1)\right]} \ \mathrm{d}\mu = \log_{10}\left( \frac{k+1}{k} \right).
\]
Nu hebben we dus bewezen dat $k$  met dichtheid $\log \left(\frac{k+1}{k}\right)$ voorkomt als het eerste cijfer van $2^n$. In het bijzonder zien we dus dat lagere $k$ vaker voorkomen dan hogere $k$. Merk nu op dat precies hetzelfde ook in het algemeen geldt voor de rij $a^n$ met $a \in \mathbb{N}$, tenzij $a$ een macht van $10$ is. Dit is precies de Wet van Benford.
\end{section}

\begin{section}{Equipartitie van polynomen}

Na het begrip \emph{unieke} ergodiciteit te hebben ge\"{i}ntroduceerd, defini\"{e}ren we het voor de hand liggende begrip met een vergelijkbare naam.

\begin{mydef}
Zij $(X,\Sigma,\mu)$ een kansruimte en $T \colon X \mapsto X$ een continue afbeelding zodat $\mu$ invariant is onder $T$. Dan noemen we $T$ \emph{ergodisch} ten opzichte van $\mu$ als voor elke functie $f \colon X \mapsto \CC \in L^2(X)$ met de eigenschap dat $f \circ T = f$ geldt dat $f$ \emph{bijna overal} gelijk is aan een zekere constante; oftewel, er bestaat een $z_0 \in \CC$ zodat $\mu(f^{-1}(z_0)) = 1$. Zulke functies $f$ heten $T$-invariant.
\end{mydef} 

Nu formuleren we een stelling die we kunnen gebruiken om de equipartitie van polynomen te analyseren, maar waarvan we het bewijs achterwege laten.

\begin{mythe}[Furstenberg]\label{Furstenberg} 
Laat $\lambda$ de Lebesgue-maat aangeven op $\TT$ en zij $X$ een compacte metrische ruimte. Zij $T: X \to X$ een uniek ergodisch homeomorfisme met unieke invariante maat $\mu$. Laat $c: X \to \TT^k$ een continue afbeelding zijn en definieer de scheve productafbeelding
\[
S(x, a) = (T(x), c(x) +  a).
\]
Als $S$ ergodisch is ten opzichte van $\mu \times \lambda_{\TT^k}$, dan is het zelfs uniek ergodisch.
\end{mythe}

Dit resultaat zullen we toepassen in het bewijs van het volgende lemma, wat ons zal gaan helpen met het begrijpen van de equipartitie van polynomen modulo 1.

\begin{mylem}\label{lemmameteens}
Zij $\alpha$ een irrationaal getal. Dan is de afbeelding $S: \TT^k \to \TT^k$ gegeven door
\[
S: (x_1, x_2, \dots,  x_k)^T \to (x_1 + \alpha, x_2 + x_1, \dots, x_k + x_{k - 1})^T
\]
uniek ergodisch.
\end{mylem}
\begin{proof}
We geven een bewijs met inductie naar $k$. Als $k = 1$ dan wordt $S$ gegeven door $S: \TT \to \TT: x_1 \to x_1 + \alpha$. Volgens lemma \ref{Rotatie} is deze uniek ergodisch.

Laat nu $k > 1$. Volgens onze inductiehypothese is de afbeelding
\[
T: (x_1, \dots, x_{k - 1})^T \to (x_1 + \alpha, x_1 + x_2, \ldots, x_{k - 1} + x_{k - 2})^T
\]
uniek ergodisch, en de afbeelding 
\[
c: \TT^{k - 1} \to \TT: (x_1, \dots, x_{k - 1}) \mapsto x_{k - 1}
\]
is duidelijk continu. Merk nu op dat het volgens stelling \ref{Furstenberg} voldoende is om aan te tonen dat $S$ ergodisch is ten opzichte van de Lebesgue maat op $\TT^k$.

Kies nu $f \in L^2(\TT^k)$ een $S$-invariante functie; we zullen laten zien dat $f$ constant is. Met behulp van een generalisatie van stelling \ref{Fourier} zien we dat er unieke $(c_n)_{n \in \ZZ^k}$ in $\CC$ bestaan zodat 
\[
f(x) = \sum_{n \in \ZZ^k} c_ne^{2 \pi i n \cdot x}
\]
voor alle $x \in \TT^k$, waar $\cdot$ het standaard-inproduct noteert.
Merk nu op dat 
\begin{align*}
n \cdot S(x) &= n_1(x_1 + \alpha) + n_2(x_1 + x_2) + \dots + n_k(x_k + x_{k - 1}) \\
&= n_1\alpha + (n_1 + n_2)x_1 + \dots + (n_{k - 1} + n_k)x_{k - 1} + n_kx_k,
\end{align*}
dus als we definiëren
\[
S': \ZZ^k \to \ZZ^k: (n_1, \dots, n_k) \mapsto (n_1 + n_2, \dots, n_{k - 1} + n_k, n_k),
\]
dan geldt $n \cdot S(x) = n_1\alpha + S'(n)\cdot x$ voor alle $n \in \ZZ^k$.
Aangezien $f(x) = f(S(x))$ volgt dat
\[
\sum_{n \in \ZZ^k} c_ne^{2\pi i n \cdot x} = f(x) = f(S(x)) = \sum_{n \in \ZZ^k} c_ne^{2\pi i n_1 \alpha}e^{2\pi i S'(n) \cdot x}.
\]
Uit de uniciteit van de Fouriercoëfficiënten volgt nu direct dat voor alle $n \in \ZZ^k$ geldt
\begin{equation}\label{coefficientvergelijking}
c_{S'(n)} = e^{2\pi i n_1 \alpha}c_n.
\end{equation}
Dus in het bijzonder zal $|c_{S'(n)}| = |c_n|$. Aangezien $f \in L^2(k)$ en 
\[
||f||_2 = \bigg|\bigg|\sum_{n \in \ZZ^k} c_n e^{2 \pi i n \cdot x}\bigg|\bigg|_2 \geq \sum_{n \in \ZZ^k}  |c_n|^2
\]
volgt dat $\sum_{n \in \ZZ^k} |c_n|^2 < \infty$. Hieruit volgt dat de baan $B_n = \{(S')^i(n) | i \in \NN_{\geq 0}\}$ uit eindig veel punten bestaat. Immers, voor elke $b \in B_n$ geldt $|c_b| = |c_n|$, en dus 
\[
\infty > ||f||_2 \geq \sum_{b \in B_n} |c_n|^2 = |B_n||c_n|.
\]
Dus er zijn $p > q$ met $(S')^p(n) = (S')^q(n)$. Dan volgt uit de voorlaatste component van $S'$ dat $n_{k - 1} + pn_k = n_{k - 1} + qn_k$, dus $n_k = 0$. Maar ook volgt uit de op twee na laatste dat $n_{k - 2} + pn_{k-1} = n_{k - 2} + qn_{k-1}$, dus $n_{k - 1} = 0$. Zo ziet men dat $n_i = 0$ voor alle $i \in \{2, \dots, k\}$. We concluderen dat voor alle $n \in \ZZ^k$ met $c_n \neq 0$ geldt $n = (n_1, 0, \dots, 0)$, en dus volgt uit (\ref{coefficientvergelijking}) dat
\[
c_n = e^{2\pi i \alpha n_1} c_n,
\]
dus $c_n = 0$ of $n_1 = 0$, want $\alpha$ is irrationaal. Er volgt dat $f$ constant is.
\end{proof}

We voeren nu een laatste definitie in, alvorens het hoofdresultaat van dit verslag te bewijzen.
\begin{mydef}
We noemen een rij punten $(x_n)$ in $\TT$ \emph{gelijkmatig verdeeld} modulo 1 als voor alle $f \in C(X)$ geldt dat
\[
\lim_{N \to \infty} \frac{1}{N}\sum_{j=1}^N f(x_j) = \int_{\TT} f(x) \ \mathrm{d}\lambda.
\]
\end{mydef}

Merk op dat we met stelling \ref{Ergodic} vinden dat voor een uniek ergodische afbeelding $T$ met betrekking tot $\lambda$ de rij beeldpunten $T^n(x)$ gelijkmatig verdeeld is modulo 1 voor alle $x \in \TT$. Deze observatie gebruiken we om de volgende stelling te bewijzen.

\begin{mythe}[Weyl] Zij $p(n) = a_kn^k + \dots + a_0$ een reëel polynoom met tenmenste één irrationale coëfficiënt. Dan is de verzameling $\{p(n) \mid n \in \NN\}$ gelijkmatig verdeeld modulo $1$.
\end{mythe}
\begin{proof}
We geven een bewijs met inductie naar $k$. Als $k =0$ dan is het simpelweg lemma \ref{Rotatie}.

Laat nu $k > 0$, en neem aan dat het geldt voor alle polynomen van lagere graad. Als $a_k$ rationaal is, dan is er een $q \in \ZZ$ met $qa_k \in \ZZ$. Voor vaste $j \in \NN$ is
\[
p(qn + j) = \sum_{i = 0}^k a_i(qn + j)^i = a_k (qn)^k + \sum_{i = 0}^{k - 1}\binom{k}{i}a_k(qn)^ij^{k - i} + \sum_{i = 0}^{k - 1}a_i(qn + j)^i = a_k(qn)^k + f_j(n),
\]
waar 
\[
f_j(n) = \sum_{l = 0}^{k - 1} \sum_{i = l}^{k}\(\binom{i}{l}a_iq^lj^{i - l}\) n^l
\] een polynoom is in $n$ van graad $(k - 1)$. Aangezien $a_kq \in \ZZ$ volgt dat $p(qn + j) \equiv f(n) \pmod{1}$. Het is gemakkelijk in te zien dat als $i$ de grootste $i$ is met $a_i \not \in \QQ$, dat dan de coëfficiënt van $n^i$ in $f_j$ ook niet in $\QQ$ zit, dan zijn immers alle termen in de som rationaal behalve één. Dus $f_j$ voldoet aan de eisen van de stelling en heeft kleinere graad, waaruit volgt dat $p(qn + j)$ gelijkmatig verdeeld modulo $1$ is voor alle vaste $j$. Maar dan zijn alle waarden van $p$ wel gelijkmatig verdeeld modulo $1$, daar men de som uit de definitie voor het rijtje $p(n)$ kan splitsen voor $j = 0,1,\ldots,n-1$.

Neem dus aan dat $a_k$ irrationaal is. Definieer $S: \{\alpha\} \times \TT^k \to \TT^k$ door
\[
S
\begin{pmatrix} \alpha \\ x_1 \\ x_2 \\ \vdots \\ x_k \end{pmatrix} = 
\begin{pmatrix} 1 & & & \\
1 & 1 & \\
& 1 & 1 & \\
& & \ddots & \ddots \\
& & & 1 & 1
\end{pmatrix}
\begin{pmatrix} \alpha \\ x_1 \\ x_2 \\ \vdots \\ x_k \end{pmatrix} 
= \begin{pmatrix} \alpha \\ x_1 + \alpha \\ x_1 + x_2 \\ \vdots \\ x_k + x_{k - 1} \end{pmatrix}.
\]
Dan 
\begin{align*}
S^n = y\begin{pmatrix} 1 & & & \\
n & 1 & \\
\binom{n}{2} & n & 1 & \\
\vdots & & \ddots & \ddots & \\
\binom{n}{k} & \hdots & & n & 1
\end{pmatrix}
\begin{pmatrix} \alpha \\ x_1 \\ x_2 \\ \vdots \\ x_k \end{pmatrix} 
= \begin{pmatrix}\alpha \\ n\alpha + x_1 \\ \\ \vdots \\ \binom{n}{k}\alpha + \binom{n}{k - 1}x_1 + \dots + n x_{k - 1} + x_k\end{pmatrix}.
\end{align*}
Volgens Lemma \ref{lemmameteens} is $S$ beperkt tot alles behalve de eerste co\"{o}rdinaat uniek ergodisch. Met onze observatie volgt dan dat al deze coordinaten gelijkmatig verdeeld zijn over $\TT$, en in het bijzonder is de laatste co\"{o}rdinaat 
\[
\binom{n}{k}\alpha + \binom{n}{k - 1}x_1 + \dots + n x_{k - 1} + x_k
\]
gelijkmatig verdeeld over $\TT$ voor alle keuzes van $\alpha$, $x_1, \ldots, x_n$. Aangezien $\binom{n}{k}$ een polynoom in $n$ van graad $k$ is, volgt dat we door $k + 1$ keer te delen met rest $\alpha, x_1, \dots, x_k$ kunnen vinden zodat
\[
p(n) = \binom{n}{k}\alpha + \binom{n}{k - 1}x_1 + \dots + n x_{k - 1} + x_k.
\]
Dus $p(n)$ is geijkmatig verdeeld over $[0, 1]$.
\end{proof}
\end{section}
\begin{section}{Conclusie}
Hier 

kan

 je

een 

conclusie

typen

van 

wel

veertien
 
hele

mooie

regels.

~Mike

Daas
\end{section}
\end{document}