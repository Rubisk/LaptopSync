\documentclass[a4paper,12pt]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}

\theoremstyle{definition}                            
\newtheorem{opgave}{Opgave}        
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Oplossing]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

%% eigen gedefinieerde commando's
\DeclareMathOperator{\ggd}{ggd}
\DeclareMathOperator{\binomial}{Bin}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\e}{\epsilon}
\newcommand{\N}{\mathbb{N}}
\renewcommand{\P}{\mathbb{P}}
%% Document
\begin{document}

%% Kopregel
\title{Differentiaalvergelijkingen Huiswerk 3}
\author{Wouter Rienks, 11007745}
\maketitle

%% Opgave
% Stel de teller in als je niet bij opgave 1 wil beginnen. 
% De teller telt automatisch door. Je gebruikt setcounter dus alleen als nummers worden overgeslagen.
% \setcounter{opgave}{0}

\textbf{E6.4 a)} Schrijf de oplossing voor het probleem
$\dot{x} = -x + \e x^3, x(0) = 2$
in de vorm
\[
x(t, \e) = x_0(t) + \e x_1(t) + O(\e^2)
\] en bepaal $x_0$ en $x_1$.
\begin{oplossing}
Invullen van $x(t, \e) = x_0(t) + \e x_1(t) + O(\e^2)$ geeft
\[
\dot{x_0} + \e \dot{x_1} + O(\e^2) = -x_0 -\e x_1 + \e x_0^3 + O(\e^2).
\] 
Dit geldt ook voor $\e = 0$, dus $\dot{x_0} = -x_0$. Dit heeft oplossingen $c e^{-t}$ voor $c \in \R$. Omdat $x_0(0)$ = $x(0, 0) = 2$ volgt $c = 2$. Door dit in te vullen vinden we
\[
-2e^{-t} + \e x_1(t) + O(\e^2) = -2e^{-t} - \e x_1(t)+ \e 8e^{-3t} + O(\e^2).
\]
Voor $\e \neq 0$ kunnen we delen door $\e$, dus
\[
\dot{x_1}(t) = -x_1(t) + 8e^{-3t}.
\]
De oplossing voor deze inhomogene vergelijking wordt gegeven door
\begin{align*}
x_1(t) &= e^{-t} \cdot 0 + e^{-t} \int_0^te^s 8 e^{-3s} \diff s \\
&= e^{-t} \int_0^t 8 e^{-2s} \diff s \\
&= e^{-t} \bigg[-\frac{1}{2} 8 e^{-2s}\bigg]_{s = 0}^{s = t} \\
&= -4e^{-3t} + 4e^{-t}.
\end{align*}
Dus we vinden
\[
x(t, \e) = 2e^{-t} + 4\e e^{-t} - 4\e e^{-3t} + O(\e^2).
\]
\end{oplossing}
\textbf{3.1.6)} Laat $\phi(t, x_0)$ de flow van de differentiaalvergelijking $\dot{x} = p(x)$, waar $p(x)$ een polynoom van graad groter dan $1$ is. Neem aan dat $p(x) > 0$ voor alle $x > a$. Toon aan dat $\phi(t, x_0)$ naar oneindig gaat in eindige tijd voor alle $x_0 > a$. 
\begin{oplossing}
We tonen eerst aan dat er $\epsilon > 0$ en $n \in \N$ bestaan waarvoor geldt $p(x) > \epsilon (x - a)^n$ voor alle $x > a$. Daarna lossen we de differentiaalvergelijking $\dot{y} = \epsilon(y - a)^n$ op met beginconditie $y(0) = x_0$, en tonen we aan dat y in eindige tijd naar oneindig gaat. Ten slotte tonen we aan dat als $x(0) = y(0)$ en voor alle $t$ met $x(t) = y(t)$ geldt $\dot{x}(t) > \dot{y}(t)$, dan $x > y$ voor alle $t > 0$ (analoog aan het bewijs van 5.1b). Hieruit concluderen we dat voor een oplossing $x$ geldt $x(t) > y(t)$ voor alle $t$, en daaruit volgt dat $x$ ook in eindige tijd naar oneindig moet gaan. 

Laat $n = \deg(p)$. Dan willen we dus een $\epsilon$ vinden zodat $\frac{p(x)}{(x - a)^n} > \epsilon$ voor alle $x > a$. 
Eerst bewijzen we dat $\lim_{x \to a+}\frac{p(x)}{(x - a)^n} > 0$. Deze limiet bestaat, want onder en boven staan enkel polynomen (al kan hij wel $\pm \infty$ zijn). Deze limiet is duidelijk $\geq 0$, want noemer en teller zijn altijd positief. Als deze limiet 0 is, dan geldt $p(x) \to 0$, want $(x - a)^n \to 0$. Door $n$ keer de stelling van L'hospital toe te passen vinden we dat $\lim_{x \to a+} \frac{p^{(n)}(x)}{n!} = 0$, dus $p^{(n)} \equiv 0$ (want $p^{(n)}$ heeft graad 0). Maar de $n$-de afgeleide van $p$ is precies $n! \cdot m$, waar $m$ de kopcoëfficient van $p$ is. Dus $m = 0$, maar dan $\deg(p) < n$. Tegenspraak, dus $\lim_{x \to a+} \frac{p(x)}{(x - a)^n} > 0$, en in het bijzonder is er een $\delta > 0$ zodat $p(x) > \epsilon_0$ voor $x \in (0, \delta]$. 

Duidelijk geldt dat als $k$ de kopcoëfficient van $p$ is, dat $\lim_{x \to \infty} \frac{p(x)}{(x - a)^n} = k$, want $p(x) = kx^n + O(x^{n - 1})$. Uit deze laatste afschatting volgt ook dat $k > 0$, omdat als $k < 0$ $p$ op den duur negatief wordt, maar $p$ is strikt positief. Dus er is een $M$ zodat $\frac{p(x)}{(x - a)^n} > \frac{k}{2}$ als $x > M$. $[\delta, M]$ is een compacte verzameling waarop $\frac{p(x)}{(x - a)^n}$ continu is, dus $\frac{p(x)}{(x - a)^n}$ neemt hier een minimum $\epsilon_1 > 0$ aan. Laat $\epsilon = \min\{\epsilon_0, \frac{\epsilon_1}{2}, \frac{k}{2}\}$. Dan geldt $\frac{p(x)}{(x - a)^n} > \epsilon$, en dus $p(x) > \epsilon (x - a)^n$ voor $x \in (a, \infty)$.

We lossen nu $\dot{y} = \epsilon(y - a)^n$ met beginconditie $y(0) = x_0 > a$ op door scheiding van variabelen toe te passen (merk op dat gegeven is $n > 1$)
\begin{align*}
\dot{y} &= \epsilon(y - a)^n \\
\int \frac{1}{(y - a)^n} \dot{y} \diff t &= \int \epsilon \diff t + C\\
\int \frac{1}{(y - a)^n} \diff y &= \epsilon t + C\\
\int \frac{1}{(y - a)^n} \diff y &= \epsilon t + C\\
\frac{1}{1 - n} \frac{1}{(y - a)^{n - 1}} &= \epsilon t + C.
\end{align*}
Door $t = 0$ in te vullen vinden we 
\[
\frac{1}{1 - n} \frac{1}{(x_0 - a)^{n - 1}} = C.
\]
Hieruit volgt direct
\[
y(t) = a + \bigg(\frac{1}{(1 - n)\epsilon t + \frac{1}{(x_0 - a)^{n - 1}}}\bigg)^{\frac{1}{n - 1}}.
\]
Omdat $x_0 > a$ volgt dat $\frac{1}{(x_0 - a)^{n - 1}} > 0$. $(1 - n) < 0$, dus $(1 - n)\epsilon t + \frac{1}{(x_0 - a)^{n - 1}}$ gaat in eindige tijd naar $0$, waardoor $y(t)$ in eindige tijd naar $\infty$ gaat.

Stel nu dat $x$ niet binnen eindige tijd naar oneindig gaat. $y$ gaat dat wel, dus laat $c > x_0$ zodat $\lim_{t \to c-} y(t) = +\infty$. Definieer 
\[
S = \sup\{t < c \mid x(t) = y(t)\}
\]
Dan is $S$ gedefiniëerd, want $x(0) = y(0)$. $S < c$, want anders geldt dat $\lim_{t \to c} x(t) = \lim_{t \to c} y(t) = +\infty$, omdat $x(t) = y(t)$ voor $t$ willekeurig dicht bij $c$. Voor $t \in (S, c)$ geldt duidelijk $x(t) \neq y(t)$. Als $x(t) > y(t)$ voor $t \in (S, c)$ volgt vanwege de tussenwaardestelling dat er een $\hat{t} \in (S, c)$ bestaat zodat $x(\hat{t}) = y(\hat{t})$, wat niet kan omdat $\hat{t} > S$. Dus $x(t) < y(t)$ voor alle $t \in (S, c)$, en
\[
\dot{x}(S) = \lim_{h \to 0+} \frac{x(S + h) - x(S)}{h} \leq \lim_{h \to 0+} \frac{y(S + h) - y(S)}{h} = \dot{y}(S)
\]
Maar $\dot{x}(S) = p(x(S)) > \epsilon(y(S) - a)^n = \dot{y}(S)$. Dus $x$ gaat wel in eindige tijd naar oneindig.

\end{oplossing}
\end{document}
