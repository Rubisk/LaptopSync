\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{bbm}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{Id}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\GL}{GL}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\expo}{exp}
\newcommand{\ii}{\mathbbm{1}}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\HH}{\mathbb{H}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand{\twomatrix}[4]{\left(\begin{smallmatrix} #1 & #2 \\ #3 & #4 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Differential Geometry: Exercise Sheet 6}
\author{Wouter Rienks, 11007745}
\maketitle

\begin{opghand}[1]
Show that the tangent bundle $TM \xrightarrow{\pi} M$ and the cotangent bundle $T^*M \xrightarrow{\pi} M$ of a Riemannian manifold are isomorphic.
\end{opghand}
\begin{oplossing}
Let $g$ be a Riemannian metric on $TM$. We then define the map $\Phi: TM \to T^*M: (p, v) \to (p, \Phi_p(v))$ where
\[
\Phi_p(v): T_pM \to \RR: w \mapsto g_p(v, w).
\]
Clearly $\pi \circ \Phi((p, v)) = \pi((p, \Phi_p(v))) = p$, so $\pi \circ \Phi = \pi$. For fixed $p$, the map $\Phi_p$ is a vector space isomorphism, since $g_p$ is a scalar product on $T_pM$, hence $\Phi_p$ is simply the Riesz isomorphism.

Since $g$ is a section, $\Phi$ depends smoothly on $p$. Since $g_p$ is linear $\Phi$ depends linearly on $v$, hence $\Phi$ depends smoothly on $v$. Therefore $\Phi$ is smooth. 

Since $\Phi_p$ is a vector space isomorphism, it is invertible. Therefore we see that the map $\Phi^{-1}: T^*M \to TM: (p, \phi) \mapsto (p, \Phi_p^{-1}(\phi))$ is an inverse for $\Phi$. Note that $\Phi_p^{-1}(\phi)$ is linear in $\phi$, hence depends smoothly on $\phi$. Since matrix inversion is a smooth map (the coefficients of the inverse are rational functions in the original coefficients), the map $p \to \Phi_p^{-1}$ is smooth (since $p \to \Phi_p$ was smooth). Hence $\Phi^{-1}$ depends smoothly on $p$ as well, and therefore $\Phi^{-1}$ is smooth. 

Thus $\Phi$ is a diffeomorphism, and hence $TM$ and $T^*M$ are isomorphic.
\end{oplossing}

\begin{opghand}[2]
Let $(M, g)$ be a Riemannian manifold, and let $f: N \to M$ be a smooth map between smooth manifolds which is an immersion. Prove that $N$ can be equipped with a Riemannian metric $h$ such that the length of any path $\gamma$ on $N$ (measured using $h$) agrees with the length of the path $\delta:= f \circ \gamma$ on $M$ (measured using $g$). Does this imply that $d_h(p, q) = d_g(f(p), f(q))$ for all $p, q \in N$. 
\end{opghand}
\begin{oplossing}
Define $h_p(v_p, w_p) := g_{f(p)}(T_pf(v_p), T_pf(w_p))$. We show that $(N, h)$ is a Riemannian metric. Since $T_pf$ is a linear map, and $g_{f(p)}$ is bilinear, $h_p$ is bilinear. Hence $h_p \in T_p^*N \otimes T_p^*N$. Since $g_p$ depends smoothly on $p$, and $f$ and $T_f$ are smooth in $p$, we see that $h_p$ depends smoothly on $p$, hence $h$ is a section of $T^*N \otimes T^*N$. 

Now $h_p$ is clearly symmetric, since $g_{f(p)}$ is symmetric, therefore
\[
h_p(v_p, w_p) = g_{f(p)}(T_pf(v_p), T_pf(w_p)) = g_{f(p)}(T_pf(w_p), T_pf(v_p)) = h_p(w_p, v_p). 
\]

To see that $h_p$ is positive definite, note that if $v_p \neq 0$, then $T_pf(v_p) \neq 0$, since $T_pf$ is injective (since $f$ is an immersion). Hence 
\[
h_p(v_p, v_p) = g_p(T_pf(v_p), T_pf(v_p)) > 0. 
\]

Thus $h$ is a Riemannian metric. Now if $\gamma: [a, b] \to N$ is any curve on $N$, then
\begin{align*}
l_h(\gamma) &= \int_a^b \sqrt{h_{\gamma(t)}(\dot \gamma(t), \dot \gamma(t))} \ dt \\
&= \int_a^b \sqrt{g_{f \circ \gamma(t)}(T_{\gamma(t)}f(\dot \gamma(t)), T_{\gamma(t)}f(\dot \gamma(t)))} \ dt \\
&= \int_a^b \sqrt{g_{f \circ \gamma(t)}(\dot{(f \circ \gamma)}(t), \dot{(f \circ \gamma)}(t))} \ dt &(\text{chain rule}) \\
&= \int_a^b \sqrt{g_{\delta(t)}(\dot{\delta}(t), \dot{\delta}(t))} \ dt \\
&= l_g(\delta),
\end{align*}
as desired.

However, $d_h(p, q) = d_g(f(p), f(q))$ does not always hold, consider $N = \RR$ and $M = S^1$. Equip $M$ with a Riemannian metric by using the standard Riemannian metric after embedding $M$ in $\RR^2$ in the obvious way. Then define $f: N \to M: t \mapsto e^{i t}$.  Then $f$ is easily seen to be an immersion (since the linear map $T_tf: c \to  ie^{i t} \cdot c $ is injective). However, since $f(0) = f(2\pi)$, we have $d_g(f(0), f(2\pi)) = 0$. Yet $d_h(0, 2\pi) = 2\pi$ (since $h$ can easily be verified to be the standard metric on $T\RR$).
\end{oplossing}
\begin{opghand}[3]
Compute the induced Riemannian metric on $S^2 \subset \RR^3$ in terms of spherical coordinates $(\varphi, \theta)$ defined by 
\[
(x_1, x_2, x_3) = (\sin \varphi \cdot \cos \theta, \sin \varphi \cdot \sin \theta, \cos \varphi) \in S^2 \subset \RR^3. 
\]
\end{opghand}
\begin{oplossing}
Call the above chart $f$. We calculate
\[
Tf = \begin{pmatrix}
\cos \varphi \cos \theta & -\sin \varphi \sin \theta \\
\cos \varphi \sin \theta & \sin \varphi \cos \theta \\
-\sin \varphi & 0
\end{pmatrix}.
\]
Hence 
\begin{align*}
Tf\(\frac{\partial}{\partial \varphi}\) &= (\cos \varphi \cos \theta, \cos \varphi \sin \theta, -\sin \varphi), \\
Tf\(\frac{\partial}{\partial \theta}\) &= (-\sin \varphi \sin \theta, \sin \varphi \cos \theta, 0).
\end{align*}
We compute (using the definition as in exercise 2)
\begin{align*}
g_{\varphi, \theta}\(\frac{\partial}{\partial \varphi}, \frac{\partial}{\partial \varphi}\) &= 1, \\
g_{\varphi, \theta}\(\frac{\partial}{\partial \theta}, \frac{\partial}{\partial \theta}\) &= \sin(\varphi)^2, \\
g_{\varphi, \theta}\(\frac{\partial}{\partial \varphi}, \frac{\partial}{\partial \theta}\) &= 0.
\end{align*}
Since we know $g_{\varphi, \theta}$ is bilinear and symmetric, we thus conclude
\[
g_{\varphi, \theta} = d\varphi \otimes d\varphi + \sin(\varphi)^2 d\theta\otimes d\theta.
\]
\end{oplossing}
\begin{opghand}[4]
Let $v_1, v_2, v_3$ be three vectors in $\RR^3$. Prove that the (standard) volume of
\[
P(v_1, v_2, v_3) := \{t_1v_1, t_2v_2, t_3v_3 \mid 0 \leq t_1, t_2, t_3 \leq 1\} \subset \RR^3
\]
is given by the modulus of the determinant of the corresponding $3 \times 3$-matrix,
\[
\text{vol}(P(v_1, v_2, v_3)) = |\det(v_1, v_2, v_3)|.
\]
\end{opghand}
\begin{oplossing}
We recall from linear algebra that any 3x3 matrix can be written as a product of elementary matrices $P_{i,j}$, $M_{i,\lambda}$ and $A_{i, j, \lambda}$ (where $i,j \in \{1, 2, 3\}$ and $\lambda \in \RR$). Here $P_{i,j}$ permutes row $i$ and $j$, $M_{i,\lambda}$  multiplies row $i$ by $\lambda \neq 0$, and $A_{i,j,\lambda}$ adds row $j$ precisely $\lambda$ times to row $i$. For example
\[
P_{1, 2} = \begin{pmatrix}
0 & 1 & 0 \\
1 & 0 & 0 \\
0 & 0 & 1
\end{pmatrix}, \quad
M_{1, \lambda} = \begin{pmatrix}
\lambda & 0 & 0 \\
0 & 1 & 0 \\
0 & 0 & 1
\end{pmatrix}, \quad
A_{2, 3, \lambda} = \begin{pmatrix}
1 & 0 & 0 \\
0 & 1 & \lambda \\
0 & 0 & 1
\end{pmatrix}.
\]
Now note that if $L$ is a linear transformation from $\RR^3$ to $\RR^3$ and $U$ is a measurable set, then $L(U)$ is also measurable. Hence $L(\vol): A \mapsto \vol(L(A))$ is a measure on $\RR^3$. Since $L(\vol)$ is translation-invariant, we know from measure theory that $L(\vol) = L([0, 1]^3)\cdot\vol$. 

So if we now set $V$ to be the linear transformation mapping $e_i \to v_i$, then we can decompose $V = \prod_{i=1}^k V_i$ where each of the $V_i$ is an elementary matrix. Since $P(v_1, v_2, v_3)$ is simply $V([0, 1]^3)$, we have (here $I^3 = [0,1]^3$)
\begin{align*}
\vol(P(v_1, v_2, v_3)) &= \vol(V(I^3) = \vol(\prod_{i=1}^kV_i(I^3)) \\
&= \vol(V_1(I^3))\vol(\prod_{i=2}^kV_i(I^3)) = \cdots = \prod_{i=1}^k\vol(V_i(I^3)).
\end{align*}
Since the determinant is a multiplicative function, it thus suffices to show $\det(A) = \vol(A(I^3))$ for any elementary matrix $A$.

If $A$ is of the form $P_{i,j}$, we have $\det(P_{i,j}) = -1$. Since clearly $P_{i,j}(I^3) = I^3$ (all we did was flip the cube), we thus have $\vol(P_{i,j}(I^3)) = 1 = |\det(P_{i,j})| $.

If $A$ is of the form $M_{i, \lambda}$, then $M_{i, \lambda}(I^3) = [0, \lambda_1]\times[0, \lambda_2]\times[0, \lambda_3]$, where $\lambda_j = \lambda$ if $j=i$ and $\lambda_j=1$ otherwise. From this we can immediately see that $\vol(M_{i, \lambda}) = |\lambda| = |\det(M_{i, \lambda})|$. 

Finally, if $A$ is of the form $A_{i, j, \lambda}$, then we may assume without loss of generality that $i = 1, j = 2$ (otherwise relabel all the axes in the proof). Then 
\[
A_{1, 2, \lambda}(I^3) = \{t_1 + \lambda t_2, t_2, t_3 \mid 0 \leq t_i \leq 1\}. 
\]
Hence
\[
\vol(A_{1, 2, \lambda}) = \int_{\RR^3} \ii_{A_{1, 2, \lambda}(I^3)} \stackrel{\text{(Fubini)}}{=} \int_0^1 \int_{\lambda t_2}^{1 + \lambda t_2} \int_0^1 1 dt_3dt_1dt_2 = 1^3 = 1,
\]
and obviously $|\det(A_{1, 2, \lambda})| = 1$, so $|\det(A_{1, 2, \lambda})| = \vol(A_{1, 2, \lambda})$ as desired.
\end{oplossing}
\newpage
\begin{opghand}[5]
Show that for a multilinear map $\omega: V \times \dots \times V \to \RR$ ($V$ is a finite-dimensional vector space) the
following are equivalent:
\begin{enumerate}[i)]
\item $\omega$ is alternating,
\item $\omega(v_1, \dots, v_n) = 0$ for all $(v_1, \dots, v_n)$ with $v_i = v_j$ for some $i \neq j$. 
\item $\omega(v_{\sigma(1)}, \dots, v_{\sigma(n)}) = \sgn(\sigma) \cdot \omega(v_1, \dots, v_n)$ for all $(v_1, \dots, v_n)$ and $\sigma \in S_n$. 
\end{enumerate}
\end{opghand}
\begin{oplossing}
We show (i) $\iff$ (ii) and (ii) $\iff$ (iii).

For (i) $\implies$ (ii), note that if $v_i = v_j$ for some $i \neq j$, then $(v_1, \dots, v_n)$ is linearly dependent (since $v_i - v_j = 0$ provides a nontrivial relationship). So if $\omega$ is alternating, we thus have $\omega(v_1, \dots, v_n) = 0$ if $v_i = v_j$ for some $i \neq j$.

We now show (ii)  $\implies$  (i). So pick any linear dependent set of vectors $(v_1, \dots, v_n)$. Then there exists a nontrivial relationship $\sum_{i=1}^n \alpha_iv_i = 0$. Without loss of generality we may assume $\alpha_1 \neq 0$ (since some $\alpha_i$ must be nonzero), so by setting $\beta_i = -\alpha_i/\alpha_1$ we obtain
\[
v_1 = \sum_{i=2}^n \beta_iv_i.
\]
Hence by bilinearity we have, assuming (ii)
\[
\omega(v_1, \dots, v_n) = \sum_{i=2}^n\beta_i \omega(v_i, v_2, \dots, v_n) \stackrel{\text{by (ii)}}{=} 0,
\]
so (ii) implies (i).

To show (ii)  $\implies$  (iii), first note that $\sgn$ is a homomorphism, hence
\[
\omega(v_{\sigma \circ \tau(1)}, \dots, v_{\sigma \circ \tau(n)}) = \sgn(\sigma)\omega(v_{\tau(1)}, \dots, v_{\tau(n)}) = \sgn(\sigma \circ \tau) \omega(v_1, \dots, v_n). 
\]
Since any permutation can be written as a product of 2-cycles, it suffices to show the result holds for any $\sigma$ that only exchanges $v_i$ and $v_j$ (with $i \neq j$). We write $[x, y]$ for the vector that is $v = (v_1, \dots, v_n)$ in all coordinates not in $\{i, j\}$, but at has $x$ as $i$'th coordinate and $y$ as $j$'th coordinate. Then using bilinearity of $\omega$ (and assuming (ii)) we see
\begin{align*}
0 &= \omega([v_i + v_j, v_i + v_j]) \\
&= \omega([v_i, v_i]) + \omega([v_i, v_j]) + \omega([v_j, v_i]) + \omega([v_j, v_j]) \\
&= 0 + \omega([v_i, v_j]) + \omega([v_j, v_i]) + 0.
\end{align*}
Hence (note that the sign of a 2-cycle is $-1$)
\[
\omega(v_1, \dots, v_n) = \omega([v_i, v_j]) = -\omega([v_j, v_i]) = \sgn(\sigma)\omega(v_{\sigma(1)}, \dots, v_{\sigma(n)}),
\]
as desired.

Finally we show (iii) $\implies$ (ii). Assume (iii) holds and suppose $v_i = v_j$ for some $i \neq j$. Then if $\sigma$ permutes only $i$ and $j$, we have $\sgn(\sigma) = -1$ and $(v_1, \dots, v_n) = (v_{\sigma(1)}, \dots, v_{\sigma(n)})$, hence
\[
\omega(v_1, \dots, v_n) = -\omega(v_{\sigma(1)}, \dots, v_{\sigma(n)}) = -\omega(v_1, \dots, v_n),
\]
so $\omega(v_1, \dots, v_n) = 0$ as desired.
\end{oplossing}

\begin{opghand}[6]
Compute for all three differential forms $\omega^1, \omega^2, \omega^3$ on $M = \RR^3$ the corresponding alternating multilinear form $\omega^1_p, \omega^2_p, \omega^3_p$ at each point $p = (x_1, x_2, x_3) \in \RR^3$.
\end{opghand}
\begin{enumerate}[a)]
\item $\omega^1 = a_1(p)\cdot dx_1 + a_2(p)\cdot dx_2 + a_3(p)\cdot dx_3$.
\begin{oplossing}
Recall that $dx_i$ is the dual basis voor $T_pM$. Since $w^1_p$ is a 1-linear map from $T_p(M) \cong \RR^3$ to $\RR$, we take $v = (v_1, v_2, v_3)^T \in T_pM$ and calculate
\begin{align*}
\omega^1_p(v) &= a_1(p) \cdot dx_1(v) + a_2(p) \cdot dx_2(v) + a_1(p) \cdot dx_2(v) \\
&= a_1(p)v_1 + a_2(p)v_2 + a_3(p)v_3.
\end{align*}
\end{oplossing}

\item $\omega^2 = a_{12}(p)\cdot dx_1\wedge dx_2 +  a_{23}(p)\cdot dx_2\wedge dx_3 +  a_{13}(p)\cdot dx_1\wedge dx_3$.
\begin{oplossing}
Recall that given vectors $v, w$ we have by construction of the duality pairing
\[
dx_i\wedge dx_j(v \wedge w) = \det \twomatrix{dx_i(v)}{dx_i(w)}{dx_j(v)}{dx_j(w)} = dx_i(v)dx_j(w) - dx_j(w)dx_i(v).
\]
Hence we calculate (given $v = (v_1, v_2, v_3)^T, w = (w_1, w_2, w_3)^T \in T_p(M)$)
\begin{align*}
\omega^2_p(v \wedge w) &= a_{12}(p)\cdot dx_1\wedge dx_2(v \wedge w) +  a_{23}(p)\cdot dx_2\wedge dx_3(v \wedge w) \\
& \qquad +  a_{13}(p)\cdot dx_1\wedge dx_3(v \wedge w) \\
&= a_{12}(p)(v_1w_2 - w_1v_2) +  a_{23}(p)(v_2w_3 - w_2v_3) \\
& \qquad +  a_{13}(p)(v_1w_3 - w_1v_3).
\end{align*}
\end{oplossing}

\item $\omega^3 = a_{123}(p)\cdot dx_1\wedge dx_2 \wedge dx_3$.
\begin{oplossing}
Given $u, v, w \in T_p(M)$, we calculate
\begin{align*}
\omega^3_p(v\wedge w\wedge u) &= a_{123}(p)\cdot dx_1\wedge dx_2 \wedge dx_3(v \wedge w \wedge u) \\
&= a_{123}(p)\det \begin{pmatrix}
dx_1(v)& dx_1(w) & dx_1(u) \\
dx_2(v)& dx_2(w) & dx_2(u) \\
dx_3(v)& dx_3(w) & dx_3(u)
\end{pmatrix} \\
&= a_{123}(p)\det \begin{pmatrix}
v_1 & w_1 & u_1 \\
v_2 & w_2 & u_2 \\
v_3 & w_3 & u_3 
\end{pmatrix} \\
&= a_{123}(p)(v_1(w_2u_3 - w_3u_2) - w_1(v_2u_3 - v_3u_2)  \\
&  \qquad + u_1(v_2w_3 - v_3w_2)).
\end{align*}
\end{oplossing}
\end{enumerate}
\end{document}