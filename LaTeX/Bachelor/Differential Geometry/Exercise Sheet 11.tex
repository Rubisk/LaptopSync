\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{bbm}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{Id}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\GL}{GL}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\expo}{exp}
\newcommand{\ii}{\mathbbm{1}}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\HH}{\mathbb{H}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand{\twomatrix}[4]{\left(\begin{smallmatrix} #1 & #2 \\ #3 & #4 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Differential Geometry: Exercise Sheet 11}
\author{Wouter Rienks, 11007745}
\maketitle

\begin{opghand}[1]
Let $M \subset \RR^n$ be a submanifold of dimension $n - 1$. 
\end{opghand}
\begin{enumerate}[i)]
\item Show that $M \subset \RR^n$ is orientable if and only if there exists a unit normal vector field $\nu$ on $M$. More precisely, show that the choice of a unit normal vector field fixes an orientation on $M$.
\begin{oplossing}
Suppose there exists a unit normal vector field $\nu$ on $M$. We fix an orientation on $M$ by calling a basis of ordered tangent vectors \linebreak $(v_1, \dots, v_{n - 1}) \in T_pM$ oriented if and only if $\det(\nu(p), v_1, \dots, v_{n - 1}) > 0$. To see that this defines an orientation, note that for any $p \in M$ we can pick a chart $\phi: V \subset R^{n - 1} \to M$ such that $\phi(0) = p$, with $V$ some open (connected) subset in $\RR^n$ containing $0$. If we denote $e_1, \dots, e_{n - 1}$ for the standard basis in $\RR^{n - 1}$, then $\phi_i(s) := T_s\phi(e_i)$ is clearly a smooth basis for the tangent space on $V$. Now $\det(\nu(s), \phi_1(s), \dots, \phi_{n - 1}(s))$ is nonzero for all $s \in V$ since $\phi_i$ is a basis for the tangent space and $\nu$ is orthogonal to the tangent space. Since $\det$ and all bases are smooth and $\det$ is everywhere nonvanishing (and $\phi(V)$ is connected), it is either everywhere positive or everywhere negative. If it is everywhere positive then $(\phi_1, \dots, \phi_{n - 1})$ is a vector field that is an oriented basis in every point $s \in V$, otherwise take $(-\phi_1, \phi_2, \dots, \phi_{n - 1})$. This shows our orientation is well defined.

Now suppose we have fixed an orientation on $M$. For any point $p \in M$, pick some chart $\phi: V \subset \RR^n \to U \subset \RR^n$ such that $\phi(V \cap \RR^{n - 1}) = M \cap U$, $\phi(0) = p$ and $0 \in V$, with $U \cap M$ connected. Since $\phi$ is a chart, $T_s\phi$ is injective for any $s \in  V$. Since $T_s\phi$ maps $T_s(\RR^{n - 1})$ onto $T_{\phi(s)}(M)$, the image of the $n$-th standard basis vector $T_s\phi\(\frac{\partial}{\partial x_n}\)$ is not contained in $T_{\phi(s)}(M)$. Now let $\pi: T_q(\RR^n) \to N_q(M)$ be the projection map, and let 
\[
u: N_q(M) \setminus \{0\} \to N_q(M): \qquad x \mapsto x / ||x||
\]
the normalization maps for any $q \in M$. Then clearly $\pi$ and $u$ are smooth. Since $T_s\phi\(\frac{\partial}{\partial x_n}\) \not \in T_{\phi(s)}(M)$, we have $\pi\(T_s\phi\(\frac{\partial}{\partial x_n}\)\) \neq 0$, hence 
\[
X_\phi(p) = u \circ \pi\(T_s\phi\(\frac{\partial}{\partial x_n}\)\)
\]
is a well defined vector field on $U \cap M$ (it is smooth since $\phi, \pi, u$ are smooth). Now for any oriented basis $v_1, \dots, v_{n - 1} \in T_p(M)$, we either have $\det(X_\phi(p), v_1, \dots, v_{n - 1}) > 0$ or $< 0$. Since $U \cap M$ is connected and $\det$ is smooth, by the intermediate value theorem this choice is uniform accross $U \cap M$. If it is positive, let $\nu_\phi(p) = X_\phi(p)$, otherwise $\nu_\phi(p) = -X_\phi(p)$. Then $\nu_\phi(p)$ is a well defined vector field on $U \cap M$, and smooth since the choice was uniform. If we can show it does not depend on the choice of chart $\phi$ than we have found a unit vector field $\nu$ on $M$. Note that given two charts $\phi, \psi$, both $\nu_\phi(p)$ and $\nu_\psi(p)$ are unit vectors contained in $N_p(M)$. Since $N_p(M)$ is one-dimensional, we either have $\nu_\phi(p) = \nu_\psi(p)$ or $\nu_\phi(p) = -\nu_\psi(p)$. However, since both $\nu_\phi$ and $\nu_\psi$ satisfy $\det(\nu(p), v_1, \dots, v_{n - 1}) > 0$, they must be identical, and $\nu(p) = \nu_\phi(p)$ is a well defined unit normal vector field.
\end{oplossing}
\item Let $f: \RR^n \to \RR$ be a smooth function having $0 \in \RR$ as a regular value. Use i) to show that $M: f^{-1}(0) \subset \RR^n$ is orientable.
\begin{oplossing}
For any $p \in M$, we again pick some chart $\phi: V \subset \RR^n \to U \subset \RR^n$ such that $\phi(V \cap \RR^{n - 1}) = M \cap U$, $\phi(0) = p$ and $0 \in V$. As in (i), the vector field $X_\phi(p)$ is once again well defined and smooth on $U \cap M$. Note that for any $s \in M$ and $v \in T_s(M)$, we must have $T_s f (v) = 0$ (this can easily be seen using the curve definition of tangent vectors, since $\gamma(t) \subset M$ implies $f \circ \gamma(t) = 0$). However, since $s \in M = f^{-1}(0)$ and $0$ is a regular value of $f$, the map $T_s(f): T_s(\RR^n) \to T_{f(s)}(\RR) \cong  \RR$ is surjective. Since $T_s(f)$ is linear, and $T_s(\RR^n) = T_s(M) \oplus N_s(M) = T_s(M) \oplus \langle X(s) \rangle$, we must have $T_f(X_\phi(s)) \neq 0$. If we force $U \cap M$ to be connected again, we again either have $T_f(X_\phi(s)) > 0$ or $T_f(X_\phi(s)) < 0$ uniformly on $U \cap M$. Now define $\nu_\phi(s)$ to be $X_\phi(s)$ if $T_f(X_\phi(s)) > 0$, and $-X_\phi(s)$ otherwise. Since the choice was uniform and $X_\phi$ was smooth, $\nu_\phi(s)$ is a smooth unit normal vector field on $U \cap M$. If we can again show that $\nu_\phi$ does not depend on the choice of chart $\phi$, then we have found a unit normal vector field on all of $M$. But once again given charts $\phi, \psi$, since $N_p(M)$ is onedimensional and $\nu_\phi, \nu_\psi$ are both unit vectors, we either have $\nu_\phi = \nu_\psi$ or $\nu_\phi = -\nu_\psi$. The second will never occur, since $T_f(\nu_\phi) = -T_f(-\nu_\phi)$ and both $T_f(\nu_\phi)$ and $T_f(\nu_\psi)$ are positive by construction. Hence $\nu$ is well defined.
\end{oplossing}
\end{enumerate}
\begin{opghand}[2]
Let $M \subset \RR^n$ be an oriented $(n - 1)$-dimensional submanifold with unit normal vector field $\nu$ as in exercise 1.
\begin{enumerate}[i)]
\item Explain geometrically why the contraction $\nu \lrcorner(dx_1 \wedge \dots \wedge dx_n)$ of $\nu$ with the standard volume form $dx_1 \wedge \dots dx_n$ on $\RR^n$ defines the canonical volume form $dvol_M \in \Omega^{n - 1}(M)$ on $M$ and prove that at every $p \in M$ it is given by
\[
\sum_{i=1}^n (-1)^{i - 1}\nu_i(p) dx_1 \wedge dx_{i - 1} \wedge dx_{i + 1}\wedge \dots \wedge dx_n,
\]
with $\nu = (\nu_1(p), \dots, \nu_n(p)) \in \in T_p M^\perp \subset RR^n$.
\begin{oplossing}
To see that it defines the canonical volume form, note that for a set of tangent vectors $v_1, \dots, v_{n - 1} \in T_pM$, the number 
\[
dx_1 \wedge \dots \wedge dx_n(\nu, v_1, \dots, v_{n - 1})
\] is simply the $n$-dimensional volume of the parallellepipid spanned by the vectors $(\nu, v_1, \dots, v_{n - 1})$. Since $\nu$ has length 1, the $(n - 1)$-dimensional volume of the parallellepipid spanned by $v_1, \dots, v_{n - 1}$ is simply the $n$-dimensional volume of the parallellepipid spanned by $(\nu, v_1, \dots, v_n)$, which is precisely $dx_1 \dots dx_n(\nu, v_1, \dots, v_{n - 1})$ as desired.

Finally, if we write $\nu = \sum_{i = 1}^n \nu_i \frac{\partial}{\partial x_i}$, then, using the properties of $\wedge$, we derive for any set of vectors $v_1, \dots, v_{n - 1} \in T_pM$ the identity
\begin{align*}
\nu &\lrcorner(dx_1 \wedge \dots \wedge dx_n)(v_1, \dots, v_{n - 1}) \\
&= dx_1 \wedge \dots \wedge dx_n(\nu, v_1, \dots, v_{n - 1}) \\
&= \sum_{i=1}^n  dx_1 \wedge \dots \wedge dx_n(\nu_i \frac{\partial}{\partial x_i}, v_1, \dots, v_{n - 1}) \\
&= \sum_{i=1}^n  (-1)^{i - 1}dx_1 \wedge \dots \wedge dx_n(v_1, \dots,  v_{i - 1}, \nu_i \frac{\partial}{\partial x_i}, v_i, \dots, v_{n - 1}) \\
&= \sum_{i=1}^n (-1)^{i - 1}\nu_i(p) dx_1 \wedge dx_{i - 1} \wedge dx_{i + 1}\wedge \dots \wedge dx_n(v_1, \dots, v_{n - 1}),
\end{align*}
which shows it is the desired form.
\end{oplossing}
\item Use the result of i) together with the statement of exercise 1 to prove that an $(n - 1)$-dimensional submanifold $M \subset \RR^n$ is orientable if and only if there exists a $(n - 1)$-form $\omega$ on $M$ with $\omega_p \neq 0$ for all $p \in M$. 
\begin{oplossing}
If $M$ is orientable, then by 1 we can pick a unit normal vector field $\nu$ on $M$. Clearly the form $\nu \lrcorner(dx_1 \wedge \dots \wedge dx_n)$ is nowhere vanishing on $M$, since by (i) it's the standard volume form.

Now if we have any such $\omega$, then we can write 
\[
\omega_p = \sum_{i = 1}^n (-1)^{i - 1}\omega_i(p)dx_1 \wedge dx_{i - 1} \wedge dx_{i + 1}\wedge \dots \wedge dx_n
\]
for some smooth functions $\omega_i: M \to \RR$. Define for any $p \in M$ the vector $v(p) = (\omega_1(p), \dots, \omega_n(p)) \in T_p(\RR^n)$, clearly $v$ is a smooth vector field. Then by (i) we have
\[
\omega_p = v_p \llcorner(dx_1 \wedge \dots \wedge dx_n).
\]
From this it follows immediately that $v_p \not \in T_p(M) \subset T_p(\RR^n)$, since if we pick any set $(u_1, \dots, u_{n - 1}) \in T_pM$, then the set $(v, u_1, \dots, u_{n - 1})$ is linearly dependent (since $\dim T_pM = n - 1$), which would imply 
\[
v_p \llcorner(dx_1 \wedge \dots \wedge dx_n)(u_1, \dots, u_{n - 1}) = 0
\]
for all sets of vectors $(u_1, \dots, u_{n - 1})$, but that would imply that we have \linebreak $v_p \llcorner(dx_1 \wedge \dots \wedge dx_n) = 0$, which is a contradiction since $\omega_p \neq 0$.

Now let $\pi: T_p(\RR^n) \to N_p(M)$ be the projection map, and let 
\[
u: N_p(M) \setminus \{0\} \to N_p(M): \qquad x \mapsto x / ||x||
\]
be the normalization map. Then $\pi(v)$ is a nowhere vanishing vector field on $M$, since $v(p) \not \in T_p(M)$ for all $p \in M$. Hence $X = u \circ \pi(v)$ is well defined. Then $X$ is smooth since both $\pi$ and $u$ are smooth, and $v$ is a smooth vector field, and per construction $X$ is a unit normal vector field on $M$, which is what we needed to construct.
\end{oplossing}
\end{enumerate}
\end{opghand}
\begin{opghand}[3]
Consider $S^2 = \partial B^3 \subset \RR^3$ with its canonical unit normal vector field $\nu$ given by $\nu(p) = p$, $p \in S^2$, and the corresponding canonical volume form $dvol_{S^2} := \nu \llcorner(dx_1 \wedge dx_2 \wedge dx_3) \in \Omega^2(S^2)$. Use Stokes's theorem to prove that for every vector field $X$ on $\RR^3$ we have
\[
\int_{S^2} \langle X, \nu \rangle dvol_{S^2} = \int_{B^3} \(\frac{\partial X_1}{\partial x_1} + \frac{\partial X_2}{\partial x_2} + \frac{\partial X_3}{\partial x_3}\) dx_1 \wedge dx_2 \wedge dx_3.
\]
\end{opghand}
\begin{oplossing}
Note that for any two linear independent tangent vectors vectors $v_1, v_2 \in T_pS^2$, the set $\nu, v_1, v_2$ is a basis for $T_p \RR^3$, and therefore we can find $\alpha, \beta, \gamma$ such that $X(p) = \alpha \nu + \beta v_1 + \gamma v_2$. We then see that
\begin{align*}
\langle X, \nu \rangle dvol_{S^2}(v_1, v_2) &= \langle \alpha \nu + \beta v_1 + \gamma v_2 , \nu \rangle dvol_{S^2}(v_1, v_2) \\
&= \alpha \cdot dvol_{S^2}(v_1, v_2) \\
&= \alpha \cdot dx_1\wedge dx_2 \wedge dx_3(\nu, v_1, v_2) \\
&= dx_1\wedge dx_2 \wedge dx_3(\alpha \nu + \beta v_1 + \gamma v_2, v_1, v_2) \\
&= X \llcorner (dx_1\wedge dx_2 \wedge dx_3),
\end{align*}
therefore the integrand on the left hand side is simply 
\[
X \llcorner (dx_1\wedge dx_2 \wedge dx_3) = X_1dx_2\wedge dx_3 - X_2dx_1\wedge dx_3 + X_3dx_1\wedge dx_2.
\]
The statement now follows immediately from Stokes's theorem, since the outward normal vector field $\nu$ gives $S^2$ the same orientation as it inherits from $B^3$ as boundary, and
\begin{align*}
d(X_1dx_2\wedge dx_3 - &X_2dx_1\wedge dx_3 + X_3dx_1\wedge dx_2) \\&= \(\frac{\partial X_1}{\partial x_1} + \frac{\partial X_2}{\partial x_2} + \frac{\partial X_3}{\partial x_3}\) dx_1 \wedge dx_2 \wedge dx_3,
\end{align*}
which is exactly the integrand we wanted on the right hand side.
\end{oplossing}
\begin{opghand}[4]
Let $X$ be a vector field on $\RR^n$. Assume that its flow $\phi_t: \RR^n \to \RR^n$ exists for all times $t \in \RR$ and assume that $\langle X, \nu \rangle \geq 0$ on $S^2 \subset \RR^3$, where $\nu$ denotes the canonical unit normal vector field on $S^2$ from exercise 3. Prove that
\[
\frac{\partial}{\partial t}\bigg \rvert_{t = 0} \vol(\phi_t(B^3)) \geq 0,
\]
where $\vol(\phi_t(B^3))$ denotes the standard volume of $\phi_t(B^3) \subset \RR^3$. 
\end{opghand}
\begin{oplossing}
By definition we have
\begin{align*}
\frac{\partial}{\partial t}\bigg \rvert_{t = 0} \vol(\phi_t(B^3)) &= \frac{\partial}{\partial t}\bigg \rvert_{t = 0} \int_{\phi_t(B^3)}dx_1\wedge dx_2 \wedge dx_3 \\
&= \frac{\partial}{\partial t}\bigg \rvert_{t = 0} \int_{B^3}\phi_t^*(dx_1\wedge dx_2 \wedge dx_3).
\end{align*}
Since $\overline{B^3}$ (with its boundary) is a compact set, the functions $\frac{\partial X_i}{\partial x_i}$ will attain some maximum $A_i$ at some point in $\overline{B^3}$. Hence $\left|\frac{\partial X_1}{\partial x_1} + \frac{\partial X_2}{\partial x_2} + \frac{\partial X_3}{\partial x_3}\right| \leq |A_1| + |A_2| + |A_3|$ at all points. Since 
\[
\int_{B^3}|A_1| + |A_2| + |A_3|dx_1 \wedge dx_2 \wedge dx_3 < \infty,
\]
by the dominated converge theorem we can exchange limit and integral and obtain
\begin{align*}
\frac{\partial}{\partial t}\bigg \rvert_{t = 0} \vol(\phi_t(B^3)) &= \int_{B^3} \(\frac{\partial X_1}{\partial x_1} + \frac{\partial X_2}{\partial x_2} + \frac{\partial X_3}{\partial x_3}\) dx_1 \wedge dx_2 \wedge dx_3 \\
&= \int_{S^2}  \langle X, \nu \rangle dvol_{S^2}. &(\text{Exercise 3}) 
\end{align*}
Since $\langle X, \nu \rangle \geq 0$, this last integral is nonnegative as long as $dvol_{S^2}$ is nonnegative in the sense that
\[
\int_{S^2}1 dvol_{S^2} > 0.
\]
To check this, one could parametrize $S^2$ with some charts $(\phi_\alpha)_{\alpha \in I}$. Then one would verify they are orientation-preserving by checking that 
\[
\det\(\nu, T_{{\phi_\alpha}^{-1}(p)}{\phi_\alpha}\(\frac{\partial}{\partial x}\), T_{{\phi_\alpha}^{-1}(p)}{\phi_\alpha}\(\frac{\partial}{\partial y}\)\) > 0
\]
for all $p \in S^2$. Finally one could calculate $\int_{S^2}1 dvol_{S^2} > 0$ by integrating $\phi_{\alpha}^*(dvol_{S^2})$ over all corresponding open subsets and check that it is all positive. But we know from calculus that the outward pointing vector field is always the ``positive'' one.
\end{oplossing}
\begin{opghand}[5]
Assume that a function $u: \RR^3 \to \RR$ is harmonic in the sense that
\[
\Delta u := \frac{\partial^2u}{\partial x_1^2} + \frac{\partial^2u}{\partial x_2^2}+ \frac{\partial^2u}{\partial x_3^2} = 0.
\]
Prove that $u$ has the following mean value property:
\[
\frac{\partial}{\partial r}\bigg \rvert_{r = 1} \int_{S^2} u_r dvol_{S^2} = 0,
\]
where $u_r: S^2 \to \RR$ is defined as $u_r(p) = u(rp)$ for all $r \in \RR, p \in S^2$.
\end{opghand}
\begin{oplossing}
Since $S^2$ is a compact set and $\frac{\partial}{\partial r} \rvert_{r = 1} u_r$ is a smooth function, $\frac{\partial}{\partial r} \rvert_{r = 1} u_r$ is bounded uniformly on $S^2$, hence by some (theorem related to) the dominated convergence theorem we can interchange derivative and integral. By the chain rule we have
\begin{align*}
\frac{\partial}{\partial r}\bigg \rvert_{r = 1} u_r(p) &= \frac{\partial}{\partial r}\bigg \rvert_{r = 1} u(rp) \\
&= Du(p) \cdot \frac{\partial}{\partial r}\bigg \rvert_{r = 1} rp \\
&= Du(p) \cdot \nu(p).
\end{align*}
So if we define the vector field $X = (\partial u/ \partial x_1, \partial u/ \partial x_2, \partial u/ \partial x_3)$, then we have $\langle X, \nu \rangle = \frac{\partial}{\partial r} \rvert_{r = 1} u_r$. Combining this with the interchanging of integral and derivative, we obtain
\begin{align*}
\frac{\partial}{\partial r}\bigg \rvert_{r = 1} \int_{S^2} u_r dvol_{S^2} &= \int_{S^2} \(\frac{\partial}{\partial r}\rvert_{r = 1}  u_r \)dvol_{S^2} \\
&=  \int_{S^2} \langle X, \nu \rangle dvol_{S^2} \\
&=  \int_{B^3} \frac{\partial^2u}{\partial x_1^2} + \frac{\partial^2u}{\partial x_2^2}+ \frac{\partial^2u}{\partial x_3^2} dx_1\wedge dx_2\wedge dx_3 &(\text{Exercise 3}) \\
&= \int_{B^3} 0 dx_1\wedge dx_2 \wedge dx_3 = 0, 
\end{align*}
as desired.
\end{oplossing}
\end{document}