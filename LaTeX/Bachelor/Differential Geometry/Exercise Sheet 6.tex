\documentclass[a4paper]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Exercise]{opghand}
\theoremstyle{definition}                            
\theoremstyle{remark}                                       
\newenvironment{oplossing}{\begin{proof}[Solution]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\id}{Id}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\GL}{GL}
\DeclareMathOperator{\expo}{exp}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}
\newcommand{\HH}{\mathbb{H}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{Differential Geometry: Exercise Sheet 6}
\author{Wouter Rienks, 11007745}
\maketitle

\begin{opghand}[17]
By employing the real, complex and quaternionic numbers, show that the spheres of dimension $0$, $1$ and $3$ carry a Lie group structure.
\end{opghand}
\begin{oplossing}
We treat all cases seperately.
\begin{itemize}
\item We can see $S^0$ as a submanifold of $\RR$, since if we set $g: \RR \to \RR: x \mapsto x^2 - 1$ then $S^0 = g^{-1}(0)$ (which is simply \{-1, 1\}). It's clear that the maps
\begin{align*}
*&: \RR^* \times \RR^* \mapsto \RR^*: (a, b) \to a \cdot b \\
\iota&: \RR^* \to \RR^*: a \mapsto a^{-1}
\end{align*}
are smooth. Now since $*(S^0) \subset S^0$ and $\iota(S^0) \subset S^0$, we may conclude that $*: S^0 \to S^0$ and $\iota: S^0 \to S^0$ are also smooth, which gives $S^0$ the structure of a Lie group.
\item We can  identify $\CC$ with $\RR^2$ using the obvious map 
\[
\varphi: \RR^2 \to \CC: (x, y) \to x + iy.
\]
Then the maps $*$ and $\iota$ defined by simply using multiplication and inversion on $\CC$ are smooth (considered $\CC^* \to \CC^*$). To see that $*$ is smooth, note that the corresponding map $\RR^2 \to \RR^2$ exists of purely polynomials in the coordinates, which is obviously smooth. To see that $\iota$ is smooth, note that the corresponding map $\RR^2 \setminus \{0\} \to \RR^2$ is of the form $(x^2 + y^2)^{-1}$ times some polynomials in the coordinates, which is smooth as long as $x^2 + y^2 \neq 0$, but we avoided $0$ already.

Now we can see $S^1$ as a submanifold of $\CC$, since if we set $g: \RR^2 \to \RR: x \mapsto x^2 + y^2 - 1$ then $S^1 = g^{-1}(0)$, so using the chart for $\CC$ we find that $S^1$ is a submanifold of $\CC$. Since once again $*(S^1) \subset S^1$ and $\iota(S^1) \subset S^1$,, we may conclude that $*: S^1 \to S^1$ and $\iota: S^1 \to S^1$ are also smooth, which gives $S^1$ the structure of a Lie group.
\item We can  identify $\HH$ with $\RR^4$ using the obvious map 
\[
\varphi: \RR^2 \to \CC: (x, y, z, w) \to x + iy + zj + wk.
\]
Then the maps $*$ and $\iota$ defined by simply using multiplication and inversion on $\HH$ are smooth (considered $\HH^* \to \HH^*$). To see that $*$ is smooth, note that the corresponding map $\RR^4 \to \RR^4$ exists of purely polynomials in the coordinates, which is obviously smooth. To see that $\iota$ is smooth, note that the corresponding map $\RR^4 \setminus \{0\}  \to \RR^4$ is of the form $(x^2 + y^2 + z^2 + w^2)^{-1}$ times some polynomials in the coordinates, which is smooth as long as $x^2 + y^2 + z^2 + w^2 \neq 0$, but we avoided $0$ already.

Now we can see $S^3$ as a submanifold of $\HH$, since if we set 
\[
g: \RR^4 \to \RR: x \mapsto x^2 + y^2 + z^2 + w^2 - 1,
\]
then $S^3 = g^{-1}(0)$, so using the chart for $\HH$ we find that $S^3$ is a submanifold of $\HH$. Since again $*(S^3) \subset S^3$ and $\iota(S^3) \subset S^3$,, we may conclude that $*: S^3 \to S^3$ and $\iota: S^3 \to S^3$ are also smooth, which gives $S^3$ the structure of a Lie group.
\end{itemize} 
\end{oplossing}
\begin{opghand}[18]
The famous hairy ball theorem states that every vector field on $S^2$ needs to be zero at some point. Use this result to prove that $S^2$ cannot carry a Lie group structure.
\end{opghand}
\begin{oplossing}
Suppose we had some Lie group structure on $S^2$. Note that $S^2$ has dimension $2$, so $T_eS^2$ is nontrivial and we can find a nonzero tangent vector $v \in T_eS^2$. Then this induces a left-invariant vector field $V$ on $S^2$ by
\[
V(g) = L_g^*(v)
\]
for any $v \in S^2$. By the hairy ball theorem there exists a $p \in S^2$ such that $V(p) = 0$. But then using the chain rule we see
\[
V(e) = L_e^*(v) = L_{p^{-1}}^* \circ L_p^*(v) = L_{p^{-1}}^*(0) = 0,
\]
however by construction we have $V(0) = v \neq 0$, so this is a contradiction. We therefore conclude $S^2$ cannot have a Lie group structure.
\end{oplossing}
\begin{opghand}[19]
Compute the Lie algebra of the Lie group $O(n) =\{A \in \RR^{n \times n} \mid A^TA = \id\}$ of orthogonal $n \times n$-matrices.
\end{opghand}
Note that if we let $g: \RR^{n \times n} \to \RR^{n \times n}: A \to A^TA - \id$ then clearly $O(n) = g^{-1}(0)$, hence the tangent space to $O(n)$ in the point $\id$ is given by $\ker T_{\id}(g)$. We calculate for any tangent vector $B \in \RR^{n \times n}$
\begin{align*}
T_{\id}(g)(B) &= \lim_{\varepsilon \to 0} \frac{g(\id + \varepsilon B)  - g(\id)}{\varepsilon} = \lim_{\varepsilon \to 0} \frac{(\id + \varepsilon B)^T(\id + \varepsilon B) - \id - 0}{\varepsilon}\\
&= \lim_{\varepsilon \to 0} \frac{\varepsilon B + \varepsilon B^T + \varepsilon^2 BB^T}{\varepsilon} = B + B^T.
\end{align*}
Hence the tangent space $T_{\id}O(n)$ is the set of all of the antisymmetric matrices $\{B \in R^{n \times n} \mid B = -B^T\}$. Therefore the Lie algebra is given by all $n$-dimensional antisymmetric matrices equipped with the standard Lie bracket.
\newpage
\begin{opghand}[20]
The determinant map defines a Lie group homomorphism from $(\GL(3, \RR), \cdot)$ to $(\RR^*, \cdot)$. Compute the corresponding homomorphism of Lie algebras.
\end{opghand}
\begin{oplossing}
We know the Lie algebra on $\RR^{3 \times 3}$ is $\RR^{3 \times 3}$ and the Lie algebra on $\RR^*$ is simply $\RR$. We first compute the map $T_{\id}(\det)$. Note that it is a linear map, so let $E_{ij}$ the matrix with a $1$ on the $(i,j)$-th entry and zeroes on all other entries. Then if $i = j$ we have $\det(Id + \varepsilon E_{ij}) = (1 + \varepsilon) \cdot 1 \cdot 1 = 1 + \varepsilon$, so 
\[
T_{\id}(\det)(E_{ij}) = \lim_{\varepsilon \to 0} \frac{\det(\id + \varepsilon E_{ij}) - \det{\id}}{\varepsilon} = \lim_{\varepsilon \to 0} \frac{\varepsilon}{\varepsilon} = 1.
\]
Now if $i \neq j$ then we can subtract some the $j$-th column from the $i$-column $\varepsilon$ times, which keeps the determinant invariant yet transforms $\id + \varepsilon E_{ij}$ into $\id$, so we then have $\det(\id + \varepsilon E_{ij}) = 1$. From this it immediately follows that $T_{id}(\det)(E_{ij}) = 0$. We conclude using the linearity of $T_{\id}(\det)$ that 
\[
T_{\id}(\det)(B) = \Tr(B).
\]
Since the Lie algebra homomorphism is given by $T_{\id}(\det)$, we conclude that $\Tr: \RR^{3 \times 3} \to \RR$ is the corresponding homomorphism of Lie algebras.
\end{oplossing}
\begin{opghand}[21]
The exponential map is defined to be the map
\[
\expo: \mathfrak{g} \to G, \xi \to \phi_1^X(e),
\]
where $\phi_1^X: G \to G$ denotes the flow of the left-invariant vector field $X$ with $X(e) = \xi$. Show that there are open neighborhoods $U$ of $0 \in \mathfrak{g}$ and $V$ of $e \in G$ such that exp restricts to a diffeomorhpism from $U$ to $V$.
\end{opghand}
Note that using the inverse function theorem it suffices to show $\exp$ has an invertible derivative in $0$ ($\exp$ is obviously smooth, since $X$ depends smoothly on $\xi$ and $\phi^X_1(e)$ depends smoothly on $X$). So let $\xi \in \mathfrak{g}$ be a tangent vector, then if $X$ is a left-invariant vector field with $X(e) = \xi$ we see
\begin{align}
T_{\exp}(0)\cdot\xi &= \frac{\partial}{\partial t} \exp(\xi t) \bigg \rvert_{t = 0} \\
&= \frac{\partial}{\partial t}\phi_1^{tX}(e) \bigg \rvert_{t = 0} \\
&= \frac{\partial}{\partial t}\phi_t^{X}(e) \bigg \rvert_{t = 0} \\
&= \xi.
\end{align}
Here (1) follows directly from the chain rule. To see that (2) holds, note that $(tX)(e) = t\xi$, so $tX$ is \emph{the} left-invariant vector field mapping $e$ to $t\xi$. 

To then see that (3) holds,  we show that $\phi^X_t(e) = \phi^{tX}_1(e)$. In order to prove this, we define $\varphi(s) = \phi^X_{s}(e)$. Then by definition of $\phi$ we have $\frac{\partial}{\partial s} \varphi(s) = X(\varphi(s))$. So by the chain rule we have $\frac{\partial}{\partial s} \varphi(ts) = (tX)(\varphi(ts))$. If we suggestively write $\psi(s) = \varphi(ts)$ then we thus have $\frac{\partial}{\partial s} \psi(s) = (tX)(\psi(s))$, so $\psi(s) = \phi^{tX}_s(e)$ by definition of $\phi^{tX}_s$. We thus conclude $\phi^{tX}_1(e) = \psi(1) = \varphi(t) = \phi^X_t(e)$, as desired. Finally (4) obviously holds since 
\[
\frac{\partial}{\partial t}\phi_t^{X}(e) \bigg \rvert_{t = 0} = X(\phi^X_t(e)) \bigg \rvert_{t = 0} = X(\phi^X_0(e)) = X(e) = \xi.
\]

So we conclude $T_{\exp}$ is simply the identity linear map and therefore invertible.
\newpage
\begin{opghand}[22]
Prove that the Lie algebra of every commutative Lie group carries a trivial Lie bracket.
\end{opghand}
\begin{oplossing}
Let $\iota: G \to G$ be the inversion map. If $G$ is commutative then 
\[
\iota(g)\iota(h) = g^{-1}h^{-1} = h^{-1}g^{-1} = (gh)^{-1} = \iota(gh)
\]
hence $\iota$ is a group homomorphism. It has been shown in the lecture that $\iota^*(X) = -X$ for all vector fields $X$. But then using the bilinearity of $[\cdot, \cdot]$ (and the fact that $\iota$ is a diffeomorphism) we see
\[
[X, Y] = [-X, -Y] = [\iota^*(X), \iota^*(Y)] = \iota^*[X, Y] = -[X, Y],
\]
which implies $[X, Y] = 0$ for all vector fields $X, Y$.
\end{oplossing}
\end{document}