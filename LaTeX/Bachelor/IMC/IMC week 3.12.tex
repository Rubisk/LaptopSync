\documentclass[a4paper,10pt]{article} 
\usepackage{mathtools, amssymb, amsthm}
\usepackage[dutch]{babel}
\usepackage{enumerate}
\usepackage[utf8]{inputenc}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{thmtools}
\usepackage{a4wide}


% environments
\declaretheoremstyle[notefont=\bfseries,notebraces={}{},headpunct={},postheadspace=1em]{handstyle}
\declaretheorem[style=handstyle,numbered=no,name=Opgave]{opghand}
\newtheorem*{lemma}{Lemma}
\theoremstyle{definition}
\theoremstyle{remark}             
\newenvironment{oplossing}{\begin{proof}[Oplossing]}{\end{proof}}
\newenvironment{uitwerking}{\begin{proof}[Uitwerking]}{\end{proof}}
\newenvironment{bewijs}{\begin{proof}[Bewijs]}{\end{proof}}

% operators
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\binomial}{Bin}
\DeclareMathOperator{\ggd}{ggd}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\rk}{rk}
\DeclareMathOperator{\im}{Im}
\DeclareMathOperator{\id}{id}

% set letters
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\TT}{\mathbb{T}}

% extra commando's
\newcommand{\twovector}[2]{\left(\begin{smallmatrix} #1 \\ #2 \end{smallmatrix}\right)}
\newcommand{\threevector}[3]{\left(\begin{smallmatrix} #1 \\ #2 \\ #3 \end{smallmatrix}\right)}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\renewcommand{\(}{\left(}
\renewcommand{\)}{\right)}

%% Document
\begin{document}

%% Kopregel
\title{IMC Week 12}
\author{Wouter Rienks, 11007745}
\maketitle
\begin{opghand}[12.11] Zij $\mathcal C$ de klasse van reëelwaardige continue differentiëerbare functies $f$ op het interval $[0, 1]$ die voldoen aan $f(0) = 0$ en $f(1) = 1$. Bepaal het grootste getal $u$ zodat
\[
u \leq \int_0^1 |f'(x) - f(x)| dx
\]
voor alle $f \in \mathcal{C}$.
\end{opghand}
\begin{oplossing}
Laat $I(f) = \int_0^1 |f'(x) - f(x)| dx$. Beschouw de deelruimte $\mathcal{D} \subseteq \mathcal{C}$ van niet-negatieve functies. Voor elke $f \in \mathcal{C}$ geldt met $a_f = \sup \{t \in [0, 1] \mid f(t) = 0\}$ dat 
\[
g(t) := \begin{cases}
f(t) & t > a_f, \\
0 & t \leq a_f.
\end{cases}
\]
voldoet aan $I(g) \leq I(f)$ en $g \geq 0$. We concluderen dat $u = \inf_{f \in \mathcal{C}} I(f) \geq \inf_{g \in \mathcal{D}} I(g)$.
Omdat $\mathcal{D} \subseteq \mathcal{C}$ volgt dat $u = \inf_{g \in \mathcal{D}} I(g)$. Nu geldt voor elke  $f \in \mathcal{D}$ dat $f(1) = 1$, dus 
\[
\xi_f = \min \{t \in [0, 1] \mid f(t) = e^{t - 1}\}
\]
bestaat, en
\begin{align*}
I(f) \geq \int_0^{\xi_f} |f'(t) - f(t)| &\geq \int_0^{\xi_t} |f'(t)| - \int_0^{\xi_t} |f(t)| \\
&\geq f(\xi_t) - \int_0^{\xi_t} e^{t - 1} \\
&= e^{\xi_t - 1} - (e^{\xi_t - 1} - e^{-1}) = \frac{1}{e}.
\end{align*}
Dus $u \geq \frac{1}{e}$. 

Merk vervolgens op dat we functies $f$ met $I(f)$ willekeurig dicht bij $1 / e$ kunnen construeren, door voor zekere $\varepsilon > 0$ op $[\varepsilon, 1]$ te stellen $f(t) = e^{t - 1}$, en $f$ op een strikt stijgende, continu-differentiëerbare manier van $0$ naar $e^{\varepsilon - 1}$ te laten gaan op $[0, \varepsilon]$. Dan zien we dat
\[
I(f) \leq \int_0^\varepsilon f'(s)ds + \int_0^\varepsilon f(s)ds \leq e^{\varepsilon - 1} + \varepsilon \to \frac{1}{e}
\]
als $\varepsilon \to 0$. We concluderen $u = \frac{1}{e}$. 
\end{oplossing}
\newpage
\begin{opghand}[12.19]
Zij $f(x)$ een continue, reëelwaardige functie op het interval $[0, 1]$. Laat zien dat
\[
\int_0^1 \int_0^1 |f(x) + f(y)| dx dy \geq \int_0^1 |f(x)|dx.
\]
\end{opghand}
\begin{oplossing}
Laat $A = \{x \in [0, 1] | f(x) \geq 0\}$ en $B = \{x \in [0, 1] | f(x) < 0\}$. Aangezien $f$ continu is zijn beide verzamelingen meetbaar, en het is evident dat $|A| + |B| = 1$.  Laat $I_A = \int_A |f|$ en $I_B = \int_B |f|$. Omdat
\begin{align*}
\int_A \int_A |f(x) + f(y)| dxdy = \int_A \int_A f(x) + f(y) = 2|A| I_A\\
\int_B \int_B |f(x) + f(y)| dxdy = \int_B \int_B -f(x) - f(y) = 2|B| I_B,
\end{align*}
zien we door symmetrie van $|f(x) + f(y)|$ dat
\[
\int_0^1 \int_0^1 |f(x) + f(y)| dx dy = 2|A|I_A + 2|B|I_B + 2 \int_A \int_B |f(x) + f(y)| dx dy.
\]
Neem zonder verlies van algemeenheid aan dat $|A| \geq |B|$ (beschouw anders $-f$). Stel eerst dat ook $I_A \geq I_B$. Dan volgt uit herschikkingsongelijkheid dat $|A| I_B + |B| I_A \leq |A| I_A + |B| I_B$. We concluderen
\begin{align*}
\int_0^1 \int_0^1 |f(x) + f(y)| dx dy &\geq 2(|A|I_A + |B|I_B) \\
& \geq |A|I_A + |A|I_B + |B|I_A + |B|I_B &(\text{vorige zin})\\
&= (|A| + |B|)\int_0^1 |f| &(I_A + I_B = \int_0^1 |f|)\\
&= \int_0^1 |f|.
\end{align*}
Resteert het geval $I_A < I_B$. Merk dan op dat
\begin{align*}
2 \int_A \int_B |f(x) + f(y)| dx dy &\geq 2 \int_A \int_B |f(x)| - | f(y)| dx dy = 2(|A| I_B - |B|I_A),
\end{align*}
zodat
\begin{align*}
\int_0^1 \int_0^1 |f(x) + f(y)| dx dy &\geq 2|A|I_A + 2|B|I_B + 2(|A| I_B - |B|I_A) \\
&= 2|A| \int_0^1|f| + 2|B|(I_B - I_A) \\
&\geq \int_0^1|f|, &(2A \geq A + B \geq 1 , I_B > I_A)
\end{align*}
zoals verlangd.
\end{oplossing}
\newpage
\begin{opghand}[12.20]
Bewijs dat er een constante $C$ bestaat zodat geldt dat als $p(x)$ een polynoom is van graad $2006$, dan is
\[
|p(0)| \leq C \int_{-1}^1|p(x)|dx.
\]
\end{opghand}
\begin{oplossing}
Door links en rechts te delen door $|p(0)| \cdot C$ volstaat het een constante $c > 0$ te vinden zodanig dat 
\[
c \leq \int_{-1}^1 |p(x)|dx
\]
voor alle polynomen $p$ van graad $2006$ met $p(0) = 1$ (als $p(0) = 0$ is de vergelijking sowieso waar).

Laat $S^{2007} = \{\vec x \in \RR^{2007} \mid ||\vec x|| = 1\}$ de eenheidsbol. Merk op dat de functie 
\[
\varphi: S^{2007} \to \RR_{> 0}: \vec x \mapsto \int_{-1}^1 \left | \sum_{i = 0}^{2007} x_it^i\right| dt
\]
continu is (en strikt positieve waarden aanneemt), omdat $S^{2007}$ compact is neemt deze functie een minimum $C$ aan.

Nu, voor elk polynoom $p$ met $||p|| \geq 1$ zal $q = p / ||p||$ voldoen aan $||q|| = 1$. Schrijven we $q(x) = \sum b_i x^i$, dan zien we dat $\vec b \in S^{2007}$, en $\int_{-1}^1 |q(x)|dx = \varphi(\vec b) \geq c$. Er volgt dat
\[
\int_{-1}^1 |p(x)|dx = ||p||\int_{-1}^1 |q(x)|dx \geq ||p||\cdot c \geq c,
\]
omdat elk polynoom met $p(0) = 1$ voldoet aan $||p|| \geq 1$ voldoet deze constante $c$ dus aan de eis.
\end{oplossing}
\end{document}



