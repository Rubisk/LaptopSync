\documentclass[a4paper,11pt]{article}
\usepackage{graphicx}
\usepackage{latexsym}
\newcommand{\binom}[2]{\left(\begin{array}{c}#1\\ #2\end{array}\right)}
\parindent0pt
\parskip\medskipamount
\begin{document}

\title{LIMO 2019}
\author{Tom Verhoeff \\
  Department of Mathematics \& Computer Science \\
  Eindhoven University of Technology \\
    \texttt{T.Verhoeff@tue.nl}
}
\date{January 2019}
\maketitle

\clearpage

\section* {Surplus?}

Consider the word \textsf{MISSISSIPPI.}
It consists of eleven letters: one M, four I's, two P's, and four S's.
These letters can be permuted in many orders, for example in alphabetic order:
\textsf{IIIIMPPSSSS}.

An \emph{inversion\/} in such a permutation is a pair of distinct letters that is not in alphabetic order.
A permutation is said to be \emph{even\/} when its number of inversions is even;
otherwise, it is called \emph{odd}.

A simple way to determine the number of inversions
is by writing the letters of the given permutation next to each other and
below them the same letters but now in alphabetic order.
Next, draw an edge between corresponding letters of the two permutations.
There is a 1--1 correspondence between inversions and crossing edges in the diagram.

\begin{figure}[hbt]
\centering
\includegraphics[height=5cm]{MISSISSIPPI-inversions}
\caption{Inversions in the word \textsf{MISSISSIPPI}}
\label{fig:inversions}
\end{figure}
For instance,
the inversions of the word \textsf{MISSISSIPPI} can be counted as crossing edges in the diagram
of Figure~\ref{fig:inversions}.
Thus, \textsf{MISSISSIPPI} has 24 inversions, and it is even.

\paragraph{Questions}
\begin{enumerate}
\item Consider the sets of even and odd permutations of the word \textsf{RARARA}.
Which set is larger, if any, and by how much?
\item Same for the word \textsf{MISSISSIPPI}.
\end{enumerate}
Motivate your answers.

Bonus: Give a general answer that applies to arbitrary words.


\clearpage

The solution is on the next page (but, of course, you try to solve it first without looking at the solution).


\clearpage
\section* {Solution}

Also see~\cite{Verhoeff:2017}.

\paragraph{Answers}
\begin{enumerate}
\item For the word \textsf{RARARA}, the number of even permutations equals
  the number of odd permutations. So, the surplus is zero.
\item For the word \textsf{MISSISSIPPI}, the number of even permutations exceeds
  that of the odd permutations. The surplus is 30.
\end{enumerate}

By analyzing shorter words (\textsf{A}, \textsf{AB}, \textsf{AAB}, \textsf{ABC}, etc.),
you quickly discover that for some words the numbers of even and odd permutations are equal,
and for others there is a surplus of even permutations,
but never a surplus of odd permutations.

One (well known) key insight is this

\textbf{Lemma}\quad
When swapping two adjacent distinct letters in a word,
the number of inversions changes by exactly one,
and, hence, the parity flips.

\textbf{Proof}\quad
See the diagram with crossing edges.
If the two adjacent distinct letters were in alphabetic order,
then they are \emph{not\/} in alphabetic order after the swap, and vice versa.
No other inversions are affected by the swap.~\hfill$\Box$

The other (less well known) key insight is captured in the following

\textbf{Theorem}\quad
The number of even permutations equals the number of odd permutations
if and only if there are two or more letters that occur an odd number of times.
And if the number of letters that occur an odd number of times is less than two,
then there are more even permutations, and this surplus can be calculated as
the multinomial coefficient
\begin{equation}
\label{eq:surplus}
  \binom{n \div 2}{k_1 \div 2,\ \ldots,\ k_m \div 2}
\end{equation}
where $\div$ denotes division without remainder,
$n$ is the total number of letters, and the $k_i$ are the counts of the letters that occur.

\textbf{Proof}\quad
Define a \emph{stutter permutation\/} as a permutation where,
if you group ---from left to right--- the letters in non-overlapping pairs,
then each group consists of identical letters.
In case of an odd number of letters, the last group contains only one letter.
Here is the pairing for the word \textsf{MISSISSIPPI} into 6~groups:
\begin{center}
  \textsf{MI / SS / IS / SI / PP / I}
\end{center}
So, \textsf{MISSISSIPPI} is not a stutter permutation,
but \textsf{IISSIISSPPM} is.

Some observations about stutter permutations:
\begin{enumerate}
\item In a stutter permutation, all letters except at most one, occur an even number of times.
\item If a word has two or more letters occurring an odd number of times,
  then it admits no stutter permutations.
\item Stutter permutations are even,
  since inversions always come in pairs.
\item All non-stutter permutations can be paired off as follows.
  Consider the leftmost letter pair (in the grouping used in the definition of stutter permutation)
  with distinct letters
  (such a pair exists because it is not a stutter permutation).
  According to the lemma,
  swapping that pair yields a permutation of opposite parity.
  These two permutations are paired off.
  Thus, there is a 1--1 correspondence between even and odd \emph{non-stutter\/} permutations.
\end{enumerate}
Therefore, the surplus of even permutations equals the number of stutter permutations.
All that remains is to count them in case there are some.
By considering the stutter pairs as single symbols,
you see why formula~(\ref{eq:surplus}) is correct.~\hfill$\Box$

Note that the total number of permutations is given by
\begin{eqnarray}
\label{eq:total}
  \binom{n}{k_1,\ \ldots,\ k_m} & = & \frac{n!}{k_1! \cdot \ldots \cdot k_m!}
\end{eqnarray}
Applying this to the word \textsf{RARARA}:
\begin{itemize}
\item The total number of permutations equals $\binom{6}{3,\ 3} = 20$.
\item There are no sutter permutations, because both letters occur an odd number of times.
\item The number of even permutations equals the number of odd permutations.
\end{itemize}
Applying this to the word \textsf{MISSISSIPPI}:
\begin{itemize}
\item The total number of permutations equals
  $$\binom{11}{4,\ 1,\ 2,\ 4} = 11\cdot 10\cdot 9\cdot 7\cdot 5 = 34\,650$$.
\item There are stutter permutations, because only the letter~\textsf{M}
  occurs an odd number of times.
\item The number of even permutations exceeds the number of odd permutations,
  and the surplus equals $$\binom{11\div 2}{4\div 2,\ 1\div 2,\ 2\div 2,\ 4\div 2} =
  \binom{5}{2,\ 0,\ 1,\ 2} = 5\cdot 3\cdot 2 = 30$$.
\end{itemize}

\subsection* {Alternative approaches}

\begin{itemize}
\item
The question about \textsf{RARARA} can easily be solved by hand,
because there are only 20~permutations.

\item
In general, the case with two letters that both occur an odd number of times has
a simpler solution, based on these observations:
\begin{itemize}
\item The number of pairs of distinct symbols is odd (product of two odd numbers).
  In case of \textsf{RARARA}, it is~$3\times 3 = 9$.
\item This is the maximum number of inversions.
\item When you \emph{reverse\/} a permutations, every inversion becomes a non-inversion,
  and vice versa.
\item Thus, the number of inversions after reversing
  equals the maximum number minus the original number of inversions.
\item Consequently, the parity of the reversed permutation is opposite that of the original permutation.
\item There are no \emph{palindromes\/} (reversal symmetric permutations) of \textsf{RARARA},
  since in a palindrome at most one letter can occur an odd number of times
  (viz.\ when the length is odd, and it appears in the middle).
\item Thus, reversal provides a 1--1 correspondence between even and odd permutations.
\end{itemize}

\end{itemize}

\section* {Considerations}

\begin{enumerate}
\item The word \textsf{RARARA} was included to
  provide something manageable by hand using brute force.
  Also, it offers some opportunity for generalization.
  
\item However,
  if you want to avoid manual solution by brute force, but keep it simpler than the general case,
  then we could ask the word \textsf{RARARARARA}.
  
\item The word \textsf{MISSISSIPPI} is not solvable by hand using brute force alone.
  Also, its answer differs from that for~Q1.
  It does not fall under the generalization of~Q1.
  I don't know whether there is a simpler solution for this specific case,
  or a generalization which is does not solve the full problem.
\end{enumerate}


\begin{thebibliography}{9}

\bibitem{Verhoeff:2017}
Tom Verhoeff.
``The Spurs of D. H. Lehmer: Hamiltonian paths in neighbor-swap graphs of permutations''.
\emph{Designs, Codes and Cryptography},
\textbf{84}(1):295--310 (July 2017).

\end{thebibliography}

\end{document}
